
build/robotflesh.elf:     file format elf32-littlearm

Sections:
Idx Name          Size      VMA       LMA       File off  Algn  Flags
  0 .isr_vector   00000188  08000000  08000000  00010000  2**0  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .text         000044e0  080001c0  080001c0  000101c0  2**6  CONTENTS, ALLOC, LOAD, READONLY, CODE
  2 .rodata       000001e4  080046a0  080046a0  000146a0  2**3  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .init_array   00000004  08004884  08004884  00014884  2**2  CONTENTS, ALLOC, LOAD, DATA
  4 .fini_array   00000004  08004888  08004888  00014888  2**2  CONTENTS, ALLOC, LOAD, DATA
  5 .data         000000e8  20000000  0800488c  00020000  2**2  CONTENTS, ALLOC, LOAD, DATA
  6 .ccmram       00000000  10000000  10000000  000200e8  2**0  CONTENTS
  7 .bss          00012ce0  200000e8  200000e8  000200e8  2**2  ALLOC
  8 ._user_heap_stack 00000600  20012dc8  20012dc8  000200e8  2**0  ALLOC
  9 .ARM.attributes 00000030  00000000  00000000  000200e8  2**0  CONTENTS, READONLY
 10 .comment      00000031  00000000  00000000  00020118  2**0  CONTENTS, READONLY
 11 .debug_info   00012d4d  00000000  00000000  00020149  2**0  CONTENTS, READONLY, DEBUGGING
 12 .debug_abbrev 00002d9d  00000000  00000000  00032e96  2**0  CONTENTS, READONLY, DEBUGGING
 13 .debug_loc    00008562  00000000  00000000  00035c33  2**0  CONTENTS, READONLY, DEBUGGING
 14 .debug_aranges 00000740  00000000  00000000  0003e198  2**3  CONTENTS, READONLY, DEBUGGING
 15 .debug_ranges 000006d0  00000000  00000000  0003e8d8  2**0  CONTENTS, READONLY, DEBUGGING
 16 .debug_line   0000483d  00000000  00000000  0003efa8  2**0  CONTENTS, READONLY, DEBUGGING
 17 .debug_str    000036eb  00000000  00000000  000437e5  2**0  CONTENTS, READONLY, DEBUGGING
 18 .debug_frame  00001ba4  00000000  00000000  00046ed0  2**2  CONTENTS, READONLY, DEBUGGING

Disassembly of section .text:

080001c0 <deregister_tm_clones>:
 80001c0:	4b04      	ldr	r3, [pc, #16]	; (80001d4 <deregister_tm_clones+0x14>)
 80001c2:	4805      	ldr	r0, [pc, #20]	; (80001d8 <deregister_tm_clones+0x18>)
 80001c4:	1a1b      	subs	r3, r3, r0
 80001c6:	2b06      	cmp	r3, #6
 80001c8:	d902      	bls.n	80001d0 <deregister_tm_clones+0x10>
 80001ca:	4b04      	ldr	r3, [pc, #16]	; (80001dc <deregister_tm_clones+0x1c>)
 80001cc:	b103      	cbz	r3, 80001d0 <deregister_tm_clones+0x10>
 80001ce:	4718      	bx	r3
 80001d0:	4770      	bx	lr
 80001d2:	bf00      	nop
 80001d4:	200000eb 	.word	0x200000eb
 80001d8:	200000e8 	.word	0x200000e8
 80001dc:	00000000 	.word	0x00000000

080001e0 <register_tm_clones>:
 80001e0:	4905      	ldr	r1, [pc, #20]	; (80001f8 <register_tm_clones+0x18>)
 80001e2:	4806      	ldr	r0, [pc, #24]	; (80001fc <register_tm_clones+0x1c>)
 80001e4:	1a09      	subs	r1, r1, r0
 80001e6:	1089      	asrs	r1, r1, #2
 80001e8:	eb01 71d1 	add.w	r1, r1, r1, lsr #31
 80001ec:	1049      	asrs	r1, r1, #1
 80001ee:	d002      	beq.n	80001f6 <register_tm_clones+0x16>
 80001f0:	4b03      	ldr	r3, [pc, #12]	; (8000200 <register_tm_clones+0x20>)
 80001f2:	b103      	cbz	r3, 80001f6 <register_tm_clones+0x16>
 80001f4:	4718      	bx	r3
 80001f6:	4770      	bx	lr
 80001f8:	200000e8 	.word	0x200000e8
 80001fc:	200000e8 	.word	0x200000e8
 8000200:	00000000 	.word	0x00000000

08000204 <__do_global_dtors_aux>:
 8000204:	b510      	push	{r4, lr}
 8000206:	4c06      	ldr	r4, [pc, #24]	; (8000220 <__do_global_dtors_aux+0x1c>)
 8000208:	7823      	ldrb	r3, [r4, #0]
 800020a:	b943      	cbnz	r3, 800021e <__do_global_dtors_aux+0x1a>
 800020c:	f7ff ffd8 	bl	80001c0 <deregister_tm_clones>
 8000210:	4b04      	ldr	r3, [pc, #16]	; (8000224 <__do_global_dtors_aux+0x20>)
 8000212:	b113      	cbz	r3, 800021a <__do_global_dtors_aux+0x16>
 8000214:	4804      	ldr	r0, [pc, #16]	; (8000228 <__do_global_dtors_aux+0x24>)
 8000216:	f3af 8000 	nop.w
 800021a:	2301      	movs	r3, #1
 800021c:	7023      	strb	r3, [r4, #0]
 800021e:	bd10      	pop	{r4, pc}
 8000220:	200000e8 	.word	0x200000e8
 8000224:	00000000 	.word	0x00000000
 8000228:	08004688 	.word	0x08004688

0800022c <frame_dummy>:
 800022c:	b508      	push	{r3, lr}
 800022e:	4b08      	ldr	r3, [pc, #32]	; (8000250 <frame_dummy+0x24>)
 8000230:	b11b      	cbz	r3, 800023a <frame_dummy+0xe>
 8000232:	4908      	ldr	r1, [pc, #32]	; (8000254 <frame_dummy+0x28>)
 8000234:	4808      	ldr	r0, [pc, #32]	; (8000258 <frame_dummy+0x2c>)
 8000236:	f3af 8000 	nop.w
 800023a:	4808      	ldr	r0, [pc, #32]	; (800025c <frame_dummy+0x30>)
 800023c:	6803      	ldr	r3, [r0, #0]
 800023e:	b913      	cbnz	r3, 8000246 <frame_dummy+0x1a>
 8000240:	e8bd 4008 	ldmia.w	sp!, {r3, lr}
 8000244:	e7cc      	b.n	80001e0 <register_tm_clones>
 8000246:	4b06      	ldr	r3, [pc, #24]	; (8000260 <frame_dummy+0x34>)
 8000248:	2b00      	cmp	r3, #0
 800024a:	d0f9      	beq.n	8000240 <frame_dummy+0x14>
 800024c:	4798      	blx	r3
 800024e:	e7f7      	b.n	8000240 <frame_dummy+0x14>
 8000250:	00000000 	.word	0x00000000
 8000254:	200000ec 	.word	0x200000ec
 8000258:	08004688 	.word	0x08004688
 800025c:	200000e8 	.word	0x200000e8
 8000260:	00000000 	.word	0x00000000

08000264 <__libc_init_array>:
 8000264:	b570      	push	{r4, r5, r6, lr}
 8000266:	4e0f      	ldr	r6, [pc, #60]	; (80002a4 <__libc_init_array+0x40>)
 8000268:	4d0f      	ldr	r5, [pc, #60]	; (80002a8 <__libc_init_array+0x44>)
 800026a:	1b76      	subs	r6, r6, r5
 800026c:	10b6      	asrs	r6, r6, #2
 800026e:	bf18      	it	ne
 8000270:	2400      	movne	r4, #0
 8000272:	d005      	beq.n	8000280 <__libc_init_array+0x1c>
 8000274:	3401      	adds	r4, #1
 8000276:	f855 3b04 	ldr.w	r3, [r5], #4
 800027a:	4798      	blx	r3
 800027c:	42a6      	cmp	r6, r4
 800027e:	d1f9      	bne.n	8000274 <__libc_init_array+0x10>
 8000280:	4e0a      	ldr	r6, [pc, #40]	; (80002ac <__libc_init_array+0x48>)
 8000282:	4d0b      	ldr	r5, [pc, #44]	; (80002b0 <__libc_init_array+0x4c>)
 8000284:	1b76      	subs	r6, r6, r5
 8000286:	f004 f9ff 	bl	8004688 <_init>
 800028a:	10b6      	asrs	r6, r6, #2
 800028c:	bf18      	it	ne
 800028e:	2400      	movne	r4, #0
 8000290:	d006      	beq.n	80002a0 <__libc_init_array+0x3c>
 8000292:	3401      	adds	r4, #1
 8000294:	f855 3b04 	ldr.w	r3, [r5], #4
 8000298:	4798      	blx	r3
 800029a:	42a6      	cmp	r6, r4
 800029c:	d1f9      	bne.n	8000292 <__libc_init_array+0x2e>
 800029e:	bd70      	pop	{r4, r5, r6, pc}
 80002a0:	bd70      	pop	{r4, r5, r6, pc}
 80002a2:	bf00      	nop
 80002a4:	08004884 	.word	0x08004884
 80002a8:	08004884 	.word	0x08004884
 80002ac:	08004888 	.word	0x08004888
 80002b0:	08004884 	.word	0x08004884

080002b4 <memcpy>:
 80002b4:	4684      	mov	ip, r0
 80002b6:	ea41 0300 	orr.w	r3, r1, r0
 80002ba:	f013 0303 	ands.w	r3, r3, #3
 80002be:	d16d      	bne.n	800039c <memcpy+0xe8>
 80002c0:	3a40      	subs	r2, #64	; 0x40
 80002c2:	d341      	bcc.n	8000348 <memcpy+0x94>
 80002c4:	f851 3b04 	ldr.w	r3, [r1], #4
 80002c8:	f840 3b04 	str.w	r3, [r0], #4
 80002cc:	f851 3b04 	ldr.w	r3, [r1], #4
 80002d0:	f840 3b04 	str.w	r3, [r0], #4
 80002d4:	f851 3b04 	ldr.w	r3, [r1], #4
 80002d8:	f840 3b04 	str.w	r3, [r0], #4
 80002dc:	f851 3b04 	ldr.w	r3, [r1], #4
 80002e0:	f840 3b04 	str.w	r3, [r0], #4
 80002e4:	f851 3b04 	ldr.w	r3, [r1], #4
 80002e8:	f840 3b04 	str.w	r3, [r0], #4
 80002ec:	f851 3b04 	ldr.w	r3, [r1], #4
 80002f0:	f840 3b04 	str.w	r3, [r0], #4
 80002f4:	f851 3b04 	ldr.w	r3, [r1], #4
 80002f8:	f840 3b04 	str.w	r3, [r0], #4
 80002fc:	f851 3b04 	ldr.w	r3, [r1], #4
 8000300:	f840 3b04 	str.w	r3, [r0], #4
 8000304:	f851 3b04 	ldr.w	r3, [r1], #4
 8000308:	f840 3b04 	str.w	r3, [r0], #4
 800030c:	f851 3b04 	ldr.w	r3, [r1], #4
 8000310:	f840 3b04 	str.w	r3, [r0], #4
 8000314:	f851 3b04 	ldr.w	r3, [r1], #4
 8000318:	f840 3b04 	str.w	r3, [r0], #4
 800031c:	f851 3b04 	ldr.w	r3, [r1], #4
 8000320:	f840 3b04 	str.w	r3, [r0], #4
 8000324:	f851 3b04 	ldr.w	r3, [r1], #4
 8000328:	f840 3b04 	str.w	r3, [r0], #4
 800032c:	f851 3b04 	ldr.w	r3, [r1], #4
 8000330:	f840 3b04 	str.w	r3, [r0], #4
 8000334:	f851 3b04 	ldr.w	r3, [r1], #4
 8000338:	f840 3b04 	str.w	r3, [r0], #4
 800033c:	f851 3b04 	ldr.w	r3, [r1], #4
 8000340:	f840 3b04 	str.w	r3, [r0], #4
 8000344:	3a40      	subs	r2, #64	; 0x40
 8000346:	d2bd      	bcs.n	80002c4 <memcpy+0x10>
 8000348:	3230      	adds	r2, #48	; 0x30
 800034a:	d311      	bcc.n	8000370 <memcpy+0xbc>
 800034c:	f851 3b04 	ldr.w	r3, [r1], #4
 8000350:	f840 3b04 	str.w	r3, [r0], #4
 8000354:	f851 3b04 	ldr.w	r3, [r1], #4
 8000358:	f840 3b04 	str.w	r3, [r0], #4
 800035c:	f851 3b04 	ldr.w	r3, [r1], #4
 8000360:	f840 3b04 	str.w	r3, [r0], #4
 8000364:	f851 3b04 	ldr.w	r3, [r1], #4
 8000368:	f840 3b04 	str.w	r3, [r0], #4
 800036c:	3a10      	subs	r2, #16
 800036e:	d2ed      	bcs.n	800034c <memcpy+0x98>
 8000370:	320c      	adds	r2, #12
 8000372:	d305      	bcc.n	8000380 <memcpy+0xcc>
 8000374:	f851 3b04 	ldr.w	r3, [r1], #4
 8000378:	f840 3b04 	str.w	r3, [r0], #4
 800037c:	3a04      	subs	r2, #4
 800037e:	d2f9      	bcs.n	8000374 <memcpy+0xc0>
 8000380:	3204      	adds	r2, #4
 8000382:	d008      	beq.n	8000396 <memcpy+0xe2>
 8000384:	07d2      	lsls	r2, r2, #31
 8000386:	bf1c      	itt	ne
 8000388:	f811 3b01 	ldrbne.w	r3, [r1], #1
 800038c:	f800 3b01 	strbne.w	r3, [r0], #1
 8000390:	d301      	bcc.n	8000396 <memcpy+0xe2>
 8000392:	880b      	ldrh	r3, [r1, #0]
 8000394:	8003      	strh	r3, [r0, #0]
 8000396:	4660      	mov	r0, ip
 8000398:	4770      	bx	lr
 800039a:	bf00      	nop
 800039c:	2a08      	cmp	r2, #8
 800039e:	d313      	bcc.n	80003c8 <memcpy+0x114>
 80003a0:	078b      	lsls	r3, r1, #30
 80003a2:	d08d      	beq.n	80002c0 <memcpy+0xc>
 80003a4:	f010 0303 	ands.w	r3, r0, #3
 80003a8:	d08a      	beq.n	80002c0 <memcpy+0xc>
 80003aa:	f1c3 0304 	rsb	r3, r3, #4
 80003ae:	1ad2      	subs	r2, r2, r3
 80003b0:	07db      	lsls	r3, r3, #31
 80003b2:	bf1c      	itt	ne
 80003b4:	f811 3b01 	ldrbne.w	r3, [r1], #1
 80003b8:	f800 3b01 	strbne.w	r3, [r0], #1
 80003bc:	d380      	bcc.n	80002c0 <memcpy+0xc>
 80003be:	f831 3b02 	ldrh.w	r3, [r1], #2
 80003c2:	f820 3b02 	strh.w	r3, [r0], #2
 80003c6:	e77b      	b.n	80002c0 <memcpy+0xc>
 80003c8:	3a04      	subs	r2, #4
 80003ca:	d3d9      	bcc.n	8000380 <memcpy+0xcc>
 80003cc:	3a01      	subs	r2, #1
 80003ce:	f811 3b01 	ldrb.w	r3, [r1], #1
 80003d2:	f800 3b01 	strb.w	r3, [r0], #1
 80003d6:	d2f9      	bcs.n	80003cc <memcpy+0x118>
 80003d8:	780b      	ldrb	r3, [r1, #0]
 80003da:	7003      	strb	r3, [r0, #0]
 80003dc:	784b      	ldrb	r3, [r1, #1]
 80003de:	7043      	strb	r3, [r0, #1]
 80003e0:	788b      	ldrb	r3, [r1, #2]
 80003e2:	7083      	strb	r3, [r0, #2]
 80003e4:	4660      	mov	r0, ip
 80003e6:	4770      	bx	lr

080003e8 <memset>:
 80003e8:	b470      	push	{r4, r5, r6}
 80003ea:	0786      	lsls	r6, r0, #30
 80003ec:	d046      	beq.n	800047c <memset+0x94>
 80003ee:	1e54      	subs	r4, r2, #1
 80003f0:	2a00      	cmp	r2, #0
 80003f2:	d041      	beq.n	8000478 <memset+0x90>
 80003f4:	b2ca      	uxtb	r2, r1
 80003f6:	4603      	mov	r3, r0
 80003f8:	e002      	b.n	8000400 <memset+0x18>
 80003fa:	f114 34ff 	adds.w	r4, r4, #4294967295	; 0xffffffff
 80003fe:	d33b      	bcc.n	8000478 <memset+0x90>
 8000400:	f803 2b01 	strb.w	r2, [r3], #1
 8000404:	079d      	lsls	r5, r3, #30
 8000406:	d1f8      	bne.n	80003fa <memset+0x12>
 8000408:	2c03      	cmp	r4, #3
 800040a:	d92e      	bls.n	800046a <memset+0x82>
 800040c:	b2cd      	uxtb	r5, r1
 800040e:	ea45 2505 	orr.w	r5, r5, r5, lsl #8
 8000412:	2c0f      	cmp	r4, #15
 8000414:	ea45 4505 	orr.w	r5, r5, r5, lsl #16
 8000418:	d919      	bls.n	800044e <memset+0x66>
 800041a:	f103 0210 	add.w	r2, r3, #16
 800041e:	4626      	mov	r6, r4
 8000420:	3e10      	subs	r6, #16
 8000422:	2e0f      	cmp	r6, #15
 8000424:	f842 5c10 	str.w	r5, [r2, #-16]
 8000428:	f842 5c0c 	str.w	r5, [r2, #-12]
 800042c:	f842 5c08 	str.w	r5, [r2, #-8]
 8000430:	f842 5c04 	str.w	r5, [r2, #-4]
 8000434:	f102 0210 	add.w	r2, r2, #16
 8000438:	d8f2      	bhi.n	8000420 <memset+0x38>
 800043a:	f1a4 0210 	sub.w	r2, r4, #16
 800043e:	f022 020f 	bic.w	r2, r2, #15
 8000442:	f004 040f 	and.w	r4, r4, #15
 8000446:	3210      	adds	r2, #16
 8000448:	2c03      	cmp	r4, #3
 800044a:	4413      	add	r3, r2
 800044c:	d90d      	bls.n	800046a <memset+0x82>
 800044e:	461e      	mov	r6, r3
 8000450:	4622      	mov	r2, r4
 8000452:	3a04      	subs	r2, #4
 8000454:	2a03      	cmp	r2, #3
 8000456:	f846 5b04 	str.w	r5, [r6], #4
 800045a:	d8fa      	bhi.n	8000452 <memset+0x6a>
 800045c:	1f22      	subs	r2, r4, #4
 800045e:	f022 0203 	bic.w	r2, r2, #3
 8000462:	3204      	adds	r2, #4
 8000464:	4413      	add	r3, r2
 8000466:	f004 0403 	and.w	r4, r4, #3
 800046a:	b12c      	cbz	r4, 8000478 <memset+0x90>
 800046c:	b2c9      	uxtb	r1, r1
 800046e:	441c      	add	r4, r3
 8000470:	f803 1b01 	strb.w	r1, [r3], #1
 8000474:	429c      	cmp	r4, r3
 8000476:	d1fb      	bne.n	8000470 <memset+0x88>
 8000478:	bc70      	pop	{r4, r5, r6}
 800047a:	4770      	bx	lr
 800047c:	4614      	mov	r4, r2
 800047e:	4603      	mov	r3, r0
 8000480:	e7c2      	b.n	8000408 <memset+0x20>
 8000482:	bf00      	nop

08000484 <__cvt>:
 8000484:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 8000488:	ec57 6b10 	vmov	r6, r7, d0
 800048c:	b084      	sub	sp, #16
 800048e:	2f00      	cmp	r7, #0
 8000490:	460d      	mov	r5, r1
 8000492:	4692      	mov	sl, r2
 8000494:	4619      	mov	r1, r3
 8000496:	9c0d      	ldr	r4, [sp, #52]	; 0x34
 8000498:	db60      	blt.n	800055c <__cvt+0xd8>
 800049a:	2300      	movs	r3, #0
 800049c:	700b      	strb	r3, [r1, #0]
 800049e:	2c66      	cmp	r4, #102	; 0x66
 80004a0:	d045      	beq.n	800052e <__cvt+0xaa>
 80004a2:	2c46      	cmp	r4, #70	; 0x46
 80004a4:	d043      	beq.n	800052e <__cvt+0xaa>
 80004a6:	f024 0320 	bic.w	r3, r4, #32
 80004aa:	2b45      	cmp	r3, #69	; 0x45
 80004ac:	d02b      	beq.n	8000506 <__cvt+0x82>
 80004ae:	aa03      	add	r2, sp, #12
 80004b0:	ab02      	add	r3, sp, #8
 80004b2:	9201      	str	r2, [sp, #4]
 80004b4:	9300      	str	r3, [sp, #0]
 80004b6:	462a      	mov	r2, r5
 80004b8:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 80004ba:	2102      	movs	r1, #2
 80004bc:	ec47 6b10 	vmov	d0, r6, r7
 80004c0:	f000 fe62 	bl	8001188 <_dtoa_r>
 80004c4:	2c67      	cmp	r4, #103	; 0x67
 80004c6:	4680      	mov	r8, r0
 80004c8:	d12a      	bne.n	8000520 <__cvt+0x9c>
 80004ca:	f01a 0f01 	tst.w	sl, #1
 80004ce:	d02c      	beq.n	800052a <__cvt+0xa6>
 80004d0:	4445      	add	r5, r8
 80004d2:	4630      	mov	r0, r6
 80004d4:	4639      	mov	r1, r7
 80004d6:	2200      	movs	r2, #0
 80004d8:	2300      	movs	r3, #0
 80004da:	f003 f8cf 	bl	800367c <__aeabi_dcmpeq>
 80004de:	2800      	cmp	r0, #0
 80004e0:	d13a      	bne.n	8000558 <__cvt+0xd4>
 80004e2:	9b03      	ldr	r3, [sp, #12]
 80004e4:	429d      	cmp	r5, r3
 80004e6:	d906      	bls.n	80004f6 <__cvt+0x72>
 80004e8:	2130      	movs	r1, #48	; 0x30
 80004ea:	1c5a      	adds	r2, r3, #1
 80004ec:	9203      	str	r2, [sp, #12]
 80004ee:	7019      	strb	r1, [r3, #0]
 80004f0:	9b03      	ldr	r3, [sp, #12]
 80004f2:	429d      	cmp	r5, r3
 80004f4:	d8f9      	bhi.n	80004ea <__cvt+0x66>
 80004f6:	9a0e      	ldr	r2, [sp, #56]	; 0x38
 80004f8:	eba3 0308 	sub.w	r3, r3, r8
 80004fc:	4640      	mov	r0, r8
 80004fe:	6013      	str	r3, [r2, #0]
 8000500:	b004      	add	sp, #16
 8000502:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 8000506:	a903      	add	r1, sp, #12
 8000508:	ab02      	add	r3, sp, #8
 800050a:	3501      	adds	r5, #1
 800050c:	9101      	str	r1, [sp, #4]
 800050e:	9300      	str	r3, [sp, #0]
 8000510:	462a      	mov	r2, r5
 8000512:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 8000514:	2102      	movs	r1, #2
 8000516:	ec47 6b10 	vmov	d0, r6, r7
 800051a:	f000 fe35 	bl	8001188 <_dtoa_r>
 800051e:	4680      	mov	r8, r0
 8000520:	2c47      	cmp	r4, #71	; 0x47
 8000522:	d1d5      	bne.n	80004d0 <__cvt+0x4c>
 8000524:	f01a 0f01 	tst.w	sl, #1
 8000528:	d1d2      	bne.n	80004d0 <__cvt+0x4c>
 800052a:	9b03      	ldr	r3, [sp, #12]
 800052c:	e7e3      	b.n	80004f6 <__cvt+0x72>
 800052e:	aa03      	add	r2, sp, #12
 8000530:	ab02      	add	r3, sp, #8
 8000532:	9201      	str	r2, [sp, #4]
 8000534:	9300      	str	r3, [sp, #0]
 8000536:	462a      	mov	r2, r5
 8000538:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 800053a:	2103      	movs	r1, #3
 800053c:	ec47 6b10 	vmov	d0, r6, r7
 8000540:	f000 fe22 	bl	8001188 <_dtoa_r>
 8000544:	7803      	ldrb	r3, [r0, #0]
 8000546:	2b30      	cmp	r3, #48	; 0x30
 8000548:	4680      	mov	r8, r0
 800054a:	eb00 0905 	add.w	r9, r0, r5
 800054e:	d00d      	beq.n	800056c <__cvt+0xe8>
 8000550:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 8000552:	681d      	ldr	r5, [r3, #0]
 8000554:	444d      	add	r5, r9
 8000556:	e7bc      	b.n	80004d2 <__cvt+0x4e>
 8000558:	462b      	mov	r3, r5
 800055a:	e7cc      	b.n	80004f6 <__cvt+0x72>
 800055c:	f107 4300 	add.w	r3, r7, #2147483648	; 0x80000000
 8000560:	f04f 0e2d 	mov.w	lr, #45	; 0x2d
 8000564:	461f      	mov	r7, r3
 8000566:	f881 e000 	strb.w	lr, [r1]
 800056a:	e798      	b.n	800049e <__cvt+0x1a>
 800056c:	2200      	movs	r2, #0
 800056e:	2300      	movs	r3, #0
 8000570:	4630      	mov	r0, r6
 8000572:	4639      	mov	r1, r7
 8000574:	f003 f882 	bl	800367c <__aeabi_dcmpeq>
 8000578:	2800      	cmp	r0, #0
 800057a:	d1e9      	bne.n	8000550 <__cvt+0xcc>
 800057c:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 800057e:	f1c5 0501 	rsb	r5, r5, #1
 8000582:	601d      	str	r5, [r3, #0]
 8000584:	444d      	add	r5, r9
 8000586:	e7a4      	b.n	80004d2 <__cvt+0x4e>

08000588 <__exponent>:
 8000588:	b4f0      	push	{r4, r5, r6, r7}
 800058a:	2900      	cmp	r1, #0
 800058c:	bfba      	itte	lt
 800058e:	4249      	neglt	r1, r1
 8000590:	232d      	movlt	r3, #45	; 0x2d
 8000592:	232b      	movge	r3, #43	; 0x2b
 8000594:	2909      	cmp	r1, #9
 8000596:	b082      	sub	sp, #8
 8000598:	7002      	strb	r2, [r0, #0]
 800059a:	7043      	strb	r3, [r0, #1]
 800059c:	f100 0402 	add.w	r4, r0, #2
 80005a0:	dd2e      	ble.n	8000600 <__exponent+0x78>
 80005a2:	f10d 0507 	add.w	r5, sp, #7
 80005a6:	462e      	mov	r6, r5
 80005a8:	4f1b      	ldr	r7, [pc, #108]	; (8000618 <__exponent+0x90>)
 80005aa:	e000      	b.n	80005ae <__exponent+0x26>
 80005ac:	4616      	mov	r6, r2
 80005ae:	fb87 2301 	smull	r2, r3, r7, r1
 80005b2:	17ca      	asrs	r2, r1, #31
 80005b4:	ebc2 02a3 	rsb	r2, r2, r3, asr #2
 80005b8:	eb02 0382 	add.w	r3, r2, r2, lsl #2
 80005bc:	eba1 0143 	sub.w	r1, r1, r3, lsl #1
 80005c0:	f101 0330 	add.w	r3, r1, #48	; 0x30
 80005c4:	4611      	mov	r1, r2
 80005c6:	2909      	cmp	r1, #9
 80005c8:	f806 3c01 	strb.w	r3, [r6, #-1]
 80005cc:	f106 32ff 	add.w	r2, r6, #4294967295	; 0xffffffff
 80005d0:	dcec      	bgt.n	80005ac <__exponent+0x24>
 80005d2:	3130      	adds	r1, #48	; 0x30
 80005d4:	1eb3      	subs	r3, r6, #2
 80005d6:	b2c9      	uxtb	r1, r1
 80005d8:	429d      	cmp	r5, r3
 80005da:	f802 1c01 	strb.w	r1, [r2, #-1]
 80005de:	d919      	bls.n	8000614 <__exponent+0x8c>
 80005e0:	4613      	mov	r3, r2
 80005e2:	4626      	mov	r6, r4
 80005e4:	e001      	b.n	80005ea <__exponent+0x62>
 80005e6:	f813 1b01 	ldrb.w	r1, [r3], #1
 80005ea:	f806 1b01 	strb.w	r1, [r6], #1
 80005ee:	429d      	cmp	r5, r3
 80005f0:	d1f9      	bne.n	80005e6 <__exponent+0x5e>
 80005f2:	1c6b      	adds	r3, r5, #1
 80005f4:	1a9a      	subs	r2, r3, r2
 80005f6:	4422      	add	r2, r4
 80005f8:	1a10      	subs	r0, r2, r0
 80005fa:	b002      	add	sp, #8
 80005fc:	bcf0      	pop	{r4, r5, r6, r7}
 80005fe:	4770      	bx	lr
 8000600:	4622      	mov	r2, r4
 8000602:	3130      	adds	r1, #48	; 0x30
 8000604:	2330      	movs	r3, #48	; 0x30
 8000606:	f802 3b02 	strb.w	r3, [r2], #2
 800060a:	7061      	strb	r1, [r4, #1]
 800060c:	1a10      	subs	r0, r2, r0
 800060e:	b002      	add	sp, #8
 8000610:	bcf0      	pop	{r4, r5, r6, r7}
 8000612:	4770      	bx	lr
 8000614:	4622      	mov	r2, r4
 8000616:	e7f9      	b.n	800060c <__exponent+0x84>
 8000618:	66666667 	.word	0x66666667

0800061c <_printf_float>:
 800061c:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8000620:	b08d      	sub	sp, #52	; 0x34
 8000622:	460c      	mov	r4, r1
 8000624:	4616      	mov	r6, r2
 8000626:	461f      	mov	r7, r3
 8000628:	4605      	mov	r5, r0
 800062a:	f8dd 8058 	ldr.w	r8, [sp, #88]	; 0x58
 800062e:	f001 fd63 	bl	80020f8 <_localeconv_r>
 8000632:	6803      	ldr	r3, [r0, #0]
 8000634:	9306      	str	r3, [sp, #24]
 8000636:	4618      	mov	r0, r3
 8000638:	f000 fca2 	bl	8000f80 <strlen>
 800063c:	f8d4 b000 	ldr.w	fp, [r4]
 8000640:	9007      	str	r0, [sp, #28]
 8000642:	2300      	movs	r3, #0
 8000644:	f41b 7f80 	tst.w	fp, #256	; 0x100
 8000648:	f894 a018 	ldrb.w	sl, [r4, #24]
 800064c:	930a      	str	r3, [sp, #40]	; 0x28
 800064e:	f000 80d3 	beq.w	80007f8 <_printf_float+0x1dc>
 8000652:	f8d8 1000 	ldr.w	r1, [r8]
 8000656:	3107      	adds	r1, #7
 8000658:	f021 0107 	bic.w	r1, r1, #7
 800065c:	e9d1 2300 	ldrd	r2, r3, [r1]
 8000660:	3108      	adds	r1, #8
 8000662:	f8c8 1000 	str.w	r1, [r8]
 8000666:	e9cd 2304 	strd	r2, r3, [sp, #16]
 800066a:	e9c4 2312 	strd	r2, r3, [r4, #72]	; 0x48
 800066e:	e9dd 2304 	ldrd	r2, r3, [sp, #16]
 8000672:	f023 4900 	bic.w	r9, r3, #2147483648	; 0x80000000
 8000676:	4690      	mov	r8, r2
 8000678:	4610      	mov	r0, r2
 800067a:	4649      	mov	r1, r9
 800067c:	f04f 32ff 	mov.w	r2, #4294967295	; 0xffffffff
 8000680:	4bb3      	ldr	r3, [pc, #716]	; (8000950 <_printf_float+0x334>)
 8000682:	f003 f82d 	bl	80036e0 <__aeabi_dcmpun>
 8000686:	2800      	cmp	r0, #0
 8000688:	d16f      	bne.n	800076a <_printf_float+0x14e>
 800068a:	4640      	mov	r0, r8
 800068c:	4649      	mov	r1, r9
 800068e:	f04f 32ff 	mov.w	r2, #4294967295	; 0xffffffff
 8000692:	4baf      	ldr	r3, [pc, #700]	; (8000950 <_printf_float+0x334>)
 8000694:	f003 f806 	bl	80036a4 <__aeabi_dcmple>
 8000698:	2800      	cmp	r0, #0
 800069a:	d166      	bne.n	800076a <_printf_float+0x14e>
 800069c:	e9dd 0104 	ldrd	r0, r1, [sp, #16]
 80006a0:	2200      	movs	r2, #0
 80006a2:	2300      	movs	r3, #0
 80006a4:	f002 fff4 	bl	8003690 <__aeabi_dcmplt>
 80006a8:	2800      	cmp	r0, #0
 80006aa:	f040 811f 	bne.w	80008ec <_printf_float+0x2d0>
 80006ae:	48a9      	ldr	r0, [pc, #676]	; (8000954 <_printf_float+0x338>)
 80006b0:	49a9      	ldr	r1, [pc, #676]	; (8000958 <_printf_float+0x33c>)
 80006b2:	f02b 0204 	bic.w	r2, fp, #4
 80006b6:	6022      	str	r2, [r4, #0]
 80006b8:	4602      	mov	r2, r0
 80006ba:	2303      	movs	r3, #3
 80006bc:	f1ba 0f47 	cmp.w	sl, #71	; 0x47
 80006c0:	bf98      	it	ls
 80006c2:	460a      	movls	r2, r1
 80006c4:	9204      	str	r2, [sp, #16]
 80006c6:	6123      	str	r3, [r4, #16]
 80006c8:	f04f 0800 	mov.w	r8, #0
 80006cc:	9700      	str	r7, [sp, #0]
 80006ce:	4633      	mov	r3, r6
 80006d0:	aa0b      	add	r2, sp, #44	; 0x2c
 80006d2:	4621      	mov	r1, r4
 80006d4:	4628      	mov	r0, r5
 80006d6:	f000 fa4f 	bl	8000b78 <_printf_common>
 80006da:	3001      	adds	r0, #1
 80006dc:	d040      	beq.n	8000760 <_printf_float+0x144>
 80006de:	6823      	ldr	r3, [r4, #0]
 80006e0:	0559      	lsls	r1, r3, #21
 80006e2:	f140 80fb 	bpl.w	80008dc <_printf_float+0x2c0>
 80006e6:	f1ba 0f65 	cmp.w	sl, #101	; 0x65
 80006ea:	f240 8094 	bls.w	8000816 <_printf_float+0x1fa>
 80006ee:	2200      	movs	r2, #0
 80006f0:	2300      	movs	r3, #0
 80006f2:	e9d4 0112 	ldrd	r0, r1, [r4, #72]	; 0x48
 80006f6:	f002 ffc1 	bl	800367c <__aeabi_dcmpeq>
 80006fa:	2800      	cmp	r0, #0
 80006fc:	f040 81b8 	bne.w	8000a70 <_printf_float+0x454>
 8000700:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8000702:	2b00      	cmp	r3, #0
 8000704:	f340 81fd 	ble.w	8000b02 <_printf_float+0x4e6>
 8000708:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800070a:	f8d4 9058 	ldr.w	r9, [r4, #88]	; 0x58
 800070e:	454b      	cmp	r3, r9
 8000710:	bfa8      	it	ge
 8000712:	464b      	movge	r3, r9
 8000714:	2b00      	cmp	r3, #0
 8000716:	469a      	mov	sl, r3
 8000718:	dd07      	ble.n	800072a <_printf_float+0x10e>
 800071a:	9a04      	ldr	r2, [sp, #16]
 800071c:	4631      	mov	r1, r6
 800071e:	4628      	mov	r0, r5
 8000720:	47b8      	blx	r7
 8000722:	3001      	adds	r0, #1
 8000724:	d01c      	beq.n	8000760 <_printf_float+0x144>
 8000726:	f8d4 9058 	ldr.w	r9, [r4, #88]	; 0x58
 800072a:	ea2a 7aea 	bic.w	sl, sl, sl, asr #31
 800072e:	eba9 030a 	sub.w	r3, r9, sl
 8000732:	2b00      	cmp	r3, #0
 8000734:	f340 8156 	ble.w	80009e4 <_printf_float+0x3c8>
 8000738:	f104 0b1a 	add.w	fp, r4, #26
 800073c:	f04f 0800 	mov.w	r8, #0
 8000740:	e005      	b.n	800074e <_printf_float+0x132>
 8000742:	6da3      	ldr	r3, [r4, #88]	; 0x58
 8000744:	eba3 020a 	sub.w	r2, r3, sl
 8000748:	4542      	cmp	r2, r8
 800074a:	f340 814a 	ble.w	80009e2 <_printf_float+0x3c6>
 800074e:	2301      	movs	r3, #1
 8000750:	465a      	mov	r2, fp
 8000752:	4631      	mov	r1, r6
 8000754:	4628      	mov	r0, r5
 8000756:	47b8      	blx	r7
 8000758:	3001      	adds	r0, #1
 800075a:	f108 0801 	add.w	r8, r8, #1
 800075e:	d1f0      	bne.n	8000742 <_printf_float+0x126>
 8000760:	f04f 30ff 	mov.w	r0, #4294967295	; 0xffffffff
 8000764:	b00d      	add	sp, #52	; 0x34
 8000766:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800076a:	e9dd 0104 	ldrd	r0, r1, [sp, #16]
 800076e:	4602      	mov	r2, r0
 8000770:	460b      	mov	r3, r1
 8000772:	f002 ffb5 	bl	80036e0 <__aeabi_dcmpun>
 8000776:	2800      	cmp	r0, #0
 8000778:	f040 80db 	bne.w	8000932 <_printf_float+0x316>
 800077c:	6861      	ldr	r1, [r4, #4]
 800077e:	1c48      	adds	r0, r1, #1
 8000780:	f000 810a 	beq.w	8000998 <_printf_float+0x37c>
 8000784:	f1ba 0f67 	cmp.w	sl, #103	; 0x67
 8000788:	f000 81a2 	beq.w	8000ad0 <_printf_float+0x4b4>
 800078c:	f1ba 0f47 	cmp.w	sl, #71	; 0x47
 8000790:	f000 80b0 	beq.w	80008f4 <_printf_float+0x2d8>
 8000794:	f44b 6280 	orr.w	r2, fp, #1024	; 0x400
 8000798:	2300      	movs	r3, #0
 800079a:	6022      	str	r2, [r4, #0]
 800079c:	a80a      	add	r0, sp, #40	; 0x28
 800079e:	9303      	str	r3, [sp, #12]
 80007a0:	ab09      	add	r3, sp, #36	; 0x24
 80007a2:	9002      	str	r0, [sp, #8]
 80007a4:	9300      	str	r3, [sp, #0]
 80007a6:	f8cd a004 	str.w	sl, [sp, #4]
 80007aa:	f10d 0323 	add.w	r3, sp, #35	; 0x23
 80007ae:	ed9d 0b04 	vldr	d0, [sp, #16]
 80007b2:	4628      	mov	r0, r5
 80007b4:	f7ff fe66 	bl	8000484 <__cvt>
 80007b8:	9004      	str	r0, [sp, #16]
 80007ba:	f1ba 0f65 	cmp.w	sl, #101	; 0x65
 80007be:	9b09      	ldr	r3, [sp, #36]	; 0x24
 80007c0:	f240 80d8 	bls.w	8000974 <_printf_float+0x358>
 80007c4:	f1ba 0f66 	cmp.w	sl, #102	; 0x66
 80007c8:	f000 8188 	beq.w	8000adc <_printf_float+0x4c0>
 80007cc:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 80007ce:	429a      	cmp	r2, r3
 80007d0:	f300 80ff 	bgt.w	80009d2 <_printf_float+0x3b6>
 80007d4:	6822      	ldr	r2, [r4, #0]
 80007d6:	6123      	str	r3, [r4, #16]
 80007d8:	07d0      	lsls	r0, r2, #31
 80007da:	d501      	bpl.n	80007e0 <_printf_float+0x1c4>
 80007dc:	1c5a      	adds	r2, r3, #1
 80007de:	6122      	str	r2, [r4, #16]
 80007e0:	65a3      	str	r3, [r4, #88]	; 0x58
 80007e2:	f04f 0800 	mov.w	r8, #0
 80007e6:	f89d 3023 	ldrb.w	r3, [sp, #35]	; 0x23
 80007ea:	2b00      	cmp	r3, #0
 80007ec:	f43f af6e 	beq.w	80006cc <_printf_float+0xb0>
 80007f0:	232d      	movs	r3, #45	; 0x2d
 80007f2:	f884 3043 	strb.w	r3, [r4, #67]	; 0x43
 80007f6:	e769      	b.n	80006cc <_printf_float+0xb0>
 80007f8:	f8d8 3000 	ldr.w	r3, [r8]
 80007fc:	3307      	adds	r3, #7
 80007fe:	f023 0307 	bic.w	r3, r3, #7
 8000802:	ed93 7b00 	vldr	d7, [r3]
 8000806:	3308      	adds	r3, #8
 8000808:	f8c8 3000 	str.w	r3, [r8]
 800080c:	ed8d 7b04 	vstr	d7, [sp, #16]
 8000810:	ed84 7b12 	vstr	d7, [r4, #72]	; 0x48
 8000814:	e72b      	b.n	800066e <_printf_float+0x52>
 8000816:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 8000818:	2a01      	cmp	r2, #1
 800081a:	dd62      	ble.n	80008e2 <_printf_float+0x2c6>
 800081c:	2301      	movs	r3, #1
 800081e:	9a04      	ldr	r2, [sp, #16]
 8000820:	4631      	mov	r1, r6
 8000822:	4628      	mov	r0, r5
 8000824:	47b8      	blx	r7
 8000826:	3001      	adds	r0, #1
 8000828:	d09a      	beq.n	8000760 <_printf_float+0x144>
 800082a:	9b07      	ldr	r3, [sp, #28]
 800082c:	9a06      	ldr	r2, [sp, #24]
 800082e:	4631      	mov	r1, r6
 8000830:	4628      	mov	r0, r5
 8000832:	47b8      	blx	r7
 8000834:	3001      	adds	r0, #1
 8000836:	d093      	beq.n	8000760 <_printf_float+0x144>
 8000838:	2300      	movs	r3, #0
 800083a:	2200      	movs	r2, #0
 800083c:	e9d4 0112 	ldrd	r0, r1, [r4, #72]	; 0x48
 8000840:	f002 ff1c 	bl	800367c <__aeabi_dcmpeq>
 8000844:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000846:	b1a0      	cbz	r0, 8000872 <_printf_float+0x256>
 8000848:	2b01      	cmp	r3, #1
 800084a:	dd1b      	ble.n	8000884 <_printf_float+0x268>
 800084c:	f104 0a1a 	add.w	sl, r4, #26
 8000850:	f04f 0900 	mov.w	r9, #0
 8000854:	e003      	b.n	800085e <_printf_float+0x242>
 8000856:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000858:	3b01      	subs	r3, #1
 800085a:	454b      	cmp	r3, r9
 800085c:	dd12      	ble.n	8000884 <_printf_float+0x268>
 800085e:	2301      	movs	r3, #1
 8000860:	4652      	mov	r2, sl
 8000862:	4631      	mov	r1, r6
 8000864:	4628      	mov	r0, r5
 8000866:	47b8      	blx	r7
 8000868:	3001      	adds	r0, #1
 800086a:	f109 0901 	add.w	r9, r9, #1
 800086e:	d1f2      	bne.n	8000856 <_printf_float+0x23a>
 8000870:	e776      	b.n	8000760 <_printf_float+0x144>
 8000872:	9a04      	ldr	r2, [sp, #16]
 8000874:	3b01      	subs	r3, #1
 8000876:	3201      	adds	r2, #1
 8000878:	4631      	mov	r1, r6
 800087a:	4628      	mov	r0, r5
 800087c:	47b8      	blx	r7
 800087e:	3001      	adds	r0, #1
 8000880:	f43f af6e 	beq.w	8000760 <_printf_float+0x144>
 8000884:	4643      	mov	r3, r8
 8000886:	f104 0250 	add.w	r2, r4, #80	; 0x50
 800088a:	4631      	mov	r1, r6
 800088c:	4628      	mov	r0, r5
 800088e:	47b8      	blx	r7
 8000890:	3001      	adds	r0, #1
 8000892:	f43f af65 	beq.w	8000760 <_printf_float+0x144>
 8000896:	6823      	ldr	r3, [r4, #0]
 8000898:	68e0      	ldr	r0, [r4, #12]
 800089a:	f013 0f02 	tst.w	r3, #2
 800089e:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
 80008a0:	d016      	beq.n	80008d0 <_printf_float+0x2b4>
 80008a2:	1ac2      	subs	r2, r0, r3
 80008a4:	2a00      	cmp	r2, #0
 80008a6:	dd13      	ble.n	80008d0 <_printf_float+0x2b4>
 80008a8:	f104 0919 	add.w	r9, r4, #25
 80008ac:	f04f 0800 	mov.w	r8, #0
 80008b0:	e004      	b.n	80008bc <_printf_float+0x2a0>
 80008b2:	68e0      	ldr	r0, [r4, #12]
 80008b4:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
 80008b6:	1ac2      	subs	r2, r0, r3
 80008b8:	4542      	cmp	r2, r8
 80008ba:	dd09      	ble.n	80008d0 <_printf_float+0x2b4>
 80008bc:	2301      	movs	r3, #1
 80008be:	464a      	mov	r2, r9
 80008c0:	4631      	mov	r1, r6
 80008c2:	4628      	mov	r0, r5
 80008c4:	47b8      	blx	r7
 80008c6:	3001      	adds	r0, #1
 80008c8:	f108 0801 	add.w	r8, r8, #1
 80008cc:	d1f1      	bne.n	80008b2 <_printf_float+0x296>
 80008ce:	e747      	b.n	8000760 <_printf_float+0x144>
 80008d0:	4298      	cmp	r0, r3
 80008d2:	bfb8      	it	lt
 80008d4:	4618      	movlt	r0, r3
 80008d6:	b00d      	add	sp, #52	; 0x34
 80008d8:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 80008dc:	9a04      	ldr	r2, [sp, #16]
 80008de:	6923      	ldr	r3, [r4, #16]
 80008e0:	e7d3      	b.n	800088a <_printf_float+0x26e>
 80008e2:	07db      	lsls	r3, r3, #31
 80008e4:	d49a      	bmi.n	800081c <_printf_float+0x200>
 80008e6:	9a04      	ldr	r2, [sp, #16]
 80008e8:	2301      	movs	r3, #1
 80008ea:	e7c5      	b.n	8000878 <_printf_float+0x25c>
 80008ec:	232d      	movs	r3, #45	; 0x2d
 80008ee:	f884 3043 	strb.w	r3, [r4, #67]	; 0x43
 80008f2:	e6dc      	b.n	80006ae <_printf_float+0x92>
 80008f4:	2900      	cmp	r1, #0
 80008f6:	f000 80ee 	beq.w	8000ad6 <_printf_float+0x4ba>
 80008fa:	f44b 6280 	orr.w	r2, fp, #1024	; 0x400
 80008fe:	2300      	movs	r3, #0
 8000900:	6022      	str	r2, [r4, #0]
 8000902:	a80a      	add	r0, sp, #40	; 0x28
 8000904:	9303      	str	r3, [sp, #12]
 8000906:	ab09      	add	r3, sp, #36	; 0x24
 8000908:	9002      	str	r0, [sp, #8]
 800090a:	9300      	str	r3, [sp, #0]
 800090c:	f8cd a004 	str.w	sl, [sp, #4]
 8000910:	f10d 0323 	add.w	r3, sp, #35	; 0x23
 8000914:	ed9d 0b04 	vldr	d0, [sp, #16]
 8000918:	4628      	mov	r0, r5
 800091a:	f7ff fdb3 	bl	8000484 <__cvt>
 800091e:	9004      	str	r0, [sp, #16]
 8000920:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8000922:	1cd9      	adds	r1, r3, #3
 8000924:	db1e      	blt.n	8000964 <_printf_float+0x348>
 8000926:	6862      	ldr	r2, [r4, #4]
 8000928:	4293      	cmp	r3, r2
 800092a:	dc1b      	bgt.n	8000964 <_printf_float+0x348>
 800092c:	f04f 0a67 	mov.w	sl, #103	; 0x67
 8000930:	e74c      	b.n	80007cc <_printf_float+0x1b0>
 8000932:	490a      	ldr	r1, [pc, #40]	; (800095c <_printf_float+0x340>)
 8000934:	4a0a      	ldr	r2, [pc, #40]	; (8000960 <_printf_float+0x344>)
 8000936:	f02b 0304 	bic.w	r3, fp, #4
 800093a:	6023      	str	r3, [r4, #0]
 800093c:	f1ba 0f47 	cmp.w	sl, #71	; 0x47
 8000940:	bf98      	it	ls
 8000942:	4611      	movls	r1, r2
 8000944:	2303      	movs	r3, #3
 8000946:	9104      	str	r1, [sp, #16]
 8000948:	6123      	str	r3, [r4, #16]
 800094a:	f04f 0800 	mov.w	r8, #0
 800094e:	e6bd      	b.n	80006cc <_printf_float+0xb0>
 8000950:	7fefffff 	.word	0x7fefffff
 8000954:	08004830 	.word	0x08004830
 8000958:	0800482c 	.word	0x0800482c
 800095c:	08004838 	.word	0x08004838
 8000960:	08004834 	.word	0x08004834
 8000964:	f1aa 0a02 	sub.w	sl, sl, #2
 8000968:	fa5f fa8a 	uxtb.w	sl, sl
 800096c:	f1ba 0f65 	cmp.w	sl, #101	; 0x65
 8000970:	f63f af28 	bhi.w	80007c4 <_printf_float+0x1a8>
 8000974:	3b01      	subs	r3, #1
 8000976:	4619      	mov	r1, r3
 8000978:	4652      	mov	r2, sl
 800097a:	f104 0050 	add.w	r0, r4, #80	; 0x50
 800097e:	9309      	str	r3, [sp, #36]	; 0x24
 8000980:	f7ff fe02 	bl	8000588 <__exponent>
 8000984:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 8000986:	1883      	adds	r3, r0, r2
 8000988:	2a01      	cmp	r2, #1
 800098a:	4680      	mov	r8, r0
 800098c:	6123      	str	r3, [r4, #16]
 800098e:	f340 80b3 	ble.w	8000af8 <_printf_float+0x4dc>
 8000992:	3301      	adds	r3, #1
 8000994:	6123      	str	r3, [r4, #16]
 8000996:	e726      	b.n	80007e6 <_printf_float+0x1ca>
 8000998:	2106      	movs	r1, #6
 800099a:	6061      	str	r1, [r4, #4]
 800099c:	f44b 6280 	orr.w	r2, fp, #1024	; 0x400
 80009a0:	2300      	movs	r3, #0
 80009a2:	6022      	str	r2, [r4, #0]
 80009a4:	a80a      	add	r0, sp, #40	; 0x28
 80009a6:	9303      	str	r3, [sp, #12]
 80009a8:	ab09      	add	r3, sp, #36	; 0x24
 80009aa:	9002      	str	r0, [sp, #8]
 80009ac:	9300      	str	r3, [sp, #0]
 80009ae:	f8cd a004 	str.w	sl, [sp, #4]
 80009b2:	f10d 0323 	add.w	r3, sp, #35	; 0x23
 80009b6:	ed9d 0b04 	vldr	d0, [sp, #16]
 80009ba:	4628      	mov	r0, r5
 80009bc:	f7ff fd62 	bl	8000484 <__cvt>
 80009c0:	f1ba 0f67 	cmp.w	sl, #103	; 0x67
 80009c4:	9004      	str	r0, [sp, #16]
 80009c6:	d0ab      	beq.n	8000920 <_printf_float+0x304>
 80009c8:	f1ba 0f47 	cmp.w	sl, #71	; 0x47
 80009cc:	f47f aef5 	bne.w	80007ba <_printf_float+0x19e>
 80009d0:	e7a6      	b.n	8000920 <_printf_float+0x304>
 80009d2:	2b00      	cmp	r3, #0
 80009d4:	bfd4      	ite	le
 80009d6:	f1c3 0102 	rsble	r1, r3, #2
 80009da:	2101      	movgt	r1, #1
 80009dc:	440a      	add	r2, r1
 80009de:	6122      	str	r2, [r4, #16]
 80009e0:	e6fe      	b.n	80007e0 <_printf_float+0x1c4>
 80009e2:	4699      	mov	r9, r3
 80009e4:	9b09      	ldr	r3, [sp, #36]	; 0x24
 80009e6:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 80009e8:	4293      	cmp	r3, r2
 80009ea:	db36      	blt.n	8000a5a <_printf_float+0x43e>
 80009ec:	6821      	ldr	r1, [r4, #0]
 80009ee:	07c9      	lsls	r1, r1, #31
 80009f0:	d433      	bmi.n	8000a5a <_printf_float+0x43e>
 80009f2:	eba2 0809 	sub.w	r8, r2, r9
 80009f6:	1ad3      	subs	r3, r2, r3
 80009f8:	4598      	cmp	r8, r3
 80009fa:	bfa8      	it	ge
 80009fc:	4698      	movge	r8, r3
 80009fe:	f1b8 0f00 	cmp.w	r8, #0
 8000a02:	dd0c      	ble.n	8000a1e <_printf_float+0x402>
 8000a04:	9b04      	ldr	r3, [sp, #16]
 8000a06:	444b      	add	r3, r9
 8000a08:	461a      	mov	r2, r3
 8000a0a:	4631      	mov	r1, r6
 8000a0c:	4643      	mov	r3, r8
 8000a0e:	4628      	mov	r0, r5
 8000a10:	47b8      	blx	r7
 8000a12:	3001      	adds	r0, #1
 8000a14:	f43f aea4 	beq.w	8000760 <_printf_float+0x144>
 8000a18:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000a1a:	9a09      	ldr	r2, [sp, #36]	; 0x24
 8000a1c:	1a9b      	subs	r3, r3, r2
 8000a1e:	ea28 79e8 	bic.w	r9, r8, r8, asr #31
 8000a22:	eba3 0309 	sub.w	r3, r3, r9
 8000a26:	2b00      	cmp	r3, #0
 8000a28:	f77f af35 	ble.w	8000896 <_printf_float+0x27a>
 8000a2c:	f104 0a1a 	add.w	sl, r4, #26
 8000a30:	f04f 0800 	mov.w	r8, #0
 8000a34:	e007      	b.n	8000a46 <_printf_float+0x42a>
 8000a36:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000a38:	9a09      	ldr	r2, [sp, #36]	; 0x24
 8000a3a:	1a9b      	subs	r3, r3, r2
 8000a3c:	eba3 0309 	sub.w	r3, r3, r9
 8000a40:	4543      	cmp	r3, r8
 8000a42:	f77f af28 	ble.w	8000896 <_printf_float+0x27a>
 8000a46:	2301      	movs	r3, #1
 8000a48:	4652      	mov	r2, sl
 8000a4a:	4631      	mov	r1, r6
 8000a4c:	4628      	mov	r0, r5
 8000a4e:	47b8      	blx	r7
 8000a50:	3001      	adds	r0, #1
 8000a52:	f108 0801 	add.w	r8, r8, #1
 8000a56:	d1ee      	bne.n	8000a36 <_printf_float+0x41a>
 8000a58:	e682      	b.n	8000760 <_printf_float+0x144>
 8000a5a:	9b07      	ldr	r3, [sp, #28]
 8000a5c:	9a06      	ldr	r2, [sp, #24]
 8000a5e:	4631      	mov	r1, r6
 8000a60:	4628      	mov	r0, r5
 8000a62:	47b8      	blx	r7
 8000a64:	3001      	adds	r0, #1
 8000a66:	f43f ae7b 	beq.w	8000760 <_printf_float+0x144>
 8000a6a:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 8000a6c:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8000a6e:	e7c0      	b.n	80009f2 <_printf_float+0x3d6>
 8000a70:	2301      	movs	r3, #1
 8000a72:	4a40      	ldr	r2, [pc, #256]	; (8000b74 <_printf_float+0x558>)
 8000a74:	4631      	mov	r1, r6
 8000a76:	4628      	mov	r0, r5
 8000a78:	47b8      	blx	r7
 8000a7a:	3001      	adds	r0, #1
 8000a7c:	f43f ae70 	beq.w	8000760 <_printf_float+0x144>
 8000a80:	9a09      	ldr	r2, [sp, #36]	; 0x24
 8000a82:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000a84:	429a      	cmp	r2, r3
 8000a86:	db03      	blt.n	8000a90 <_printf_float+0x474>
 8000a88:	6823      	ldr	r3, [r4, #0]
 8000a8a:	07da      	lsls	r2, r3, #31
 8000a8c:	f57f af04 	bpl.w	8000898 <_printf_float+0x27c>
 8000a90:	9b07      	ldr	r3, [sp, #28]
 8000a92:	9a06      	ldr	r2, [sp, #24]
 8000a94:	4631      	mov	r1, r6
 8000a96:	4628      	mov	r0, r5
 8000a98:	47b8      	blx	r7
 8000a9a:	3001      	adds	r0, #1
 8000a9c:	f43f ae60 	beq.w	8000760 <_printf_float+0x144>
 8000aa0:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000aa2:	2b01      	cmp	r3, #1
 8000aa4:	f77f aef7 	ble.w	8000896 <_printf_float+0x27a>
 8000aa8:	f104 091a 	add.w	r9, r4, #26
 8000aac:	f04f 0800 	mov.w	r8, #0
 8000ab0:	e004      	b.n	8000abc <_printf_float+0x4a0>
 8000ab2:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000ab4:	3b01      	subs	r3, #1
 8000ab6:	4543      	cmp	r3, r8
 8000ab8:	f77f aeed 	ble.w	8000896 <_printf_float+0x27a>
 8000abc:	2301      	movs	r3, #1
 8000abe:	464a      	mov	r2, r9
 8000ac0:	4631      	mov	r1, r6
 8000ac2:	4628      	mov	r0, r5
 8000ac4:	47b8      	blx	r7
 8000ac6:	3001      	adds	r0, #1
 8000ac8:	f108 0801 	add.w	r8, r8, #1
 8000acc:	d1f1      	bne.n	8000ab2 <_printf_float+0x496>
 8000ace:	e647      	b.n	8000760 <_printf_float+0x144>
 8000ad0:	2900      	cmp	r1, #0
 8000ad2:	f47f af12 	bne.w	80008fa <_printf_float+0x2de>
 8000ad6:	2101      	movs	r1, #1
 8000ad8:	6061      	str	r1, [r4, #4]
 8000ada:	e75f      	b.n	800099c <_printf_float+0x380>
 8000adc:	2b00      	cmp	r3, #0
 8000ade:	6862      	ldr	r2, [r4, #4]
 8000ae0:	dd3d      	ble.n	8000b5e <_printf_float+0x542>
 8000ae2:	6123      	str	r3, [r4, #16]
 8000ae4:	b11a      	cbz	r2, 8000aee <_printf_float+0x4d2>
 8000ae6:	3201      	adds	r2, #1
 8000ae8:	441a      	add	r2, r3
 8000aea:	6122      	str	r2, [r4, #16]
 8000aec:	e678      	b.n	80007e0 <_printf_float+0x1c4>
 8000aee:	6821      	ldr	r1, [r4, #0]
 8000af0:	07c8      	lsls	r0, r1, #31
 8000af2:	f57f ae75 	bpl.w	80007e0 <_printf_float+0x1c4>
 8000af6:	e7f6      	b.n	8000ae6 <_printf_float+0x4ca>
 8000af8:	6822      	ldr	r2, [r4, #0]
 8000afa:	07d2      	lsls	r2, r2, #31
 8000afc:	f57f ae73 	bpl.w	80007e6 <_printf_float+0x1ca>
 8000b00:	e747      	b.n	8000992 <_printf_float+0x376>
 8000b02:	2301      	movs	r3, #1
 8000b04:	4a1b      	ldr	r2, [pc, #108]	; (8000b74 <_printf_float+0x558>)
 8000b06:	4631      	mov	r1, r6
 8000b08:	4628      	mov	r0, r5
 8000b0a:	47b8      	blx	r7
 8000b0c:	3001      	adds	r0, #1
 8000b0e:	f43f ae27 	beq.w	8000760 <_printf_float+0x144>
 8000b12:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8000b14:	b92b      	cbnz	r3, 8000b22 <_printf_float+0x506>
 8000b16:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000b18:	b91b      	cbnz	r3, 8000b22 <_printf_float+0x506>
 8000b1a:	6823      	ldr	r3, [r4, #0]
 8000b1c:	07d8      	lsls	r0, r3, #31
 8000b1e:	f57f aebb 	bpl.w	8000898 <_printf_float+0x27c>
 8000b22:	9b07      	ldr	r3, [sp, #28]
 8000b24:	9a06      	ldr	r2, [sp, #24]
 8000b26:	4631      	mov	r1, r6
 8000b28:	4628      	mov	r0, r5
 8000b2a:	47b8      	blx	r7
 8000b2c:	3001      	adds	r0, #1
 8000b2e:	f43f ae17 	beq.w	8000760 <_printf_float+0x144>
 8000b32:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8000b34:	2b00      	cmp	r3, #0
 8000b36:	da1a      	bge.n	8000b6e <_printf_float+0x552>
 8000b38:	f104 091a 	add.w	r9, r4, #26
 8000b3c:	f04f 0800 	mov.w	r8, #0
 8000b40:	e003      	b.n	8000b4a <_printf_float+0x52e>
 8000b42:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8000b44:	425b      	negs	r3, r3
 8000b46:	4543      	cmp	r3, r8
 8000b48:	dd11      	ble.n	8000b6e <_printf_float+0x552>
 8000b4a:	2301      	movs	r3, #1
 8000b4c:	464a      	mov	r2, r9
 8000b4e:	4631      	mov	r1, r6
 8000b50:	4628      	mov	r0, r5
 8000b52:	47b8      	blx	r7
 8000b54:	3001      	adds	r0, #1
 8000b56:	f108 0801 	add.w	r8, r8, #1
 8000b5a:	d1f2      	bne.n	8000b42 <_printf_float+0x526>
 8000b5c:	e600      	b.n	8000760 <_printf_float+0x144>
 8000b5e:	b922      	cbnz	r2, 8000b6a <_printf_float+0x54e>
 8000b60:	6821      	ldr	r1, [r4, #0]
 8000b62:	07c9      	lsls	r1, r1, #31
 8000b64:	d401      	bmi.n	8000b6a <_printf_float+0x54e>
 8000b66:	2201      	movs	r2, #1
 8000b68:	e739      	b.n	80009de <_printf_float+0x3c2>
 8000b6a:	3202      	adds	r2, #2
 8000b6c:	e737      	b.n	80009de <_printf_float+0x3c2>
 8000b6e:	9a04      	ldr	r2, [sp, #16]
 8000b70:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8000b72:	e68a      	b.n	800088a <_printf_float+0x26e>
 8000b74:	0800483c 	.word	0x0800483c

08000b78 <_printf_common>:
 8000b78:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 8000b7c:	460c      	mov	r4, r1
 8000b7e:	4691      	mov	r9, r2
 8000b80:	690a      	ldr	r2, [r1, #16]
 8000b82:	6889      	ldr	r1, [r1, #8]
 8000b84:	f8dd 8020 	ldr.w	r8, [sp, #32]
 8000b88:	428a      	cmp	r2, r1
 8000b8a:	bfb8      	it	lt
 8000b8c:	460a      	movlt	r2, r1
 8000b8e:	f8c9 2000 	str.w	r2, [r9]
 8000b92:	f894 1043 	ldrb.w	r1, [r4, #67]	; 0x43
 8000b96:	4606      	mov	r6, r0
 8000b98:	461f      	mov	r7, r3
 8000b9a:	b111      	cbz	r1, 8000ba2 <_printf_common+0x2a>
 8000b9c:	3201      	adds	r2, #1
 8000b9e:	f8c9 2000 	str.w	r2, [r9]
 8000ba2:	6823      	ldr	r3, [r4, #0]
 8000ba4:	0699      	lsls	r1, r3, #26
 8000ba6:	d55d      	bpl.n	8000c64 <_printf_common+0xec>
 8000ba8:	f8d9 2000 	ldr.w	r2, [r9]
 8000bac:	3202      	adds	r2, #2
 8000bae:	f8c9 2000 	str.w	r2, [r9]
 8000bb2:	6823      	ldr	r3, [r4, #0]
 8000bb4:	f013 0f06 	tst.w	r3, #6
 8000bb8:	4619      	mov	r1, r3
 8000bba:	d11d      	bne.n	8000bf8 <_printf_common+0x80>
 8000bbc:	68e1      	ldr	r1, [r4, #12]
 8000bbe:	1a8a      	subs	r2, r1, r2
 8000bc0:	2a00      	cmp	r2, #0
 8000bc2:	bfd8      	it	le
 8000bc4:	4619      	movle	r1, r3
 8000bc6:	dd17      	ble.n	8000bf8 <_printf_common+0x80>
 8000bc8:	f104 0a19 	add.w	sl, r4, #25
 8000bcc:	2500      	movs	r5, #0
 8000bce:	e005      	b.n	8000bdc <_printf_common+0x64>
 8000bd0:	68e3      	ldr	r3, [r4, #12]
 8000bd2:	f8d9 2000 	ldr.w	r2, [r9]
 8000bd6:	1a9b      	subs	r3, r3, r2
 8000bd8:	42ab      	cmp	r3, r5
 8000bda:	dd0c      	ble.n	8000bf6 <_printf_common+0x7e>
 8000bdc:	2301      	movs	r3, #1
 8000bde:	4652      	mov	r2, sl
 8000be0:	4639      	mov	r1, r7
 8000be2:	4630      	mov	r0, r6
 8000be4:	47c0      	blx	r8
 8000be6:	3001      	adds	r0, #1
 8000be8:	f105 0501 	add.w	r5, r5, #1
 8000bec:	d1f0      	bne.n	8000bd0 <_printf_common+0x58>
 8000bee:	f04f 30ff 	mov.w	r0, #4294967295	; 0xffffffff
 8000bf2:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 8000bf6:	6821      	ldr	r1, [r4, #0]
 8000bf8:	f894 3043 	ldrb.w	r3, [r4, #67]	; 0x43
 8000bfc:	3300      	adds	r3, #0
 8000bfe:	bf18      	it	ne
 8000c00:	2301      	movne	r3, #1
 8000c02:	068a      	lsls	r2, r1, #26
 8000c04:	d50a      	bpl.n	8000c1c <_printf_common+0xa4>
 8000c06:	18e1      	adds	r1, r4, r3
 8000c08:	1c5a      	adds	r2, r3, #1
 8000c0a:	2030      	movs	r0, #48	; 0x30
 8000c0c:	f881 0043 	strb.w	r0, [r1, #67]	; 0x43
 8000c10:	4422      	add	r2, r4
 8000c12:	f894 1045 	ldrb.w	r1, [r4, #69]	; 0x45
 8000c16:	f882 1043 	strb.w	r1, [r2, #67]	; 0x43
 8000c1a:	3302      	adds	r3, #2
 8000c1c:	f104 0243 	add.w	r2, r4, #67	; 0x43
 8000c20:	4639      	mov	r1, r7
 8000c22:	4630      	mov	r0, r6
 8000c24:	47c0      	blx	r8
 8000c26:	3001      	adds	r0, #1
 8000c28:	d0e1      	beq.n	8000bee <_printf_common+0x76>
 8000c2a:	6823      	ldr	r3, [r4, #0]
 8000c2c:	f003 0306 	and.w	r3, r3, #6
 8000c30:	2b04      	cmp	r3, #4
 8000c32:	d029      	beq.n	8000c88 <_printf_common+0x110>
 8000c34:	68a3      	ldr	r3, [r4, #8]
 8000c36:	6922      	ldr	r2, [r4, #16]
 8000c38:	4293      	cmp	r3, r2
 8000c3a:	bfc8      	it	gt
 8000c3c:	f04f 0900 	movgt.w	r9, #0
 8000c40:	dd30      	ble.n	8000ca4 <_printf_common+0x12c>
 8000c42:	1a9b      	subs	r3, r3, r2
 8000c44:	4499      	add	r9, r3
 8000c46:	341a      	adds	r4, #26
 8000c48:	2500      	movs	r5, #0
 8000c4a:	e001      	b.n	8000c50 <_printf_common+0xd8>
 8000c4c:	454d      	cmp	r5, r9
 8000c4e:	d029      	beq.n	8000ca4 <_printf_common+0x12c>
 8000c50:	2301      	movs	r3, #1
 8000c52:	4622      	mov	r2, r4
 8000c54:	4639      	mov	r1, r7
 8000c56:	4630      	mov	r0, r6
 8000c58:	47c0      	blx	r8
 8000c5a:	3001      	adds	r0, #1
 8000c5c:	f105 0501 	add.w	r5, r5, #1
 8000c60:	d1f4      	bne.n	8000c4c <_printf_common+0xd4>
 8000c62:	e7c4      	b.n	8000bee <_printf_common+0x76>
 8000c64:	f013 0f06 	tst.w	r3, #6
 8000c68:	d108      	bne.n	8000c7c <_printf_common+0x104>
 8000c6a:	68e1      	ldr	r1, [r4, #12]
 8000c6c:	f8d9 2000 	ldr.w	r2, [r9]
 8000c70:	1a8a      	subs	r2, r1, r2
 8000c72:	2a00      	cmp	r2, #0
 8000c74:	bfd8      	it	le
 8000c76:	4619      	movle	r1, r3
 8000c78:	dca6      	bgt.n	8000bc8 <_printf_common+0x50>
 8000c7a:	e7bd      	b.n	8000bf8 <_printf_common+0x80>
 8000c7c:	f894 3043 	ldrb.w	r3, [r4, #67]	; 0x43
 8000c80:	3300      	adds	r3, #0
 8000c82:	bf18      	it	ne
 8000c84:	2301      	movne	r3, #1
 8000c86:	e7c9      	b.n	8000c1c <_printf_common+0xa4>
 8000c88:	f8d9 1000 	ldr.w	r1, [r9]
 8000c8c:	68e0      	ldr	r0, [r4, #12]
 8000c8e:	68a3      	ldr	r3, [r4, #8]
 8000c90:	6922      	ldr	r2, [r4, #16]
 8000c92:	eba0 0901 	sub.w	r9, r0, r1
 8000c96:	4293      	cmp	r3, r2
 8000c98:	ea29 79e9 	bic.w	r9, r9, r9, asr #31
 8000c9c:	dcd1      	bgt.n	8000c42 <_printf_common+0xca>
 8000c9e:	f1b9 0f00 	cmp.w	r9, #0
 8000ca2:	d1d0      	bne.n	8000c46 <_printf_common+0xce>
 8000ca4:	2000      	movs	r0, #0
 8000ca6:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 8000caa:	bf00      	nop

08000cac <_printf_i>:
 8000cac:	e92d 43f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, lr}
 8000cb0:	460c      	mov	r4, r1
 8000cb2:	7e09      	ldrb	r1, [r1, #24]
 8000cb4:	b085      	sub	sp, #20
 8000cb6:	296e      	cmp	r1, #110	; 0x6e
 8000cb8:	4606      	mov	r6, r0
 8000cba:	4617      	mov	r7, r2
 8000cbc:	980c      	ldr	r0, [sp, #48]	; 0x30
 8000cbe:	4698      	mov	r8, r3
 8000cc0:	f104 0c43 	add.w	ip, r4, #67	; 0x43
 8000cc4:	d055      	beq.n	8000d72 <_printf_i+0xc6>
 8000cc6:	d80f      	bhi.n	8000ce8 <_printf_i+0x3c>
 8000cc8:	2963      	cmp	r1, #99	; 0x63
 8000cca:	f000 811b 	beq.w	8000f04 <_printf_i+0x258>
 8000cce:	f200 80d6 	bhi.w	8000e7e <_printf_i+0x1d2>
 8000cd2:	2900      	cmp	r1, #0
 8000cd4:	d05c      	beq.n	8000d90 <_printf_i+0xe4>
 8000cd6:	2958      	cmp	r1, #88	; 0x58
 8000cd8:	f040 80e8 	bne.w	8000eac <_printf_i+0x200>
 8000cdc:	f884 1045 	strb.w	r1, [r4, #69]	; 0x45
 8000ce0:	6822      	ldr	r2, [r4, #0]
 8000ce2:	f8df e260 	ldr.w	lr, [pc, #608]	; 8000f44 <_printf_i+0x298>
 8000ce6:	e013      	b.n	8000d10 <_printf_i+0x64>
 8000ce8:	2973      	cmp	r1, #115	; 0x73
 8000cea:	f000 80fd 	beq.w	8000ee8 <_printf_i+0x23c>
 8000cee:	f200 8089 	bhi.w	8000e04 <_printf_i+0x158>
 8000cf2:	296f      	cmp	r1, #111	; 0x6f
 8000cf4:	f000 80e4 	beq.w	8000ec0 <_printf_i+0x214>
 8000cf8:	2970      	cmp	r1, #112	; 0x70
 8000cfa:	f040 80d7 	bne.w	8000eac <_printf_i+0x200>
 8000cfe:	6822      	ldr	r2, [r4, #0]
 8000d00:	f042 0220 	orr.w	r2, r2, #32
 8000d04:	6022      	str	r2, [r4, #0]
 8000d06:	2378      	movs	r3, #120	; 0x78
 8000d08:	f8df e23c 	ldr.w	lr, [pc, #572]	; 8000f48 <_printf_i+0x29c>
 8000d0c:	f884 3045 	strb.w	r3, [r4, #69]	; 0x45
 8000d10:	6803      	ldr	r3, [r0, #0]
 8000d12:	0615      	lsls	r5, r2, #24
 8000d14:	f103 0104 	add.w	r1, r3, #4
 8000d18:	f100 80ff 	bmi.w	8000f1a <_printf_i+0x26e>
 8000d1c:	0655      	lsls	r5, r2, #25
 8000d1e:	bf4b      	itete	mi
 8000d20:	881b      	ldrhmi	r3, [r3, #0]
 8000d22:	681b      	ldrpl	r3, [r3, #0]
 8000d24:	6001      	strmi	r1, [r0, #0]
 8000d26:	6001      	strpl	r1, [r0, #0]
 8000d28:	07d0      	lsls	r0, r2, #31
 8000d2a:	bf44      	itt	mi
 8000d2c:	f042 0220 	orrmi.w	r2, r2, #32
 8000d30:	6022      	strmi	r2, [r4, #0]
 8000d32:	2b00      	cmp	r3, #0
 8000d34:	f040 8095 	bne.w	8000e62 <_printf_i+0x1b6>
 8000d38:	6822      	ldr	r2, [r4, #0]
 8000d3a:	f022 0220 	bic.w	r2, r2, #32
 8000d3e:	6022      	str	r2, [r4, #0]
 8000d40:	2110      	movs	r1, #16
 8000d42:	2200      	movs	r2, #0
 8000d44:	f884 2043 	strb.w	r2, [r4, #67]	; 0x43
 8000d48:	6860      	ldr	r0, [r4, #4]
 8000d4a:	60a0      	str	r0, [r4, #8]
 8000d4c:	2800      	cmp	r0, #0
 8000d4e:	f2c0 808d 	blt.w	8000e6c <_printf_i+0x1c0>
 8000d52:	6822      	ldr	r2, [r4, #0]
 8000d54:	f022 0204 	bic.w	r2, r2, #4
 8000d58:	6022      	str	r2, [r4, #0]
 8000d5a:	2b00      	cmp	r3, #0
 8000d5c:	d167      	bne.n	8000e2e <_printf_i+0x182>
 8000d5e:	2800      	cmp	r0, #0
 8000d60:	f040 8086 	bne.w	8000e70 <_printf_i+0x1c4>
 8000d64:	4665      	mov	r5, ip
 8000d66:	2908      	cmp	r1, #8
 8000d68:	d06f      	beq.n	8000e4a <_printf_i+0x19e>
 8000d6a:	ebac 0305 	sub.w	r3, ip, r5
 8000d6e:	6123      	str	r3, [r4, #16]
 8000d70:	e011      	b.n	8000d96 <_printf_i+0xea>
 8000d72:	6823      	ldr	r3, [r4, #0]
 8000d74:	061a      	lsls	r2, r3, #24
 8000d76:	f100 80d3 	bmi.w	8000f20 <_printf_i+0x274>
 8000d7a:	f013 0f40 	tst.w	r3, #64	; 0x40
 8000d7e:	6803      	ldr	r3, [r0, #0]
 8000d80:	6962      	ldr	r2, [r4, #20]
 8000d82:	f103 0104 	add.w	r1, r3, #4
 8000d86:	6001      	str	r1, [r0, #0]
 8000d88:	681b      	ldr	r3, [r3, #0]
 8000d8a:	bf14      	ite	ne
 8000d8c:	801a      	strhne	r2, [r3, #0]
 8000d8e:	601a      	streq	r2, [r3, #0]
 8000d90:	2300      	movs	r3, #0
 8000d92:	4665      	mov	r5, ip
 8000d94:	6123      	str	r3, [r4, #16]
 8000d96:	f8cd 8000 	str.w	r8, [sp]
 8000d9a:	463b      	mov	r3, r7
 8000d9c:	aa03      	add	r2, sp, #12
 8000d9e:	4621      	mov	r1, r4
 8000da0:	4630      	mov	r0, r6
 8000da2:	f7ff fee9 	bl	8000b78 <_printf_common>
 8000da6:	3001      	adds	r0, #1
 8000da8:	d021      	beq.n	8000dee <_printf_i+0x142>
 8000daa:	462a      	mov	r2, r5
 8000dac:	6923      	ldr	r3, [r4, #16]
 8000dae:	4639      	mov	r1, r7
 8000db0:	4630      	mov	r0, r6
 8000db2:	47c0      	blx	r8
 8000db4:	3001      	adds	r0, #1
 8000db6:	d01a      	beq.n	8000dee <_printf_i+0x142>
 8000db8:	6823      	ldr	r3, [r4, #0]
 8000dba:	68e0      	ldr	r0, [r4, #12]
 8000dbc:	f013 0f02 	tst.w	r3, #2
 8000dc0:	9b03      	ldr	r3, [sp, #12]
 8000dc2:	d019      	beq.n	8000df8 <_printf_i+0x14c>
 8000dc4:	1ac2      	subs	r2, r0, r3
 8000dc6:	2a00      	cmp	r2, #0
 8000dc8:	dd16      	ble.n	8000df8 <_printf_i+0x14c>
 8000dca:	f104 0919 	add.w	r9, r4, #25
 8000dce:	2500      	movs	r5, #0
 8000dd0:	e004      	b.n	8000ddc <_printf_i+0x130>
 8000dd2:	68e0      	ldr	r0, [r4, #12]
 8000dd4:	9b03      	ldr	r3, [sp, #12]
 8000dd6:	1ac2      	subs	r2, r0, r3
 8000dd8:	42aa      	cmp	r2, r5
 8000dda:	dd0d      	ble.n	8000df8 <_printf_i+0x14c>
 8000ddc:	2301      	movs	r3, #1
 8000dde:	464a      	mov	r2, r9
 8000de0:	4639      	mov	r1, r7
 8000de2:	4630      	mov	r0, r6
 8000de4:	47c0      	blx	r8
 8000de6:	3001      	adds	r0, #1
 8000de8:	f105 0501 	add.w	r5, r5, #1
 8000dec:	d1f1      	bne.n	8000dd2 <_printf_i+0x126>
 8000dee:	f04f 30ff 	mov.w	r0, #4294967295	; 0xffffffff
 8000df2:	b005      	add	sp, #20
 8000df4:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
 8000df8:	4298      	cmp	r0, r3
 8000dfa:	bfb8      	it	lt
 8000dfc:	4618      	movlt	r0, r3
 8000dfe:	b005      	add	sp, #20
 8000e00:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
 8000e04:	2975      	cmp	r1, #117	; 0x75
 8000e06:	d05b      	beq.n	8000ec0 <_printf_i+0x214>
 8000e08:	2978      	cmp	r1, #120	; 0x78
 8000e0a:	d14f      	bne.n	8000eac <_printf_i+0x200>
 8000e0c:	6822      	ldr	r2, [r4, #0]
 8000e0e:	e77a      	b.n	8000d06 <_printf_i+0x5a>
 8000e10:	6861      	ldr	r1, [r4, #4]
 8000e12:	60a1      	str	r1, [r4, #8]
 8000e14:	202d      	movs	r0, #45	; 0x2d
 8000e16:	2900      	cmp	r1, #0
 8000e18:	f1c3 0300 	rsb	r3, r3, #0
 8000e1c:	f884 0043 	strb.w	r0, [r4, #67]	; 0x43
 8000e20:	db21      	blt.n	8000e66 <_printf_i+0x1ba>
 8000e22:	f022 0204 	bic.w	r2, r2, #4
 8000e26:	f8df e11c 	ldr.w	lr, [pc, #284]	; 8000f44 <_printf_i+0x298>
 8000e2a:	6022      	str	r2, [r4, #0]
 8000e2c:	210a      	movs	r1, #10
 8000e2e:	4665      	mov	r5, ip
 8000e30:	fbb3 f2f1 	udiv	r2, r3, r1
 8000e34:	fb01 3012 	mls	r0, r1, r2, r3
 8000e38:	4613      	mov	r3, r2
 8000e3a:	f81e 2000 	ldrb.w	r2, [lr, r0]
 8000e3e:	f805 2d01 	strb.w	r2, [r5, #-1]!
 8000e42:	2b00      	cmp	r3, #0
 8000e44:	d1f4      	bne.n	8000e30 <_printf_i+0x184>
 8000e46:	2908      	cmp	r1, #8
 8000e48:	d18f      	bne.n	8000d6a <_printf_i+0xbe>
 8000e4a:	6823      	ldr	r3, [r4, #0]
 8000e4c:	07d9      	lsls	r1, r3, #31
 8000e4e:	d58c      	bpl.n	8000d6a <_printf_i+0xbe>
 8000e50:	6862      	ldr	r2, [r4, #4]
 8000e52:	6923      	ldr	r3, [r4, #16]
 8000e54:	429a      	cmp	r2, r3
 8000e56:	dc88      	bgt.n	8000d6a <_printf_i+0xbe>
 8000e58:	2330      	movs	r3, #48	; 0x30
 8000e5a:	f805 3c01 	strb.w	r3, [r5, #-1]
 8000e5e:	3d01      	subs	r5, #1
 8000e60:	e783      	b.n	8000d6a <_printf_i+0xbe>
 8000e62:	2110      	movs	r1, #16
 8000e64:	e76d      	b.n	8000d42 <_printf_i+0x96>
 8000e66:	f8df e0dc 	ldr.w	lr, [pc, #220]	; 8000f44 <_printf_i+0x298>
 8000e6a:	210a      	movs	r1, #10
 8000e6c:	2b00      	cmp	r3, #0
 8000e6e:	d1de      	bne.n	8000e2e <_printf_i+0x182>
 8000e70:	f89e 3000 	ldrb.w	r3, [lr]
 8000e74:	f884 3042 	strb.w	r3, [r4, #66]	; 0x42
 8000e78:	f104 0542 	add.w	r5, r4, #66	; 0x42
 8000e7c:	e773      	b.n	8000d66 <_printf_i+0xba>
 8000e7e:	2964      	cmp	r1, #100	; 0x64
 8000e80:	d001      	beq.n	8000e86 <_printf_i+0x1da>
 8000e82:	2969      	cmp	r1, #105	; 0x69
 8000e84:	d112      	bne.n	8000eac <_printf_i+0x200>
 8000e86:	6822      	ldr	r2, [r4, #0]
 8000e88:	6803      	ldr	r3, [r0, #0]
 8000e8a:	0615      	lsls	r5, r2, #24
 8000e8c:	f103 0104 	add.w	r1, r3, #4
 8000e90:	d452      	bmi.n	8000f38 <_printf_i+0x28c>
 8000e92:	0655      	lsls	r5, r2, #25
 8000e94:	bf4b      	itete	mi
 8000e96:	f9b3 3000 	ldrshmi.w	r3, [r3]
 8000e9a:	681b      	ldrpl	r3, [r3, #0]
 8000e9c:	6001      	strmi	r1, [r0, #0]
 8000e9e:	6001      	strpl	r1, [r0, #0]
 8000ea0:	2b00      	cmp	r3, #0
 8000ea2:	dbb5      	blt.n	8000e10 <_printf_i+0x164>
 8000ea4:	f8df e09c 	ldr.w	lr, [pc, #156]	; 8000f44 <_printf_i+0x298>
 8000ea8:	210a      	movs	r1, #10
 8000eaa:	e74d      	b.n	8000d48 <_printf_i+0x9c>
 8000eac:	2301      	movs	r3, #1
 8000eae:	f884 1042 	strb.w	r1, [r4, #66]	; 0x42
 8000eb2:	6123      	str	r3, [r4, #16]
 8000eb4:	f104 0542 	add.w	r5, r4, #66	; 0x42
 8000eb8:	2300      	movs	r3, #0
 8000eba:	f884 3043 	strb.w	r3, [r4, #67]	; 0x43
 8000ebe:	e76a      	b.n	8000d96 <_printf_i+0xea>
 8000ec0:	6823      	ldr	r3, [r4, #0]
 8000ec2:	061a      	lsls	r2, r3, #24
 8000ec4:	d433      	bmi.n	8000f2e <_printf_i+0x282>
 8000ec6:	f013 0f40 	tst.w	r3, #64	; 0x40
 8000eca:	6803      	ldr	r3, [r0, #0]
 8000ecc:	f103 0204 	add.w	r2, r3, #4
 8000ed0:	bf15      	itete	ne
 8000ed2:	881b      	ldrhne	r3, [r3, #0]
 8000ed4:	681b      	ldreq	r3, [r3, #0]
 8000ed6:	6002      	strne	r2, [r0, #0]
 8000ed8:	6002      	streq	r2, [r0, #0]
 8000eda:	296f      	cmp	r1, #111	; 0x6f
 8000edc:	bf14      	ite	ne
 8000ede:	210a      	movne	r1, #10
 8000ee0:	2108      	moveq	r1, #8
 8000ee2:	f8df e060 	ldr.w	lr, [pc, #96]	; 8000f44 <_printf_i+0x298>
 8000ee6:	e72c      	b.n	8000d42 <_printf_i+0x96>
 8000ee8:	6803      	ldr	r3, [r0, #0]
 8000eea:	6862      	ldr	r2, [r4, #4]
 8000eec:	1d19      	adds	r1, r3, #4
 8000eee:	6001      	str	r1, [r0, #0]
 8000ef0:	681d      	ldr	r5, [r3, #0]
 8000ef2:	2100      	movs	r1, #0
 8000ef4:	4628      	mov	r0, r5
 8000ef6:	f001 f923 	bl	8002140 <memchr>
 8000efa:	b300      	cbz	r0, 8000f3e <_printf_i+0x292>
 8000efc:	1b40      	subs	r0, r0, r5
 8000efe:	6060      	str	r0, [r4, #4]
 8000f00:	6120      	str	r0, [r4, #16]
 8000f02:	e7d9      	b.n	8000eb8 <_printf_i+0x20c>
 8000f04:	6803      	ldr	r3, [r0, #0]
 8000f06:	681a      	ldr	r2, [r3, #0]
 8000f08:	1d19      	adds	r1, r3, #4
 8000f0a:	2301      	movs	r3, #1
 8000f0c:	6001      	str	r1, [r0, #0]
 8000f0e:	f104 0542 	add.w	r5, r4, #66	; 0x42
 8000f12:	f884 2042 	strb.w	r2, [r4, #66]	; 0x42
 8000f16:	6123      	str	r3, [r4, #16]
 8000f18:	e7ce      	b.n	8000eb8 <_printf_i+0x20c>
 8000f1a:	6001      	str	r1, [r0, #0]
 8000f1c:	681b      	ldr	r3, [r3, #0]
 8000f1e:	e703      	b.n	8000d28 <_printf_i+0x7c>
 8000f20:	6803      	ldr	r3, [r0, #0]
 8000f22:	6962      	ldr	r2, [r4, #20]
 8000f24:	1d19      	adds	r1, r3, #4
 8000f26:	6001      	str	r1, [r0, #0]
 8000f28:	681b      	ldr	r3, [r3, #0]
 8000f2a:	601a      	str	r2, [r3, #0]
 8000f2c:	e730      	b.n	8000d90 <_printf_i+0xe4>
 8000f2e:	6803      	ldr	r3, [r0, #0]
 8000f30:	1d1a      	adds	r2, r3, #4
 8000f32:	6002      	str	r2, [r0, #0]
 8000f34:	681b      	ldr	r3, [r3, #0]
 8000f36:	e7d0      	b.n	8000eda <_printf_i+0x22e>
 8000f38:	6001      	str	r1, [r0, #0]
 8000f3a:	681b      	ldr	r3, [r3, #0]
 8000f3c:	e7b0      	b.n	8000ea0 <_printf_i+0x1f4>
 8000f3e:	6860      	ldr	r0, [r4, #4]
 8000f40:	e7de      	b.n	8000f00 <_printf_i+0x254>
 8000f42:	bf00      	nop
 8000f44:	08004840 	.word	0x08004840
 8000f48:	08004854 	.word	0x08004854
	...

08000f80 <strlen>:
 8000f80:	f890 f000 	pld	[r0]
 8000f84:	e96d 4502 	strd	r4, r5, [sp, #-8]!
 8000f88:	f020 0107 	bic.w	r1, r0, #7
 8000f8c:	f06f 0c00 	mvn.w	ip, #0
 8000f90:	f010 0407 	ands.w	r4, r0, #7
 8000f94:	f891 f020 	pld	[r1, #32]
 8000f98:	f040 8049 	bne.w	800102e <strlen+0xae>
 8000f9c:	f04f 0400 	mov.w	r4, #0
 8000fa0:	f06f 0007 	mvn.w	r0, #7
 8000fa4:	e9d1 2300 	ldrd	r2, r3, [r1]
 8000fa8:	f891 f040 	pld	[r1, #64]	; 0x40
 8000fac:	f100 0008 	add.w	r0, r0, #8
 8000fb0:	fa82 f24c 	uadd8	r2, r2, ip
 8000fb4:	faa4 f28c 	sel	r2, r4, ip
 8000fb8:	fa83 f34c 	uadd8	r3, r3, ip
 8000fbc:	faa2 f38c 	sel	r3, r2, ip
 8000fc0:	bb4b      	cbnz	r3, 8001016 <strlen+0x96>
 8000fc2:	e9d1 2302 	ldrd	r2, r3, [r1, #8]
 8000fc6:	fa82 f24c 	uadd8	r2, r2, ip
 8000fca:	f100 0008 	add.w	r0, r0, #8
 8000fce:	faa4 f28c 	sel	r2, r4, ip
 8000fd2:	fa83 f34c 	uadd8	r3, r3, ip
 8000fd6:	faa2 f38c 	sel	r3, r2, ip
 8000fda:	b9e3      	cbnz	r3, 8001016 <strlen+0x96>
 8000fdc:	e9d1 2304 	ldrd	r2, r3, [r1, #16]
 8000fe0:	fa82 f24c 	uadd8	r2, r2, ip
 8000fe4:	f100 0008 	add.w	r0, r0, #8
 8000fe8:	faa4 f28c 	sel	r2, r4, ip
 8000fec:	fa83 f34c 	uadd8	r3, r3, ip
 8000ff0:	faa2 f38c 	sel	r3, r2, ip
 8000ff4:	b97b      	cbnz	r3, 8001016 <strlen+0x96>
 8000ff6:	e9d1 2306 	ldrd	r2, r3, [r1, #24]
 8000ffa:	f101 0120 	add.w	r1, r1, #32
 8000ffe:	fa82 f24c 	uadd8	r2, r2, ip
 8001002:	f100 0008 	add.w	r0, r0, #8
 8001006:	faa4 f28c 	sel	r2, r4, ip
 800100a:	fa83 f34c 	uadd8	r3, r3, ip
 800100e:	faa2 f38c 	sel	r3, r2, ip
 8001012:	2b00      	cmp	r3, #0
 8001014:	d0c6      	beq.n	8000fa4 <strlen+0x24>
 8001016:	2a00      	cmp	r2, #0
 8001018:	bf04      	itt	eq
 800101a:	3004      	addeq	r0, #4
 800101c:	461a      	moveq	r2, r3
 800101e:	ba12      	rev	r2, r2
 8001020:	fab2 f282 	clz	r2, r2
 8001024:	e8fd 4502 	ldrd	r4, r5, [sp], #8
 8001028:	eb00 00d2 	add.w	r0, r0, r2, lsr #3
 800102c:	4770      	bx	lr
 800102e:	e9d1 2300 	ldrd	r2, r3, [r1]
 8001032:	f004 0503 	and.w	r5, r4, #3
 8001036:	f1c4 0000 	rsb	r0, r4, #0
 800103a:	ea4f 05c5 	mov.w	r5, r5, lsl #3
 800103e:	f014 0f04 	tst.w	r4, #4
 8001042:	f891 f040 	pld	[r1, #64]	; 0x40
 8001046:	fa0c f505 	lsl.w	r5, ip, r5
 800104a:	ea62 0205 	orn	r2, r2, r5
 800104e:	bf1c      	itt	ne
 8001050:	ea63 0305 	ornne	r3, r3, r5
 8001054:	4662      	movne	r2, ip
 8001056:	f04f 0400 	mov.w	r4, #0
 800105a:	e7a9      	b.n	8000fb0 <strlen+0x30>
 800105c:	0000      	movs	r0, r0
	...

08001060 <quorem>:
 8001060:	6902      	ldr	r2, [r0, #16]
 8001062:	690b      	ldr	r3, [r1, #16]
 8001064:	4293      	cmp	r3, r2
 8001066:	f300 808d 	bgt.w	8001184 <quorem+0x124>
 800106a:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800106e:	f103 38ff 	add.w	r8, r3, #4294967295	; 0xffffffff
 8001072:	f101 0714 	add.w	r7, r1, #20
 8001076:	f100 0b14 	add.w	fp, r0, #20
 800107a:	f857 2028 	ldr.w	r2, [r7, r8, lsl #2]
 800107e:	f85b 3028 	ldr.w	r3, [fp, r8, lsl #2]
 8001082:	ea4f 0488 	mov.w	r4, r8, lsl #2
 8001086:	b083      	sub	sp, #12
 8001088:	3201      	adds	r2, #1
 800108a:	fbb3 f9f2 	udiv	r9, r3, r2
 800108e:	eb0b 0304 	add.w	r3, fp, r4
 8001092:	9400      	str	r4, [sp, #0]
 8001094:	eb07 0a04 	add.w	sl, r7, r4
 8001098:	9301      	str	r3, [sp, #4]
 800109a:	f1b9 0f00 	cmp.w	r9, #0
 800109e:	d039      	beq.n	8001114 <quorem+0xb4>
 80010a0:	2500      	movs	r5, #0
 80010a2:	462e      	mov	r6, r5
 80010a4:	46bc      	mov	ip, r7
 80010a6:	46de      	mov	lr, fp
 80010a8:	f85c 4b04 	ldr.w	r4, [ip], #4
 80010ac:	f8de 3000 	ldr.w	r3, [lr]
 80010b0:	b2a2      	uxth	r2, r4
 80010b2:	fb09 5502 	mla	r5, r9, r2, r5
 80010b6:	0c22      	lsrs	r2, r4, #16
 80010b8:	0c2c      	lsrs	r4, r5, #16
 80010ba:	fb09 4202 	mla	r2, r9, r2, r4
 80010be:	b2ad      	uxth	r5, r5
 80010c0:	1b75      	subs	r5, r6, r5
 80010c2:	b296      	uxth	r6, r2
 80010c4:	ebc6 4613 	rsb	r6, r6, r3, lsr #16
 80010c8:	fa15 f383 	uxtah	r3, r5, r3
 80010cc:	eb06 4623 	add.w	r6, r6, r3, asr #16
 80010d0:	b29b      	uxth	r3, r3
 80010d2:	ea43 4306 	orr.w	r3, r3, r6, lsl #16
 80010d6:	45e2      	cmp	sl, ip
 80010d8:	ea4f 4512 	mov.w	r5, r2, lsr #16
 80010dc:	f84e 3b04 	str.w	r3, [lr], #4
 80010e0:	ea4f 4626 	mov.w	r6, r6, asr #16
 80010e4:	d2e0      	bcs.n	80010a8 <quorem+0x48>
 80010e6:	9b00      	ldr	r3, [sp, #0]
 80010e8:	f85b 3003 	ldr.w	r3, [fp, r3]
 80010ec:	b993      	cbnz	r3, 8001114 <quorem+0xb4>
 80010ee:	9c01      	ldr	r4, [sp, #4]
 80010f0:	1f23      	subs	r3, r4, #4
 80010f2:	459b      	cmp	fp, r3
 80010f4:	d20c      	bcs.n	8001110 <quorem+0xb0>
 80010f6:	f854 3c04 	ldr.w	r3, [r4, #-4]
 80010fa:	b94b      	cbnz	r3, 8001110 <quorem+0xb0>
 80010fc:	f1a4 0308 	sub.w	r3, r4, #8
 8001100:	e002      	b.n	8001108 <quorem+0xa8>
 8001102:	681a      	ldr	r2, [r3, #0]
 8001104:	3b04      	subs	r3, #4
 8001106:	b91a      	cbnz	r2, 8001110 <quorem+0xb0>
 8001108:	459b      	cmp	fp, r3
 800110a:	f108 38ff 	add.w	r8, r8, #4294967295	; 0xffffffff
 800110e:	d3f8      	bcc.n	8001102 <quorem+0xa2>
 8001110:	f8c0 8010 	str.w	r8, [r0, #16]
 8001114:	4604      	mov	r4, r0
 8001116:	f001 fadb 	bl	80026d0 <__mcmp>
 800111a:	2800      	cmp	r0, #0
 800111c:	db2e      	blt.n	800117c <quorem+0x11c>
 800111e:	f109 0901 	add.w	r9, r9, #1
 8001122:	465d      	mov	r5, fp
 8001124:	2300      	movs	r3, #0
 8001126:	f857 1b04 	ldr.w	r1, [r7], #4
 800112a:	6828      	ldr	r0, [r5, #0]
 800112c:	b28a      	uxth	r2, r1
 800112e:	1a9a      	subs	r2, r3, r2
 8001130:	0c0b      	lsrs	r3, r1, #16
 8001132:	fa12 f280 	uxtah	r2, r2, r0
 8001136:	ebc3 4310 	rsb	r3, r3, r0, lsr #16
 800113a:	eb03 4322 	add.w	r3, r3, r2, asr #16
 800113e:	b292      	uxth	r2, r2
 8001140:	ea42 4203 	orr.w	r2, r2, r3, lsl #16
 8001144:	45ba      	cmp	sl, r7
 8001146:	f845 2b04 	str.w	r2, [r5], #4
 800114a:	ea4f 4323 	mov.w	r3, r3, asr #16
 800114e:	d2ea      	bcs.n	8001126 <quorem+0xc6>
 8001150:	f85b 2028 	ldr.w	r2, [fp, r8, lsl #2]
 8001154:	eb0b 0388 	add.w	r3, fp, r8, lsl #2
 8001158:	b982      	cbnz	r2, 800117c <quorem+0x11c>
 800115a:	1f1a      	subs	r2, r3, #4
 800115c:	4593      	cmp	fp, r2
 800115e:	d20b      	bcs.n	8001178 <quorem+0x118>
 8001160:	f853 2c04 	ldr.w	r2, [r3, #-4]
 8001164:	b942      	cbnz	r2, 8001178 <quorem+0x118>
 8001166:	3b08      	subs	r3, #8
 8001168:	e002      	b.n	8001170 <quorem+0x110>
 800116a:	681a      	ldr	r2, [r3, #0]
 800116c:	3b04      	subs	r3, #4
 800116e:	b91a      	cbnz	r2, 8001178 <quorem+0x118>
 8001170:	459b      	cmp	fp, r3
 8001172:	f108 38ff 	add.w	r8, r8, #4294967295	; 0xffffffff
 8001176:	d3f8      	bcc.n	800116a <quorem+0x10a>
 8001178:	f8c4 8010 	str.w	r8, [r4, #16]
 800117c:	4648      	mov	r0, r9
 800117e:	b003      	add	sp, #12
 8001180:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8001184:	2000      	movs	r0, #0
 8001186:	4770      	bx	lr

08001188 <_dtoa_r>:
 8001188:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 800118c:	ec57 6b10 	vmov	r6, r7, d0
 8001190:	b099      	sub	sp, #100	; 0x64
 8001192:	6a45      	ldr	r5, [r0, #36]	; 0x24
 8001194:	9102      	str	r1, [sp, #8]
 8001196:	4604      	mov	r4, r0
 8001198:	4693      	mov	fp, r2
 800119a:	9304      	str	r3, [sp, #16]
 800119c:	e9cd 6700 	strd	r6, r7, [sp]
 80011a0:	2d00      	cmp	r5, #0
 80011a2:	f000 8203 	beq.w	80015ac <_dtoa_r+0x424>
 80011a6:	6829      	ldr	r1, [r5, #0]
 80011a8:	b149      	cbz	r1, 80011be <_dtoa_r+0x36>
 80011aa:	686a      	ldr	r2, [r5, #4]
 80011ac:	604a      	str	r2, [r1, #4]
 80011ae:	2301      	movs	r3, #1
 80011b0:	4093      	lsls	r3, r2
 80011b2:	608b      	str	r3, [r1, #8]
 80011b4:	f001 f848 	bl	8002248 <_Bfree>
 80011b8:	6a63      	ldr	r3, [r4, #36]	; 0x24
 80011ba:	2200      	movs	r2, #0
 80011bc:	601a      	str	r2, [r3, #0]
 80011be:	e9dd 2300 	ldrd	r2, r3, [sp]
 80011c2:	2b00      	cmp	r3, #0
 80011c4:	4699      	mov	r9, r3
 80011c6:	db3f      	blt.n	8001248 <_dtoa_r+0xc0>
 80011c8:	9a22      	ldr	r2, [sp, #136]	; 0x88
 80011ca:	2300      	movs	r3, #0
 80011cc:	6013      	str	r3, [r2, #0]
 80011ce:	4ba8      	ldr	r3, [pc, #672]	; (8001470 <_dtoa_r+0x2e8>)
 80011d0:	ea33 0309 	bics.w	r3, r3, r9
 80011d4:	d01a      	beq.n	800120c <_dtoa_r+0x84>
 80011d6:	e9dd 6700 	ldrd	r6, r7, [sp]
 80011da:	2200      	movs	r2, #0
 80011dc:	2300      	movs	r3, #0
 80011de:	4630      	mov	r0, r6
 80011e0:	4639      	mov	r1, r7
 80011e2:	f002 fa4b 	bl	800367c <__aeabi_dcmpeq>
 80011e6:	4680      	mov	r8, r0
 80011e8:	2800      	cmp	r0, #0
 80011ea:	d036      	beq.n	800125a <_dtoa_r+0xd2>
 80011ec:	9a04      	ldr	r2, [sp, #16]
 80011ee:	2301      	movs	r3, #1
 80011f0:	6013      	str	r3, [r2, #0]
 80011f2:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 80011f4:	2b00      	cmp	r3, #0
 80011f6:	f000 80cb 	beq.w	8001390 <_dtoa_r+0x208>
 80011fa:	4b9e      	ldr	r3, [pc, #632]	; (8001474 <_dtoa_r+0x2ec>)
 80011fc:	9a23      	ldr	r2, [sp, #140]	; 0x8c
 80011fe:	f103 39ff 	add.w	r9, r3, #4294967295	; 0xffffffff
 8001202:	6013      	str	r3, [r2, #0]
 8001204:	4648      	mov	r0, r9
 8001206:	b019      	add	sp, #100	; 0x64
 8001208:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800120c:	9a04      	ldr	r2, [sp, #16]
 800120e:	f242 730f 	movw	r3, #9999	; 0x270f
 8001212:	6013      	str	r3, [r2, #0]
 8001214:	9b00      	ldr	r3, [sp, #0]
 8001216:	b97b      	cbnz	r3, 8001238 <_dtoa_r+0xb0>
 8001218:	f3c9 0313 	ubfx	r3, r9, #0, #20
 800121c:	b963      	cbnz	r3, 8001238 <_dtoa_r+0xb0>
 800121e:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 8001220:	f8df 9260 	ldr.w	r9, [pc, #608]	; 8001484 <_dtoa_r+0x2fc>
 8001224:	2b00      	cmp	r3, #0
 8001226:	d0ed      	beq.n	8001204 <_dtoa_r+0x7c>
 8001228:	f109 0308 	add.w	r3, r9, #8
 800122c:	9a23      	ldr	r2, [sp, #140]	; 0x8c
 800122e:	4648      	mov	r0, r9
 8001230:	6013      	str	r3, [r2, #0]
 8001232:	b019      	add	sp, #100	; 0x64
 8001234:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8001238:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 800123a:	f8df 924c 	ldr.w	r9, [pc, #588]	; 8001488 <_dtoa_r+0x300>
 800123e:	2b00      	cmp	r3, #0
 8001240:	d0e0      	beq.n	8001204 <_dtoa_r+0x7c>
 8001242:	f109 0303 	add.w	r3, r9, #3
 8001246:	e7f1      	b.n	800122c <_dtoa_r+0xa4>
 8001248:	9a01      	ldr	r2, [sp, #4]
 800124a:	f022 4900 	bic.w	r9, r2, #2147483648	; 0x80000000
 800124e:	9a22      	ldr	r2, [sp, #136]	; 0x88
 8001250:	f8cd 9004 	str.w	r9, [sp, #4]
 8001254:	2301      	movs	r3, #1
 8001256:	6013      	str	r3, [r2, #0]
 8001258:	e7b9      	b.n	80011ce <_dtoa_r+0x46>
 800125a:	aa16      	add	r2, sp, #88	; 0x58
 800125c:	a917      	add	r1, sp, #92	; 0x5c
 800125e:	ec47 6b10 	vmov	d0, r6, r7
 8001262:	4620      	mov	r0, r4
 8001264:	f001 fb60 	bl	8002928 <__d2b>
 8001268:	ea5f 5519 	movs.w	r5, r9, lsr #20
 800126c:	4682      	mov	sl, r0
 800126e:	d073      	beq.n	8001358 <_dtoa_r+0x1d0>
 8001270:	f3c7 0313 	ubfx	r3, r7, #0, #20
 8001274:	f043 517f 	orr.w	r1, r3, #1069547520	; 0x3fc00000
 8001278:	f8cd 8048 	str.w	r8, [sp, #72]	; 0x48
 800127c:	f8dd 8058 	ldr.w	r8, [sp, #88]	; 0x58
 8001280:	4630      	mov	r0, r6
 8001282:	f2a5 35ff 	subw	r5, r5, #1023	; 0x3ff
 8001286:	f441 1140 	orr.w	r1, r1, #3145728	; 0x300000
 800128a:	2200      	movs	r2, #0
 800128c:	4b7a      	ldr	r3, [pc, #488]	; (8001478 <_dtoa_r+0x2f0>)
 800128e:	f001 fdd9 	bl	8002e44 <__aeabi_dsub>
 8001292:	a371      	add	r3, pc, #452	; (adr r3, 8001458 <_dtoa_r+0x2d0>)
 8001294:	e9d3 2300 	ldrd	r2, r3, [r3]
 8001298:	f001 ff88 	bl	80031ac <__aeabi_dmul>
 800129c:	a370      	add	r3, pc, #448	; (adr r3, 8001460 <_dtoa_r+0x2d8>)
 800129e:	e9d3 2300 	ldrd	r2, r3, [r3]
 80012a2:	f001 fdd1 	bl	8002e48 <__adddf3>
 80012a6:	4606      	mov	r6, r0
 80012a8:	4628      	mov	r0, r5
 80012aa:	460f      	mov	r7, r1
 80012ac:	f001 ff18 	bl	80030e0 <__aeabi_i2d>
 80012b0:	a36d      	add	r3, pc, #436	; (adr r3, 8001468 <_dtoa_r+0x2e0>)
 80012b2:	e9d3 2300 	ldrd	r2, r3, [r3]
 80012b6:	f001 ff79 	bl	80031ac <__aeabi_dmul>
 80012ba:	4602      	mov	r2, r0
 80012bc:	460b      	mov	r3, r1
 80012be:	4630      	mov	r0, r6
 80012c0:	4639      	mov	r1, r7
 80012c2:	f001 fdc1 	bl	8002e48 <__adddf3>
 80012c6:	4606      	mov	r6, r0
 80012c8:	460f      	mov	r7, r1
 80012ca:	f002 fa1f 	bl	800370c <__aeabi_d2iz>
 80012ce:	2200      	movs	r2, #0
 80012d0:	9005      	str	r0, [sp, #20]
 80012d2:	2300      	movs	r3, #0
 80012d4:	4630      	mov	r0, r6
 80012d6:	4639      	mov	r1, r7
 80012d8:	f002 f9da 	bl	8003690 <__aeabi_dcmplt>
 80012dc:	2800      	cmp	r0, #0
 80012de:	f040 8174 	bne.w	80015ca <_dtoa_r+0x442>
 80012e2:	9e05      	ldr	r6, [sp, #20]
 80012e4:	2e16      	cmp	r6, #22
 80012e6:	d856      	bhi.n	8001396 <_dtoa_r+0x20e>
 80012e8:	4b64      	ldr	r3, [pc, #400]	; (800147c <_dtoa_r+0x2f4>)
 80012ea:	eb03 03c6 	add.w	r3, r3, r6, lsl #3
 80012ee:	e9d3 0100 	ldrd	r0, r1, [r3]
 80012f2:	e9dd 2300 	ldrd	r2, r3, [sp]
 80012f6:	f002 f9e9 	bl	80036cc <__aeabi_dcmpgt>
 80012fa:	2800      	cmp	r0, #0
 80012fc:	f000 81cd 	beq.w	800169a <_dtoa_r+0x512>
 8001300:	1e73      	subs	r3, r6, #1
 8001302:	9305      	str	r3, [sp, #20]
 8001304:	2300      	movs	r3, #0
 8001306:	930c      	str	r3, [sp, #48]	; 0x30
 8001308:	eba8 0505 	sub.w	r5, r8, r5
 800130c:	1e6b      	subs	r3, r5, #1
 800130e:	9306      	str	r3, [sp, #24]
 8001310:	f100 8155 	bmi.w	80015be <_dtoa_r+0x436>
 8001314:	2300      	movs	r3, #0
 8001316:	9307      	str	r3, [sp, #28]
 8001318:	9b05      	ldr	r3, [sp, #20]
 800131a:	2b00      	cmp	r3, #0
 800131c:	f2c0 8167 	blt.w	80015ee <_dtoa_r+0x466>
 8001320:	9a06      	ldr	r2, [sp, #24]
 8001322:	930b      	str	r3, [sp, #44]	; 0x2c
 8001324:	4611      	mov	r1, r2
 8001326:	4419      	add	r1, r3
 8001328:	2300      	movs	r3, #0
 800132a:	9106      	str	r1, [sp, #24]
 800132c:	930a      	str	r3, [sp, #40]	; 0x28
 800132e:	9b02      	ldr	r3, [sp, #8]
 8001330:	2b09      	cmp	r3, #9
 8001332:	d833      	bhi.n	800139c <_dtoa_r+0x214>
 8001334:	2b05      	cmp	r3, #5
 8001336:	f340 8691 	ble.w	800205c <_dtoa_r+0xed4>
 800133a:	3b04      	subs	r3, #4
 800133c:	9302      	str	r3, [sp, #8]
 800133e:	f04f 0800 	mov.w	r8, #0
 8001342:	9b02      	ldr	r3, [sp, #8]
 8001344:	3b02      	subs	r3, #2
 8001346:	2b03      	cmp	r3, #3
 8001348:	f200 868c 	bhi.w	8002064 <_dtoa_r+0xedc>
 800134c:	e8df f013 	tbh	[pc, r3, lsl #1]
 8001350:	02db03fb 	.word	0x02db03fb
 8001354:	04980408 	.word	0x04980408
 8001358:	f8dd 8058 	ldr.w	r8, [sp, #88]	; 0x58
 800135c:	9d17      	ldr	r5, [sp, #92]	; 0x5c
 800135e:	4445      	add	r5, r8
 8001360:	f205 4332 	addw	r3, r5, #1074	; 0x432
 8001364:	2b20      	cmp	r3, #32
 8001366:	f340 819a 	ble.w	800169e <_dtoa_r+0x516>
 800136a:	f1c3 0340 	rsb	r3, r3, #64	; 0x40
 800136e:	fa09 f903 	lsl.w	r9, r9, r3
 8001372:	9b00      	ldr	r3, [sp, #0]
 8001374:	f205 4012 	addw	r0, r5, #1042	; 0x412
 8001378:	fa23 f000 	lsr.w	r0, r3, r0
 800137c:	ea40 0009 	orr.w	r0, r0, r9
 8001380:	f001 fe9e 	bl	80030c0 <__aeabi_ui2d>
 8001384:	2301      	movs	r3, #1
 8001386:	3d01      	subs	r5, #1
 8001388:	f1a1 71f8 	sub.w	r1, r1, #32505856	; 0x1f00000
 800138c:	9312      	str	r3, [sp, #72]	; 0x48
 800138e:	e77c      	b.n	800128a <_dtoa_r+0x102>
 8001390:	f8df 90f8 	ldr.w	r9, [pc, #248]	; 800148c <_dtoa_r+0x304>
 8001394:	e736      	b.n	8001204 <_dtoa_r+0x7c>
 8001396:	2301      	movs	r3, #1
 8001398:	930c      	str	r3, [sp, #48]	; 0x30
 800139a:	e7b5      	b.n	8001308 <_dtoa_r+0x180>
 800139c:	6a66      	ldr	r6, [r4, #36]	; 0x24
 800139e:	2500      	movs	r5, #0
 80013a0:	6075      	str	r5, [r6, #4]
 80013a2:	4629      	mov	r1, r5
 80013a4:	4620      	mov	r0, r4
 80013a6:	f000 ff1b 	bl	80021e0 <_Balloc>
 80013aa:	6a63      	ldr	r3, [r4, #36]	; 0x24
 80013ac:	6030      	str	r0, [r6, #0]
 80013ae:	f04f 32ff 	mov.w	r2, #4294967295	; 0xffffffff
 80013b2:	f8d3 9000 	ldr.w	r9, [r3]
 80013b6:	9208      	str	r2, [sp, #32]
 80013b8:	2301      	movs	r3, #1
 80013ba:	46ab      	mov	fp, r5
 80013bc:	9502      	str	r5, [sp, #8]
 80013be:	920d      	str	r2, [sp, #52]	; 0x34
 80013c0:	9309      	str	r3, [sp, #36]	; 0x24
 80013c2:	9b17      	ldr	r3, [sp, #92]	; 0x5c
 80013c4:	2b00      	cmp	r3, #0
 80013c6:	f2c0 80d1 	blt.w	800156c <_dtoa_r+0x3e4>
 80013ca:	9a05      	ldr	r2, [sp, #20]
 80013cc:	2a0e      	cmp	r2, #14
 80013ce:	f300 80cd 	bgt.w	800156c <_dtoa_r+0x3e4>
 80013d2:	4b2a      	ldr	r3, [pc, #168]	; (800147c <_dtoa_r+0x2f4>)
 80013d4:	eb03 03c2 	add.w	r3, r3, r2, lsl #3
 80013d8:	ed93 7b00 	vldr	d7, [r3]
 80013dc:	f1bb 0f00 	cmp.w	fp, #0
 80013e0:	ed8d 7b02 	vstr	d7, [sp, #8]
 80013e4:	f2c0 83da 	blt.w	8001b9c <_dtoa_r+0xa14>
 80013e8:	e9dd 6700 	ldrd	r6, r7, [sp]
 80013ec:	e9dd 2302 	ldrd	r2, r3, [sp, #8]
 80013f0:	4630      	mov	r0, r6
 80013f2:	4639      	mov	r1, r7
 80013f4:	f002 f804 	bl	8003400 <__aeabi_ddiv>
 80013f8:	f002 f988 	bl	800370c <__aeabi_d2iz>
 80013fc:	4680      	mov	r8, r0
 80013fe:	f001 fe6f 	bl	80030e0 <__aeabi_i2d>
 8001402:	e9dd 2302 	ldrd	r2, r3, [sp, #8]
 8001406:	f001 fed1 	bl	80031ac <__aeabi_dmul>
 800140a:	460b      	mov	r3, r1
 800140c:	4602      	mov	r2, r0
 800140e:	4639      	mov	r1, r7
 8001410:	4630      	mov	r0, r6
 8001412:	f001 fd17 	bl	8002e44 <__aeabi_dsub>
 8001416:	f108 0330 	add.w	r3, r8, #48	; 0x30
 800141a:	f889 3000 	strb.w	r3, [r9]
 800141e:	9b08      	ldr	r3, [sp, #32]
 8001420:	2b01      	cmp	r3, #1
 8001422:	4606      	mov	r6, r0
 8001424:	460f      	mov	r7, r1
 8001426:	f109 0501 	add.w	r5, r9, #1
 800142a:	d061      	beq.n	80014f0 <_dtoa_r+0x368>
 800142c:	2200      	movs	r2, #0
 800142e:	4b14      	ldr	r3, [pc, #80]	; (8001480 <_dtoa_r+0x2f8>)
 8001430:	f001 febc 	bl	80031ac <__aeabi_dmul>
 8001434:	2200      	movs	r2, #0
 8001436:	2300      	movs	r3, #0
 8001438:	4606      	mov	r6, r0
 800143a:	460f      	mov	r7, r1
 800143c:	f002 f91e 	bl	800367c <__aeabi_dcmpeq>
 8001440:	2800      	cmp	r0, #0
 8001442:	f040 8080 	bne.w	8001546 <_dtoa_r+0x3be>
 8001446:	f8cd a000 	str.w	sl, [sp]
 800144a:	9406      	str	r4, [sp, #24]
 800144c:	e9dd ab02 	ldrd	sl, fp, [sp, #8]
 8001450:	9c08      	ldr	r4, [sp, #32]
 8001452:	e028      	b.n	80014a6 <_dtoa_r+0x31e>
 8001454:	f3af 8000 	nop.w
 8001458:	636f4361 	.word	0x636f4361
 800145c:	3fd287a7 	.word	0x3fd287a7
 8001460:	8b60c8b3 	.word	0x8b60c8b3
 8001464:	3fc68a28 	.word	0x3fc68a28
 8001468:	509f79fb 	.word	0x509f79fb
 800146c:	3fd34413 	.word	0x3fd34413
 8001470:	7ff00000 	.word	0x7ff00000
 8001474:	0800483d 	.word	0x0800483d
 8001478:	3ff80000 	.word	0x3ff80000
 800147c:	08004710 	.word	0x08004710
 8001480:	40240000 	.word	0x40240000
 8001484:	08004868 	.word	0x08004868
 8001488:	08004874 	.word	0x08004874
 800148c:	0800483c 	.word	0x0800483c
 8001490:	f001 fe8c 	bl	80031ac <__aeabi_dmul>
 8001494:	2200      	movs	r2, #0
 8001496:	2300      	movs	r3, #0
 8001498:	4606      	mov	r6, r0
 800149a:	460f      	mov	r7, r1
 800149c:	f002 f8ee 	bl	800367c <__aeabi_dcmpeq>
 80014a0:	2800      	cmp	r0, #0
 80014a2:	f040 83f0 	bne.w	8001c86 <_dtoa_r+0xafe>
 80014a6:	4652      	mov	r2, sl
 80014a8:	465b      	mov	r3, fp
 80014aa:	4630      	mov	r0, r6
 80014ac:	4639      	mov	r1, r7
 80014ae:	f001 ffa7 	bl	8003400 <__aeabi_ddiv>
 80014b2:	f002 f92b 	bl	800370c <__aeabi_d2iz>
 80014b6:	4680      	mov	r8, r0
 80014b8:	f001 fe12 	bl	80030e0 <__aeabi_i2d>
 80014bc:	4652      	mov	r2, sl
 80014be:	465b      	mov	r3, fp
 80014c0:	f001 fe74 	bl	80031ac <__aeabi_dmul>
 80014c4:	4602      	mov	r2, r0
 80014c6:	460b      	mov	r3, r1
 80014c8:	4630      	mov	r0, r6
 80014ca:	4639      	mov	r1, r7
 80014cc:	f001 fcba 	bl	8002e44 <__aeabi_dsub>
 80014d0:	f108 0e30 	add.w	lr, r8, #48	; 0x30
 80014d4:	f805 eb01 	strb.w	lr, [r5], #1
 80014d8:	eba5 0e09 	sub.w	lr, r5, r9
 80014dc:	45a6      	cmp	lr, r4
 80014de:	4606      	mov	r6, r0
 80014e0:	460f      	mov	r7, r1
 80014e2:	f04f 0200 	mov.w	r2, #0
 80014e6:	4b71      	ldr	r3, [pc, #452]	; (80016ac <_dtoa_r+0x524>)
 80014e8:	d1d2      	bne.n	8001490 <_dtoa_r+0x308>
 80014ea:	f8dd a000 	ldr.w	sl, [sp]
 80014ee:	9c06      	ldr	r4, [sp, #24]
 80014f0:	4632      	mov	r2, r6
 80014f2:	463b      	mov	r3, r7
 80014f4:	4630      	mov	r0, r6
 80014f6:	4639      	mov	r1, r7
 80014f8:	f001 fca6 	bl	8002e48 <__adddf3>
 80014fc:	4606      	mov	r6, r0
 80014fe:	460f      	mov	r7, r1
 8001500:	4602      	mov	r2, r0
 8001502:	460b      	mov	r3, r1
 8001504:	e9dd 0102 	ldrd	r0, r1, [sp, #8]
 8001508:	f002 f8c2 	bl	8003690 <__aeabi_dcmplt>
 800150c:	b948      	cbnz	r0, 8001522 <_dtoa_r+0x39a>
 800150e:	4632      	mov	r2, r6
 8001510:	463b      	mov	r3, r7
 8001512:	e9dd 0102 	ldrd	r0, r1, [sp, #8]
 8001516:	f002 f8b1 	bl	800367c <__aeabi_dcmpeq>
 800151a:	b1a0      	cbz	r0, 8001546 <_dtoa_r+0x3be>
 800151c:	f018 0f01 	tst.w	r8, #1
 8001520:	d011      	beq.n	8001546 <_dtoa_r+0x3be>
 8001522:	f815 8c01 	ldrb.w	r8, [r5, #-1]
 8001526:	1e6b      	subs	r3, r5, #1
 8001528:	e004      	b.n	8001534 <_dtoa_r+0x3ac>
 800152a:	4599      	cmp	r9, r3
 800152c:	f000 8464 	beq.w	8001df8 <_dtoa_r+0xc70>
 8001530:	f813 8d01 	ldrb.w	r8, [r3, #-1]!
 8001534:	f1b8 0f39 	cmp.w	r8, #57	; 0x39
 8001538:	f103 0501 	add.w	r5, r3, #1
 800153c:	d0f5      	beq.n	800152a <_dtoa_r+0x3a2>
 800153e:	f108 0801 	add.w	r8, r8, #1
 8001542:	f883 8000 	strb.w	r8, [r3]
 8001546:	4651      	mov	r1, sl
 8001548:	4620      	mov	r0, r4
 800154a:	f000 fe7d 	bl	8002248 <_Bfree>
 800154e:	2200      	movs	r2, #0
 8001550:	9b05      	ldr	r3, [sp, #20]
 8001552:	702a      	strb	r2, [r5, #0]
 8001554:	9a04      	ldr	r2, [sp, #16]
 8001556:	3301      	adds	r3, #1
 8001558:	6013      	str	r3, [r2, #0]
 800155a:	9b23      	ldr	r3, [sp, #140]	; 0x8c
 800155c:	2b00      	cmp	r3, #0
 800155e:	f43f ae51 	beq.w	8001204 <_dtoa_r+0x7c>
 8001562:	4648      	mov	r0, r9
 8001564:	601d      	str	r5, [r3, #0]
 8001566:	b019      	add	sp, #100	; 0x64
 8001568:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800156c:	9a09      	ldr	r2, [sp, #36]	; 0x24
 800156e:	2a00      	cmp	r2, #0
 8001570:	d046      	beq.n	8001600 <_dtoa_r+0x478>
 8001572:	9a02      	ldr	r2, [sp, #8]
 8001574:	2a01      	cmp	r2, #1
 8001576:	f340 8339 	ble.w	8001bec <_dtoa_r+0xa64>
 800157a:	9b08      	ldr	r3, [sp, #32]
 800157c:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 800157e:	1e5f      	subs	r7, r3, #1
 8001580:	42ba      	cmp	r2, r7
 8001582:	f2c0 83b2 	blt.w	8001cea <_dtoa_r+0xb62>
 8001586:	1bd7      	subs	r7, r2, r7
 8001588:	9b08      	ldr	r3, [sp, #32]
 800158a:	2b00      	cmp	r3, #0
 800158c:	f2c0 84c6 	blt.w	8001f1c <_dtoa_r+0xd94>
 8001590:	9d07      	ldr	r5, [sp, #28]
 8001592:	9b08      	ldr	r3, [sp, #32]
 8001594:	9a07      	ldr	r2, [sp, #28]
 8001596:	441a      	add	r2, r3
 8001598:	9207      	str	r2, [sp, #28]
 800159a:	9a06      	ldr	r2, [sp, #24]
 800159c:	2101      	movs	r1, #1
 800159e:	441a      	add	r2, r3
 80015a0:	4620      	mov	r0, r4
 80015a2:	9206      	str	r2, [sp, #24]
 80015a4:	f000 ff48 	bl	8002438 <__i2b>
 80015a8:	4606      	mov	r6, r0
 80015aa:	e02c      	b.n	8001606 <_dtoa_r+0x47e>
 80015ac:	2010      	movs	r0, #16
 80015ae:	f000 fdb3 	bl	8002118 <malloc>
 80015b2:	6260      	str	r0, [r4, #36]	; 0x24
 80015b4:	6045      	str	r5, [r0, #4]
 80015b6:	6085      	str	r5, [r0, #8]
 80015b8:	6005      	str	r5, [r0, #0]
 80015ba:	60c5      	str	r5, [r0, #12]
 80015bc:	e5ff      	b.n	80011be <_dtoa_r+0x36>
 80015be:	f1c5 0301 	rsb	r3, r5, #1
 80015c2:	9307      	str	r3, [sp, #28]
 80015c4:	2300      	movs	r3, #0
 80015c6:	9306      	str	r3, [sp, #24]
 80015c8:	e6a6      	b.n	8001318 <_dtoa_r+0x190>
 80015ca:	f8dd 9014 	ldr.w	r9, [sp, #20]
 80015ce:	4648      	mov	r0, r9
 80015d0:	f001 fd86 	bl	80030e0 <__aeabi_i2d>
 80015d4:	4602      	mov	r2, r0
 80015d6:	460b      	mov	r3, r1
 80015d8:	4630      	mov	r0, r6
 80015da:	4639      	mov	r1, r7
 80015dc:	f002 f84e 	bl	800367c <__aeabi_dcmpeq>
 80015e0:	2800      	cmp	r0, #0
 80015e2:	f47f ae7e 	bne.w	80012e2 <_dtoa_r+0x15a>
 80015e6:	f109 33ff 	add.w	r3, r9, #4294967295	; 0xffffffff
 80015ea:	9305      	str	r3, [sp, #20]
 80015ec:	e679      	b.n	80012e2 <_dtoa_r+0x15a>
 80015ee:	9a07      	ldr	r2, [sp, #28]
 80015f0:	9b05      	ldr	r3, [sp, #20]
 80015f2:	1ad2      	subs	r2, r2, r3
 80015f4:	425b      	negs	r3, r3
 80015f6:	930a      	str	r3, [sp, #40]	; 0x28
 80015f8:	2300      	movs	r3, #0
 80015fa:	9207      	str	r2, [sp, #28]
 80015fc:	930b      	str	r3, [sp, #44]	; 0x2c
 80015fe:	e696      	b.n	800132e <_dtoa_r+0x1a6>
 8001600:	9f0a      	ldr	r7, [sp, #40]	; 0x28
 8001602:	9d07      	ldr	r5, [sp, #28]
 8001604:	9e09      	ldr	r6, [sp, #36]	; 0x24
 8001606:	2d00      	cmp	r5, #0
 8001608:	dd0c      	ble.n	8001624 <_dtoa_r+0x49c>
 800160a:	9906      	ldr	r1, [sp, #24]
 800160c:	2900      	cmp	r1, #0
 800160e:	460b      	mov	r3, r1
 8001610:	dd08      	ble.n	8001624 <_dtoa_r+0x49c>
 8001612:	42a9      	cmp	r1, r5
 8001614:	9a07      	ldr	r2, [sp, #28]
 8001616:	bfa8      	it	ge
 8001618:	462b      	movge	r3, r5
 800161a:	1ad2      	subs	r2, r2, r3
 800161c:	1aed      	subs	r5, r5, r3
 800161e:	1acb      	subs	r3, r1, r3
 8001620:	9207      	str	r2, [sp, #28]
 8001622:	9306      	str	r3, [sp, #24]
 8001624:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8001626:	b1d3      	cbz	r3, 800165e <_dtoa_r+0x4d6>
 8001628:	9b09      	ldr	r3, [sp, #36]	; 0x24
 800162a:	2b00      	cmp	r3, #0
 800162c:	f000 82d7 	beq.w	8001bde <_dtoa_r+0xa56>
 8001630:	2f00      	cmp	r7, #0
 8001632:	dd10      	ble.n	8001656 <_dtoa_r+0x4ce>
 8001634:	4631      	mov	r1, r6
 8001636:	463a      	mov	r2, r7
 8001638:	4620      	mov	r0, r4
 800163a:	f000 ff99 	bl	8002570 <__pow5mult>
 800163e:	4652      	mov	r2, sl
 8001640:	4601      	mov	r1, r0
 8001642:	4606      	mov	r6, r0
 8001644:	4620      	mov	r0, r4
 8001646:	f000 ff01 	bl	800244c <__multiply>
 800164a:	4651      	mov	r1, sl
 800164c:	4680      	mov	r8, r0
 800164e:	4620      	mov	r0, r4
 8001650:	f000 fdfa 	bl	8002248 <_Bfree>
 8001654:	46c2      	mov	sl, r8
 8001656:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8001658:	1bda      	subs	r2, r3, r7
 800165a:	f040 82c1 	bne.w	8001be0 <_dtoa_r+0xa58>
 800165e:	2101      	movs	r1, #1
 8001660:	4620      	mov	r0, r4
 8001662:	f000 fee9 	bl	8002438 <__i2b>
 8001666:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
 8001668:	2b00      	cmp	r3, #0
 800166a:	4680      	mov	r8, r0
 800166c:	dd20      	ble.n	80016b0 <_dtoa_r+0x528>
 800166e:	4601      	mov	r1, r0
 8001670:	461a      	mov	r2, r3
 8001672:	4620      	mov	r0, r4
 8001674:	f000 ff7c 	bl	8002570 <__pow5mult>
 8001678:	9b02      	ldr	r3, [sp, #8]
 800167a:	2b01      	cmp	r3, #1
 800167c:	4680      	mov	r8, r0
 800167e:	f340 8272 	ble.w	8001b66 <_dtoa_r+0x9de>
 8001682:	2300      	movs	r3, #0
 8001684:	930a      	str	r3, [sp, #40]	; 0x28
 8001686:	f8d8 3010 	ldr.w	r3, [r8, #16]
 800168a:	eb08 0383 	add.w	r3, r8, r3, lsl #2
 800168e:	6918      	ldr	r0, [r3, #16]
 8001690:	f000 fe82 	bl	8002398 <__hi0bits>
 8001694:	f1c0 0020 	rsb	r0, r0, #32
 8001698:	e014      	b.n	80016c4 <_dtoa_r+0x53c>
 800169a:	900c      	str	r0, [sp, #48]	; 0x30
 800169c:	e634      	b.n	8001308 <_dtoa_r+0x180>
 800169e:	f1c3 0920 	rsb	r9, r3, #32
 80016a2:	9b00      	ldr	r3, [sp, #0]
 80016a4:	fa03 f009 	lsl.w	r0, r3, r9
 80016a8:	e66a      	b.n	8001380 <_dtoa_r+0x1f8>
 80016aa:	bf00      	nop
 80016ac:	40240000 	.word	0x40240000
 80016b0:	9b02      	ldr	r3, [sp, #8]
 80016b2:	2b01      	cmp	r3, #1
 80016b4:	f340 82a3 	ble.w	8001bfe <_dtoa_r+0xa76>
 80016b8:	2300      	movs	r3, #0
 80016ba:	930a      	str	r3, [sp, #40]	; 0x28
 80016bc:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
 80016be:	2001      	movs	r0, #1
 80016c0:	2b00      	cmp	r3, #0
 80016c2:	d1e0      	bne.n	8001686 <_dtoa_r+0x4fe>
 80016c4:	9a06      	ldr	r2, [sp, #24]
 80016c6:	4410      	add	r0, r2
 80016c8:	f010 001f 	ands.w	r0, r0, #31
 80016cc:	f000 80a5 	beq.w	800181a <_dtoa_r+0x692>
 80016d0:	f1c0 0320 	rsb	r3, r0, #32
 80016d4:	2b04      	cmp	r3, #4
 80016d6:	f340 84bb 	ble.w	8002050 <_dtoa_r+0xec8>
 80016da:	9b07      	ldr	r3, [sp, #28]
 80016dc:	f1c0 001c 	rsb	r0, r0, #28
 80016e0:	4403      	add	r3, r0
 80016e2:	9307      	str	r3, [sp, #28]
 80016e4:	4613      	mov	r3, r2
 80016e6:	4403      	add	r3, r0
 80016e8:	4405      	add	r5, r0
 80016ea:	9306      	str	r3, [sp, #24]
 80016ec:	9b07      	ldr	r3, [sp, #28]
 80016ee:	2b00      	cmp	r3, #0
 80016f0:	dd05      	ble.n	80016fe <_dtoa_r+0x576>
 80016f2:	4651      	mov	r1, sl
 80016f4:	461a      	mov	r2, r3
 80016f6:	4620      	mov	r0, r4
 80016f8:	f000 ff96 	bl	8002628 <__lshift>
 80016fc:	4682      	mov	sl, r0
 80016fe:	9b06      	ldr	r3, [sp, #24]
 8001700:	2b00      	cmp	r3, #0
 8001702:	dd05      	ble.n	8001710 <_dtoa_r+0x588>
 8001704:	4641      	mov	r1, r8
 8001706:	461a      	mov	r2, r3
 8001708:	4620      	mov	r0, r4
 800170a:	f000 ff8d 	bl	8002628 <__lshift>
 800170e:	4680      	mov	r8, r0
 8001710:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 8001712:	2b00      	cmp	r3, #0
 8001714:	f040 808a 	bne.w	800182c <_dtoa_r+0x6a4>
 8001718:	9b08      	ldr	r3, [sp, #32]
 800171a:	2b00      	cmp	r3, #0
 800171c:	f340 827f 	ble.w	8001c1e <_dtoa_r+0xa96>
 8001720:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8001722:	2b00      	cmp	r3, #0
 8001724:	f000 809c 	beq.w	8001860 <_dtoa_r+0x6d8>
 8001728:	2d00      	cmp	r5, #0
 800172a:	dd05      	ble.n	8001738 <_dtoa_r+0x5b0>
 800172c:	4631      	mov	r1, r6
 800172e:	462a      	mov	r2, r5
 8001730:	4620      	mov	r0, r4
 8001732:	f000 ff79 	bl	8002628 <__lshift>
 8001736:	4606      	mov	r6, r0
 8001738:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 800173a:	2b00      	cmp	r3, #0
 800173c:	f040 8366 	bne.w	8001e0c <_dtoa_r+0xc84>
 8001740:	9606      	str	r6, [sp, #24]
 8001742:	9b08      	ldr	r3, [sp, #32]
 8001744:	f8cd 9028 	str.w	r9, [sp, #40]	; 0x28
 8001748:	3b01      	subs	r3, #1
 800174a:	444b      	add	r3, r9
 800174c:	9308      	str	r3, [sp, #32]
 800174e:	9b00      	ldr	r3, [sp, #0]
 8001750:	464f      	mov	r7, r9
 8001752:	f003 0301 	and.w	r3, r3, #1
 8001756:	46c1      	mov	r9, r8
 8001758:	f8dd 8018 	ldr.w	r8, [sp, #24]
 800175c:	9309      	str	r3, [sp, #36]	; 0x24
 800175e:	4649      	mov	r1, r9
 8001760:	4650      	mov	r0, sl
 8001762:	f7ff fc7d 	bl	8001060 <quorem>
 8001766:	4631      	mov	r1, r6
 8001768:	4605      	mov	r5, r0
 800176a:	4650      	mov	r0, sl
 800176c:	f000 ffb0 	bl	80026d0 <__mcmp>
 8001770:	4642      	mov	r2, r8
 8001772:	4649      	mov	r1, r9
 8001774:	4683      	mov	fp, r0
 8001776:	4620      	mov	r0, r4
 8001778:	f000 ffc6 	bl	8002708 <__mdiff>
 800177c:	f105 0230 	add.w	r2, r5, #48	; 0x30
 8001780:	9200      	str	r2, [sp, #0]
 8001782:	68c2      	ldr	r2, [r0, #12]
 8001784:	4601      	mov	r1, r0
 8001786:	2a00      	cmp	r2, #0
 8001788:	f040 8244 	bne.w	8001c14 <_dtoa_r+0xa8c>
 800178c:	9007      	str	r0, [sp, #28]
 800178e:	4650      	mov	r0, sl
 8001790:	f000 ff9e 	bl	80026d0 <__mcmp>
 8001794:	9b07      	ldr	r3, [sp, #28]
 8001796:	9006      	str	r0, [sp, #24]
 8001798:	4619      	mov	r1, r3
 800179a:	4620      	mov	r0, r4
 800179c:	f000 fd54 	bl	8002248 <_Bfree>
 80017a0:	9a06      	ldr	r2, [sp, #24]
 80017a2:	b932      	cbnz	r2, 80017b2 <_dtoa_r+0x62a>
 80017a4:	9b02      	ldr	r3, [sp, #8]
 80017a6:	b923      	cbnz	r3, 80017b2 <_dtoa_r+0x62a>
 80017a8:	9b09      	ldr	r3, [sp, #36]	; 0x24
 80017aa:	2b00      	cmp	r3, #0
 80017ac:	f000 8422 	beq.w	8001ff4 <_dtoa_r+0xe6c>
 80017b0:	9a02      	ldr	r2, [sp, #8]
 80017b2:	f1bb 0f00 	cmp.w	fp, #0
 80017b6:	f2c0 82b6 	blt.w	8001d26 <_dtoa_r+0xb9e>
 80017ba:	d105      	bne.n	80017c8 <_dtoa_r+0x640>
 80017bc:	9b02      	ldr	r3, [sp, #8]
 80017be:	b91b      	cbnz	r3, 80017c8 <_dtoa_r+0x640>
 80017c0:	9b09      	ldr	r3, [sp, #36]	; 0x24
 80017c2:	2b00      	cmp	r3, #0
 80017c4:	f000 82af 	beq.w	8001d26 <_dtoa_r+0xb9e>
 80017c8:	2a00      	cmp	r2, #0
 80017ca:	f300 8333 	bgt.w	8001e34 <_dtoa_r+0xcac>
 80017ce:	f89d 3000 	ldrb.w	r3, [sp]
 80017d2:	703b      	strb	r3, [r7, #0]
 80017d4:	9b08      	ldr	r3, [sp, #32]
 80017d6:	f107 0b01 	add.w	fp, r7, #1
 80017da:	429f      	cmp	r7, r3
 80017dc:	465d      	mov	r5, fp
 80017de:	f000 8339 	beq.w	8001e54 <_dtoa_r+0xccc>
 80017e2:	4651      	mov	r1, sl
 80017e4:	2300      	movs	r3, #0
 80017e6:	220a      	movs	r2, #10
 80017e8:	4620      	mov	r0, r4
 80017ea:	f000 fd49 	bl	8002280 <__multadd>
 80017ee:	4546      	cmp	r6, r8
 80017f0:	4682      	mov	sl, r0
 80017f2:	4631      	mov	r1, r6
 80017f4:	f04f 0300 	mov.w	r3, #0
 80017f8:	f04f 020a 	mov.w	r2, #10
 80017fc:	4620      	mov	r0, r4
 80017fe:	f000 8203 	beq.w	8001c08 <_dtoa_r+0xa80>
 8001802:	f000 fd3d 	bl	8002280 <__multadd>
 8001806:	4641      	mov	r1, r8
 8001808:	4606      	mov	r6, r0
 800180a:	2300      	movs	r3, #0
 800180c:	220a      	movs	r2, #10
 800180e:	4620      	mov	r0, r4
 8001810:	f000 fd36 	bl	8002280 <__multadd>
 8001814:	465f      	mov	r7, fp
 8001816:	4680      	mov	r8, r0
 8001818:	e7a1      	b.n	800175e <_dtoa_r+0x5d6>
 800181a:	201c      	movs	r0, #28
 800181c:	9b07      	ldr	r3, [sp, #28]
 800181e:	4403      	add	r3, r0
 8001820:	9307      	str	r3, [sp, #28]
 8001822:	9b06      	ldr	r3, [sp, #24]
 8001824:	4403      	add	r3, r0
 8001826:	4405      	add	r5, r0
 8001828:	9306      	str	r3, [sp, #24]
 800182a:	e75f      	b.n	80016ec <_dtoa_r+0x564>
 800182c:	4641      	mov	r1, r8
 800182e:	4650      	mov	r0, sl
 8001830:	f000 ff4e 	bl	80026d0 <__mcmp>
 8001834:	2800      	cmp	r0, #0
 8001836:	f6bf af6f 	bge.w	8001718 <_dtoa_r+0x590>
 800183a:	9f05      	ldr	r7, [sp, #20]
 800183c:	4651      	mov	r1, sl
 800183e:	2300      	movs	r3, #0
 8001840:	220a      	movs	r2, #10
 8001842:	4620      	mov	r0, r4
 8001844:	3f01      	subs	r7, #1
 8001846:	9705      	str	r7, [sp, #20]
 8001848:	f000 fd1a 	bl	8002280 <__multadd>
 800184c:	9b09      	ldr	r3, [sp, #36]	; 0x24
 800184e:	4682      	mov	sl, r0
 8001850:	2b00      	cmp	r3, #0
 8001852:	f040 83e3 	bne.w	800201c <_dtoa_r+0xe94>
 8001856:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 8001858:	2b00      	cmp	r3, #0
 800185a:	f340 83ec 	ble.w	8002036 <_dtoa_r+0xeae>
 800185e:	9308      	str	r3, [sp, #32]
 8001860:	464d      	mov	r5, r9
 8001862:	f8dd b020 	ldr.w	fp, [sp, #32]
 8001866:	e002      	b.n	800186e <_dtoa_r+0x6e6>
 8001868:	f000 fd0a 	bl	8002280 <__multadd>
 800186c:	4682      	mov	sl, r0
 800186e:	4641      	mov	r1, r8
 8001870:	4650      	mov	r0, sl
 8001872:	f7ff fbf5 	bl	8001060 <quorem>
 8001876:	f100 0730 	add.w	r7, r0, #48	; 0x30
 800187a:	f805 7b01 	strb.w	r7, [r5], #1
 800187e:	eba5 0309 	sub.w	r3, r5, r9
 8001882:	455b      	cmp	r3, fp
 8001884:	f04f 020a 	mov.w	r2, #10
 8001888:	f04f 0300 	mov.w	r3, #0
 800188c:	4651      	mov	r1, sl
 800188e:	4620      	mov	r0, r4
 8001890:	dbea      	blt.n	8001868 <_dtoa_r+0x6e0>
 8001892:	9b08      	ldr	r3, [sp, #32]
 8001894:	9700      	str	r7, [sp, #0]
 8001896:	2b01      	cmp	r3, #1
 8001898:	bfac      	ite	ge
 800189a:	eb09 0503 	addge.w	r5, r9, r3
 800189e:	f109 0501 	addlt.w	r5, r9, #1
 80018a2:	f04f 0b00 	mov.w	fp, #0
 80018a6:	4651      	mov	r1, sl
 80018a8:	2201      	movs	r2, #1
 80018aa:	4620      	mov	r0, r4
 80018ac:	f000 febc 	bl	8002628 <__lshift>
 80018b0:	4641      	mov	r1, r8
 80018b2:	4682      	mov	sl, r0
 80018b4:	f000 ff0c 	bl	80026d0 <__mcmp>
 80018b8:	2800      	cmp	r0, #0
 80018ba:	f340 8255 	ble.w	8001d68 <_dtoa_r+0xbe0>
 80018be:	f815 2c01 	ldrb.w	r2, [r5, #-1]
 80018c2:	1e6b      	subs	r3, r5, #1
 80018c4:	e004      	b.n	80018d0 <_dtoa_r+0x748>
 80018c6:	454b      	cmp	r3, r9
 80018c8:	f000 81c5 	beq.w	8001c56 <_dtoa_r+0xace>
 80018cc:	f813 2d01 	ldrb.w	r2, [r3, #-1]!
 80018d0:	2a39      	cmp	r2, #57	; 0x39
 80018d2:	f103 0501 	add.w	r5, r3, #1
 80018d6:	d0f6      	beq.n	80018c6 <_dtoa_r+0x73e>
 80018d8:	3201      	adds	r2, #1
 80018da:	701a      	strb	r2, [r3, #0]
 80018dc:	4641      	mov	r1, r8
 80018de:	4620      	mov	r0, r4
 80018e0:	f000 fcb2 	bl	8002248 <_Bfree>
 80018e4:	2e00      	cmp	r6, #0
 80018e6:	f43f ae2e 	beq.w	8001546 <_dtoa_r+0x3be>
 80018ea:	f1bb 0f00 	cmp.w	fp, #0
 80018ee:	d005      	beq.n	80018fc <_dtoa_r+0x774>
 80018f0:	45b3      	cmp	fp, r6
 80018f2:	d003      	beq.n	80018fc <_dtoa_r+0x774>
 80018f4:	4659      	mov	r1, fp
 80018f6:	4620      	mov	r0, r4
 80018f8:	f000 fca6 	bl	8002248 <_Bfree>
 80018fc:	4631      	mov	r1, r6
 80018fe:	4620      	mov	r0, r4
 8001900:	f000 fca2 	bl	8002248 <_Bfree>
 8001904:	e61f      	b.n	8001546 <_dtoa_r+0x3be>
 8001906:	2300      	movs	r3, #0
 8001908:	9309      	str	r3, [sp, #36]	; 0x24
 800190a:	9b05      	ldr	r3, [sp, #20]
 800190c:	445b      	add	r3, fp
 800190e:	930d      	str	r3, [sp, #52]	; 0x34
 8001910:	3301      	adds	r3, #1
 8001912:	2b01      	cmp	r3, #1
 8001914:	461f      	mov	r7, r3
 8001916:	461d      	mov	r5, r3
 8001918:	9308      	str	r3, [sp, #32]
 800191a:	bfb8      	it	lt
 800191c:	2701      	movlt	r7, #1
 800191e:	6a66      	ldr	r6, [r4, #36]	; 0x24
 8001920:	2100      	movs	r1, #0
 8001922:	2f17      	cmp	r7, #23
 8001924:	6071      	str	r1, [r6, #4]
 8001926:	d90a      	bls.n	800193e <_dtoa_r+0x7b6>
 8001928:	2201      	movs	r2, #1
 800192a:	2304      	movs	r3, #4
 800192c:	005b      	lsls	r3, r3, #1
 800192e:	f103 0014 	add.w	r0, r3, #20
 8001932:	42b8      	cmp	r0, r7
 8001934:	4611      	mov	r1, r2
 8001936:	f102 0201 	add.w	r2, r2, #1
 800193a:	d9f7      	bls.n	800192c <_dtoa_r+0x7a4>
 800193c:	6071      	str	r1, [r6, #4]
 800193e:	4620      	mov	r0, r4
 8001940:	f000 fc4e 	bl	80021e0 <_Balloc>
 8001944:	6a63      	ldr	r3, [r4, #36]	; 0x24
 8001946:	6030      	str	r0, [r6, #0]
 8001948:	2d0e      	cmp	r5, #14
 800194a:	f8d3 9000 	ldr.w	r9, [r3]
 800194e:	f63f ad38 	bhi.w	80013c2 <_dtoa_r+0x23a>
 8001952:	f1b8 0f00 	cmp.w	r8, #0
 8001956:	f43f ad34 	beq.w	80013c2 <_dtoa_r+0x23a>
 800195a:	ed9d 7b00 	vldr	d7, [sp]
 800195e:	9905      	ldr	r1, [sp, #20]
 8001960:	2900      	cmp	r1, #0
 8001962:	ed8d 7b0e 	vstr	d7, [sp, #56]	; 0x38
 8001966:	f340 8219 	ble.w	8001d9c <_dtoa_r+0xc14>
 800196a:	4bbe      	ldr	r3, [pc, #760]	; (8001c64 <_dtoa_r+0xadc>)
 800196c:	f001 020f 	and.w	r2, r1, #15
 8001970:	eb03 03c2 	add.w	r3, r3, r2, lsl #3
 8001974:	110d      	asrs	r5, r1, #4
 8001976:	e9d3 6700 	ldrd	r6, r7, [r3]
 800197a:	06eb      	lsls	r3, r5, #27
 800197c:	f140 81bd 	bpl.w	8001cfa <_dtoa_r+0xb72>
 8001980:	4bb9      	ldr	r3, [pc, #740]	; (8001c68 <_dtoa_r+0xae0>)
 8001982:	ec51 0b17 	vmov	r0, r1, d7
 8001986:	e9d3 2308 	ldrd	r2, r3, [r3, #32]
 800198a:	f001 fd39 	bl	8003400 <__aeabi_ddiv>
 800198e:	e9cd 0110 	strd	r0, r1, [sp, #64]	; 0x40
 8001992:	2303      	movs	r3, #3
 8001994:	f005 050f 	and.w	r5, r5, #15
 8001998:	9300      	str	r3, [sp, #0]
 800199a:	b1c5      	cbz	r5, 80019ce <_dtoa_r+0x846>
 800199c:	f8cd b04c 	str.w	fp, [sp, #76]	; 0x4c
 80019a0:	f8df 82c4 	ldr.w	r8, [pc, #708]	; 8001c68 <_dtoa_r+0xae0>
 80019a4:	46a3      	mov	fp, r4
 80019a6:	9c00      	ldr	r4, [sp, #0]
 80019a8:	07e8      	lsls	r0, r5, #31
 80019aa:	d508      	bpl.n	80019be <_dtoa_r+0x836>
 80019ac:	4630      	mov	r0, r6
 80019ae:	4639      	mov	r1, r7
 80019b0:	e9d8 2300 	ldrd	r2, r3, [r8]
 80019b4:	f001 fbfa 	bl	80031ac <__aeabi_dmul>
 80019b8:	3401      	adds	r4, #1
 80019ba:	4606      	mov	r6, r0
 80019bc:	460f      	mov	r7, r1
 80019be:	106d      	asrs	r5, r5, #1
 80019c0:	f108 0808 	add.w	r8, r8, #8
 80019c4:	d1f0      	bne.n	80019a8 <_dtoa_r+0x820>
 80019c6:	9400      	str	r4, [sp, #0]
 80019c8:	465c      	mov	r4, fp
 80019ca:	f8dd b04c 	ldr.w	fp, [sp, #76]	; 0x4c
 80019ce:	463b      	mov	r3, r7
 80019d0:	4632      	mov	r2, r6
 80019d2:	e9dd 0110 	ldrd	r0, r1, [sp, #64]	; 0x40
 80019d6:	f001 fd13 	bl	8003400 <__aeabi_ddiv>
 80019da:	4607      	mov	r7, r0
 80019dc:	4688      	mov	r8, r1
 80019de:	9b0c      	ldr	r3, [sp, #48]	; 0x30
 80019e0:	b143      	cbz	r3, 80019f4 <_dtoa_r+0x86c>
 80019e2:	2200      	movs	r2, #0
 80019e4:	4ba1      	ldr	r3, [pc, #644]	; (8001c6c <_dtoa_r+0xae4>)
 80019e6:	4638      	mov	r0, r7
 80019e8:	4641      	mov	r1, r8
 80019ea:	f001 fe51 	bl	8003690 <__aeabi_dcmplt>
 80019ee:	2800      	cmp	r0, #0
 80019f0:	f040 82a6 	bne.w	8001f40 <_dtoa_r+0xdb8>
 80019f4:	9800      	ldr	r0, [sp, #0]
 80019f6:	f001 fb73 	bl	80030e0 <__aeabi_i2d>
 80019fa:	463a      	mov	r2, r7
 80019fc:	4643      	mov	r3, r8
 80019fe:	f001 fbd5 	bl	80031ac <__aeabi_dmul>
 8001a02:	4b9b      	ldr	r3, [pc, #620]	; (8001c70 <_dtoa_r+0xae8>)
 8001a04:	2200      	movs	r2, #0
 8001a06:	f001 fa1f 	bl	8002e48 <__adddf3>
 8001a0a:	f1a1 7350 	sub.w	r3, r1, #54525952	; 0x3400000
 8001a0e:	e9cd 0100 	strd	r0, r1, [sp]
 8001a12:	9301      	str	r3, [sp, #4]
 8001a14:	9b08      	ldr	r3, [sp, #32]
 8001a16:	2b00      	cmp	r3, #0
 8001a18:	f000 8149 	beq.w	8001cae <_dtoa_r+0xb26>
 8001a1c:	9b05      	ldr	r3, [sp, #20]
 8001a1e:	9313      	str	r3, [sp, #76]	; 0x4c
 8001a20:	9b08      	ldr	r3, [sp, #32]
 8001a22:	9310      	str	r3, [sp, #64]	; 0x40
 8001a24:	9b09      	ldr	r3, [sp, #36]	; 0x24
 8001a26:	2b00      	cmp	r3, #0
 8001a28:	f000 821f 	beq.w	8001e6a <_dtoa_r+0xce2>
 8001a2c:	9a10      	ldr	r2, [sp, #64]	; 0x40
 8001a2e:	4b8d      	ldr	r3, [pc, #564]	; (8001c64 <_dtoa_r+0xadc>)
 8001a30:	4990      	ldr	r1, [pc, #576]	; (8001c74 <_dtoa_r+0xaec>)
 8001a32:	eb03 03c2 	add.w	r3, r3, r2, lsl #3
 8001a36:	e953 2302 	ldrd	r2, r3, [r3, #-8]
 8001a3a:	2000      	movs	r0, #0
 8001a3c:	f001 fce0 	bl	8003400 <__aeabi_ddiv>
 8001a40:	e9dd 2300 	ldrd	r2, r3, [sp]
 8001a44:	f001 f9fe 	bl	8002e44 <__aeabi_dsub>
 8001a48:	e9cd 0100 	strd	r0, r1, [sp]
 8001a4c:	4641      	mov	r1, r8
 8001a4e:	4638      	mov	r0, r7
 8001a50:	f001 fe5c 	bl	800370c <__aeabi_d2iz>
 8001a54:	4605      	mov	r5, r0
 8001a56:	f001 fb43 	bl	80030e0 <__aeabi_i2d>
 8001a5a:	4602      	mov	r2, r0
 8001a5c:	460b      	mov	r3, r1
 8001a5e:	4638      	mov	r0, r7
 8001a60:	4641      	mov	r1, r8
 8001a62:	f001 f9ef 	bl	8002e44 <__aeabi_dsub>
 8001a66:	3530      	adds	r5, #48	; 0x30
 8001a68:	fa5f f885 	uxtb.w	r8, r5
 8001a6c:	4606      	mov	r6, r0
 8001a6e:	460f      	mov	r7, r1
 8001a70:	4602      	mov	r2, r0
 8001a72:	460b      	mov	r3, r1
 8001a74:	f889 8000 	strb.w	r8, [r9]
 8001a78:	e9dd 0100 	ldrd	r0, r1, [sp]
 8001a7c:	f109 0501 	add.w	r5, r9, #1
 8001a80:	f001 fe24 	bl	80036cc <__aeabi_dcmpgt>
 8001a84:	2800      	cmp	r0, #0
 8001a86:	d15b      	bne.n	8001b40 <_dtoa_r+0x9b8>
 8001a88:	4632      	mov	r2, r6
 8001a8a:	463b      	mov	r3, r7
 8001a8c:	2000      	movs	r0, #0
 8001a8e:	4977      	ldr	r1, [pc, #476]	; (8001c6c <_dtoa_r+0xae4>)
 8001a90:	f001 f9d8 	bl	8002e44 <__aeabi_dsub>
 8001a94:	4602      	mov	r2, r0
 8001a96:	460b      	mov	r3, r1
 8001a98:	e9dd 0100 	ldrd	r0, r1, [sp]
 8001a9c:	f001 fe16 	bl	80036cc <__aeabi_dcmpgt>
 8001aa0:	2800      	cmp	r0, #0
 8001aa2:	f040 82a0 	bne.w	8001fe6 <_dtoa_r+0xe5e>
 8001aa6:	9a10      	ldr	r2, [sp, #64]	; 0x40
 8001aa8:	2a01      	cmp	r2, #1
 8001aaa:	f000 8171 	beq.w	8001d90 <_dtoa_r+0xc08>
 8001aae:	9b10      	ldr	r3, [sp, #64]	; 0x40
 8001ab0:	f8cd 9050 	str.w	r9, [sp, #80]	; 0x50
 8001ab4:	444b      	add	r3, r9
 8001ab6:	f8cd a040 	str.w	sl, [sp, #64]	; 0x40
 8001aba:	f8cd b054 	str.w	fp, [sp, #84]	; 0x54
 8001abe:	4699      	mov	r9, r3
 8001ac0:	46a0      	mov	r8, r4
 8001ac2:	e9dd ab00 	ldrd	sl, fp, [sp]
 8001ac6:	e00d      	b.n	8001ae4 <_dtoa_r+0x95c>
 8001ac8:	2000      	movs	r0, #0
 8001aca:	4968      	ldr	r1, [pc, #416]	; (8001c6c <_dtoa_r+0xae4>)
 8001acc:	f001 f9ba 	bl	8002e44 <__aeabi_dsub>
 8001ad0:	4652      	mov	r2, sl
 8001ad2:	465b      	mov	r3, fp
 8001ad4:	f001 fddc 	bl	8003690 <__aeabi_dcmplt>
 8001ad8:	2800      	cmp	r0, #0
 8001ada:	f040 827d 	bne.w	8001fd8 <_dtoa_r+0xe50>
 8001ade:	454d      	cmp	r5, r9
 8001ae0:	f000 814f 	beq.w	8001d82 <_dtoa_r+0xbfa>
 8001ae4:	4650      	mov	r0, sl
 8001ae6:	4659      	mov	r1, fp
 8001ae8:	2200      	movs	r2, #0
 8001aea:	4b63      	ldr	r3, [pc, #396]	; (8001c78 <_dtoa_r+0xaf0>)
 8001aec:	f001 fb5e 	bl	80031ac <__aeabi_dmul>
 8001af0:	2200      	movs	r2, #0
 8001af2:	4b61      	ldr	r3, [pc, #388]	; (8001c78 <_dtoa_r+0xaf0>)
 8001af4:	4682      	mov	sl, r0
 8001af6:	468b      	mov	fp, r1
 8001af8:	4630      	mov	r0, r6
 8001afa:	4639      	mov	r1, r7
 8001afc:	f001 fb56 	bl	80031ac <__aeabi_dmul>
 8001b00:	460f      	mov	r7, r1
 8001b02:	4606      	mov	r6, r0
 8001b04:	f001 fe02 	bl	800370c <__aeabi_d2iz>
 8001b08:	4604      	mov	r4, r0
 8001b0a:	f001 fae9 	bl	80030e0 <__aeabi_i2d>
 8001b0e:	4602      	mov	r2, r0
 8001b10:	460b      	mov	r3, r1
 8001b12:	4630      	mov	r0, r6
 8001b14:	4639      	mov	r1, r7
 8001b16:	f001 f995 	bl	8002e44 <__aeabi_dsub>
 8001b1a:	3430      	adds	r4, #48	; 0x30
 8001b1c:	b2e4      	uxtb	r4, r4
 8001b1e:	4652      	mov	r2, sl
 8001b20:	465b      	mov	r3, fp
 8001b22:	f805 4b01 	strb.w	r4, [r5], #1
 8001b26:	4606      	mov	r6, r0
 8001b28:	460f      	mov	r7, r1
 8001b2a:	f001 fdb1 	bl	8003690 <__aeabi_dcmplt>
 8001b2e:	4632      	mov	r2, r6
 8001b30:	463b      	mov	r3, r7
 8001b32:	2800      	cmp	r0, #0
 8001b34:	d0c8      	beq.n	8001ac8 <_dtoa_r+0x940>
 8001b36:	f8dd a040 	ldr.w	sl, [sp, #64]	; 0x40
 8001b3a:	f8dd 9050 	ldr.w	r9, [sp, #80]	; 0x50
 8001b3e:	4644      	mov	r4, r8
 8001b40:	9b13      	ldr	r3, [sp, #76]	; 0x4c
 8001b42:	9305      	str	r3, [sp, #20]
 8001b44:	e4ff      	b.n	8001546 <_dtoa_r+0x3be>
 8001b46:	2300      	movs	r3, #0
 8001b48:	9309      	str	r3, [sp, #36]	; 0x24
 8001b4a:	f1bb 0f00 	cmp.w	fp, #0
 8001b4e:	f340 80db 	ble.w	8001d08 <_dtoa_r+0xb80>
 8001b52:	465f      	mov	r7, fp
 8001b54:	465d      	mov	r5, fp
 8001b56:	f8cd b034 	str.w	fp, [sp, #52]	; 0x34
 8001b5a:	f8cd b020 	str.w	fp, [sp, #32]
 8001b5e:	e6de      	b.n	800191e <_dtoa_r+0x796>
 8001b60:	2301      	movs	r3, #1
 8001b62:	9309      	str	r3, [sp, #36]	; 0x24
 8001b64:	e7f1      	b.n	8001b4a <_dtoa_r+0x9c2>
 8001b66:	9b00      	ldr	r3, [sp, #0]
 8001b68:	2b00      	cmp	r3, #0
 8001b6a:	f47f ad8a 	bne.w	8001682 <_dtoa_r+0x4fa>
 8001b6e:	e9dd 1200 	ldrd	r1, r2, [sp]
 8001b72:	f3c2 0313 	ubfx	r3, r2, #0, #20
 8001b76:	2b00      	cmp	r3, #0
 8001b78:	f47f ad9e 	bne.w	80016b8 <_dtoa_r+0x530>
 8001b7c:	f022 4700 	bic.w	r7, r2, #2147483648	; 0x80000000
 8001b80:	0d3f      	lsrs	r7, r7, #20
 8001b82:	053f      	lsls	r7, r7, #20
 8001b84:	2f00      	cmp	r7, #0
 8001b86:	f000 8232 	beq.w	8001fee <_dtoa_r+0xe66>
 8001b8a:	9b07      	ldr	r3, [sp, #28]
 8001b8c:	3301      	adds	r3, #1
 8001b8e:	9307      	str	r3, [sp, #28]
 8001b90:	9b06      	ldr	r3, [sp, #24]
 8001b92:	3301      	adds	r3, #1
 8001b94:	9306      	str	r3, [sp, #24]
 8001b96:	2301      	movs	r3, #1
 8001b98:	930a      	str	r3, [sp, #40]	; 0x28
 8001b9a:	e58f      	b.n	80016bc <_dtoa_r+0x534>
 8001b9c:	9b08      	ldr	r3, [sp, #32]
 8001b9e:	2b00      	cmp	r3, #0
 8001ba0:	f73f ac22 	bgt.w	80013e8 <_dtoa_r+0x260>
 8001ba4:	f040 809d 	bne.w	8001ce2 <_dtoa_r+0xb5a>
 8001ba8:	2200      	movs	r2, #0
 8001baa:	4b34      	ldr	r3, [pc, #208]	; (8001c7c <_dtoa_r+0xaf4>)
 8001bac:	e9dd 0102 	ldrd	r0, r1, [sp, #8]
 8001bb0:	f001 fafc 	bl	80031ac <__aeabi_dmul>
 8001bb4:	e9dd 2300 	ldrd	r2, r3, [sp]
 8001bb8:	f001 fd7e 	bl	80036b8 <__aeabi_dcmpge>
 8001bbc:	f8dd 8020 	ldr.w	r8, [sp, #32]
 8001bc0:	4646      	mov	r6, r8
 8001bc2:	2800      	cmp	r0, #0
 8001bc4:	d03e      	beq.n	8001c44 <_dtoa_r+0xabc>
 8001bc6:	ea6f 030b 	mvn.w	r3, fp
 8001bca:	9305      	str	r3, [sp, #20]
 8001bcc:	464d      	mov	r5, r9
 8001bce:	4641      	mov	r1, r8
 8001bd0:	4620      	mov	r0, r4
 8001bd2:	f000 fb39 	bl	8002248 <_Bfree>
 8001bd6:	2e00      	cmp	r6, #0
 8001bd8:	f43f acb5 	beq.w	8001546 <_dtoa_r+0x3be>
 8001bdc:	e68e      	b.n	80018fc <_dtoa_r+0x774>
 8001bde:	9a0a      	ldr	r2, [sp, #40]	; 0x28
 8001be0:	4651      	mov	r1, sl
 8001be2:	4620      	mov	r0, r4
 8001be4:	f000 fcc4 	bl	8002570 <__pow5mult>
 8001be8:	4682      	mov	sl, r0
 8001bea:	e538      	b.n	800165e <_dtoa_r+0x4d6>
 8001bec:	9a12      	ldr	r2, [sp, #72]	; 0x48
 8001bee:	2a00      	cmp	r2, #0
 8001bf0:	f000 819a 	beq.w	8001f28 <_dtoa_r+0xda0>
 8001bf4:	f203 4333 	addw	r3, r3, #1075	; 0x433
 8001bf8:	9f0a      	ldr	r7, [sp, #40]	; 0x28
 8001bfa:	9d07      	ldr	r5, [sp, #28]
 8001bfc:	e4ca      	b.n	8001594 <_dtoa_r+0x40c>
 8001bfe:	9b00      	ldr	r3, [sp, #0]
 8001c00:	2b00      	cmp	r3, #0
 8001c02:	f47f ad59 	bne.w	80016b8 <_dtoa_r+0x530>
 8001c06:	e7b2      	b.n	8001b6e <_dtoa_r+0x9e6>
 8001c08:	f000 fb3a 	bl	8002280 <__multadd>
 8001c0c:	465f      	mov	r7, fp
 8001c0e:	4606      	mov	r6, r0
 8001c10:	4680      	mov	r8, r0
 8001c12:	e5a4      	b.n	800175e <_dtoa_r+0x5d6>
 8001c14:	4620      	mov	r0, r4
 8001c16:	f000 fb17 	bl	8002248 <_Bfree>
 8001c1a:	2201      	movs	r2, #1
 8001c1c:	e5c9      	b.n	80017b2 <_dtoa_r+0x62a>
 8001c1e:	9b02      	ldr	r3, [sp, #8]
 8001c20:	2b02      	cmp	r3, #2
 8001c22:	f77f ad7d 	ble.w	8001720 <_dtoa_r+0x598>
 8001c26:	9b08      	ldr	r3, [sp, #32]
 8001c28:	2b00      	cmp	r3, #0
 8001c2a:	d1cc      	bne.n	8001bc6 <_dtoa_r+0xa3e>
 8001c2c:	4641      	mov	r1, r8
 8001c2e:	2205      	movs	r2, #5
 8001c30:	4620      	mov	r0, r4
 8001c32:	f000 fb25 	bl	8002280 <__multadd>
 8001c36:	4601      	mov	r1, r0
 8001c38:	4680      	mov	r8, r0
 8001c3a:	4650      	mov	r0, sl
 8001c3c:	f000 fd48 	bl	80026d0 <__mcmp>
 8001c40:	2800      	cmp	r0, #0
 8001c42:	ddc0      	ble.n	8001bc6 <_dtoa_r+0xa3e>
 8001c44:	9a05      	ldr	r2, [sp, #20]
 8001c46:	2331      	movs	r3, #49	; 0x31
 8001c48:	3201      	adds	r2, #1
 8001c4a:	9205      	str	r2, [sp, #20]
 8001c4c:	f889 3000 	strb.w	r3, [r9]
 8001c50:	f109 0501 	add.w	r5, r9, #1
 8001c54:	e7bb      	b.n	8001bce <_dtoa_r+0xa46>
 8001c56:	9a05      	ldr	r2, [sp, #20]
 8001c58:	2331      	movs	r3, #49	; 0x31
 8001c5a:	3201      	adds	r2, #1
 8001c5c:	9205      	str	r2, [sp, #20]
 8001c5e:	f889 3000 	strb.w	r3, [r9]
 8001c62:	e63b      	b.n	80018dc <_dtoa_r+0x754>
 8001c64:	08004710 	.word	0x08004710
 8001c68:	08004800 	.word	0x08004800
 8001c6c:	3ff00000 	.word	0x3ff00000
 8001c70:	401c0000 	.word	0x401c0000
 8001c74:	3fe00000 	.word	0x3fe00000
 8001c78:	40240000 	.word	0x40240000
 8001c7c:	40140000 	.word	0x40140000
 8001c80:	2301      	movs	r3, #1
 8001c82:	9309      	str	r3, [sp, #36]	; 0x24
 8001c84:	e641      	b.n	800190a <_dtoa_r+0x782>
 8001c86:	f8dd a000 	ldr.w	sl, [sp]
 8001c8a:	9c06      	ldr	r4, [sp, #24]
 8001c8c:	e45b      	b.n	8001546 <_dtoa_r+0x3be>
 8001c8e:	9800      	ldr	r0, [sp, #0]
 8001c90:	f001 fa26 	bl	80030e0 <__aeabi_i2d>
 8001c94:	463a      	mov	r2, r7
 8001c96:	4643      	mov	r3, r8
 8001c98:	f001 fa88 	bl	80031ac <__aeabi_dmul>
 8001c9c:	4bbe      	ldr	r3, [pc, #760]	; (8001f98 <_dtoa_r+0xe10>)
 8001c9e:	2200      	movs	r2, #0
 8001ca0:	f001 f8d2 	bl	8002e48 <__adddf3>
 8001ca4:	f1a1 7350 	sub.w	r3, r1, #54525952	; 0x3400000
 8001ca8:	e9cd 0100 	strd	r0, r1, [sp]
 8001cac:	9301      	str	r3, [sp, #4]
 8001cae:	4638      	mov	r0, r7
 8001cb0:	2200      	movs	r2, #0
 8001cb2:	4bba      	ldr	r3, [pc, #744]	; (8001f9c <_dtoa_r+0xe14>)
 8001cb4:	4641      	mov	r1, r8
 8001cb6:	f001 f8c5 	bl	8002e44 <__aeabi_dsub>
 8001cba:	e9dd 2300 	ldrd	r2, r3, [sp]
 8001cbe:	4606      	mov	r6, r0
 8001cc0:	460f      	mov	r7, r1
 8001cc2:	f001 fd03 	bl	80036cc <__aeabi_dcmpgt>
 8001cc6:	2800      	cmp	r0, #0
 8001cc8:	f040 80cb 	bne.w	8001e62 <_dtoa_r+0xcda>
 8001ccc:	e9dd 0100 	ldrd	r0, r1, [sp]
 8001cd0:	4602      	mov	r2, r0
 8001cd2:	f101 4300 	add.w	r3, r1, #2147483648	; 0x80000000
 8001cd6:	4630      	mov	r0, r6
 8001cd8:	4639      	mov	r1, r7
 8001cda:	f001 fcd9 	bl	8003690 <__aeabi_dcmplt>
 8001cde:	2800      	cmp	r0, #0
 8001ce0:	d056      	beq.n	8001d90 <_dtoa_r+0xc08>
 8001ce2:	f04f 0800 	mov.w	r8, #0
 8001ce6:	4646      	mov	r6, r8
 8001ce8:	e76d      	b.n	8001bc6 <_dtoa_r+0xa3e>
 8001cea:	9b0a      	ldr	r3, [sp, #40]	; 0x28
 8001cec:	9a0b      	ldr	r2, [sp, #44]	; 0x2c
 8001cee:	970a      	str	r7, [sp, #40]	; 0x28
 8001cf0:	1afb      	subs	r3, r7, r3
 8001cf2:	441a      	add	r2, r3
 8001cf4:	920b      	str	r2, [sp, #44]	; 0x2c
 8001cf6:	2700      	movs	r7, #0
 8001cf8:	e446      	b.n	8001588 <_dtoa_r+0x400>
 8001cfa:	ed9d 7b0e 	vldr	d7, [sp, #56]	; 0x38
 8001cfe:	2302      	movs	r3, #2
 8001d00:	ed8d 7b10 	vstr	d7, [sp, #64]	; 0x40
 8001d04:	9300      	str	r3, [sp, #0]
 8001d06:	e648      	b.n	800199a <_dtoa_r+0x812>
 8001d08:	6a65      	ldr	r5, [r4, #36]	; 0x24
 8001d0a:	2100      	movs	r1, #0
 8001d0c:	6069      	str	r1, [r5, #4]
 8001d0e:	4620      	mov	r0, r4
 8001d10:	f000 fa66 	bl	80021e0 <_Balloc>
 8001d14:	6a63      	ldr	r3, [r4, #36]	; 0x24
 8001d16:	6028      	str	r0, [r5, #0]
 8001d18:	2201      	movs	r2, #1
 8001d1a:	9208      	str	r2, [sp, #32]
 8001d1c:	f8d3 9000 	ldr.w	r9, [r3]
 8001d20:	920d      	str	r2, [sp, #52]	; 0x34
 8001d22:	4693      	mov	fp, r2
 8001d24:	e615      	b.n	8001952 <_dtoa_r+0x7ca>
 8001d26:	2a00      	cmp	r2, #0
 8001d28:	f8cd 8018 	str.w	r8, [sp, #24]
 8001d2c:	46c8      	mov	r8, r9
 8001d2e:	f8dd 9028 	ldr.w	r9, [sp, #40]	; 0x28
 8001d32:	dd12      	ble.n	8001d5a <_dtoa_r+0xbd2>
 8001d34:	4651      	mov	r1, sl
 8001d36:	2201      	movs	r2, #1
 8001d38:	4620      	mov	r0, r4
 8001d3a:	f000 fc75 	bl	8002628 <__lshift>
 8001d3e:	4641      	mov	r1, r8
 8001d40:	4682      	mov	sl, r0
 8001d42:	f000 fcc5 	bl	80026d0 <__mcmp>
 8001d46:	2800      	cmp	r0, #0
 8001d48:	f340 8161 	ble.w	800200e <_dtoa_r+0xe86>
 8001d4c:	9b00      	ldr	r3, [sp, #0]
 8001d4e:	2b39      	cmp	r3, #57	; 0x39
 8001d50:	f000 811c 	beq.w	8001f8c <_dtoa_r+0xe04>
 8001d54:	f105 0331 	add.w	r3, r5, #49	; 0x31
 8001d58:	9300      	str	r3, [sp, #0]
 8001d5a:	f89d 3000 	ldrb.w	r3, [sp]
 8001d5e:	703b      	strb	r3, [r7, #0]
 8001d60:	46b3      	mov	fp, r6
 8001d62:	1c7d      	adds	r5, r7, #1
 8001d64:	9e06      	ldr	r6, [sp, #24]
 8001d66:	e5b9      	b.n	80018dc <_dtoa_r+0x754>
 8001d68:	d104      	bne.n	8001d74 <_dtoa_r+0xbec>
 8001d6a:	9b00      	ldr	r3, [sp, #0]
 8001d6c:	07db      	lsls	r3, r3, #31
 8001d6e:	d501      	bpl.n	8001d74 <_dtoa_r+0xbec>
 8001d70:	e5a5      	b.n	80018be <_dtoa_r+0x736>
 8001d72:	4615      	mov	r5, r2
 8001d74:	f815 3c01 	ldrb.w	r3, [r5, #-1]
 8001d78:	2b30      	cmp	r3, #48	; 0x30
 8001d7a:	f105 32ff 	add.w	r2, r5, #4294967295	; 0xffffffff
 8001d7e:	d0f8      	beq.n	8001d72 <_dtoa_r+0xbea>
 8001d80:	e5ac      	b.n	80018dc <_dtoa_r+0x754>
 8001d82:	f8dd a040 	ldr.w	sl, [sp, #64]	; 0x40
 8001d86:	f8dd 9050 	ldr.w	r9, [sp, #80]	; 0x50
 8001d8a:	f8dd b054 	ldr.w	fp, [sp, #84]	; 0x54
 8001d8e:	4644      	mov	r4, r8
 8001d90:	e9dd 230e 	ldrd	r2, r3, [sp, #56]	; 0x38
 8001d94:	e9cd 2300 	strd	r2, r3, [sp]
 8001d98:	f7ff bb13 	b.w	80013c2 <_dtoa_r+0x23a>
 8001d9c:	f000 80cb 	beq.w	8001f36 <_dtoa_r+0xdae>
 8001da0:	9b05      	ldr	r3, [sp, #20]
 8001da2:	425d      	negs	r5, r3
 8001da4:	4b7e      	ldr	r3, [pc, #504]	; (8001fa0 <_dtoa_r+0xe18>)
 8001da6:	f005 020f 	and.w	r2, r5, #15
 8001daa:	eb03 03c2 	add.w	r3, r3, r2, lsl #3
 8001dae:	e9d3 2300 	ldrd	r2, r3, [r3]
 8001db2:	e9dd 010e 	ldrd	r0, r1, [sp, #56]	; 0x38
 8001db6:	f001 f9f9 	bl	80031ac <__aeabi_dmul>
 8001dba:	112d      	asrs	r5, r5, #4
 8001dbc:	4607      	mov	r7, r0
 8001dbe:	4688      	mov	r8, r1
 8001dc0:	f000 813e 	beq.w	8002040 <_dtoa_r+0xeb8>
 8001dc4:	2302      	movs	r3, #2
 8001dc6:	4e77      	ldr	r6, [pc, #476]	; (8001fa4 <_dtoa_r+0xe1c>)
 8001dc8:	f8cd b040 	str.w	fp, [sp, #64]	; 0x40
 8001dcc:	46a3      	mov	fp, r4
 8001dce:	461c      	mov	r4, r3
 8001dd0:	07e9      	lsls	r1, r5, #31
 8001dd2:	d508      	bpl.n	8001de6 <_dtoa_r+0xc5e>
 8001dd4:	4638      	mov	r0, r7
 8001dd6:	4641      	mov	r1, r8
 8001dd8:	e9d6 2300 	ldrd	r2, r3, [r6]
 8001ddc:	f001 f9e6 	bl	80031ac <__aeabi_dmul>
 8001de0:	3401      	adds	r4, #1
 8001de2:	4607      	mov	r7, r0
 8001de4:	4688      	mov	r8, r1
 8001de6:	106d      	asrs	r5, r5, #1
 8001de8:	f106 0608 	add.w	r6, r6, #8
 8001dec:	d1f0      	bne.n	8001dd0 <_dtoa_r+0xc48>
 8001dee:	9400      	str	r4, [sp, #0]
 8001df0:	465c      	mov	r4, fp
 8001df2:	f8dd b040 	ldr.w	fp, [sp, #64]	; 0x40
 8001df6:	e5f2      	b.n	80019de <_dtoa_r+0x856>
 8001df8:	2230      	movs	r2, #48	; 0x30
 8001dfa:	f889 2000 	strb.w	r2, [r9]
 8001dfe:	9a05      	ldr	r2, [sp, #20]
 8001e00:	f815 8c01 	ldrb.w	r8, [r5, #-1]
 8001e04:	3201      	adds	r2, #1
 8001e06:	9205      	str	r2, [sp, #20]
 8001e08:	f7ff bb99 	b.w	800153e <_dtoa_r+0x3b6>
 8001e0c:	6871      	ldr	r1, [r6, #4]
 8001e0e:	4620      	mov	r0, r4
 8001e10:	f000 f9e6 	bl	80021e0 <_Balloc>
 8001e14:	6933      	ldr	r3, [r6, #16]
 8001e16:	3302      	adds	r3, #2
 8001e18:	009a      	lsls	r2, r3, #2
 8001e1a:	4605      	mov	r5, r0
 8001e1c:	f106 010c 	add.w	r1, r6, #12
 8001e20:	300c      	adds	r0, #12
 8001e22:	f7fe fa47 	bl	80002b4 <memcpy>
 8001e26:	4629      	mov	r1, r5
 8001e28:	2201      	movs	r2, #1
 8001e2a:	4620      	mov	r0, r4
 8001e2c:	f000 fbfc 	bl	8002628 <__lshift>
 8001e30:	9006      	str	r0, [sp, #24]
 8001e32:	e486      	b.n	8001742 <_dtoa_r+0x5ba>
 8001e34:	9b00      	ldr	r3, [sp, #0]
 8001e36:	f8cd 8018 	str.w	r8, [sp, #24]
 8001e3a:	2b39      	cmp	r3, #57	; 0x39
 8001e3c:	46c8      	mov	r8, r9
 8001e3e:	f8dd 9028 	ldr.w	r9, [sp, #40]	; 0x28
 8001e42:	f000 80a3 	beq.w	8001f8c <_dtoa_r+0xe04>
 8001e46:	9b00      	ldr	r3, [sp, #0]
 8001e48:	3301      	adds	r3, #1
 8001e4a:	46b3      	mov	fp, r6
 8001e4c:	703b      	strb	r3, [r7, #0]
 8001e4e:	1c7d      	adds	r5, r7, #1
 8001e50:	9e06      	ldr	r6, [sp, #24]
 8001e52:	e543      	b.n	80018dc <_dtoa_r+0x754>
 8001e54:	4643      	mov	r3, r8
 8001e56:	46b3      	mov	fp, r6
 8001e58:	46c8      	mov	r8, r9
 8001e5a:	461e      	mov	r6, r3
 8001e5c:	f8dd 9028 	ldr.w	r9, [sp, #40]	; 0x28
 8001e60:	e521      	b.n	80018a6 <_dtoa_r+0x71e>
 8001e62:	f04f 0800 	mov.w	r8, #0
 8001e66:	4646      	mov	r6, r8
 8001e68:	e6ec      	b.n	8001c44 <_dtoa_r+0xabc>
 8001e6a:	9b10      	ldr	r3, [sp, #64]	; 0x40
 8001e6c:	494c      	ldr	r1, [pc, #304]	; (8001fa0 <_dtoa_r+0xe18>)
 8001e6e:	eb01 01c3 	add.w	r1, r1, r3, lsl #3
 8001e72:	e951 0102 	ldrd	r0, r1, [r1, #-8]
 8001e76:	e9dd 2300 	ldrd	r2, r3, [sp]
 8001e7a:	f001 f997 	bl	80031ac <__aeabi_dmul>
 8001e7e:	e9cd 0100 	strd	r0, r1, [sp]
 8001e82:	4638      	mov	r0, r7
 8001e84:	4641      	mov	r1, r8
 8001e86:	f001 fc41 	bl	800370c <__aeabi_d2iz>
 8001e8a:	4605      	mov	r5, r0
 8001e8c:	f001 f928 	bl	80030e0 <__aeabi_i2d>
 8001e90:	460b      	mov	r3, r1
 8001e92:	4602      	mov	r2, r0
 8001e94:	4641      	mov	r1, r8
 8001e96:	4638      	mov	r0, r7
 8001e98:	f000 ffd4 	bl	8002e44 <__aeabi_dsub>
 8001e9c:	9b10      	ldr	r3, [sp, #64]	; 0x40
 8001e9e:	3530      	adds	r5, #48	; 0x30
 8001ea0:	2b01      	cmp	r3, #1
 8001ea2:	f889 5000 	strb.w	r5, [r9]
 8001ea6:	4606      	mov	r6, r0
 8001ea8:	460f      	mov	r7, r1
 8001eaa:	f109 0501 	add.w	r5, r9, #1
 8001eae:	d023      	beq.n	8001ef8 <_dtoa_r+0xd70>
 8001eb0:	9b10      	ldr	r3, [sp, #64]	; 0x40
 8001eb2:	f8cd a040 	str.w	sl, [sp, #64]	; 0x40
 8001eb6:	444b      	add	r3, r9
 8001eb8:	46ca      	mov	sl, r9
 8001eba:	4699      	mov	r9, r3
 8001ebc:	2200      	movs	r2, #0
 8001ebe:	4b3a      	ldr	r3, [pc, #232]	; (8001fa8 <_dtoa_r+0xe20>)
 8001ec0:	4630      	mov	r0, r6
 8001ec2:	4639      	mov	r1, r7
 8001ec4:	f001 f972 	bl	80031ac <__aeabi_dmul>
 8001ec8:	460f      	mov	r7, r1
 8001eca:	4606      	mov	r6, r0
 8001ecc:	f001 fc1e 	bl	800370c <__aeabi_d2iz>
 8001ed0:	4680      	mov	r8, r0
 8001ed2:	f001 f905 	bl	80030e0 <__aeabi_i2d>
 8001ed6:	f108 0830 	add.w	r8, r8, #48	; 0x30
 8001eda:	4602      	mov	r2, r0
 8001edc:	460b      	mov	r3, r1
 8001ede:	4630      	mov	r0, r6
 8001ee0:	4639      	mov	r1, r7
 8001ee2:	f000 ffaf 	bl	8002e44 <__aeabi_dsub>
 8001ee6:	f805 8b01 	strb.w	r8, [r5], #1
 8001eea:	454d      	cmp	r5, r9
 8001eec:	4606      	mov	r6, r0
 8001eee:	460f      	mov	r7, r1
 8001ef0:	d1e4      	bne.n	8001ebc <_dtoa_r+0xd34>
 8001ef2:	46d1      	mov	r9, sl
 8001ef4:	f8dd a040 	ldr.w	sl, [sp, #64]	; 0x40
 8001ef8:	4b2c      	ldr	r3, [pc, #176]	; (8001fac <_dtoa_r+0xe24>)
 8001efa:	2200      	movs	r2, #0
 8001efc:	e9dd 0100 	ldrd	r0, r1, [sp]
 8001f00:	f000 ffa2 	bl	8002e48 <__adddf3>
 8001f04:	4632      	mov	r2, r6
 8001f06:	463b      	mov	r3, r7
 8001f08:	f001 fbc2 	bl	8003690 <__aeabi_dcmplt>
 8001f0c:	2800      	cmp	r0, #0
 8001f0e:	d04f      	beq.n	8001fb0 <_dtoa_r+0xe28>
 8001f10:	9b13      	ldr	r3, [sp, #76]	; 0x4c
 8001f12:	9305      	str	r3, [sp, #20]
 8001f14:	f815 8c01 	ldrb.w	r8, [r5, #-1]
 8001f18:	f7ff bb05 	b.w	8001526 <_dtoa_r+0x39e>
 8001f1c:	9b07      	ldr	r3, [sp, #28]
 8001f1e:	9a08      	ldr	r2, [sp, #32]
 8001f20:	1a9d      	subs	r5, r3, r2
 8001f22:	2300      	movs	r3, #0
 8001f24:	f7ff bb36 	b.w	8001594 <_dtoa_r+0x40c>
 8001f28:	9b16      	ldr	r3, [sp, #88]	; 0x58
 8001f2a:	9f0a      	ldr	r7, [sp, #40]	; 0x28
 8001f2c:	9d07      	ldr	r5, [sp, #28]
 8001f2e:	f1c3 0336 	rsb	r3, r3, #54	; 0x36
 8001f32:	f7ff bb2f 	b.w	8001594 <_dtoa_r+0x40c>
 8001f36:	2302      	movs	r3, #2
 8001f38:	e9dd 780e 	ldrd	r7, r8, [sp, #56]	; 0x38
 8001f3c:	9300      	str	r3, [sp, #0]
 8001f3e:	e54e      	b.n	80019de <_dtoa_r+0x856>
 8001f40:	9b08      	ldr	r3, [sp, #32]
 8001f42:	2b00      	cmp	r3, #0
 8001f44:	f43f aea3 	beq.w	8001c8e <_dtoa_r+0xb06>
 8001f48:	9d0d      	ldr	r5, [sp, #52]	; 0x34
 8001f4a:	2d00      	cmp	r5, #0
 8001f4c:	f77f af20 	ble.w	8001d90 <_dtoa_r+0xc08>
 8001f50:	2200      	movs	r2, #0
 8001f52:	4b15      	ldr	r3, [pc, #84]	; (8001fa8 <_dtoa_r+0xe20>)
 8001f54:	4638      	mov	r0, r7
 8001f56:	4641      	mov	r1, r8
 8001f58:	f001 f928 	bl	80031ac <__aeabi_dmul>
 8001f5c:	4607      	mov	r7, r0
 8001f5e:	9800      	ldr	r0, [sp, #0]
 8001f60:	4688      	mov	r8, r1
 8001f62:	3001      	adds	r0, #1
 8001f64:	f001 f8bc 	bl	80030e0 <__aeabi_i2d>
 8001f68:	463a      	mov	r2, r7
 8001f6a:	4643      	mov	r3, r8
 8001f6c:	f001 f91e 	bl	80031ac <__aeabi_dmul>
 8001f70:	4b09      	ldr	r3, [pc, #36]	; (8001f98 <_dtoa_r+0xe10>)
 8001f72:	2200      	movs	r2, #0
 8001f74:	f000 ff68 	bl	8002e48 <__adddf3>
 8001f78:	f1a1 7350 	sub.w	r3, r1, #54525952	; 0x3400000
 8001f7c:	e9cd 0100 	strd	r0, r1, [sp]
 8001f80:	9301      	str	r3, [sp, #4]
 8001f82:	9b05      	ldr	r3, [sp, #20]
 8001f84:	9510      	str	r5, [sp, #64]	; 0x40
 8001f86:	3b01      	subs	r3, #1
 8001f88:	9313      	str	r3, [sp, #76]	; 0x4c
 8001f8a:	e54b      	b.n	8001a24 <_dtoa_r+0x89c>
 8001f8c:	2239      	movs	r2, #57	; 0x39
 8001f8e:	46b3      	mov	fp, r6
 8001f90:	703a      	strb	r2, [r7, #0]
 8001f92:	9e06      	ldr	r6, [sp, #24]
 8001f94:	1c7d      	adds	r5, r7, #1
 8001f96:	e494      	b.n	80018c2 <_dtoa_r+0x73a>
 8001f98:	401c0000 	.word	0x401c0000
 8001f9c:	40140000 	.word	0x40140000
 8001fa0:	08004710 	.word	0x08004710
 8001fa4:	08004800 	.word	0x08004800
 8001fa8:	40240000 	.word	0x40240000
 8001fac:	3fe00000 	.word	0x3fe00000
 8001fb0:	e9dd 2300 	ldrd	r2, r3, [sp]
 8001fb4:	2000      	movs	r0, #0
 8001fb6:	4937      	ldr	r1, [pc, #220]	; (8002094 <_dtoa_r+0xf0c>)
 8001fb8:	f000 ff44 	bl	8002e44 <__aeabi_dsub>
 8001fbc:	4632      	mov	r2, r6
 8001fbe:	463b      	mov	r3, r7
 8001fc0:	f001 fb84 	bl	80036cc <__aeabi_dcmpgt>
 8001fc4:	b908      	cbnz	r0, 8001fca <_dtoa_r+0xe42>
 8001fc6:	e6e3      	b.n	8001d90 <_dtoa_r+0xc08>
 8001fc8:	4615      	mov	r5, r2
 8001fca:	f815 3c01 	ldrb.w	r3, [r5, #-1]
 8001fce:	2b30      	cmp	r3, #48	; 0x30
 8001fd0:	f105 32ff 	add.w	r2, r5, #4294967295	; 0xffffffff
 8001fd4:	d0f8      	beq.n	8001fc8 <_dtoa_r+0xe40>
 8001fd6:	e5b3      	b.n	8001b40 <_dtoa_r+0x9b8>
 8001fd8:	4643      	mov	r3, r8
 8001fda:	f8dd a040 	ldr.w	sl, [sp, #64]	; 0x40
 8001fde:	f8dd 9050 	ldr.w	r9, [sp, #80]	; 0x50
 8001fe2:	46a0      	mov	r8, r4
 8001fe4:	461c      	mov	r4, r3
 8001fe6:	9b13      	ldr	r3, [sp, #76]	; 0x4c
 8001fe8:	9305      	str	r3, [sp, #20]
 8001fea:	f7ff ba9c 	b.w	8001526 <_dtoa_r+0x39e>
 8001fee:	970a      	str	r7, [sp, #40]	; 0x28
 8001ff0:	f7ff bb64 	b.w	80016bc <_dtoa_r+0x534>
 8001ff4:	9b00      	ldr	r3, [sp, #0]
 8001ff6:	f8cd 8018 	str.w	r8, [sp, #24]
 8001ffa:	2b39      	cmp	r3, #57	; 0x39
 8001ffc:	46c8      	mov	r8, r9
 8001ffe:	f8dd 9028 	ldr.w	r9, [sp, #40]	; 0x28
 8002002:	d0c3      	beq.n	8001f8c <_dtoa_r+0xe04>
 8002004:	f1bb 0f00 	cmp.w	fp, #0
 8002008:	f73f aea4 	bgt.w	8001d54 <_dtoa_r+0xbcc>
 800200c:	e6a5      	b.n	8001d5a <_dtoa_r+0xbd2>
 800200e:	f47f aea4 	bne.w	8001d5a <_dtoa_r+0xbd2>
 8002012:	9b00      	ldr	r3, [sp, #0]
 8002014:	07da      	lsls	r2, r3, #31
 8002016:	f57f aea0 	bpl.w	8001d5a <_dtoa_r+0xbd2>
 800201a:	e697      	b.n	8001d4c <_dtoa_r+0xbc4>
 800201c:	4631      	mov	r1, r6
 800201e:	2300      	movs	r3, #0
 8002020:	220a      	movs	r2, #10
 8002022:	4620      	mov	r0, r4
 8002024:	f000 f92c 	bl	8002280 <__multadd>
 8002028:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800202a:	2b00      	cmp	r3, #0
 800202c:	4606      	mov	r6, r0
 800202e:	dd0a      	ble.n	8002046 <_dtoa_r+0xebe>
 8002030:	9308      	str	r3, [sp, #32]
 8002032:	f7ff bb79 	b.w	8001728 <_dtoa_r+0x5a0>
 8002036:	9b02      	ldr	r3, [sp, #8]
 8002038:	2b02      	cmp	r3, #2
 800203a:	dc27      	bgt.n	800208c <_dtoa_r+0xf04>
 800203c:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800203e:	e40e      	b.n	800185e <_dtoa_r+0x6d6>
 8002040:	2302      	movs	r3, #2
 8002042:	9300      	str	r3, [sp, #0]
 8002044:	e4cb      	b.n	80019de <_dtoa_r+0x856>
 8002046:	9b02      	ldr	r3, [sp, #8]
 8002048:	2b02      	cmp	r3, #2
 800204a:	dc1f      	bgt.n	800208c <_dtoa_r+0xf04>
 800204c:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800204e:	e7ef      	b.n	8002030 <_dtoa_r+0xea8>
 8002050:	f43f ab4c 	beq.w	80016ec <_dtoa_r+0x564>
 8002054:	f1c0 003c 	rsb	r0, r0, #60	; 0x3c
 8002058:	f7ff bbe0 	b.w	800181c <_dtoa_r+0x694>
 800205c:	f04f 0801 	mov.w	r8, #1
 8002060:	f7ff b96f 	b.w	8001342 <_dtoa_r+0x1ba>
 8002064:	6a66      	ldr	r6, [r4, #36]	; 0x24
 8002066:	2500      	movs	r5, #0
 8002068:	6075      	str	r5, [r6, #4]
 800206a:	4629      	mov	r1, r5
 800206c:	4620      	mov	r0, r4
 800206e:	f000 f8b7 	bl	80021e0 <_Balloc>
 8002072:	6a63      	ldr	r3, [r4, #36]	; 0x24
 8002074:	6030      	str	r0, [r6, #0]
 8002076:	f04f 32ff 	mov.w	r2, #4294967295	; 0xffffffff
 800207a:	f8d3 9000 	ldr.w	r9, [r3]
 800207e:	9208      	str	r2, [sp, #32]
 8002080:	2301      	movs	r3, #1
 8002082:	46ab      	mov	fp, r5
 8002084:	920d      	str	r2, [sp, #52]	; 0x34
 8002086:	9309      	str	r3, [sp, #36]	; 0x24
 8002088:	f7ff b99b 	b.w	80013c2 <_dtoa_r+0x23a>
 800208c:	9b0d      	ldr	r3, [sp, #52]	; 0x34
 800208e:	9308      	str	r3, [sp, #32]
 8002090:	e5c9      	b.n	8001c26 <_dtoa_r+0xa9e>
 8002092:	bf00      	nop
 8002094:	3fe00000 	.word	0x3fe00000

08002098 <_setlocale_r>:
 8002098:	b1b2      	cbz	r2, 80020c8 <_setlocale_r+0x30>
 800209a:	b510      	push	{r4, lr}
 800209c:	490b      	ldr	r1, [pc, #44]	; (80020cc <_setlocale_r+0x34>)
 800209e:	4610      	mov	r0, r2
 80020a0:	4614      	mov	r4, r2
 80020a2:	f000 fd61 	bl	8002b68 <strcmp>
 80020a6:	b908      	cbnz	r0, 80020ac <_setlocale_r+0x14>
 80020a8:	4809      	ldr	r0, [pc, #36]	; (80020d0 <_setlocale_r+0x38>)
 80020aa:	bd10      	pop	{r4, pc}
 80020ac:	4908      	ldr	r1, [pc, #32]	; (80020d0 <_setlocale_r+0x38>)
 80020ae:	4620      	mov	r0, r4
 80020b0:	f000 fd5a 	bl	8002b68 <strcmp>
 80020b4:	2800      	cmp	r0, #0
 80020b6:	d0f7      	beq.n	80020a8 <_setlocale_r+0x10>
 80020b8:	4620      	mov	r0, r4
 80020ba:	4906      	ldr	r1, [pc, #24]	; (80020d4 <_setlocale_r+0x3c>)
 80020bc:	f000 fd54 	bl	8002b68 <strcmp>
 80020c0:	2800      	cmp	r0, #0
 80020c2:	d0f1      	beq.n	80020a8 <_setlocale_r+0x10>
 80020c4:	2000      	movs	r0, #0
 80020c6:	bd10      	pop	{r4, pc}
 80020c8:	4801      	ldr	r0, [pc, #4]	; (80020d0 <_setlocale_r+0x38>)
 80020ca:	4770      	bx	lr
 80020cc:	08004878 	.word	0x08004878
 80020d0:	08004828 	.word	0x08004828
 80020d4:	08004850 	.word	0x08004850

080020d8 <__locale_charset>:
 80020d8:	4800      	ldr	r0, [pc, #0]	; (80020dc <__locale_charset+0x4>)
 80020da:	4770      	bx	lr
 80020dc:	20000064 	.word	0x20000064

080020e0 <__locale_mb_cur_max>:
 80020e0:	4b01      	ldr	r3, [pc, #4]	; (80020e8 <__locale_mb_cur_max+0x8>)
 80020e2:	6a18      	ldr	r0, [r3, #32]
 80020e4:	4770      	bx	lr
 80020e6:	bf00      	nop
 80020e8:	20000064 	.word	0x20000064

080020ec <__locale_msgcharset>:
 80020ec:	4800      	ldr	r0, [pc, #0]	; (80020f0 <__locale_msgcharset+0x4>)
 80020ee:	4770      	bx	lr
 80020f0:	20000088 	.word	0x20000088

080020f4 <__locale_cjk_lang>:
 80020f4:	2000      	movs	r0, #0
 80020f6:	4770      	bx	lr

080020f8 <_localeconv_r>:
 80020f8:	4800      	ldr	r0, [pc, #0]	; (80020fc <_localeconv_r+0x4>)
 80020fa:	4770      	bx	lr
 80020fc:	200000a8 	.word	0x200000a8

08002100 <setlocale>:
 8002100:	4b02      	ldr	r3, [pc, #8]	; (800210c <setlocale+0xc>)
 8002102:	460a      	mov	r2, r1
 8002104:	4601      	mov	r1, r0
 8002106:	6818      	ldr	r0, [r3, #0]
 8002108:	f7ff bfc6 	b.w	8002098 <_setlocale_r>
 800210c:	20000000 	.word	0x20000000

08002110 <localeconv>:
 8002110:	4800      	ldr	r0, [pc, #0]	; (8002114 <localeconv+0x4>)
 8002112:	4770      	bx	lr
 8002114:	200000a8 	.word	0x200000a8

08002118 <malloc>:
 8002118:	4b02      	ldr	r3, [pc, #8]	; (8002124 <malloc+0xc>)
 800211a:	4601      	mov	r1, r0
 800211c:	6818      	ldr	r0, [r3, #0]
 800211e:	f001 bd39 	b.w	8003b94 <_malloc_r>
 8002122:	bf00      	nop
 8002124:	20000000 	.word	0x20000000

08002128 <free>:
 8002128:	4b02      	ldr	r3, [pc, #8]	; (8002134 <free+0xc>)
 800212a:	4601      	mov	r1, r0
 800212c:	6818      	ldr	r0, [r3, #0]
 800212e:	f001 bd3e 	b.w	8003bae <_free_r>
 8002132:	bf00      	nop
 8002134:	20000000 	.word	0x20000000
	...

08002140 <memchr>:
 8002140:	f001 01ff 	and.w	r1, r1, #255	; 0xff
 8002144:	2a10      	cmp	r2, #16
 8002146:	db2b      	blt.n	80021a0 <memchr+0x60>
 8002148:	f010 0f07 	tst.w	r0, #7
 800214c:	d008      	beq.n	8002160 <memchr+0x20>
 800214e:	f810 3b01 	ldrb.w	r3, [r0], #1
 8002152:	3a01      	subs	r2, #1
 8002154:	428b      	cmp	r3, r1
 8002156:	d02d      	beq.n	80021b4 <memchr+0x74>
 8002158:	f010 0f07 	tst.w	r0, #7
 800215c:	b342      	cbz	r2, 80021b0 <memchr+0x70>
 800215e:	d1f6      	bne.n	800214e <memchr+0xe>
 8002160:	b4f0      	push	{r4, r5, r6, r7}
 8002162:	ea41 2101 	orr.w	r1, r1, r1, lsl #8
 8002166:	ea41 4101 	orr.w	r1, r1, r1, lsl #16
 800216a:	f022 0407 	bic.w	r4, r2, #7
 800216e:	f07f 0700 	mvns.w	r7, #0
 8002172:	2300      	movs	r3, #0
 8002174:	e8f0 5602 	ldrd	r5, r6, [r0], #8
 8002178:	3c08      	subs	r4, #8
 800217a:	ea85 0501 	eor.w	r5, r5, r1
 800217e:	ea86 0601 	eor.w	r6, r6, r1
 8002182:	fa85 f547 	uadd8	r5, r5, r7
 8002186:	faa3 f587 	sel	r5, r3, r7
 800218a:	fa86 f647 	uadd8	r6, r6, r7
 800218e:	faa5 f687 	sel	r6, r5, r7
 8002192:	b98e      	cbnz	r6, 80021b8 <memchr+0x78>
 8002194:	d1ee      	bne.n	8002174 <memchr+0x34>
 8002196:	bcf0      	pop	{r4, r5, r6, r7}
 8002198:	f001 01ff 	and.w	r1, r1, #255	; 0xff
 800219c:	f002 0207 	and.w	r2, r2, #7
 80021a0:	b132      	cbz	r2, 80021b0 <memchr+0x70>
 80021a2:	f810 3b01 	ldrb.w	r3, [r0], #1
 80021a6:	3a01      	subs	r2, #1
 80021a8:	ea83 0301 	eor.w	r3, r3, r1
 80021ac:	b113      	cbz	r3, 80021b4 <memchr+0x74>
 80021ae:	d1f8      	bne.n	80021a2 <memchr+0x62>
 80021b0:	2000      	movs	r0, #0
 80021b2:	4770      	bx	lr
 80021b4:	3801      	subs	r0, #1
 80021b6:	4770      	bx	lr
 80021b8:	2d00      	cmp	r5, #0
 80021ba:	bf06      	itte	eq
 80021bc:	4635      	moveq	r5, r6
 80021be:	3803      	subeq	r0, #3
 80021c0:	3807      	subne	r0, #7
 80021c2:	f015 0f01 	tst.w	r5, #1
 80021c6:	d107      	bne.n	80021d8 <memchr+0x98>
 80021c8:	3001      	adds	r0, #1
 80021ca:	f415 7f80 	tst.w	r5, #256	; 0x100
 80021ce:	bf02      	ittt	eq
 80021d0:	3001      	addeq	r0, #1
 80021d2:	f415 3fc0 	tsteq.w	r5, #98304	; 0x18000
 80021d6:	3001      	addeq	r0, #1
 80021d8:	bcf0      	pop	{r4, r5, r6, r7}
 80021da:	3801      	subs	r0, #1
 80021dc:	4770      	bx	lr
 80021de:	bf00      	nop

080021e0 <_Balloc>:
 80021e0:	b570      	push	{r4, r5, r6, lr}
 80021e2:	6a44      	ldr	r4, [r0, #36]	; 0x24
 80021e4:	4606      	mov	r6, r0
 80021e6:	460d      	mov	r5, r1
 80021e8:	b15c      	cbz	r4, 8002202 <_Balloc+0x22>
 80021ea:	68e3      	ldr	r3, [r4, #12]
 80021ec:	b193      	cbz	r3, 8002214 <_Balloc+0x34>
 80021ee:	f853 0025 	ldr.w	r0, [r3, r5, lsl #2]
 80021f2:	b1d8      	cbz	r0, 800222c <_Balloc+0x4c>
 80021f4:	6802      	ldr	r2, [r0, #0]
 80021f6:	f843 2025 	str.w	r2, [r3, r5, lsl #2]
 80021fa:	2300      	movs	r3, #0
 80021fc:	6103      	str	r3, [r0, #16]
 80021fe:	60c3      	str	r3, [r0, #12]
 8002200:	bd70      	pop	{r4, r5, r6, pc}
 8002202:	2010      	movs	r0, #16
 8002204:	f7ff ff88 	bl	8002118 <malloc>
 8002208:	6270      	str	r0, [r6, #36]	; 0x24
 800220a:	6044      	str	r4, [r0, #4]
 800220c:	6084      	str	r4, [r0, #8]
 800220e:	6004      	str	r4, [r0, #0]
 8002210:	60c4      	str	r4, [r0, #12]
 8002212:	4604      	mov	r4, r0
 8002214:	2221      	movs	r2, #33	; 0x21
 8002216:	2104      	movs	r1, #4
 8002218:	4630      	mov	r0, r6
 800221a:	f000 fc87 	bl	8002b2c <_calloc_r>
 800221e:	6a73      	ldr	r3, [r6, #36]	; 0x24
 8002220:	60e0      	str	r0, [r4, #12]
 8002222:	68db      	ldr	r3, [r3, #12]
 8002224:	2b00      	cmp	r3, #0
 8002226:	d1e2      	bne.n	80021ee <_Balloc+0xe>
 8002228:	2000      	movs	r0, #0
 800222a:	bd70      	pop	{r4, r5, r6, pc}
 800222c:	2101      	movs	r1, #1
 800222e:	fa01 f405 	lsl.w	r4, r1, r5
 8002232:	1d62      	adds	r2, r4, #5
 8002234:	4630      	mov	r0, r6
 8002236:	0092      	lsls	r2, r2, #2
 8002238:	f000 fc78 	bl	8002b2c <_calloc_r>
 800223c:	2800      	cmp	r0, #0
 800223e:	d0f3      	beq.n	8002228 <_Balloc+0x48>
 8002240:	6045      	str	r5, [r0, #4]
 8002242:	6084      	str	r4, [r0, #8]
 8002244:	e7d9      	b.n	80021fa <_Balloc+0x1a>
 8002246:	bf00      	nop

08002248 <_Bfree>:
 8002248:	b530      	push	{r4, r5, lr}
 800224a:	6a45      	ldr	r5, [r0, #36]	; 0x24
 800224c:	b083      	sub	sp, #12
 800224e:	4604      	mov	r4, r0
 8002250:	b155      	cbz	r5, 8002268 <_Bfree+0x20>
 8002252:	b139      	cbz	r1, 8002264 <_Bfree+0x1c>
 8002254:	6a63      	ldr	r3, [r4, #36]	; 0x24
 8002256:	684a      	ldr	r2, [r1, #4]
 8002258:	68db      	ldr	r3, [r3, #12]
 800225a:	f853 0022 	ldr.w	r0, [r3, r2, lsl #2]
 800225e:	6008      	str	r0, [r1, #0]
 8002260:	f843 1022 	str.w	r1, [r3, r2, lsl #2]
 8002264:	b003      	add	sp, #12
 8002266:	bd30      	pop	{r4, r5, pc}
 8002268:	2010      	movs	r0, #16
 800226a:	9101      	str	r1, [sp, #4]
 800226c:	f7ff ff54 	bl	8002118 <malloc>
 8002270:	9901      	ldr	r1, [sp, #4]
 8002272:	6260      	str	r0, [r4, #36]	; 0x24
 8002274:	6045      	str	r5, [r0, #4]
 8002276:	6085      	str	r5, [r0, #8]
 8002278:	6005      	str	r5, [r0, #0]
 800227a:	60c5      	str	r5, [r0, #12]
 800227c:	e7e9      	b.n	8002252 <_Bfree+0xa>
 800227e:	bf00      	nop

08002280 <__multadd>:
 8002280:	b5f0      	push	{r4, r5, r6, r7, lr}
 8002282:	690c      	ldr	r4, [r1, #16]
 8002284:	b083      	sub	sp, #12
 8002286:	460d      	mov	r5, r1
 8002288:	4606      	mov	r6, r0
 800228a:	f101 0e14 	add.w	lr, r1, #20
 800228e:	2700      	movs	r7, #0
 8002290:	f8de 0000 	ldr.w	r0, [lr]
 8002294:	b281      	uxth	r1, r0
 8002296:	fb02 3301 	mla	r3, r2, r1, r3
 800229a:	0c01      	lsrs	r1, r0, #16
 800229c:	0c18      	lsrs	r0, r3, #16
 800229e:	fb02 0101 	mla	r1, r2, r1, r0
 80022a2:	b29b      	uxth	r3, r3
 80022a4:	3701      	adds	r7, #1
 80022a6:	eb03 4301 	add.w	r3, r3, r1, lsl #16
 80022aa:	42bc      	cmp	r4, r7
 80022ac:	f84e 3b04 	str.w	r3, [lr], #4
 80022b0:	ea4f 4311 	mov.w	r3, r1, lsr #16
 80022b4:	dcec      	bgt.n	8002290 <__multadd+0x10>
 80022b6:	b13b      	cbz	r3, 80022c8 <__multadd+0x48>
 80022b8:	68aa      	ldr	r2, [r5, #8]
 80022ba:	4294      	cmp	r4, r2
 80022bc:	da07      	bge.n	80022ce <__multadd+0x4e>
 80022be:	eb05 0284 	add.w	r2, r5, r4, lsl #2
 80022c2:	3401      	adds	r4, #1
 80022c4:	6153      	str	r3, [r2, #20]
 80022c6:	612c      	str	r4, [r5, #16]
 80022c8:	4628      	mov	r0, r5
 80022ca:	b003      	add	sp, #12
 80022cc:	bdf0      	pop	{r4, r5, r6, r7, pc}
 80022ce:	6869      	ldr	r1, [r5, #4]
 80022d0:	9301      	str	r3, [sp, #4]
 80022d2:	3101      	adds	r1, #1
 80022d4:	4630      	mov	r0, r6
 80022d6:	f7ff ff83 	bl	80021e0 <_Balloc>
 80022da:	692a      	ldr	r2, [r5, #16]
 80022dc:	3202      	adds	r2, #2
 80022de:	f105 010c 	add.w	r1, r5, #12
 80022e2:	4607      	mov	r7, r0
 80022e4:	0092      	lsls	r2, r2, #2
 80022e6:	300c      	adds	r0, #12
 80022e8:	f7fd ffe4 	bl	80002b4 <memcpy>
 80022ec:	4629      	mov	r1, r5
 80022ee:	4630      	mov	r0, r6
 80022f0:	f7ff ffaa 	bl	8002248 <_Bfree>
 80022f4:	463d      	mov	r5, r7
 80022f6:	9b01      	ldr	r3, [sp, #4]
 80022f8:	e7e1      	b.n	80022be <__multadd+0x3e>
 80022fa:	bf00      	nop

080022fc <__s2b>:
 80022fc:	e92d 43f8 	stmdb	sp!, {r3, r4, r5, r6, r7, r8, r9, lr}
 8002300:	4c24      	ldr	r4, [pc, #144]	; (8002394 <__s2b+0x98>)
 8002302:	9e08      	ldr	r6, [sp, #32]
 8002304:	461f      	mov	r7, r3
 8002306:	3308      	adds	r3, #8
 8002308:	fb84 4e03 	smull	r4, lr, r4, r3
 800230c:	17db      	asrs	r3, r3, #31
 800230e:	ebc3 0e6e 	rsb	lr, r3, lr, asr #1
 8002312:	f1be 0f01 	cmp.w	lr, #1
 8002316:	4605      	mov	r5, r0
 8002318:	460c      	mov	r4, r1
 800231a:	4690      	mov	r8, r2
 800231c:	dd35      	ble.n	800238a <__s2b+0x8e>
 800231e:	2301      	movs	r3, #1
 8002320:	2100      	movs	r1, #0
 8002322:	005b      	lsls	r3, r3, #1
 8002324:	459e      	cmp	lr, r3
 8002326:	f101 0101 	add.w	r1, r1, #1
 800232a:	dcfa      	bgt.n	8002322 <__s2b+0x26>
 800232c:	4628      	mov	r0, r5
 800232e:	f7ff ff57 	bl	80021e0 <_Balloc>
 8002332:	2301      	movs	r3, #1
 8002334:	f1b8 0f09 	cmp.w	r8, #9
 8002338:	6146      	str	r6, [r0, #20]
 800233a:	6103      	str	r3, [r0, #16]
 800233c:	dd21      	ble.n	8002382 <__s2b+0x86>
 800233e:	f104 0909 	add.w	r9, r4, #9
 8002342:	464e      	mov	r6, r9
 8002344:	4444      	add	r4, r8
 8002346:	f816 3b01 	ldrb.w	r3, [r6], #1
 800234a:	4601      	mov	r1, r0
 800234c:	3b30      	subs	r3, #48	; 0x30
 800234e:	220a      	movs	r2, #10
 8002350:	4628      	mov	r0, r5
 8002352:	f7ff ff95 	bl	8002280 <__multadd>
 8002356:	42a6      	cmp	r6, r4
 8002358:	d1f5      	bne.n	8002346 <__s2b+0x4a>
 800235a:	f1a8 0408 	sub.w	r4, r8, #8
 800235e:	444c      	add	r4, r9
 8002360:	4547      	cmp	r7, r8
 8002362:	dd14      	ble.n	800238e <__s2b+0x92>
 8002364:	eba7 0708 	sub.w	r7, r7, r8
 8002368:	4427      	add	r7, r4
 800236a:	f814 3b01 	ldrb.w	r3, [r4], #1
 800236e:	4601      	mov	r1, r0
 8002370:	3b30      	subs	r3, #48	; 0x30
 8002372:	220a      	movs	r2, #10
 8002374:	4628      	mov	r0, r5
 8002376:	f7ff ff83 	bl	8002280 <__multadd>
 800237a:	42a7      	cmp	r7, r4
 800237c:	d1f5      	bne.n	800236a <__s2b+0x6e>
 800237e:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
 8002382:	340a      	adds	r4, #10
 8002384:	f04f 0809 	mov.w	r8, #9
 8002388:	e7ea      	b.n	8002360 <__s2b+0x64>
 800238a:	2100      	movs	r1, #0
 800238c:	e7ce      	b.n	800232c <__s2b+0x30>
 800238e:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
 8002392:	bf00      	nop
 8002394:	38e38e39 	.word	0x38e38e39

08002398 <__hi0bits>:
 8002398:	0c02      	lsrs	r2, r0, #16
 800239a:	0412      	lsls	r2, r2, #16
 800239c:	4603      	mov	r3, r0
 800239e:	b9b2      	cbnz	r2, 80023ce <__hi0bits+0x36>
 80023a0:	0403      	lsls	r3, r0, #16
 80023a2:	2010      	movs	r0, #16
 80023a4:	f013 4f7f 	tst.w	r3, #4278190080	; 0xff000000
 80023a8:	bf04      	itt	eq
 80023aa:	021b      	lsleq	r3, r3, #8
 80023ac:	3008      	addeq	r0, #8
 80023ae:	f013 4f70 	tst.w	r3, #4026531840	; 0xf0000000
 80023b2:	bf04      	itt	eq
 80023b4:	011b      	lsleq	r3, r3, #4
 80023b6:	3004      	addeq	r0, #4
 80023b8:	f013 4f40 	tst.w	r3, #3221225472	; 0xc0000000
 80023bc:	bf04      	itt	eq
 80023be:	009b      	lsleq	r3, r3, #2
 80023c0:	3002      	addeq	r0, #2
 80023c2:	2b00      	cmp	r3, #0
 80023c4:	db02      	blt.n	80023cc <__hi0bits+0x34>
 80023c6:	005b      	lsls	r3, r3, #1
 80023c8:	d403      	bmi.n	80023d2 <__hi0bits+0x3a>
 80023ca:	2020      	movs	r0, #32
 80023cc:	4770      	bx	lr
 80023ce:	2000      	movs	r0, #0
 80023d0:	e7e8      	b.n	80023a4 <__hi0bits+0xc>
 80023d2:	3001      	adds	r0, #1
 80023d4:	4770      	bx	lr
 80023d6:	bf00      	nop

080023d8 <__lo0bits>:
 80023d8:	6803      	ldr	r3, [r0, #0]
 80023da:	f013 0207 	ands.w	r2, r3, #7
 80023de:	4601      	mov	r1, r0
 80023e0:	d007      	beq.n	80023f2 <__lo0bits+0x1a>
 80023e2:	07da      	lsls	r2, r3, #31
 80023e4:	d421      	bmi.n	800242a <__lo0bits+0x52>
 80023e6:	0798      	lsls	r0, r3, #30
 80023e8:	d421      	bmi.n	800242e <__lo0bits+0x56>
 80023ea:	089b      	lsrs	r3, r3, #2
 80023ec:	600b      	str	r3, [r1, #0]
 80023ee:	2002      	movs	r0, #2
 80023f0:	4770      	bx	lr
 80023f2:	b298      	uxth	r0, r3
 80023f4:	b198      	cbz	r0, 800241e <__lo0bits+0x46>
 80023f6:	4610      	mov	r0, r2
 80023f8:	f013 0fff 	tst.w	r3, #255	; 0xff
 80023fc:	bf04      	itt	eq
 80023fe:	0a1b      	lsreq	r3, r3, #8
 8002400:	3008      	addeq	r0, #8
 8002402:	071a      	lsls	r2, r3, #28
 8002404:	bf04      	itt	eq
 8002406:	091b      	lsreq	r3, r3, #4
 8002408:	3004      	addeq	r0, #4
 800240a:	079a      	lsls	r2, r3, #30
 800240c:	bf04      	itt	eq
 800240e:	089b      	lsreq	r3, r3, #2
 8002410:	3002      	addeq	r0, #2
 8002412:	07da      	lsls	r2, r3, #31
 8002414:	d407      	bmi.n	8002426 <__lo0bits+0x4e>
 8002416:	085b      	lsrs	r3, r3, #1
 8002418:	d104      	bne.n	8002424 <__lo0bits+0x4c>
 800241a:	2020      	movs	r0, #32
 800241c:	4770      	bx	lr
 800241e:	0c1b      	lsrs	r3, r3, #16
 8002420:	2010      	movs	r0, #16
 8002422:	e7e9      	b.n	80023f8 <__lo0bits+0x20>
 8002424:	3001      	adds	r0, #1
 8002426:	600b      	str	r3, [r1, #0]
 8002428:	4770      	bx	lr
 800242a:	2000      	movs	r0, #0
 800242c:	4770      	bx	lr
 800242e:	085b      	lsrs	r3, r3, #1
 8002430:	600b      	str	r3, [r1, #0]
 8002432:	2001      	movs	r0, #1
 8002434:	4770      	bx	lr
 8002436:	bf00      	nop

08002438 <__i2b>:
 8002438:	b510      	push	{r4, lr}
 800243a:	460c      	mov	r4, r1
 800243c:	2101      	movs	r1, #1
 800243e:	f7ff fecf 	bl	80021e0 <_Balloc>
 8002442:	2201      	movs	r2, #1
 8002444:	6144      	str	r4, [r0, #20]
 8002446:	6102      	str	r2, [r0, #16]
 8002448:	bd10      	pop	{r4, pc}
 800244a:	bf00      	nop

0800244c <__multiply>:
 800244c:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8002450:	690c      	ldr	r4, [r1, #16]
 8002452:	6915      	ldr	r5, [r2, #16]
 8002454:	42ac      	cmp	r4, r5
 8002456:	b083      	sub	sp, #12
 8002458:	468b      	mov	fp, r1
 800245a:	4616      	mov	r6, r2
 800245c:	da04      	bge.n	8002468 <__multiply+0x1c>
 800245e:	4622      	mov	r2, r4
 8002460:	46b3      	mov	fp, r6
 8002462:	462c      	mov	r4, r5
 8002464:	460e      	mov	r6, r1
 8002466:	4615      	mov	r5, r2
 8002468:	f8db 3008 	ldr.w	r3, [fp, #8]
 800246c:	f8db 1004 	ldr.w	r1, [fp, #4]
 8002470:	eb04 0805 	add.w	r8, r4, r5
 8002474:	4598      	cmp	r8, r3
 8002476:	bfc8      	it	gt
 8002478:	3101      	addgt	r1, #1
 800247a:	f7ff feb1 	bl	80021e0 <_Balloc>
 800247e:	f100 0914 	add.w	r9, r0, #20
 8002482:	eb09 0a88 	add.w	sl, r9, r8, lsl #2
 8002486:	45d1      	cmp	r9, sl
 8002488:	9000      	str	r0, [sp, #0]
 800248a:	d205      	bcs.n	8002498 <__multiply+0x4c>
 800248c:	464b      	mov	r3, r9
 800248e:	2100      	movs	r1, #0
 8002490:	f843 1b04 	str.w	r1, [r3], #4
 8002494:	459a      	cmp	sl, r3
 8002496:	d8fb      	bhi.n	8002490 <__multiply+0x44>
 8002498:	f106 0c14 	add.w	ip, r6, #20
 800249c:	eb0c 0385 	add.w	r3, ip, r5, lsl #2
 80024a0:	f10b 0b14 	add.w	fp, fp, #20
 80024a4:	459c      	cmp	ip, r3
 80024a6:	eb0b 0e84 	add.w	lr, fp, r4, lsl #2
 80024aa:	d24c      	bcs.n	8002546 <__multiply+0xfa>
 80024ac:	f8cd a004 	str.w	sl, [sp, #4]
 80024b0:	469a      	mov	sl, r3
 80024b2:	f8dc 5000 	ldr.w	r5, [ip]
 80024b6:	b2af      	uxth	r7, r5
 80024b8:	b1ef      	cbz	r7, 80024f6 <__multiply+0xaa>
 80024ba:	2100      	movs	r1, #0
 80024bc:	464d      	mov	r5, r9
 80024be:	465e      	mov	r6, fp
 80024c0:	460c      	mov	r4, r1
 80024c2:	f856 2b04 	ldr.w	r2, [r6], #4
 80024c6:	6828      	ldr	r0, [r5, #0]
 80024c8:	b293      	uxth	r3, r2
 80024ca:	b281      	uxth	r1, r0
 80024cc:	fb07 1303 	mla	r3, r7, r3, r1
 80024d0:	0c12      	lsrs	r2, r2, #16
 80024d2:	0c01      	lsrs	r1, r0, #16
 80024d4:	4423      	add	r3, r4
 80024d6:	fb07 1102 	mla	r1, r7, r2, r1
 80024da:	eb01 4113 	add.w	r1, r1, r3, lsr #16
 80024de:	b29b      	uxth	r3, r3
 80024e0:	ea43 4301 	orr.w	r3, r3, r1, lsl #16
 80024e4:	45b6      	cmp	lr, r6
 80024e6:	f845 3b04 	str.w	r3, [r5], #4
 80024ea:	ea4f 4411 	mov.w	r4, r1, lsr #16
 80024ee:	d8e8      	bhi.n	80024c2 <__multiply+0x76>
 80024f0:	602c      	str	r4, [r5, #0]
 80024f2:	f8dc 5000 	ldr.w	r5, [ip]
 80024f6:	0c2d      	lsrs	r5, r5, #16
 80024f8:	d01d      	beq.n	8002536 <__multiply+0xea>
 80024fa:	f8d9 3000 	ldr.w	r3, [r9]
 80024fe:	4648      	mov	r0, r9
 8002500:	461c      	mov	r4, r3
 8002502:	4659      	mov	r1, fp
 8002504:	2200      	movs	r2, #0
 8002506:	880e      	ldrh	r6, [r1, #0]
 8002508:	0c24      	lsrs	r4, r4, #16
 800250a:	fb05 4406 	mla	r4, r5, r6, r4
 800250e:	4422      	add	r2, r4
 8002510:	b29b      	uxth	r3, r3
 8002512:	ea43 4302 	orr.w	r3, r3, r2, lsl #16
 8002516:	f840 3b04 	str.w	r3, [r0], #4
 800251a:	f851 3b04 	ldr.w	r3, [r1], #4
 800251e:	6804      	ldr	r4, [r0, #0]
 8002520:	0c1b      	lsrs	r3, r3, #16
 8002522:	b2a6      	uxth	r6, r4
 8002524:	fb05 6303 	mla	r3, r5, r3, r6
 8002528:	eb03 4312 	add.w	r3, r3, r2, lsr #16
 800252c:	458e      	cmp	lr, r1
 800252e:	ea4f 4213 	mov.w	r2, r3, lsr #16
 8002532:	d8e8      	bhi.n	8002506 <__multiply+0xba>
 8002534:	6003      	str	r3, [r0, #0]
 8002536:	f10c 0c04 	add.w	ip, ip, #4
 800253a:	45e2      	cmp	sl, ip
 800253c:	f109 0904 	add.w	r9, r9, #4
 8002540:	d8b7      	bhi.n	80024b2 <__multiply+0x66>
 8002542:	f8dd a004 	ldr.w	sl, [sp, #4]
 8002546:	f1b8 0f00 	cmp.w	r8, #0
 800254a:	dd0b      	ble.n	8002564 <__multiply+0x118>
 800254c:	f85a 3c04 	ldr.w	r3, [sl, #-4]
 8002550:	f1aa 0a04 	sub.w	sl, sl, #4
 8002554:	b11b      	cbz	r3, 800255e <__multiply+0x112>
 8002556:	e005      	b.n	8002564 <__multiply+0x118>
 8002558:	f85a 3d04 	ldr.w	r3, [sl, #-4]!
 800255c:	b913      	cbnz	r3, 8002564 <__multiply+0x118>
 800255e:	f1b8 0801 	subs.w	r8, r8, #1
 8002562:	d1f9      	bne.n	8002558 <__multiply+0x10c>
 8002564:	9800      	ldr	r0, [sp, #0]
 8002566:	f8c0 8010 	str.w	r8, [r0, #16]
 800256a:	b003      	add	sp, #12
 800256c:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

08002570 <__pow5mult>:
 8002570:	e92d 43f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, lr}
 8002574:	f012 0303 	ands.w	r3, r2, #3
 8002578:	b083      	sub	sp, #12
 800257a:	4614      	mov	r4, r2
 800257c:	4607      	mov	r7, r0
 800257e:	d12d      	bne.n	80025dc <__pow5mult+0x6c>
 8002580:	460e      	mov	r6, r1
 8002582:	10a4      	asrs	r4, r4, #2
 8002584:	d01c      	beq.n	80025c0 <__pow5mult+0x50>
 8002586:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8002588:	b38b      	cbz	r3, 80025ee <__pow5mult+0x7e>
 800258a:	689d      	ldr	r5, [r3, #8]
 800258c:	2d00      	cmp	r5, #0
 800258e:	d039      	beq.n	8002604 <__pow5mult+0x94>
 8002590:	07e3      	lsls	r3, r4, #31
 8002592:	f04f 0900 	mov.w	r9, #0
 8002596:	d406      	bmi.n	80025a6 <__pow5mult+0x36>
 8002598:	1064      	asrs	r4, r4, #1
 800259a:	d011      	beq.n	80025c0 <__pow5mult+0x50>
 800259c:	6828      	ldr	r0, [r5, #0]
 800259e:	b198      	cbz	r0, 80025c8 <__pow5mult+0x58>
 80025a0:	4605      	mov	r5, r0
 80025a2:	07e3      	lsls	r3, r4, #31
 80025a4:	d5f8      	bpl.n	8002598 <__pow5mult+0x28>
 80025a6:	4631      	mov	r1, r6
 80025a8:	462a      	mov	r2, r5
 80025aa:	4638      	mov	r0, r7
 80025ac:	f7ff ff4e 	bl	800244c <__multiply>
 80025b0:	4631      	mov	r1, r6
 80025b2:	4680      	mov	r8, r0
 80025b4:	4638      	mov	r0, r7
 80025b6:	f7ff fe47 	bl	8002248 <_Bfree>
 80025ba:	1064      	asrs	r4, r4, #1
 80025bc:	4646      	mov	r6, r8
 80025be:	d1ed      	bne.n	800259c <__pow5mult+0x2c>
 80025c0:	4630      	mov	r0, r6
 80025c2:	b003      	add	sp, #12
 80025c4:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
 80025c8:	462a      	mov	r2, r5
 80025ca:	4629      	mov	r1, r5
 80025cc:	4638      	mov	r0, r7
 80025ce:	f7ff ff3d 	bl	800244c <__multiply>
 80025d2:	6028      	str	r0, [r5, #0]
 80025d4:	f8c0 9000 	str.w	r9, [r0]
 80025d8:	4605      	mov	r5, r0
 80025da:	e7e2      	b.n	80025a2 <__pow5mult+0x32>
 80025dc:	1e5a      	subs	r2, r3, #1
 80025de:	4d11      	ldr	r5, [pc, #68]	; (8002624 <__pow5mult+0xb4>)
 80025e0:	2300      	movs	r3, #0
 80025e2:	f855 2022 	ldr.w	r2, [r5, r2, lsl #2]
 80025e6:	f7ff fe4b 	bl	8002280 <__multadd>
 80025ea:	4606      	mov	r6, r0
 80025ec:	e7c9      	b.n	8002582 <__pow5mult+0x12>
 80025ee:	2010      	movs	r0, #16
 80025f0:	9301      	str	r3, [sp, #4]
 80025f2:	f7ff fd91 	bl	8002118 <malloc>
 80025f6:	9b01      	ldr	r3, [sp, #4]
 80025f8:	6278      	str	r0, [r7, #36]	; 0x24
 80025fa:	6043      	str	r3, [r0, #4]
 80025fc:	6083      	str	r3, [r0, #8]
 80025fe:	6003      	str	r3, [r0, #0]
 8002600:	60c3      	str	r3, [r0, #12]
 8002602:	4603      	mov	r3, r0
 8002604:	2101      	movs	r1, #1
 8002606:	4638      	mov	r0, r7
 8002608:	9301      	str	r3, [sp, #4]
 800260a:	f7ff fde9 	bl	80021e0 <_Balloc>
 800260e:	9b01      	ldr	r3, [sp, #4]
 8002610:	4605      	mov	r5, r0
 8002612:	2101      	movs	r1, #1
 8002614:	f240 2071 	movw	r0, #625	; 0x271
 8002618:	2200      	movs	r2, #0
 800261a:	6168      	str	r0, [r5, #20]
 800261c:	6129      	str	r1, [r5, #16]
 800261e:	609d      	str	r5, [r3, #8]
 8002620:	602a      	str	r2, [r5, #0]
 8002622:	e7b5      	b.n	8002590 <__pow5mult+0x20>
 8002624:	08004700 	.word	0x08004700

08002628 <__lshift>:
 8002628:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 800262c:	4691      	mov	r9, r2
 800262e:	690a      	ldr	r2, [r1, #16]
 8002630:	688b      	ldr	r3, [r1, #8]
 8002632:	ea4f 1a69 	mov.w	sl, r9, asr #5
 8002636:	eb0a 0802 	add.w	r8, sl, r2
 800263a:	f108 0401 	add.w	r4, r8, #1
 800263e:	429c      	cmp	r4, r3
 8002640:	460d      	mov	r5, r1
 8002642:	4607      	mov	r7, r0
 8002644:	6849      	ldr	r1, [r1, #4]
 8002646:	dd04      	ble.n	8002652 <__lshift+0x2a>
 8002648:	005b      	lsls	r3, r3, #1
 800264a:	429c      	cmp	r4, r3
 800264c:	f101 0101 	add.w	r1, r1, #1
 8002650:	dcfa      	bgt.n	8002648 <__lshift+0x20>
 8002652:	4638      	mov	r0, r7
 8002654:	f7ff fdc4 	bl	80021e0 <_Balloc>
 8002658:	f1ba 0f00 	cmp.w	sl, #0
 800265c:	4606      	mov	r6, r0
 800265e:	f100 0314 	add.w	r3, r0, #20
 8002662:	dd32      	ble.n	80026ca <__lshift+0xa2>
 8002664:	eb03 018a 	add.w	r1, r3, sl, lsl #2
 8002668:	2200      	movs	r2, #0
 800266a:	f843 2b04 	str.w	r2, [r3], #4
 800266e:	428b      	cmp	r3, r1
 8002670:	d1fb      	bne.n	800266a <__lshift+0x42>
 8002672:	6928      	ldr	r0, [r5, #16]
 8002674:	f105 0314 	add.w	r3, r5, #20
 8002678:	f019 091f 	ands.w	r9, r9, #31
 800267c:	eb03 0e80 	add.w	lr, r3, r0, lsl #2
 8002680:	d01b      	beq.n	80026ba <__lshift+0x92>
 8002682:	f1c9 0c20 	rsb	ip, r9, #32
 8002686:	2200      	movs	r2, #0
 8002688:	6818      	ldr	r0, [r3, #0]
 800268a:	fa00 f009 	lsl.w	r0, r0, r9
 800268e:	4310      	orrs	r0, r2
 8002690:	f841 0b04 	str.w	r0, [r1], #4
 8002694:	f853 2b04 	ldr.w	r2, [r3], #4
 8002698:	459e      	cmp	lr, r3
 800269a:	fa22 f20c 	lsr.w	r2, r2, ip
 800269e:	d8f3      	bhi.n	8002688 <__lshift+0x60>
 80026a0:	600a      	str	r2, [r1, #0]
 80026a2:	b10a      	cbz	r2, 80026a8 <__lshift+0x80>
 80026a4:	f108 0402 	add.w	r4, r8, #2
 80026a8:	3c01      	subs	r4, #1
 80026aa:	4638      	mov	r0, r7
 80026ac:	6134      	str	r4, [r6, #16]
 80026ae:	4629      	mov	r1, r5
 80026b0:	f7ff fdca 	bl	8002248 <_Bfree>
 80026b4:	4630      	mov	r0, r6
 80026b6:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 80026ba:	3904      	subs	r1, #4
 80026bc:	f853 2b04 	ldr.w	r2, [r3], #4
 80026c0:	f841 2f04 	str.w	r2, [r1, #4]!
 80026c4:	459e      	cmp	lr, r3
 80026c6:	d8f9      	bhi.n	80026bc <__lshift+0x94>
 80026c8:	e7ee      	b.n	80026a8 <__lshift+0x80>
 80026ca:	4619      	mov	r1, r3
 80026cc:	e7d1      	b.n	8002672 <__lshift+0x4a>
 80026ce:	bf00      	nop

080026d0 <__mcmp>:
 80026d0:	b430      	push	{r4, r5}
 80026d2:	690b      	ldr	r3, [r1, #16]
 80026d4:	4605      	mov	r5, r0
 80026d6:	6900      	ldr	r0, [r0, #16]
 80026d8:	1ac0      	subs	r0, r0, r3
 80026da:	d10f      	bne.n	80026fc <__mcmp+0x2c>
 80026dc:	009b      	lsls	r3, r3, #2
 80026de:	3514      	adds	r5, #20
 80026e0:	3114      	adds	r1, #20
 80026e2:	4419      	add	r1, r3
 80026e4:	442b      	add	r3, r5
 80026e6:	e001      	b.n	80026ec <__mcmp+0x1c>
 80026e8:	429d      	cmp	r5, r3
 80026ea:	d207      	bcs.n	80026fc <__mcmp+0x2c>
 80026ec:	f853 4d04 	ldr.w	r4, [r3, #-4]!
 80026f0:	f851 2d04 	ldr.w	r2, [r1, #-4]!
 80026f4:	4294      	cmp	r4, r2
 80026f6:	d0f7      	beq.n	80026e8 <__mcmp+0x18>
 80026f8:	d302      	bcc.n	8002700 <__mcmp+0x30>
 80026fa:	2001      	movs	r0, #1
 80026fc:	bc30      	pop	{r4, r5}
 80026fe:	4770      	bx	lr
 8002700:	f04f 30ff 	mov.w	r0, #4294967295	; 0xffffffff
 8002704:	e7fa      	b.n	80026fc <__mcmp+0x2c>
 8002706:	bf00      	nop

08002708 <__mdiff>:
 8002708:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
 800270c:	690f      	ldr	r7, [r1, #16]
 800270e:	460e      	mov	r6, r1
 8002710:	6911      	ldr	r1, [r2, #16]
 8002712:	1a7f      	subs	r7, r7, r1
 8002714:	2f00      	cmp	r7, #0
 8002716:	4690      	mov	r8, r2
 8002718:	d117      	bne.n	800274a <__mdiff+0x42>
 800271a:	0089      	lsls	r1, r1, #2
 800271c:	f106 0514 	add.w	r5, r6, #20
 8002720:	f102 0e14 	add.w	lr, r2, #20
 8002724:	186b      	adds	r3, r5, r1
 8002726:	4471      	add	r1, lr
 8002728:	e001      	b.n	800272e <__mdiff+0x26>
 800272a:	429d      	cmp	r5, r3
 800272c:	d25c      	bcs.n	80027e8 <__mdiff+0xe0>
 800272e:	f853 2d04 	ldr.w	r2, [r3, #-4]!
 8002732:	f851 4d04 	ldr.w	r4, [r1, #-4]!
 8002736:	42a2      	cmp	r2, r4
 8002738:	d0f7      	beq.n	800272a <__mdiff+0x22>
 800273a:	d25e      	bcs.n	80027fa <__mdiff+0xf2>
 800273c:	4633      	mov	r3, r6
 800273e:	462c      	mov	r4, r5
 8002740:	4646      	mov	r6, r8
 8002742:	4675      	mov	r5, lr
 8002744:	4698      	mov	r8, r3
 8002746:	2701      	movs	r7, #1
 8002748:	e005      	b.n	8002756 <__mdiff+0x4e>
 800274a:	db58      	blt.n	80027fe <__mdiff+0xf6>
 800274c:	f106 0514 	add.w	r5, r6, #20
 8002750:	f108 0414 	add.w	r4, r8, #20
 8002754:	2700      	movs	r7, #0
 8002756:	6871      	ldr	r1, [r6, #4]
 8002758:	f7ff fd42 	bl	80021e0 <_Balloc>
 800275c:	f8d8 3010 	ldr.w	r3, [r8, #16]
 8002760:	6936      	ldr	r6, [r6, #16]
 8002762:	60c7      	str	r7, [r0, #12]
 8002764:	eb04 0c83 	add.w	ip, r4, r3, lsl #2
 8002768:	46a6      	mov	lr, r4
 800276a:	eb05 0786 	add.w	r7, r5, r6, lsl #2
 800276e:	f100 0414 	add.w	r4, r0, #20
 8002772:	2300      	movs	r3, #0
 8002774:	f85e 1b04 	ldr.w	r1, [lr], #4
 8002778:	f855 8b04 	ldr.w	r8, [r5], #4
 800277c:	b28a      	uxth	r2, r1
 800277e:	fa13 f388 	uxtah	r3, r3, r8
 8002782:	0c09      	lsrs	r1, r1, #16
 8002784:	1a9a      	subs	r2, r3, r2
 8002786:	ebc1 4318 	rsb	r3, r1, r8, lsr #16
 800278a:	eb03 4322 	add.w	r3, r3, r2, asr #16
 800278e:	b292      	uxth	r2, r2
 8002790:	ea42 4203 	orr.w	r2, r2, r3, lsl #16
 8002794:	45f4      	cmp	ip, lr
 8002796:	f844 2b04 	str.w	r2, [r4], #4
 800279a:	ea4f 4323 	mov.w	r3, r3, asr #16
 800279e:	d8e9      	bhi.n	8002774 <__mdiff+0x6c>
 80027a0:	42af      	cmp	r7, r5
 80027a2:	d917      	bls.n	80027d4 <__mdiff+0xcc>
 80027a4:	46a4      	mov	ip, r4
 80027a6:	46ae      	mov	lr, r5
 80027a8:	f85e 2b04 	ldr.w	r2, [lr], #4
 80027ac:	fa13 f382 	uxtah	r3, r3, r2
 80027b0:	1419      	asrs	r1, r3, #16
 80027b2:	eb01 4112 	add.w	r1, r1, r2, lsr #16
 80027b6:	b29b      	uxth	r3, r3
 80027b8:	ea43 4201 	orr.w	r2, r3, r1, lsl #16
 80027bc:	4577      	cmp	r7, lr
 80027be:	f84c 2b04 	str.w	r2, [ip], #4
 80027c2:	ea4f 4321 	mov.w	r3, r1, asr #16
 80027c6:	d8ef      	bhi.n	80027a8 <__mdiff+0xa0>
 80027c8:	43ed      	mvns	r5, r5
 80027ca:	442f      	add	r7, r5
 80027cc:	f027 0703 	bic.w	r7, r7, #3
 80027d0:	3704      	adds	r7, #4
 80027d2:	443c      	add	r4, r7
 80027d4:	3c04      	subs	r4, #4
 80027d6:	b922      	cbnz	r2, 80027e2 <__mdiff+0xda>
 80027d8:	f854 3d04 	ldr.w	r3, [r4, #-4]!
 80027dc:	3e01      	subs	r6, #1
 80027de:	2b00      	cmp	r3, #0
 80027e0:	d0fa      	beq.n	80027d8 <__mdiff+0xd0>
 80027e2:	6106      	str	r6, [r0, #16]
 80027e4:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}
 80027e8:	2100      	movs	r1, #0
 80027ea:	f7ff fcf9 	bl	80021e0 <_Balloc>
 80027ee:	2201      	movs	r2, #1
 80027f0:	2300      	movs	r3, #0
 80027f2:	6102      	str	r2, [r0, #16]
 80027f4:	6143      	str	r3, [r0, #20]
 80027f6:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}
 80027fa:	4674      	mov	r4, lr
 80027fc:	e7ab      	b.n	8002756 <__mdiff+0x4e>
 80027fe:	4633      	mov	r3, r6
 8002800:	f106 0414 	add.w	r4, r6, #20
 8002804:	f102 0514 	add.w	r5, r2, #20
 8002808:	4616      	mov	r6, r2
 800280a:	2701      	movs	r7, #1
 800280c:	4698      	mov	r8, r3
 800280e:	e7a2      	b.n	8002756 <__mdiff+0x4e>

08002810 <__ulp>:
 8002810:	4b13      	ldr	r3, [pc, #76]	; (8002860 <__ulp+0x50>)
 8002812:	ee10 2a90 	vmov	r2, s1
 8002816:	401a      	ands	r2, r3
 8002818:	f1a2 7350 	sub.w	r3, r2, #54525952	; 0x3400000
 800281c:	2b00      	cmp	r3, #0
 800281e:	dd04      	ble.n	800282a <__ulp+0x1a>
 8002820:	2000      	movs	r0, #0
 8002822:	4619      	mov	r1, r3
 8002824:	ec41 0b10 	vmov	d0, r0, r1
 8002828:	4770      	bx	lr
 800282a:	425b      	negs	r3, r3
 800282c:	151b      	asrs	r3, r3, #20
 800282e:	2b13      	cmp	r3, #19
 8002830:	dd0d      	ble.n	800284e <__ulp+0x3e>
 8002832:	3b14      	subs	r3, #20
 8002834:	2b1e      	cmp	r3, #30
 8002836:	bfdd      	ittte	le
 8002838:	f1c3 031f 	rsble	r3, r3, #31
 800283c:	2201      	movle	r2, #1
 800283e:	fa02 f303 	lslle.w	r3, r2, r3
 8002842:	2301      	movgt	r3, #1
 8002844:	2100      	movs	r1, #0
 8002846:	4618      	mov	r0, r3
 8002848:	ec41 0b10 	vmov	d0, r0, r1
 800284c:	4770      	bx	lr
 800284e:	f44f 2200 	mov.w	r2, #524288	; 0x80000
 8002852:	2000      	movs	r0, #0
 8002854:	fa42 f103 	asr.w	r1, r2, r3
 8002858:	ec41 0b10 	vmov	d0, r0, r1
 800285c:	4770      	bx	lr
 800285e:	bf00      	nop
 8002860:	7ff00000 	.word	0x7ff00000

08002864 <__b2d>:
 8002864:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 8002866:	6904      	ldr	r4, [r0, #16]
 8002868:	f100 0614 	add.w	r6, r0, #20
 800286c:	eb06 0484 	add.w	r4, r6, r4, lsl #2
 8002870:	f854 5c04 	ldr.w	r5, [r4, #-4]
 8002874:	4628      	mov	r0, r5
 8002876:	f7ff fd8f 	bl	8002398 <__hi0bits>
 800287a:	f1c0 0320 	rsb	r3, r0, #32
 800287e:	280a      	cmp	r0, #10
 8002880:	600b      	str	r3, [r1, #0]
 8002882:	f1a4 0104 	sub.w	r1, r4, #4
 8002886:	dc17      	bgt.n	80028b8 <__b2d+0x54>
 8002888:	428e      	cmp	r6, r1
 800288a:	f1c0 070b 	rsb	r7, r0, #11
 800288e:	bf38      	it	cc
 8002890:	f854 1c08 	ldrcc.w	r1, [r4, #-8]
 8002894:	fa25 fe07 	lsr.w	lr, r5, r7
 8002898:	f100 0015 	add.w	r0, r0, #21
 800289c:	f04e 537f 	orr.w	r3, lr, #1069547520	; 0x3fc00000
 80028a0:	bf34      	ite	cc
 80028a2:	40f9      	lsrcc	r1, r7
 80028a4:	2100      	movcs	r1, #0
 80028a6:	fa05 f000 	lsl.w	r0, r5, r0
 80028aa:	f443 1340 	orr.w	r3, r3, #3145728	; 0x300000
 80028ae:	ea40 0201 	orr.w	r2, r0, r1
 80028b2:	ec43 2b10 	vmov	d0, r2, r3
 80028b6:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
 80028b8:	428e      	cmp	r6, r1
 80028ba:	d21d      	bcs.n	80028f8 <__b2d+0x94>
 80028bc:	380b      	subs	r0, #11
 80028be:	f854 7c08 	ldr.w	r7, [r4, #-8]
 80028c2:	d01c      	beq.n	80028fe <__b2d+0x9a>
 80028c4:	4085      	lsls	r5, r0
 80028c6:	f045 557f 	orr.w	r5, r5, #1069547520	; 0x3fc00000
 80028ca:	f1c0 0c20 	rsb	ip, r0, #32
 80028ce:	f1a4 0e08 	sub.w	lr, r4, #8
 80028d2:	f445 1540 	orr.w	r5, r5, #3145728	; 0x300000
 80028d6:	fa27 f10c 	lsr.w	r1, r7, ip
 80028da:	4576      	cmp	r6, lr
 80028dc:	ea45 0301 	orr.w	r3, r5, r1
 80028e0:	d21e      	bcs.n	8002920 <__b2d+0xbc>
 80028e2:	f854 1c0c 	ldr.w	r1, [r4, #-12]
 80028e6:	fa07 f000 	lsl.w	r0, r7, r0
 80028ea:	fa21 f10c 	lsr.w	r1, r1, ip
 80028ee:	4308      	orrs	r0, r1
 80028f0:	4602      	mov	r2, r0
 80028f2:	ec43 2b10 	vmov	d0, r2, r3
 80028f6:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
 80028f8:	380b      	subs	r0, #11
 80028fa:	d108      	bne.n	800290e <__b2d+0xaa>
 80028fc:	4607      	mov	r7, r0
 80028fe:	f045 537f 	orr.w	r3, r5, #1069547520	; 0x3fc00000
 8002902:	f443 1340 	orr.w	r3, r3, #3145728	; 0x300000
 8002906:	463a      	mov	r2, r7
 8002908:	ec43 2b10 	vmov	d0, r2, r3
 800290c:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
 800290e:	fa05 f000 	lsl.w	r0, r5, r0
 8002912:	f040 537f 	orr.w	r3, r0, #1069547520	; 0x3fc00000
 8002916:	2000      	movs	r0, #0
 8002918:	f443 1340 	orr.w	r3, r3, #3145728	; 0x300000
 800291c:	4602      	mov	r2, r0
 800291e:	e7e8      	b.n	80028f2 <__b2d+0x8e>
 8002920:	fa07 f000 	lsl.w	r0, r7, r0
 8002924:	4602      	mov	r2, r0
 8002926:	e7e4      	b.n	80028f2 <__b2d+0x8e>

08002928 <__d2b>:
 8002928:	e92d 43f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, lr}
 800292c:	ec57 6b10 	vmov	r6, r7, d0
 8002930:	b083      	sub	sp, #12
 8002932:	4688      	mov	r8, r1
 8002934:	2101      	movs	r1, #1
 8002936:	463c      	mov	r4, r7
 8002938:	f3c7 550a 	ubfx	r5, r7, #20, #11
 800293c:	4617      	mov	r7, r2
 800293e:	f7ff fc4f 	bl	80021e0 <_Balloc>
 8002942:	f3c4 0413 	ubfx	r4, r4, #0, #20
 8002946:	4681      	mov	r9, r0
 8002948:	b10d      	cbz	r5, 800294e <__d2b+0x26>
 800294a:	f444 1480 	orr.w	r4, r4, #1048576	; 0x100000
 800294e:	9401      	str	r4, [sp, #4]
 8002950:	b31e      	cbz	r6, 800299a <__d2b+0x72>
 8002952:	a802      	add	r0, sp, #8
 8002954:	f840 6d08 	str.w	r6, [r0, #-8]!
 8002958:	f7ff fd3e 	bl	80023d8 <__lo0bits>
 800295c:	2800      	cmp	r0, #0
 800295e:	d134      	bne.n	80029ca <__d2b+0xa2>
 8002960:	e89d 000c 	ldmia.w	sp, {r2, r3}
 8002964:	f8c9 2014 	str.w	r2, [r9, #20]
 8002968:	2b00      	cmp	r3, #0
 800296a:	bf0c      	ite	eq
 800296c:	2101      	moveq	r1, #1
 800296e:	2102      	movne	r1, #2
 8002970:	f8c9 3018 	str.w	r3, [r9, #24]
 8002974:	f8c9 1010 	str.w	r1, [r9, #16]
 8002978:	b9dd      	cbnz	r5, 80029b2 <__d2b+0x8a>
 800297a:	eb09 0381 	add.w	r3, r9, r1, lsl #2
 800297e:	f2a0 4032 	subw	r0, r0, #1074	; 0x432
 8002982:	f8c8 0000 	str.w	r0, [r8]
 8002986:	6918      	ldr	r0, [r3, #16]
 8002988:	f7ff fd06 	bl	8002398 <__hi0bits>
 800298c:	ebc0 1041 	rsb	r0, r0, r1, lsl #5
 8002990:	6038      	str	r0, [r7, #0]
 8002992:	4648      	mov	r0, r9
 8002994:	b003      	add	sp, #12
 8002996:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
 800299a:	a801      	add	r0, sp, #4
 800299c:	f7ff fd1c 	bl	80023d8 <__lo0bits>
 80029a0:	9b01      	ldr	r3, [sp, #4]
 80029a2:	f8c9 3014 	str.w	r3, [r9, #20]
 80029a6:	2101      	movs	r1, #1
 80029a8:	3020      	adds	r0, #32
 80029aa:	f8c9 1010 	str.w	r1, [r9, #16]
 80029ae:	2d00      	cmp	r5, #0
 80029b0:	d0e3      	beq.n	800297a <__d2b+0x52>
 80029b2:	f2a5 4533 	subw	r5, r5, #1075	; 0x433
 80029b6:	4405      	add	r5, r0
 80029b8:	f1c0 0035 	rsb	r0, r0, #53	; 0x35
 80029bc:	f8c8 5000 	str.w	r5, [r8]
 80029c0:	6038      	str	r0, [r7, #0]
 80029c2:	4648      	mov	r0, r9
 80029c4:	b003      	add	sp, #12
 80029c6:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
 80029ca:	e89d 000a 	ldmia.w	sp, {r1, r3}
 80029ce:	f1c0 0220 	rsb	r2, r0, #32
 80029d2:	fa03 f202 	lsl.w	r2, r3, r2
 80029d6:	430a      	orrs	r2, r1
 80029d8:	40c3      	lsrs	r3, r0
 80029da:	9301      	str	r3, [sp, #4]
 80029dc:	f8c9 2014 	str.w	r2, [r9, #20]
 80029e0:	e7c2      	b.n	8002968 <__d2b+0x40>
 80029e2:	bf00      	nop

080029e4 <__ratio>:
 80029e4:	e92d 43f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, lr}
 80029e8:	b083      	sub	sp, #12
 80029ea:	4688      	mov	r8, r1
 80029ec:	4669      	mov	r1, sp
 80029ee:	4681      	mov	r9, r0
 80029f0:	f7ff ff38 	bl	8002864 <__b2d>
 80029f4:	4640      	mov	r0, r8
 80029f6:	a901      	add	r1, sp, #4
 80029f8:	ec55 4b10 	vmov	r4, r5, d0
 80029fc:	f7ff ff32 	bl	8002864 <__b2d>
 8002a00:	e89d 000a 	ldmia.w	sp, {r1, r3}
 8002a04:	f8d9 0010 	ldr.w	r0, [r9, #16]
 8002a08:	f8d8 2010 	ldr.w	r2, [r8, #16]
 8002a0c:	1acb      	subs	r3, r1, r3
 8002a0e:	1a80      	subs	r0, r0, r2
 8002a10:	eb03 1340 	add.w	r3, r3, r0, lsl #5
 8002a14:	2b00      	cmp	r3, #0
 8002a16:	ec57 6b10 	vmov	r6, r7, d0
 8002a1a:	dd0d      	ble.n	8002a38 <__ratio+0x54>
 8002a1c:	eb05 5903 	add.w	r9, r5, r3, lsl #20
 8002a20:	464d      	mov	r5, r9
 8002a22:	4632      	mov	r2, r6
 8002a24:	463b      	mov	r3, r7
 8002a26:	4620      	mov	r0, r4
 8002a28:	4629      	mov	r1, r5
 8002a2a:	f000 fce9 	bl	8003400 <__aeabi_ddiv>
 8002a2e:	ec41 0b10 	vmov	d0, r0, r1
 8002a32:	b003      	add	sp, #12
 8002a34:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
 8002a38:	ebc3 3303 	rsb	r3, r3, r3, lsl #12
 8002a3c:	eb07 5103 	add.w	r1, r7, r3, lsl #20
 8002a40:	460f      	mov	r7, r1
 8002a42:	e7ee      	b.n	8002a22 <__ratio+0x3e>

08002a44 <_mprec_log10>:
 8002a44:	2817      	cmp	r0, #23
 8002a46:	b5d0      	push	{r4, r6, r7, lr}
 8002a48:	4604      	mov	r4, r0
 8002a4a:	dd0c      	ble.n	8002a66 <_mprec_log10+0x22>
 8002a4c:	490a      	ldr	r1, [pc, #40]	; (8002a78 <_mprec_log10+0x34>)
 8002a4e:	4f0b      	ldr	r7, [pc, #44]	; (8002a7c <_mprec_log10+0x38>)
 8002a50:	2000      	movs	r0, #0
 8002a52:	2600      	movs	r6, #0
 8002a54:	4632      	mov	r2, r6
 8002a56:	463b      	mov	r3, r7
 8002a58:	f000 fba8 	bl	80031ac <__aeabi_dmul>
 8002a5c:	3c01      	subs	r4, #1
 8002a5e:	d1f9      	bne.n	8002a54 <_mprec_log10+0x10>
 8002a60:	ec41 0b10 	vmov	d0, r0, r1
 8002a64:	bdd0      	pop	{r4, r6, r7, pc}
 8002a66:	4b06      	ldr	r3, [pc, #24]	; (8002a80 <_mprec_log10+0x3c>)
 8002a68:	eb03 04c0 	add.w	r4, r3, r0, lsl #3
 8002a6c:	e9d4 0104 	ldrd	r0, r1, [r4, #16]
 8002a70:	ec41 0b10 	vmov	d0, r0, r1
 8002a74:	bdd0      	pop	{r4, r6, r7, pc}
 8002a76:	bf00      	nop
 8002a78:	3ff00000 	.word	0x3ff00000
 8002a7c:	40240000 	.word	0x40240000
 8002a80:	08004700 	.word	0x08004700

08002a84 <__copybits>:
 8002a84:	b470      	push	{r4, r5, r6}
 8002a86:	6914      	ldr	r4, [r2, #16]
 8002a88:	1e4e      	subs	r6, r1, #1
 8002a8a:	f102 0314 	add.w	r3, r2, #20
 8002a8e:	1176      	asrs	r6, r6, #5
 8002a90:	eb03 0184 	add.w	r1, r3, r4, lsl #2
 8002a94:	3601      	adds	r6, #1
 8002a96:	428b      	cmp	r3, r1
 8002a98:	eb00 0686 	add.w	r6, r0, r6, lsl #2
 8002a9c:	d20c      	bcs.n	8002ab8 <__copybits+0x34>
 8002a9e:	1f04      	subs	r4, r0, #4
 8002aa0:	f853 5b04 	ldr.w	r5, [r3], #4
 8002aa4:	f844 5f04 	str.w	r5, [r4, #4]!
 8002aa8:	4299      	cmp	r1, r3
 8002aaa:	d8f9      	bhi.n	8002aa0 <__copybits+0x1c>
 8002aac:	1a8b      	subs	r3, r1, r2
 8002aae:	3b15      	subs	r3, #21
 8002ab0:	f023 0303 	bic.w	r3, r3, #3
 8002ab4:	3304      	adds	r3, #4
 8002ab6:	4418      	add	r0, r3
 8002ab8:	4286      	cmp	r6, r0
 8002aba:	d904      	bls.n	8002ac6 <__copybits+0x42>
 8002abc:	2300      	movs	r3, #0
 8002abe:	f840 3b04 	str.w	r3, [r0], #4
 8002ac2:	4286      	cmp	r6, r0
 8002ac4:	d8fb      	bhi.n	8002abe <__copybits+0x3a>
 8002ac6:	bc70      	pop	{r4, r5, r6}
 8002ac8:	4770      	bx	lr
 8002aca:	bf00      	nop

08002acc <__any_on>:
 8002acc:	6903      	ldr	r3, [r0, #16]
 8002ace:	114a      	asrs	r2, r1, #5
 8002ad0:	4293      	cmp	r3, r2
 8002ad2:	b410      	push	{r4}
 8002ad4:	f100 0414 	add.w	r4, r0, #20
 8002ad8:	da10      	bge.n	8002afc <__any_on+0x30>
 8002ada:	eb04 0383 	add.w	r3, r4, r3, lsl #2
 8002ade:	429c      	cmp	r4, r3
 8002ae0:	d221      	bcs.n	8002b26 <__any_on+0x5a>
 8002ae2:	f853 0c04 	ldr.w	r0, [r3, #-4]
 8002ae6:	3b04      	subs	r3, #4
 8002ae8:	b118      	cbz	r0, 8002af2 <__any_on+0x26>
 8002aea:	e015      	b.n	8002b18 <__any_on+0x4c>
 8002aec:	f853 2d04 	ldr.w	r2, [r3, #-4]!
 8002af0:	b992      	cbnz	r2, 8002b18 <__any_on+0x4c>
 8002af2:	429c      	cmp	r4, r3
 8002af4:	d3fa      	bcc.n	8002aec <__any_on+0x20>
 8002af6:	f85d 4b04 	ldr.w	r4, [sp], #4
 8002afa:	4770      	bx	lr
 8002afc:	dd10      	ble.n	8002b20 <__any_on+0x54>
 8002afe:	f011 011f 	ands.w	r1, r1, #31
 8002b02:	d00d      	beq.n	8002b20 <__any_on+0x54>
 8002b04:	f854 0022 	ldr.w	r0, [r4, r2, lsl #2]
 8002b08:	fa20 f301 	lsr.w	r3, r0, r1
 8002b0c:	fa03 f101 	lsl.w	r1, r3, r1
 8002b10:	4288      	cmp	r0, r1
 8002b12:	eb04 0382 	add.w	r3, r4, r2, lsl #2
 8002b16:	d0e2      	beq.n	8002ade <__any_on+0x12>
 8002b18:	2001      	movs	r0, #1
 8002b1a:	f85d 4b04 	ldr.w	r4, [sp], #4
 8002b1e:	4770      	bx	lr
 8002b20:	eb04 0382 	add.w	r3, r4, r2, lsl #2
 8002b24:	e7db      	b.n	8002ade <__any_on+0x12>
 8002b26:	2000      	movs	r0, #0
 8002b28:	e7e5      	b.n	8002af6 <__any_on+0x2a>
 8002b2a:	bf00      	nop

08002b2c <_calloc_r>:
 8002b2c:	b538      	push	{r3, r4, r5, lr}
 8002b2e:	fb02 f401 	mul.w	r4, r2, r1
 8002b32:	4621      	mov	r1, r4
 8002b34:	f001 f82e 	bl	8003b94 <_malloc_r>
 8002b38:	4605      	mov	r5, r0
 8002b3a:	b118      	cbz	r0, 8002b44 <_calloc_r+0x18>
 8002b3c:	4622      	mov	r2, r4
 8002b3e:	2100      	movs	r1, #0
 8002b40:	f7fd fc52 	bl	80003e8 <memset>
 8002b44:	4628      	mov	r0, r5
 8002b46:	bd38      	pop	{r3, r4, r5, pc}
	...
 8002b60:	eba2 0003 	sub.w	r0, r2, r3
 8002b64:	4770      	bx	lr
 8002b66:	bf00      	nop

08002b68 <strcmp>:
 8002b68:	7802      	ldrb	r2, [r0, #0]
 8002b6a:	780b      	ldrb	r3, [r1, #0]
 8002b6c:	2a01      	cmp	r2, #1
 8002b6e:	bf28      	it	cs
 8002b70:	429a      	cmpcs	r2, r3
 8002b72:	d1f5      	bne.n	8002b60 <_calloc_r+0x34>
 8002b74:	e96d 4504 	strd	r4, r5, [sp, #-16]!
 8002b78:	ea40 0401 	orr.w	r4, r0, r1
 8002b7c:	e9cd 6702 	strd	r6, r7, [sp, #8]
 8002b80:	f06f 0c00 	mvn.w	ip, #0
 8002b84:	ea4f 7244 	mov.w	r2, r4, lsl #29
 8002b88:	b312      	cbz	r2, 8002bd0 <strcmp+0x68>
 8002b8a:	ea80 0401 	eor.w	r4, r0, r1
 8002b8e:	f014 0f07 	tst.w	r4, #7
 8002b92:	d16a      	bne.n	8002c6a <strcmp+0x102>
 8002b94:	f000 0407 	and.w	r4, r0, #7
 8002b98:	f020 0007 	bic.w	r0, r0, #7
 8002b9c:	f004 0503 	and.w	r5, r4, #3
 8002ba0:	f021 0107 	bic.w	r1, r1, #7
 8002ba4:	ea4f 05c5 	mov.w	r5, r5, lsl #3
 8002ba8:	e8f0 2304 	ldrd	r2, r3, [r0], #16
 8002bac:	f014 0f04 	tst.w	r4, #4
 8002bb0:	e8f1 6704 	ldrd	r6, r7, [r1], #16
 8002bb4:	fa0c f405 	lsl.w	r4, ip, r5
 8002bb8:	ea62 0204 	orn	r2, r2, r4
 8002bbc:	ea66 0604 	orn	r6, r6, r4
 8002bc0:	d00a      	beq.n	8002bd8 <strcmp+0x70>
 8002bc2:	ea63 0304 	orn	r3, r3, r4
 8002bc6:	4662      	mov	r2, ip
 8002bc8:	ea67 0704 	orn	r7, r7, r4
 8002bcc:	4666      	mov	r6, ip
 8002bce:	e003      	b.n	8002bd8 <strcmp+0x70>
 8002bd0:	e8f0 2304 	ldrd	r2, r3, [r0], #16
 8002bd4:	e8f1 6704 	ldrd	r6, r7, [r1], #16
 8002bd8:	fa82 f54c 	uadd8	r5, r2, ip
 8002bdc:	ea82 0406 	eor.w	r4, r2, r6
 8002be0:	faa4 f48c 	sel	r4, r4, ip
 8002be4:	bb6c      	cbnz	r4, 8002c42 <strcmp+0xda>
 8002be6:	fa83 f54c 	uadd8	r5, r3, ip
 8002bea:	ea83 0507 	eor.w	r5, r3, r7
 8002bee:	faa5 f58c 	sel	r5, r5, ip
 8002bf2:	b995      	cbnz	r5, 8002c1a <strcmp+0xb2>
 8002bf4:	e950 2302 	ldrd	r2, r3, [r0, #-8]
 8002bf8:	e951 6702 	ldrd	r6, r7, [r1, #-8]
 8002bfc:	fa82 f54c 	uadd8	r5, r2, ip
 8002c00:	ea82 0406 	eor.w	r4, r2, r6
 8002c04:	faa4 f48c 	sel	r4, r4, ip
 8002c08:	fa83 f54c 	uadd8	r5, r3, ip
 8002c0c:	ea83 0507 	eor.w	r5, r3, r7
 8002c10:	faa5 f58c 	sel	r5, r5, ip
 8002c14:	4325      	orrs	r5, r4
 8002c16:	d0db      	beq.n	8002bd0 <strcmp+0x68>
 8002c18:	b99c      	cbnz	r4, 8002c42 <strcmp+0xda>
 8002c1a:	ba2d      	rev	r5, r5
 8002c1c:	fab5 f485 	clz	r4, r5
 8002c20:	f024 0407 	bic.w	r4, r4, #7
 8002c24:	fa27 f104 	lsr.w	r1, r7, r4
 8002c28:	e9dd 6702 	ldrd	r6, r7, [sp, #8]
 8002c2c:	fa23 f304 	lsr.w	r3, r3, r4
 8002c30:	f003 00ff 	and.w	r0, r3, #255	; 0xff
 8002c34:	f001 01ff 	and.w	r1, r1, #255	; 0xff
 8002c38:	e8fd 4504 	ldrd	r4, r5, [sp], #16
 8002c3c:	eba0 0001 	sub.w	r0, r0, r1
 8002c40:	4770      	bx	lr
 8002c42:	ba24      	rev	r4, r4
 8002c44:	fab4 f484 	clz	r4, r4
 8002c48:	f024 0407 	bic.w	r4, r4, #7
 8002c4c:	fa26 f104 	lsr.w	r1, r6, r4
 8002c50:	e9dd 6702 	ldrd	r6, r7, [sp, #8]
 8002c54:	fa22 f204 	lsr.w	r2, r2, r4
 8002c58:	f002 00ff 	and.w	r0, r2, #255	; 0xff
 8002c5c:	f001 01ff 	and.w	r1, r1, #255	; 0xff
 8002c60:	e8fd 4504 	ldrd	r4, r5, [sp], #16
 8002c64:	eba0 0001 	sub.w	r0, r0, r1
 8002c68:	4770      	bx	lr
 8002c6a:	f014 0f03 	tst.w	r4, #3
 8002c6e:	d13c      	bne.n	8002cea <strcmp+0x182>
 8002c70:	f010 0403 	ands.w	r4, r0, #3
 8002c74:	d128      	bne.n	8002cc8 <strcmp+0x160>
 8002c76:	f850 2b08 	ldr.w	r2, [r0], #8
 8002c7a:	f851 3b08 	ldr.w	r3, [r1], #8
 8002c7e:	fa82 f54c 	uadd8	r5, r2, ip
 8002c82:	ea82 0503 	eor.w	r5, r2, r3
 8002c86:	faa5 f58c 	sel	r5, r5, ip
 8002c8a:	b95d      	cbnz	r5, 8002ca4 <strcmp+0x13c>
 8002c8c:	f850 2c04 	ldr.w	r2, [r0, #-4]
 8002c90:	f851 3c04 	ldr.w	r3, [r1, #-4]
 8002c94:	fa82 f54c 	uadd8	r5, r2, ip
 8002c98:	ea82 0503 	eor.w	r5, r2, r3
 8002c9c:	faa5 f58c 	sel	r5, r5, ip
 8002ca0:	2d00      	cmp	r5, #0
 8002ca2:	d0e8      	beq.n	8002c76 <strcmp+0x10e>
 8002ca4:	ba2d      	rev	r5, r5
 8002ca6:	fab5 f485 	clz	r4, r5
 8002caa:	f024 0407 	bic.w	r4, r4, #7
 8002cae:	fa23 f104 	lsr.w	r1, r3, r4
 8002cb2:	fa22 f204 	lsr.w	r2, r2, r4
 8002cb6:	f002 00ff 	and.w	r0, r2, #255	; 0xff
 8002cba:	f001 01ff 	and.w	r1, r1, #255	; 0xff
 8002cbe:	e8fd 4504 	ldrd	r4, r5, [sp], #16
 8002cc2:	eba0 0001 	sub.w	r0, r0, r1
 8002cc6:	4770      	bx	lr
 8002cc8:	ea4f 04c4 	mov.w	r4, r4, lsl #3
 8002ccc:	f020 0003 	bic.w	r0, r0, #3
 8002cd0:	f850 2b08 	ldr.w	r2, [r0], #8
 8002cd4:	f021 0103 	bic.w	r1, r1, #3
 8002cd8:	f851 3b08 	ldr.w	r3, [r1], #8
 8002cdc:	fa0c f404 	lsl.w	r4, ip, r4
 8002ce0:	ea62 0204 	orn	r2, r2, r4
 8002ce4:	ea63 0304 	orn	r3, r3, r4
 8002ce8:	e7c9      	b.n	8002c7e <strcmp+0x116>
 8002cea:	f010 0403 	ands.w	r4, r0, #3
 8002cee:	d01a      	beq.n	8002d26 <strcmp+0x1be>
 8002cf0:	eba1 0104 	sub.w	r1, r1, r4
 8002cf4:	f020 0003 	bic.w	r0, r0, #3
 8002cf8:	07e4      	lsls	r4, r4, #31
 8002cfa:	f850 2b04 	ldr.w	r2, [r0], #4
 8002cfe:	d006      	beq.n	8002d0e <strcmp+0x1a6>
 8002d00:	d20f      	bcs.n	8002d22 <strcmp+0x1ba>
 8002d02:	788b      	ldrb	r3, [r1, #2]
 8002d04:	fa5f f4a2 	uxtb.w	r4, r2, ror #16
 8002d08:	1ae4      	subs	r4, r4, r3
 8002d0a:	d106      	bne.n	8002d1a <strcmp+0x1b2>
 8002d0c:	b12b      	cbz	r3, 8002d1a <strcmp+0x1b2>
 8002d0e:	78cb      	ldrb	r3, [r1, #3]
 8002d10:	fa5f f4b2 	uxtb.w	r4, r2, ror #24
 8002d14:	1ae4      	subs	r4, r4, r3
 8002d16:	d100      	bne.n	8002d1a <strcmp+0x1b2>
 8002d18:	b91b      	cbnz	r3, 8002d22 <strcmp+0x1ba>
 8002d1a:	4620      	mov	r0, r4
 8002d1c:	f85d 4b10 	ldr.w	r4, [sp], #16
 8002d20:	4770      	bx	lr
 8002d22:	f101 0104 	add.w	r1, r1, #4
 8002d26:	f850 2b04 	ldr.w	r2, [r0], #4
 8002d2a:	07cc      	lsls	r4, r1, #31
 8002d2c:	f021 0103 	bic.w	r1, r1, #3
 8002d30:	f851 3b04 	ldr.w	r3, [r1], #4
 8002d34:	d848      	bhi.n	8002dc8 <strcmp+0x260>
 8002d36:	d224      	bcs.n	8002d82 <strcmp+0x21a>
 8002d38:	f022 447f 	bic.w	r4, r2, #4278190080	; 0xff000000
 8002d3c:	fa82 f54c 	uadd8	r5, r2, ip
 8002d40:	ea94 2513 	eors.w	r5, r4, r3, lsr #8
 8002d44:	faa5 f58c 	sel	r5, r5, ip
 8002d48:	d10a      	bne.n	8002d60 <strcmp+0x1f8>
 8002d4a:	b965      	cbnz	r5, 8002d66 <strcmp+0x1fe>
 8002d4c:	f851 3b04 	ldr.w	r3, [r1], #4
 8002d50:	ea84 0402 	eor.w	r4, r4, r2
 8002d54:	ebb4 6f03 	cmp.w	r4, r3, lsl #24
 8002d58:	d10e      	bne.n	8002d78 <strcmp+0x210>
 8002d5a:	f850 2b04 	ldr.w	r2, [r0], #4
 8002d5e:	e7eb      	b.n	8002d38 <strcmp+0x1d0>
 8002d60:	ea4f 2313 	mov.w	r3, r3, lsr #8
 8002d64:	e055      	b.n	8002e12 <strcmp+0x2aa>
 8002d66:	f035 457f 	bics.w	r5, r5, #4278190080	; 0xff000000
 8002d6a:	d14d      	bne.n	8002e08 <strcmp+0x2a0>
 8002d6c:	7808      	ldrb	r0, [r1, #0]
 8002d6e:	e8fd 4504 	ldrd	r4, r5, [sp], #16
 8002d72:	f1c0 0000 	rsb	r0, r0, #0
 8002d76:	4770      	bx	lr
 8002d78:	ea4f 6212 	mov.w	r2, r2, lsr #24
 8002d7c:	f003 03ff 	and.w	r3, r3, #255	; 0xff
 8002d80:	e047      	b.n	8002e12 <strcmp+0x2aa>
 8002d82:	ea02 441c 	and.w	r4, r2, ip, lsr #16
 8002d86:	fa82 f54c 	uadd8	r5, r2, ip
 8002d8a:	ea94 4513 	eors.w	r5, r4, r3, lsr #16
 8002d8e:	faa5 f58c 	sel	r5, r5, ip
 8002d92:	d10a      	bne.n	8002daa <strcmp+0x242>
 8002d94:	b965      	cbnz	r5, 8002db0 <strcmp+0x248>
 8002d96:	f851 3b04 	ldr.w	r3, [r1], #4
 8002d9a:	ea84 0402 	eor.w	r4, r4, r2
 8002d9e:	ebb4 4f03 	cmp.w	r4, r3, lsl #16
 8002da2:	d10c      	bne.n	8002dbe <strcmp+0x256>
 8002da4:	f850 2b04 	ldr.w	r2, [r0], #4
 8002da8:	e7eb      	b.n	8002d82 <strcmp+0x21a>
 8002daa:	ea4f 4313 	mov.w	r3, r3, lsr #16
 8002dae:	e030      	b.n	8002e12 <strcmp+0x2aa>
 8002db0:	ea15 451c 	ands.w	r5, r5, ip, lsr #16
 8002db4:	d128      	bne.n	8002e08 <strcmp+0x2a0>
 8002db6:	880b      	ldrh	r3, [r1, #0]
 8002db8:	ea4f 4212 	mov.w	r2, r2, lsr #16
 8002dbc:	e029      	b.n	8002e12 <strcmp+0x2aa>
 8002dbe:	ea4f 4212 	mov.w	r2, r2, lsr #16
 8002dc2:	ea03 431c 	and.w	r3, r3, ip, lsr #16
 8002dc6:	e024      	b.n	8002e12 <strcmp+0x2aa>
 8002dc8:	f002 04ff 	and.w	r4, r2, #255	; 0xff
 8002dcc:	fa82 f54c 	uadd8	r5, r2, ip
 8002dd0:	ea94 6513 	eors.w	r5, r4, r3, lsr #24
 8002dd4:	faa5 f58c 	sel	r5, r5, ip
 8002dd8:	d10a      	bne.n	8002df0 <strcmp+0x288>
 8002dda:	b965      	cbnz	r5, 8002df6 <strcmp+0x28e>
 8002ddc:	f851 3b04 	ldr.w	r3, [r1], #4
 8002de0:	ea84 0402 	eor.w	r4, r4, r2
 8002de4:	ebb4 2f03 	cmp.w	r4, r3, lsl #8
 8002de8:	d109      	bne.n	8002dfe <strcmp+0x296>
 8002dea:	f850 2b04 	ldr.w	r2, [r0], #4
 8002dee:	e7eb      	b.n	8002dc8 <strcmp+0x260>
 8002df0:	ea4f 6313 	mov.w	r3, r3, lsr #24
 8002df4:	e00d      	b.n	8002e12 <strcmp+0x2aa>
 8002df6:	f015 0fff 	tst.w	r5, #255	; 0xff
 8002dfa:	d105      	bne.n	8002e08 <strcmp+0x2a0>
 8002dfc:	680b      	ldr	r3, [r1, #0]
 8002dfe:	ea4f 2212 	mov.w	r2, r2, lsr #8
 8002e02:	f023 437f 	bic.w	r3, r3, #4278190080	; 0xff000000
 8002e06:	e004      	b.n	8002e12 <strcmp+0x2aa>
 8002e08:	f04f 0000 	mov.w	r0, #0
 8002e0c:	e8fd 4504 	ldrd	r4, r5, [sp], #16
 8002e10:	4770      	bx	lr
 8002e12:	ba12      	rev	r2, r2
 8002e14:	ba1b      	rev	r3, r3
 8002e16:	fa82 f44c 	uadd8	r4, r2, ip
 8002e1a:	ea82 0403 	eor.w	r4, r2, r3
 8002e1e:	faa4 f58c 	sel	r5, r4, ip
 8002e22:	fab5 f485 	clz	r4, r5
 8002e26:	fa02 f204 	lsl.w	r2, r2, r4
 8002e2a:	fa03 f304 	lsl.w	r3, r3, r4
 8002e2e:	ea4f 6012 	mov.w	r0, r2, lsr #24
 8002e32:	e8fd 4504 	ldrd	r4, r5, [sp], #16
 8002e36:	eba0 6013 	sub.w	r0, r0, r3, lsr #24
 8002e3a:	4770      	bx	lr

08002e3c <__aeabi_drsub>:
 8002e3c:	f081 4100 	eor.w	r1, r1, #2147483648	; 0x80000000
 8002e40:	e002      	b.n	8002e48 <__adddf3>
 8002e42:	bf00      	nop

08002e44 <__aeabi_dsub>:
 8002e44:	f083 4300 	eor.w	r3, r3, #2147483648	; 0x80000000

08002e48 <__adddf3>:
 8002e48:	b530      	push	{r4, r5, lr}
 8002e4a:	ea4f 0441 	mov.w	r4, r1, lsl #1
 8002e4e:	ea4f 0543 	mov.w	r5, r3, lsl #1
 8002e52:	ea94 0f05 	teq	r4, r5
 8002e56:	bf08      	it	eq
 8002e58:	ea90 0f02 	teqeq	r0, r2
 8002e5c:	bf1f      	itttt	ne
 8002e5e:	ea54 0c00 	orrsne.w	ip, r4, r0
 8002e62:	ea55 0c02 	orrsne.w	ip, r5, r2
 8002e66:	ea7f 5c64 	mvnsne.w	ip, r4, asr #21
 8002e6a:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 8002e6e:	f000 80e2 	beq.w	8003036 <__adddf3+0x1ee>
 8002e72:	ea4f 5454 	mov.w	r4, r4, lsr #21
 8002e76:	ebd4 5555 	rsbs	r5, r4, r5, lsr #21
 8002e7a:	bfb8      	it	lt
 8002e7c:	426d      	neglt	r5, r5
 8002e7e:	dd0c      	ble.n	8002e9a <__adddf3+0x52>
 8002e80:	442c      	add	r4, r5
 8002e82:	ea80 0202 	eor.w	r2, r0, r2
 8002e86:	ea81 0303 	eor.w	r3, r1, r3
 8002e8a:	ea82 0000 	eor.w	r0, r2, r0
 8002e8e:	ea83 0101 	eor.w	r1, r3, r1
 8002e92:	ea80 0202 	eor.w	r2, r0, r2
 8002e96:	ea81 0303 	eor.w	r3, r1, r3
 8002e9a:	2d36      	cmp	r5, #54	; 0x36
 8002e9c:	bf88      	it	hi
 8002e9e:	bd30      	pophi	{r4, r5, pc}
 8002ea0:	f011 4f00 	tst.w	r1, #2147483648	; 0x80000000
 8002ea4:	ea4f 3101 	mov.w	r1, r1, lsl #12
 8002ea8:	f44f 1c80 	mov.w	ip, #1048576	; 0x100000
 8002eac:	ea4c 3111 	orr.w	r1, ip, r1, lsr #12
 8002eb0:	d002      	beq.n	8002eb8 <__adddf3+0x70>
 8002eb2:	4240      	negs	r0, r0
 8002eb4:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 8002eb8:	f013 4f00 	tst.w	r3, #2147483648	; 0x80000000
 8002ebc:	ea4f 3303 	mov.w	r3, r3, lsl #12
 8002ec0:	ea4c 3313 	orr.w	r3, ip, r3, lsr #12
 8002ec4:	d002      	beq.n	8002ecc <__adddf3+0x84>
 8002ec6:	4252      	negs	r2, r2
 8002ec8:	eb63 0343 	sbc.w	r3, r3, r3, lsl #1
 8002ecc:	ea94 0f05 	teq	r4, r5
 8002ed0:	f000 80a7 	beq.w	8003022 <__adddf3+0x1da>
 8002ed4:	f1a4 0401 	sub.w	r4, r4, #1
 8002ed8:	f1d5 0e20 	rsbs	lr, r5, #32
 8002edc:	db0d      	blt.n	8002efa <__adddf3+0xb2>
 8002ede:	fa02 fc0e 	lsl.w	ip, r2, lr
 8002ee2:	fa22 f205 	lsr.w	r2, r2, r5
 8002ee6:	1880      	adds	r0, r0, r2
 8002ee8:	f141 0100 	adc.w	r1, r1, #0
 8002eec:	fa03 f20e 	lsl.w	r2, r3, lr
 8002ef0:	1880      	adds	r0, r0, r2
 8002ef2:	fa43 f305 	asr.w	r3, r3, r5
 8002ef6:	4159      	adcs	r1, r3
 8002ef8:	e00e      	b.n	8002f18 <__adddf3+0xd0>
 8002efa:	f1a5 0520 	sub.w	r5, r5, #32
 8002efe:	f10e 0e20 	add.w	lr, lr, #32
 8002f02:	2a01      	cmp	r2, #1
 8002f04:	fa03 fc0e 	lsl.w	ip, r3, lr
 8002f08:	bf28      	it	cs
 8002f0a:	f04c 0c02 	orrcs.w	ip, ip, #2
 8002f0e:	fa43 f305 	asr.w	r3, r3, r5
 8002f12:	18c0      	adds	r0, r0, r3
 8002f14:	eb51 71e3 	adcs.w	r1, r1, r3, asr #31
 8002f18:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8002f1c:	d507      	bpl.n	8002f2e <__adddf3+0xe6>
 8002f1e:	f04f 0e00 	mov.w	lr, #0
 8002f22:	f1dc 0c00 	rsbs	ip, ip, #0
 8002f26:	eb7e 0000 	sbcs.w	r0, lr, r0
 8002f2a:	eb6e 0101 	sbc.w	r1, lr, r1
 8002f2e:	f5b1 1f80 	cmp.w	r1, #1048576	; 0x100000
 8002f32:	d31b      	bcc.n	8002f6c <__adddf3+0x124>
 8002f34:	f5b1 1f00 	cmp.w	r1, #2097152	; 0x200000
 8002f38:	d30c      	bcc.n	8002f54 <__adddf3+0x10c>
 8002f3a:	0849      	lsrs	r1, r1, #1
 8002f3c:	ea5f 0030 	movs.w	r0, r0, rrx
 8002f40:	ea4f 0c3c 	mov.w	ip, ip, rrx
 8002f44:	f104 0401 	add.w	r4, r4, #1
 8002f48:	ea4f 5244 	mov.w	r2, r4, lsl #21
 8002f4c:	f512 0f80 	cmn.w	r2, #4194304	; 0x400000
 8002f50:	f080 809a 	bcs.w	8003088 <__adddf3+0x240>
 8002f54:	f1bc 4f00 	cmp.w	ip, #2147483648	; 0x80000000
 8002f58:	bf08      	it	eq
 8002f5a:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 8002f5e:	f150 0000 	adcs.w	r0, r0, #0
 8002f62:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 8002f66:	ea41 0105 	orr.w	r1, r1, r5
 8002f6a:	bd30      	pop	{r4, r5, pc}
 8002f6c:	ea5f 0c4c 	movs.w	ip, ip, lsl #1
 8002f70:	4140      	adcs	r0, r0
 8002f72:	eb41 0101 	adc.w	r1, r1, r1
 8002f76:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 8002f7a:	f1a4 0401 	sub.w	r4, r4, #1
 8002f7e:	d1e9      	bne.n	8002f54 <__adddf3+0x10c>
 8002f80:	f091 0f00 	teq	r1, #0
 8002f84:	bf04      	itt	eq
 8002f86:	4601      	moveq	r1, r0
 8002f88:	2000      	moveq	r0, #0
 8002f8a:	fab1 f381 	clz	r3, r1
 8002f8e:	bf08      	it	eq
 8002f90:	3320      	addeq	r3, #32
 8002f92:	f1a3 030b 	sub.w	r3, r3, #11
 8002f96:	f1b3 0220 	subs.w	r2, r3, #32
 8002f9a:	da0c      	bge.n	8002fb6 <__adddf3+0x16e>
 8002f9c:	320c      	adds	r2, #12
 8002f9e:	dd08      	ble.n	8002fb2 <__adddf3+0x16a>
 8002fa0:	f102 0c14 	add.w	ip, r2, #20
 8002fa4:	f1c2 020c 	rsb	r2, r2, #12
 8002fa8:	fa01 f00c 	lsl.w	r0, r1, ip
 8002fac:	fa21 f102 	lsr.w	r1, r1, r2
 8002fb0:	e00c      	b.n	8002fcc <__adddf3+0x184>
 8002fb2:	f102 0214 	add.w	r2, r2, #20
 8002fb6:	bfd8      	it	le
 8002fb8:	f1c2 0c20 	rsble	ip, r2, #32
 8002fbc:	fa01 f102 	lsl.w	r1, r1, r2
 8002fc0:	fa20 fc0c 	lsr.w	ip, r0, ip
 8002fc4:	bfdc      	itt	le
 8002fc6:	ea41 010c 	orrle.w	r1, r1, ip
 8002fca:	4090      	lslle	r0, r2
 8002fcc:	1ae4      	subs	r4, r4, r3
 8002fce:	bfa2      	ittt	ge
 8002fd0:	eb01 5104 	addge.w	r1, r1, r4, lsl #20
 8002fd4:	4329      	orrge	r1, r5
 8002fd6:	bd30      	popge	{r4, r5, pc}
 8002fd8:	ea6f 0404 	mvn.w	r4, r4
 8002fdc:	3c1f      	subs	r4, #31
 8002fde:	da1c      	bge.n	800301a <__adddf3+0x1d2>
 8002fe0:	340c      	adds	r4, #12
 8002fe2:	dc0e      	bgt.n	8003002 <__adddf3+0x1ba>
 8002fe4:	f104 0414 	add.w	r4, r4, #20
 8002fe8:	f1c4 0220 	rsb	r2, r4, #32
 8002fec:	fa20 f004 	lsr.w	r0, r0, r4
 8002ff0:	fa01 f302 	lsl.w	r3, r1, r2
 8002ff4:	ea40 0003 	orr.w	r0, r0, r3
 8002ff8:	fa21 f304 	lsr.w	r3, r1, r4
 8002ffc:	ea45 0103 	orr.w	r1, r5, r3
 8003000:	bd30      	pop	{r4, r5, pc}
 8003002:	f1c4 040c 	rsb	r4, r4, #12
 8003006:	f1c4 0220 	rsb	r2, r4, #32
 800300a:	fa20 f002 	lsr.w	r0, r0, r2
 800300e:	fa01 f304 	lsl.w	r3, r1, r4
 8003012:	ea40 0003 	orr.w	r0, r0, r3
 8003016:	4629      	mov	r1, r5
 8003018:	bd30      	pop	{r4, r5, pc}
 800301a:	fa21 f004 	lsr.w	r0, r1, r4
 800301e:	4629      	mov	r1, r5
 8003020:	bd30      	pop	{r4, r5, pc}
 8003022:	f094 0f00 	teq	r4, #0
 8003026:	f483 1380 	eor.w	r3, r3, #1048576	; 0x100000
 800302a:	bf06      	itte	eq
 800302c:	f481 1180 	eoreq.w	r1, r1, #1048576	; 0x100000
 8003030:	3401      	addeq	r4, #1
 8003032:	3d01      	subne	r5, #1
 8003034:	e74e      	b.n	8002ed4 <__adddf3+0x8c>
 8003036:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 800303a:	bf18      	it	ne
 800303c:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 8003040:	d029      	beq.n	8003096 <__adddf3+0x24e>
 8003042:	ea94 0f05 	teq	r4, r5
 8003046:	bf08      	it	eq
 8003048:	ea90 0f02 	teqeq	r0, r2
 800304c:	d005      	beq.n	800305a <__adddf3+0x212>
 800304e:	ea54 0c00 	orrs.w	ip, r4, r0
 8003052:	bf04      	itt	eq
 8003054:	4619      	moveq	r1, r3
 8003056:	4610      	moveq	r0, r2
 8003058:	bd30      	pop	{r4, r5, pc}
 800305a:	ea91 0f03 	teq	r1, r3
 800305e:	bf1e      	ittt	ne
 8003060:	2100      	movne	r1, #0
 8003062:	2000      	movne	r0, #0
 8003064:	bd30      	popne	{r4, r5, pc}
 8003066:	ea5f 5c54 	movs.w	ip, r4, lsr #21
 800306a:	d105      	bne.n	8003078 <__adddf3+0x230>
 800306c:	0040      	lsls	r0, r0, #1
 800306e:	4149      	adcs	r1, r1
 8003070:	bf28      	it	cs
 8003072:	f041 4100 	orrcs.w	r1, r1, #2147483648	; 0x80000000
 8003076:	bd30      	pop	{r4, r5, pc}
 8003078:	f514 0480 	adds.w	r4, r4, #4194304	; 0x400000
 800307c:	bf3c      	itt	cc
 800307e:	f501 1180 	addcc.w	r1, r1, #1048576	; 0x100000
 8003082:	bd30      	popcc	{r4, r5, pc}
 8003084:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8003088:	f045 41fe 	orr.w	r1, r5, #2130706432	; 0x7f000000
 800308c:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 8003090:	f04f 0000 	mov.w	r0, #0
 8003094:	bd30      	pop	{r4, r5, pc}
 8003096:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 800309a:	bf1a      	itte	ne
 800309c:	4619      	movne	r1, r3
 800309e:	4610      	movne	r0, r2
 80030a0:	ea7f 5c65 	mvnseq.w	ip, r5, asr #21
 80030a4:	bf1c      	itt	ne
 80030a6:	460b      	movne	r3, r1
 80030a8:	4602      	movne	r2, r0
 80030aa:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 80030ae:	bf06      	itte	eq
 80030b0:	ea52 3503 	orrseq.w	r5, r2, r3, lsl #12
 80030b4:	ea91 0f03 	teqeq	r1, r3
 80030b8:	f441 2100 	orrne.w	r1, r1, #524288	; 0x80000
 80030bc:	bd30      	pop	{r4, r5, pc}
 80030be:	bf00      	nop

080030c0 <__aeabi_ui2d>:
 80030c0:	f090 0f00 	teq	r0, #0
 80030c4:	bf04      	itt	eq
 80030c6:	2100      	moveq	r1, #0
 80030c8:	4770      	bxeq	lr
 80030ca:	b530      	push	{r4, r5, lr}
 80030cc:	f44f 6480 	mov.w	r4, #1024	; 0x400
 80030d0:	f104 0432 	add.w	r4, r4, #50	; 0x32
 80030d4:	f04f 0500 	mov.w	r5, #0
 80030d8:	f04f 0100 	mov.w	r1, #0
 80030dc:	e750      	b.n	8002f80 <__adddf3+0x138>
 80030de:	bf00      	nop

080030e0 <__aeabi_i2d>:
 80030e0:	f090 0f00 	teq	r0, #0
 80030e4:	bf04      	itt	eq
 80030e6:	2100      	moveq	r1, #0
 80030e8:	4770      	bxeq	lr
 80030ea:	b530      	push	{r4, r5, lr}
 80030ec:	f44f 6480 	mov.w	r4, #1024	; 0x400
 80030f0:	f104 0432 	add.w	r4, r4, #50	; 0x32
 80030f4:	f010 4500 	ands.w	r5, r0, #2147483648	; 0x80000000
 80030f8:	bf48      	it	mi
 80030fa:	4240      	negmi	r0, r0
 80030fc:	f04f 0100 	mov.w	r1, #0
 8003100:	e73e      	b.n	8002f80 <__adddf3+0x138>
 8003102:	bf00      	nop

08003104 <__aeabi_f2d>:
 8003104:	0042      	lsls	r2, r0, #1
 8003106:	ea4f 01e2 	mov.w	r1, r2, asr #3
 800310a:	ea4f 0131 	mov.w	r1, r1, rrx
 800310e:	ea4f 7002 	mov.w	r0, r2, lsl #28
 8003112:	bf1f      	itttt	ne
 8003114:	f012 437f 	andsne.w	r3, r2, #4278190080	; 0xff000000
 8003118:	f093 4f7f 	teqne	r3, #4278190080	; 0xff000000
 800311c:	f081 5160 	eorne.w	r1, r1, #939524096	; 0x38000000
 8003120:	4770      	bxne	lr
 8003122:	f092 0f00 	teq	r2, #0
 8003126:	bf14      	ite	ne
 8003128:	f093 4f7f 	teqne	r3, #4278190080	; 0xff000000
 800312c:	4770      	bxeq	lr
 800312e:	b530      	push	{r4, r5, lr}
 8003130:	f44f 7460 	mov.w	r4, #896	; 0x380
 8003134:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8003138:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 800313c:	e720      	b.n	8002f80 <__adddf3+0x138>
 800313e:	bf00      	nop

08003140 <__aeabi_ul2d>:
 8003140:	ea50 0201 	orrs.w	r2, r0, r1
 8003144:	bf08      	it	eq
 8003146:	4770      	bxeq	lr
 8003148:	b530      	push	{r4, r5, lr}
 800314a:	f04f 0500 	mov.w	r5, #0
 800314e:	e00a      	b.n	8003166 <__aeabi_l2d+0x16>

08003150 <__aeabi_l2d>:
 8003150:	ea50 0201 	orrs.w	r2, r0, r1
 8003154:	bf08      	it	eq
 8003156:	4770      	bxeq	lr
 8003158:	b530      	push	{r4, r5, lr}
 800315a:	f011 4500 	ands.w	r5, r1, #2147483648	; 0x80000000
 800315e:	d502      	bpl.n	8003166 <__aeabi_l2d+0x16>
 8003160:	4240      	negs	r0, r0
 8003162:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 8003166:	f44f 6480 	mov.w	r4, #1024	; 0x400
 800316a:	f104 0432 	add.w	r4, r4, #50	; 0x32
 800316e:	ea5f 5c91 	movs.w	ip, r1, lsr #22
 8003172:	f43f aedc 	beq.w	8002f2e <__adddf3+0xe6>
 8003176:	f04f 0203 	mov.w	r2, #3
 800317a:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 800317e:	bf18      	it	ne
 8003180:	3203      	addne	r2, #3
 8003182:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 8003186:	bf18      	it	ne
 8003188:	3203      	addne	r2, #3
 800318a:	eb02 02dc 	add.w	r2, r2, ip, lsr #3
 800318e:	f1c2 0320 	rsb	r3, r2, #32
 8003192:	fa00 fc03 	lsl.w	ip, r0, r3
 8003196:	fa20 f002 	lsr.w	r0, r0, r2
 800319a:	fa01 fe03 	lsl.w	lr, r1, r3
 800319e:	ea40 000e 	orr.w	r0, r0, lr
 80031a2:	fa21 f102 	lsr.w	r1, r1, r2
 80031a6:	4414      	add	r4, r2
 80031a8:	e6c1      	b.n	8002f2e <__adddf3+0xe6>
 80031aa:	bf00      	nop

080031ac <__aeabi_dmul>:
 80031ac:	b570      	push	{r4, r5, r6, lr}
 80031ae:	f04f 0cff 	mov.w	ip, #255	; 0xff
 80031b2:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 80031b6:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 80031ba:	bf1d      	ittte	ne
 80031bc:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 80031c0:	ea94 0f0c 	teqne	r4, ip
 80031c4:	ea95 0f0c 	teqne	r5, ip
 80031c8:	f000 f8de 	bleq	8003388 <__aeabi_dmul+0x1dc>
 80031cc:	442c      	add	r4, r5
 80031ce:	ea81 0603 	eor.w	r6, r1, r3
 80031d2:	ea21 514c 	bic.w	r1, r1, ip, lsl #21
 80031d6:	ea23 534c 	bic.w	r3, r3, ip, lsl #21
 80031da:	ea50 3501 	orrs.w	r5, r0, r1, lsl #12
 80031de:	bf18      	it	ne
 80031e0:	ea52 3503 	orrsne.w	r5, r2, r3, lsl #12
 80031e4:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 80031e8:	f443 1380 	orr.w	r3, r3, #1048576	; 0x100000
 80031ec:	d038      	beq.n	8003260 <__aeabi_dmul+0xb4>
 80031ee:	fba0 ce02 	umull	ip, lr, r0, r2
 80031f2:	f04f 0500 	mov.w	r5, #0
 80031f6:	fbe1 e502 	umlal	lr, r5, r1, r2
 80031fa:	f006 4200 	and.w	r2, r6, #2147483648	; 0x80000000
 80031fe:	fbe0 e503 	umlal	lr, r5, r0, r3
 8003202:	f04f 0600 	mov.w	r6, #0
 8003206:	fbe1 5603 	umlal	r5, r6, r1, r3
 800320a:	f09c 0f00 	teq	ip, #0
 800320e:	bf18      	it	ne
 8003210:	f04e 0e01 	orrne.w	lr, lr, #1
 8003214:	f1a4 04ff 	sub.w	r4, r4, #255	; 0xff
 8003218:	f5b6 7f00 	cmp.w	r6, #512	; 0x200
 800321c:	f564 7440 	sbc.w	r4, r4, #768	; 0x300
 8003220:	d204      	bcs.n	800322c <__aeabi_dmul+0x80>
 8003222:	ea5f 0e4e 	movs.w	lr, lr, lsl #1
 8003226:	416d      	adcs	r5, r5
 8003228:	eb46 0606 	adc.w	r6, r6, r6
 800322c:	ea42 21c6 	orr.w	r1, r2, r6, lsl #11
 8003230:	ea41 5155 	orr.w	r1, r1, r5, lsr #21
 8003234:	ea4f 20c5 	mov.w	r0, r5, lsl #11
 8003238:	ea40 505e 	orr.w	r0, r0, lr, lsr #21
 800323c:	ea4f 2ece 	mov.w	lr, lr, lsl #11
 8003240:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 8003244:	bf88      	it	hi
 8003246:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 800324a:	d81e      	bhi.n	800328a <__aeabi_dmul+0xde>
 800324c:	f1be 4f00 	cmp.w	lr, #2147483648	; 0x80000000
 8003250:	bf08      	it	eq
 8003252:	ea5f 0e50 	movseq.w	lr, r0, lsr #1
 8003256:	f150 0000 	adcs.w	r0, r0, #0
 800325a:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 800325e:	bd70      	pop	{r4, r5, r6, pc}
 8003260:	f006 4600 	and.w	r6, r6, #2147483648	; 0x80000000
 8003264:	ea46 0101 	orr.w	r1, r6, r1
 8003268:	ea40 0002 	orr.w	r0, r0, r2
 800326c:	ea81 0103 	eor.w	r1, r1, r3
 8003270:	ebb4 045c 	subs.w	r4, r4, ip, lsr #1
 8003274:	bfc2      	ittt	gt
 8003276:	ebd4 050c 	rsbsgt	r5, r4, ip
 800327a:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 800327e:	bd70      	popgt	{r4, r5, r6, pc}
 8003280:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8003284:	f04f 0e00 	mov.w	lr, #0
 8003288:	3c01      	subs	r4, #1
 800328a:	f300 80ab 	bgt.w	80033e4 <__aeabi_dmul+0x238>
 800328e:	f114 0f36 	cmn.w	r4, #54	; 0x36
 8003292:	bfde      	ittt	le
 8003294:	2000      	movle	r0, #0
 8003296:	f001 4100 	andle.w	r1, r1, #2147483648	; 0x80000000
 800329a:	bd70      	pople	{r4, r5, r6, pc}
 800329c:	f1c4 0400 	rsb	r4, r4, #0
 80032a0:	3c20      	subs	r4, #32
 80032a2:	da35      	bge.n	8003310 <__aeabi_dmul+0x164>
 80032a4:	340c      	adds	r4, #12
 80032a6:	dc1b      	bgt.n	80032e0 <__aeabi_dmul+0x134>
 80032a8:	f104 0414 	add.w	r4, r4, #20
 80032ac:	f1c4 0520 	rsb	r5, r4, #32
 80032b0:	fa00 f305 	lsl.w	r3, r0, r5
 80032b4:	fa20 f004 	lsr.w	r0, r0, r4
 80032b8:	fa01 f205 	lsl.w	r2, r1, r5
 80032bc:	ea40 0002 	orr.w	r0, r0, r2
 80032c0:	f001 4200 	and.w	r2, r1, #2147483648	; 0x80000000
 80032c4:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 80032c8:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 80032cc:	fa21 f604 	lsr.w	r6, r1, r4
 80032d0:	eb42 0106 	adc.w	r1, r2, r6
 80032d4:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 80032d8:	bf08      	it	eq
 80032da:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 80032de:	bd70      	pop	{r4, r5, r6, pc}
 80032e0:	f1c4 040c 	rsb	r4, r4, #12
 80032e4:	f1c4 0520 	rsb	r5, r4, #32
 80032e8:	fa00 f304 	lsl.w	r3, r0, r4
 80032ec:	fa20 f005 	lsr.w	r0, r0, r5
 80032f0:	fa01 f204 	lsl.w	r2, r1, r4
 80032f4:	ea40 0002 	orr.w	r0, r0, r2
 80032f8:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 80032fc:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 8003300:	f141 0100 	adc.w	r1, r1, #0
 8003304:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 8003308:	bf08      	it	eq
 800330a:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 800330e:	bd70      	pop	{r4, r5, r6, pc}
 8003310:	f1c4 0520 	rsb	r5, r4, #32
 8003314:	fa00 f205 	lsl.w	r2, r0, r5
 8003318:	ea4e 0e02 	orr.w	lr, lr, r2
 800331c:	fa20 f304 	lsr.w	r3, r0, r4
 8003320:	fa01 f205 	lsl.w	r2, r1, r5
 8003324:	ea43 0302 	orr.w	r3, r3, r2
 8003328:	fa21 f004 	lsr.w	r0, r1, r4
 800332c:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8003330:	fa21 f204 	lsr.w	r2, r1, r4
 8003334:	ea20 0002 	bic.w	r0, r0, r2
 8003338:	eb00 70d3 	add.w	r0, r0, r3, lsr #31
 800333c:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 8003340:	bf08      	it	eq
 8003342:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 8003346:	bd70      	pop	{r4, r5, r6, pc}
 8003348:	f094 0f00 	teq	r4, #0
 800334c:	d10f      	bne.n	800336e <__aeabi_dmul+0x1c2>
 800334e:	f001 4600 	and.w	r6, r1, #2147483648	; 0x80000000
 8003352:	0040      	lsls	r0, r0, #1
 8003354:	eb41 0101 	adc.w	r1, r1, r1
 8003358:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 800335c:	bf08      	it	eq
 800335e:	3c01      	subeq	r4, #1
 8003360:	d0f7      	beq.n	8003352 <__aeabi_dmul+0x1a6>
 8003362:	ea41 0106 	orr.w	r1, r1, r6
 8003366:	f095 0f00 	teq	r5, #0
 800336a:	bf18      	it	ne
 800336c:	4770      	bxne	lr
 800336e:	f003 4600 	and.w	r6, r3, #2147483648	; 0x80000000
 8003372:	0052      	lsls	r2, r2, #1
 8003374:	eb43 0303 	adc.w	r3, r3, r3
 8003378:	f413 1f80 	tst.w	r3, #1048576	; 0x100000
 800337c:	bf08      	it	eq
 800337e:	3d01      	subeq	r5, #1
 8003380:	d0f7      	beq.n	8003372 <__aeabi_dmul+0x1c6>
 8003382:	ea43 0306 	orr.w	r3, r3, r6
 8003386:	4770      	bx	lr
 8003388:	ea94 0f0c 	teq	r4, ip
 800338c:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 8003390:	bf18      	it	ne
 8003392:	ea95 0f0c 	teqne	r5, ip
 8003396:	d00c      	beq.n	80033b2 <__aeabi_dmul+0x206>
 8003398:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 800339c:	bf18      	it	ne
 800339e:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 80033a2:	d1d1      	bne.n	8003348 <__aeabi_dmul+0x19c>
 80033a4:	ea81 0103 	eor.w	r1, r1, r3
 80033a8:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 80033ac:	f04f 0000 	mov.w	r0, #0
 80033b0:	bd70      	pop	{r4, r5, r6, pc}
 80033b2:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 80033b6:	bf06      	itte	eq
 80033b8:	4610      	moveq	r0, r2
 80033ba:	4619      	moveq	r1, r3
 80033bc:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 80033c0:	d019      	beq.n	80033f6 <__aeabi_dmul+0x24a>
 80033c2:	ea94 0f0c 	teq	r4, ip
 80033c6:	d102      	bne.n	80033ce <__aeabi_dmul+0x222>
 80033c8:	ea50 3601 	orrs.w	r6, r0, r1, lsl #12
 80033cc:	d113      	bne.n	80033f6 <__aeabi_dmul+0x24a>
 80033ce:	ea95 0f0c 	teq	r5, ip
 80033d2:	d105      	bne.n	80033e0 <__aeabi_dmul+0x234>
 80033d4:	ea52 3603 	orrs.w	r6, r2, r3, lsl #12
 80033d8:	bf1c      	itt	ne
 80033da:	4610      	movne	r0, r2
 80033dc:	4619      	movne	r1, r3
 80033de:	d10a      	bne.n	80033f6 <__aeabi_dmul+0x24a>
 80033e0:	ea81 0103 	eor.w	r1, r1, r3
 80033e4:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 80033e8:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 80033ec:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 80033f0:	f04f 0000 	mov.w	r0, #0
 80033f4:	bd70      	pop	{r4, r5, r6, pc}
 80033f6:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 80033fa:	f441 0178 	orr.w	r1, r1, #16252928	; 0xf80000
 80033fe:	bd70      	pop	{r4, r5, r6, pc}

08003400 <__aeabi_ddiv>:
 8003400:	b570      	push	{r4, r5, r6, lr}
 8003402:	f04f 0cff 	mov.w	ip, #255	; 0xff
 8003406:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 800340a:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 800340e:	bf1d      	ittte	ne
 8003410:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 8003414:	ea94 0f0c 	teqne	r4, ip
 8003418:	ea95 0f0c 	teqne	r5, ip
 800341c:	f000 f8a7 	bleq	800356e <__aeabi_ddiv+0x16e>
 8003420:	eba4 0405 	sub.w	r4, r4, r5
 8003424:	ea81 0e03 	eor.w	lr, r1, r3
 8003428:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 800342c:	ea4f 3101 	mov.w	r1, r1, lsl #12
 8003430:	f000 8088 	beq.w	8003544 <__aeabi_ddiv+0x144>
 8003434:	ea4f 3303 	mov.w	r3, r3, lsl #12
 8003438:	f04f 5580 	mov.w	r5, #268435456	; 0x10000000
 800343c:	ea45 1313 	orr.w	r3, r5, r3, lsr #4
 8003440:	ea43 6312 	orr.w	r3, r3, r2, lsr #24
 8003444:	ea4f 2202 	mov.w	r2, r2, lsl #8
 8003448:	ea45 1511 	orr.w	r5, r5, r1, lsr #4
 800344c:	ea45 6510 	orr.w	r5, r5, r0, lsr #24
 8003450:	ea4f 2600 	mov.w	r6, r0, lsl #8
 8003454:	f00e 4100 	and.w	r1, lr, #2147483648	; 0x80000000
 8003458:	429d      	cmp	r5, r3
 800345a:	bf08      	it	eq
 800345c:	4296      	cmpeq	r6, r2
 800345e:	f144 04fd 	adc.w	r4, r4, #253	; 0xfd
 8003462:	f504 7440 	add.w	r4, r4, #768	; 0x300
 8003466:	d202      	bcs.n	800346e <__aeabi_ddiv+0x6e>
 8003468:	085b      	lsrs	r3, r3, #1
 800346a:	ea4f 0232 	mov.w	r2, r2, rrx
 800346e:	1ab6      	subs	r6, r6, r2
 8003470:	eb65 0503 	sbc.w	r5, r5, r3
 8003474:	085b      	lsrs	r3, r3, #1
 8003476:	ea4f 0232 	mov.w	r2, r2, rrx
 800347a:	f44f 1080 	mov.w	r0, #1048576	; 0x100000
 800347e:	f44f 2c00 	mov.w	ip, #524288	; 0x80000
 8003482:	ebb6 0e02 	subs.w	lr, r6, r2
 8003486:	eb75 0e03 	sbcs.w	lr, r5, r3
 800348a:	bf22      	ittt	cs
 800348c:	1ab6      	subcs	r6, r6, r2
 800348e:	4675      	movcs	r5, lr
 8003490:	ea40 000c 	orrcs.w	r0, r0, ip
 8003494:	085b      	lsrs	r3, r3, #1
 8003496:	ea4f 0232 	mov.w	r2, r2, rrx
 800349a:	ebb6 0e02 	subs.w	lr, r6, r2
 800349e:	eb75 0e03 	sbcs.w	lr, r5, r3
 80034a2:	bf22      	ittt	cs
 80034a4:	1ab6      	subcs	r6, r6, r2
 80034a6:	4675      	movcs	r5, lr
 80034a8:	ea40 005c 	orrcs.w	r0, r0, ip, lsr #1
 80034ac:	085b      	lsrs	r3, r3, #1
 80034ae:	ea4f 0232 	mov.w	r2, r2, rrx
 80034b2:	ebb6 0e02 	subs.w	lr, r6, r2
 80034b6:	eb75 0e03 	sbcs.w	lr, r5, r3
 80034ba:	bf22      	ittt	cs
 80034bc:	1ab6      	subcs	r6, r6, r2
 80034be:	4675      	movcs	r5, lr
 80034c0:	ea40 009c 	orrcs.w	r0, r0, ip, lsr #2
 80034c4:	085b      	lsrs	r3, r3, #1
 80034c6:	ea4f 0232 	mov.w	r2, r2, rrx
 80034ca:	ebb6 0e02 	subs.w	lr, r6, r2
 80034ce:	eb75 0e03 	sbcs.w	lr, r5, r3
 80034d2:	bf22      	ittt	cs
 80034d4:	1ab6      	subcs	r6, r6, r2
 80034d6:	4675      	movcs	r5, lr
 80034d8:	ea40 00dc 	orrcs.w	r0, r0, ip, lsr #3
 80034dc:	ea55 0e06 	orrs.w	lr, r5, r6
 80034e0:	d018      	beq.n	8003514 <__aeabi_ddiv+0x114>
 80034e2:	ea4f 1505 	mov.w	r5, r5, lsl #4
 80034e6:	ea45 7516 	orr.w	r5, r5, r6, lsr #28
 80034ea:	ea4f 1606 	mov.w	r6, r6, lsl #4
 80034ee:	ea4f 03c3 	mov.w	r3, r3, lsl #3
 80034f2:	ea43 7352 	orr.w	r3, r3, r2, lsr #29
 80034f6:	ea4f 02c2 	mov.w	r2, r2, lsl #3
 80034fa:	ea5f 1c1c 	movs.w	ip, ip, lsr #4
 80034fe:	d1c0      	bne.n	8003482 <__aeabi_ddiv+0x82>
 8003500:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 8003504:	d10b      	bne.n	800351e <__aeabi_ddiv+0x11e>
 8003506:	ea41 0100 	orr.w	r1, r1, r0
 800350a:	f04f 0000 	mov.w	r0, #0
 800350e:	f04f 4c00 	mov.w	ip, #2147483648	; 0x80000000
 8003512:	e7b6      	b.n	8003482 <__aeabi_ddiv+0x82>
 8003514:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 8003518:	bf04      	itt	eq
 800351a:	4301      	orreq	r1, r0
 800351c:	2000      	moveq	r0, #0
 800351e:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 8003522:	bf88      	it	hi
 8003524:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 8003528:	f63f aeaf 	bhi.w	800328a <__aeabi_dmul+0xde>
 800352c:	ebb5 0c03 	subs.w	ip, r5, r3
 8003530:	bf04      	itt	eq
 8003532:	ebb6 0c02 	subseq.w	ip, r6, r2
 8003536:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 800353a:	f150 0000 	adcs.w	r0, r0, #0
 800353e:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 8003542:	bd70      	pop	{r4, r5, r6, pc}
 8003544:	f00e 4e00 	and.w	lr, lr, #2147483648	; 0x80000000
 8003548:	ea4e 3111 	orr.w	r1, lr, r1, lsr #12
 800354c:	eb14 045c 	adds.w	r4, r4, ip, lsr #1
 8003550:	bfc2      	ittt	gt
 8003552:	ebd4 050c 	rsbsgt	r5, r4, ip
 8003556:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 800355a:	bd70      	popgt	{r4, r5, r6, pc}
 800355c:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8003560:	f04f 0e00 	mov.w	lr, #0
 8003564:	3c01      	subs	r4, #1
 8003566:	e690      	b.n	800328a <__aeabi_dmul+0xde>
 8003568:	ea45 0e06 	orr.w	lr, r5, r6
 800356c:	e68d      	b.n	800328a <__aeabi_dmul+0xde>
 800356e:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 8003572:	ea94 0f0c 	teq	r4, ip
 8003576:	bf08      	it	eq
 8003578:	ea95 0f0c 	teqeq	r5, ip
 800357c:	f43f af3b 	beq.w	80033f6 <__aeabi_dmul+0x24a>
 8003580:	ea94 0f0c 	teq	r4, ip
 8003584:	d10a      	bne.n	800359c <__aeabi_ddiv+0x19c>
 8003586:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 800358a:	f47f af34 	bne.w	80033f6 <__aeabi_dmul+0x24a>
 800358e:	ea95 0f0c 	teq	r5, ip
 8003592:	f47f af25 	bne.w	80033e0 <__aeabi_dmul+0x234>
 8003596:	4610      	mov	r0, r2
 8003598:	4619      	mov	r1, r3
 800359a:	e72c      	b.n	80033f6 <__aeabi_dmul+0x24a>
 800359c:	ea95 0f0c 	teq	r5, ip
 80035a0:	d106      	bne.n	80035b0 <__aeabi_ddiv+0x1b0>
 80035a2:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 80035a6:	f43f aefd 	beq.w	80033a4 <__aeabi_dmul+0x1f8>
 80035aa:	4610      	mov	r0, r2
 80035ac:	4619      	mov	r1, r3
 80035ae:	e722      	b.n	80033f6 <__aeabi_dmul+0x24a>
 80035b0:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 80035b4:	bf18      	it	ne
 80035b6:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 80035ba:	f47f aec5 	bne.w	8003348 <__aeabi_dmul+0x19c>
 80035be:	ea50 0441 	orrs.w	r4, r0, r1, lsl #1
 80035c2:	f47f af0d 	bne.w	80033e0 <__aeabi_dmul+0x234>
 80035c6:	ea52 0543 	orrs.w	r5, r2, r3, lsl #1
 80035ca:	f47f aeeb 	bne.w	80033a4 <__aeabi_dmul+0x1f8>
 80035ce:	e712      	b.n	80033f6 <__aeabi_dmul+0x24a>

080035d0 <__gedf2>:
 80035d0:	f04f 3cff 	mov.w	ip, #4294967295	; 0xffffffff
 80035d4:	e006      	b.n	80035e4 <__cmpdf2+0x4>
 80035d6:	bf00      	nop

080035d8 <__ledf2>:
 80035d8:	f04f 0c01 	mov.w	ip, #1
 80035dc:	e002      	b.n	80035e4 <__cmpdf2+0x4>
 80035de:	bf00      	nop

080035e0 <__cmpdf2>:
 80035e0:	f04f 0c01 	mov.w	ip, #1
 80035e4:	f84d cd04 	str.w	ip, [sp, #-4]!
 80035e8:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 80035ec:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 80035f0:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 80035f4:	bf18      	it	ne
 80035f6:	ea7f 5c6c 	mvnsne.w	ip, ip, asr #21
 80035fa:	d01b      	beq.n	8003634 <__cmpdf2+0x54>
 80035fc:	b001      	add	sp, #4
 80035fe:	ea50 0c41 	orrs.w	ip, r0, r1, lsl #1
 8003602:	bf0c      	ite	eq
 8003604:	ea52 0c43 	orrseq.w	ip, r2, r3, lsl #1
 8003608:	ea91 0f03 	teqne	r1, r3
 800360c:	bf02      	ittt	eq
 800360e:	ea90 0f02 	teqeq	r0, r2
 8003612:	2000      	moveq	r0, #0
 8003614:	4770      	bxeq	lr
 8003616:	f110 0f00 	cmn.w	r0, #0
 800361a:	ea91 0f03 	teq	r1, r3
 800361e:	bf58      	it	pl
 8003620:	4299      	cmppl	r1, r3
 8003622:	bf08      	it	eq
 8003624:	4290      	cmpeq	r0, r2
 8003626:	bf2c      	ite	cs
 8003628:	17d8      	asrcs	r0, r3, #31
 800362a:	ea6f 70e3 	mvncc.w	r0, r3, asr #31
 800362e:	f040 0001 	orr.w	r0, r0, #1
 8003632:	4770      	bx	lr
 8003634:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 8003638:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 800363c:	d102      	bne.n	8003644 <__cmpdf2+0x64>
 800363e:	ea50 3c01 	orrs.w	ip, r0, r1, lsl #12
 8003642:	d107      	bne.n	8003654 <__cmpdf2+0x74>
 8003644:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 8003648:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 800364c:	d1d6      	bne.n	80035fc <__cmpdf2+0x1c>
 800364e:	ea52 3c03 	orrs.w	ip, r2, r3, lsl #12
 8003652:	d0d3      	beq.n	80035fc <__cmpdf2+0x1c>
 8003654:	f85d 0b04 	ldr.w	r0, [sp], #4
 8003658:	4770      	bx	lr
 800365a:	bf00      	nop

0800365c <__aeabi_cdrcmple>:
 800365c:	4684      	mov	ip, r0
 800365e:	4610      	mov	r0, r2
 8003660:	4662      	mov	r2, ip
 8003662:	468c      	mov	ip, r1
 8003664:	4619      	mov	r1, r3
 8003666:	4663      	mov	r3, ip
 8003668:	e000      	b.n	800366c <__aeabi_cdcmpeq>
 800366a:	bf00      	nop

0800366c <__aeabi_cdcmpeq>:
 800366c:	b501      	push	{r0, lr}
 800366e:	f7ff ffb7 	bl	80035e0 <__cmpdf2>
 8003672:	2800      	cmp	r0, #0
 8003674:	bf48      	it	mi
 8003676:	f110 0f00 	cmnmi.w	r0, #0
 800367a:	bd01      	pop	{r0, pc}

0800367c <__aeabi_dcmpeq>:
 800367c:	f84d ed08 	str.w	lr, [sp, #-8]!
 8003680:	f7ff fff4 	bl	800366c <__aeabi_cdcmpeq>
 8003684:	bf0c      	ite	eq
 8003686:	2001      	moveq	r0, #1
 8003688:	2000      	movne	r0, #0
 800368a:	f85d fb08 	ldr.w	pc, [sp], #8
 800368e:	bf00      	nop

08003690 <__aeabi_dcmplt>:
 8003690:	f84d ed08 	str.w	lr, [sp, #-8]!
 8003694:	f7ff ffea 	bl	800366c <__aeabi_cdcmpeq>
 8003698:	bf34      	ite	cc
 800369a:	2001      	movcc	r0, #1
 800369c:	2000      	movcs	r0, #0
 800369e:	f85d fb08 	ldr.w	pc, [sp], #8
 80036a2:	bf00      	nop

080036a4 <__aeabi_dcmple>:
 80036a4:	f84d ed08 	str.w	lr, [sp, #-8]!
 80036a8:	f7ff ffe0 	bl	800366c <__aeabi_cdcmpeq>
 80036ac:	bf94      	ite	ls
 80036ae:	2001      	movls	r0, #1
 80036b0:	2000      	movhi	r0, #0
 80036b2:	f85d fb08 	ldr.w	pc, [sp], #8
 80036b6:	bf00      	nop

080036b8 <__aeabi_dcmpge>:
 80036b8:	f84d ed08 	str.w	lr, [sp, #-8]!
 80036bc:	f7ff ffce 	bl	800365c <__aeabi_cdrcmple>
 80036c0:	bf94      	ite	ls
 80036c2:	2001      	movls	r0, #1
 80036c4:	2000      	movhi	r0, #0
 80036c6:	f85d fb08 	ldr.w	pc, [sp], #8
 80036ca:	bf00      	nop

080036cc <__aeabi_dcmpgt>:
 80036cc:	f84d ed08 	str.w	lr, [sp, #-8]!
 80036d0:	f7ff ffc4 	bl	800365c <__aeabi_cdrcmple>
 80036d4:	bf34      	ite	cc
 80036d6:	2001      	movcc	r0, #1
 80036d8:	2000      	movcs	r0, #0
 80036da:	f85d fb08 	ldr.w	pc, [sp], #8
 80036de:	bf00      	nop

080036e0 <__aeabi_dcmpun>:
 80036e0:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 80036e4:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 80036e8:	d102      	bne.n	80036f0 <__aeabi_dcmpun+0x10>
 80036ea:	ea50 3c01 	orrs.w	ip, r0, r1, lsl #12
 80036ee:	d10a      	bne.n	8003706 <__aeabi_dcmpun+0x26>
 80036f0:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 80036f4:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 80036f8:	d102      	bne.n	8003700 <__aeabi_dcmpun+0x20>
 80036fa:	ea52 3c03 	orrs.w	ip, r2, r3, lsl #12
 80036fe:	d102      	bne.n	8003706 <__aeabi_dcmpun+0x26>
 8003700:	f04f 0000 	mov.w	r0, #0
 8003704:	4770      	bx	lr
 8003706:	f04f 0001 	mov.w	r0, #1
 800370a:	4770      	bx	lr

0800370c <__aeabi_d2iz>:
 800370c:	ea4f 0241 	mov.w	r2, r1, lsl #1
 8003710:	f512 1200 	adds.w	r2, r2, #2097152	; 0x200000
 8003714:	d215      	bcs.n	8003742 <__aeabi_d2iz+0x36>
 8003716:	d511      	bpl.n	800373c <__aeabi_d2iz+0x30>
 8003718:	f46f 7378 	mvn.w	r3, #992	; 0x3e0
 800371c:	ebb3 5262 	subs.w	r2, r3, r2, asr #21
 8003720:	d912      	bls.n	8003748 <__aeabi_d2iz+0x3c>
 8003722:	ea4f 23c1 	mov.w	r3, r1, lsl #11
 8003726:	f043 4300 	orr.w	r3, r3, #2147483648	; 0x80000000
 800372a:	ea43 5350 	orr.w	r3, r3, r0, lsr #21
 800372e:	f011 4f00 	tst.w	r1, #2147483648	; 0x80000000
 8003732:	fa23 f002 	lsr.w	r0, r3, r2
 8003736:	bf18      	it	ne
 8003738:	4240      	negne	r0, r0
 800373a:	4770      	bx	lr
 800373c:	f04f 0000 	mov.w	r0, #0
 8003740:	4770      	bx	lr
 8003742:	ea50 3001 	orrs.w	r0, r0, r1, lsl #12
 8003746:	d105      	bne.n	8003754 <__aeabi_d2iz+0x48>
 8003748:	f011 4000 	ands.w	r0, r1, #2147483648	; 0x80000000
 800374c:	bf08      	it	eq
 800374e:	f06f 4000 	mvneq.w	r0, #2147483648	; 0x80000000
 8003752:	4770      	bx	lr
 8003754:	f04f 0000 	mov.w	r0, #0
 8003758:	4770      	bx	lr
 800375a:	bf00      	nop

0800375c <Reset_Handler>:
 800375c:	f8df d034 	ldr.w	sp, [pc, #52]	; 8003794 <LoopFillZerobss+0x14>
 8003760:	2100      	movs	r1, #0
 8003762:	e003      	b.n	800376c <LoopCopyDataInit>

08003764 <CopyDataInit>:
 8003764:	4b0c      	ldr	r3, [pc, #48]	; (8003798 <LoopFillZerobss+0x18>)
 8003766:	585b      	ldr	r3, [r3, r1]
 8003768:	5043      	str	r3, [r0, r1]
 800376a:	3104      	adds	r1, #4

0800376c <LoopCopyDataInit>:
 800376c:	480b      	ldr	r0, [pc, #44]	; (800379c <LoopFillZerobss+0x1c>)
 800376e:	4b0c      	ldr	r3, [pc, #48]	; (80037a0 <LoopFillZerobss+0x20>)
 8003770:	1842      	adds	r2, r0, r1
 8003772:	429a      	cmp	r2, r3
 8003774:	d3f6      	bcc.n	8003764 <CopyDataInit>
 8003776:	4a0b      	ldr	r2, [pc, #44]	; (80037a4 <LoopFillZerobss+0x24>)
 8003778:	e002      	b.n	8003780 <LoopFillZerobss>

0800377a <FillZerobss>:
 800377a:	2300      	movs	r3, #0
 800377c:	f842 3b04 	str.w	r3, [r2], #4

08003780 <LoopFillZerobss>:
 8003780:	4b09      	ldr	r3, [pc, #36]	; (80037a8 <LoopFillZerobss+0x28>)
 8003782:	429a      	cmp	r2, r3
 8003784:	d3f9      	bcc.n	800377a <FillZerobss>
 8003786:	f000 ff49 	bl	800461c <SystemInit>
 800378a:	f7fc fd6b 	bl	8000264 <__libc_init_array>
 800378e:	f000 f9f5 	bl	8003b7c <main>
 8003792:	4770      	bx	lr
 8003794:	20020000 	.word	0x20020000
 8003798:	0800488c 	.word	0x0800488c
 800379c:	20000000 	.word	0x20000000
 80037a0:	200000e8 	.word	0x200000e8
 80037a4:	200000e8 	.word	0x200000e8
 80037a8:	20012dc8 	.word	0x20012dc8

080037ac <ADC_IRQHandler>:
 80037ac:	e7fe      	b.n	80037ac <ADC_IRQHandler>
	...

080037b0 <NVIC_SetPriorityGrouping>:
 80037b0:	b480      	push	{r7}
 80037b2:	b085      	sub	sp, #20
 80037b4:	af00      	add	r7, sp, #0
 80037b6:	6078      	str	r0, [r7, #4]
 80037b8:	687b      	ldr	r3, [r7, #4]
 80037ba:	f003 0307 	and.w	r3, r3, #7
 80037be:	60fb      	str	r3, [r7, #12]
 80037c0:	4b0c      	ldr	r3, [pc, #48]	; (80037f4 <NVIC_SetPriorityGrouping+0x44>)
 80037c2:	68db      	ldr	r3, [r3, #12]
 80037c4:	60bb      	str	r3, [r7, #8]
 80037c6:	68ba      	ldr	r2, [r7, #8]
 80037c8:	f64f 03ff 	movw	r3, #63743	; 0xf8ff
 80037cc:	4013      	ands	r3, r2
 80037ce:	60bb      	str	r3, [r7, #8]
 80037d0:	68fb      	ldr	r3, [r7, #12]
 80037d2:	021a      	lsls	r2, r3, #8
 80037d4:	68bb      	ldr	r3, [r7, #8]
 80037d6:	4313      	orrs	r3, r2
 80037d8:	f043 63bf 	orr.w	r3, r3, #100139008	; 0x5f80000
 80037dc:	f443 3300 	orr.w	r3, r3, #131072	; 0x20000
 80037e0:	60bb      	str	r3, [r7, #8]
 80037e2:	4a04      	ldr	r2, [pc, #16]	; (80037f4 <NVIC_SetPriorityGrouping+0x44>)
 80037e4:	68bb      	ldr	r3, [r7, #8]
 80037e6:	60d3      	str	r3, [r2, #12]
 80037e8:	bf00      	nop
 80037ea:	3714      	adds	r7, #20
 80037ec:	46bd      	mov	sp, r7
 80037ee:	f85d 7b04 	ldr.w	r7, [sp], #4
 80037f2:	4770      	bx	lr
 80037f4:	e000ed00 	.word	0xe000ed00

080037f8 <NVIC_SetPriority>:
 80037f8:	b480      	push	{r7}
 80037fa:	b083      	sub	sp, #12
 80037fc:	af00      	add	r7, sp, #0
 80037fe:	4603      	mov	r3, r0
 8003800:	6039      	str	r1, [r7, #0]
 8003802:	71fb      	strb	r3, [r7, #7]
 8003804:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8003808:	2b00      	cmp	r3, #0
 800380a:	da0b      	bge.n	8003824 <NVIC_SetPriority+0x2c>
 800380c:	490d      	ldr	r1, [pc, #52]	; (8003844 <NVIC_SetPriority+0x4c>)
 800380e:	79fb      	ldrb	r3, [r7, #7]
 8003810:	f003 030f 	and.w	r3, r3, #15
 8003814:	3b04      	subs	r3, #4
 8003816:	683a      	ldr	r2, [r7, #0]
 8003818:	b2d2      	uxtb	r2, r2
 800381a:	0112      	lsls	r2, r2, #4
 800381c:	b2d2      	uxtb	r2, r2
 800381e:	440b      	add	r3, r1
 8003820:	761a      	strb	r2, [r3, #24]
 8003822:	e009      	b.n	8003838 <NVIC_SetPriority+0x40>
 8003824:	4908      	ldr	r1, [pc, #32]	; (8003848 <NVIC_SetPriority+0x50>)
 8003826:	f997 3007 	ldrsb.w	r3, [r7, #7]
 800382a:	683a      	ldr	r2, [r7, #0]
 800382c:	b2d2      	uxtb	r2, r2
 800382e:	0112      	lsls	r2, r2, #4
 8003830:	b2d2      	uxtb	r2, r2
 8003832:	440b      	add	r3, r1
 8003834:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8003838:	bf00      	nop
 800383a:	370c      	adds	r7, #12
 800383c:	46bd      	mov	sp, r7
 800383e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003842:	4770      	bx	lr
 8003844:	e000ed00 	.word	0xe000ed00
 8003848:	e000e100 	.word	0xe000e100

0800384c <SysTick_Config>:
 800384c:	b580      	push	{r7, lr}
 800384e:	b082      	sub	sp, #8
 8003850:	af00      	add	r7, sp, #0
 8003852:	6078      	str	r0, [r7, #4]
 8003854:	687b      	ldr	r3, [r7, #4]
 8003856:	3b01      	subs	r3, #1
 8003858:	f1b3 7f80 	cmp.w	r3, #16777216	; 0x1000000
 800385c:	d301      	bcc.n	8003862 <SysTick_Config+0x16>
 800385e:	2301      	movs	r3, #1
 8003860:	e00f      	b.n	8003882 <SysTick_Config+0x36>
 8003862:	4a0a      	ldr	r2, [pc, #40]	; (800388c <SysTick_Config+0x40>)
 8003864:	687b      	ldr	r3, [r7, #4]
 8003866:	3b01      	subs	r3, #1
 8003868:	6053      	str	r3, [r2, #4]
 800386a:	210f      	movs	r1, #15
 800386c:	f04f 30ff 	mov.w	r0, #4294967295	; 0xffffffff
 8003870:	f7ff ffc2 	bl	80037f8 <NVIC_SetPriority>
 8003874:	4b05      	ldr	r3, [pc, #20]	; (800388c <SysTick_Config+0x40>)
 8003876:	2200      	movs	r2, #0
 8003878:	609a      	str	r2, [r3, #8]
 800387a:	4b04      	ldr	r3, [pc, #16]	; (800388c <SysTick_Config+0x40>)
 800387c:	2207      	movs	r2, #7
 800387e:	601a      	str	r2, [r3, #0]
 8003880:	2300      	movs	r3, #0
 8003882:	4618      	mov	r0, r3
 8003884:	3708      	adds	r7, #8
 8003886:	46bd      	mov	sp, r7
 8003888:	bd80      	pop	{r7, pc}
 800388a:	bf00      	nop
 800388c:	e000e010 	.word	0xe000e010

08003890 <LL_RCC_HSE_Enable>:
 8003890:	b480      	push	{r7}
 8003892:	af00      	add	r7, sp, #0
 8003894:	4a05      	ldr	r2, [pc, #20]	; (80038ac <LL_RCC_HSE_Enable+0x1c>)
 8003896:	4b05      	ldr	r3, [pc, #20]	; (80038ac <LL_RCC_HSE_Enable+0x1c>)
 8003898:	681b      	ldr	r3, [r3, #0]
 800389a:	f443 3380 	orr.w	r3, r3, #65536	; 0x10000
 800389e:	6013      	str	r3, [r2, #0]
 80038a0:	bf00      	nop
 80038a2:	46bd      	mov	sp, r7
 80038a4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80038a8:	4770      	bx	lr
 80038aa:	bf00      	nop
 80038ac:	40023800 	.word	0x40023800

080038b0 <LL_RCC_HSE_IsReady>:
 80038b0:	b480      	push	{r7}
 80038b2:	af00      	add	r7, sp, #0
 80038b4:	4b07      	ldr	r3, [pc, #28]	; (80038d4 <LL_RCC_HSE_IsReady+0x24>)
 80038b6:	681b      	ldr	r3, [r3, #0]
 80038b8:	f403 3300 	and.w	r3, r3, #131072	; 0x20000
 80038bc:	f5b3 3f00 	cmp.w	r3, #131072	; 0x20000
 80038c0:	bf0c      	ite	eq
 80038c2:	2301      	moveq	r3, #1
 80038c4:	2300      	movne	r3, #0
 80038c6:	b2db      	uxtb	r3, r3
 80038c8:	4618      	mov	r0, r3
 80038ca:	46bd      	mov	sp, r7
 80038cc:	f85d 7b04 	ldr.w	r7, [sp], #4
 80038d0:	4770      	bx	lr
 80038d2:	bf00      	nop
 80038d4:	40023800 	.word	0x40023800

080038d8 <LL_RCC_SetSysClkSource>:
 80038d8:	b480      	push	{r7}
 80038da:	b083      	sub	sp, #12
 80038dc:	af00      	add	r7, sp, #0
 80038de:	6078      	str	r0, [r7, #4]
 80038e0:	4906      	ldr	r1, [pc, #24]	; (80038fc <LL_RCC_SetSysClkSource+0x24>)
 80038e2:	4b06      	ldr	r3, [pc, #24]	; (80038fc <LL_RCC_SetSysClkSource+0x24>)
 80038e4:	689b      	ldr	r3, [r3, #8]
 80038e6:	f023 0203 	bic.w	r2, r3, #3
 80038ea:	687b      	ldr	r3, [r7, #4]
 80038ec:	4313      	orrs	r3, r2
 80038ee:	608b      	str	r3, [r1, #8]
 80038f0:	bf00      	nop
 80038f2:	370c      	adds	r7, #12
 80038f4:	46bd      	mov	sp, r7
 80038f6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80038fa:	4770      	bx	lr
 80038fc:	40023800 	.word	0x40023800

08003900 <LL_RCC_GetSysClkSource>:
 8003900:	b480      	push	{r7}
 8003902:	af00      	add	r7, sp, #0
 8003904:	4b04      	ldr	r3, [pc, #16]	; (8003918 <LL_RCC_GetSysClkSource+0x18>)
 8003906:	689b      	ldr	r3, [r3, #8]
 8003908:	f003 030c 	and.w	r3, r3, #12
 800390c:	4618      	mov	r0, r3
 800390e:	46bd      	mov	sp, r7
 8003910:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003914:	4770      	bx	lr
 8003916:	bf00      	nop
 8003918:	40023800 	.word	0x40023800

0800391c <LL_RCC_SetAHBPrescaler>:
 800391c:	b480      	push	{r7}
 800391e:	b083      	sub	sp, #12
 8003920:	af00      	add	r7, sp, #0
 8003922:	6078      	str	r0, [r7, #4]
 8003924:	4906      	ldr	r1, [pc, #24]	; (8003940 <LL_RCC_SetAHBPrescaler+0x24>)
 8003926:	4b06      	ldr	r3, [pc, #24]	; (8003940 <LL_RCC_SetAHBPrescaler+0x24>)
 8003928:	689b      	ldr	r3, [r3, #8]
 800392a:	f023 02f0 	bic.w	r2, r3, #240	; 0xf0
 800392e:	687b      	ldr	r3, [r7, #4]
 8003930:	4313      	orrs	r3, r2
 8003932:	608b      	str	r3, [r1, #8]
 8003934:	bf00      	nop
 8003936:	370c      	adds	r7, #12
 8003938:	46bd      	mov	sp, r7
 800393a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800393e:	4770      	bx	lr
 8003940:	40023800 	.word	0x40023800

08003944 <LL_RCC_SetAPB1Prescaler>:
 8003944:	b480      	push	{r7}
 8003946:	b083      	sub	sp, #12
 8003948:	af00      	add	r7, sp, #0
 800394a:	6078      	str	r0, [r7, #4]
 800394c:	4906      	ldr	r1, [pc, #24]	; (8003968 <LL_RCC_SetAPB1Prescaler+0x24>)
 800394e:	4b06      	ldr	r3, [pc, #24]	; (8003968 <LL_RCC_SetAPB1Prescaler+0x24>)
 8003950:	689b      	ldr	r3, [r3, #8]
 8003952:	f423 52e0 	bic.w	r2, r3, #7168	; 0x1c00
 8003956:	687b      	ldr	r3, [r7, #4]
 8003958:	4313      	orrs	r3, r2
 800395a:	608b      	str	r3, [r1, #8]
 800395c:	bf00      	nop
 800395e:	370c      	adds	r7, #12
 8003960:	46bd      	mov	sp, r7
 8003962:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003966:	4770      	bx	lr
 8003968:	40023800 	.word	0x40023800

0800396c <LL_RCC_SetAPB2Prescaler>:
 800396c:	b480      	push	{r7}
 800396e:	b083      	sub	sp, #12
 8003970:	af00      	add	r7, sp, #0
 8003972:	6078      	str	r0, [r7, #4]
 8003974:	4906      	ldr	r1, [pc, #24]	; (8003990 <LL_RCC_SetAPB2Prescaler+0x24>)
 8003976:	4b06      	ldr	r3, [pc, #24]	; (8003990 <LL_RCC_SetAPB2Prescaler+0x24>)
 8003978:	689b      	ldr	r3, [r3, #8]
 800397a:	f423 4260 	bic.w	r2, r3, #57344	; 0xe000
 800397e:	687b      	ldr	r3, [r7, #4]
 8003980:	4313      	orrs	r3, r2
 8003982:	608b      	str	r3, [r1, #8]
 8003984:	bf00      	nop
 8003986:	370c      	adds	r7, #12
 8003988:	46bd      	mov	sp, r7
 800398a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800398e:	4770      	bx	lr
 8003990:	40023800 	.word	0x40023800

08003994 <LL_RCC_PLL_Enable>:
 8003994:	b480      	push	{r7}
 8003996:	af00      	add	r7, sp, #0
 8003998:	4a05      	ldr	r2, [pc, #20]	; (80039b0 <LL_RCC_PLL_Enable+0x1c>)
 800399a:	4b05      	ldr	r3, [pc, #20]	; (80039b0 <LL_RCC_PLL_Enable+0x1c>)
 800399c:	681b      	ldr	r3, [r3, #0]
 800399e:	f043 7380 	orr.w	r3, r3, #16777216	; 0x1000000
 80039a2:	6013      	str	r3, [r2, #0]
 80039a4:	bf00      	nop
 80039a6:	46bd      	mov	sp, r7
 80039a8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80039ac:	4770      	bx	lr
 80039ae:	bf00      	nop
 80039b0:	40023800 	.word	0x40023800

080039b4 <LL_RCC_PLL_IsReady>:
 80039b4:	b480      	push	{r7}
 80039b6:	af00      	add	r7, sp, #0
 80039b8:	4b07      	ldr	r3, [pc, #28]	; (80039d8 <LL_RCC_PLL_IsReady+0x24>)
 80039ba:	681b      	ldr	r3, [r3, #0]
 80039bc:	f003 7300 	and.w	r3, r3, #33554432	; 0x2000000
 80039c0:	f1b3 7f00 	cmp.w	r3, #33554432	; 0x2000000
 80039c4:	bf0c      	ite	eq
 80039c6:	2301      	moveq	r3, #1
 80039c8:	2300      	movne	r3, #0
 80039ca:	b2db      	uxtb	r3, r3
 80039cc:	4618      	mov	r0, r3
 80039ce:	46bd      	mov	sp, r7
 80039d0:	f85d 7b04 	ldr.w	r7, [sp], #4
 80039d4:	4770      	bx	lr
 80039d6:	bf00      	nop
 80039d8:	40023800 	.word	0x40023800

080039dc <LL_RCC_PLL_ConfigDomain_SYS>:
 80039dc:	b480      	push	{r7}
 80039de:	b085      	sub	sp, #20
 80039e0:	af00      	add	r7, sp, #0
 80039e2:	60f8      	str	r0, [r7, #12]
 80039e4:	60b9      	str	r1, [r7, #8]
 80039e6:	607a      	str	r2, [r7, #4]
 80039e8:	603b      	str	r3, [r7, #0]
 80039ea:	480d      	ldr	r0, [pc, #52]	; (8003a20 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 80039ec:	4b0c      	ldr	r3, [pc, #48]	; (8003a20 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 80039ee:	685a      	ldr	r2, [r3, #4]
 80039f0:	4b0c      	ldr	r3, [pc, #48]	; (8003a24 <LL_RCC_PLL_ConfigDomain_SYS+0x48>)
 80039f2:	4013      	ands	r3, r2
 80039f4:	68f9      	ldr	r1, [r7, #12]
 80039f6:	68ba      	ldr	r2, [r7, #8]
 80039f8:	4311      	orrs	r1, r2
 80039fa:	687a      	ldr	r2, [r7, #4]
 80039fc:	0192      	lsls	r2, r2, #6
 80039fe:	430a      	orrs	r2, r1
 8003a00:	4313      	orrs	r3, r2
 8003a02:	6043      	str	r3, [r0, #4]
 8003a04:	4906      	ldr	r1, [pc, #24]	; (8003a20 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8003a06:	4b06      	ldr	r3, [pc, #24]	; (8003a20 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8003a08:	685b      	ldr	r3, [r3, #4]
 8003a0a:	f423 3240 	bic.w	r2, r3, #196608	; 0x30000
 8003a0e:	683b      	ldr	r3, [r7, #0]
 8003a10:	4313      	orrs	r3, r2
 8003a12:	604b      	str	r3, [r1, #4]
 8003a14:	bf00      	nop
 8003a16:	3714      	adds	r7, #20
 8003a18:	46bd      	mov	sp, r7
 8003a1a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003a1e:	4770      	bx	lr
 8003a20:	40023800 	.word	0x40023800
 8003a24:	ffbf8000 	.word	0xffbf8000

08003a28 <LL_FLASH_SetLatency>:
 8003a28:	b480      	push	{r7}
 8003a2a:	b083      	sub	sp, #12
 8003a2c:	af00      	add	r7, sp, #0
 8003a2e:	6078      	str	r0, [r7, #4]
 8003a30:	4906      	ldr	r1, [pc, #24]	; (8003a4c <LL_FLASH_SetLatency+0x24>)
 8003a32:	4b06      	ldr	r3, [pc, #24]	; (8003a4c <LL_FLASH_SetLatency+0x24>)
 8003a34:	681b      	ldr	r3, [r3, #0]
 8003a36:	f023 020f 	bic.w	r2, r3, #15
 8003a3a:	687b      	ldr	r3, [r7, #4]
 8003a3c:	4313      	orrs	r3, r2
 8003a3e:	600b      	str	r3, [r1, #0]
 8003a40:	bf00      	nop
 8003a42:	370c      	adds	r7, #12
 8003a44:	46bd      	mov	sp, r7
 8003a46:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003a4a:	4770      	bx	lr
 8003a4c:	40023c00 	.word	0x40023c00

08003a50 <LL_AHB1_GRP1_EnableClock>:
 8003a50:	b480      	push	{r7}
 8003a52:	b085      	sub	sp, #20
 8003a54:	af00      	add	r7, sp, #0
 8003a56:	6078      	str	r0, [r7, #4]
 8003a58:	4908      	ldr	r1, [pc, #32]	; (8003a7c <LL_AHB1_GRP1_EnableClock+0x2c>)
 8003a5a:	4b08      	ldr	r3, [pc, #32]	; (8003a7c <LL_AHB1_GRP1_EnableClock+0x2c>)
 8003a5c:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8003a5e:	687b      	ldr	r3, [r7, #4]
 8003a60:	4313      	orrs	r3, r2
 8003a62:	630b      	str	r3, [r1, #48]	; 0x30
 8003a64:	4b05      	ldr	r3, [pc, #20]	; (8003a7c <LL_AHB1_GRP1_EnableClock+0x2c>)
 8003a66:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8003a68:	687b      	ldr	r3, [r7, #4]
 8003a6a:	4013      	ands	r3, r2
 8003a6c:	60fb      	str	r3, [r7, #12]
 8003a6e:	68fb      	ldr	r3, [r7, #12]
 8003a70:	bf00      	nop
 8003a72:	3714      	adds	r7, #20
 8003a74:	46bd      	mov	sp, r7
 8003a76:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003a7a:	4770      	bx	lr
 8003a7c:	40023800 	.word	0x40023800

08003a80 <LL_GPIO_SetPinMode>:
 8003a80:	b480      	push	{r7}
 8003a82:	b089      	sub	sp, #36	; 0x24
 8003a84:	af00      	add	r7, sp, #0
 8003a86:	60f8      	str	r0, [r7, #12]
 8003a88:	60b9      	str	r1, [r7, #8]
 8003a8a:	607a      	str	r2, [r7, #4]
 8003a8c:	68fb      	ldr	r3, [r7, #12]
 8003a8e:	681a      	ldr	r2, [r3, #0]
 8003a90:	68bb      	ldr	r3, [r7, #8]
 8003a92:	617b      	str	r3, [r7, #20]
 8003a94:	697b      	ldr	r3, [r7, #20]
 8003a96:	fa93 f3a3 	rbit	r3, r3
 8003a9a:	613b      	str	r3, [r7, #16]
 8003a9c:	693b      	ldr	r3, [r7, #16]
 8003a9e:	fab3 f383 	clz	r3, r3
 8003aa2:	005b      	lsls	r3, r3, #1
 8003aa4:	2103      	movs	r1, #3
 8003aa6:	fa01 f303 	lsl.w	r3, r1, r3
 8003aaa:	43db      	mvns	r3, r3
 8003aac:	401a      	ands	r2, r3
 8003aae:	68bb      	ldr	r3, [r7, #8]
 8003ab0:	61fb      	str	r3, [r7, #28]
 8003ab2:	69fb      	ldr	r3, [r7, #28]
 8003ab4:	fa93 f3a3 	rbit	r3, r3
 8003ab8:	61bb      	str	r3, [r7, #24]
 8003aba:	69bb      	ldr	r3, [r7, #24]
 8003abc:	fab3 f383 	clz	r3, r3
 8003ac0:	005b      	lsls	r3, r3, #1
 8003ac2:	6879      	ldr	r1, [r7, #4]
 8003ac4:	fa01 f303 	lsl.w	r3, r1, r3
 8003ac8:	431a      	orrs	r2, r3
 8003aca:	68fb      	ldr	r3, [r7, #12]
 8003acc:	601a      	str	r2, [r3, #0]
 8003ace:	bf00      	nop
 8003ad0:	3724      	adds	r7, #36	; 0x24
 8003ad2:	46bd      	mov	sp, r7
 8003ad4:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003ad8:	4770      	bx	lr
	...

08003adc <rcc_config>:
 8003adc:	b580      	push	{r7, lr}
 8003ade:	af00      	add	r7, sp, #0
 8003ae0:	f7ff fed6 	bl	8003890 <LL_RCC_HSE_Enable>
 8003ae4:	bf00      	nop
 8003ae6:	f7ff fee3 	bl	80038b0 <LL_RCC_HSE_IsReady>
 8003aea:	4603      	mov	r3, r0
 8003aec:	2b01      	cmp	r3, #1
 8003aee:	d1fa      	bne.n	8003ae6 <rcc_config+0xa>
 8003af0:	2005      	movs	r0, #5
 8003af2:	f7ff ff99 	bl	8003a28 <LL_FLASH_SetLatency>
 8003af6:	2300      	movs	r3, #0
 8003af8:	f44f 72a8 	mov.w	r2, #336	; 0x150
 8003afc:	2108      	movs	r1, #8
 8003afe:	f44f 0080 	mov.w	r0, #4194304	; 0x400000
 8003b02:	f7ff ff6b 	bl	80039dc <LL_RCC_PLL_ConfigDomain_SYS>
 8003b06:	f7ff ff45 	bl	8003994 <LL_RCC_PLL_Enable>
 8003b0a:	bf00      	nop
 8003b0c:	f7ff ff52 	bl	80039b4 <LL_RCC_PLL_IsReady>
 8003b10:	4603      	mov	r3, r0
 8003b12:	2b01      	cmp	r3, #1
 8003b14:	d1fa      	bne.n	8003b0c <rcc_config+0x30>
 8003b16:	2000      	movs	r0, #0
 8003b18:	f7ff ff00 	bl	800391c <LL_RCC_SetAHBPrescaler>
 8003b1c:	2002      	movs	r0, #2
 8003b1e:	f7ff fedb 	bl	80038d8 <LL_RCC_SetSysClkSource>
 8003b22:	bf00      	nop
 8003b24:	f7ff feec 	bl	8003900 <LL_RCC_GetSysClkSource>
 8003b28:	4603      	mov	r3, r0
 8003b2a:	2b08      	cmp	r3, #8
 8003b2c:	d1fa      	bne.n	8003b24 <rcc_config+0x48>
 8003b2e:	f44f 50a0 	mov.w	r0, #5120	; 0x1400
 8003b32:	f7ff ff07 	bl	8003944 <LL_RCC_SetAPB1Prescaler>
 8003b36:	f44f 4000 	mov.w	r0, #32768	; 0x8000
 8003b3a:	f7ff ff17 	bl	800396c <LL_RCC_SetAPB2Prescaler>
 8003b3e:	4804      	ldr	r0, [pc, #16]	; (8003b50 <rcc_config+0x74>)
 8003b40:	f7ff fe84 	bl	800384c <SysTick_Config>
 8003b44:	4b03      	ldr	r3, [pc, #12]	; (8003b54 <rcc_config+0x78>)
 8003b46:	4a04      	ldr	r2, [pc, #16]	; (8003b58 <rcc_config+0x7c>)
 8003b48:	601a      	str	r2, [r3, #0]
 8003b4a:	bf00      	nop
 8003b4c:	bd80      	pop	{r7, pc}
 8003b4e:	bf00      	nop
 8003b50:	00029040 	.word	0x00029040
 8003b54:	200000e4 	.word	0x200000e4
 8003b58:	0a037a00 	.word	0x0a037a00

08003b5c <gpio_config>:
 8003b5c:	b580      	push	{r7, lr}
 8003b5e:	af00      	add	r7, sp, #0
 8003b60:	2008      	movs	r0, #8
 8003b62:	f7ff ff75 	bl	8003a50 <LL_AHB1_GRP1_EnableClock>
 8003b66:	2201      	movs	r2, #1
 8003b68:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 8003b6c:	4802      	ldr	r0, [pc, #8]	; (8003b78 <gpio_config+0x1c>)
 8003b6e:	f7ff ff87 	bl	8003a80 <LL_GPIO_SetPinMode>
 8003b72:	bf00      	nop
 8003b74:	bd80      	pop	{r7, pc}
 8003b76:	bf00      	nop
 8003b78:	40020c00 	.word	0x40020c00

08003b7c <main>:
 8003b7c:	b580      	push	{r7, lr}
 8003b7e:	af00      	add	r7, sp, #0
 8003b80:	f7ff ffac 	bl	8003adc <rcc_config>
 8003b84:	f7ff ffea 	bl	8003b5c <gpio_config>
 8003b88:	2000      	movs	r0, #0
 8003b8a:	f7ff fe11 	bl	80037b0 <NVIC_SetPriorityGrouping>
 8003b8e:	2300      	movs	r3, #0
 8003b90:	4618      	mov	r0, r3
 8003b92:	bd80      	pop	{r7, pc}

08003b94 <_malloc_r>:
 8003b94:	b580      	push	{r7, lr}
 8003b96:	b082      	sub	sp, #8
 8003b98:	af00      	add	r7, sp, #0
 8003b9a:	6078      	str	r0, [r7, #4]
 8003b9c:	6039      	str	r1, [r7, #0]
 8003b9e:	6838      	ldr	r0, [r7, #0]
 8003ba0:	f000 fb52 	bl	8004248 <pvPortMalloc>
 8003ba4:	4603      	mov	r3, r0
 8003ba6:	4618      	mov	r0, r3
 8003ba8:	3708      	adds	r7, #8
 8003baa:	46bd      	mov	sp, r7
 8003bac:	bd80      	pop	{r7, pc}

08003bae <_free_r>:
 8003bae:	b580      	push	{r7, lr}
 8003bb0:	b082      	sub	sp, #8
 8003bb2:	af00      	add	r7, sp, #0
 8003bb4:	6078      	str	r0, [r7, #4]
 8003bb6:	6039      	str	r1, [r7, #0]
 8003bb8:	6838      	ldr	r0, [r7, #0]
 8003bba:	f000 fc13 	bl	80043e4 <vPortFree>
 8003bbe:	bf00      	nop
 8003bc0:	3708      	adds	r7, #8
 8003bc2:	46bd      	mov	sp, r7
 8003bc4:	bd80      	pop	{r7, pc}

08003bc6 <vApplicationMallocFailedHook>:
 8003bc6:	b480      	push	{r7}
 8003bc8:	af00      	add	r7, sp, #0
 8003bca:	bf00      	nop
 8003bcc:	46bd      	mov	sp, r7
 8003bce:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003bd2:	4770      	bx	lr

08003bd4 <vApplicationTickHook>:
 8003bd4:	b480      	push	{r7}
 8003bd6:	af00      	add	r7, sp, #0
 8003bd8:	bf00      	nop
 8003bda:	46bd      	mov	sp, r7
 8003bdc:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003be0:	4770      	bx	lr

08003be2 <vApplicationStackOverflowHook>:
 8003be2:	b480      	push	{r7}
 8003be4:	b085      	sub	sp, #20
 8003be6:	af00      	add	r7, sp, #0
 8003be8:	6078      	str	r0, [r7, #4]
 8003bea:	6039      	str	r1, [r7, #0]
 8003bec:	f04f 0350 	mov.w	r3, #80	; 0x50
 8003bf0:	f383 8811 	msr	BASEPRI, r3
 8003bf4:	f3bf 8f6f 	isb	sy
 8003bf8:	f3bf 8f4f 	dsb	sy
 8003bfc:	60fb      	str	r3, [r7, #12]
 8003bfe:	e7fe      	b.n	8003bfe <vApplicationStackOverflowHook+0x1c>

08003c00 <vListInsertEnd>:
 8003c00:	b480      	push	{r7}
 8003c02:	b085      	sub	sp, #20
 8003c04:	af00      	add	r7, sp, #0
 8003c06:	6078      	str	r0, [r7, #4]
 8003c08:	6039      	str	r1, [r7, #0]
 8003c0a:	687b      	ldr	r3, [r7, #4]
 8003c0c:	685b      	ldr	r3, [r3, #4]
 8003c0e:	60fb      	str	r3, [r7, #12]
 8003c10:	683b      	ldr	r3, [r7, #0]
 8003c12:	68fa      	ldr	r2, [r7, #12]
 8003c14:	605a      	str	r2, [r3, #4]
 8003c16:	68fb      	ldr	r3, [r7, #12]
 8003c18:	689a      	ldr	r2, [r3, #8]
 8003c1a:	683b      	ldr	r3, [r7, #0]
 8003c1c:	609a      	str	r2, [r3, #8]
 8003c1e:	68fb      	ldr	r3, [r7, #12]
 8003c20:	689b      	ldr	r3, [r3, #8]
 8003c22:	683a      	ldr	r2, [r7, #0]
 8003c24:	605a      	str	r2, [r3, #4]
 8003c26:	68fb      	ldr	r3, [r7, #12]
 8003c28:	683a      	ldr	r2, [r7, #0]
 8003c2a:	609a      	str	r2, [r3, #8]
 8003c2c:	683b      	ldr	r3, [r7, #0]
 8003c2e:	687a      	ldr	r2, [r7, #4]
 8003c30:	611a      	str	r2, [r3, #16]
 8003c32:	687b      	ldr	r3, [r7, #4]
 8003c34:	681b      	ldr	r3, [r3, #0]
 8003c36:	1c5a      	adds	r2, r3, #1
 8003c38:	687b      	ldr	r3, [r7, #4]
 8003c3a:	601a      	str	r2, [r3, #0]
 8003c3c:	bf00      	nop
 8003c3e:	3714      	adds	r7, #20
 8003c40:	46bd      	mov	sp, r7
 8003c42:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003c46:	4770      	bx	lr

08003c48 <uxListRemove>:
 8003c48:	b480      	push	{r7}
 8003c4a:	b085      	sub	sp, #20
 8003c4c:	af00      	add	r7, sp, #0
 8003c4e:	6078      	str	r0, [r7, #4]
 8003c50:	687b      	ldr	r3, [r7, #4]
 8003c52:	691b      	ldr	r3, [r3, #16]
 8003c54:	60fb      	str	r3, [r7, #12]
 8003c56:	687b      	ldr	r3, [r7, #4]
 8003c58:	685b      	ldr	r3, [r3, #4]
 8003c5a:	687a      	ldr	r2, [r7, #4]
 8003c5c:	6892      	ldr	r2, [r2, #8]
 8003c5e:	609a      	str	r2, [r3, #8]
 8003c60:	687b      	ldr	r3, [r7, #4]
 8003c62:	689b      	ldr	r3, [r3, #8]
 8003c64:	687a      	ldr	r2, [r7, #4]
 8003c66:	6852      	ldr	r2, [r2, #4]
 8003c68:	605a      	str	r2, [r3, #4]
 8003c6a:	68fb      	ldr	r3, [r7, #12]
 8003c6c:	685a      	ldr	r2, [r3, #4]
 8003c6e:	687b      	ldr	r3, [r7, #4]
 8003c70:	429a      	cmp	r2, r3
 8003c72:	d103      	bne.n	8003c7c <uxListRemove+0x34>
 8003c74:	687b      	ldr	r3, [r7, #4]
 8003c76:	689a      	ldr	r2, [r3, #8]
 8003c78:	68fb      	ldr	r3, [r7, #12]
 8003c7a:	605a      	str	r2, [r3, #4]
 8003c7c:	687b      	ldr	r3, [r7, #4]
 8003c7e:	2200      	movs	r2, #0
 8003c80:	611a      	str	r2, [r3, #16]
 8003c82:	68fb      	ldr	r3, [r7, #12]
 8003c84:	681b      	ldr	r3, [r3, #0]
 8003c86:	1e5a      	subs	r2, r3, #1
 8003c88:	68fb      	ldr	r3, [r7, #12]
 8003c8a:	601a      	str	r2, [r3, #0]
 8003c8c:	68fb      	ldr	r3, [r7, #12]
 8003c8e:	681b      	ldr	r3, [r3, #0]
 8003c90:	4618      	mov	r0, r3
 8003c92:	3714      	adds	r7, #20
 8003c94:	46bd      	mov	sp, r7
 8003c96:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003c9a:	4770      	bx	lr

08003c9c <vTaskSuspendAll>:
 8003c9c:	b480      	push	{r7}
 8003c9e:	af00      	add	r7, sp, #0
 8003ca0:	4b04      	ldr	r3, [pc, #16]	; (8003cb4 <vTaskSuspendAll+0x18>)
 8003ca2:	681b      	ldr	r3, [r3, #0]
 8003ca4:	3301      	adds	r3, #1
 8003ca6:	4a03      	ldr	r2, [pc, #12]	; (8003cb4 <vTaskSuspendAll+0x18>)
 8003ca8:	6013      	str	r3, [r2, #0]
 8003caa:	bf00      	nop
 8003cac:	46bd      	mov	sp, r7
 8003cae:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003cb2:	4770      	bx	lr
 8003cb4:	200001a4 	.word	0x200001a4

08003cb8 <xTaskResumeAll>:
 8003cb8:	b580      	push	{r7, lr}
 8003cba:	b084      	sub	sp, #16
 8003cbc:	af00      	add	r7, sp, #0
 8003cbe:	2300      	movs	r3, #0
 8003cc0:	60fb      	str	r3, [r7, #12]
 8003cc2:	2300      	movs	r3, #0
 8003cc4:	60bb      	str	r3, [r7, #8]
 8003cc6:	4b41      	ldr	r3, [pc, #260]	; (8003dcc <xTaskResumeAll+0x114>)
 8003cc8:	681b      	ldr	r3, [r3, #0]
 8003cca:	2b00      	cmp	r3, #0
 8003ccc:	d109      	bne.n	8003ce2 <xTaskResumeAll+0x2a>
 8003cce:	f04f 0350 	mov.w	r3, #80	; 0x50
 8003cd2:	f383 8811 	msr	BASEPRI, r3
 8003cd6:	f3bf 8f6f 	isb	sy
 8003cda:	f3bf 8f4f 	dsb	sy
 8003cde:	603b      	str	r3, [r7, #0]
 8003ce0:	e7fe      	b.n	8003ce0 <xTaskResumeAll+0x28>
 8003ce2:	f000 fa01 	bl	80040e8 <vPortEnterCritical>
 8003ce6:	4b39      	ldr	r3, [pc, #228]	; (8003dcc <xTaskResumeAll+0x114>)
 8003ce8:	681b      	ldr	r3, [r3, #0]
 8003cea:	3b01      	subs	r3, #1
 8003cec:	4a37      	ldr	r2, [pc, #220]	; (8003dcc <xTaskResumeAll+0x114>)
 8003cee:	6013      	str	r3, [r2, #0]
 8003cf0:	4b36      	ldr	r3, [pc, #216]	; (8003dcc <xTaskResumeAll+0x114>)
 8003cf2:	681b      	ldr	r3, [r3, #0]
 8003cf4:	2b00      	cmp	r3, #0
 8003cf6:	d161      	bne.n	8003dbc <xTaskResumeAll+0x104>
 8003cf8:	4b35      	ldr	r3, [pc, #212]	; (8003dd0 <xTaskResumeAll+0x118>)
 8003cfa:	681b      	ldr	r3, [r3, #0]
 8003cfc:	2b00      	cmp	r3, #0
 8003cfe:	d05d      	beq.n	8003dbc <xTaskResumeAll+0x104>
 8003d00:	e02e      	b.n	8003d60 <xTaskResumeAll+0xa8>
 8003d02:	4b34      	ldr	r3, [pc, #208]	; (8003dd4 <xTaskResumeAll+0x11c>)
 8003d04:	68db      	ldr	r3, [r3, #12]
 8003d06:	68db      	ldr	r3, [r3, #12]
 8003d08:	60fb      	str	r3, [r7, #12]
 8003d0a:	68fb      	ldr	r3, [r7, #12]
 8003d0c:	3318      	adds	r3, #24
 8003d0e:	4618      	mov	r0, r3
 8003d10:	f7ff ff9a 	bl	8003c48 <uxListRemove>
 8003d14:	68fb      	ldr	r3, [r7, #12]
 8003d16:	3304      	adds	r3, #4
 8003d18:	4618      	mov	r0, r3
 8003d1a:	f7ff ff95 	bl	8003c48 <uxListRemove>
 8003d1e:	68fb      	ldr	r3, [r7, #12]
 8003d20:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8003d22:	2201      	movs	r2, #1
 8003d24:	409a      	lsls	r2, r3
 8003d26:	4b2c      	ldr	r3, [pc, #176]	; (8003dd8 <xTaskResumeAll+0x120>)
 8003d28:	681b      	ldr	r3, [r3, #0]
 8003d2a:	4313      	orrs	r3, r2
 8003d2c:	4a2a      	ldr	r2, [pc, #168]	; (8003dd8 <xTaskResumeAll+0x120>)
 8003d2e:	6013      	str	r3, [r2, #0]
 8003d30:	68fb      	ldr	r3, [r7, #12]
 8003d32:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8003d34:	4613      	mov	r3, r2
 8003d36:	009b      	lsls	r3, r3, #2
 8003d38:	4413      	add	r3, r2
 8003d3a:	009b      	lsls	r3, r3, #2
 8003d3c:	4a27      	ldr	r2, [pc, #156]	; (8003ddc <xTaskResumeAll+0x124>)
 8003d3e:	441a      	add	r2, r3
 8003d40:	68fb      	ldr	r3, [r7, #12]
 8003d42:	3304      	adds	r3, #4
 8003d44:	4619      	mov	r1, r3
 8003d46:	4610      	mov	r0, r2
 8003d48:	f7ff ff5a 	bl	8003c00 <vListInsertEnd>
 8003d4c:	68fb      	ldr	r3, [r7, #12]
 8003d4e:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8003d50:	4b23      	ldr	r3, [pc, #140]	; (8003de0 <xTaskResumeAll+0x128>)
 8003d52:	681b      	ldr	r3, [r3, #0]
 8003d54:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8003d56:	429a      	cmp	r2, r3
 8003d58:	d302      	bcc.n	8003d60 <xTaskResumeAll+0xa8>
 8003d5a:	4b22      	ldr	r3, [pc, #136]	; (8003de4 <xTaskResumeAll+0x12c>)
 8003d5c:	2201      	movs	r2, #1
 8003d5e:	601a      	str	r2, [r3, #0]
 8003d60:	4b1c      	ldr	r3, [pc, #112]	; (8003dd4 <xTaskResumeAll+0x11c>)
 8003d62:	681b      	ldr	r3, [r3, #0]
 8003d64:	2b00      	cmp	r3, #0
 8003d66:	d1cc      	bne.n	8003d02 <xTaskResumeAll+0x4a>
 8003d68:	68fb      	ldr	r3, [r7, #12]
 8003d6a:	2b00      	cmp	r3, #0
 8003d6c:	d001      	beq.n	8003d72 <xTaskResumeAll+0xba>
 8003d6e:	f000 f985 	bl	800407c <prvResetNextTaskUnblockTime>
 8003d72:	4b1d      	ldr	r3, [pc, #116]	; (8003de8 <xTaskResumeAll+0x130>)
 8003d74:	681b      	ldr	r3, [r3, #0]
 8003d76:	607b      	str	r3, [r7, #4]
 8003d78:	687b      	ldr	r3, [r7, #4]
 8003d7a:	2b00      	cmp	r3, #0
 8003d7c:	d010      	beq.n	8003da0 <xTaskResumeAll+0xe8>
 8003d7e:	f000 f837 	bl	8003df0 <xTaskIncrementTick>
 8003d82:	4603      	mov	r3, r0
 8003d84:	2b00      	cmp	r3, #0
 8003d86:	d002      	beq.n	8003d8e <xTaskResumeAll+0xd6>
 8003d88:	4b16      	ldr	r3, [pc, #88]	; (8003de4 <xTaskResumeAll+0x12c>)
 8003d8a:	2201      	movs	r2, #1
 8003d8c:	601a      	str	r2, [r3, #0]
 8003d8e:	687b      	ldr	r3, [r7, #4]
 8003d90:	3b01      	subs	r3, #1
 8003d92:	607b      	str	r3, [r7, #4]
 8003d94:	687b      	ldr	r3, [r7, #4]
 8003d96:	2b00      	cmp	r3, #0
 8003d98:	d1f1      	bne.n	8003d7e <xTaskResumeAll+0xc6>
 8003d9a:	4b13      	ldr	r3, [pc, #76]	; (8003de8 <xTaskResumeAll+0x130>)
 8003d9c:	2200      	movs	r2, #0
 8003d9e:	601a      	str	r2, [r3, #0]
 8003da0:	4b10      	ldr	r3, [pc, #64]	; (8003de4 <xTaskResumeAll+0x12c>)
 8003da2:	681b      	ldr	r3, [r3, #0]
 8003da4:	2b00      	cmp	r3, #0
 8003da6:	d009      	beq.n	8003dbc <xTaskResumeAll+0x104>
 8003da8:	2301      	movs	r3, #1
 8003daa:	60bb      	str	r3, [r7, #8]
 8003dac:	4b0f      	ldr	r3, [pc, #60]	; (8003dec <xTaskResumeAll+0x134>)
 8003dae:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8003db2:	601a      	str	r2, [r3, #0]
 8003db4:	f3bf 8f4f 	dsb	sy
 8003db8:	f3bf 8f6f 	isb	sy
 8003dbc:	f000 f9c2 	bl	8004144 <vPortExitCritical>
 8003dc0:	68bb      	ldr	r3, [r7, #8]
 8003dc2:	4618      	mov	r0, r3
 8003dc4:	3710      	adds	r7, #16
 8003dc6:	46bd      	mov	sp, r7
 8003dc8:	bd80      	pop	{r7, pc}
 8003dca:	bf00      	nop
 8003dcc:	200001a4 	.word	0x200001a4
 8003dd0:	20000188 	.word	0x20000188
 8003dd4:	20000174 	.word	0x20000174
 8003dd8:	20000190 	.word	0x20000190
 8003ddc:	20000108 	.word	0x20000108
 8003de0:	20000104 	.word	0x20000104
 8003de4:	20000198 	.word	0x20000198
 8003de8:	20000194 	.word	0x20000194
 8003dec:	e000ed04 	.word	0xe000ed04

08003df0 <xTaskIncrementTick>:
 8003df0:	b580      	push	{r7, lr}
 8003df2:	b086      	sub	sp, #24
 8003df4:	af00      	add	r7, sp, #0
 8003df6:	2300      	movs	r3, #0
 8003df8:	617b      	str	r3, [r7, #20]
 8003dfa:	4b52      	ldr	r3, [pc, #328]	; (8003f44 <xTaskIncrementTick+0x154>)
 8003dfc:	681b      	ldr	r3, [r3, #0]
 8003dfe:	2b00      	cmp	r3, #0
 8003e00:	f040 8093 	bne.w	8003f2a <xTaskIncrementTick+0x13a>
 8003e04:	4b50      	ldr	r3, [pc, #320]	; (8003f48 <xTaskIncrementTick+0x158>)
 8003e06:	681b      	ldr	r3, [r3, #0]
 8003e08:	3301      	adds	r3, #1
 8003e0a:	613b      	str	r3, [r7, #16]
 8003e0c:	4a4e      	ldr	r2, [pc, #312]	; (8003f48 <xTaskIncrementTick+0x158>)
 8003e0e:	693b      	ldr	r3, [r7, #16]
 8003e10:	6013      	str	r3, [r2, #0]
 8003e12:	693b      	ldr	r3, [r7, #16]
 8003e14:	2b00      	cmp	r3, #0
 8003e16:	d11f      	bne.n	8003e58 <xTaskIncrementTick+0x68>
 8003e18:	4b4c      	ldr	r3, [pc, #304]	; (8003f4c <xTaskIncrementTick+0x15c>)
 8003e1a:	681b      	ldr	r3, [r3, #0]
 8003e1c:	681b      	ldr	r3, [r3, #0]
 8003e1e:	2b00      	cmp	r3, #0
 8003e20:	d009      	beq.n	8003e36 <xTaskIncrementTick+0x46>
 8003e22:	f04f 0350 	mov.w	r3, #80	; 0x50
 8003e26:	f383 8811 	msr	BASEPRI, r3
 8003e2a:	f3bf 8f6f 	isb	sy
 8003e2e:	f3bf 8f4f 	dsb	sy
 8003e32:	603b      	str	r3, [r7, #0]
 8003e34:	e7fe      	b.n	8003e34 <xTaskIncrementTick+0x44>
 8003e36:	4b45      	ldr	r3, [pc, #276]	; (8003f4c <xTaskIncrementTick+0x15c>)
 8003e38:	681b      	ldr	r3, [r3, #0]
 8003e3a:	60fb      	str	r3, [r7, #12]
 8003e3c:	4b44      	ldr	r3, [pc, #272]	; (8003f50 <xTaskIncrementTick+0x160>)
 8003e3e:	681b      	ldr	r3, [r3, #0]
 8003e40:	4a42      	ldr	r2, [pc, #264]	; (8003f4c <xTaskIncrementTick+0x15c>)
 8003e42:	6013      	str	r3, [r2, #0]
 8003e44:	4a42      	ldr	r2, [pc, #264]	; (8003f50 <xTaskIncrementTick+0x160>)
 8003e46:	68fb      	ldr	r3, [r7, #12]
 8003e48:	6013      	str	r3, [r2, #0]
 8003e4a:	4b42      	ldr	r3, [pc, #264]	; (8003f54 <xTaskIncrementTick+0x164>)
 8003e4c:	681b      	ldr	r3, [r3, #0]
 8003e4e:	3301      	adds	r3, #1
 8003e50:	4a40      	ldr	r2, [pc, #256]	; (8003f54 <xTaskIncrementTick+0x164>)
 8003e52:	6013      	str	r3, [r2, #0]
 8003e54:	f000 f912 	bl	800407c <prvResetNextTaskUnblockTime>
 8003e58:	4b3f      	ldr	r3, [pc, #252]	; (8003f58 <xTaskIncrementTick+0x168>)
 8003e5a:	681b      	ldr	r3, [r3, #0]
 8003e5c:	693a      	ldr	r2, [r7, #16]
 8003e5e:	429a      	cmp	r2, r3
 8003e60:	d348      	bcc.n	8003ef4 <xTaskIncrementTick+0x104>
 8003e62:	4b3a      	ldr	r3, [pc, #232]	; (8003f4c <xTaskIncrementTick+0x15c>)
 8003e64:	681b      	ldr	r3, [r3, #0]
 8003e66:	681b      	ldr	r3, [r3, #0]
 8003e68:	2b00      	cmp	r3, #0
 8003e6a:	d104      	bne.n	8003e76 <xTaskIncrementTick+0x86>
 8003e6c:	4b3a      	ldr	r3, [pc, #232]	; (8003f58 <xTaskIncrementTick+0x168>)
 8003e6e:	f04f 32ff 	mov.w	r2, #4294967295	; 0xffffffff
 8003e72:	601a      	str	r2, [r3, #0]
 8003e74:	e03e      	b.n	8003ef4 <xTaskIncrementTick+0x104>
 8003e76:	4b35      	ldr	r3, [pc, #212]	; (8003f4c <xTaskIncrementTick+0x15c>)
 8003e78:	681b      	ldr	r3, [r3, #0]
 8003e7a:	68db      	ldr	r3, [r3, #12]
 8003e7c:	68db      	ldr	r3, [r3, #12]
 8003e7e:	60bb      	str	r3, [r7, #8]
 8003e80:	68bb      	ldr	r3, [r7, #8]
 8003e82:	685b      	ldr	r3, [r3, #4]
 8003e84:	607b      	str	r3, [r7, #4]
 8003e86:	693a      	ldr	r2, [r7, #16]
 8003e88:	687b      	ldr	r3, [r7, #4]
 8003e8a:	429a      	cmp	r2, r3
 8003e8c:	d203      	bcs.n	8003e96 <xTaskIncrementTick+0xa6>
 8003e8e:	4a32      	ldr	r2, [pc, #200]	; (8003f58 <xTaskIncrementTick+0x168>)
 8003e90:	687b      	ldr	r3, [r7, #4]
 8003e92:	6013      	str	r3, [r2, #0]
 8003e94:	e02e      	b.n	8003ef4 <xTaskIncrementTick+0x104>
 8003e96:	68bb      	ldr	r3, [r7, #8]
 8003e98:	3304      	adds	r3, #4
 8003e9a:	4618      	mov	r0, r3
 8003e9c:	f7ff fed4 	bl	8003c48 <uxListRemove>
 8003ea0:	68bb      	ldr	r3, [r7, #8]
 8003ea2:	6a9b      	ldr	r3, [r3, #40]	; 0x28
 8003ea4:	2b00      	cmp	r3, #0
 8003ea6:	d004      	beq.n	8003eb2 <xTaskIncrementTick+0xc2>
 8003ea8:	68bb      	ldr	r3, [r7, #8]
 8003eaa:	3318      	adds	r3, #24
 8003eac:	4618      	mov	r0, r3
 8003eae:	f7ff fecb 	bl	8003c48 <uxListRemove>
 8003eb2:	68bb      	ldr	r3, [r7, #8]
 8003eb4:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8003eb6:	2201      	movs	r2, #1
 8003eb8:	409a      	lsls	r2, r3
 8003eba:	4b28      	ldr	r3, [pc, #160]	; (8003f5c <xTaskIncrementTick+0x16c>)
 8003ebc:	681b      	ldr	r3, [r3, #0]
 8003ebe:	4313      	orrs	r3, r2
 8003ec0:	4a26      	ldr	r2, [pc, #152]	; (8003f5c <xTaskIncrementTick+0x16c>)
 8003ec2:	6013      	str	r3, [r2, #0]
 8003ec4:	68bb      	ldr	r3, [r7, #8]
 8003ec6:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8003ec8:	4613      	mov	r3, r2
 8003eca:	009b      	lsls	r3, r3, #2
 8003ecc:	4413      	add	r3, r2
 8003ece:	009b      	lsls	r3, r3, #2
 8003ed0:	4a23      	ldr	r2, [pc, #140]	; (8003f60 <xTaskIncrementTick+0x170>)
 8003ed2:	441a      	add	r2, r3
 8003ed4:	68bb      	ldr	r3, [r7, #8]
 8003ed6:	3304      	adds	r3, #4
 8003ed8:	4619      	mov	r1, r3
 8003eda:	4610      	mov	r0, r2
 8003edc:	f7ff fe90 	bl	8003c00 <vListInsertEnd>
 8003ee0:	68bb      	ldr	r3, [r7, #8]
 8003ee2:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8003ee4:	4b1f      	ldr	r3, [pc, #124]	; (8003f64 <xTaskIncrementTick+0x174>)
 8003ee6:	681b      	ldr	r3, [r3, #0]
 8003ee8:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8003eea:	429a      	cmp	r2, r3
 8003eec:	d3b9      	bcc.n	8003e62 <xTaskIncrementTick+0x72>
 8003eee:	2301      	movs	r3, #1
 8003ef0:	617b      	str	r3, [r7, #20]
 8003ef2:	e7b6      	b.n	8003e62 <xTaskIncrementTick+0x72>
 8003ef4:	4b1b      	ldr	r3, [pc, #108]	; (8003f64 <xTaskIncrementTick+0x174>)
 8003ef6:	681b      	ldr	r3, [r3, #0]
 8003ef8:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8003efa:	4919      	ldr	r1, [pc, #100]	; (8003f60 <xTaskIncrementTick+0x170>)
 8003efc:	4613      	mov	r3, r2
 8003efe:	009b      	lsls	r3, r3, #2
 8003f00:	4413      	add	r3, r2
 8003f02:	009b      	lsls	r3, r3, #2
 8003f04:	440b      	add	r3, r1
 8003f06:	681b      	ldr	r3, [r3, #0]
 8003f08:	2b01      	cmp	r3, #1
 8003f0a:	d901      	bls.n	8003f10 <xTaskIncrementTick+0x120>
 8003f0c:	2301      	movs	r3, #1
 8003f0e:	617b      	str	r3, [r7, #20]
 8003f10:	4b15      	ldr	r3, [pc, #84]	; (8003f68 <xTaskIncrementTick+0x178>)
 8003f12:	681b      	ldr	r3, [r3, #0]
 8003f14:	2b00      	cmp	r3, #0
 8003f16:	d101      	bne.n	8003f1c <xTaskIncrementTick+0x12c>
 8003f18:	f7ff fe5c 	bl	8003bd4 <vApplicationTickHook>
 8003f1c:	4b13      	ldr	r3, [pc, #76]	; (8003f6c <xTaskIncrementTick+0x17c>)
 8003f1e:	681b      	ldr	r3, [r3, #0]
 8003f20:	2b00      	cmp	r3, #0
 8003f22:	d009      	beq.n	8003f38 <xTaskIncrementTick+0x148>
 8003f24:	2301      	movs	r3, #1
 8003f26:	617b      	str	r3, [r7, #20]
 8003f28:	e006      	b.n	8003f38 <xTaskIncrementTick+0x148>
 8003f2a:	4b0f      	ldr	r3, [pc, #60]	; (8003f68 <xTaskIncrementTick+0x178>)
 8003f2c:	681b      	ldr	r3, [r3, #0]
 8003f2e:	3301      	adds	r3, #1
 8003f30:	4a0d      	ldr	r2, [pc, #52]	; (8003f68 <xTaskIncrementTick+0x178>)
 8003f32:	6013      	str	r3, [r2, #0]
 8003f34:	f7ff fe4e 	bl	8003bd4 <vApplicationTickHook>
 8003f38:	697b      	ldr	r3, [r7, #20]
 8003f3a:	4618      	mov	r0, r3
 8003f3c:	3718      	adds	r7, #24
 8003f3e:	46bd      	mov	sp, r7
 8003f40:	bd80      	pop	{r7, pc}
 8003f42:	bf00      	nop
 8003f44:	200001a4 	.word	0x200001a4
 8003f48:	2000018c 	.word	0x2000018c
 8003f4c:	2000016c 	.word	0x2000016c
 8003f50:	20000170 	.word	0x20000170
 8003f54:	2000019c 	.word	0x2000019c
 8003f58:	200001a0 	.word	0x200001a0
 8003f5c:	20000190 	.word	0x20000190
 8003f60:	20000108 	.word	0x20000108
 8003f64:	20000104 	.word	0x20000104
 8003f68:	20000194 	.word	0x20000194
 8003f6c:	20000198 	.word	0x20000198

08003f70 <vTaskSwitchContext>:
 8003f70:	b580      	push	{r7, lr}
 8003f72:	b088      	sub	sp, #32
 8003f74:	af00      	add	r7, sp, #0
 8003f76:	4b3b      	ldr	r3, [pc, #236]	; (8004064 <vTaskSwitchContext+0xf4>)
 8003f78:	681b      	ldr	r3, [r3, #0]
 8003f7a:	2b00      	cmp	r3, #0
 8003f7c:	d003      	beq.n	8003f86 <vTaskSwitchContext+0x16>
 8003f7e:	4b3a      	ldr	r3, [pc, #232]	; (8004068 <vTaskSwitchContext+0xf8>)
 8003f80:	2201      	movs	r2, #1
 8003f82:	601a      	str	r2, [r3, #0]
 8003f84:	e06a      	b.n	800405c <vTaskSwitchContext+0xec>
 8003f86:	4b38      	ldr	r3, [pc, #224]	; (8004068 <vTaskSwitchContext+0xf8>)
 8003f88:	2200      	movs	r2, #0
 8003f8a:	601a      	str	r2, [r3, #0]
 8003f8c:	4b37      	ldr	r3, [pc, #220]	; (800406c <vTaskSwitchContext+0xfc>)
 8003f8e:	681b      	ldr	r3, [r3, #0]
 8003f90:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 8003f92:	61fb      	str	r3, [r7, #28]
 8003f94:	f04f 33a5 	mov.w	r3, #2779096485	; 0xa5a5a5a5
 8003f98:	61bb      	str	r3, [r7, #24]
 8003f9a:	69fb      	ldr	r3, [r7, #28]
 8003f9c:	681a      	ldr	r2, [r3, #0]
 8003f9e:	69bb      	ldr	r3, [r7, #24]
 8003fa0:	429a      	cmp	r2, r3
 8003fa2:	d111      	bne.n	8003fc8 <vTaskSwitchContext+0x58>
 8003fa4:	69fb      	ldr	r3, [r7, #28]
 8003fa6:	3304      	adds	r3, #4
 8003fa8:	681a      	ldr	r2, [r3, #0]
 8003faa:	69bb      	ldr	r3, [r7, #24]
 8003fac:	429a      	cmp	r2, r3
 8003fae:	d10b      	bne.n	8003fc8 <vTaskSwitchContext+0x58>
 8003fb0:	69fb      	ldr	r3, [r7, #28]
 8003fb2:	3308      	adds	r3, #8
 8003fb4:	681a      	ldr	r2, [r3, #0]
 8003fb6:	69bb      	ldr	r3, [r7, #24]
 8003fb8:	429a      	cmp	r2, r3
 8003fba:	d105      	bne.n	8003fc8 <vTaskSwitchContext+0x58>
 8003fbc:	69fb      	ldr	r3, [r7, #28]
 8003fbe:	330c      	adds	r3, #12
 8003fc0:	681a      	ldr	r2, [r3, #0]
 8003fc2:	69bb      	ldr	r3, [r7, #24]
 8003fc4:	429a      	cmp	r2, r3
 8003fc6:	d008      	beq.n	8003fda <vTaskSwitchContext+0x6a>
 8003fc8:	4b28      	ldr	r3, [pc, #160]	; (800406c <vTaskSwitchContext+0xfc>)
 8003fca:	681a      	ldr	r2, [r3, #0]
 8003fcc:	4b27      	ldr	r3, [pc, #156]	; (800406c <vTaskSwitchContext+0xfc>)
 8003fce:	681b      	ldr	r3, [r3, #0]
 8003fd0:	3334      	adds	r3, #52	; 0x34
 8003fd2:	4619      	mov	r1, r3
 8003fd4:	4610      	mov	r0, r2
 8003fd6:	f7ff fe04 	bl	8003be2 <vApplicationStackOverflowHook>
 8003fda:	4b25      	ldr	r3, [pc, #148]	; (8004070 <vTaskSwitchContext+0x100>)
 8003fdc:	681b      	ldr	r3, [r3, #0]
 8003fde:	60fb      	str	r3, [r7, #12]
 8003fe0:	68fb      	ldr	r3, [r7, #12]
 8003fe2:	fab3 f383 	clz	r3, r3
 8003fe6:	72fb      	strb	r3, [r7, #11]
 8003fe8:	7afb      	ldrb	r3, [r7, #11]
 8003fea:	f1c3 031f 	rsb	r3, r3, #31
 8003fee:	617b      	str	r3, [r7, #20]
 8003ff0:	4920      	ldr	r1, [pc, #128]	; (8004074 <vTaskSwitchContext+0x104>)
 8003ff2:	697a      	ldr	r2, [r7, #20]
 8003ff4:	4613      	mov	r3, r2
 8003ff6:	009b      	lsls	r3, r3, #2
 8003ff8:	4413      	add	r3, r2
 8003ffa:	009b      	lsls	r3, r3, #2
 8003ffc:	440b      	add	r3, r1
 8003ffe:	681b      	ldr	r3, [r3, #0]
 8004000:	2b00      	cmp	r3, #0
 8004002:	d109      	bne.n	8004018 <vTaskSwitchContext+0xa8>
 8004004:	f04f 0350 	mov.w	r3, #80	; 0x50
 8004008:	f383 8811 	msr	BASEPRI, r3
 800400c:	f3bf 8f6f 	isb	sy
 8004010:	f3bf 8f4f 	dsb	sy
 8004014:	607b      	str	r3, [r7, #4]
 8004016:	e7fe      	b.n	8004016 <vTaskSwitchContext+0xa6>
 8004018:	697a      	ldr	r2, [r7, #20]
 800401a:	4613      	mov	r3, r2
 800401c:	009b      	lsls	r3, r3, #2
 800401e:	4413      	add	r3, r2
 8004020:	009b      	lsls	r3, r3, #2
 8004022:	4a14      	ldr	r2, [pc, #80]	; (8004074 <vTaskSwitchContext+0x104>)
 8004024:	4413      	add	r3, r2
 8004026:	613b      	str	r3, [r7, #16]
 8004028:	693b      	ldr	r3, [r7, #16]
 800402a:	685b      	ldr	r3, [r3, #4]
 800402c:	685a      	ldr	r2, [r3, #4]
 800402e:	693b      	ldr	r3, [r7, #16]
 8004030:	605a      	str	r2, [r3, #4]
 8004032:	693b      	ldr	r3, [r7, #16]
 8004034:	685a      	ldr	r2, [r3, #4]
 8004036:	693b      	ldr	r3, [r7, #16]
 8004038:	3308      	adds	r3, #8
 800403a:	429a      	cmp	r2, r3
 800403c:	d104      	bne.n	8004048 <vTaskSwitchContext+0xd8>
 800403e:	693b      	ldr	r3, [r7, #16]
 8004040:	685b      	ldr	r3, [r3, #4]
 8004042:	685a      	ldr	r2, [r3, #4]
 8004044:	693b      	ldr	r3, [r7, #16]
 8004046:	605a      	str	r2, [r3, #4]
 8004048:	693b      	ldr	r3, [r7, #16]
 800404a:	685b      	ldr	r3, [r3, #4]
 800404c:	68db      	ldr	r3, [r3, #12]
 800404e:	4a07      	ldr	r2, [pc, #28]	; (800406c <vTaskSwitchContext+0xfc>)
 8004050:	6013      	str	r3, [r2, #0]
 8004052:	4b06      	ldr	r3, [pc, #24]	; (800406c <vTaskSwitchContext+0xfc>)
 8004054:	681b      	ldr	r3, [r3, #0]
 8004056:	3350      	adds	r3, #80	; 0x50
 8004058:	4a07      	ldr	r2, [pc, #28]	; (8004078 <vTaskSwitchContext+0x108>)
 800405a:	6013      	str	r3, [r2, #0]
 800405c:	bf00      	nop
 800405e:	3720      	adds	r7, #32
 8004060:	46bd      	mov	sp, r7
 8004062:	bd80      	pop	{r7, pc}
 8004064:	200001a4 	.word	0x200001a4
 8004068:	20000198 	.word	0x20000198
 800406c:	20000104 	.word	0x20000104
 8004070:	20000190 	.word	0x20000190
 8004074:	20000108 	.word	0x20000108
 8004078:	20000000 	.word	0x20000000

0800407c <prvResetNextTaskUnblockTime>:
 800407c:	b480      	push	{r7}
 800407e:	af00      	add	r7, sp, #0
 8004080:	4b0a      	ldr	r3, [pc, #40]	; (80040ac <prvResetNextTaskUnblockTime+0x30>)
 8004082:	681b      	ldr	r3, [r3, #0]
 8004084:	681b      	ldr	r3, [r3, #0]
 8004086:	2b00      	cmp	r3, #0
 8004088:	d104      	bne.n	8004094 <prvResetNextTaskUnblockTime+0x18>
 800408a:	4b09      	ldr	r3, [pc, #36]	; (80040b0 <prvResetNextTaskUnblockTime+0x34>)
 800408c:	f04f 32ff 	mov.w	r2, #4294967295	; 0xffffffff
 8004090:	601a      	str	r2, [r3, #0]
 8004092:	e005      	b.n	80040a0 <prvResetNextTaskUnblockTime+0x24>
 8004094:	4b05      	ldr	r3, [pc, #20]	; (80040ac <prvResetNextTaskUnblockTime+0x30>)
 8004096:	681b      	ldr	r3, [r3, #0]
 8004098:	68db      	ldr	r3, [r3, #12]
 800409a:	681b      	ldr	r3, [r3, #0]
 800409c:	4a04      	ldr	r2, [pc, #16]	; (80040b0 <prvResetNextTaskUnblockTime+0x34>)
 800409e:	6013      	str	r3, [r2, #0]
 80040a0:	bf00      	nop
 80040a2:	46bd      	mov	sp, r7
 80040a4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80040a8:	4770      	bx	lr
 80040aa:	bf00      	nop
 80040ac:	2000016c 	.word	0x2000016c
 80040b0:	200001a0 	.word	0x200001a0
	...

080040c0 <SVC_Handler>:
 80040c0:	4b07      	ldr	r3, [pc, #28]	; (80040e0 <pxCurrentTCBConst2>)
 80040c2:	6819      	ldr	r1, [r3, #0]
 80040c4:	6808      	ldr	r0, [r1, #0]
 80040c6:	e8b0 4ff0 	ldmia.w	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80040ca:	f380 8809 	msr	PSP, r0
 80040ce:	f3bf 8f6f 	isb	sy
 80040d2:	f04f 0000 	mov.w	r0, #0
 80040d6:	f380 8811 	msr	BASEPRI, r0
 80040da:	4770      	bx	lr
 80040dc:	f3af 8000 	nop.w

080040e0 <pxCurrentTCBConst2>:
 80040e0:	20000104 	.word	0x20000104
 80040e4:	bf00      	nop
 80040e6:	bf00      	nop

080040e8 <vPortEnterCritical>:
 80040e8:	b480      	push	{r7}
 80040ea:	b083      	sub	sp, #12
 80040ec:	af00      	add	r7, sp, #0
 80040ee:	f04f 0350 	mov.w	r3, #80	; 0x50
 80040f2:	f383 8811 	msr	BASEPRI, r3
 80040f6:	f3bf 8f6f 	isb	sy
 80040fa:	f3bf 8f4f 	dsb	sy
 80040fe:	607b      	str	r3, [r7, #4]
 8004100:	4b0e      	ldr	r3, [pc, #56]	; (800413c <vPortEnterCritical+0x54>)
 8004102:	681b      	ldr	r3, [r3, #0]
 8004104:	3301      	adds	r3, #1
 8004106:	4a0d      	ldr	r2, [pc, #52]	; (800413c <vPortEnterCritical+0x54>)
 8004108:	6013      	str	r3, [r2, #0]
 800410a:	4b0c      	ldr	r3, [pc, #48]	; (800413c <vPortEnterCritical+0x54>)
 800410c:	681b      	ldr	r3, [r3, #0]
 800410e:	2b01      	cmp	r3, #1
 8004110:	d10e      	bne.n	8004130 <vPortEnterCritical+0x48>
 8004112:	4b0b      	ldr	r3, [pc, #44]	; (8004140 <vPortEnterCritical+0x58>)
 8004114:	681b      	ldr	r3, [r3, #0]
 8004116:	b2db      	uxtb	r3, r3
 8004118:	2b00      	cmp	r3, #0
 800411a:	d009      	beq.n	8004130 <vPortEnterCritical+0x48>
 800411c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8004120:	f383 8811 	msr	BASEPRI, r3
 8004124:	f3bf 8f6f 	isb	sy
 8004128:	f3bf 8f4f 	dsb	sy
 800412c:	603b      	str	r3, [r7, #0]
 800412e:	e7fe      	b.n	800412e <vPortEnterCritical+0x46>
 8004130:	bf00      	nop
 8004132:	370c      	adds	r7, #12
 8004134:	46bd      	mov	sp, r7
 8004136:	f85d 7b04 	ldr.w	r7, [sp], #4
 800413a:	4770      	bx	lr
 800413c:	200000e0 	.word	0x200000e0
 8004140:	e000ed04 	.word	0xe000ed04

08004144 <vPortExitCritical>:
 8004144:	b480      	push	{r7}
 8004146:	b083      	sub	sp, #12
 8004148:	af00      	add	r7, sp, #0
 800414a:	4b11      	ldr	r3, [pc, #68]	; (8004190 <vPortExitCritical+0x4c>)
 800414c:	681b      	ldr	r3, [r3, #0]
 800414e:	2b00      	cmp	r3, #0
 8004150:	d109      	bne.n	8004166 <vPortExitCritical+0x22>
 8004152:	f04f 0350 	mov.w	r3, #80	; 0x50
 8004156:	f383 8811 	msr	BASEPRI, r3
 800415a:	f3bf 8f6f 	isb	sy
 800415e:	f3bf 8f4f 	dsb	sy
 8004162:	607b      	str	r3, [r7, #4]
 8004164:	e7fe      	b.n	8004164 <vPortExitCritical+0x20>
 8004166:	4b0a      	ldr	r3, [pc, #40]	; (8004190 <vPortExitCritical+0x4c>)
 8004168:	681b      	ldr	r3, [r3, #0]
 800416a:	3b01      	subs	r3, #1
 800416c:	4a08      	ldr	r2, [pc, #32]	; (8004190 <vPortExitCritical+0x4c>)
 800416e:	6013      	str	r3, [r2, #0]
 8004170:	4b07      	ldr	r3, [pc, #28]	; (8004190 <vPortExitCritical+0x4c>)
 8004172:	681b      	ldr	r3, [r3, #0]
 8004174:	2b00      	cmp	r3, #0
 8004176:	d104      	bne.n	8004182 <vPortExitCritical+0x3e>
 8004178:	2300      	movs	r3, #0
 800417a:	603b      	str	r3, [r7, #0]
 800417c:	683b      	ldr	r3, [r7, #0]
 800417e:	f383 8811 	msr	BASEPRI, r3
 8004182:	bf00      	nop
 8004184:	370c      	adds	r7, #12
 8004186:	46bd      	mov	sp, r7
 8004188:	f85d 7b04 	ldr.w	r7, [sp], #4
 800418c:	4770      	bx	lr
 800418e:	bf00      	nop
 8004190:	200000e0 	.word	0x200000e0
	...

080041a0 <PendSV_Handler>:
 80041a0:	f3ef 8009 	mrs	r0, PSP
 80041a4:	f3bf 8f6f 	isb	sy
 80041a8:	4b15      	ldr	r3, [pc, #84]	; (8004200 <pxCurrentTCBConst>)
 80041aa:	681a      	ldr	r2, [r3, #0]
 80041ac:	f01e 0f10 	tst.w	lr, #16
 80041b0:	bf08      	it	eq
 80041b2:	ed20 8a10 	vstmdbeq	r0!, {s16-s31}
 80041b6:	e920 4ff0 	stmdb	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80041ba:	6010      	str	r0, [r2, #0]
 80041bc:	e92d 0009 	stmdb	sp!, {r0, r3}
 80041c0:	f04f 0050 	mov.w	r0, #80	; 0x50
 80041c4:	f380 8811 	msr	BASEPRI, r0
 80041c8:	f3bf 8f4f 	dsb	sy
 80041cc:	f3bf 8f6f 	isb	sy
 80041d0:	f7ff fece 	bl	8003f70 <vTaskSwitchContext>
 80041d4:	f04f 0000 	mov.w	r0, #0
 80041d8:	f380 8811 	msr	BASEPRI, r0
 80041dc:	bc09      	pop	{r0, r3}
 80041de:	6819      	ldr	r1, [r3, #0]
 80041e0:	6808      	ldr	r0, [r1, #0]
 80041e2:	e8b0 4ff0 	ldmia.w	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80041e6:	f01e 0f10 	tst.w	lr, #16
 80041ea:	bf08      	it	eq
 80041ec:	ecb0 8a10 	vldmiaeq	r0!, {s16-s31}
 80041f0:	f380 8809 	msr	PSP, r0
 80041f4:	f3bf 8f6f 	isb	sy
 80041f8:	4770      	bx	lr
 80041fa:	bf00      	nop
 80041fc:	f3af 8000 	nop.w

08004200 <pxCurrentTCBConst>:
 8004200:	20000104 	.word	0x20000104
 8004204:	bf00      	nop
 8004206:	bf00      	nop

08004208 <SysTick_Handler>:
 8004208:	b580      	push	{r7, lr}
 800420a:	b082      	sub	sp, #8
 800420c:	af00      	add	r7, sp, #0
 800420e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8004212:	f383 8811 	msr	BASEPRI, r3
 8004216:	f3bf 8f6f 	isb	sy
 800421a:	f3bf 8f4f 	dsb	sy
 800421e:	607b      	str	r3, [r7, #4]
 8004220:	f7ff fde6 	bl	8003df0 <xTaskIncrementTick>
 8004224:	4603      	mov	r3, r0
 8004226:	2b00      	cmp	r3, #0
 8004228:	d003      	beq.n	8004232 <SysTick_Handler+0x2a>
 800422a:	4b06      	ldr	r3, [pc, #24]	; (8004244 <SysTick_Handler+0x3c>)
 800422c:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8004230:	601a      	str	r2, [r3, #0]
 8004232:	2300      	movs	r3, #0
 8004234:	603b      	str	r3, [r7, #0]
 8004236:	683b      	ldr	r3, [r7, #0]
 8004238:	f383 8811 	msr	BASEPRI, r3
 800423c:	bf00      	nop
 800423e:	3708      	adds	r7, #8
 8004240:	46bd      	mov	sp, r7
 8004242:	bd80      	pop	{r7, pc}
 8004244:	e000ed04 	.word	0xe000ed04

08004248 <pvPortMalloc>:
 8004248:	b580      	push	{r7, lr}
 800424a:	b08a      	sub	sp, #40	; 0x28
 800424c:	af00      	add	r7, sp, #0
 800424e:	6078      	str	r0, [r7, #4]
 8004250:	2300      	movs	r3, #0
 8004252:	61fb      	str	r3, [r7, #28]
 8004254:	f7ff fd22 	bl	8003c9c <vTaskSuspendAll>
 8004258:	4b5c      	ldr	r3, [pc, #368]	; (80043cc <pvPortMalloc+0x184>)
 800425a:	681b      	ldr	r3, [r3, #0]
 800425c:	2b00      	cmp	r3, #0
 800425e:	d101      	bne.n	8004264 <pvPortMalloc+0x1c>
 8004260:	f000 f920 	bl	80044a4 <prvHeapInit>
 8004264:	4b5a      	ldr	r3, [pc, #360]	; (80043d0 <pvPortMalloc+0x188>)
 8004266:	681a      	ldr	r2, [r3, #0]
 8004268:	687b      	ldr	r3, [r7, #4]
 800426a:	4013      	ands	r3, r2
 800426c:	2b00      	cmp	r3, #0
 800426e:	f040 8091 	bne.w	8004394 <pvPortMalloc+0x14c>
 8004272:	687b      	ldr	r3, [r7, #4]
 8004274:	2b00      	cmp	r3, #0
 8004276:	d01c      	beq.n	80042b2 <pvPortMalloc+0x6a>
 8004278:	2208      	movs	r2, #8
 800427a:	687b      	ldr	r3, [r7, #4]
 800427c:	4413      	add	r3, r2
 800427e:	607b      	str	r3, [r7, #4]
 8004280:	687b      	ldr	r3, [r7, #4]
 8004282:	f003 0307 	and.w	r3, r3, #7
 8004286:	2b00      	cmp	r3, #0
 8004288:	d013      	beq.n	80042b2 <pvPortMalloc+0x6a>
 800428a:	687b      	ldr	r3, [r7, #4]
 800428c:	f023 0307 	bic.w	r3, r3, #7
 8004290:	3308      	adds	r3, #8
 8004292:	607b      	str	r3, [r7, #4]
 8004294:	687b      	ldr	r3, [r7, #4]
 8004296:	f003 0307 	and.w	r3, r3, #7
 800429a:	2b00      	cmp	r3, #0
 800429c:	d009      	beq.n	80042b2 <pvPortMalloc+0x6a>
 800429e:	f04f 0350 	mov.w	r3, #80	; 0x50
 80042a2:	f383 8811 	msr	BASEPRI, r3
 80042a6:	f3bf 8f6f 	isb	sy
 80042aa:	f3bf 8f4f 	dsb	sy
 80042ae:	617b      	str	r3, [r7, #20]
 80042b0:	e7fe      	b.n	80042b0 <pvPortMalloc+0x68>
 80042b2:	687b      	ldr	r3, [r7, #4]
 80042b4:	2b00      	cmp	r3, #0
 80042b6:	d06d      	beq.n	8004394 <pvPortMalloc+0x14c>
 80042b8:	4b46      	ldr	r3, [pc, #280]	; (80043d4 <pvPortMalloc+0x18c>)
 80042ba:	681b      	ldr	r3, [r3, #0]
 80042bc:	687a      	ldr	r2, [r7, #4]
 80042be:	429a      	cmp	r2, r3
 80042c0:	d868      	bhi.n	8004394 <pvPortMalloc+0x14c>
 80042c2:	4b45      	ldr	r3, [pc, #276]	; (80043d8 <pvPortMalloc+0x190>)
 80042c4:	623b      	str	r3, [r7, #32]
 80042c6:	4b44      	ldr	r3, [pc, #272]	; (80043d8 <pvPortMalloc+0x190>)
 80042c8:	681b      	ldr	r3, [r3, #0]
 80042ca:	627b      	str	r3, [r7, #36]	; 0x24
 80042cc:	e004      	b.n	80042d8 <pvPortMalloc+0x90>
 80042ce:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80042d0:	623b      	str	r3, [r7, #32]
 80042d2:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80042d4:	681b      	ldr	r3, [r3, #0]
 80042d6:	627b      	str	r3, [r7, #36]	; 0x24
 80042d8:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80042da:	685a      	ldr	r2, [r3, #4]
 80042dc:	687b      	ldr	r3, [r7, #4]
 80042de:	429a      	cmp	r2, r3
 80042e0:	d203      	bcs.n	80042ea <pvPortMalloc+0xa2>
 80042e2:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80042e4:	681b      	ldr	r3, [r3, #0]
 80042e6:	2b00      	cmp	r3, #0
 80042e8:	d1f1      	bne.n	80042ce <pvPortMalloc+0x86>
 80042ea:	4b38      	ldr	r3, [pc, #224]	; (80043cc <pvPortMalloc+0x184>)
 80042ec:	681b      	ldr	r3, [r3, #0]
 80042ee:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 80042f0:	429a      	cmp	r2, r3
 80042f2:	d04f      	beq.n	8004394 <pvPortMalloc+0x14c>
 80042f4:	6a3b      	ldr	r3, [r7, #32]
 80042f6:	681b      	ldr	r3, [r3, #0]
 80042f8:	2208      	movs	r2, #8
 80042fa:	4413      	add	r3, r2
 80042fc:	61fb      	str	r3, [r7, #28]
 80042fe:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8004300:	681a      	ldr	r2, [r3, #0]
 8004302:	6a3b      	ldr	r3, [r7, #32]
 8004304:	601a      	str	r2, [r3, #0]
 8004306:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8004308:	685a      	ldr	r2, [r3, #4]
 800430a:	687b      	ldr	r3, [r7, #4]
 800430c:	1ad2      	subs	r2, r2, r3
 800430e:	2308      	movs	r3, #8
 8004310:	005b      	lsls	r3, r3, #1
 8004312:	429a      	cmp	r2, r3
 8004314:	d91e      	bls.n	8004354 <pvPortMalloc+0x10c>
 8004316:	6a7a      	ldr	r2, [r7, #36]	; 0x24
 8004318:	687b      	ldr	r3, [r7, #4]
 800431a:	4413      	add	r3, r2
 800431c:	61bb      	str	r3, [r7, #24]
 800431e:	69bb      	ldr	r3, [r7, #24]
 8004320:	f003 0307 	and.w	r3, r3, #7
 8004324:	2b00      	cmp	r3, #0
 8004326:	d009      	beq.n	800433c <pvPortMalloc+0xf4>
 8004328:	f04f 0350 	mov.w	r3, #80	; 0x50
 800432c:	f383 8811 	msr	BASEPRI, r3
 8004330:	f3bf 8f6f 	isb	sy
 8004334:	f3bf 8f4f 	dsb	sy
 8004338:	613b      	str	r3, [r7, #16]
 800433a:	e7fe      	b.n	800433a <pvPortMalloc+0xf2>
 800433c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800433e:	685a      	ldr	r2, [r3, #4]
 8004340:	687b      	ldr	r3, [r7, #4]
 8004342:	1ad2      	subs	r2, r2, r3
 8004344:	69bb      	ldr	r3, [r7, #24]
 8004346:	605a      	str	r2, [r3, #4]
 8004348:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800434a:	687a      	ldr	r2, [r7, #4]
 800434c:	605a      	str	r2, [r3, #4]
 800434e:	69b8      	ldr	r0, [r7, #24]
 8004350:	f000 f90a 	bl	8004568 <prvInsertBlockIntoFreeList>
 8004354:	4b1f      	ldr	r3, [pc, #124]	; (80043d4 <pvPortMalloc+0x18c>)
 8004356:	681a      	ldr	r2, [r3, #0]
 8004358:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800435a:	685b      	ldr	r3, [r3, #4]
 800435c:	1ad3      	subs	r3, r2, r3
 800435e:	4a1d      	ldr	r2, [pc, #116]	; (80043d4 <pvPortMalloc+0x18c>)
 8004360:	6013      	str	r3, [r2, #0]
 8004362:	4b1c      	ldr	r3, [pc, #112]	; (80043d4 <pvPortMalloc+0x18c>)
 8004364:	681a      	ldr	r2, [r3, #0]
 8004366:	4b1d      	ldr	r3, [pc, #116]	; (80043dc <pvPortMalloc+0x194>)
 8004368:	681b      	ldr	r3, [r3, #0]
 800436a:	429a      	cmp	r2, r3
 800436c:	d203      	bcs.n	8004376 <pvPortMalloc+0x12e>
 800436e:	4b19      	ldr	r3, [pc, #100]	; (80043d4 <pvPortMalloc+0x18c>)
 8004370:	681b      	ldr	r3, [r3, #0]
 8004372:	4a1a      	ldr	r2, [pc, #104]	; (80043dc <pvPortMalloc+0x194>)
 8004374:	6013      	str	r3, [r2, #0]
 8004376:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8004378:	685a      	ldr	r2, [r3, #4]
 800437a:	4b15      	ldr	r3, [pc, #84]	; (80043d0 <pvPortMalloc+0x188>)
 800437c:	681b      	ldr	r3, [r3, #0]
 800437e:	431a      	orrs	r2, r3
 8004380:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8004382:	605a      	str	r2, [r3, #4]
 8004384:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8004386:	2200      	movs	r2, #0
 8004388:	601a      	str	r2, [r3, #0]
 800438a:	4b15      	ldr	r3, [pc, #84]	; (80043e0 <pvPortMalloc+0x198>)
 800438c:	681b      	ldr	r3, [r3, #0]
 800438e:	3301      	adds	r3, #1
 8004390:	4a13      	ldr	r2, [pc, #76]	; (80043e0 <pvPortMalloc+0x198>)
 8004392:	6013      	str	r3, [r2, #0]
 8004394:	f7ff fc90 	bl	8003cb8 <xTaskResumeAll>
 8004398:	69fb      	ldr	r3, [r7, #28]
 800439a:	2b00      	cmp	r3, #0
 800439c:	d101      	bne.n	80043a2 <pvPortMalloc+0x15a>
 800439e:	f7ff fc12 	bl	8003bc6 <vApplicationMallocFailedHook>
 80043a2:	69fb      	ldr	r3, [r7, #28]
 80043a4:	f003 0307 	and.w	r3, r3, #7
 80043a8:	2b00      	cmp	r3, #0
 80043aa:	d009      	beq.n	80043c0 <pvPortMalloc+0x178>
 80043ac:	f04f 0350 	mov.w	r3, #80	; 0x50
 80043b0:	f383 8811 	msr	BASEPRI, r3
 80043b4:	f3bf 8f6f 	isb	sy
 80043b8:	f3bf 8f4f 	dsb	sy
 80043bc:	60fb      	str	r3, [r7, #12]
 80043be:	e7fe      	b.n	80043be <pvPortMalloc+0x176>
 80043c0:	69fb      	ldr	r3, [r7, #28]
 80043c2:	4618      	mov	r0, r3
 80043c4:	3728      	adds	r7, #40	; 0x28
 80043c6:	46bd      	mov	sp, r7
 80043c8:	bd80      	pop	{r7, pc}
 80043ca:	bf00      	nop
 80043cc:	20012db0 	.word	0x20012db0
 80043d0:	20012dc4 	.word	0x20012dc4
 80043d4:	20012db4 	.word	0x20012db4
 80043d8:	20012da8 	.word	0x20012da8
 80043dc:	20012db8 	.word	0x20012db8
 80043e0:	20012dbc 	.word	0x20012dbc

080043e4 <vPortFree>:
 80043e4:	b580      	push	{r7, lr}
 80043e6:	b086      	sub	sp, #24
 80043e8:	af00      	add	r7, sp, #0
 80043ea:	6078      	str	r0, [r7, #4]
 80043ec:	687b      	ldr	r3, [r7, #4]
 80043ee:	617b      	str	r3, [r7, #20]
 80043f0:	687b      	ldr	r3, [r7, #4]
 80043f2:	2b00      	cmp	r3, #0
 80043f4:	d04b      	beq.n	800448e <vPortFree+0xaa>
 80043f6:	2308      	movs	r3, #8
 80043f8:	425b      	negs	r3, r3
 80043fa:	697a      	ldr	r2, [r7, #20]
 80043fc:	4413      	add	r3, r2
 80043fe:	617b      	str	r3, [r7, #20]
 8004400:	697b      	ldr	r3, [r7, #20]
 8004402:	613b      	str	r3, [r7, #16]
 8004404:	693b      	ldr	r3, [r7, #16]
 8004406:	685a      	ldr	r2, [r3, #4]
 8004408:	4b23      	ldr	r3, [pc, #140]	; (8004498 <vPortFree+0xb4>)
 800440a:	681b      	ldr	r3, [r3, #0]
 800440c:	4013      	ands	r3, r2
 800440e:	2b00      	cmp	r3, #0
 8004410:	d109      	bne.n	8004426 <vPortFree+0x42>
 8004412:	f04f 0350 	mov.w	r3, #80	; 0x50
 8004416:	f383 8811 	msr	BASEPRI, r3
 800441a:	f3bf 8f6f 	isb	sy
 800441e:	f3bf 8f4f 	dsb	sy
 8004422:	60fb      	str	r3, [r7, #12]
 8004424:	e7fe      	b.n	8004424 <vPortFree+0x40>
 8004426:	693b      	ldr	r3, [r7, #16]
 8004428:	681b      	ldr	r3, [r3, #0]
 800442a:	2b00      	cmp	r3, #0
 800442c:	d009      	beq.n	8004442 <vPortFree+0x5e>
 800442e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8004432:	f383 8811 	msr	BASEPRI, r3
 8004436:	f3bf 8f6f 	isb	sy
 800443a:	f3bf 8f4f 	dsb	sy
 800443e:	60bb      	str	r3, [r7, #8]
 8004440:	e7fe      	b.n	8004440 <vPortFree+0x5c>
 8004442:	693b      	ldr	r3, [r7, #16]
 8004444:	685a      	ldr	r2, [r3, #4]
 8004446:	4b14      	ldr	r3, [pc, #80]	; (8004498 <vPortFree+0xb4>)
 8004448:	681b      	ldr	r3, [r3, #0]
 800444a:	4013      	ands	r3, r2
 800444c:	2b00      	cmp	r3, #0
 800444e:	d01e      	beq.n	800448e <vPortFree+0xaa>
 8004450:	693b      	ldr	r3, [r7, #16]
 8004452:	681b      	ldr	r3, [r3, #0]
 8004454:	2b00      	cmp	r3, #0
 8004456:	d11a      	bne.n	800448e <vPortFree+0xaa>
 8004458:	693b      	ldr	r3, [r7, #16]
 800445a:	685a      	ldr	r2, [r3, #4]
 800445c:	4b0e      	ldr	r3, [pc, #56]	; (8004498 <vPortFree+0xb4>)
 800445e:	681b      	ldr	r3, [r3, #0]
 8004460:	43db      	mvns	r3, r3
 8004462:	401a      	ands	r2, r3
 8004464:	693b      	ldr	r3, [r7, #16]
 8004466:	605a      	str	r2, [r3, #4]
 8004468:	f7ff fc18 	bl	8003c9c <vTaskSuspendAll>
 800446c:	693b      	ldr	r3, [r7, #16]
 800446e:	685a      	ldr	r2, [r3, #4]
 8004470:	4b0a      	ldr	r3, [pc, #40]	; (800449c <vPortFree+0xb8>)
 8004472:	681b      	ldr	r3, [r3, #0]
 8004474:	4413      	add	r3, r2
 8004476:	4a09      	ldr	r2, [pc, #36]	; (800449c <vPortFree+0xb8>)
 8004478:	6013      	str	r3, [r2, #0]
 800447a:	6938      	ldr	r0, [r7, #16]
 800447c:	f000 f874 	bl	8004568 <prvInsertBlockIntoFreeList>
 8004480:	4b07      	ldr	r3, [pc, #28]	; (80044a0 <vPortFree+0xbc>)
 8004482:	681b      	ldr	r3, [r3, #0]
 8004484:	3301      	adds	r3, #1
 8004486:	4a06      	ldr	r2, [pc, #24]	; (80044a0 <vPortFree+0xbc>)
 8004488:	6013      	str	r3, [r2, #0]
 800448a:	f7ff fc15 	bl	8003cb8 <xTaskResumeAll>
 800448e:	bf00      	nop
 8004490:	3718      	adds	r7, #24
 8004492:	46bd      	mov	sp, r7
 8004494:	bd80      	pop	{r7, pc}
 8004496:	bf00      	nop
 8004498:	20012dc4 	.word	0x20012dc4
 800449c:	20012db4 	.word	0x20012db4
 80044a0:	20012dc0 	.word	0x20012dc0

080044a4 <prvHeapInit>:
 80044a4:	b480      	push	{r7}
 80044a6:	b085      	sub	sp, #20
 80044a8:	af00      	add	r7, sp, #0
 80044aa:	f44f 3396 	mov.w	r3, #76800	; 0x12c00
 80044ae:	60bb      	str	r3, [r7, #8]
 80044b0:	4b27      	ldr	r3, [pc, #156]	; (8004550 <prvHeapInit+0xac>)
 80044b2:	60fb      	str	r3, [r7, #12]
 80044b4:	68fb      	ldr	r3, [r7, #12]
 80044b6:	f003 0307 	and.w	r3, r3, #7
 80044ba:	2b00      	cmp	r3, #0
 80044bc:	d00c      	beq.n	80044d8 <prvHeapInit+0x34>
 80044be:	68fb      	ldr	r3, [r7, #12]
 80044c0:	3307      	adds	r3, #7
 80044c2:	60fb      	str	r3, [r7, #12]
 80044c4:	68fb      	ldr	r3, [r7, #12]
 80044c6:	f023 0307 	bic.w	r3, r3, #7
 80044ca:	60fb      	str	r3, [r7, #12]
 80044cc:	68ba      	ldr	r2, [r7, #8]
 80044ce:	68fb      	ldr	r3, [r7, #12]
 80044d0:	1ad3      	subs	r3, r2, r3
 80044d2:	4a1f      	ldr	r2, [pc, #124]	; (8004550 <prvHeapInit+0xac>)
 80044d4:	4413      	add	r3, r2
 80044d6:	60bb      	str	r3, [r7, #8]
 80044d8:	68fb      	ldr	r3, [r7, #12]
 80044da:	607b      	str	r3, [r7, #4]
 80044dc:	4a1d      	ldr	r2, [pc, #116]	; (8004554 <prvHeapInit+0xb0>)
 80044de:	687b      	ldr	r3, [r7, #4]
 80044e0:	6013      	str	r3, [r2, #0]
 80044e2:	4b1c      	ldr	r3, [pc, #112]	; (8004554 <prvHeapInit+0xb0>)
 80044e4:	2200      	movs	r2, #0
 80044e6:	605a      	str	r2, [r3, #4]
 80044e8:	687a      	ldr	r2, [r7, #4]
 80044ea:	68bb      	ldr	r3, [r7, #8]
 80044ec:	4413      	add	r3, r2
 80044ee:	60fb      	str	r3, [r7, #12]
 80044f0:	2208      	movs	r2, #8
 80044f2:	68fb      	ldr	r3, [r7, #12]
 80044f4:	1a9b      	subs	r3, r3, r2
 80044f6:	60fb      	str	r3, [r7, #12]
 80044f8:	68fb      	ldr	r3, [r7, #12]
 80044fa:	f023 0307 	bic.w	r3, r3, #7
 80044fe:	60fb      	str	r3, [r7, #12]
 8004500:	68fb      	ldr	r3, [r7, #12]
 8004502:	4a15      	ldr	r2, [pc, #84]	; (8004558 <prvHeapInit+0xb4>)
 8004504:	6013      	str	r3, [r2, #0]
 8004506:	4b14      	ldr	r3, [pc, #80]	; (8004558 <prvHeapInit+0xb4>)
 8004508:	681b      	ldr	r3, [r3, #0]
 800450a:	2200      	movs	r2, #0
 800450c:	605a      	str	r2, [r3, #4]
 800450e:	4b12      	ldr	r3, [pc, #72]	; (8004558 <prvHeapInit+0xb4>)
 8004510:	681b      	ldr	r3, [r3, #0]
 8004512:	2200      	movs	r2, #0
 8004514:	601a      	str	r2, [r3, #0]
 8004516:	687b      	ldr	r3, [r7, #4]
 8004518:	603b      	str	r3, [r7, #0]
 800451a:	683b      	ldr	r3, [r7, #0]
 800451c:	68fa      	ldr	r2, [r7, #12]
 800451e:	1ad2      	subs	r2, r2, r3
 8004520:	683b      	ldr	r3, [r7, #0]
 8004522:	605a      	str	r2, [r3, #4]
 8004524:	4b0c      	ldr	r3, [pc, #48]	; (8004558 <prvHeapInit+0xb4>)
 8004526:	681a      	ldr	r2, [r3, #0]
 8004528:	683b      	ldr	r3, [r7, #0]
 800452a:	601a      	str	r2, [r3, #0]
 800452c:	683b      	ldr	r3, [r7, #0]
 800452e:	685b      	ldr	r3, [r3, #4]
 8004530:	4a0a      	ldr	r2, [pc, #40]	; (800455c <prvHeapInit+0xb8>)
 8004532:	6013      	str	r3, [r2, #0]
 8004534:	683b      	ldr	r3, [r7, #0]
 8004536:	685b      	ldr	r3, [r3, #4]
 8004538:	4a09      	ldr	r2, [pc, #36]	; (8004560 <prvHeapInit+0xbc>)
 800453a:	6013      	str	r3, [r2, #0]
 800453c:	4b09      	ldr	r3, [pc, #36]	; (8004564 <prvHeapInit+0xc0>)
 800453e:	f04f 4200 	mov.w	r2, #2147483648	; 0x80000000
 8004542:	601a      	str	r2, [r3, #0]
 8004544:	bf00      	nop
 8004546:	3714      	adds	r7, #20
 8004548:	46bd      	mov	sp, r7
 800454a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800454e:	4770      	bx	lr
 8004550:	200001a8 	.word	0x200001a8
 8004554:	20012da8 	.word	0x20012da8
 8004558:	20012db0 	.word	0x20012db0
 800455c:	20012db8 	.word	0x20012db8
 8004560:	20012db4 	.word	0x20012db4
 8004564:	20012dc4 	.word	0x20012dc4

08004568 <prvInsertBlockIntoFreeList>:
 8004568:	b480      	push	{r7}
 800456a:	b085      	sub	sp, #20
 800456c:	af00      	add	r7, sp, #0
 800456e:	6078      	str	r0, [r7, #4]
 8004570:	4b28      	ldr	r3, [pc, #160]	; (8004614 <prvInsertBlockIntoFreeList+0xac>)
 8004572:	60fb      	str	r3, [r7, #12]
 8004574:	e002      	b.n	800457c <prvInsertBlockIntoFreeList+0x14>
 8004576:	68fb      	ldr	r3, [r7, #12]
 8004578:	681b      	ldr	r3, [r3, #0]
 800457a:	60fb      	str	r3, [r7, #12]
 800457c:	68fb      	ldr	r3, [r7, #12]
 800457e:	681a      	ldr	r2, [r3, #0]
 8004580:	687b      	ldr	r3, [r7, #4]
 8004582:	429a      	cmp	r2, r3
 8004584:	d3f7      	bcc.n	8004576 <prvInsertBlockIntoFreeList+0xe>
 8004586:	68fb      	ldr	r3, [r7, #12]
 8004588:	60bb      	str	r3, [r7, #8]
 800458a:	68fb      	ldr	r3, [r7, #12]
 800458c:	685b      	ldr	r3, [r3, #4]
 800458e:	68ba      	ldr	r2, [r7, #8]
 8004590:	441a      	add	r2, r3
 8004592:	687b      	ldr	r3, [r7, #4]
 8004594:	429a      	cmp	r2, r3
 8004596:	d108      	bne.n	80045aa <prvInsertBlockIntoFreeList+0x42>
 8004598:	68fb      	ldr	r3, [r7, #12]
 800459a:	685a      	ldr	r2, [r3, #4]
 800459c:	687b      	ldr	r3, [r7, #4]
 800459e:	685b      	ldr	r3, [r3, #4]
 80045a0:	441a      	add	r2, r3
 80045a2:	68fb      	ldr	r3, [r7, #12]
 80045a4:	605a      	str	r2, [r3, #4]
 80045a6:	68fb      	ldr	r3, [r7, #12]
 80045a8:	607b      	str	r3, [r7, #4]
 80045aa:	687b      	ldr	r3, [r7, #4]
 80045ac:	60bb      	str	r3, [r7, #8]
 80045ae:	687b      	ldr	r3, [r7, #4]
 80045b0:	685b      	ldr	r3, [r3, #4]
 80045b2:	68ba      	ldr	r2, [r7, #8]
 80045b4:	441a      	add	r2, r3
 80045b6:	68fb      	ldr	r3, [r7, #12]
 80045b8:	681b      	ldr	r3, [r3, #0]
 80045ba:	429a      	cmp	r2, r3
 80045bc:	d118      	bne.n	80045f0 <prvInsertBlockIntoFreeList+0x88>
 80045be:	68fb      	ldr	r3, [r7, #12]
 80045c0:	681a      	ldr	r2, [r3, #0]
 80045c2:	4b15      	ldr	r3, [pc, #84]	; (8004618 <prvInsertBlockIntoFreeList+0xb0>)
 80045c4:	681b      	ldr	r3, [r3, #0]
 80045c6:	429a      	cmp	r2, r3
 80045c8:	d00d      	beq.n	80045e6 <prvInsertBlockIntoFreeList+0x7e>
 80045ca:	687b      	ldr	r3, [r7, #4]
 80045cc:	685a      	ldr	r2, [r3, #4]
 80045ce:	68fb      	ldr	r3, [r7, #12]
 80045d0:	681b      	ldr	r3, [r3, #0]
 80045d2:	685b      	ldr	r3, [r3, #4]
 80045d4:	441a      	add	r2, r3
 80045d6:	687b      	ldr	r3, [r7, #4]
 80045d8:	605a      	str	r2, [r3, #4]
 80045da:	68fb      	ldr	r3, [r7, #12]
 80045dc:	681b      	ldr	r3, [r3, #0]
 80045de:	681a      	ldr	r2, [r3, #0]
 80045e0:	687b      	ldr	r3, [r7, #4]
 80045e2:	601a      	str	r2, [r3, #0]
 80045e4:	e008      	b.n	80045f8 <prvInsertBlockIntoFreeList+0x90>
 80045e6:	4b0c      	ldr	r3, [pc, #48]	; (8004618 <prvInsertBlockIntoFreeList+0xb0>)
 80045e8:	681a      	ldr	r2, [r3, #0]
 80045ea:	687b      	ldr	r3, [r7, #4]
 80045ec:	601a      	str	r2, [r3, #0]
 80045ee:	e003      	b.n	80045f8 <prvInsertBlockIntoFreeList+0x90>
 80045f0:	68fb      	ldr	r3, [r7, #12]
 80045f2:	681a      	ldr	r2, [r3, #0]
 80045f4:	687b      	ldr	r3, [r7, #4]
 80045f6:	601a      	str	r2, [r3, #0]
 80045f8:	68fa      	ldr	r2, [r7, #12]
 80045fa:	687b      	ldr	r3, [r7, #4]
 80045fc:	429a      	cmp	r2, r3
 80045fe:	d002      	beq.n	8004606 <prvInsertBlockIntoFreeList+0x9e>
 8004600:	68fb      	ldr	r3, [r7, #12]
 8004602:	687a      	ldr	r2, [r7, #4]
 8004604:	601a      	str	r2, [r3, #0]
 8004606:	bf00      	nop
 8004608:	3714      	adds	r7, #20
 800460a:	46bd      	mov	sp, r7
 800460c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004610:	4770      	bx	lr
 8004612:	bf00      	nop
 8004614:	20012da8 	.word	0x20012da8
 8004618:	20012db0 	.word	0x20012db0

0800461c <SystemInit>:
 800461c:	b480      	push	{r7}
 800461e:	af00      	add	r7, sp, #0
 8004620:	4a16      	ldr	r2, [pc, #88]	; (800467c <SystemInit+0x60>)
 8004622:	4b16      	ldr	r3, [pc, #88]	; (800467c <SystemInit+0x60>)
 8004624:	f8d3 3088 	ldr.w	r3, [r3, #136]	; 0x88
 8004628:	f443 0370 	orr.w	r3, r3, #15728640	; 0xf00000
 800462c:	f8c2 3088 	str.w	r3, [r2, #136]	; 0x88
 8004630:	4a13      	ldr	r2, [pc, #76]	; (8004680 <SystemInit+0x64>)
 8004632:	4b13      	ldr	r3, [pc, #76]	; (8004680 <SystemInit+0x64>)
 8004634:	681b      	ldr	r3, [r3, #0]
 8004636:	f043 0301 	orr.w	r3, r3, #1
 800463a:	6013      	str	r3, [r2, #0]
 800463c:	4b10      	ldr	r3, [pc, #64]	; (8004680 <SystemInit+0x64>)
 800463e:	2200      	movs	r2, #0
 8004640:	609a      	str	r2, [r3, #8]
 8004642:	4a0f      	ldr	r2, [pc, #60]	; (8004680 <SystemInit+0x64>)
 8004644:	4b0e      	ldr	r3, [pc, #56]	; (8004680 <SystemInit+0x64>)
 8004646:	681b      	ldr	r3, [r3, #0]
 8004648:	f023 7384 	bic.w	r3, r3, #17301504	; 0x1080000
 800464c:	f423 3380 	bic.w	r3, r3, #65536	; 0x10000
 8004650:	6013      	str	r3, [r2, #0]
 8004652:	4b0b      	ldr	r3, [pc, #44]	; (8004680 <SystemInit+0x64>)
 8004654:	4a0b      	ldr	r2, [pc, #44]	; (8004684 <SystemInit+0x68>)
 8004656:	605a      	str	r2, [r3, #4]
 8004658:	4a09      	ldr	r2, [pc, #36]	; (8004680 <SystemInit+0x64>)
 800465a:	4b09      	ldr	r3, [pc, #36]	; (8004680 <SystemInit+0x64>)
 800465c:	681b      	ldr	r3, [r3, #0]
 800465e:	f423 2380 	bic.w	r3, r3, #262144	; 0x40000
 8004662:	6013      	str	r3, [r2, #0]
 8004664:	4b06      	ldr	r3, [pc, #24]	; (8004680 <SystemInit+0x64>)
 8004666:	2200      	movs	r2, #0
 8004668:	60da      	str	r2, [r3, #12]
 800466a:	4b04      	ldr	r3, [pc, #16]	; (800467c <SystemInit+0x60>)
 800466c:	f04f 6200 	mov.w	r2, #134217728	; 0x8000000
 8004670:	609a      	str	r2, [r3, #8]
 8004672:	bf00      	nop
 8004674:	46bd      	mov	sp, r7
 8004676:	f85d 7b04 	ldr.w	r7, [sp], #4
 800467a:	4770      	bx	lr
 800467c:	e000ed00 	.word	0xe000ed00
 8004680:	40023800 	.word	0x40023800
 8004684:	24003010 	.word	0x24003010

08004688 <_init>:
 8004688:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 800468a:	bf00      	nop
 800468c:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800468e:	bc08      	pop	{r3}
 8004690:	469e      	mov	lr, r3
 8004692:	4770      	bx	lr

08004694 <_fini>:
 8004694:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 8004696:	bf00      	nop
 8004698:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800469a:	bc08      	pop	{r3}
 800469c:	469e      	mov	lr, r3
 800469e:	4770      	bx	lr
