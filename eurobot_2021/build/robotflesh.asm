
build/robotflesh.elf:     file format elf32-littlearm

Sections:
Idx Name          Size      VMA       LMA       File off  Algn  Flags
  0 .isr_vector   00000188  08000000  08000000  00010000  2**0  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .text         000096d0  08000190  08000190  00010190  2**4  CONTENTS, ALLOC, LOAD, READONLY, CODE
  2 .rodata       00000518  08009860  08009860  00019860  2**3  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .init_array   00000008  08009d78  08009d78  00019d78  2**2  CONTENTS, ALLOC, LOAD, DATA
  4 .fini_array   00000004  08009d80  08009d80  00019d80  2**2  CONTENTS, ALLOC, LOAD, DATA
  5 .data         000008b4  20000000  08009d84  00020000  2**3  CONTENTS, ALLOC, LOAD, DATA
  6 .ccmram       00000000  10000000  10000000  000208b4  2**0  CONTENTS
  7 .bss          0000589c  200008b8  200008b8  000208b8  2**3  ALLOC
  8 ._user_heap_stack 00000600  20006154  20006154  000208b8  2**0  ALLOC
  9 .ARM.attributes 00000030  00000000  00000000  000208b4  2**0  CONTENTS, READONLY
 10 .comment      00000031  00000000  00000000  000208e4  2**0  CONTENTS, READONLY
 11 .debug_info   0001f506  00000000  00000000  00020915  2**0  CONTENTS, READONLY, DEBUGGING
 12 .debug_abbrev 000045c9  00000000  00000000  0003fe1b  2**0  CONTENTS, READONLY, DEBUGGING
 13 .debug_loc    00008f6f  00000000  00000000  000443e4  2**0  CONTENTS, READONLY, DEBUGGING
 14 .debug_aranges 00000fa8  00000000  00000000  0004d358  2**3  CONTENTS, READONLY, DEBUGGING
 15 .debug_ranges 00000cc0  00000000  00000000  0004e300  2**0  CONTENTS, READONLY, DEBUGGING
 16 .debug_line   00006e7d  00000000  00000000  0004efc0  2**0  CONTENTS, READONLY, DEBUGGING
 17 .debug_str    00005179  00000000  00000000  00055e3d  2**0  CONTENTS, READONLY, DEBUGGING
 18 .debug_frame  00003cc0  00000000  00000000  0005afb8  2**2  CONTENTS, READONLY, DEBUGGING

Disassembly of section .text:

08000190 <deregister_tm_clones>:
 8000190:	4b04      	ldr	r3, [pc, #16]	; (80001a4 <deregister_tm_clones+0x14>)
 8000192:	4805      	ldr	r0, [pc, #20]	; (80001a8 <deregister_tm_clones+0x18>)
 8000194:	1a1b      	subs	r3, r3, r0
 8000196:	2b06      	cmp	r3, #6
 8000198:	d902      	bls.n	80001a0 <deregister_tm_clones+0x10>
 800019a:	4b04      	ldr	r3, [pc, #16]	; (80001ac <deregister_tm_clones+0x1c>)
 800019c:	b103      	cbz	r3, 80001a0 <deregister_tm_clones+0x10>
 800019e:	4718      	bx	r3
 80001a0:	4770      	bx	lr
 80001a2:	bf00      	nop
 80001a4:	200008b7 	.word	0x200008b7
 80001a8:	200008b4 	.word	0x200008b4
 80001ac:	00000000 	.word	0x00000000

080001b0 <register_tm_clones>:
 80001b0:	4905      	ldr	r1, [pc, #20]	; (80001c8 <register_tm_clones+0x18>)
 80001b2:	4806      	ldr	r0, [pc, #24]	; (80001cc <register_tm_clones+0x1c>)
 80001b4:	1a09      	subs	r1, r1, r0
 80001b6:	1089      	asrs	r1, r1, #2
 80001b8:	eb01 71d1 	add.w	r1, r1, r1, lsr #31
 80001bc:	1049      	asrs	r1, r1, #1
 80001be:	d002      	beq.n	80001c6 <register_tm_clones+0x16>
 80001c0:	4b03      	ldr	r3, [pc, #12]	; (80001d0 <register_tm_clones+0x20>)
 80001c2:	b103      	cbz	r3, 80001c6 <register_tm_clones+0x16>
 80001c4:	4718      	bx	r3
 80001c6:	4770      	bx	lr
 80001c8:	200008b4 	.word	0x200008b4
 80001cc:	200008b4 	.word	0x200008b4
 80001d0:	00000000 	.word	0x00000000

080001d4 <__do_global_dtors_aux>:
 80001d4:	b510      	push	{r4, lr}
 80001d6:	4c06      	ldr	r4, [pc, #24]	; (80001f0 <__do_global_dtors_aux+0x1c>)
 80001d8:	7823      	ldrb	r3, [r4, #0]
 80001da:	b943      	cbnz	r3, 80001ee <__do_global_dtors_aux+0x1a>
 80001dc:	f7ff ffd8 	bl	8000190 <deregister_tm_clones>
 80001e0:	4b04      	ldr	r3, [pc, #16]	; (80001f4 <__do_global_dtors_aux+0x20>)
 80001e2:	b113      	cbz	r3, 80001ea <__do_global_dtors_aux+0x16>
 80001e4:	4804      	ldr	r0, [pc, #16]	; (80001f8 <__do_global_dtors_aux+0x24>)
 80001e6:	f3af 8000 	nop.w
 80001ea:	2301      	movs	r3, #1
 80001ec:	7023      	strb	r3, [r4, #0]
 80001ee:	bd10      	pop	{r4, pc}
 80001f0:	200008b8 	.word	0x200008b8
 80001f4:	00000000 	.word	0x00000000
 80001f8:	08009848 	.word	0x08009848

080001fc <frame_dummy>:
 80001fc:	b508      	push	{r3, lr}
 80001fe:	4b08      	ldr	r3, [pc, #32]	; (8000220 <frame_dummy+0x24>)
 8000200:	b11b      	cbz	r3, 800020a <frame_dummy+0xe>
 8000202:	4908      	ldr	r1, [pc, #32]	; (8000224 <frame_dummy+0x28>)
 8000204:	4808      	ldr	r0, [pc, #32]	; (8000228 <frame_dummy+0x2c>)
 8000206:	f3af 8000 	nop.w
 800020a:	4808      	ldr	r0, [pc, #32]	; (800022c <frame_dummy+0x30>)
 800020c:	6803      	ldr	r3, [r0, #0]
 800020e:	b913      	cbnz	r3, 8000216 <frame_dummy+0x1a>
 8000210:	e8bd 4008 	ldmia.w	sp!, {r3, lr}
 8000214:	e7cc      	b.n	80001b0 <register_tm_clones>
 8000216:	4b06      	ldr	r3, [pc, #24]	; (8000230 <frame_dummy+0x34>)
 8000218:	2b00      	cmp	r3, #0
 800021a:	d0f9      	beq.n	8000210 <frame_dummy+0x14>
 800021c:	4798      	blx	r3
 800021e:	e7f7      	b.n	8000210 <frame_dummy+0x14>
 8000220:	00000000 	.word	0x00000000
 8000224:	200008bc 	.word	0x200008bc
 8000228:	08009848 	.word	0x08009848
 800022c:	200008b4 	.word	0x200008b4
 8000230:	00000000 	.word	0x00000000

08000234 <__aeabi_drsub>:
 8000234:	f081 4100 	eor.w	r1, r1, #2147483648	; 0x80000000
 8000238:	e002      	b.n	8000240 <__adddf3>
 800023a:	bf00      	nop

0800023c <__aeabi_dsub>:
 800023c:	f083 4300 	eor.w	r3, r3, #2147483648	; 0x80000000

08000240 <__adddf3>:
 8000240:	b530      	push	{r4, r5, lr}
 8000242:	ea4f 0441 	mov.w	r4, r1, lsl #1
 8000246:	ea4f 0543 	mov.w	r5, r3, lsl #1
 800024a:	ea94 0f05 	teq	r4, r5
 800024e:	bf08      	it	eq
 8000250:	ea90 0f02 	teqeq	r0, r2
 8000254:	bf1f      	itttt	ne
 8000256:	ea54 0c00 	orrsne.w	ip, r4, r0
 800025a:	ea55 0c02 	orrsne.w	ip, r5, r2
 800025e:	ea7f 5c64 	mvnsne.w	ip, r4, asr #21
 8000262:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 8000266:	f000 80e2 	beq.w	800042e <__adddf3+0x1ee>
 800026a:	ea4f 5454 	mov.w	r4, r4, lsr #21
 800026e:	ebd4 5555 	rsbs	r5, r4, r5, lsr #21
 8000272:	bfb8      	it	lt
 8000274:	426d      	neglt	r5, r5
 8000276:	dd0c      	ble.n	8000292 <__adddf3+0x52>
 8000278:	442c      	add	r4, r5
 800027a:	ea80 0202 	eor.w	r2, r0, r2
 800027e:	ea81 0303 	eor.w	r3, r1, r3
 8000282:	ea82 0000 	eor.w	r0, r2, r0
 8000286:	ea83 0101 	eor.w	r1, r3, r1
 800028a:	ea80 0202 	eor.w	r2, r0, r2
 800028e:	ea81 0303 	eor.w	r3, r1, r3
 8000292:	2d36      	cmp	r5, #54	; 0x36
 8000294:	bf88      	it	hi
 8000296:	bd30      	pophi	{r4, r5, pc}
 8000298:	f011 4f00 	tst.w	r1, #2147483648	; 0x80000000
 800029c:	ea4f 3101 	mov.w	r1, r1, lsl #12
 80002a0:	f44f 1c80 	mov.w	ip, #1048576	; 0x100000
 80002a4:	ea4c 3111 	orr.w	r1, ip, r1, lsr #12
 80002a8:	d002      	beq.n	80002b0 <__adddf3+0x70>
 80002aa:	4240      	negs	r0, r0
 80002ac:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 80002b0:	f013 4f00 	tst.w	r3, #2147483648	; 0x80000000
 80002b4:	ea4f 3303 	mov.w	r3, r3, lsl #12
 80002b8:	ea4c 3313 	orr.w	r3, ip, r3, lsr #12
 80002bc:	d002      	beq.n	80002c4 <__adddf3+0x84>
 80002be:	4252      	negs	r2, r2
 80002c0:	eb63 0343 	sbc.w	r3, r3, r3, lsl #1
 80002c4:	ea94 0f05 	teq	r4, r5
 80002c8:	f000 80a7 	beq.w	800041a <__adddf3+0x1da>
 80002cc:	f1a4 0401 	sub.w	r4, r4, #1
 80002d0:	f1d5 0e20 	rsbs	lr, r5, #32
 80002d4:	db0d      	blt.n	80002f2 <__adddf3+0xb2>
 80002d6:	fa02 fc0e 	lsl.w	ip, r2, lr
 80002da:	fa22 f205 	lsr.w	r2, r2, r5
 80002de:	1880      	adds	r0, r0, r2
 80002e0:	f141 0100 	adc.w	r1, r1, #0
 80002e4:	fa03 f20e 	lsl.w	r2, r3, lr
 80002e8:	1880      	adds	r0, r0, r2
 80002ea:	fa43 f305 	asr.w	r3, r3, r5
 80002ee:	4159      	adcs	r1, r3
 80002f0:	e00e      	b.n	8000310 <__adddf3+0xd0>
 80002f2:	f1a5 0520 	sub.w	r5, r5, #32
 80002f6:	f10e 0e20 	add.w	lr, lr, #32
 80002fa:	2a01      	cmp	r2, #1
 80002fc:	fa03 fc0e 	lsl.w	ip, r3, lr
 8000300:	bf28      	it	cs
 8000302:	f04c 0c02 	orrcs.w	ip, ip, #2
 8000306:	fa43 f305 	asr.w	r3, r3, r5
 800030a:	18c0      	adds	r0, r0, r3
 800030c:	eb51 71e3 	adcs.w	r1, r1, r3, asr #31
 8000310:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8000314:	d507      	bpl.n	8000326 <__adddf3+0xe6>
 8000316:	f04f 0e00 	mov.w	lr, #0
 800031a:	f1dc 0c00 	rsbs	ip, ip, #0
 800031e:	eb7e 0000 	sbcs.w	r0, lr, r0
 8000322:	eb6e 0101 	sbc.w	r1, lr, r1
 8000326:	f5b1 1f80 	cmp.w	r1, #1048576	; 0x100000
 800032a:	d31b      	bcc.n	8000364 <__adddf3+0x124>
 800032c:	f5b1 1f00 	cmp.w	r1, #2097152	; 0x200000
 8000330:	d30c      	bcc.n	800034c <__adddf3+0x10c>
 8000332:	0849      	lsrs	r1, r1, #1
 8000334:	ea5f 0030 	movs.w	r0, r0, rrx
 8000338:	ea4f 0c3c 	mov.w	ip, ip, rrx
 800033c:	f104 0401 	add.w	r4, r4, #1
 8000340:	ea4f 5244 	mov.w	r2, r4, lsl #21
 8000344:	f512 0f80 	cmn.w	r2, #4194304	; 0x400000
 8000348:	f080 809a 	bcs.w	8000480 <__adddf3+0x240>
 800034c:	f1bc 4f00 	cmp.w	ip, #2147483648	; 0x80000000
 8000350:	bf08      	it	eq
 8000352:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 8000356:	f150 0000 	adcs.w	r0, r0, #0
 800035a:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 800035e:	ea41 0105 	orr.w	r1, r1, r5
 8000362:	bd30      	pop	{r4, r5, pc}
 8000364:	ea5f 0c4c 	movs.w	ip, ip, lsl #1
 8000368:	4140      	adcs	r0, r0
 800036a:	eb41 0101 	adc.w	r1, r1, r1
 800036e:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 8000372:	f1a4 0401 	sub.w	r4, r4, #1
 8000376:	d1e9      	bne.n	800034c <__adddf3+0x10c>
 8000378:	f091 0f00 	teq	r1, #0
 800037c:	bf04      	itt	eq
 800037e:	4601      	moveq	r1, r0
 8000380:	2000      	moveq	r0, #0
 8000382:	fab1 f381 	clz	r3, r1
 8000386:	bf08      	it	eq
 8000388:	3320      	addeq	r3, #32
 800038a:	f1a3 030b 	sub.w	r3, r3, #11
 800038e:	f1b3 0220 	subs.w	r2, r3, #32
 8000392:	da0c      	bge.n	80003ae <__adddf3+0x16e>
 8000394:	320c      	adds	r2, #12
 8000396:	dd08      	ble.n	80003aa <__adddf3+0x16a>
 8000398:	f102 0c14 	add.w	ip, r2, #20
 800039c:	f1c2 020c 	rsb	r2, r2, #12
 80003a0:	fa01 f00c 	lsl.w	r0, r1, ip
 80003a4:	fa21 f102 	lsr.w	r1, r1, r2
 80003a8:	e00c      	b.n	80003c4 <__adddf3+0x184>
 80003aa:	f102 0214 	add.w	r2, r2, #20
 80003ae:	bfd8      	it	le
 80003b0:	f1c2 0c20 	rsble	ip, r2, #32
 80003b4:	fa01 f102 	lsl.w	r1, r1, r2
 80003b8:	fa20 fc0c 	lsr.w	ip, r0, ip
 80003bc:	bfdc      	itt	le
 80003be:	ea41 010c 	orrle.w	r1, r1, ip
 80003c2:	4090      	lslle	r0, r2
 80003c4:	1ae4      	subs	r4, r4, r3
 80003c6:	bfa2      	ittt	ge
 80003c8:	eb01 5104 	addge.w	r1, r1, r4, lsl #20
 80003cc:	4329      	orrge	r1, r5
 80003ce:	bd30      	popge	{r4, r5, pc}
 80003d0:	ea6f 0404 	mvn.w	r4, r4
 80003d4:	3c1f      	subs	r4, #31
 80003d6:	da1c      	bge.n	8000412 <__adddf3+0x1d2>
 80003d8:	340c      	adds	r4, #12
 80003da:	dc0e      	bgt.n	80003fa <__adddf3+0x1ba>
 80003dc:	f104 0414 	add.w	r4, r4, #20
 80003e0:	f1c4 0220 	rsb	r2, r4, #32
 80003e4:	fa20 f004 	lsr.w	r0, r0, r4
 80003e8:	fa01 f302 	lsl.w	r3, r1, r2
 80003ec:	ea40 0003 	orr.w	r0, r0, r3
 80003f0:	fa21 f304 	lsr.w	r3, r1, r4
 80003f4:	ea45 0103 	orr.w	r1, r5, r3
 80003f8:	bd30      	pop	{r4, r5, pc}
 80003fa:	f1c4 040c 	rsb	r4, r4, #12
 80003fe:	f1c4 0220 	rsb	r2, r4, #32
 8000402:	fa20 f002 	lsr.w	r0, r0, r2
 8000406:	fa01 f304 	lsl.w	r3, r1, r4
 800040a:	ea40 0003 	orr.w	r0, r0, r3
 800040e:	4629      	mov	r1, r5
 8000410:	bd30      	pop	{r4, r5, pc}
 8000412:	fa21 f004 	lsr.w	r0, r1, r4
 8000416:	4629      	mov	r1, r5
 8000418:	bd30      	pop	{r4, r5, pc}
 800041a:	f094 0f00 	teq	r4, #0
 800041e:	f483 1380 	eor.w	r3, r3, #1048576	; 0x100000
 8000422:	bf06      	itte	eq
 8000424:	f481 1180 	eoreq.w	r1, r1, #1048576	; 0x100000
 8000428:	3401      	addeq	r4, #1
 800042a:	3d01      	subne	r5, #1
 800042c:	e74e      	b.n	80002cc <__adddf3+0x8c>
 800042e:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 8000432:	bf18      	it	ne
 8000434:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 8000438:	d029      	beq.n	800048e <__adddf3+0x24e>
 800043a:	ea94 0f05 	teq	r4, r5
 800043e:	bf08      	it	eq
 8000440:	ea90 0f02 	teqeq	r0, r2
 8000444:	d005      	beq.n	8000452 <__adddf3+0x212>
 8000446:	ea54 0c00 	orrs.w	ip, r4, r0
 800044a:	bf04      	itt	eq
 800044c:	4619      	moveq	r1, r3
 800044e:	4610      	moveq	r0, r2
 8000450:	bd30      	pop	{r4, r5, pc}
 8000452:	ea91 0f03 	teq	r1, r3
 8000456:	bf1e      	ittt	ne
 8000458:	2100      	movne	r1, #0
 800045a:	2000      	movne	r0, #0
 800045c:	bd30      	popne	{r4, r5, pc}
 800045e:	ea5f 5c54 	movs.w	ip, r4, lsr #21
 8000462:	d105      	bne.n	8000470 <__adddf3+0x230>
 8000464:	0040      	lsls	r0, r0, #1
 8000466:	4149      	adcs	r1, r1
 8000468:	bf28      	it	cs
 800046a:	f041 4100 	orrcs.w	r1, r1, #2147483648	; 0x80000000
 800046e:	bd30      	pop	{r4, r5, pc}
 8000470:	f514 0480 	adds.w	r4, r4, #4194304	; 0x400000
 8000474:	bf3c      	itt	cc
 8000476:	f501 1180 	addcc.w	r1, r1, #1048576	; 0x100000
 800047a:	bd30      	popcc	{r4, r5, pc}
 800047c:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8000480:	f045 41fe 	orr.w	r1, r5, #2130706432	; 0x7f000000
 8000484:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 8000488:	f04f 0000 	mov.w	r0, #0
 800048c:	bd30      	pop	{r4, r5, pc}
 800048e:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 8000492:	bf1a      	itte	ne
 8000494:	4619      	movne	r1, r3
 8000496:	4610      	movne	r0, r2
 8000498:	ea7f 5c65 	mvnseq.w	ip, r5, asr #21
 800049c:	bf1c      	itt	ne
 800049e:	460b      	movne	r3, r1
 80004a0:	4602      	movne	r2, r0
 80004a2:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 80004a6:	bf06      	itte	eq
 80004a8:	ea52 3503 	orrseq.w	r5, r2, r3, lsl #12
 80004ac:	ea91 0f03 	teqeq	r1, r3
 80004b0:	f441 2100 	orrne.w	r1, r1, #524288	; 0x80000
 80004b4:	bd30      	pop	{r4, r5, pc}
 80004b6:	bf00      	nop

080004b8 <__aeabi_ui2d>:
 80004b8:	f090 0f00 	teq	r0, #0
 80004bc:	bf04      	itt	eq
 80004be:	2100      	moveq	r1, #0
 80004c0:	4770      	bxeq	lr
 80004c2:	b530      	push	{r4, r5, lr}
 80004c4:	f44f 6480 	mov.w	r4, #1024	; 0x400
 80004c8:	f104 0432 	add.w	r4, r4, #50	; 0x32
 80004cc:	f04f 0500 	mov.w	r5, #0
 80004d0:	f04f 0100 	mov.w	r1, #0
 80004d4:	e750      	b.n	8000378 <__adddf3+0x138>
 80004d6:	bf00      	nop

080004d8 <__aeabi_i2d>:
 80004d8:	f090 0f00 	teq	r0, #0
 80004dc:	bf04      	itt	eq
 80004de:	2100      	moveq	r1, #0
 80004e0:	4770      	bxeq	lr
 80004e2:	b530      	push	{r4, r5, lr}
 80004e4:	f44f 6480 	mov.w	r4, #1024	; 0x400
 80004e8:	f104 0432 	add.w	r4, r4, #50	; 0x32
 80004ec:	f010 4500 	ands.w	r5, r0, #2147483648	; 0x80000000
 80004f0:	bf48      	it	mi
 80004f2:	4240      	negmi	r0, r0
 80004f4:	f04f 0100 	mov.w	r1, #0
 80004f8:	e73e      	b.n	8000378 <__adddf3+0x138>
 80004fa:	bf00      	nop

080004fc <__aeabi_f2d>:
 80004fc:	0042      	lsls	r2, r0, #1
 80004fe:	ea4f 01e2 	mov.w	r1, r2, asr #3
 8000502:	ea4f 0131 	mov.w	r1, r1, rrx
 8000506:	ea4f 7002 	mov.w	r0, r2, lsl #28
 800050a:	bf1f      	itttt	ne
 800050c:	f012 437f 	andsne.w	r3, r2, #4278190080	; 0xff000000
 8000510:	f093 4f7f 	teqne	r3, #4278190080	; 0xff000000
 8000514:	f081 5160 	eorne.w	r1, r1, #939524096	; 0x38000000
 8000518:	4770      	bxne	lr
 800051a:	f092 0f00 	teq	r2, #0
 800051e:	bf14      	ite	ne
 8000520:	f093 4f7f 	teqne	r3, #4278190080	; 0xff000000
 8000524:	4770      	bxeq	lr
 8000526:	b530      	push	{r4, r5, lr}
 8000528:	f44f 7460 	mov.w	r4, #896	; 0x380
 800052c:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8000530:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 8000534:	e720      	b.n	8000378 <__adddf3+0x138>
 8000536:	bf00      	nop

08000538 <__aeabi_ul2d>:
 8000538:	ea50 0201 	orrs.w	r2, r0, r1
 800053c:	bf08      	it	eq
 800053e:	4770      	bxeq	lr
 8000540:	b530      	push	{r4, r5, lr}
 8000542:	f04f 0500 	mov.w	r5, #0
 8000546:	e00a      	b.n	800055e <__aeabi_l2d+0x16>

08000548 <__aeabi_l2d>:
 8000548:	ea50 0201 	orrs.w	r2, r0, r1
 800054c:	bf08      	it	eq
 800054e:	4770      	bxeq	lr
 8000550:	b530      	push	{r4, r5, lr}
 8000552:	f011 4500 	ands.w	r5, r1, #2147483648	; 0x80000000
 8000556:	d502      	bpl.n	800055e <__aeabi_l2d+0x16>
 8000558:	4240      	negs	r0, r0
 800055a:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 800055e:	f44f 6480 	mov.w	r4, #1024	; 0x400
 8000562:	f104 0432 	add.w	r4, r4, #50	; 0x32
 8000566:	ea5f 5c91 	movs.w	ip, r1, lsr #22
 800056a:	f43f aedc 	beq.w	8000326 <__adddf3+0xe6>
 800056e:	f04f 0203 	mov.w	r2, #3
 8000572:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 8000576:	bf18      	it	ne
 8000578:	3203      	addne	r2, #3
 800057a:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 800057e:	bf18      	it	ne
 8000580:	3203      	addne	r2, #3
 8000582:	eb02 02dc 	add.w	r2, r2, ip, lsr #3
 8000586:	f1c2 0320 	rsb	r3, r2, #32
 800058a:	fa00 fc03 	lsl.w	ip, r0, r3
 800058e:	fa20 f002 	lsr.w	r0, r0, r2
 8000592:	fa01 fe03 	lsl.w	lr, r1, r3
 8000596:	ea40 000e 	orr.w	r0, r0, lr
 800059a:	fa21 f102 	lsr.w	r1, r1, r2
 800059e:	4414      	add	r4, r2
 80005a0:	e6c1      	b.n	8000326 <__adddf3+0xe6>
 80005a2:	bf00      	nop

080005a4 <__aeabi_d2f>:
 80005a4:	ea4f 0241 	mov.w	r2, r1, lsl #1
 80005a8:	f1b2 43e0 	subs.w	r3, r2, #1879048192	; 0x70000000
 80005ac:	bf24      	itt	cs
 80005ae:	f5b3 1c00 	subscs.w	ip, r3, #2097152	; 0x200000
 80005b2:	f1dc 5cfe 	rsbscs	ip, ip, #532676608	; 0x1fc00000
 80005b6:	d90d      	bls.n	80005d4 <__aeabi_d2f+0x30>
 80005b8:	f001 4c00 	and.w	ip, r1, #2147483648	; 0x80000000
 80005bc:	ea4f 02c0 	mov.w	r2, r0, lsl #3
 80005c0:	ea4c 7050 	orr.w	r0, ip, r0, lsr #29
 80005c4:	f1b2 4f00 	cmp.w	r2, #2147483648	; 0x80000000
 80005c8:	eb40 0083 	adc.w	r0, r0, r3, lsl #2
 80005cc:	bf08      	it	eq
 80005ce:	f020 0001 	biceq.w	r0, r0, #1
 80005d2:	4770      	bx	lr
 80005d4:	f011 4f80 	tst.w	r1, #1073741824	; 0x40000000
 80005d8:	d121      	bne.n	800061e <__aeabi_d2f+0x7a>
 80005da:	f113 7238 	adds.w	r2, r3, #48234496	; 0x2e00000
 80005de:	bfbc      	itt	lt
 80005e0:	f001 4000 	andlt.w	r0, r1, #2147483648	; 0x80000000
 80005e4:	4770      	bxlt	lr
 80005e6:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 80005ea:	ea4f 5252 	mov.w	r2, r2, lsr #21
 80005ee:	f1c2 0218 	rsb	r2, r2, #24
 80005f2:	f1c2 0c20 	rsb	ip, r2, #32
 80005f6:	fa10 f30c 	lsls.w	r3, r0, ip
 80005fa:	fa20 f002 	lsr.w	r0, r0, r2
 80005fe:	bf18      	it	ne
 8000600:	f040 0001 	orrne.w	r0, r0, #1
 8000604:	ea4f 23c1 	mov.w	r3, r1, lsl #11
 8000608:	ea4f 23d3 	mov.w	r3, r3, lsr #11
 800060c:	fa03 fc0c 	lsl.w	ip, r3, ip
 8000610:	ea40 000c 	orr.w	r0, r0, ip
 8000614:	fa23 f302 	lsr.w	r3, r3, r2
 8000618:	ea4f 0343 	mov.w	r3, r3, lsl #1
 800061c:	e7cc      	b.n	80005b8 <__aeabi_d2f+0x14>
 800061e:	ea7f 5362 	mvns.w	r3, r2, asr #21
 8000622:	d107      	bne.n	8000634 <__aeabi_d2f+0x90>
 8000624:	ea50 3301 	orrs.w	r3, r0, r1, lsl #12
 8000628:	bf1e      	ittt	ne
 800062a:	f04f 40fe 	movne.w	r0, #2130706432	; 0x7f000000
 800062e:	f440 0040 	orrne.w	r0, r0, #12582912	; 0xc00000
 8000632:	4770      	bxne	lr
 8000634:	f001 4000 	and.w	r0, r1, #2147483648	; 0x80000000
 8000638:	f040 40fe 	orr.w	r0, r0, #2130706432	; 0x7f000000
 800063c:	f440 0000 	orr.w	r0, r0, #8388608	; 0x800000
 8000640:	4770      	bx	lr
 8000642:	bf00      	nop

08000644 <atexit>:
 8000644:	2300      	movs	r3, #0
 8000646:	4601      	mov	r1, r0
 8000648:	461a      	mov	r2, r3
 800064a:	4618      	mov	r0, r3
 800064c:	f000 bc4c 	b.w	8000ee8 <__register_exitproc>

08000650 <__libc_fini_array>:
 8000650:	b538      	push	{r3, r4, r5, lr}
 8000652:	4c0a      	ldr	r4, [pc, #40]	; (800067c <__libc_fini_array+0x2c>)
 8000654:	4d0a      	ldr	r5, [pc, #40]	; (8000680 <__libc_fini_array+0x30>)
 8000656:	1b64      	subs	r4, r4, r5
 8000658:	10a4      	asrs	r4, r4, #2
 800065a:	d00a      	beq.n	8000672 <__libc_fini_array+0x22>
 800065c:	f104 4380 	add.w	r3, r4, #1073741824	; 0x40000000
 8000660:	3b01      	subs	r3, #1
 8000662:	eb05 0583 	add.w	r5, r5, r3, lsl #2
 8000666:	3c01      	subs	r4, #1
 8000668:	f855 3904 	ldr.w	r3, [r5], #-4
 800066c:	4798      	blx	r3
 800066e:	2c00      	cmp	r4, #0
 8000670:	d1f9      	bne.n	8000666 <__libc_fini_array+0x16>
 8000672:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
 8000676:	f009 b8ed 	b.w	8009854 <_fini>
 800067a:	bf00      	nop
 800067c:	08009d84 	.word	0x08009d84
 8000680:	08009d80 	.word	0x08009d80

08000684 <__libc_init_array>:
 8000684:	b570      	push	{r4, r5, r6, lr}
 8000686:	4e0f      	ldr	r6, [pc, #60]	; (80006c4 <__libc_init_array+0x40>)
 8000688:	4d0f      	ldr	r5, [pc, #60]	; (80006c8 <__libc_init_array+0x44>)
 800068a:	1b76      	subs	r6, r6, r5
 800068c:	10b6      	asrs	r6, r6, #2
 800068e:	bf18      	it	ne
 8000690:	2400      	movne	r4, #0
 8000692:	d005      	beq.n	80006a0 <__libc_init_array+0x1c>
 8000694:	3401      	adds	r4, #1
 8000696:	f855 3b04 	ldr.w	r3, [r5], #4
 800069a:	4798      	blx	r3
 800069c:	42a6      	cmp	r6, r4
 800069e:	d1f9      	bne.n	8000694 <__libc_init_array+0x10>
 80006a0:	4e0a      	ldr	r6, [pc, #40]	; (80006cc <__libc_init_array+0x48>)
 80006a2:	4d0b      	ldr	r5, [pc, #44]	; (80006d0 <__libc_init_array+0x4c>)
 80006a4:	1b76      	subs	r6, r6, r5
 80006a6:	f009 f8cf 	bl	8009848 <_init>
 80006aa:	10b6      	asrs	r6, r6, #2
 80006ac:	bf18      	it	ne
 80006ae:	2400      	movne	r4, #0
 80006b0:	d006      	beq.n	80006c0 <__libc_init_array+0x3c>
 80006b2:	3401      	adds	r4, #1
 80006b4:	f855 3b04 	ldr.w	r3, [r5], #4
 80006b8:	4798      	blx	r3
 80006ba:	42a6      	cmp	r6, r4
 80006bc:	d1f9      	bne.n	80006b2 <__libc_init_array+0x2e>
 80006be:	bd70      	pop	{r4, r5, r6, pc}
 80006c0:	bd70      	pop	{r4, r5, r6, pc}
 80006c2:	bf00      	nop
 80006c4:	08009d78 	.word	0x08009d78
 80006c8:	08009d78 	.word	0x08009d78
 80006cc:	08009d80 	.word	0x08009d80
 80006d0:	08009d78 	.word	0x08009d78

080006d4 <malloc>:
 80006d4:	4b02      	ldr	r3, [pc, #8]	; (80006e0 <malloc+0xc>)
 80006d6:	4601      	mov	r1, r0
 80006d8:	6818      	ldr	r0, [r3, #0]
 80006da:	f000 b80b 	b.w	80006f4 <_malloc_r>
 80006de:	bf00      	nop
 80006e0:	20000000 	.word	0x20000000

080006e4 <free>:
 80006e4:	4b02      	ldr	r3, [pc, #8]	; (80006f0 <free+0xc>)
 80006e6:	4601      	mov	r1, r0
 80006e8:	6818      	ldr	r0, [r3, #0]
 80006ea:	f003 bbdb 	b.w	8003ea4 <_free_r>
 80006ee:	bf00      	nop
 80006f0:	20000000 	.word	0x20000000

080006f4 <_malloc_r>:
 80006f4:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80006f8:	f101 060b 	add.w	r6, r1, #11
 80006fc:	2e16      	cmp	r6, #22
 80006fe:	b083      	sub	sp, #12
 8000700:	4605      	mov	r5, r0
 8000702:	f240 809e 	bls.w	8000842 <_malloc_r+0x14e>
 8000706:	f036 0607 	bics.w	r6, r6, #7
 800070a:	f100 80bd 	bmi.w	8000888 <_malloc_r+0x194>
 800070e:	42b1      	cmp	r1, r6
 8000710:	f200 80ba 	bhi.w	8000888 <_malloc_r+0x194>
 8000714:	f000 fb80 	bl	8000e18 <__malloc_lock>
 8000718:	f5b6 7ffc 	cmp.w	r6, #504	; 0x1f8
 800071c:	f0c0 828f 	bcc.w	8000c3e <_malloc_r+0x54a>
 8000720:	0a73      	lsrs	r3, r6, #9
 8000722:	f000 80b8 	beq.w	8000896 <_malloc_r+0x1a2>
 8000726:	2b04      	cmp	r3, #4
 8000728:	f200 817d 	bhi.w	8000a26 <_malloc_r+0x332>
 800072c:	09b3      	lsrs	r3, r6, #6
 800072e:	f103 0039 	add.w	r0, r3, #57	; 0x39
 8000732:	f103 0e38 	add.w	lr, r3, #56	; 0x38
 8000736:	00c3      	lsls	r3, r0, #3
 8000738:	4fc1      	ldr	r7, [pc, #772]	; (8000a40 <_malloc_r+0x34c>)
 800073a:	443b      	add	r3, r7
 800073c:	f1a3 0108 	sub.w	r1, r3, #8
 8000740:	685c      	ldr	r4, [r3, #4]
 8000742:	42a1      	cmp	r1, r4
 8000744:	d106      	bne.n	8000754 <_malloc_r+0x60>
 8000746:	e00c      	b.n	8000762 <_malloc_r+0x6e>
 8000748:	2a00      	cmp	r2, #0
 800074a:	f280 80aa 	bge.w	80008a2 <_malloc_r+0x1ae>
 800074e:	68e4      	ldr	r4, [r4, #12]
 8000750:	42a1      	cmp	r1, r4
 8000752:	d006      	beq.n	8000762 <_malloc_r+0x6e>
 8000754:	6863      	ldr	r3, [r4, #4]
 8000756:	f023 0303 	bic.w	r3, r3, #3
 800075a:	1b9a      	subs	r2, r3, r6
 800075c:	2a0f      	cmp	r2, #15
 800075e:	ddf3      	ble.n	8000748 <_malloc_r+0x54>
 8000760:	4670      	mov	r0, lr
 8000762:	693c      	ldr	r4, [r7, #16]
 8000764:	f8df e2dc 	ldr.w	lr, [pc, #732]	; 8000a44 <_malloc_r+0x350>
 8000768:	4574      	cmp	r4, lr
 800076a:	f000 81a7 	beq.w	8000abc <_malloc_r+0x3c8>
 800076e:	6863      	ldr	r3, [r4, #4]
 8000770:	f023 0303 	bic.w	r3, r3, #3
 8000774:	1b9a      	subs	r2, r3, r6
 8000776:	2a0f      	cmp	r2, #15
 8000778:	f300 818c 	bgt.w	8000a94 <_malloc_r+0x3a0>
 800077c:	2a00      	cmp	r2, #0
 800077e:	f8c7 e014 	str.w	lr, [r7, #20]
 8000782:	f8c7 e010 	str.w	lr, [r7, #16]
 8000786:	f280 809d 	bge.w	80008c4 <_malloc_r+0x1d0>
 800078a:	f5b3 7f00 	cmp.w	r3, #512	; 0x200
 800078e:	f080 815d 	bcs.w	8000a4c <_malloc_r+0x358>
 8000792:	08db      	lsrs	r3, r3, #3
 8000794:	f103 0c01 	add.w	ip, r3, #1
 8000798:	1099      	asrs	r1, r3, #2
 800079a:	687a      	ldr	r2, [r7, #4]
 800079c:	f857 803c 	ldr.w	r8, [r7, ip, lsl #3]
 80007a0:	f8c4 8008 	str.w	r8, [r4, #8]
 80007a4:	2301      	movs	r3, #1
 80007a6:	408b      	lsls	r3, r1
 80007a8:	eb07 01cc 	add.w	r1, r7, ip, lsl #3
 80007ac:	4313      	orrs	r3, r2
 80007ae:	3908      	subs	r1, #8
 80007b0:	60e1      	str	r1, [r4, #12]
 80007b2:	607b      	str	r3, [r7, #4]
 80007b4:	f847 403c 	str.w	r4, [r7, ip, lsl #3]
 80007b8:	f8c8 400c 	str.w	r4, [r8, #12]
 80007bc:	1082      	asrs	r2, r0, #2
 80007be:	2401      	movs	r4, #1
 80007c0:	4094      	lsls	r4, r2
 80007c2:	429c      	cmp	r4, r3
 80007c4:	f200 808b 	bhi.w	80008de <_malloc_r+0x1ea>
 80007c8:	421c      	tst	r4, r3
 80007ca:	d106      	bne.n	80007da <_malloc_r+0xe6>
 80007cc:	f020 0003 	bic.w	r0, r0, #3
 80007d0:	0064      	lsls	r4, r4, #1
 80007d2:	421c      	tst	r4, r3
 80007d4:	f100 0004 	add.w	r0, r0, #4
 80007d8:	d0fa      	beq.n	80007d0 <_malloc_r+0xdc>
 80007da:	eb07 09c0 	add.w	r9, r7, r0, lsl #3
 80007de:	46cc      	mov	ip, r9
 80007e0:	4680      	mov	r8, r0
 80007e2:	f8dc 300c 	ldr.w	r3, [ip, #12]
 80007e6:	459c      	cmp	ip, r3
 80007e8:	d107      	bne.n	80007fa <_malloc_r+0x106>
 80007ea:	e169      	b.n	8000ac0 <_malloc_r+0x3cc>
 80007ec:	2a00      	cmp	r2, #0
 80007ee:	f280 8177 	bge.w	8000ae0 <_malloc_r+0x3ec>
 80007f2:	68db      	ldr	r3, [r3, #12]
 80007f4:	459c      	cmp	ip, r3
 80007f6:	f000 8163 	beq.w	8000ac0 <_malloc_r+0x3cc>
 80007fa:	6859      	ldr	r1, [r3, #4]
 80007fc:	f021 0103 	bic.w	r1, r1, #3
 8000800:	1b8a      	subs	r2, r1, r6
 8000802:	2a0f      	cmp	r2, #15
 8000804:	ddf2      	ble.n	80007ec <_malloc_r+0xf8>
 8000806:	f8d3 c00c 	ldr.w	ip, [r3, #12]
 800080a:	f8d3 8008 	ldr.w	r8, [r3, #8]
 800080e:	9301      	str	r3, [sp, #4]
 8000810:	199c      	adds	r4, r3, r6
 8000812:	4628      	mov	r0, r5
 8000814:	f046 0601 	orr.w	r6, r6, #1
 8000818:	f042 0501 	orr.w	r5, r2, #1
 800081c:	605e      	str	r6, [r3, #4]
 800081e:	f8c8 c00c 	str.w	ip, [r8, #12]
 8000822:	f8cc 8008 	str.w	r8, [ip, #8]
 8000826:	617c      	str	r4, [r7, #20]
 8000828:	613c      	str	r4, [r7, #16]
 800082a:	f8c4 e00c 	str.w	lr, [r4, #12]
 800082e:	f8c4 e008 	str.w	lr, [r4, #8]
 8000832:	6065      	str	r5, [r4, #4]
 8000834:	505a      	str	r2, [r3, r1]
 8000836:	f000 faf1 	bl	8000e1c <__malloc_unlock>
 800083a:	9b01      	ldr	r3, [sp, #4]
 800083c:	f103 0408 	add.w	r4, r3, #8
 8000840:	e01e      	b.n	8000880 <_malloc_r+0x18c>
 8000842:	2910      	cmp	r1, #16
 8000844:	d820      	bhi.n	8000888 <_malloc_r+0x194>
 8000846:	f000 fae7 	bl	8000e18 <__malloc_lock>
 800084a:	2610      	movs	r6, #16
 800084c:	2318      	movs	r3, #24
 800084e:	2002      	movs	r0, #2
 8000850:	4f7b      	ldr	r7, [pc, #492]	; (8000a40 <_malloc_r+0x34c>)
 8000852:	443b      	add	r3, r7
 8000854:	f1a3 0208 	sub.w	r2, r3, #8
 8000858:	685c      	ldr	r4, [r3, #4]
 800085a:	4294      	cmp	r4, r2
 800085c:	f000 8139 	beq.w	8000ad2 <_malloc_r+0x3de>
 8000860:	6863      	ldr	r3, [r4, #4]
 8000862:	68e1      	ldr	r1, [r4, #12]
 8000864:	68a6      	ldr	r6, [r4, #8]
 8000866:	f023 0303 	bic.w	r3, r3, #3
 800086a:	4423      	add	r3, r4
 800086c:	4628      	mov	r0, r5
 800086e:	685a      	ldr	r2, [r3, #4]
 8000870:	60f1      	str	r1, [r6, #12]
 8000872:	f042 0201 	orr.w	r2, r2, #1
 8000876:	608e      	str	r6, [r1, #8]
 8000878:	605a      	str	r2, [r3, #4]
 800087a:	f000 facf 	bl	8000e1c <__malloc_unlock>
 800087e:	3408      	adds	r4, #8
 8000880:	4620      	mov	r0, r4
 8000882:	b003      	add	sp, #12
 8000884:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8000888:	2400      	movs	r4, #0
 800088a:	230c      	movs	r3, #12
 800088c:	4620      	mov	r0, r4
 800088e:	602b      	str	r3, [r5, #0]
 8000890:	b003      	add	sp, #12
 8000892:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8000896:	2040      	movs	r0, #64	; 0x40
 8000898:	f44f 7300 	mov.w	r3, #512	; 0x200
 800089c:	f04f 0e3f 	mov.w	lr, #63	; 0x3f
 80008a0:	e74a      	b.n	8000738 <_malloc_r+0x44>
 80008a2:	4423      	add	r3, r4
 80008a4:	68e1      	ldr	r1, [r4, #12]
 80008a6:	685a      	ldr	r2, [r3, #4]
 80008a8:	68a6      	ldr	r6, [r4, #8]
 80008aa:	f042 0201 	orr.w	r2, r2, #1
 80008ae:	60f1      	str	r1, [r6, #12]
 80008b0:	4628      	mov	r0, r5
 80008b2:	608e      	str	r6, [r1, #8]
 80008b4:	605a      	str	r2, [r3, #4]
 80008b6:	f000 fab1 	bl	8000e1c <__malloc_unlock>
 80008ba:	3408      	adds	r4, #8
 80008bc:	4620      	mov	r0, r4
 80008be:	b003      	add	sp, #12
 80008c0:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 80008c4:	4423      	add	r3, r4
 80008c6:	4628      	mov	r0, r5
 80008c8:	685a      	ldr	r2, [r3, #4]
 80008ca:	f042 0201 	orr.w	r2, r2, #1
 80008ce:	605a      	str	r2, [r3, #4]
 80008d0:	f000 faa4 	bl	8000e1c <__malloc_unlock>
 80008d4:	3408      	adds	r4, #8
 80008d6:	4620      	mov	r0, r4
 80008d8:	b003      	add	sp, #12
 80008da:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 80008de:	68bc      	ldr	r4, [r7, #8]
 80008e0:	6863      	ldr	r3, [r4, #4]
 80008e2:	f023 0903 	bic.w	r9, r3, #3
 80008e6:	45b1      	cmp	r9, r6
 80008e8:	d304      	bcc.n	80008f4 <_malloc_r+0x200>
 80008ea:	eba9 0306 	sub.w	r3, r9, r6
 80008ee:	2b0f      	cmp	r3, #15
 80008f0:	f300 8089 	bgt.w	8000a06 <_malloc_r+0x312>
 80008f4:	f8df a150 	ldr.w	sl, [pc, #336]	; 8000a48 <_malloc_r+0x354>
 80008f8:	f8d7 2408 	ldr.w	r2, [r7, #1032]	; 0x408
 80008fc:	f8da 3000 	ldr.w	r3, [sl]
 8000900:	3201      	adds	r2, #1
 8000902:	eb04 0b09 	add.w	fp, r4, r9
 8000906:	eb06 0803 	add.w	r8, r6, r3
 800090a:	f000 8150 	beq.w	8000bae <_malloc_r+0x4ba>
 800090e:	f508 5880 	add.w	r8, r8, #4096	; 0x1000
 8000912:	f108 080f 	add.w	r8, r8, #15
 8000916:	f428 687f 	bic.w	r8, r8, #4080	; 0xff0
 800091a:	f028 080f 	bic.w	r8, r8, #15
 800091e:	4641      	mov	r1, r8
 8000920:	4628      	mov	r0, r5
 8000922:	f000 facf 	bl	8000ec4 <_sbrk_r>
 8000926:	1c42      	adds	r2, r0, #1
 8000928:	4603      	mov	r3, r0
 800092a:	f000 80f6 	beq.w	8000b1a <_malloc_r+0x426>
 800092e:	4583      	cmp	fp, r0
 8000930:	f200 80f1 	bhi.w	8000b16 <_malloc_r+0x422>
 8000934:	f8da 2004 	ldr.w	r2, [sl, #4]
 8000938:	4442      	add	r2, r8
 800093a:	f8ca 2004 	str.w	r2, [sl, #4]
 800093e:	f000 8147 	beq.w	8000bd0 <_malloc_r+0x4dc>
 8000942:	f8d7 1408 	ldr.w	r1, [r7, #1032]	; 0x408
 8000946:	3101      	adds	r1, #1
 8000948:	bf1b      	ittet	ne
 800094a:	eba3 0b0b 	subne.w	fp, r3, fp
 800094e:	445a      	addne	r2, fp
 8000950:	f8c7 3408 	streq.w	r3, [r7, #1032]	; 0x408
 8000954:	f8ca 2004 	strne.w	r2, [sl, #4]
 8000958:	f013 0207 	ands.w	r2, r3, #7
 800095c:	f000 810d 	beq.w	8000b7a <_malloc_r+0x486>
 8000960:	f1c2 0108 	rsb	r1, r2, #8
 8000964:	f5c2 5280 	rsb	r2, r2, #4096	; 0x1000
 8000968:	440b      	add	r3, r1
 800096a:	3208      	adds	r2, #8
 800096c:	4498      	add	r8, r3
 800096e:	f3c8 080b 	ubfx	r8, r8, #0, #12
 8000972:	eba2 0808 	sub.w	r8, r2, r8
 8000976:	4641      	mov	r1, r8
 8000978:	4628      	mov	r0, r5
 800097a:	9301      	str	r3, [sp, #4]
 800097c:	f000 faa2 	bl	8000ec4 <_sbrk_r>
 8000980:	1c43      	adds	r3, r0, #1
 8000982:	9b01      	ldr	r3, [sp, #4]
 8000984:	f000 813e 	beq.w	8000c04 <_malloc_r+0x510>
 8000988:	1ac1      	subs	r1, r0, r3
 800098a:	4441      	add	r1, r8
 800098c:	f041 0101 	orr.w	r1, r1, #1
 8000990:	f8da 2004 	ldr.w	r2, [sl, #4]
 8000994:	60bb      	str	r3, [r7, #8]
 8000996:	4442      	add	r2, r8
 8000998:	42bc      	cmp	r4, r7
 800099a:	6059      	str	r1, [r3, #4]
 800099c:	f8ca 2004 	str.w	r2, [sl, #4]
 80009a0:	d016      	beq.n	80009d0 <_malloc_r+0x2dc>
 80009a2:	f1b9 0f0f 	cmp.w	r9, #15
 80009a6:	f240 80f4 	bls.w	8000b92 <_malloc_r+0x49e>
 80009aa:	6861      	ldr	r1, [r4, #4]
 80009ac:	f1a9 030c 	sub.w	r3, r9, #12
 80009b0:	f023 0307 	bic.w	r3, r3, #7
 80009b4:	18e0      	adds	r0, r4, r3
 80009b6:	f001 0101 	and.w	r1, r1, #1
 80009ba:	f04f 0e05 	mov.w	lr, #5
 80009be:	4319      	orrs	r1, r3
 80009c0:	2b0f      	cmp	r3, #15
 80009c2:	6061      	str	r1, [r4, #4]
 80009c4:	f8c0 e004 	str.w	lr, [r0, #4]
 80009c8:	f8c0 e008 	str.w	lr, [r0, #8]
 80009cc:	f200 8112 	bhi.w	8000bf4 <_malloc_r+0x500>
 80009d0:	f8da 302c 	ldr.w	r3, [sl, #44]	; 0x2c
 80009d4:	68bc      	ldr	r4, [r7, #8]
 80009d6:	429a      	cmp	r2, r3
 80009d8:	f8da 3030 	ldr.w	r3, [sl, #48]	; 0x30
 80009dc:	bf88      	it	hi
 80009de:	f8ca 202c 	strhi.w	r2, [sl, #44]	; 0x2c
 80009e2:	429a      	cmp	r2, r3
 80009e4:	bf88      	it	hi
 80009e6:	f8ca 2030 	strhi.w	r2, [sl, #48]	; 0x30
 80009ea:	6862      	ldr	r2, [r4, #4]
 80009ec:	f022 0203 	bic.w	r2, r2, #3
 80009f0:	4296      	cmp	r6, r2
 80009f2:	eba2 0306 	sub.w	r3, r2, r6
 80009f6:	d801      	bhi.n	80009fc <_malloc_r+0x308>
 80009f8:	2b0f      	cmp	r3, #15
 80009fa:	dc04      	bgt.n	8000a06 <_malloc_r+0x312>
 80009fc:	4628      	mov	r0, r5
 80009fe:	f000 fa0d 	bl	8000e1c <__malloc_unlock>
 8000a02:	2400      	movs	r4, #0
 8000a04:	e73c      	b.n	8000880 <_malloc_r+0x18c>
 8000a06:	19a2      	adds	r2, r4, r6
 8000a08:	f043 0301 	orr.w	r3, r3, #1
 8000a0c:	f046 0601 	orr.w	r6, r6, #1
 8000a10:	6066      	str	r6, [r4, #4]
 8000a12:	4628      	mov	r0, r5
 8000a14:	60ba      	str	r2, [r7, #8]
 8000a16:	6053      	str	r3, [r2, #4]
 8000a18:	f000 fa00 	bl	8000e1c <__malloc_unlock>
 8000a1c:	3408      	adds	r4, #8
 8000a1e:	4620      	mov	r0, r4
 8000a20:	b003      	add	sp, #12
 8000a22:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8000a26:	2b14      	cmp	r3, #20
 8000a28:	d969      	bls.n	8000afe <_malloc_r+0x40a>
 8000a2a:	2b54      	cmp	r3, #84	; 0x54
 8000a2c:	f200 809b 	bhi.w	8000b66 <_malloc_r+0x472>
 8000a30:	0b33      	lsrs	r3, r6, #12
 8000a32:	f103 006f 	add.w	r0, r3, #111	; 0x6f
 8000a36:	f103 0e6e 	add.w	lr, r3, #110	; 0x6e
 8000a3a:	00c3      	lsls	r3, r0, #3
 8000a3c:	e67c      	b.n	8000738 <_malloc_r+0x44>
 8000a3e:	bf00      	nop
 8000a40:	20000430 	.word	0x20000430
 8000a44:	20000438 	.word	0x20000438
 8000a48:	200008d4 	.word	0x200008d4
 8000a4c:	0a5a      	lsrs	r2, r3, #9
 8000a4e:	2a04      	cmp	r2, #4
 8000a50:	d95b      	bls.n	8000b0a <_malloc_r+0x416>
 8000a52:	2a14      	cmp	r2, #20
 8000a54:	f200 80ae 	bhi.w	8000bb4 <_malloc_r+0x4c0>
 8000a58:	f102 015c 	add.w	r1, r2, #92	; 0x5c
 8000a5c:	00c9      	lsls	r1, r1, #3
 8000a5e:	325b      	adds	r2, #91	; 0x5b
 8000a60:	eb07 0c01 	add.w	ip, r7, r1
 8000a64:	5879      	ldr	r1, [r7, r1]
 8000a66:	f1ac 0c08 	sub.w	ip, ip, #8
 8000a6a:	458c      	cmp	ip, r1
 8000a6c:	f000 8088 	beq.w	8000b80 <_malloc_r+0x48c>
 8000a70:	684a      	ldr	r2, [r1, #4]
 8000a72:	f022 0203 	bic.w	r2, r2, #3
 8000a76:	4293      	cmp	r3, r2
 8000a78:	d273      	bcs.n	8000b62 <_malloc_r+0x46e>
 8000a7a:	6889      	ldr	r1, [r1, #8]
 8000a7c:	458c      	cmp	ip, r1
 8000a7e:	d1f7      	bne.n	8000a70 <_malloc_r+0x37c>
 8000a80:	f8dc 200c 	ldr.w	r2, [ip, #12]
 8000a84:	687b      	ldr	r3, [r7, #4]
 8000a86:	60e2      	str	r2, [r4, #12]
 8000a88:	f8c4 c008 	str.w	ip, [r4, #8]
 8000a8c:	6094      	str	r4, [r2, #8]
 8000a8e:	f8cc 400c 	str.w	r4, [ip, #12]
 8000a92:	e693      	b.n	80007bc <_malloc_r+0xc8>
 8000a94:	19a1      	adds	r1, r4, r6
 8000a96:	f046 0c01 	orr.w	ip, r6, #1
 8000a9a:	f042 0601 	orr.w	r6, r2, #1
 8000a9e:	f8c4 c004 	str.w	ip, [r4, #4]
 8000aa2:	4628      	mov	r0, r5
 8000aa4:	6179      	str	r1, [r7, #20]
 8000aa6:	6139      	str	r1, [r7, #16]
 8000aa8:	f8c1 e00c 	str.w	lr, [r1, #12]
 8000aac:	f8c1 e008 	str.w	lr, [r1, #8]
 8000ab0:	604e      	str	r6, [r1, #4]
 8000ab2:	50e2      	str	r2, [r4, r3]
 8000ab4:	f000 f9b2 	bl	8000e1c <__malloc_unlock>
 8000ab8:	3408      	adds	r4, #8
 8000aba:	e6e1      	b.n	8000880 <_malloc_r+0x18c>
 8000abc:	687b      	ldr	r3, [r7, #4]
 8000abe:	e67d      	b.n	80007bc <_malloc_r+0xc8>
 8000ac0:	f108 0801 	add.w	r8, r8, #1
 8000ac4:	f018 0f03 	tst.w	r8, #3
 8000ac8:	f10c 0c08 	add.w	ip, ip, #8
 8000acc:	f47f ae89 	bne.w	80007e2 <_malloc_r+0xee>
 8000ad0:	e02d      	b.n	8000b2e <_malloc_r+0x43a>
 8000ad2:	68dc      	ldr	r4, [r3, #12]
 8000ad4:	42a3      	cmp	r3, r4
 8000ad6:	bf08      	it	eq
 8000ad8:	3002      	addeq	r0, #2
 8000ada:	f43f ae42 	beq.w	8000762 <_malloc_r+0x6e>
 8000ade:	e6bf      	b.n	8000860 <_malloc_r+0x16c>
 8000ae0:	4419      	add	r1, r3
 8000ae2:	461c      	mov	r4, r3
 8000ae4:	684a      	ldr	r2, [r1, #4]
 8000ae6:	68db      	ldr	r3, [r3, #12]
 8000ae8:	f854 6f08 	ldr.w	r6, [r4, #8]!
 8000aec:	f042 0201 	orr.w	r2, r2, #1
 8000af0:	604a      	str	r2, [r1, #4]
 8000af2:	4628      	mov	r0, r5
 8000af4:	60f3      	str	r3, [r6, #12]
 8000af6:	609e      	str	r6, [r3, #8]
 8000af8:	f000 f990 	bl	8000e1c <__malloc_unlock>
 8000afc:	e6c0      	b.n	8000880 <_malloc_r+0x18c>
 8000afe:	f103 005c 	add.w	r0, r3, #92	; 0x5c
 8000b02:	f103 0e5b 	add.w	lr, r3, #91	; 0x5b
 8000b06:	00c3      	lsls	r3, r0, #3
 8000b08:	e616      	b.n	8000738 <_malloc_r+0x44>
 8000b0a:	099a      	lsrs	r2, r3, #6
 8000b0c:	f102 0139 	add.w	r1, r2, #57	; 0x39
 8000b10:	00c9      	lsls	r1, r1, #3
 8000b12:	3238      	adds	r2, #56	; 0x38
 8000b14:	e7a4      	b.n	8000a60 <_malloc_r+0x36c>
 8000b16:	42bc      	cmp	r4, r7
 8000b18:	d054      	beq.n	8000bc4 <_malloc_r+0x4d0>
 8000b1a:	68bc      	ldr	r4, [r7, #8]
 8000b1c:	6862      	ldr	r2, [r4, #4]
 8000b1e:	f022 0203 	bic.w	r2, r2, #3
 8000b22:	e765      	b.n	80009f0 <_malloc_r+0x2fc>
 8000b24:	f859 3908 	ldr.w	r3, [r9], #-8
 8000b28:	4599      	cmp	r9, r3
 8000b2a:	f040 8086 	bne.w	8000c3a <_malloc_r+0x546>
 8000b2e:	f010 0f03 	tst.w	r0, #3
 8000b32:	f100 30ff 	add.w	r0, r0, #4294967295
 8000b36:	d1f5      	bne.n	8000b24 <_malloc_r+0x430>
 8000b38:	687b      	ldr	r3, [r7, #4]
 8000b3a:	ea23 0304 	bic.w	r3, r3, r4
 8000b3e:	607b      	str	r3, [r7, #4]
 8000b40:	0064      	lsls	r4, r4, #1
 8000b42:	429c      	cmp	r4, r3
 8000b44:	f63f aecb 	bhi.w	80008de <_malloc_r+0x1ea>
 8000b48:	2c00      	cmp	r4, #0
 8000b4a:	f43f aec8 	beq.w	80008de <_malloc_r+0x1ea>
 8000b4e:	421c      	tst	r4, r3
 8000b50:	4640      	mov	r0, r8
 8000b52:	f47f ae42 	bne.w	80007da <_malloc_r+0xe6>
 8000b56:	0064      	lsls	r4, r4, #1
 8000b58:	421c      	tst	r4, r3
 8000b5a:	f100 0004 	add.w	r0, r0, #4
 8000b5e:	d0fa      	beq.n	8000b56 <_malloc_r+0x462>
 8000b60:	e63b      	b.n	80007da <_malloc_r+0xe6>
 8000b62:	468c      	mov	ip, r1
 8000b64:	e78c      	b.n	8000a80 <_malloc_r+0x38c>
 8000b66:	f5b3 7faa 	cmp.w	r3, #340	; 0x154
 8000b6a:	d815      	bhi.n	8000b98 <_malloc_r+0x4a4>
 8000b6c:	0bf3      	lsrs	r3, r6, #15
 8000b6e:	f103 0078 	add.w	r0, r3, #120	; 0x78
 8000b72:	f103 0e77 	add.w	lr, r3, #119	; 0x77
 8000b76:	00c3      	lsls	r3, r0, #3
 8000b78:	e5de      	b.n	8000738 <_malloc_r+0x44>
 8000b7a:	f44f 5280 	mov.w	r2, #4096	; 0x1000
 8000b7e:	e6f5      	b.n	800096c <_malloc_r+0x278>
 8000b80:	687b      	ldr	r3, [r7, #4]
 8000b82:	1092      	asrs	r2, r2, #2
 8000b84:	2101      	movs	r1, #1
 8000b86:	fa01 f202 	lsl.w	r2, r1, r2
 8000b8a:	4313      	orrs	r3, r2
 8000b8c:	607b      	str	r3, [r7, #4]
 8000b8e:	4662      	mov	r2, ip
 8000b90:	e779      	b.n	8000a86 <_malloc_r+0x392>
 8000b92:	2201      	movs	r2, #1
 8000b94:	605a      	str	r2, [r3, #4]
 8000b96:	e731      	b.n	80009fc <_malloc_r+0x308>
 8000b98:	f240 5254 	movw	r2, #1364	; 0x554
 8000b9c:	4293      	cmp	r3, r2
 8000b9e:	d823      	bhi.n	8000be8 <_malloc_r+0x4f4>
 8000ba0:	0cb3      	lsrs	r3, r6, #18
 8000ba2:	f103 007d 	add.w	r0, r3, #125	; 0x7d
 8000ba6:	f103 0e7c 	add.w	lr, r3, #124	; 0x7c
 8000baa:	00c3      	lsls	r3, r0, #3
 8000bac:	e5c4      	b.n	8000738 <_malloc_r+0x44>
 8000bae:	f108 0810 	add.w	r8, r8, #16
 8000bb2:	e6b4      	b.n	800091e <_malloc_r+0x22a>
 8000bb4:	2a54      	cmp	r2, #84	; 0x54
 8000bb6:	d829      	bhi.n	8000c0c <_malloc_r+0x518>
 8000bb8:	0b1a      	lsrs	r2, r3, #12
 8000bba:	f102 016f 	add.w	r1, r2, #111	; 0x6f
 8000bbe:	00c9      	lsls	r1, r1, #3
 8000bc0:	326e      	adds	r2, #110	; 0x6e
 8000bc2:	e74d      	b.n	8000a60 <_malloc_r+0x36c>
 8000bc4:	f8da 2004 	ldr.w	r2, [sl, #4]
 8000bc8:	4442      	add	r2, r8
 8000bca:	f8ca 2004 	str.w	r2, [sl, #4]
 8000bce:	e6b8      	b.n	8000942 <_malloc_r+0x24e>
 8000bd0:	f3cb 010b 	ubfx	r1, fp, #0, #12
 8000bd4:	2900      	cmp	r1, #0
 8000bd6:	f47f aeb4 	bne.w	8000942 <_malloc_r+0x24e>
 8000bda:	eb09 0308 	add.w	r3, r9, r8
 8000bde:	68b9      	ldr	r1, [r7, #8]
 8000be0:	f043 0301 	orr.w	r3, r3, #1
 8000be4:	604b      	str	r3, [r1, #4]
 8000be6:	e6f3      	b.n	80009d0 <_malloc_r+0x2dc>
 8000be8:	207f      	movs	r0, #127	; 0x7f
 8000bea:	f44f 737e 	mov.w	r3, #1016	; 0x3f8
 8000bee:	f04f 0e7e 	mov.w	lr, #126	; 0x7e
 8000bf2:	e5a1      	b.n	8000738 <_malloc_r+0x44>
 8000bf4:	f104 0108 	add.w	r1, r4, #8
 8000bf8:	4628      	mov	r0, r5
 8000bfa:	f003 f953 	bl	8003ea4 <_free_r>
 8000bfe:	f8da 2004 	ldr.w	r2, [sl, #4]
 8000c02:	e6e5      	b.n	80009d0 <_malloc_r+0x2dc>
 8000c04:	2101      	movs	r1, #1
 8000c06:	f04f 0800 	mov.w	r8, #0
 8000c0a:	e6c1      	b.n	8000990 <_malloc_r+0x29c>
 8000c0c:	f5b2 7faa 	cmp.w	r2, #340	; 0x154
 8000c10:	d805      	bhi.n	8000c1e <_malloc_r+0x52a>
 8000c12:	0bda      	lsrs	r2, r3, #15
 8000c14:	f102 0178 	add.w	r1, r2, #120	; 0x78
 8000c18:	00c9      	lsls	r1, r1, #3
 8000c1a:	3277      	adds	r2, #119	; 0x77
 8000c1c:	e720      	b.n	8000a60 <_malloc_r+0x36c>
 8000c1e:	f240 5154 	movw	r1, #1364	; 0x554
 8000c22:	428a      	cmp	r2, r1
 8000c24:	d805      	bhi.n	8000c32 <_malloc_r+0x53e>
 8000c26:	0c9a      	lsrs	r2, r3, #18
 8000c28:	f102 017d 	add.w	r1, r2, #125	; 0x7d
 8000c2c:	00c9      	lsls	r1, r1, #3
 8000c2e:	327c      	adds	r2, #124	; 0x7c
 8000c30:	e716      	b.n	8000a60 <_malloc_r+0x36c>
 8000c32:	f44f 717e 	mov.w	r1, #1016	; 0x3f8
 8000c36:	227e      	movs	r2, #126	; 0x7e
 8000c38:	e712      	b.n	8000a60 <_malloc_r+0x36c>
 8000c3a:	687b      	ldr	r3, [r7, #4]
 8000c3c:	e780      	b.n	8000b40 <_malloc_r+0x44c>
 8000c3e:	08f0      	lsrs	r0, r6, #3
 8000c40:	f106 0308 	add.w	r3, r6, #8
 8000c44:	e604      	b.n	8000850 <_malloc_r+0x15c>
 8000c46:	bf00      	nop

08000c48 <memcpy>:
 8000c48:	4684      	mov	ip, r0
 8000c4a:	ea41 0300 	orr.w	r3, r1, r0
 8000c4e:	f013 0303 	ands.w	r3, r3, #3
 8000c52:	d16d      	bne.n	8000d30 <memcpy+0xe8>
 8000c54:	3a40      	subs	r2, #64	; 0x40
 8000c56:	d341      	bcc.n	8000cdc <memcpy+0x94>
 8000c58:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c5c:	f840 3b04 	str.w	r3, [r0], #4
 8000c60:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c64:	f840 3b04 	str.w	r3, [r0], #4
 8000c68:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c6c:	f840 3b04 	str.w	r3, [r0], #4
 8000c70:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c74:	f840 3b04 	str.w	r3, [r0], #4
 8000c78:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c7c:	f840 3b04 	str.w	r3, [r0], #4
 8000c80:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c84:	f840 3b04 	str.w	r3, [r0], #4
 8000c88:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c8c:	f840 3b04 	str.w	r3, [r0], #4
 8000c90:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c94:	f840 3b04 	str.w	r3, [r0], #4
 8000c98:	f851 3b04 	ldr.w	r3, [r1], #4
 8000c9c:	f840 3b04 	str.w	r3, [r0], #4
 8000ca0:	f851 3b04 	ldr.w	r3, [r1], #4
 8000ca4:	f840 3b04 	str.w	r3, [r0], #4
 8000ca8:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cac:	f840 3b04 	str.w	r3, [r0], #4
 8000cb0:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cb4:	f840 3b04 	str.w	r3, [r0], #4
 8000cb8:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cbc:	f840 3b04 	str.w	r3, [r0], #4
 8000cc0:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cc4:	f840 3b04 	str.w	r3, [r0], #4
 8000cc8:	f851 3b04 	ldr.w	r3, [r1], #4
 8000ccc:	f840 3b04 	str.w	r3, [r0], #4
 8000cd0:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cd4:	f840 3b04 	str.w	r3, [r0], #4
 8000cd8:	3a40      	subs	r2, #64	; 0x40
 8000cda:	d2bd      	bcs.n	8000c58 <memcpy+0x10>
 8000cdc:	3230      	adds	r2, #48	; 0x30
 8000cde:	d311      	bcc.n	8000d04 <memcpy+0xbc>
 8000ce0:	f851 3b04 	ldr.w	r3, [r1], #4
 8000ce4:	f840 3b04 	str.w	r3, [r0], #4
 8000ce8:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cec:	f840 3b04 	str.w	r3, [r0], #4
 8000cf0:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cf4:	f840 3b04 	str.w	r3, [r0], #4
 8000cf8:	f851 3b04 	ldr.w	r3, [r1], #4
 8000cfc:	f840 3b04 	str.w	r3, [r0], #4
 8000d00:	3a10      	subs	r2, #16
 8000d02:	d2ed      	bcs.n	8000ce0 <memcpy+0x98>
 8000d04:	320c      	adds	r2, #12
 8000d06:	d305      	bcc.n	8000d14 <memcpy+0xcc>
 8000d08:	f851 3b04 	ldr.w	r3, [r1], #4
 8000d0c:	f840 3b04 	str.w	r3, [r0], #4
 8000d10:	3a04      	subs	r2, #4
 8000d12:	d2f9      	bcs.n	8000d08 <memcpy+0xc0>
 8000d14:	3204      	adds	r2, #4
 8000d16:	d008      	beq.n	8000d2a <memcpy+0xe2>
 8000d18:	07d2      	lsls	r2, r2, #31
 8000d1a:	bf1c      	itt	ne
 8000d1c:	f811 3b01 	ldrbne.w	r3, [r1], #1
 8000d20:	f800 3b01 	strbne.w	r3, [r0], #1
 8000d24:	d301      	bcc.n	8000d2a <memcpy+0xe2>
 8000d26:	880b      	ldrh	r3, [r1, #0]
 8000d28:	8003      	strh	r3, [r0, #0]
 8000d2a:	4660      	mov	r0, ip
 8000d2c:	4770      	bx	lr
 8000d2e:	bf00      	nop
 8000d30:	2a08      	cmp	r2, #8
 8000d32:	d313      	bcc.n	8000d5c <memcpy+0x114>
 8000d34:	078b      	lsls	r3, r1, #30
 8000d36:	d08d      	beq.n	8000c54 <memcpy+0xc>
 8000d38:	f010 0303 	ands.w	r3, r0, #3
 8000d3c:	d08a      	beq.n	8000c54 <memcpy+0xc>
 8000d3e:	f1c3 0304 	rsb	r3, r3, #4
 8000d42:	1ad2      	subs	r2, r2, r3
 8000d44:	07db      	lsls	r3, r3, #31
 8000d46:	bf1c      	itt	ne
 8000d48:	f811 3b01 	ldrbne.w	r3, [r1], #1
 8000d4c:	f800 3b01 	strbne.w	r3, [r0], #1
 8000d50:	d380      	bcc.n	8000c54 <memcpy+0xc>
 8000d52:	f831 3b02 	ldrh.w	r3, [r1], #2
 8000d56:	f820 3b02 	strh.w	r3, [r0], #2
 8000d5a:	e77b      	b.n	8000c54 <memcpy+0xc>
 8000d5c:	3a04      	subs	r2, #4
 8000d5e:	d3d9      	bcc.n	8000d14 <memcpy+0xcc>
 8000d60:	3a01      	subs	r2, #1
 8000d62:	f811 3b01 	ldrb.w	r3, [r1], #1
 8000d66:	f800 3b01 	strb.w	r3, [r0], #1
 8000d6a:	d2f9      	bcs.n	8000d60 <memcpy+0x118>
 8000d6c:	780b      	ldrb	r3, [r1, #0]
 8000d6e:	7003      	strb	r3, [r0, #0]
 8000d70:	784b      	ldrb	r3, [r1, #1]
 8000d72:	7043      	strb	r3, [r0, #1]
 8000d74:	788b      	ldrb	r3, [r1, #2]
 8000d76:	7083      	strb	r3, [r0, #2]
 8000d78:	4660      	mov	r0, ip
 8000d7a:	4770      	bx	lr

08000d7c <memset>:
 8000d7c:	b470      	push	{r4, r5, r6}
 8000d7e:	0786      	lsls	r6, r0, #30
 8000d80:	d046      	beq.n	8000e10 <memset+0x94>
 8000d82:	1e54      	subs	r4, r2, #1
 8000d84:	2a00      	cmp	r2, #0
 8000d86:	d041      	beq.n	8000e0c <memset+0x90>
 8000d88:	b2ca      	uxtb	r2, r1
 8000d8a:	4603      	mov	r3, r0
 8000d8c:	e002      	b.n	8000d94 <memset+0x18>
 8000d8e:	f114 34ff 	adds.w	r4, r4, #4294967295
 8000d92:	d33b      	bcc.n	8000e0c <memset+0x90>
 8000d94:	f803 2b01 	strb.w	r2, [r3], #1
 8000d98:	079d      	lsls	r5, r3, #30
 8000d9a:	d1f8      	bne.n	8000d8e <memset+0x12>
 8000d9c:	2c03      	cmp	r4, #3
 8000d9e:	d92e      	bls.n	8000dfe <memset+0x82>
 8000da0:	b2cd      	uxtb	r5, r1
 8000da2:	ea45 2505 	orr.w	r5, r5, r5, lsl #8
 8000da6:	2c0f      	cmp	r4, #15
 8000da8:	ea45 4505 	orr.w	r5, r5, r5, lsl #16
 8000dac:	d919      	bls.n	8000de2 <memset+0x66>
 8000dae:	f103 0210 	add.w	r2, r3, #16
 8000db2:	4626      	mov	r6, r4
 8000db4:	3e10      	subs	r6, #16
 8000db6:	2e0f      	cmp	r6, #15
 8000db8:	f842 5c10 	str.w	r5, [r2, #-16]
 8000dbc:	f842 5c0c 	str.w	r5, [r2, #-12]
 8000dc0:	f842 5c08 	str.w	r5, [r2, #-8]
 8000dc4:	f842 5c04 	str.w	r5, [r2, #-4]
 8000dc8:	f102 0210 	add.w	r2, r2, #16
 8000dcc:	d8f2      	bhi.n	8000db4 <memset+0x38>
 8000dce:	f1a4 0210 	sub.w	r2, r4, #16
 8000dd2:	f022 020f 	bic.w	r2, r2, #15
 8000dd6:	f004 040f 	and.w	r4, r4, #15
 8000dda:	3210      	adds	r2, #16
 8000ddc:	2c03      	cmp	r4, #3
 8000dde:	4413      	add	r3, r2
 8000de0:	d90d      	bls.n	8000dfe <memset+0x82>
 8000de2:	461e      	mov	r6, r3
 8000de4:	4622      	mov	r2, r4
 8000de6:	3a04      	subs	r2, #4
 8000de8:	2a03      	cmp	r2, #3
 8000dea:	f846 5b04 	str.w	r5, [r6], #4
 8000dee:	d8fa      	bhi.n	8000de6 <memset+0x6a>
 8000df0:	1f22      	subs	r2, r4, #4
 8000df2:	f022 0203 	bic.w	r2, r2, #3
 8000df6:	3204      	adds	r2, #4
 8000df8:	4413      	add	r3, r2
 8000dfa:	f004 0403 	and.w	r4, r4, #3
 8000dfe:	b12c      	cbz	r4, 8000e0c <memset+0x90>
 8000e00:	b2c9      	uxtb	r1, r1
 8000e02:	441c      	add	r4, r3
 8000e04:	f803 1b01 	strb.w	r1, [r3], #1
 8000e08:	429c      	cmp	r4, r3
 8000e0a:	d1fb      	bne.n	8000e04 <memset+0x88>
 8000e0c:	bc70      	pop	{r4, r5, r6}
 8000e0e:	4770      	bx	lr
 8000e10:	4614      	mov	r4, r2
 8000e12:	4603      	mov	r3, r0
 8000e14:	e7c2      	b.n	8000d9c <memset+0x20>
 8000e16:	bf00      	nop

08000e18 <__malloc_lock>:
 8000e18:	4770      	bx	lr
 8000e1a:	bf00      	nop

08000e1c <__malloc_unlock>:
 8000e1c:	4770      	bx	lr
 8000e1e:	bf00      	nop

08000e20 <cleanup_glue>:
 8000e20:	b538      	push	{r3, r4, r5, lr}
 8000e22:	460c      	mov	r4, r1
 8000e24:	6809      	ldr	r1, [r1, #0]
 8000e26:	4605      	mov	r5, r0
 8000e28:	b109      	cbz	r1, 8000e2e <cleanup_glue+0xe>
 8000e2a:	f7ff fff9 	bl	8000e20 <cleanup_glue>
 8000e2e:	4621      	mov	r1, r4
 8000e30:	4628      	mov	r0, r5
 8000e32:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
 8000e36:	f003 b835 	b.w	8003ea4 <_free_r>
 8000e3a:	bf00      	nop

08000e3c <_reclaim_reent>:
 8000e3c:	4b20      	ldr	r3, [pc, #128]	; (8000ec0 <_reclaim_reent+0x84>)
 8000e3e:	681b      	ldr	r3, [r3, #0]
 8000e40:	4283      	cmp	r3, r0
 8000e42:	d03c      	beq.n	8000ebe <_reclaim_reent+0x82>
 8000e44:	6cc3      	ldr	r3, [r0, #76]	; 0x4c
 8000e46:	b570      	push	{r4, r5, r6, lr}
 8000e48:	4605      	mov	r5, r0
 8000e4a:	b18b      	cbz	r3, 8000e70 <_reclaim_reent+0x34>
 8000e4c:	2600      	movs	r6, #0
 8000e4e:	5999      	ldr	r1, [r3, r6]
 8000e50:	b139      	cbz	r1, 8000e62 <_reclaim_reent+0x26>
 8000e52:	680c      	ldr	r4, [r1, #0]
 8000e54:	4628      	mov	r0, r5
 8000e56:	f003 f825 	bl	8003ea4 <_free_r>
 8000e5a:	4621      	mov	r1, r4
 8000e5c:	2c00      	cmp	r4, #0
 8000e5e:	d1f8      	bne.n	8000e52 <_reclaim_reent+0x16>
 8000e60:	6ceb      	ldr	r3, [r5, #76]	; 0x4c
 8000e62:	3604      	adds	r6, #4
 8000e64:	2e80      	cmp	r6, #128	; 0x80
 8000e66:	d1f2      	bne.n	8000e4e <_reclaim_reent+0x12>
 8000e68:	4619      	mov	r1, r3
 8000e6a:	4628      	mov	r0, r5
 8000e6c:	f003 f81a 	bl	8003ea4 <_free_r>
 8000e70:	6c29      	ldr	r1, [r5, #64]	; 0x40
 8000e72:	b111      	cbz	r1, 8000e7a <_reclaim_reent+0x3e>
 8000e74:	4628      	mov	r0, r5
 8000e76:	f003 f815 	bl	8003ea4 <_free_r>
 8000e7a:	f8d5 1148 	ldr.w	r1, [r5, #328]	; 0x148
 8000e7e:	b151      	cbz	r1, 8000e96 <_reclaim_reent+0x5a>
 8000e80:	f505 76a6 	add.w	r6, r5, #332	; 0x14c
 8000e84:	42b1      	cmp	r1, r6
 8000e86:	d006      	beq.n	8000e96 <_reclaim_reent+0x5a>
 8000e88:	680c      	ldr	r4, [r1, #0]
 8000e8a:	4628      	mov	r0, r5
 8000e8c:	f003 f80a 	bl	8003ea4 <_free_r>
 8000e90:	42a6      	cmp	r6, r4
 8000e92:	4621      	mov	r1, r4
 8000e94:	d1f8      	bne.n	8000e88 <_reclaim_reent+0x4c>
 8000e96:	6d69      	ldr	r1, [r5, #84]	; 0x54
 8000e98:	b111      	cbz	r1, 8000ea0 <_reclaim_reent+0x64>
 8000e9a:	4628      	mov	r0, r5
 8000e9c:	f003 f802 	bl	8003ea4 <_free_r>
 8000ea0:	6bab      	ldr	r3, [r5, #56]	; 0x38
 8000ea2:	b903      	cbnz	r3, 8000ea6 <_reclaim_reent+0x6a>
 8000ea4:	bd70      	pop	{r4, r5, r6, pc}
 8000ea6:	6beb      	ldr	r3, [r5, #60]	; 0x3c
 8000ea8:	4628      	mov	r0, r5
 8000eaa:	4798      	blx	r3
 8000eac:	f8d5 12e0 	ldr.w	r1, [r5, #736]	; 0x2e0
 8000eb0:	2900      	cmp	r1, #0
 8000eb2:	d0f7      	beq.n	8000ea4 <_reclaim_reent+0x68>
 8000eb4:	4628      	mov	r0, r5
 8000eb6:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
 8000eba:	f7ff bfb1 	b.w	8000e20 <cleanup_glue>
 8000ebe:	4770      	bx	lr
 8000ec0:	20000000 	.word	0x20000000

08000ec4 <_sbrk_r>:
 8000ec4:	b538      	push	{r3, r4, r5, lr}
 8000ec6:	4c07      	ldr	r4, [pc, #28]	; (8000ee4 <_sbrk_r+0x20>)
 8000ec8:	2300      	movs	r3, #0
 8000eca:	4605      	mov	r5, r0
 8000ecc:	4608      	mov	r0, r1
 8000ece:	6023      	str	r3, [r4, #0]
 8000ed0:	f000 faf6 	bl	80014c0 <_sbrk>
 8000ed4:	1c43      	adds	r3, r0, #1
 8000ed6:	d000      	beq.n	8000eda <_sbrk_r+0x16>
 8000ed8:	bd38      	pop	{r3, r4, r5, pc}
 8000eda:	6823      	ldr	r3, [r4, #0]
 8000edc:	2b00      	cmp	r3, #0
 8000ede:	d0fb      	beq.n	8000ed8 <_sbrk_r+0x14>
 8000ee0:	602b      	str	r3, [r5, #0]
 8000ee2:	bd38      	pop	{r3, r4, r5, pc}
 8000ee4:	20006150 	.word	0x20006150

08000ee8 <__register_exitproc>:
 8000ee8:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 8000eec:	4c25      	ldr	r4, [pc, #148]	; (8000f84 <__register_exitproc+0x9c>)
 8000eee:	6825      	ldr	r5, [r4, #0]
 8000ef0:	f8d5 4148 	ldr.w	r4, [r5, #328]	; 0x148
 8000ef4:	4606      	mov	r6, r0
 8000ef6:	4688      	mov	r8, r1
 8000ef8:	4692      	mov	sl, r2
 8000efa:	4699      	mov	r9, r3
 8000efc:	b3c4      	cbz	r4, 8000f70 <__register_exitproc+0x88>
 8000efe:	6860      	ldr	r0, [r4, #4]
 8000f00:	281f      	cmp	r0, #31
 8000f02:	dc17      	bgt.n	8000f34 <__register_exitproc+0x4c>
 8000f04:	1c43      	adds	r3, r0, #1
 8000f06:	b176      	cbz	r6, 8000f26 <__register_exitproc+0x3e>
 8000f08:	eb04 0580 	add.w	r5, r4, r0, lsl #2
 8000f0c:	2201      	movs	r2, #1
 8000f0e:	f8c5 a088 	str.w	sl, [r5, #136]	; 0x88
 8000f12:	f8d4 1188 	ldr.w	r1, [r4, #392]	; 0x188
 8000f16:	4082      	lsls	r2, r0
 8000f18:	4311      	orrs	r1, r2
 8000f1a:	2e02      	cmp	r6, #2
 8000f1c:	f8c4 1188 	str.w	r1, [r4, #392]	; 0x188
 8000f20:	f8c5 9108 	str.w	r9, [r5, #264]	; 0x108
 8000f24:	d01e      	beq.n	8000f64 <__register_exitproc+0x7c>
 8000f26:	3002      	adds	r0, #2
 8000f28:	6063      	str	r3, [r4, #4]
 8000f2a:	f844 8020 	str.w	r8, [r4, r0, lsl #2]
 8000f2e:	2000      	movs	r0, #0
 8000f30:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 8000f34:	4b14      	ldr	r3, [pc, #80]	; (8000f88 <__register_exitproc+0xa0>)
 8000f36:	b303      	cbz	r3, 8000f7a <__register_exitproc+0x92>
 8000f38:	f44f 70c8 	mov.w	r0, #400	; 0x190
 8000f3c:	f7ff fbca 	bl	80006d4 <malloc>
 8000f40:	4604      	mov	r4, r0
 8000f42:	b1d0      	cbz	r0, 8000f7a <__register_exitproc+0x92>
 8000f44:	f8d5 3148 	ldr.w	r3, [r5, #328]	; 0x148
 8000f48:	2700      	movs	r7, #0
 8000f4a:	e880 0088 	stmia.w	r0, {r3, r7}
 8000f4e:	f8c5 4148 	str.w	r4, [r5, #328]	; 0x148
 8000f52:	4638      	mov	r0, r7
 8000f54:	2301      	movs	r3, #1
 8000f56:	f8c4 7188 	str.w	r7, [r4, #392]	; 0x188
 8000f5a:	f8c4 718c 	str.w	r7, [r4, #396]	; 0x18c
 8000f5e:	2e00      	cmp	r6, #0
 8000f60:	d0e1      	beq.n	8000f26 <__register_exitproc+0x3e>
 8000f62:	e7d1      	b.n	8000f08 <__register_exitproc+0x20>
 8000f64:	f8d4 118c 	ldr.w	r1, [r4, #396]	; 0x18c
 8000f68:	430a      	orrs	r2, r1
 8000f6a:	f8c4 218c 	str.w	r2, [r4, #396]	; 0x18c
 8000f6e:	e7da      	b.n	8000f26 <__register_exitproc+0x3e>
 8000f70:	f505 74a6 	add.w	r4, r5, #332	; 0x14c
 8000f74:	f8c5 4148 	str.w	r4, [r5, #328]	; 0x148
 8000f78:	e7c1      	b.n	8000efe <__register_exitproc+0x16>
 8000f7a:	f04f 30ff 	mov.w	r0, #4294967295
 8000f7e:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 8000f82:	bf00      	nop
 8000f84:	080098d8 	.word	0x080098d8
 8000f88:	080006d5 	.word	0x080006d5

08000f8c <__aeabi_dmul>:
 8000f8c:	b570      	push	{r4, r5, r6, lr}
 8000f8e:	f04f 0cff 	mov.w	ip, #255	; 0xff
 8000f92:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 8000f96:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 8000f9a:	bf1d      	ittte	ne
 8000f9c:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 8000fa0:	ea94 0f0c 	teqne	r4, ip
 8000fa4:	ea95 0f0c 	teqne	r5, ip
 8000fa8:	f000 f8de 	bleq	8001168 <__aeabi_dmul+0x1dc>
 8000fac:	442c      	add	r4, r5
 8000fae:	ea81 0603 	eor.w	r6, r1, r3
 8000fb2:	ea21 514c 	bic.w	r1, r1, ip, lsl #21
 8000fb6:	ea23 534c 	bic.w	r3, r3, ip, lsl #21
 8000fba:	ea50 3501 	orrs.w	r5, r0, r1, lsl #12
 8000fbe:	bf18      	it	ne
 8000fc0:	ea52 3503 	orrsne.w	r5, r2, r3, lsl #12
 8000fc4:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8000fc8:	f443 1380 	orr.w	r3, r3, #1048576	; 0x100000
 8000fcc:	d038      	beq.n	8001040 <__aeabi_dmul+0xb4>
 8000fce:	fba0 ce02 	umull	ip, lr, r0, r2
 8000fd2:	f04f 0500 	mov.w	r5, #0
 8000fd6:	fbe1 e502 	umlal	lr, r5, r1, r2
 8000fda:	f006 4200 	and.w	r2, r6, #2147483648	; 0x80000000
 8000fde:	fbe0 e503 	umlal	lr, r5, r0, r3
 8000fe2:	f04f 0600 	mov.w	r6, #0
 8000fe6:	fbe1 5603 	umlal	r5, r6, r1, r3
 8000fea:	f09c 0f00 	teq	ip, #0
 8000fee:	bf18      	it	ne
 8000ff0:	f04e 0e01 	orrne.w	lr, lr, #1
 8000ff4:	f1a4 04ff 	sub.w	r4, r4, #255	; 0xff
 8000ff8:	f5b6 7f00 	cmp.w	r6, #512	; 0x200
 8000ffc:	f564 7440 	sbc.w	r4, r4, #768	; 0x300
 8001000:	d204      	bcs.n	800100c <__aeabi_dmul+0x80>
 8001002:	ea5f 0e4e 	movs.w	lr, lr, lsl #1
 8001006:	416d      	adcs	r5, r5
 8001008:	eb46 0606 	adc.w	r6, r6, r6
 800100c:	ea42 21c6 	orr.w	r1, r2, r6, lsl #11
 8001010:	ea41 5155 	orr.w	r1, r1, r5, lsr #21
 8001014:	ea4f 20c5 	mov.w	r0, r5, lsl #11
 8001018:	ea40 505e 	orr.w	r0, r0, lr, lsr #21
 800101c:	ea4f 2ece 	mov.w	lr, lr, lsl #11
 8001020:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 8001024:	bf88      	it	hi
 8001026:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 800102a:	d81e      	bhi.n	800106a <__aeabi_dmul+0xde>
 800102c:	f1be 4f00 	cmp.w	lr, #2147483648	; 0x80000000
 8001030:	bf08      	it	eq
 8001032:	ea5f 0e50 	movseq.w	lr, r0, lsr #1
 8001036:	f150 0000 	adcs.w	r0, r0, #0
 800103a:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 800103e:	bd70      	pop	{r4, r5, r6, pc}
 8001040:	f006 4600 	and.w	r6, r6, #2147483648	; 0x80000000
 8001044:	ea46 0101 	orr.w	r1, r6, r1
 8001048:	ea40 0002 	orr.w	r0, r0, r2
 800104c:	ea81 0103 	eor.w	r1, r1, r3
 8001050:	ebb4 045c 	subs.w	r4, r4, ip, lsr #1
 8001054:	bfc2      	ittt	gt
 8001056:	ebd4 050c 	rsbsgt	r5, r4, ip
 800105a:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 800105e:	bd70      	popgt	{r4, r5, r6, pc}
 8001060:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8001064:	f04f 0e00 	mov.w	lr, #0
 8001068:	3c01      	subs	r4, #1
 800106a:	f300 80ab 	bgt.w	80011c4 <__aeabi_dmul+0x238>
 800106e:	f114 0f36 	cmn.w	r4, #54	; 0x36
 8001072:	bfde      	ittt	le
 8001074:	2000      	movle	r0, #0
 8001076:	f001 4100 	andle.w	r1, r1, #2147483648	; 0x80000000
 800107a:	bd70      	pople	{r4, r5, r6, pc}
 800107c:	f1c4 0400 	rsb	r4, r4, #0
 8001080:	3c20      	subs	r4, #32
 8001082:	da35      	bge.n	80010f0 <__aeabi_dmul+0x164>
 8001084:	340c      	adds	r4, #12
 8001086:	dc1b      	bgt.n	80010c0 <__aeabi_dmul+0x134>
 8001088:	f104 0414 	add.w	r4, r4, #20
 800108c:	f1c4 0520 	rsb	r5, r4, #32
 8001090:	fa00 f305 	lsl.w	r3, r0, r5
 8001094:	fa20 f004 	lsr.w	r0, r0, r4
 8001098:	fa01 f205 	lsl.w	r2, r1, r5
 800109c:	ea40 0002 	orr.w	r0, r0, r2
 80010a0:	f001 4200 	and.w	r2, r1, #2147483648	; 0x80000000
 80010a4:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 80010a8:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 80010ac:	fa21 f604 	lsr.w	r6, r1, r4
 80010b0:	eb42 0106 	adc.w	r1, r2, r6
 80010b4:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 80010b8:	bf08      	it	eq
 80010ba:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 80010be:	bd70      	pop	{r4, r5, r6, pc}
 80010c0:	f1c4 040c 	rsb	r4, r4, #12
 80010c4:	f1c4 0520 	rsb	r5, r4, #32
 80010c8:	fa00 f304 	lsl.w	r3, r0, r4
 80010cc:	fa20 f005 	lsr.w	r0, r0, r5
 80010d0:	fa01 f204 	lsl.w	r2, r1, r4
 80010d4:	ea40 0002 	orr.w	r0, r0, r2
 80010d8:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 80010dc:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 80010e0:	f141 0100 	adc.w	r1, r1, #0
 80010e4:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 80010e8:	bf08      	it	eq
 80010ea:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 80010ee:	bd70      	pop	{r4, r5, r6, pc}
 80010f0:	f1c4 0520 	rsb	r5, r4, #32
 80010f4:	fa00 f205 	lsl.w	r2, r0, r5
 80010f8:	ea4e 0e02 	orr.w	lr, lr, r2
 80010fc:	fa20 f304 	lsr.w	r3, r0, r4
 8001100:	fa01 f205 	lsl.w	r2, r1, r5
 8001104:	ea43 0302 	orr.w	r3, r3, r2
 8001108:	fa21 f004 	lsr.w	r0, r1, r4
 800110c:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8001110:	fa21 f204 	lsr.w	r2, r1, r4
 8001114:	ea20 0002 	bic.w	r0, r0, r2
 8001118:	eb00 70d3 	add.w	r0, r0, r3, lsr #31
 800111c:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 8001120:	bf08      	it	eq
 8001122:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 8001126:	bd70      	pop	{r4, r5, r6, pc}
 8001128:	f094 0f00 	teq	r4, #0
 800112c:	d10f      	bne.n	800114e <__aeabi_dmul+0x1c2>
 800112e:	f001 4600 	and.w	r6, r1, #2147483648	; 0x80000000
 8001132:	0040      	lsls	r0, r0, #1
 8001134:	eb41 0101 	adc.w	r1, r1, r1
 8001138:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 800113c:	bf08      	it	eq
 800113e:	3c01      	subeq	r4, #1
 8001140:	d0f7      	beq.n	8001132 <__aeabi_dmul+0x1a6>
 8001142:	ea41 0106 	orr.w	r1, r1, r6
 8001146:	f095 0f00 	teq	r5, #0
 800114a:	bf18      	it	ne
 800114c:	4770      	bxne	lr
 800114e:	f003 4600 	and.w	r6, r3, #2147483648	; 0x80000000
 8001152:	0052      	lsls	r2, r2, #1
 8001154:	eb43 0303 	adc.w	r3, r3, r3
 8001158:	f413 1f80 	tst.w	r3, #1048576	; 0x100000
 800115c:	bf08      	it	eq
 800115e:	3d01      	subeq	r5, #1
 8001160:	d0f7      	beq.n	8001152 <__aeabi_dmul+0x1c6>
 8001162:	ea43 0306 	orr.w	r3, r3, r6
 8001166:	4770      	bx	lr
 8001168:	ea94 0f0c 	teq	r4, ip
 800116c:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 8001170:	bf18      	it	ne
 8001172:	ea95 0f0c 	teqne	r5, ip
 8001176:	d00c      	beq.n	8001192 <__aeabi_dmul+0x206>
 8001178:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 800117c:	bf18      	it	ne
 800117e:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 8001182:	d1d1      	bne.n	8001128 <__aeabi_dmul+0x19c>
 8001184:	ea81 0103 	eor.w	r1, r1, r3
 8001188:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 800118c:	f04f 0000 	mov.w	r0, #0
 8001190:	bd70      	pop	{r4, r5, r6, pc}
 8001192:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 8001196:	bf06      	itte	eq
 8001198:	4610      	moveq	r0, r2
 800119a:	4619      	moveq	r1, r3
 800119c:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 80011a0:	d019      	beq.n	80011d6 <__aeabi_dmul+0x24a>
 80011a2:	ea94 0f0c 	teq	r4, ip
 80011a6:	d102      	bne.n	80011ae <__aeabi_dmul+0x222>
 80011a8:	ea50 3601 	orrs.w	r6, r0, r1, lsl #12
 80011ac:	d113      	bne.n	80011d6 <__aeabi_dmul+0x24a>
 80011ae:	ea95 0f0c 	teq	r5, ip
 80011b2:	d105      	bne.n	80011c0 <__aeabi_dmul+0x234>
 80011b4:	ea52 3603 	orrs.w	r6, r2, r3, lsl #12
 80011b8:	bf1c      	itt	ne
 80011ba:	4610      	movne	r0, r2
 80011bc:	4619      	movne	r1, r3
 80011be:	d10a      	bne.n	80011d6 <__aeabi_dmul+0x24a>
 80011c0:	ea81 0103 	eor.w	r1, r1, r3
 80011c4:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 80011c8:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 80011cc:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 80011d0:	f04f 0000 	mov.w	r0, #0
 80011d4:	bd70      	pop	{r4, r5, r6, pc}
 80011d6:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 80011da:	f441 0178 	orr.w	r1, r1, #16252928	; 0xf80000
 80011de:	bd70      	pop	{r4, r5, r6, pc}

080011e0 <__aeabi_ddiv>:
 80011e0:	b570      	push	{r4, r5, r6, lr}
 80011e2:	f04f 0cff 	mov.w	ip, #255	; 0xff
 80011e6:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 80011ea:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 80011ee:	bf1d      	ittte	ne
 80011f0:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 80011f4:	ea94 0f0c 	teqne	r4, ip
 80011f8:	ea95 0f0c 	teqne	r5, ip
 80011fc:	f000 f8a7 	bleq	800134e <__aeabi_ddiv+0x16e>
 8001200:	eba4 0405 	sub.w	r4, r4, r5
 8001204:	ea81 0e03 	eor.w	lr, r1, r3
 8001208:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 800120c:	ea4f 3101 	mov.w	r1, r1, lsl #12
 8001210:	f000 8088 	beq.w	8001324 <__aeabi_ddiv+0x144>
 8001214:	ea4f 3303 	mov.w	r3, r3, lsl #12
 8001218:	f04f 5580 	mov.w	r5, #268435456	; 0x10000000
 800121c:	ea45 1313 	orr.w	r3, r5, r3, lsr #4
 8001220:	ea43 6312 	orr.w	r3, r3, r2, lsr #24
 8001224:	ea4f 2202 	mov.w	r2, r2, lsl #8
 8001228:	ea45 1511 	orr.w	r5, r5, r1, lsr #4
 800122c:	ea45 6510 	orr.w	r5, r5, r0, lsr #24
 8001230:	ea4f 2600 	mov.w	r6, r0, lsl #8
 8001234:	f00e 4100 	and.w	r1, lr, #2147483648	; 0x80000000
 8001238:	429d      	cmp	r5, r3
 800123a:	bf08      	it	eq
 800123c:	4296      	cmpeq	r6, r2
 800123e:	f144 04fd 	adc.w	r4, r4, #253	; 0xfd
 8001242:	f504 7440 	add.w	r4, r4, #768	; 0x300
 8001246:	d202      	bcs.n	800124e <__aeabi_ddiv+0x6e>
 8001248:	085b      	lsrs	r3, r3, #1
 800124a:	ea4f 0232 	mov.w	r2, r2, rrx
 800124e:	1ab6      	subs	r6, r6, r2
 8001250:	eb65 0503 	sbc.w	r5, r5, r3
 8001254:	085b      	lsrs	r3, r3, #1
 8001256:	ea4f 0232 	mov.w	r2, r2, rrx
 800125a:	f44f 1080 	mov.w	r0, #1048576	; 0x100000
 800125e:	f44f 2c00 	mov.w	ip, #524288	; 0x80000
 8001262:	ebb6 0e02 	subs.w	lr, r6, r2
 8001266:	eb75 0e03 	sbcs.w	lr, r5, r3
 800126a:	bf22      	ittt	cs
 800126c:	1ab6      	subcs	r6, r6, r2
 800126e:	4675      	movcs	r5, lr
 8001270:	ea40 000c 	orrcs.w	r0, r0, ip
 8001274:	085b      	lsrs	r3, r3, #1
 8001276:	ea4f 0232 	mov.w	r2, r2, rrx
 800127a:	ebb6 0e02 	subs.w	lr, r6, r2
 800127e:	eb75 0e03 	sbcs.w	lr, r5, r3
 8001282:	bf22      	ittt	cs
 8001284:	1ab6      	subcs	r6, r6, r2
 8001286:	4675      	movcs	r5, lr
 8001288:	ea40 005c 	orrcs.w	r0, r0, ip, lsr #1
 800128c:	085b      	lsrs	r3, r3, #1
 800128e:	ea4f 0232 	mov.w	r2, r2, rrx
 8001292:	ebb6 0e02 	subs.w	lr, r6, r2
 8001296:	eb75 0e03 	sbcs.w	lr, r5, r3
 800129a:	bf22      	ittt	cs
 800129c:	1ab6      	subcs	r6, r6, r2
 800129e:	4675      	movcs	r5, lr
 80012a0:	ea40 009c 	orrcs.w	r0, r0, ip, lsr #2
 80012a4:	085b      	lsrs	r3, r3, #1
 80012a6:	ea4f 0232 	mov.w	r2, r2, rrx
 80012aa:	ebb6 0e02 	subs.w	lr, r6, r2
 80012ae:	eb75 0e03 	sbcs.w	lr, r5, r3
 80012b2:	bf22      	ittt	cs
 80012b4:	1ab6      	subcs	r6, r6, r2
 80012b6:	4675      	movcs	r5, lr
 80012b8:	ea40 00dc 	orrcs.w	r0, r0, ip, lsr #3
 80012bc:	ea55 0e06 	orrs.w	lr, r5, r6
 80012c0:	d018      	beq.n	80012f4 <__aeabi_ddiv+0x114>
 80012c2:	ea4f 1505 	mov.w	r5, r5, lsl #4
 80012c6:	ea45 7516 	orr.w	r5, r5, r6, lsr #28
 80012ca:	ea4f 1606 	mov.w	r6, r6, lsl #4
 80012ce:	ea4f 03c3 	mov.w	r3, r3, lsl #3
 80012d2:	ea43 7352 	orr.w	r3, r3, r2, lsr #29
 80012d6:	ea4f 02c2 	mov.w	r2, r2, lsl #3
 80012da:	ea5f 1c1c 	movs.w	ip, ip, lsr #4
 80012de:	d1c0      	bne.n	8001262 <__aeabi_ddiv+0x82>
 80012e0:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 80012e4:	d10b      	bne.n	80012fe <__aeabi_ddiv+0x11e>
 80012e6:	ea41 0100 	orr.w	r1, r1, r0
 80012ea:	f04f 0000 	mov.w	r0, #0
 80012ee:	f04f 4c00 	mov.w	ip, #2147483648	; 0x80000000
 80012f2:	e7b6      	b.n	8001262 <__aeabi_ddiv+0x82>
 80012f4:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 80012f8:	bf04      	itt	eq
 80012fa:	4301      	orreq	r1, r0
 80012fc:	2000      	moveq	r0, #0
 80012fe:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 8001302:	bf88      	it	hi
 8001304:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 8001308:	f63f aeaf 	bhi.w	800106a <__aeabi_dmul+0xde>
 800130c:	ebb5 0c03 	subs.w	ip, r5, r3
 8001310:	bf04      	itt	eq
 8001312:	ebb6 0c02 	subseq.w	ip, r6, r2
 8001316:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 800131a:	f150 0000 	adcs.w	r0, r0, #0
 800131e:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 8001322:	bd70      	pop	{r4, r5, r6, pc}
 8001324:	f00e 4e00 	and.w	lr, lr, #2147483648	; 0x80000000
 8001328:	ea4e 3111 	orr.w	r1, lr, r1, lsr #12
 800132c:	eb14 045c 	adds.w	r4, r4, ip, lsr #1
 8001330:	bfc2      	ittt	gt
 8001332:	ebd4 050c 	rsbsgt	r5, r4, ip
 8001336:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 800133a:	bd70      	popgt	{r4, r5, r6, pc}
 800133c:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8001340:	f04f 0e00 	mov.w	lr, #0
 8001344:	3c01      	subs	r4, #1
 8001346:	e690      	b.n	800106a <__aeabi_dmul+0xde>
 8001348:	ea45 0e06 	orr.w	lr, r5, r6
 800134c:	e68d      	b.n	800106a <__aeabi_dmul+0xde>
 800134e:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 8001352:	ea94 0f0c 	teq	r4, ip
 8001356:	bf08      	it	eq
 8001358:	ea95 0f0c 	teqeq	r5, ip
 800135c:	f43f af3b 	beq.w	80011d6 <__aeabi_dmul+0x24a>
 8001360:	ea94 0f0c 	teq	r4, ip
 8001364:	d10a      	bne.n	800137c <__aeabi_ddiv+0x19c>
 8001366:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 800136a:	f47f af34 	bne.w	80011d6 <__aeabi_dmul+0x24a>
 800136e:	ea95 0f0c 	teq	r5, ip
 8001372:	f47f af25 	bne.w	80011c0 <__aeabi_dmul+0x234>
 8001376:	4610      	mov	r0, r2
 8001378:	4619      	mov	r1, r3
 800137a:	e72c      	b.n	80011d6 <__aeabi_dmul+0x24a>
 800137c:	ea95 0f0c 	teq	r5, ip
 8001380:	d106      	bne.n	8001390 <__aeabi_ddiv+0x1b0>
 8001382:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 8001386:	f43f aefd 	beq.w	8001184 <__aeabi_dmul+0x1f8>
 800138a:	4610      	mov	r0, r2
 800138c:	4619      	mov	r1, r3
 800138e:	e722      	b.n	80011d6 <__aeabi_dmul+0x24a>
 8001390:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 8001394:	bf18      	it	ne
 8001396:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 800139a:	f47f aec5 	bne.w	8001128 <__aeabi_dmul+0x19c>
 800139e:	ea50 0441 	orrs.w	r4, r0, r1, lsl #1
 80013a2:	f47f af0d 	bne.w	80011c0 <__aeabi_dmul+0x234>
 80013a6:	ea52 0543 	orrs.w	r5, r2, r3, lsl #1
 80013aa:	f47f aeeb 	bne.w	8001184 <__aeabi_dmul+0x1f8>
 80013ae:	e712      	b.n	80011d6 <__aeabi_dmul+0x24a>

080013b0 <__gedf2>:
 80013b0:	f04f 3cff 	mov.w	ip, #4294967295
 80013b4:	e006      	b.n	80013c4 <__cmpdf2+0x4>
 80013b6:	bf00      	nop

080013b8 <__ledf2>:
 80013b8:	f04f 0c01 	mov.w	ip, #1
 80013bc:	e002      	b.n	80013c4 <__cmpdf2+0x4>
 80013be:	bf00      	nop

080013c0 <__cmpdf2>:
 80013c0:	f04f 0c01 	mov.w	ip, #1
 80013c4:	f84d cd04 	str.w	ip, [sp, #-4]!
 80013c8:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 80013cc:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 80013d0:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 80013d4:	bf18      	it	ne
 80013d6:	ea7f 5c6c 	mvnsne.w	ip, ip, asr #21
 80013da:	d01b      	beq.n	8001414 <__cmpdf2+0x54>
 80013dc:	b001      	add	sp, #4
 80013de:	ea50 0c41 	orrs.w	ip, r0, r1, lsl #1
 80013e2:	bf0c      	ite	eq
 80013e4:	ea52 0c43 	orrseq.w	ip, r2, r3, lsl #1
 80013e8:	ea91 0f03 	teqne	r1, r3
 80013ec:	bf02      	ittt	eq
 80013ee:	ea90 0f02 	teqeq	r0, r2
 80013f2:	2000      	moveq	r0, #0
 80013f4:	4770      	bxeq	lr
 80013f6:	f110 0f00 	cmn.w	r0, #0
 80013fa:	ea91 0f03 	teq	r1, r3
 80013fe:	bf58      	it	pl
 8001400:	4299      	cmppl	r1, r3
 8001402:	bf08      	it	eq
 8001404:	4290      	cmpeq	r0, r2
 8001406:	bf2c      	ite	cs
 8001408:	17d8      	asrcs	r0, r3, #31
 800140a:	ea6f 70e3 	mvncc.w	r0, r3, asr #31
 800140e:	f040 0001 	orr.w	r0, r0, #1
 8001412:	4770      	bx	lr
 8001414:	ea4f 0c41 	mov.w	ip, r1, lsl #1
 8001418:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 800141c:	d102      	bne.n	8001424 <__cmpdf2+0x64>
 800141e:	ea50 3c01 	orrs.w	ip, r0, r1, lsl #12
 8001422:	d107      	bne.n	8001434 <__cmpdf2+0x74>
 8001424:	ea4f 0c43 	mov.w	ip, r3, lsl #1
 8001428:	ea7f 5c6c 	mvns.w	ip, ip, asr #21
 800142c:	d1d6      	bne.n	80013dc <__cmpdf2+0x1c>
 800142e:	ea52 3c03 	orrs.w	ip, r2, r3, lsl #12
 8001432:	d0d3      	beq.n	80013dc <__cmpdf2+0x1c>
 8001434:	f85d 0b04 	ldr.w	r0, [sp], #4
 8001438:	4770      	bx	lr
 800143a:	bf00      	nop

0800143c <__aeabi_cdrcmple>:
 800143c:	4684      	mov	ip, r0
 800143e:	4610      	mov	r0, r2
 8001440:	4662      	mov	r2, ip
 8001442:	468c      	mov	ip, r1
 8001444:	4619      	mov	r1, r3
 8001446:	4663      	mov	r3, ip
 8001448:	e000      	b.n	800144c <__aeabi_cdcmpeq>
 800144a:	bf00      	nop

0800144c <__aeabi_cdcmpeq>:
 800144c:	b501      	push	{r0, lr}
 800144e:	f7ff ffb7 	bl	80013c0 <__cmpdf2>
 8001452:	2800      	cmp	r0, #0
 8001454:	bf48      	it	mi
 8001456:	f110 0f00 	cmnmi.w	r0, #0
 800145a:	bd01      	pop	{r0, pc}

0800145c <__aeabi_dcmpeq>:
 800145c:	f84d ed08 	str.w	lr, [sp, #-8]!
 8001460:	f7ff fff4 	bl	800144c <__aeabi_cdcmpeq>
 8001464:	bf0c      	ite	eq
 8001466:	2001      	moveq	r0, #1
 8001468:	2000      	movne	r0, #0
 800146a:	f85d fb08 	ldr.w	pc, [sp], #8
 800146e:	bf00      	nop

08001470 <__aeabi_dcmplt>:
 8001470:	f84d ed08 	str.w	lr, [sp, #-8]!
 8001474:	f7ff ffea 	bl	800144c <__aeabi_cdcmpeq>
 8001478:	bf34      	ite	cc
 800147a:	2001      	movcc	r0, #1
 800147c:	2000      	movcs	r0, #0
 800147e:	f85d fb08 	ldr.w	pc, [sp], #8
 8001482:	bf00      	nop

08001484 <__aeabi_dcmple>:
 8001484:	f84d ed08 	str.w	lr, [sp, #-8]!
 8001488:	f7ff ffe0 	bl	800144c <__aeabi_cdcmpeq>
 800148c:	bf94      	ite	ls
 800148e:	2001      	movls	r0, #1
 8001490:	2000      	movhi	r0, #0
 8001492:	f85d fb08 	ldr.w	pc, [sp], #8
 8001496:	bf00      	nop

08001498 <__aeabi_dcmpge>:
 8001498:	f84d ed08 	str.w	lr, [sp, #-8]!
 800149c:	f7ff ffce 	bl	800143c <__aeabi_cdrcmple>
 80014a0:	bf94      	ite	ls
 80014a2:	2001      	movls	r0, #1
 80014a4:	2000      	movhi	r0, #0
 80014a6:	f85d fb08 	ldr.w	pc, [sp], #8
 80014aa:	bf00      	nop

080014ac <__aeabi_dcmpgt>:
 80014ac:	f84d ed08 	str.w	lr, [sp, #-8]!
 80014b0:	f7ff ffc4 	bl	800143c <__aeabi_cdrcmple>
 80014b4:	bf34      	ite	cc
 80014b6:	2001      	movcc	r0, #1
 80014b8:	2000      	movcs	r0, #0
 80014ba:	f85d fb08 	ldr.w	pc, [sp], #8
 80014be:	bf00      	nop

080014c0 <_sbrk>:
 80014c0:	4a04      	ldr	r2, [pc, #16]	; (80014d4 <_sbrk+0x14>)
 80014c2:	4905      	ldr	r1, [pc, #20]	; (80014d8 <_sbrk+0x18>)
 80014c4:	6813      	ldr	r3, [r2, #0]
 80014c6:	2b00      	cmp	r3, #0
 80014c8:	bf08      	it	eq
 80014ca:	460b      	moveq	r3, r1
 80014cc:	4418      	add	r0, r3
 80014ce:	6010      	str	r0, [r2, #0]
 80014d0:	4618      	mov	r0, r3
 80014d2:	4770      	bx	lr
 80014d4:	20000908 	.word	0x20000908
 80014d8:	20006154 	.word	0x20006154

080014dc <cosf>:
 80014dc:	b500      	push	{lr}
 80014de:	ee10 3a10 	vmov	r3, s0
 80014e2:	4a20      	ldr	r2, [pc, #128]	; (8001564 <cosf+0x88>)
 80014e4:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 80014e8:	4293      	cmp	r3, r2
 80014ea:	b083      	sub	sp, #12
 80014ec:	dd19      	ble.n	8001522 <cosf+0x46>
 80014ee:	f1b3 4fff 	cmp.w	r3, #2139095040	; 0x7f800000
 80014f2:	db04      	blt.n	80014fe <cosf+0x22>
 80014f4:	ee30 0a40 	vsub.f32	s0, s0, s0
 80014f8:	b003      	add	sp, #12
 80014fa:	f85d fb04 	ldr.w	pc, [sp], #4
 80014fe:	4668      	mov	r0, sp
 8001500:	f000 f964 	bl	80017cc <__ieee754_rem_pio2f>
 8001504:	f000 0003 	and.w	r0, r0, #3
 8001508:	2801      	cmp	r0, #1
 800150a:	d01a      	beq.n	8001542 <cosf+0x66>
 800150c:	2802      	cmp	r0, #2
 800150e:	d00f      	beq.n	8001530 <cosf+0x54>
 8001510:	b300      	cbz	r0, 8001554 <cosf+0x78>
 8001512:	2001      	movs	r0, #1
 8001514:	eddd 0a01 	vldr	s1, [sp, #4]
 8001518:	ed9d 0a00 	vldr	s0, [sp]
 800151c:	f000 fe68 	bl	80021f0 <__kernel_sinf>
 8001520:	e7ea      	b.n	80014f8 <cosf+0x1c>
 8001522:	eddf 0a11 	vldr	s1, [pc, #68]	; 8001568 <cosf+0x8c>
 8001526:	f000 fa99 	bl	8001a5c <__kernel_cosf>
 800152a:	b003      	add	sp, #12
 800152c:	f85d fb04 	ldr.w	pc, [sp], #4
 8001530:	eddd 0a01 	vldr	s1, [sp, #4]
 8001534:	ed9d 0a00 	vldr	s0, [sp]
 8001538:	f000 fa90 	bl	8001a5c <__kernel_cosf>
 800153c:	eeb1 0a40 	vneg.f32	s0, s0
 8001540:	e7da      	b.n	80014f8 <cosf+0x1c>
 8001542:	eddd 0a01 	vldr	s1, [sp, #4]
 8001546:	ed9d 0a00 	vldr	s0, [sp]
 800154a:	f000 fe51 	bl	80021f0 <__kernel_sinf>
 800154e:	eeb1 0a40 	vneg.f32	s0, s0
 8001552:	e7d1      	b.n	80014f8 <cosf+0x1c>
 8001554:	eddd 0a01 	vldr	s1, [sp, #4]
 8001558:	ed9d 0a00 	vldr	s0, [sp]
 800155c:	f000 fa7e 	bl	8001a5c <__kernel_cosf>
 8001560:	e7ca      	b.n	80014f8 <cosf+0x1c>
 8001562:	bf00      	nop
 8001564:	3f490fd8 	.word	0x3f490fd8
 8001568:	00000000 	.word	0x00000000

0800156c <sinf>:
 800156c:	b500      	push	{lr}
 800156e:	ee10 3a10 	vmov	r3, s0
 8001572:	4a21      	ldr	r2, [pc, #132]	; (80015f8 <sinf+0x8c>)
 8001574:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 8001578:	4293      	cmp	r3, r2
 800157a:	b083      	sub	sp, #12
 800157c:	dd1a      	ble.n	80015b4 <sinf+0x48>
 800157e:	f1b3 4fff 	cmp.w	r3, #2139095040	; 0x7f800000
 8001582:	db04      	blt.n	800158e <sinf+0x22>
 8001584:	ee30 0a40 	vsub.f32	s0, s0, s0
 8001588:	b003      	add	sp, #12
 800158a:	f85d fb04 	ldr.w	pc, [sp], #4
 800158e:	4668      	mov	r0, sp
 8001590:	f000 f91c 	bl	80017cc <__ieee754_rem_pio2f>
 8001594:	f000 0003 	and.w	r0, r0, #3
 8001598:	2801      	cmp	r0, #1
 800159a:	d01d      	beq.n	80015d8 <sinf+0x6c>
 800159c:	2802      	cmp	r0, #2
 800159e:	d011      	beq.n	80015c4 <sinf+0x58>
 80015a0:	b308      	cbz	r0, 80015e6 <sinf+0x7a>
 80015a2:	eddd 0a01 	vldr	s1, [sp, #4]
 80015a6:	ed9d 0a00 	vldr	s0, [sp]
 80015aa:	f000 fa57 	bl	8001a5c <__kernel_cosf>
 80015ae:	eeb1 0a40 	vneg.f32	s0, s0
 80015b2:	e7e9      	b.n	8001588 <sinf+0x1c>
 80015b4:	2000      	movs	r0, #0
 80015b6:	eddf 0a11 	vldr	s1, [pc, #68]	; 80015fc <sinf+0x90>
 80015ba:	f000 fe19 	bl	80021f0 <__kernel_sinf>
 80015be:	b003      	add	sp, #12
 80015c0:	f85d fb04 	ldr.w	pc, [sp], #4
 80015c4:	2001      	movs	r0, #1
 80015c6:	eddd 0a01 	vldr	s1, [sp, #4]
 80015ca:	ed9d 0a00 	vldr	s0, [sp]
 80015ce:	f000 fe0f 	bl	80021f0 <__kernel_sinf>
 80015d2:	eeb1 0a40 	vneg.f32	s0, s0
 80015d6:	e7d7      	b.n	8001588 <sinf+0x1c>
 80015d8:	eddd 0a01 	vldr	s1, [sp, #4]
 80015dc:	ed9d 0a00 	vldr	s0, [sp]
 80015e0:	f000 fa3c 	bl	8001a5c <__kernel_cosf>
 80015e4:	e7d0      	b.n	8001588 <sinf+0x1c>
 80015e6:	2001      	movs	r0, #1
 80015e8:	eddd 0a01 	vldr	s1, [sp, #4]
 80015ec:	ed9d 0a00 	vldr	s0, [sp]
 80015f0:	f000 fdfe 	bl	80021f0 <__kernel_sinf>
 80015f4:	e7c8      	b.n	8001588 <sinf+0x1c>
 80015f6:	bf00      	nop
 80015f8:	3f490fd8 	.word	0x3f490fd8
 80015fc:	00000000 	.word	0x00000000

08001600 <atan2>:
 8001600:	f000 b802 	b.w	8001608 <__ieee754_atan2>
 8001604:	0000      	movs	r0, r0
	...

08001608 <__ieee754_atan2>:
 8001608:	ec51 0b11 	vmov	r0, r1, d1
 800160c:	4243      	negs	r3, r0
 800160e:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 8001612:	4303      	orrs	r3, r0
 8001614:	f021 4800 	bic.w	r8, r1, #2147483648	; 0x80000000
 8001618:	4f6b      	ldr	r7, [pc, #428]	; (80017c8 <__ieee754_atan2+0x1c0>)
 800161a:	ea48 73d3 	orr.w	r3, r8, r3, lsr #31
 800161e:	42bb      	cmp	r3, r7
 8001620:	ec55 4b10 	vmov	r4, r5, d0
 8001624:	d845      	bhi.n	80016b2 <__ieee754_atan2+0xaa>
 8001626:	4263      	negs	r3, r4
 8001628:	4323      	orrs	r3, r4
 800162a:	f025 4a00 	bic.w	sl, r5, #2147483648	; 0x80000000
 800162e:	ea4a 73d3 	orr.w	r3, sl, r3, lsr #31
 8001632:	42bb      	cmp	r3, r7
 8001634:	46a9      	mov	r9, r5
 8001636:	d83c      	bhi.n	80016b2 <__ieee754_atan2+0xaa>
 8001638:	f101 4340 	add.w	r3, r1, #3221225472	; 0xc0000000
 800163c:	f503 1380 	add.w	r3, r3, #1048576	; 0x100000
 8001640:	4303      	orrs	r3, r0
 8001642:	468e      	mov	lr, r1
 8001644:	d04c      	beq.n	80016e0 <__ieee754_atan2+0xd8>
 8001646:	178e      	asrs	r6, r1, #30
 8001648:	f006 0602 	and.w	r6, r6, #2
 800164c:	ea54 030a 	orrs.w	r3, r4, sl
 8001650:	ea46 76d5 	orr.w	r6, r6, r5, lsr #31
 8001654:	d035      	beq.n	80016c2 <__ieee754_atan2+0xba>
 8001656:	ea50 0308 	orrs.w	r3, r0, r8
 800165a:	d03a      	beq.n	80016d2 <__ieee754_atan2+0xca>
 800165c:	45b8      	cmp	r8, r7
 800165e:	d053      	beq.n	8001708 <__ieee754_atan2+0x100>
 8001660:	45ba      	cmp	sl, r7
 8001662:	d036      	beq.n	80016d2 <__ieee754_atan2+0xca>
 8001664:	ebaa 0808 	sub.w	r8, sl, r8
 8001668:	ea4f 5828 	mov.w	r8, r8, asr #20
 800166c:	f1b8 0f3c 	cmp.w	r8, #60	; 0x3c
 8001670:	dc3e      	bgt.n	80016f0 <__ieee754_atan2+0xe8>
 8001672:	2900      	cmp	r1, #0
 8001674:	db64      	blt.n	8001740 <__ieee754_atan2+0x138>
 8001676:	4602      	mov	r2, r0
 8001678:	460b      	mov	r3, r1
 800167a:	4620      	mov	r0, r4
 800167c:	4629      	mov	r1, r5
 800167e:	f7ff fdaf 	bl	80011e0 <__aeabi_ddiv>
 8001682:	ec41 0b10 	vmov	d0, r0, r1
 8001686:	f000 ffa9 	bl	80025dc <fabs>
 800168a:	f000 fdf9 	bl	8002280 <atan>
 800168e:	ec51 0b10 	vmov	r0, r1, d0
 8001692:	2e01      	cmp	r6, #1
 8001694:	d051      	beq.n	800173a <__ieee754_atan2+0x132>
 8001696:	2e02      	cmp	r6, #2
 8001698:	d042      	beq.n	8001720 <__ieee754_atan2+0x118>
 800169a:	b176      	cbz	r6, 80016ba <__ieee754_atan2+0xb2>
 800169c:	a338      	add	r3, pc, #224	; (adr r3, 8001780 <__ieee754_atan2+0x178>)
 800169e:	e9d3 2300 	ldrd	r2, r3, [r3]
 80016a2:	f7fe fdcb 	bl	800023c <__aeabi_dsub>
 80016a6:	a338      	add	r3, pc, #224	; (adr r3, 8001788 <__ieee754_atan2+0x180>)
 80016a8:	e9d3 2300 	ldrd	r2, r3, [r3]
 80016ac:	f7fe fdc6 	bl	800023c <__aeabi_dsub>
 80016b0:	e003      	b.n	80016ba <__ieee754_atan2+0xb2>
 80016b2:	4622      	mov	r2, r4
 80016b4:	462b      	mov	r3, r5
 80016b6:	f7fe fdc3 	bl	8000240 <__adddf3>
 80016ba:	ec41 0b10 	vmov	d0, r0, r1
 80016be:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 80016c2:	2e02      	cmp	r6, #2
 80016c4:	d018      	beq.n	80016f8 <__ieee754_atan2+0xf0>
 80016c6:	2e03      	cmp	r6, #3
 80016c8:	d10e      	bne.n	80016e8 <__ieee754_atan2+0xe0>
 80016ca:	a131      	add	r1, pc, #196	; (adr r1, 8001790 <__ieee754_atan2+0x188>)
 80016cc:	e9d1 0100 	ldrd	r0, r1, [r1]
 80016d0:	e7f3      	b.n	80016ba <__ieee754_atan2+0xb2>
 80016d2:	f1b9 0f00 	cmp.w	r9, #0
 80016d6:	db13      	blt.n	8001700 <__ieee754_atan2+0xf8>
 80016d8:	a12f      	add	r1, pc, #188	; (adr r1, 8001798 <__ieee754_atan2+0x190>)
 80016da:	e9d1 0100 	ldrd	r0, r1, [r1]
 80016de:	e7ec      	b.n	80016ba <__ieee754_atan2+0xb2>
 80016e0:	e8bd 47f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 80016e4:	f000 bdcc 	b.w	8002280 <atan>
 80016e8:	ee10 0a10 	vmov	r0, s0
 80016ec:	4629      	mov	r1, r5
 80016ee:	e7e4      	b.n	80016ba <__ieee754_atan2+0xb2>
 80016f0:	a129      	add	r1, pc, #164	; (adr r1, 8001798 <__ieee754_atan2+0x190>)
 80016f2:	e9d1 0100 	ldrd	r0, r1, [r1]
 80016f6:	e7cc      	b.n	8001692 <__ieee754_atan2+0x8a>
 80016f8:	a123      	add	r1, pc, #140	; (adr r1, 8001788 <__ieee754_atan2+0x180>)
 80016fa:	e9d1 0100 	ldrd	r0, r1, [r1]
 80016fe:	e7dc      	b.n	80016ba <__ieee754_atan2+0xb2>
 8001700:	a127      	add	r1, pc, #156	; (adr r1, 80017a0 <__ieee754_atan2+0x198>)
 8001702:	e9d1 0100 	ldrd	r0, r1, [r1]
 8001706:	e7d8      	b.n	80016ba <__ieee754_atan2+0xb2>
 8001708:	45c2      	cmp	sl, r8
 800170a:	d01f      	beq.n	800174c <__ieee754_atan2+0x144>
 800170c:	2e02      	cmp	r6, #2
 800170e:	d0f3      	beq.n	80016f8 <__ieee754_atan2+0xf0>
 8001710:	2e03      	cmp	r6, #3
 8001712:	d0da      	beq.n	80016ca <__ieee754_atan2+0xc2>
 8001714:	2e01      	cmp	r6, #1
 8001716:	f04f 0000 	mov.w	r0, #0
 800171a:	d021      	beq.n	8001760 <__ieee754_atan2+0x158>
 800171c:	2100      	movs	r1, #0
 800171e:	e7cc      	b.n	80016ba <__ieee754_atan2+0xb2>
 8001720:	a317      	add	r3, pc, #92	; (adr r3, 8001780 <__ieee754_atan2+0x178>)
 8001722:	e9d3 2300 	ldrd	r2, r3, [r3]
 8001726:	f7fe fd89 	bl	800023c <__aeabi_dsub>
 800172a:	4602      	mov	r2, r0
 800172c:	460b      	mov	r3, r1
 800172e:	a116      	add	r1, pc, #88	; (adr r1, 8001788 <__ieee754_atan2+0x180>)
 8001730:	e9d1 0100 	ldrd	r0, r1, [r1]
 8001734:	f7fe fd82 	bl	800023c <__aeabi_dsub>
 8001738:	e7bf      	b.n	80016ba <__ieee754_atan2+0xb2>
 800173a:	f101 4100 	add.w	r1, r1, #2147483648	; 0x80000000
 800173e:	e7bc      	b.n	80016ba <__ieee754_atan2+0xb2>
 8001740:	f118 0f3c 	cmn.w	r8, #60	; 0x3c
 8001744:	da97      	bge.n	8001676 <__ieee754_atan2+0x6e>
 8001746:	2000      	movs	r0, #0
 8001748:	2100      	movs	r1, #0
 800174a:	e7a2      	b.n	8001692 <__ieee754_atan2+0x8a>
 800174c:	2e02      	cmp	r6, #2
 800174e:	d012      	beq.n	8001776 <__ieee754_atan2+0x16e>
 8001750:	2e03      	cmp	r6, #3
 8001752:	d00c      	beq.n	800176e <__ieee754_atan2+0x166>
 8001754:	2e01      	cmp	r6, #1
 8001756:	d006      	beq.n	8001766 <__ieee754_atan2+0x15e>
 8001758:	a113      	add	r1, pc, #76	; (adr r1, 80017a8 <__ieee754_atan2+0x1a0>)
 800175a:	e9d1 0100 	ldrd	r0, r1, [r1]
 800175e:	e7ac      	b.n	80016ba <__ieee754_atan2+0xb2>
 8001760:	f04f 4100 	mov.w	r1, #2147483648	; 0x80000000
 8001764:	e7a9      	b.n	80016ba <__ieee754_atan2+0xb2>
 8001766:	a112      	add	r1, pc, #72	; (adr r1, 80017b0 <__ieee754_atan2+0x1a8>)
 8001768:	e9d1 0100 	ldrd	r0, r1, [r1]
 800176c:	e7a5      	b.n	80016ba <__ieee754_atan2+0xb2>
 800176e:	a112      	add	r1, pc, #72	; (adr r1, 80017b8 <__ieee754_atan2+0x1b0>)
 8001770:	e9d1 0100 	ldrd	r0, r1, [r1]
 8001774:	e7a1      	b.n	80016ba <__ieee754_atan2+0xb2>
 8001776:	a112      	add	r1, pc, #72	; (adr r1, 80017c0 <__ieee754_atan2+0x1b8>)
 8001778:	e9d1 0100 	ldrd	r0, r1, [r1]
 800177c:	e79d      	b.n	80016ba <__ieee754_atan2+0xb2>
 800177e:	bf00      	nop
 8001780:	33145c07 	.word	0x33145c07
 8001784:	3ca1a626 	.word	0x3ca1a626
 8001788:	54442d18 	.word	0x54442d18
 800178c:	400921fb 	.word	0x400921fb
 8001790:	54442d18 	.word	0x54442d18
 8001794:	c00921fb 	.word	0xc00921fb
 8001798:	54442d18 	.word	0x54442d18
 800179c:	3ff921fb 	.word	0x3ff921fb
 80017a0:	54442d18 	.word	0x54442d18
 80017a4:	bff921fb 	.word	0xbff921fb
 80017a8:	54442d18 	.word	0x54442d18
 80017ac:	3fe921fb 	.word	0x3fe921fb
 80017b0:	54442d18 	.word	0x54442d18
 80017b4:	bfe921fb 	.word	0xbfe921fb
 80017b8:	7f3321d2 	.word	0x7f3321d2
 80017bc:	c002d97c 	.word	0xc002d97c
 80017c0:	7f3321d2 	.word	0x7f3321d2
 80017c4:	4002d97c 	.word	0x4002d97c
 80017c8:	7ff00000 	.word	0x7ff00000

080017cc <__ieee754_rem_pio2f>:
 80017cc:	b570      	push	{r4, r5, r6, lr}
 80017ce:	ee10 3a10 	vmov	r3, s0
 80017d2:	4a94      	ldr	r2, [pc, #592]	; (8001a24 <__ieee754_rem_pio2f+0x258>)
 80017d4:	f023 4400 	bic.w	r4, r3, #2147483648	; 0x80000000
 80017d8:	4294      	cmp	r4, r2
 80017da:	b086      	sub	sp, #24
 80017dc:	4605      	mov	r5, r0
 80017de:	dd68      	ble.n	80018b2 <__ieee754_rem_pio2f+0xe6>
 80017e0:	4a91      	ldr	r2, [pc, #580]	; (8001a28 <__ieee754_rem_pio2f+0x25c>)
 80017e2:	4294      	cmp	r4, r2
 80017e4:	ee10 6a10 	vmov	r6, s0
 80017e8:	dc1a      	bgt.n	8001820 <__ieee754_rem_pio2f+0x54>
 80017ea:	2b00      	cmp	r3, #0
 80017ec:	eddf 7a8f 	vldr	s15, [pc, #572]	; 8001a2c <__ieee754_rem_pio2f+0x260>
 80017f0:	4a8f      	ldr	r2, [pc, #572]	; (8001a30 <__ieee754_rem_pio2f+0x264>)
 80017f2:	f024 040f 	bic.w	r4, r4, #15
 80017f6:	f340 80dd 	ble.w	80019b4 <__ieee754_rem_pio2f+0x1e8>
 80017fa:	4294      	cmp	r4, r2
 80017fc:	ee70 7a67 	vsub.f32	s15, s0, s15
 8001800:	d066      	beq.n	80018d0 <__ieee754_rem_pio2f+0x104>
 8001802:	ed9f 7a8c 	vldr	s14, [pc, #560]	; 8001a34 <__ieee754_rem_pio2f+0x268>
 8001806:	ee77 6ac7 	vsub.f32	s13, s15, s14
 800180a:	2001      	movs	r0, #1
 800180c:	ee77 7ae6 	vsub.f32	s15, s15, s13
 8001810:	edc5 6a00 	vstr	s13, [r5]
 8001814:	ee77 7ac7 	vsub.f32	s15, s15, s14
 8001818:	edc5 7a01 	vstr	s15, [r5, #4]
 800181c:	b006      	add	sp, #24
 800181e:	bd70      	pop	{r4, r5, r6, pc}
 8001820:	4a85      	ldr	r2, [pc, #532]	; (8001a38 <__ieee754_rem_pio2f+0x26c>)
 8001822:	4294      	cmp	r4, r2
 8001824:	dd66      	ble.n	80018f4 <__ieee754_rem_pio2f+0x128>
 8001826:	f1b4 4fff 	cmp.w	r4, #2139095040	; 0x7f800000
 800182a:	da49      	bge.n	80018c0 <__ieee754_rem_pio2f+0xf4>
 800182c:	15e2      	asrs	r2, r4, #23
 800182e:	3a86      	subs	r2, #134	; 0x86
 8001830:	eba4 53c2 	sub.w	r3, r4, r2, lsl #23
 8001834:	ee07 3a90 	vmov	s15, r3
 8001838:	eebd 7ae7 	vcvt.s32.f32	s14, s15
 800183c:	eddf 6a7f 	vldr	s13, [pc, #508]	; 8001a3c <__ieee754_rem_pio2f+0x270>
 8001840:	eeb8 7ac7 	vcvt.f32.s32	s14, s14
 8001844:	ee77 7ac7 	vsub.f32	s15, s15, s14
 8001848:	ed8d 7a03 	vstr	s14, [sp, #12]
 800184c:	ee67 7aa6 	vmul.f32	s15, s15, s13
 8001850:	eebd 7ae7 	vcvt.s32.f32	s14, s15
 8001854:	eeb8 7ac7 	vcvt.f32.s32	s14, s14
 8001858:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800185c:	ed8d 7a04 	vstr	s14, [sp, #16]
 8001860:	ee67 7aa6 	vmul.f32	s15, s15, s13
 8001864:	eef5 7a40 	vcmp.f32	s15, #0.0
 8001868:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800186c:	edcd 7a05 	vstr	s15, [sp, #20]
 8001870:	f040 80b3 	bne.w	80019da <__ieee754_rem_pio2f+0x20e>
 8001874:	eeb5 7a40 	vcmp.f32	s14, #0.0
 8001878:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800187c:	bf0c      	ite	eq
 800187e:	2301      	moveq	r3, #1
 8001880:	2302      	movne	r3, #2
 8001882:	496f      	ldr	r1, [pc, #444]	; (8001a40 <__ieee754_rem_pio2f+0x274>)
 8001884:	9101      	str	r1, [sp, #4]
 8001886:	2102      	movs	r1, #2
 8001888:	9100      	str	r1, [sp, #0]
 800188a:	a803      	add	r0, sp, #12
 800188c:	4629      	mov	r1, r5
 800188e:	f000 f967 	bl	8001b60 <__kernel_rem_pio2f>
 8001892:	2e00      	cmp	r6, #0
 8001894:	da12      	bge.n	80018bc <__ieee754_rem_pio2f+0xf0>
 8001896:	ed95 7a00 	vldr	s14, [r5]
 800189a:	edd5 7a01 	vldr	s15, [r5, #4]
 800189e:	eeb1 7a47 	vneg.f32	s14, s14
 80018a2:	eef1 7a67 	vneg.f32	s15, s15
 80018a6:	4240      	negs	r0, r0
 80018a8:	ed85 7a00 	vstr	s14, [r5]
 80018ac:	edc5 7a01 	vstr	s15, [r5, #4]
 80018b0:	e004      	b.n	80018bc <__ieee754_rem_pio2f+0xf0>
 80018b2:	2200      	movs	r2, #0
 80018b4:	ed85 0a00 	vstr	s0, [r5]
 80018b8:	6042      	str	r2, [r0, #4]
 80018ba:	2000      	movs	r0, #0
 80018bc:	b006      	add	sp, #24
 80018be:	bd70      	pop	{r4, r5, r6, pc}
 80018c0:	ee70 7a40 	vsub.f32	s15, s0, s0
 80018c4:	2000      	movs	r0, #0
 80018c6:	edc5 7a01 	vstr	s15, [r5, #4]
 80018ca:	edc5 7a00 	vstr	s15, [r5]
 80018ce:	e7f5      	b.n	80018bc <__ieee754_rem_pio2f+0xf0>
 80018d0:	eddf 6a5c 	vldr	s13, [pc, #368]	; 8001a44 <__ieee754_rem_pio2f+0x278>
 80018d4:	ed9f 7a5c 	vldr	s14, [pc, #368]	; 8001a48 <__ieee754_rem_pio2f+0x27c>
 80018d8:	ee77 7ae6 	vsub.f32	s15, s15, s13
 80018dc:	2001      	movs	r0, #1
 80018de:	ee77 6ac7 	vsub.f32	s13, s15, s14
 80018e2:	ee77 7ae6 	vsub.f32	s15, s15, s13
 80018e6:	edc5 6a00 	vstr	s13, [r5]
 80018ea:	ee77 7ac7 	vsub.f32	s15, s15, s14
 80018ee:	edc5 7a01 	vstr	s15, [r5, #4]
 80018f2:	e7e3      	b.n	80018bc <__ieee754_rem_pio2f+0xf0>
 80018f4:	f000 fe7a 	bl	80025ec <fabsf>
 80018f8:	eddf 6a54 	vldr	s13, [pc, #336]	; 8001a4c <__ieee754_rem_pio2f+0x280>
 80018fc:	eddf 5a4b 	vldr	s11, [pc, #300]	; 8001a2c <__ieee754_rem_pio2f+0x260>
 8001900:	eddf 7a4c 	vldr	s15, [pc, #304]	; 8001a34 <__ieee754_rem_pio2f+0x268>
 8001904:	eeb6 7a00 	vmov.f32	s14, #96	; 0x3f000000  0.5
 8001908:	eea0 7a26 	vfma.f32	s14, s0, s13
 800190c:	eebd 7ac7 	vcvt.s32.f32	s14, s14
 8001910:	eef8 6ac7 	vcvt.f32.s32	s13, s14
 8001914:	ee17 0a10 	vmov	r0, s14
 8001918:	eeb1 6a66 	vneg.f32	s12, s13
 800191c:	281f      	cmp	r0, #31
 800191e:	eea6 0a25 	vfma.f32	s0, s12, s11
 8001922:	ee66 7aa7 	vmul.f32	s15, s13, s15
 8001926:	dc1d      	bgt.n	8001964 <__ieee754_rem_pio2f+0x198>
 8001928:	4a49      	ldr	r2, [pc, #292]	; (8001a50 <__ieee754_rem_pio2f+0x284>)
 800192a:	1e41      	subs	r1, r0, #1
 800192c:	f024 03ff 	bic.w	r3, r4, #255	; 0xff
 8001930:	f852 2021 	ldr.w	r2, [r2, r1, lsl #2]
 8001934:	4293      	cmp	r3, r2
 8001936:	d015      	beq.n	8001964 <__ieee754_rem_pio2f+0x198>
 8001938:	ee30 7a67 	vsub.f32	s14, s0, s15
 800193c:	ed85 7a00 	vstr	s14, [r5]
 8001940:	ee30 0a47 	vsub.f32	s0, s0, s14
 8001944:	2e00      	cmp	r6, #0
 8001946:	ee30 0a67 	vsub.f32	s0, s0, s15
 800194a:	ed85 0a01 	vstr	s0, [r5, #4]
 800194e:	dab5      	bge.n	80018bc <__ieee754_rem_pio2f+0xf0>
 8001950:	eeb1 7a47 	vneg.f32	s14, s14
 8001954:	eeb1 0a40 	vneg.f32	s0, s0
 8001958:	ed85 7a00 	vstr	s14, [r5]
 800195c:	ed85 0a01 	vstr	s0, [r5, #4]
 8001960:	4240      	negs	r0, r0
 8001962:	e7ab      	b.n	80018bc <__ieee754_rem_pio2f+0xf0>
 8001964:	ee30 7a67 	vsub.f32	s14, s0, s15
 8001968:	15e4      	asrs	r4, r4, #23
 800196a:	ee17 3a10 	vmov	r3, s14
 800196e:	f3c3 53c7 	ubfx	r3, r3, #23, #8
 8001972:	1ae3      	subs	r3, r4, r3
 8001974:	2b08      	cmp	r3, #8
 8001976:	dde1      	ble.n	800193c <__ieee754_rem_pio2f+0x170>
 8001978:	eddf 7a32 	vldr	s15, [pc, #200]	; 8001a44 <__ieee754_rem_pio2f+0x278>
 800197c:	ed9f 7a32 	vldr	s14, [pc, #200]	; 8001a48 <__ieee754_rem_pio2f+0x27c>
 8001980:	eef0 5a40 	vmov.f32	s11, s0
 8001984:	eee6 5a27 	vfma.f32	s11, s12, s15
 8001988:	ee30 0a65 	vsub.f32	s0, s0, s11
 800198c:	eea6 0a27 	vfma.f32	s0, s12, s15
 8001990:	eef0 7a40 	vmov.f32	s15, s0
 8001994:	eed6 7a87 	vfnms.f32	s15, s13, s14
 8001998:	ee35 7ae7 	vsub.f32	s14, s11, s15
 800199c:	ee17 3a10 	vmov	r3, s14
 80019a0:	f3c3 53c7 	ubfx	r3, r3, #23, #8
 80019a4:	1ae4      	subs	r4, r4, r3
 80019a6:	2c19      	cmp	r4, #25
 80019a8:	dc2c      	bgt.n	8001a04 <__ieee754_rem_pio2f+0x238>
 80019aa:	ed85 7a00 	vstr	s14, [r5]
 80019ae:	eeb0 0a65 	vmov.f32	s0, s11
 80019b2:	e7c5      	b.n	8001940 <__ieee754_rem_pio2f+0x174>
 80019b4:	4294      	cmp	r4, r2
 80019b6:	ee70 7a27 	vadd.f32	s15, s0, s15
 80019ba:	d010      	beq.n	80019de <__ieee754_rem_pio2f+0x212>
 80019bc:	ed9f 7a1d 	vldr	s14, [pc, #116]	; 8001a34 <__ieee754_rem_pio2f+0x268>
 80019c0:	ee77 6a87 	vadd.f32	s13, s15, s14
 80019c4:	f04f 30ff 	mov.w	r0, #4294967295
 80019c8:	ee77 7ae6 	vsub.f32	s15, s15, s13
 80019cc:	edc5 6a00 	vstr	s13, [r5]
 80019d0:	ee77 7a87 	vadd.f32	s15, s15, s14
 80019d4:	edc5 7a01 	vstr	s15, [r5, #4]
 80019d8:	e770      	b.n	80018bc <__ieee754_rem_pio2f+0xf0>
 80019da:	2303      	movs	r3, #3
 80019dc:	e751      	b.n	8001882 <__ieee754_rem_pio2f+0xb6>
 80019de:	eddf 6a19 	vldr	s13, [pc, #100]	; 8001a44 <__ieee754_rem_pio2f+0x278>
 80019e2:	ed9f 7a19 	vldr	s14, [pc, #100]	; 8001a48 <__ieee754_rem_pio2f+0x27c>
 80019e6:	ee77 7aa6 	vadd.f32	s15, s15, s13
 80019ea:	f04f 30ff 	mov.w	r0, #4294967295
 80019ee:	ee77 6a87 	vadd.f32	s13, s15, s14
 80019f2:	ee77 7ae6 	vsub.f32	s15, s15, s13
 80019f6:	edc5 6a00 	vstr	s13, [r5]
 80019fa:	ee77 7a87 	vadd.f32	s15, s15, s14
 80019fe:	edc5 7a01 	vstr	s15, [r5, #4]
 8001a02:	e75b      	b.n	80018bc <__ieee754_rem_pio2f+0xf0>
 8001a04:	ed9f 7a13 	vldr	s14, [pc, #76]	; 8001a54 <__ieee754_rem_pio2f+0x288>
 8001a08:	ed9f 5a13 	vldr	s10, [pc, #76]	; 8001a58 <__ieee754_rem_pio2f+0x28c>
 8001a0c:	eeb0 0a65 	vmov.f32	s0, s11
 8001a10:	eea6 0a07 	vfma.f32	s0, s12, s14
 8001a14:	ee75 7ac0 	vsub.f32	s15, s11, s0
 8001a18:	eee6 7a07 	vfma.f32	s15, s12, s14
 8001a1c:	eed6 7a85 	vfnms.f32	s15, s13, s10
 8001a20:	e78a      	b.n	8001938 <__ieee754_rem_pio2f+0x16c>
 8001a22:	bf00      	nop
 8001a24:	3f490fd8 	.word	0x3f490fd8
 8001a28:	4016cbe3 	.word	0x4016cbe3
 8001a2c:	3fc90f80 	.word	0x3fc90f80
 8001a30:	3fc90fd0 	.word	0x3fc90fd0
 8001a34:	37354443 	.word	0x37354443
 8001a38:	43490f80 	.word	0x43490f80
 8001a3c:	43800000 	.word	0x43800000
 8001a40:	0800995c 	.word	0x0800995c
 8001a44:	37354400 	.word	0x37354400
 8001a48:	2e85a308 	.word	0x2e85a308
 8001a4c:	3f22f984 	.word	0x3f22f984
 8001a50:	080098dc 	.word	0x080098dc
 8001a54:	2e85a300 	.word	0x2e85a300
 8001a58:	248d3132 	.word	0x248d3132

08001a5c <__kernel_cosf>:
 8001a5c:	ee10 3a10 	vmov	r3, s0
 8001a60:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 8001a64:	f1b3 5f48 	cmp.w	r3, #838860800	; 0x32000000
 8001a68:	da2c      	bge.n	8001ac4 <__kernel_cosf+0x68>
 8001a6a:	eefd 7ac0 	vcvt.s32.f32	s15, s0
 8001a6e:	ee17 3a90 	vmov	r3, s15
 8001a72:	2b00      	cmp	r3, #0
 8001a74:	d060      	beq.n	8001b38 <__kernel_cosf+0xdc>
 8001a76:	ee20 7a00 	vmul.f32	s14, s0, s0
 8001a7a:	eddf 4a31 	vldr	s9, [pc, #196]	; 8001b40 <__kernel_cosf+0xe4>
 8001a7e:	ed9f 5a31 	vldr	s10, [pc, #196]	; 8001b44 <__kernel_cosf+0xe8>
 8001a82:	eddf 5a31 	vldr	s11, [pc, #196]	; 8001b48 <__kernel_cosf+0xec>
 8001a86:	ed9f 6a31 	vldr	s12, [pc, #196]	; 8001b4c <__kernel_cosf+0xf0>
 8001a8a:	eddf 7a31 	vldr	s15, [pc, #196]	; 8001b50 <__kernel_cosf+0xf4>
 8001a8e:	eddf 6a31 	vldr	s13, [pc, #196]	; 8001b54 <__kernel_cosf+0xf8>
 8001a92:	eea7 5a24 	vfma.f32	s10, s14, s9
 8001a96:	eee7 5a05 	vfma.f32	s11, s14, s10
 8001a9a:	eea7 6a25 	vfma.f32	s12, s14, s11
 8001a9e:	eee7 7a06 	vfma.f32	s15, s14, s12
 8001aa2:	eee7 6a27 	vfma.f32	s13, s14, s15
 8001aa6:	ee66 6a87 	vmul.f32	s13, s13, s14
 8001aaa:	ee60 0ac0 	vnmul.f32	s1, s1, s0
 8001aae:	eeb6 6a00 	vmov.f32	s12, #96	; 0x3f000000  0.5
 8001ab2:	eee7 0a26 	vfma.f32	s1, s14, s13
 8001ab6:	eef7 7a00 	vmov.f32	s15, #112	; 0x3f800000  1.0
 8001aba:	eed7 0a06 	vfnms.f32	s1, s14, s12
 8001abe:	ee37 0ae0 	vsub.f32	s0, s15, s1
 8001ac2:	4770      	bx	lr
 8001ac4:	ee20 7a00 	vmul.f32	s14, s0, s0
 8001ac8:	eddf 4a1d 	vldr	s9, [pc, #116]	; 8001b40 <__kernel_cosf+0xe4>
 8001acc:	ed9f 5a1d 	vldr	s10, [pc, #116]	; 8001b44 <__kernel_cosf+0xe8>
 8001ad0:	eddf 5a1d 	vldr	s11, [pc, #116]	; 8001b48 <__kernel_cosf+0xec>
 8001ad4:	ed9f 6a1d 	vldr	s12, [pc, #116]	; 8001b4c <__kernel_cosf+0xf0>
 8001ad8:	eddf 7a1d 	vldr	s15, [pc, #116]	; 8001b50 <__kernel_cosf+0xf4>
 8001adc:	eddf 6a1d 	vldr	s13, [pc, #116]	; 8001b54 <__kernel_cosf+0xf8>
 8001ae0:	4a1d      	ldr	r2, [pc, #116]	; (8001b58 <__kernel_cosf+0xfc>)
 8001ae2:	eea7 5a24 	vfma.f32	s10, s14, s9
 8001ae6:	4293      	cmp	r3, r2
 8001ae8:	eee7 5a05 	vfma.f32	s11, s14, s10
 8001aec:	eea7 6a25 	vfma.f32	s12, s14, s11
 8001af0:	eee7 7a06 	vfma.f32	s15, s14, s12
 8001af4:	eee7 6a27 	vfma.f32	s13, s14, s15
 8001af8:	ee66 6a87 	vmul.f32	s13, s13, s14
 8001afc:	ddd5      	ble.n	8001aaa <__kernel_cosf+0x4e>
 8001afe:	4a17      	ldr	r2, [pc, #92]	; (8001b5c <__kernel_cosf+0x100>)
 8001b00:	4293      	cmp	r3, r2
 8001b02:	dc14      	bgt.n	8001b2e <__kernel_cosf+0xd2>
 8001b04:	f103 437f 	add.w	r3, r3, #4278190080	; 0xff000000
 8001b08:	ee07 3a90 	vmov	s15, r3
 8001b0c:	eeb7 6a00 	vmov.f32	s12, #112	; 0x3f800000  1.0
 8001b10:	ee36 6a67 	vsub.f32	s12, s12, s15
 8001b14:	ee60 0ac0 	vnmul.f32	s1, s1, s0
 8001b18:	eef6 5a00 	vmov.f32	s11, #96	; 0x3f000000  0.5
 8001b1c:	eee7 0a26 	vfma.f32	s1, s14, s13
 8001b20:	eed7 7a25 	vfnms.f32	s15, s14, s11
 8001b24:	ee77 7ae0 	vsub.f32	s15, s15, s1
 8001b28:	ee36 0a67 	vsub.f32	s0, s12, s15
 8001b2c:	4770      	bx	lr
 8001b2e:	eeb6 6a07 	vmov.f32	s12, #103	; 0x3f380000  0.7187500
 8001b32:	eef5 7a02 	vmov.f32	s15, #82	; 0x3e900000  0.2812500
 8001b36:	e7ed      	b.n	8001b14 <__kernel_cosf+0xb8>
 8001b38:	eeb7 0a00 	vmov.f32	s0, #112	; 0x3f800000  1.0
 8001b3c:	4770      	bx	lr
 8001b3e:	bf00      	nop
 8001b40:	ad47d74e 	.word	0xad47d74e
 8001b44:	310f74f6 	.word	0x310f74f6
 8001b48:	b493f27c 	.word	0xb493f27c
 8001b4c:	37d00d01 	.word	0x37d00d01
 8001b50:	bab60b61 	.word	0xbab60b61
 8001b54:	3d2aaaab 	.word	0x3d2aaaab
 8001b58:	3e999999 	.word	0x3e999999
 8001b5c:	3f480000 	.word	0x3f480000

08001b60 <__kernel_rem_pio2f>:
 8001b60:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8001b64:	ed2d 8b04 	vpush	{d8-d9}
 8001b68:	b0d9      	sub	sp, #356	; 0x164
 8001b6a:	f103 3aff 	add.w	sl, r3, #4294967295
 8001b6e:	9304      	str	r3, [sp, #16]
 8001b70:	1ed3      	subs	r3, r2, #3
 8001b72:	bf48      	it	mi
 8001b74:	1d13      	addmi	r3, r2, #4
 8001b76:	4cd4      	ldr	r4, [pc, #848]	; (8001ec8 <__kernel_rem_pio2f+0x368>)
 8001b78:	9d66      	ldr	r5, [sp, #408]	; 0x198
 8001b7a:	9102      	str	r1, [sp, #8]
 8001b7c:	10db      	asrs	r3, r3, #3
 8001b7e:	ea23 73e3 	bic.w	r3, r3, r3, asr #31
 8001b82:	4619      	mov	r1, r3
 8001b84:	f854 6025 	ldr.w	r6, [r4, r5, lsl #2]
 8001b88:	9306      	str	r3, [sp, #24]
 8001b8a:	3301      	adds	r3, #1
 8001b8c:	eba2 03c3 	sub.w	r3, r2, r3, lsl #3
 8001b90:	9301      	str	r3, [sp, #4]
 8001b92:	eba1 030a 	sub.w	r3, r1, sl
 8001b96:	eb16 010a 	adds.w	r1, r6, sl
 8001b9a:	4681      	mov	r9, r0
 8001b9c:	d416      	bmi.n	8001bcc <__kernel_rem_pio2f+0x6c>
 8001b9e:	4419      	add	r1, r3
 8001ba0:	ed9f 7aca 	vldr	s14, [pc, #808]	; 8001ecc <__kernel_rem_pio2f+0x36c>
 8001ba4:	9867      	ldr	r0, [sp, #412]	; 0x19c
 8001ba6:	3101      	adds	r1, #1
 8001ba8:	aa1c      	add	r2, sp, #112	; 0x70
 8001baa:	2b00      	cmp	r3, #0
 8001bac:	bfa4      	itt	ge
 8001bae:	f850 4023 	ldrge.w	r4, [r0, r3, lsl #2]
 8001bb2:	ee07 4a90 	vmovge	s15, r4
 8001bb6:	f103 0301 	add.w	r3, r3, #1
 8001bba:	bfac      	ite	ge
 8001bbc:	eef8 7ae7 	vcvtge.f32.s32	s15, s15
 8001bc0:	eef0 7a47 	vmovlt.f32	s15, s14
 8001bc4:	428b      	cmp	r3, r1
 8001bc6:	ece2 7a01 	vstmia	r2!, {s15}
 8001bca:	d1ee      	bne.n	8001baa <__kernel_rem_pio2f+0x4a>
 8001bcc:	2e00      	cmp	r6, #0
 8001bce:	f2c0 8307 	blt.w	80021e0 <__kernel_rem_pio2f+0x680>
 8001bd2:	9b04      	ldr	r3, [sp, #16]
 8001bd4:	ad44      	add	r5, sp, #272	; 0x110
 8001bd6:	009c      	lsls	r4, r3, #2
 8001bd8:	1c77      	adds	r7, r6, #1
 8001bda:	ab1c      	add	r3, sp, #112	; 0x70
 8001bdc:	eb05 0787 	add.w	r7, r5, r7, lsl #2
 8001be0:	1918      	adds	r0, r3, r4
 8001be2:	eb09 0104 	add.w	r1, r9, r4
 8001be6:	f1ba 0f00 	cmp.w	sl, #0
 8001bea:	f2c0 8275 	blt.w	80020d8 <__kernel_rem_pio2f+0x578>
 8001bee:	eddf 7ab7 	vldr	s15, [pc, #732]	; 8001ecc <__kernel_rem_pio2f+0x36c>
 8001bf2:	464b      	mov	r3, r9
 8001bf4:	4602      	mov	r2, r0
 8001bf6:	ecf3 6a01 	vldmia	r3!, {s13}
 8001bfa:	ed32 7a01 	vldmdb	r2!, {s14}
 8001bfe:	428b      	cmp	r3, r1
 8001c00:	eee6 7a87 	vfma.f32	s15, s13, s14
 8001c04:	d1f7      	bne.n	8001bf6 <__kernel_rem_pio2f+0x96>
 8001c06:	ece5 7a01 	vstmia	r5!, {s15}
 8001c0a:	42bd      	cmp	r5, r7
 8001c0c:	f100 0004 	add.w	r0, r0, #4
 8001c10:	d1e9      	bne.n	8001be6 <__kernel_rem_pio2f+0x86>
 8001c12:	f106 4380 	add.w	r3, r6, #1073741824	; 0x40000000
 8001c16:	3b01      	subs	r3, #1
 8001c18:	009b      	lsls	r3, r3, #2
 8001c1a:	ad08      	add	r5, sp, #32
 8001c1c:	1f1a      	subs	r2, r3, #4
 8001c1e:	18aa      	adds	r2, r5, r2
 8001c20:	46b3      	mov	fp, r6
 8001c22:	9603      	str	r6, [sp, #12]
 8001c24:	eddf 8aab 	vldr	s17, [pc, #684]	; 8001ed4 <__kernel_rem_pio2f+0x374>
 8001c28:	ed9f 8aa9 	vldr	s16, [pc, #676]	; 8001ed0 <__kernel_rem_pio2f+0x370>
 8001c2c:	9e01      	ldr	r6, [sp, #4]
 8001c2e:	9207      	str	r2, [sp, #28]
 8001c30:	444c      	add	r4, r9
 8001c32:	eb05 0803 	add.w	r8, r5, r3
 8001c36:	af44      	add	r7, sp, #272	; 0x110
 8001c38:	ea4f 038b 	mov.w	r3, fp, lsl #2
 8001c3c:	aa58      	add	r2, sp, #352	; 0x160
 8001c3e:	441a      	add	r2, r3
 8001c40:	f1bb 0f00 	cmp.w	fp, #0
 8001c44:	ed12 0a14 	vldr	s0, [r2, #-80]	; 0xffffffb0
 8001c48:	dd18      	ble.n	8001c7c <__kernel_rem_pio2f+0x11c>
 8001c4a:	a907      	add	r1, sp, #28
 8001c4c:	eb07 028b 	add.w	r2, r7, fp, lsl #2
 8001c50:	ee60 7a28 	vmul.f32	s15, s0, s17
 8001c54:	eeb0 7a40 	vmov.f32	s14, s0
 8001c58:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 8001c5c:	ed72 6a01 	vldmdb	r2!, {s13}
 8001c60:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 8001c64:	42ba      	cmp	r2, r7
 8001c66:	eea7 7ac8 	vfms.f32	s14, s15, s16
 8001c6a:	ee37 0aa6 	vadd.f32	s0, s15, s13
 8001c6e:	eebd 7ac7 	vcvt.s32.f32	s14, s14
 8001c72:	ee17 0a10 	vmov	r0, s14
 8001c76:	f841 0f04 	str.w	r0, [r1, #4]!
 8001c7a:	d1e9      	bne.n	8001c50 <__kernel_rem_pio2f+0xf0>
 8001c7c:	4630      	mov	r0, r6
 8001c7e:	9305      	str	r3, [sp, #20]
 8001c80:	f000 fd04 	bl	800268c <scalbnf>
 8001c84:	eeb0 9a40 	vmov.f32	s18, s0
 8001c88:	eeb4 0a00 	vmov.f32	s0, #64	; 0x3e000000  0.125
 8001c8c:	ee29 0a00 	vmul.f32	s0, s18, s0
 8001c90:	f000 fcb4 	bl	80025fc <floorf>
 8001c94:	eef2 7a00 	vmov.f32	s15, #32	; 0x41000000  8.0
 8001c98:	eea0 9a67 	vfms.f32	s18, s0, s15
 8001c9c:	2e00      	cmp	r6, #0
 8001c9e:	9b05      	ldr	r3, [sp, #20]
 8001ca0:	eefd 9ac9 	vcvt.s32.f32	s19, s18
 8001ca4:	eef8 7ae9 	vcvt.f32.s32	s15, s19
 8001ca8:	ee39 9a67 	vsub.f32	s18, s18, s15
 8001cac:	f340 80c9 	ble.w	8001e42 <__kernel_rem_pio2f+0x2e2>
 8001cb0:	f10b 3eff 	add.w	lr, fp, #4294967295
 8001cb4:	f1c6 0208 	rsb	r2, r6, #8
 8001cb8:	f855 102e 	ldr.w	r1, [r5, lr, lsl #2]
 8001cbc:	fa41 f002 	asr.w	r0, r1, r2
 8001cc0:	fa00 f202 	lsl.w	r2, r0, r2
 8001cc4:	1a89      	subs	r1, r1, r2
 8001cc6:	f845 102e 	str.w	r1, [r5, lr, lsl #2]
 8001cca:	ee19 ea90 	vmov	lr, s19
 8001cce:	4486      	add	lr, r0
 8001cd0:	f1c6 0207 	rsb	r2, r6, #7
 8001cd4:	ee09 ea90 	vmov	s19, lr
 8001cd8:	4111      	asrs	r1, r2
 8001cda:	2900      	cmp	r1, #0
 8001cdc:	dd51      	ble.n	8001d82 <__kernel_rem_pio2f+0x222>
 8001cde:	ee19 2a90 	vmov	r2, s19
 8001ce2:	f1bb 0f00 	cmp.w	fp, #0
 8001ce6:	f102 0201 	add.w	r2, r2, #1
 8001cea:	ee09 2a90 	vmov	s19, r2
 8001cee:	f340 8212 	ble.w	8002116 <__kernel_rem_pio2f+0x5b6>
 8001cf2:	682a      	ldr	r2, [r5, #0]
 8001cf4:	2a00      	cmp	r2, #0
 8001cf6:	f040 8269 	bne.w	80021cc <__kernel_rem_pio2f+0x66c>
 8001cfa:	f1bb 0f01 	cmp.w	fp, #1
 8001cfe:	f340 8229 	ble.w	8002154 <__kernel_rem_pio2f+0x5f4>
 8001d02:	46ae      	mov	lr, r5
 8001d04:	2001      	movs	r0, #1
 8001d06:	f85e 2f04 	ldr.w	r2, [lr, #4]!
 8001d0a:	2a00      	cmp	r2, #0
 8001d0c:	f000 821e 	beq.w	800214c <__kernel_rem_pio2f+0x5ec>
 8001d10:	f100 0e01 	add.w	lr, r0, #1
 8001d14:	f5c2 7280 	rsb	r2, r2, #256	; 0x100
 8001d18:	45f3      	cmp	fp, lr
 8001d1a:	f845 2020 	str.w	r2, [r5, r0, lsl #2]
 8001d1e:	dd12      	ble.n	8001d46 <__kernel_rem_pio2f+0x1e6>
 8001d20:	f855 202e 	ldr.w	r2, [r5, lr, lsl #2]
 8001d24:	3002      	adds	r0, #2
 8001d26:	f1c2 02ff 	rsb	r2, r2, #255	; 0xff
 8001d2a:	4583      	cmp	fp, r0
 8001d2c:	f845 202e 	str.w	r2, [r5, lr, lsl #2]
 8001d30:	dd09      	ble.n	8001d46 <__kernel_rem_pio2f+0x1e6>
 8001d32:	eb05 0080 	add.w	r0, r5, r0, lsl #2
 8001d36:	18ea      	adds	r2, r5, r3
 8001d38:	6803      	ldr	r3, [r0, #0]
 8001d3a:	f1c3 03ff 	rsb	r3, r3, #255	; 0xff
 8001d3e:	f840 3b04 	str.w	r3, [r0], #4
 8001d42:	4290      	cmp	r0, r2
 8001d44:	d1f8      	bne.n	8001d38 <__kernel_rem_pio2f+0x1d8>
 8001d46:	2e00      	cmp	r6, #0
 8001d48:	dd0d      	ble.n	8001d66 <__kernel_rem_pio2f+0x206>
 8001d4a:	2e01      	cmp	r6, #1
 8001d4c:	f04f 0201 	mov.w	r2, #1
 8001d50:	f040 81e7 	bne.w	8002122 <__kernel_rem_pio2f+0x5c2>
 8001d54:	f10b 30ff 	add.w	r0, fp, #4294967295
 8001d58:	f855 3020 	ldr.w	r3, [r5, r0, lsl #2]
 8001d5c:	f003 037f 	and.w	r3, r3, #127	; 0x7f
 8001d60:	f845 3020 	str.w	r3, [r5, r0, lsl #2]
 8001d64:	e1e7      	b.n	8002136 <__kernel_rem_pio2f+0x5d6>
 8001d66:	2902      	cmp	r1, #2
 8001d68:	d10b      	bne.n	8001d82 <__kernel_rem_pio2f+0x222>
 8001d6a:	eef7 7a00 	vmov.f32	s15, #112	; 0x3f800000  1.0
 8001d6e:	ee37 9ac9 	vsub.f32	s18, s15, s18
 8001d72:	4630      	mov	r0, r6
 8001d74:	eeb7 0a00 	vmov.f32	s0, #112	; 0x3f800000  1.0
 8001d78:	f000 fc88 	bl	800268c <scalbnf>
 8001d7c:	ee39 9a40 	vsub.f32	s18, s18, s0
 8001d80:	2102      	movs	r1, #2
 8001d82:	eeb5 9a40 	vcmp.f32	s18, #0.0
 8001d86:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8001d8a:	d170      	bne.n	8001e6e <__kernel_rem_pio2f+0x30e>
 8001d8c:	9b03      	ldr	r3, [sp, #12]
 8001d8e:	f10b 3eff 	add.w	lr, fp, #4294967295
 8001d92:	4573      	cmp	r3, lr
 8001d94:	dc0e      	bgt.n	8001db4 <__kernel_rem_pio2f+0x254>
 8001d96:	f10b 4280 	add.w	r2, fp, #1073741824	; 0x40000000
 8001d9a:	3a01      	subs	r2, #1
 8001d9c:	eb05 0282 	add.w	r2, r5, r2, lsl #2
 8001da0:	2000      	movs	r0, #0
 8001da2:	f852 3904 	ldr.w	r3, [r2], #-4
 8001da6:	4542      	cmp	r2, r8
 8001da8:	ea40 0003 	orr.w	r0, r0, r3
 8001dac:	d1f9      	bne.n	8001da2 <__kernel_rem_pio2f+0x242>
 8001dae:	2800      	cmp	r0, #0
 8001db0:	f040 8094 	bne.w	8001edc <__kernel_rem_pio2f+0x37c>
 8001db4:	9b03      	ldr	r3, [sp, #12]
 8001db6:	3b01      	subs	r3, #1
 8001db8:	f855 3023 	ldr.w	r3, [r5, r3, lsl #2]
 8001dbc:	2b00      	cmp	r3, #0
 8001dbe:	f040 81d3 	bne.w	8002168 <__kernel_rem_pio2f+0x608>
 8001dc2:	9b07      	ldr	r3, [sp, #28]
 8001dc4:	f04f 0c01 	mov.w	ip, #1
 8001dc8:	f853 2904 	ldr.w	r2, [r3], #-4
 8001dcc:	f10c 0c01 	add.w	ip, ip, #1
 8001dd0:	2a00      	cmp	r2, #0
 8001dd2:	d0f9      	beq.n	8001dc8 <__kernel_rem_pio2f+0x268>
 8001dd4:	44dc      	add	ip, fp
 8001dd6:	f10b 0e01 	add.w	lr, fp, #1
 8001dda:	45e6      	cmp	lr, ip
 8001ddc:	dc2f      	bgt.n	8001e3e <__kernel_rem_pio2f+0x2de>
 8001dde:	9a06      	ldr	r2, [sp, #24]
 8001de0:	9b04      	ldr	r3, [sp, #16]
 8001de2:	eb0e 0102 	add.w	r1, lr, r2
 8001de6:	eb03 000b 	add.w	r0, r3, fp
 8001dea:	eb0c 0302 	add.w	r3, ip, r2
 8001dee:	aa1c      	add	r2, sp, #112	; 0x70
 8001df0:	f101 4180 	add.w	r1, r1, #1073741824	; 0x40000000
 8001df4:	eb02 0080 	add.w	r0, r2, r0, lsl #2
 8001df8:	9a67      	ldr	r2, [sp, #412]	; 0x19c
 8001dfa:	3901      	subs	r1, #1
 8001dfc:	eb02 0181 	add.w	r1, r2, r1, lsl #2
 8001e00:	eb07 0e8e 	add.w	lr, r7, lr, lsl #2
 8001e04:	eb02 0b83 	add.w	fp, r2, r3, lsl #2
 8001e08:	f851 3f04 	ldr.w	r3, [r1, #4]!
 8001e0c:	ee07 3a90 	vmov	s15, r3
 8001e10:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 8001e14:	f1ba 0f00 	cmp.w	sl, #0
 8001e18:	ece0 7a01 	vstmia	r0!, {s15}
 8001e1c:	eddf 7a2b 	vldr	s15, [pc, #172]	; 8001ecc <__kernel_rem_pio2f+0x36c>
 8001e20:	db09      	blt.n	8001e36 <__kernel_rem_pio2f+0x2d6>
 8001e22:	464b      	mov	r3, r9
 8001e24:	4602      	mov	r2, r0
 8001e26:	ecf3 6a01 	vldmia	r3!, {s13}
 8001e2a:	ed32 7a01 	vldmdb	r2!, {s14}
 8001e2e:	42a3      	cmp	r3, r4
 8001e30:	eee6 7a87 	vfma.f32	s15, s13, s14
 8001e34:	d1f7      	bne.n	8001e26 <__kernel_rem_pio2f+0x2c6>
 8001e36:	4559      	cmp	r1, fp
 8001e38:	ecee 7a01 	vstmia	lr!, {s15}
 8001e3c:	d1e4      	bne.n	8001e08 <__kernel_rem_pio2f+0x2a8>
 8001e3e:	46e3      	mov	fp, ip
 8001e40:	e6fa      	b.n	8001c38 <__kernel_rem_pio2f+0xd8>
 8001e42:	d105      	bne.n	8001e50 <__kernel_rem_pio2f+0x2f0>
 8001e44:	f10b 32ff 	add.w	r2, fp, #4294967295
 8001e48:	f855 1022 	ldr.w	r1, [r5, r2, lsl #2]
 8001e4c:	1209      	asrs	r1, r1, #8
 8001e4e:	e744      	b.n	8001cda <__kernel_rem_pio2f+0x17a>
 8001e50:	eef6 7a00 	vmov.f32	s15, #96	; 0x3f000000  0.5
 8001e54:	eeb4 9ae7 	vcmpe.f32	s18, s15
 8001e58:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8001e5c:	f280 8146 	bge.w	80020ec <__kernel_rem_pio2f+0x58c>
 8001e60:	eeb5 9a40 	vcmp.f32	s18, #0.0
 8001e64:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8001e68:	f04f 0100 	mov.w	r1, #0
 8001e6c:	d08e      	beq.n	8001d8c <__kernel_rem_pio2f+0x22c>
 8001e6e:	9c01      	ldr	r4, [sp, #4]
 8001e70:	9e03      	ldr	r6, [sp, #12]
 8001e72:	eeb0 0a49 	vmov.f32	s0, s18
 8001e76:	4260      	negs	r0, r4
 8001e78:	4688      	mov	r8, r1
 8001e7a:	f000 fc07 	bl	800268c <scalbnf>
 8001e7e:	ed9f 7a14 	vldr	s14, [pc, #80]	; 8001ed0 <__kernel_rem_pio2f+0x370>
 8001e82:	eeb4 0ac7 	vcmpe.f32	s0, s14
 8001e86:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8001e8a:	f2c0 8185 	blt.w	8002198 <__kernel_rem_pio2f+0x638>
 8001e8e:	eddf 7a11 	vldr	s15, [pc, #68]	; 8001ed4 <__kernel_rem_pio2f+0x374>
 8001e92:	ee60 7a27 	vmul.f32	s15, s0, s15
 8001e96:	4623      	mov	r3, r4
 8001e98:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 8001e9c:	3308      	adds	r3, #8
 8001e9e:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 8001ea2:	9301      	str	r3, [sp, #4]
 8001ea4:	eea7 0ac7 	vfms.f32	s0, s15, s14
 8001ea8:	f10b 0301 	add.w	r3, fp, #1
 8001eac:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 8001eb0:	eebd 0ac0 	vcvt.s32.f32	s0, s0
 8001eb4:	ee10 2a10 	vmov	r2, s0
 8001eb8:	f845 202b 	str.w	r2, [r5, fp, lsl #2]
 8001ebc:	ee17 2a90 	vmov	r2, s15
 8001ec0:	f845 2023 	str.w	r2, [r5, r3, lsl #2]
 8001ec4:	e01f      	b.n	8001f06 <__kernel_rem_pio2f+0x3a6>
 8001ec6:	bf00      	nop
 8001ec8:	08009c74 	.word	0x08009c74
 8001ecc:	00000000 	.word	0x00000000
 8001ed0:	43800000 	.word	0x43800000
 8001ed4:	3b800000 	.word	0x3b800000
 8001ed8:	3fc90000 	.word	0x3fc90000
 8001edc:	4688      	mov	r8, r1
 8001ede:	9901      	ldr	r1, [sp, #4]
 8001ee0:	f855 202e 	ldr.w	r2, [r5, lr, lsl #2]
 8001ee4:	9e03      	ldr	r6, [sp, #12]
 8001ee6:	3908      	subs	r1, #8
 8001ee8:	4673      	mov	r3, lr
 8001eea:	9101      	str	r1, [sp, #4]
 8001eec:	b95a      	cbnz	r2, 8001f06 <__kernel_rem_pio2f+0x3a6>
 8001eee:	f10e 4280 	add.w	r2, lr, #1073741824	; 0x40000000
 8001ef2:	3a01      	subs	r2, #1
 8001ef4:	eb05 0282 	add.w	r2, r5, r2, lsl #2
 8001ef8:	f852 0904 	ldr.w	r0, [r2], #-4
 8001efc:	3b01      	subs	r3, #1
 8001efe:	3908      	subs	r1, #8
 8001f00:	2800      	cmp	r0, #0
 8001f02:	d0f9      	beq.n	8001ef8 <__kernel_rem_pio2f+0x398>
 8001f04:	9101      	str	r1, [sp, #4]
 8001f06:	9801      	ldr	r0, [sp, #4]
 8001f08:	9303      	str	r3, [sp, #12]
 8001f0a:	eeb7 0a00 	vmov.f32	s0, #112	; 0x3f800000  1.0
 8001f0e:	f000 fbbd 	bl	800268c <scalbnf>
 8001f12:	9b03      	ldr	r3, [sp, #12]
 8001f14:	2b00      	cmp	r3, #0
 8001f16:	f2c0 814a 	blt.w	80021ae <__kernel_rem_pio2f+0x64e>
 8001f1a:	009f      	lsls	r7, r3, #2
 8001f1c:	aa44      	add	r2, sp, #272	; 0x110
 8001f1e:	19d0      	adds	r0, r2, r7
 8001f20:	f107 0e04 	add.w	lr, r7, #4
 8001f24:	ed1f 7a15 	vldr	s14, [pc, #-84]	; 8001ed4 <__kernel_rem_pio2f+0x374>
 8001f28:	eb05 020e 	add.w	r2, r5, lr
 8001f2c:	1d01      	adds	r1, r0, #4
 8001f2e:	ed72 7a01 	vldmdb	r2!, {s15}
 8001f32:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 8001f36:	42aa      	cmp	r2, r5
 8001f38:	ee67 7a80 	vmul.f32	s15, s15, s0
 8001f3c:	ee20 0a07 	vmul.f32	s0, s0, s14
 8001f40:	ed61 7a01 	vstmdb	r1!, {s15}
 8001f44:	d1f3      	bne.n	8001f2e <__kernel_rem_pio2f+0x3ce>
 8001f46:	ed1f 6a1c 	vldr	s12, [pc, #-112]	; 8001ed8 <__kernel_rem_pio2f+0x378>
 8001f4a:	4605      	mov	r5, r0
 8001f4c:	f50d 7c86 	add.w	ip, sp, #268	; 0x10c
 8001f50:	2400      	movs	r4, #0
 8001f52:	2e00      	cmp	r6, #0
 8001f54:	f2c0 80dc 	blt.w	8002110 <__kernel_rem_pio2f+0x5b0>
 8001f58:	48a3      	ldr	r0, [pc, #652]	; (80021e8 <__kernel_rem_pio2f+0x688>)
 8001f5a:	eddf 7aa4 	vldr	s15, [pc, #656]	; 80021ec <__kernel_rem_pio2f+0x68c>
 8001f5e:	4629      	mov	r1, r5
 8001f60:	eeb0 7a46 	vmov.f32	s14, s12
 8001f64:	2200      	movs	r2, #0
 8001f66:	e003      	b.n	8001f70 <__kernel_rem_pio2f+0x410>
 8001f68:	42a2      	cmp	r2, r4
 8001f6a:	dc08      	bgt.n	8001f7e <__kernel_rem_pio2f+0x41e>
 8001f6c:	ecb0 7a01 	vldmia	r0!, {s14}
 8001f70:	ecf1 6a01 	vldmia	r1!, {s13}
 8001f74:	3201      	adds	r2, #1
 8001f76:	4296      	cmp	r6, r2
 8001f78:	eee7 7a26 	vfma.f32	s15, s14, s13
 8001f7c:	daf4      	bge.n	8001f68 <__kernel_rem_pio2f+0x408>
 8001f7e:	3d04      	subs	r5, #4
 8001f80:	aa58      	add	r2, sp, #352	; 0x160
 8001f82:	eb02 0284 	add.w	r2, r2, r4, lsl #2
 8001f86:	4565      	cmp	r5, ip
 8001f88:	ed42 7a28 	vstr	s15, [r2, #-160]	; 0xffffff60
 8001f8c:	f104 0401 	add.w	r4, r4, #1
 8001f90:	d1df      	bne.n	8001f52 <__kernel_rem_pio2f+0x3f2>
 8001f92:	9a66      	ldr	r2, [sp, #408]	; 0x198
 8001f94:	2a03      	cmp	r2, #3
 8001f96:	d85a      	bhi.n	800204e <__kernel_rem_pio2f+0x4ee>
 8001f98:	e8df f002 	tbb	[pc, r2]
 8001f9c:	0262628c 	.word	0x0262628c
 8001fa0:	2b00      	cmp	r3, #0
 8001fa2:	f340 8101 	ble.w	80021a8 <__kernel_rem_pio2f+0x648>
 8001fa6:	aa58      	add	r2, sp, #352	; 0x160
 8001fa8:	443a      	add	r2, r7
 8001faa:	a930      	add	r1, sp, #192	; 0xc0
 8001fac:	ed12 7a28 	vldr	s14, [r2, #-160]	; 0xffffff60
 8001fb0:	1d3a      	adds	r2, r7, #4
 8001fb2:	440a      	add	r2, r1
 8001fb4:	a831      	add	r0, sp, #196	; 0xc4
 8001fb6:	4439      	add	r1, r7
 8001fb8:	ed51 7a01 	vldr	s15, [r1, #-4]
 8001fbc:	ee77 6a87 	vadd.f32	s13, s15, s14
 8001fc0:	ee77 7ae6 	vsub.f32	s15, s15, s13
 8001fc4:	ee77 7a87 	vadd.f32	s15, s15, s14
 8001fc8:	eeb0 7a66 	vmov.f32	s14, s13
 8001fcc:	ed62 7a01 	vstmdb	r2!, {s15}
 8001fd0:	4282      	cmp	r2, r0
 8001fd2:	ed61 6a01 	vstmdb	r1!, {s13}
 8001fd6:	d1ef      	bne.n	8001fb8 <__kernel_rem_pio2f+0x458>
 8001fd8:	2b01      	cmp	r3, #1
 8001fda:	f340 80e5 	ble.w	80021a8 <__kernel_rem_pio2f+0x648>
 8001fde:	ab58      	add	r3, sp, #352	; 0x160
 8001fe0:	443b      	add	r3, r7
 8001fe2:	aa30      	add	r2, sp, #192	; 0xc0
 8001fe4:	ed13 7a28 	vldr	s14, [r3, #-160]	; 0xffffff60
 8001fe8:	1d3b      	adds	r3, r7, #4
 8001fea:	a932      	add	r1, sp, #200	; 0xc8
 8001fec:	4417      	add	r7, r2
 8001fee:	441a      	add	r2, r3
 8001ff0:	ed57 7a01 	vldr	s15, [r7, #-4]
 8001ff4:	ee77 6a87 	vadd.f32	s13, s15, s14
 8001ff8:	ee77 7ae6 	vsub.f32	s15, s15, s13
 8001ffc:	ee77 7a87 	vadd.f32	s15, s15, s14
 8002000:	eeb0 7a66 	vmov.f32	s14, s13
 8002004:	ed62 7a01 	vstmdb	r2!, {s15}
 8002008:	4291      	cmp	r1, r2
 800200a:	ed67 6a01 	vstmdb	r7!, {s13}
 800200e:	d1ef      	bne.n	8001ff0 <__kernel_rem_pio2f+0x490>
 8002010:	a930      	add	r1, sp, #192	; 0xc0
 8002012:	eddf 7a76 	vldr	s15, [pc, #472]	; 80021ec <__kernel_rem_pio2f+0x68c>
 8002016:	440b      	add	r3, r1
 8002018:	ed33 7a01 	vldmdb	r3!, {s14}
 800201c:	429a      	cmp	r2, r3
 800201e:	ee77 7a87 	vadd.f32	s15, s15, s14
 8002022:	d1f9      	bne.n	8002018 <__kernel_rem_pio2f+0x4b8>
 8002024:	4643      	mov	r3, r8
 8002026:	2b00      	cmp	r3, #0
 8002028:	f000 80ae 	beq.w	8002188 <__kernel_rem_pio2f+0x628>
 800202c:	eddd 6a30 	vldr	s13, [sp, #192]	; 0xc0
 8002030:	ed9d 7a31 	vldr	s14, [sp, #196]	; 0xc4
 8002034:	9a02      	ldr	r2, [sp, #8]
 8002036:	eef1 7a67 	vneg.f32	s15, s15
 800203a:	eef1 6a66 	vneg.f32	s13, s13
 800203e:	eeb1 7a47 	vneg.f32	s14, s14
 8002042:	edc2 7a02 	vstr	s15, [r2, #8]
 8002046:	edc2 6a00 	vstr	s13, [r2]
 800204a:	ed82 7a01 	vstr	s14, [r2, #4]
 800204e:	ee19 3a90 	vmov	r3, s19
 8002052:	f003 0007 	and.w	r0, r3, #7
 8002056:	b059      	add	sp, #356	; 0x164
 8002058:	ecbd 8b04 	vpop	{d8-d9}
 800205c:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8002060:	1d3a      	adds	r2, r7, #4
 8002062:	a930      	add	r1, sp, #192	; 0xc0
 8002064:	eddf 7a61 	vldr	s15, [pc, #388]	; 80021ec <__kernel_rem_pio2f+0x68c>
 8002068:	440a      	add	r2, r1
 800206a:	ed32 7a01 	vldmdb	r2!, {s14}
 800206e:	428a      	cmp	r2, r1
 8002070:	ee77 7a87 	vadd.f32	s15, s15, s14
 8002074:	d1f9      	bne.n	800206a <__kernel_rem_pio2f+0x50a>
 8002076:	4642      	mov	r2, r8
 8002078:	2a00      	cmp	r2, #0
 800207a:	d078      	beq.n	800216e <__kernel_rem_pio2f+0x60e>
 800207c:	eddd 6a30 	vldr	s13, [sp, #192]	; 0xc0
 8002080:	9a02      	ldr	r2, [sp, #8]
 8002082:	eeb1 7a67 	vneg.f32	s14, s15
 8002086:	2b00      	cmp	r3, #0
 8002088:	ee76 7ae7 	vsub.f32	s15, s13, s15
 800208c:	ed82 7a00 	vstr	s14, [r2]
 8002090:	dd0a      	ble.n	80020a8 <__kernel_rem_pio2f+0x548>
 8002092:	a931      	add	r1, sp, #196	; 0xc4
 8002094:	2201      	movs	r2, #1
 8002096:	ecb1 7a01 	vldmia	r1!, {s14}
 800209a:	3201      	adds	r2, #1
 800209c:	4293      	cmp	r3, r2
 800209e:	ee77 7a87 	vadd.f32	s15, s15, s14
 80020a2:	daf8      	bge.n	8002096 <__kernel_rem_pio2f+0x536>
 80020a4:	4643      	mov	r3, r8
 80020a6:	b10b      	cbz	r3, 80020ac <__kernel_rem_pio2f+0x54c>
 80020a8:	eef1 7a67 	vneg.f32	s15, s15
 80020ac:	9b02      	ldr	r3, [sp, #8]
 80020ae:	edc3 7a01 	vstr	s15, [r3, #4]
 80020b2:	e7cc      	b.n	800204e <__kernel_rem_pio2f+0x4ee>
 80020b4:	ab30      	add	r3, sp, #192	; 0xc0
 80020b6:	eddf 7a4d 	vldr	s15, [pc, #308]	; 80021ec <__kernel_rem_pio2f+0x68c>
 80020ba:	449e      	add	lr, r3
 80020bc:	ed3e 7a01 	vldmdb	lr!, {s14}
 80020c0:	4573      	cmp	r3, lr
 80020c2:	ee77 7a87 	vadd.f32	s15, s15, s14
 80020c6:	d1f9      	bne.n	80020bc <__kernel_rem_pio2f+0x55c>
 80020c8:	4643      	mov	r3, r8
 80020ca:	b10b      	cbz	r3, 80020d0 <__kernel_rem_pio2f+0x570>
 80020cc:	eef1 7a67 	vneg.f32	s15, s15
 80020d0:	9b02      	ldr	r3, [sp, #8]
 80020d2:	edc3 7a00 	vstr	s15, [r3]
 80020d6:	e7ba      	b.n	800204e <__kernel_rem_pio2f+0x4ee>
 80020d8:	eddf 7a44 	vldr	s15, [pc, #272]	; 80021ec <__kernel_rem_pio2f+0x68c>
 80020dc:	ece5 7a01 	vstmia	r5!, {s15}
 80020e0:	42bd      	cmp	r5, r7
 80020e2:	f100 0004 	add.w	r0, r0, #4
 80020e6:	f47f ad7e 	bne.w	8001be6 <__kernel_rem_pio2f+0x86>
 80020ea:	e592      	b.n	8001c12 <__kernel_rem_pio2f+0xb2>
 80020ec:	ee19 2a90 	vmov	r2, s19
 80020f0:	f1bb 0f00 	cmp.w	fp, #0
 80020f4:	f102 0201 	add.w	r2, r2, #1
 80020f8:	ee09 2a90 	vmov	s19, r2
 80020fc:	bfc8      	it	gt
 80020fe:	2102      	movgt	r1, #2
 8002100:	f73f adf7 	bgt.w	8001cf2 <__kernel_rem_pio2f+0x192>
 8002104:	eef7 7a00 	vmov.f32	s15, #112	; 0x3f800000  1.0
 8002108:	ee37 9ac9 	vsub.f32	s18, s15, s18
 800210c:	2102      	movs	r1, #2
 800210e:	e638      	b.n	8001d82 <__kernel_rem_pio2f+0x222>
 8002110:	eddf 7a36 	vldr	s15, [pc, #216]	; 80021ec <__kernel_rem_pio2f+0x68c>
 8002114:	e733      	b.n	8001f7e <__kernel_rem_pio2f+0x41e>
 8002116:	2e00      	cmp	r6, #0
 8002118:	dd1e      	ble.n	8002158 <__kernel_rem_pio2f+0x5f8>
 800211a:	2200      	movs	r2, #0
 800211c:	2e01      	cmp	r6, #1
 800211e:	f43f ae19 	beq.w	8001d54 <__kernel_rem_pio2f+0x1f4>
 8002122:	2e02      	cmp	r6, #2
 8002124:	d107      	bne.n	8002136 <__kernel_rem_pio2f+0x5d6>
 8002126:	f10b 30ff 	add.w	r0, fp, #4294967295
 800212a:	f855 3020 	ldr.w	r3, [r5, r0, lsl #2]
 800212e:	f003 033f 	and.w	r3, r3, #63	; 0x3f
 8002132:	f845 3020 	str.w	r3, [r5, r0, lsl #2]
 8002136:	2902      	cmp	r1, #2
 8002138:	f47f ae23 	bne.w	8001d82 <__kernel_rem_pio2f+0x222>
 800213c:	eef7 7a00 	vmov.f32	s15, #112	; 0x3f800000  1.0
 8002140:	ee37 9ac9 	vsub.f32	s18, s15, s18
 8002144:	2a00      	cmp	r2, #0
 8002146:	f43f ae1c 	beq.w	8001d82 <__kernel_rem_pio2f+0x222>
 800214a:	e612      	b.n	8001d72 <__kernel_rem_pio2f+0x212>
 800214c:	3001      	adds	r0, #1
 800214e:	4583      	cmp	fp, r0
 8002150:	f47f add9 	bne.w	8001d06 <__kernel_rem_pio2f+0x1a6>
 8002154:	2e00      	cmp	r6, #0
 8002156:	dce1      	bgt.n	800211c <__kernel_rem_pio2f+0x5bc>
 8002158:	2902      	cmp	r1, #2
 800215a:	f47f ae12 	bne.w	8001d82 <__kernel_rem_pio2f+0x222>
 800215e:	eef7 7a00 	vmov.f32	s15, #112	; 0x3f800000  1.0
 8002162:	ee37 9ac9 	vsub.f32	s18, s15, s18
 8002166:	e60c      	b.n	8001d82 <__kernel_rem_pio2f+0x222>
 8002168:	f04f 0c01 	mov.w	ip, #1
 800216c:	e632      	b.n	8001dd4 <__kernel_rem_pio2f+0x274>
 800216e:	ed9d 7a30 	vldr	s14, [sp, #192]	; 0xc0
 8002172:	9a02      	ldr	r2, [sp, #8]
 8002174:	2b00      	cmp	r3, #0
 8002176:	edc2 7a00 	vstr	s15, [r2]
 800217a:	ee77 7a67 	vsub.f32	s15, s14, s15
 800217e:	dc88      	bgt.n	8002092 <__kernel_rem_pio2f+0x532>
 8002180:	9b02      	ldr	r3, [sp, #8]
 8002182:	edc3 7a01 	vstr	s15, [r3, #4]
 8002186:	e762      	b.n	800204e <__kernel_rem_pio2f+0x4ee>
 8002188:	9802      	ldr	r0, [sp, #8]
 800218a:	9a30      	ldr	r2, [sp, #192]	; 0xc0
 800218c:	9b31      	ldr	r3, [sp, #196]	; 0xc4
 800218e:	edc0 7a02 	vstr	s15, [r0, #8]
 8002192:	6002      	str	r2, [r0, #0]
 8002194:	6043      	str	r3, [r0, #4]
 8002196:	e75a      	b.n	800204e <__kernel_rem_pio2f+0x4ee>
 8002198:	eebd 0ac0 	vcvt.s32.f32	s0, s0
 800219c:	465b      	mov	r3, fp
 800219e:	ee10 2a10 	vmov	r2, s0
 80021a2:	f845 202b 	str.w	r2, [r5, fp, lsl #2]
 80021a6:	e6ae      	b.n	8001f06 <__kernel_rem_pio2f+0x3a6>
 80021a8:	eddf 7a10 	vldr	s15, [pc, #64]	; 80021ec <__kernel_rem_pio2f+0x68c>
 80021ac:	e73a      	b.n	8002024 <__kernel_rem_pio2f+0x4c4>
 80021ae:	9a66      	ldr	r2, [sp, #408]	; 0x198
 80021b0:	2a03      	cmp	r2, #3
 80021b2:	f63f af4c 	bhi.w	800204e <__kernel_rem_pio2f+0x4ee>
 80021b6:	a101      	add	r1, pc, #4	; (adr r1, 80021bc <__kernel_rem_pio2f+0x65c>)
 80021b8:	f851 f022 	ldr.w	pc, [r1, r2, lsl #2]
 80021bc:	080021db 	.word	0x080021db
 80021c0:	080021d5 	.word	0x080021d5
 80021c4:	080021d5 	.word	0x080021d5
 80021c8:	080021a9 	.word	0x080021a9
 80021cc:	f04f 0e01 	mov.w	lr, #1
 80021d0:	2000      	movs	r0, #0
 80021d2:	e59f      	b.n	8001d14 <__kernel_rem_pio2f+0x1b4>
 80021d4:	eddf 7a05 	vldr	s15, [pc, #20]	; 80021ec <__kernel_rem_pio2f+0x68c>
 80021d8:	e74d      	b.n	8002076 <__kernel_rem_pio2f+0x516>
 80021da:	eddf 7a04 	vldr	s15, [pc, #16]	; 80021ec <__kernel_rem_pio2f+0x68c>
 80021de:	e773      	b.n	80020c8 <__kernel_rem_pio2f+0x568>
 80021e0:	9b04      	ldr	r3, [sp, #16]
 80021e2:	009c      	lsls	r4, r3, #2
 80021e4:	e515      	b.n	8001c12 <__kernel_rem_pio2f+0xb2>
 80021e6:	bf00      	nop
 80021e8:	08009c84 	.word	0x08009c84
 80021ec:	00000000 	.word	0x00000000

080021f0 <__kernel_sinf>:
 80021f0:	ee10 3a10 	vmov	r3, s0
 80021f4:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 80021f8:	f1b3 5f48 	cmp.w	r3, #838860800	; 0x32000000
 80021fc:	da04      	bge.n	8002208 <__kernel_sinf+0x18>
 80021fe:	eefd 7ac0 	vcvt.s32.f32	s15, s0
 8002202:	ee17 3a90 	vmov	r3, s15
 8002206:	b323      	cbz	r3, 8002252 <__kernel_sinf+0x62>
 8002208:	ee60 7a00 	vmul.f32	s15, s0, s0
 800220c:	ed9f 5a15 	vldr	s10, [pc, #84]	; 8002264 <__kernel_sinf+0x74>
 8002210:	eddf 5a15 	vldr	s11, [pc, #84]	; 8002268 <__kernel_sinf+0x78>
 8002214:	ed9f 6a15 	vldr	s12, [pc, #84]	; 800226c <__kernel_sinf+0x7c>
 8002218:	eddf 6a15 	vldr	s13, [pc, #84]	; 8002270 <__kernel_sinf+0x80>
 800221c:	ed9f 7a15 	vldr	s14, [pc, #84]	; 8002274 <__kernel_sinf+0x84>
 8002220:	eee7 5a85 	vfma.f32	s11, s15, s10
 8002224:	ee20 5a27 	vmul.f32	s10, s0, s15
 8002228:	eea7 6aa5 	vfma.f32	s12, s15, s11
 800222c:	eee7 6a86 	vfma.f32	s13, s15, s12
 8002230:	eea7 7aa6 	vfma.f32	s14, s15, s13
 8002234:	b170      	cbz	r0, 8002254 <__kernel_sinf+0x64>
 8002236:	ee27 7a45 	vnmul.f32	s14, s14, s10
 800223a:	eef6 6a00 	vmov.f32	s13, #96	; 0x3f000000  0.5
 800223e:	eea0 7aa6 	vfma.f32	s14, s1, s13
 8002242:	eddf 6a0d 	vldr	s13, [pc, #52]	; 8002278 <__kernel_sinf+0x88>
 8002246:	eed7 0a87 	vfnms.f32	s1, s15, s14
 800224a:	eee5 0a26 	vfma.f32	s1, s10, s13
 800224e:	ee30 0a60 	vsub.f32	s0, s0, s1
 8002252:	4770      	bx	lr
 8002254:	eddf 6a09 	vldr	s13, [pc, #36]	; 800227c <__kernel_sinf+0x8c>
 8002258:	eee7 6a87 	vfma.f32	s13, s15, s14
 800225c:	eea5 0a26 	vfma.f32	s0, s10, s13
 8002260:	4770      	bx	lr
 8002262:	bf00      	nop
 8002264:	2f2ec9d3 	.word	0x2f2ec9d3
 8002268:	b2d72f34 	.word	0xb2d72f34
 800226c:	3638ef1b 	.word	0x3638ef1b
 8002270:	b9500d01 	.word	0xb9500d01
 8002274:	3c088889 	.word	0x3c088889
 8002278:	3e2aaaab 	.word	0x3e2aaaab
 800227c:	be2aaaab 	.word	0xbe2aaaab

08002280 <atan>:
 8002280:	e92d 4ff8 	stmdb	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8002284:	ec55 4b10 	vmov	r4, r5, d0
 8002288:	4bcb      	ldr	r3, [pc, #812]	; (80025b8 <atan+0x338>)
 800228a:	f025 4600 	bic.w	r6, r5, #2147483648	; 0x80000000
 800228e:	429e      	cmp	r6, r3
 8002290:	46aa      	mov	sl, r5
 8002292:	dd10      	ble.n	80022b6 <atan+0x36>
 8002294:	4bc9      	ldr	r3, [pc, #804]	; (80025bc <atan+0x33c>)
 8002296:	429e      	cmp	r6, r3
 8002298:	f300 80b0 	bgt.w	80023fc <atan+0x17c>
 800229c:	f000 80ab 	beq.w	80023f6 <atan+0x176>
 80022a0:	f1ba 0f00 	cmp.w	sl, #0
 80022a4:	f340 80f4 	ble.w	8002490 <atan+0x210>
 80022a8:	a1a7      	add	r1, pc, #668	; (adr r1, 8002548 <atan+0x2c8>)
 80022aa:	e9d1 0100 	ldrd	r0, r1, [r1]
 80022ae:	ec41 0b10 	vmov	d0, r0, r1
 80022b2:	e8bd 8ff8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
 80022b6:	4bc2      	ldr	r3, [pc, #776]	; (80025c0 <atan+0x340>)
 80022b8:	429e      	cmp	r6, r3
 80022ba:	f300 80be 	bgt.w	800243a <atan+0x1ba>
 80022be:	f1a3 73de 	sub.w	r3, r3, #29097984	; 0x1bc0000
 80022c2:	429e      	cmp	r6, r3
 80022c4:	f340 80a4 	ble.w	8002410 <atan+0x190>
 80022c8:	f04f 3bff 	mov.w	fp, #4294967295
 80022cc:	4622      	mov	r2, r4
 80022ce:	462b      	mov	r3, r5
 80022d0:	4620      	mov	r0, r4
 80022d2:	4629      	mov	r1, r5
 80022d4:	f7fe fe5a 	bl	8000f8c <__aeabi_dmul>
 80022d8:	4602      	mov	r2, r0
 80022da:	460b      	mov	r3, r1
 80022dc:	4680      	mov	r8, r0
 80022de:	4689      	mov	r9, r1
 80022e0:	f7fe fe54 	bl	8000f8c <__aeabi_dmul>
 80022e4:	a39a      	add	r3, pc, #616	; (adr r3, 8002550 <atan+0x2d0>)
 80022e6:	e9d3 2300 	ldrd	r2, r3, [r3]
 80022ea:	4606      	mov	r6, r0
 80022ec:	460f      	mov	r7, r1
 80022ee:	f7fe fe4d 	bl	8000f8c <__aeabi_dmul>
 80022f2:	a399      	add	r3, pc, #612	; (adr r3, 8002558 <atan+0x2d8>)
 80022f4:	e9d3 2300 	ldrd	r2, r3, [r3]
 80022f8:	f7fd ffa2 	bl	8000240 <__adddf3>
 80022fc:	4632      	mov	r2, r6
 80022fe:	463b      	mov	r3, r7
 8002300:	f7fe fe44 	bl	8000f8c <__aeabi_dmul>
 8002304:	a396      	add	r3, pc, #600	; (adr r3, 8002560 <atan+0x2e0>)
 8002306:	e9d3 2300 	ldrd	r2, r3, [r3]
 800230a:	f7fd ff99 	bl	8000240 <__adddf3>
 800230e:	4632      	mov	r2, r6
 8002310:	463b      	mov	r3, r7
 8002312:	f7fe fe3b 	bl	8000f8c <__aeabi_dmul>
 8002316:	a394      	add	r3, pc, #592	; (adr r3, 8002568 <atan+0x2e8>)
 8002318:	e9d3 2300 	ldrd	r2, r3, [r3]
 800231c:	f7fd ff90 	bl	8000240 <__adddf3>
 8002320:	4632      	mov	r2, r6
 8002322:	463b      	mov	r3, r7
 8002324:	f7fe fe32 	bl	8000f8c <__aeabi_dmul>
 8002328:	a391      	add	r3, pc, #580	; (adr r3, 8002570 <atan+0x2f0>)
 800232a:	e9d3 2300 	ldrd	r2, r3, [r3]
 800232e:	f7fd ff87 	bl	8000240 <__adddf3>
 8002332:	4632      	mov	r2, r6
 8002334:	463b      	mov	r3, r7
 8002336:	f7fe fe29 	bl	8000f8c <__aeabi_dmul>
 800233a:	a38f      	add	r3, pc, #572	; (adr r3, 8002578 <atan+0x2f8>)
 800233c:	e9d3 2300 	ldrd	r2, r3, [r3]
 8002340:	f7fd ff7e 	bl	8000240 <__adddf3>
 8002344:	4642      	mov	r2, r8
 8002346:	464b      	mov	r3, r9
 8002348:	f7fe fe20 	bl	8000f8c <__aeabi_dmul>
 800234c:	a38c      	add	r3, pc, #560	; (adr r3, 8002580 <atan+0x300>)
 800234e:	e9d3 2300 	ldrd	r2, r3, [r3]
 8002352:	4680      	mov	r8, r0
 8002354:	4689      	mov	r9, r1
 8002356:	4630      	mov	r0, r6
 8002358:	4639      	mov	r1, r7
 800235a:	f7fe fe17 	bl	8000f8c <__aeabi_dmul>
 800235e:	a38a      	add	r3, pc, #552	; (adr r3, 8002588 <atan+0x308>)
 8002360:	e9d3 2300 	ldrd	r2, r3, [r3]
 8002364:	f7fd ff6a 	bl	800023c <__aeabi_dsub>
 8002368:	4632      	mov	r2, r6
 800236a:	463b      	mov	r3, r7
 800236c:	f7fe fe0e 	bl	8000f8c <__aeabi_dmul>
 8002370:	a387      	add	r3, pc, #540	; (adr r3, 8002590 <atan+0x310>)
 8002372:	e9d3 2300 	ldrd	r2, r3, [r3]
 8002376:	f7fd ff61 	bl	800023c <__aeabi_dsub>
 800237a:	4632      	mov	r2, r6
 800237c:	463b      	mov	r3, r7
 800237e:	f7fe fe05 	bl	8000f8c <__aeabi_dmul>
 8002382:	a385      	add	r3, pc, #532	; (adr r3, 8002598 <atan+0x318>)
 8002384:	e9d3 2300 	ldrd	r2, r3, [r3]
 8002388:	f7fd ff58 	bl	800023c <__aeabi_dsub>
 800238c:	4632      	mov	r2, r6
 800238e:	463b      	mov	r3, r7
 8002390:	f7fe fdfc 	bl	8000f8c <__aeabi_dmul>
 8002394:	a382      	add	r3, pc, #520	; (adr r3, 80025a0 <atan+0x320>)
 8002396:	e9d3 2300 	ldrd	r2, r3, [r3]
 800239a:	f7fd ff4f 	bl	800023c <__aeabi_dsub>
 800239e:	4632      	mov	r2, r6
 80023a0:	463b      	mov	r3, r7
 80023a2:	f7fe fdf3 	bl	8000f8c <__aeabi_dmul>
 80023a6:	f1bb 3fff 	cmp.w	fp, #4294967295
 80023aa:	4602      	mov	r2, r0
 80023ac:	460b      	mov	r3, r1
 80023ae:	d073      	beq.n	8002498 <atan+0x218>
 80023b0:	4e84      	ldr	r6, [pc, #528]	; (80025c4 <atan+0x344>)
 80023b2:	4640      	mov	r0, r8
 80023b4:	4649      	mov	r1, r9
 80023b6:	f7fd ff43 	bl	8000240 <__adddf3>
 80023ba:	eb06 0bcb 	add.w	fp, r6, fp, lsl #3
 80023be:	4622      	mov	r2, r4
 80023c0:	462b      	mov	r3, r5
 80023c2:	f7fe fde3 	bl	8000f8c <__aeabi_dmul>
 80023c6:	e9db 2308 	ldrd	r2, r3, [fp, #32]
 80023ca:	f7fd ff37 	bl	800023c <__aeabi_dsub>
 80023ce:	4622      	mov	r2, r4
 80023d0:	462b      	mov	r3, r5
 80023d2:	f7fd ff33 	bl	800023c <__aeabi_dsub>
 80023d6:	4602      	mov	r2, r0
 80023d8:	460b      	mov	r3, r1
 80023da:	e9db 0100 	ldrd	r0, r1, [fp]
 80023de:	f7fd ff2d 	bl	800023c <__aeabi_dsub>
 80023e2:	f1ba 0f00 	cmp.w	sl, #0
 80023e6:	da0f      	bge.n	8002408 <atan+0x188>
 80023e8:	f101 4300 	add.w	r3, r1, #2147483648	; 0x80000000
 80023ec:	4619      	mov	r1, r3
 80023ee:	ec41 0b10 	vmov	d0, r0, r1
 80023f2:	e8bd 8ff8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
 80023f6:	2c00      	cmp	r4, #0
 80023f8:	f43f af52 	beq.w	80022a0 <atan+0x20>
 80023fc:	4622      	mov	r2, r4
 80023fe:	462b      	mov	r3, r5
 8002400:	4620      	mov	r0, r4
 8002402:	4629      	mov	r1, r5
 8002404:	f7fd ff1c 	bl	8000240 <__adddf3>
 8002408:	ec41 0b10 	vmov	d0, r0, r1
 800240c:	e8bd 8ff8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8002410:	a365      	add	r3, pc, #404	; (adr r3, 80025a8 <atan+0x328>)
 8002412:	e9d3 2300 	ldrd	r2, r3, [r3]
 8002416:	ee10 0a10 	vmov	r0, s0
 800241a:	4629      	mov	r1, r5
 800241c:	f7fd ff10 	bl	8000240 <__adddf3>
 8002420:	2200      	movs	r2, #0
 8002422:	4b69      	ldr	r3, [pc, #420]	; (80025c8 <atan+0x348>)
 8002424:	f7ff f842 	bl	80014ac <__aeabi_dcmpgt>
 8002428:	2800      	cmp	r0, #0
 800242a:	f43f af4d 	beq.w	80022c8 <atan+0x48>
 800242e:	4620      	mov	r0, r4
 8002430:	4629      	mov	r1, r5
 8002432:	ec41 0b10 	vmov	d0, r0, r1
 8002436:	e8bd 8ff8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800243a:	f000 f8cf 	bl	80025dc <fabs>
 800243e:	4b63      	ldr	r3, [pc, #396]	; (80025cc <atan+0x34c>)
 8002440:	429e      	cmp	r6, r3
 8002442:	ec55 4b10 	vmov	r4, r5, d0
 8002446:	dc36      	bgt.n	80024b6 <atan+0x236>
 8002448:	f5a3 2350 	sub.w	r3, r3, #851968	; 0xd0000
 800244c:	429e      	cmp	r6, r3
 800244e:	dc5f      	bgt.n	8002510 <atan+0x290>
 8002450:	ee10 2a10 	vmov	r2, s0
 8002454:	ee10 0a10 	vmov	r0, s0
 8002458:	462b      	mov	r3, r5
 800245a:	4629      	mov	r1, r5
 800245c:	f7fd fef0 	bl	8000240 <__adddf3>
 8002460:	2200      	movs	r2, #0
 8002462:	4b59      	ldr	r3, [pc, #356]	; (80025c8 <atan+0x348>)
 8002464:	f7fd feea 	bl	800023c <__aeabi_dsub>
 8002468:	2200      	movs	r2, #0
 800246a:	4606      	mov	r6, r0
 800246c:	460f      	mov	r7, r1
 800246e:	f04f 4380 	mov.w	r3, #1073741824	; 0x40000000
 8002472:	4620      	mov	r0, r4
 8002474:	4629      	mov	r1, r5
 8002476:	f7fd fee3 	bl	8000240 <__adddf3>
 800247a:	4602      	mov	r2, r0
 800247c:	460b      	mov	r3, r1
 800247e:	4630      	mov	r0, r6
 8002480:	4639      	mov	r1, r7
 8002482:	f7fe fead 	bl	80011e0 <__aeabi_ddiv>
 8002486:	f04f 0b00 	mov.w	fp, #0
 800248a:	4604      	mov	r4, r0
 800248c:	460d      	mov	r5, r1
 800248e:	e71d      	b.n	80022cc <atan+0x4c>
 8002490:	a147      	add	r1, pc, #284	; (adr r1, 80025b0 <atan+0x330>)
 8002492:	e9d1 0100 	ldrd	r0, r1, [r1]
 8002496:	e7b7      	b.n	8002408 <atan+0x188>
 8002498:	4640      	mov	r0, r8
 800249a:	4649      	mov	r1, r9
 800249c:	f7fd fed0 	bl	8000240 <__adddf3>
 80024a0:	4622      	mov	r2, r4
 80024a2:	462b      	mov	r3, r5
 80024a4:	f7fe fd72 	bl	8000f8c <__aeabi_dmul>
 80024a8:	4602      	mov	r2, r0
 80024aa:	460b      	mov	r3, r1
 80024ac:	4620      	mov	r0, r4
 80024ae:	4629      	mov	r1, r5
 80024b0:	f7fd fec4 	bl	800023c <__aeabi_dsub>
 80024b4:	e7a8      	b.n	8002408 <atan+0x188>
 80024b6:	4b46      	ldr	r3, [pc, #280]	; (80025d0 <atan+0x350>)
 80024b8:	429e      	cmp	r6, r3
 80024ba:	dc1d      	bgt.n	80024f8 <atan+0x278>
 80024bc:	ee10 0a10 	vmov	r0, s0
 80024c0:	2200      	movs	r2, #0
 80024c2:	4b44      	ldr	r3, [pc, #272]	; (80025d4 <atan+0x354>)
 80024c4:	4629      	mov	r1, r5
 80024c6:	f7fd feb9 	bl	800023c <__aeabi_dsub>
 80024ca:	2200      	movs	r2, #0
 80024cc:	4606      	mov	r6, r0
 80024ce:	460f      	mov	r7, r1
 80024d0:	4b40      	ldr	r3, [pc, #256]	; (80025d4 <atan+0x354>)
 80024d2:	4620      	mov	r0, r4
 80024d4:	4629      	mov	r1, r5
 80024d6:	f7fe fd59 	bl	8000f8c <__aeabi_dmul>
 80024da:	2200      	movs	r2, #0
 80024dc:	4b3a      	ldr	r3, [pc, #232]	; (80025c8 <atan+0x348>)
 80024de:	f7fd feaf 	bl	8000240 <__adddf3>
 80024e2:	4602      	mov	r2, r0
 80024e4:	460b      	mov	r3, r1
 80024e6:	4630      	mov	r0, r6
 80024e8:	4639      	mov	r1, r7
 80024ea:	f7fe fe79 	bl	80011e0 <__aeabi_ddiv>
 80024ee:	f04f 0b02 	mov.w	fp, #2
 80024f2:	4604      	mov	r4, r0
 80024f4:	460d      	mov	r5, r1
 80024f6:	e6e9      	b.n	80022cc <atan+0x4c>
 80024f8:	462b      	mov	r3, r5
 80024fa:	ee10 2a10 	vmov	r2, s0
 80024fe:	2000      	movs	r0, #0
 8002500:	4935      	ldr	r1, [pc, #212]	; (80025d8 <atan+0x358>)
 8002502:	f7fe fe6d 	bl	80011e0 <__aeabi_ddiv>
 8002506:	f04f 0b03 	mov.w	fp, #3
 800250a:	4604      	mov	r4, r0
 800250c:	460d      	mov	r5, r1
 800250e:	e6dd      	b.n	80022cc <atan+0x4c>
 8002510:	ee10 0a10 	vmov	r0, s0
 8002514:	2200      	movs	r2, #0
 8002516:	4b2c      	ldr	r3, [pc, #176]	; (80025c8 <atan+0x348>)
 8002518:	4629      	mov	r1, r5
 800251a:	f7fd fe8f 	bl	800023c <__aeabi_dsub>
 800251e:	2200      	movs	r2, #0
 8002520:	4606      	mov	r6, r0
 8002522:	460f      	mov	r7, r1
 8002524:	4b28      	ldr	r3, [pc, #160]	; (80025c8 <atan+0x348>)
 8002526:	4620      	mov	r0, r4
 8002528:	4629      	mov	r1, r5
 800252a:	f7fd fe89 	bl	8000240 <__adddf3>
 800252e:	4602      	mov	r2, r0
 8002530:	460b      	mov	r3, r1
 8002532:	4630      	mov	r0, r6
 8002534:	4639      	mov	r1, r7
 8002536:	f7fe fe53 	bl	80011e0 <__aeabi_ddiv>
 800253a:	f04f 0b01 	mov.w	fp, #1
 800253e:	4604      	mov	r4, r0
 8002540:	460d      	mov	r5, r1
 8002542:	e6c3      	b.n	80022cc <atan+0x4c>
 8002544:	f3af 8000 	nop.w
 8002548:	54442d18 	.word	0x54442d18
 800254c:	3ff921fb 	.word	0x3ff921fb
 8002550:	e322da11 	.word	0xe322da11
 8002554:	3f90ad3a 	.word	0x3f90ad3a
 8002558:	24760deb 	.word	0x24760deb
 800255c:	3fa97b4b 	.word	0x3fa97b4b
 8002560:	a0d03d51 	.word	0xa0d03d51
 8002564:	3fb10d66 	.word	0x3fb10d66
 8002568:	c54c206e 	.word	0xc54c206e
 800256c:	3fb745cd 	.word	0x3fb745cd
 8002570:	920083ff 	.word	0x920083ff
 8002574:	3fc24924 	.word	0x3fc24924
 8002578:	5555550d 	.word	0x5555550d
 800257c:	3fd55555 	.word	0x3fd55555
 8002580:	2c6a6c2f 	.word	0x2c6a6c2f
 8002584:	bfa2b444 	.word	0xbfa2b444
 8002588:	52defd9a 	.word	0x52defd9a
 800258c:	3fadde2d 	.word	0x3fadde2d
 8002590:	af749a6d 	.word	0xaf749a6d
 8002594:	3fb3b0f2 	.word	0x3fb3b0f2
 8002598:	fe231671 	.word	0xfe231671
 800259c:	3fbc71c6 	.word	0x3fbc71c6
 80025a0:	9998ebc4 	.word	0x9998ebc4
 80025a4:	3fc99999 	.word	0x3fc99999
 80025a8:	8800759c 	.word	0x8800759c
 80025ac:	7e37e43c 	.word	0x7e37e43c
 80025b0:	54442d18 	.word	0x54442d18
 80025b4:	bff921fb 	.word	0xbff921fb
 80025b8:	440fffff 	.word	0x440fffff
 80025bc:	7ff00000 	.word	0x7ff00000
 80025c0:	3fdbffff 	.word	0x3fdbffff
 80025c4:	08009cb0 	.word	0x08009cb0
 80025c8:	3ff00000 	.word	0x3ff00000
 80025cc:	3ff2ffff 	.word	0x3ff2ffff
 80025d0:	40037fff 	.word	0x40037fff
 80025d4:	3ff80000 	.word	0x3ff80000
 80025d8:	bff00000 	.word	0xbff00000

080025dc <fabs>:
 80025dc:	ec53 2b10 	vmov	r2, r3, d0
 80025e0:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 80025e4:	ec43 2b10 	vmov	d0, r2, r3
 80025e8:	4770      	bx	lr
 80025ea:	bf00      	nop

080025ec <fabsf>:
 80025ec:	ee10 3a10 	vmov	r3, s0
 80025f0:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 80025f4:	ee00 3a10 	vmov	s0, r3
 80025f8:	4770      	bx	lr
 80025fa:	bf00      	nop

080025fc <floorf>:
 80025fc:	ee10 2a10 	vmov	r2, s0
 8002600:	f022 4100 	bic.w	r1, r2, #2147483648	; 0x80000000
 8002604:	0dcb      	lsrs	r3, r1, #23
 8002606:	3b7f      	subs	r3, #127	; 0x7f
 8002608:	2b16      	cmp	r3, #22
 800260a:	dc17      	bgt.n	800263c <floorf+0x40>
 800260c:	2b00      	cmp	r3, #0
 800260e:	ee10 0a10 	vmov	r0, s0
 8002612:	db19      	blt.n	8002648 <floorf+0x4c>
 8002614:	491a      	ldr	r1, [pc, #104]	; (8002680 <floorf+0x84>)
 8002616:	4119      	asrs	r1, r3
 8002618:	420a      	tst	r2, r1
 800261a:	d022      	beq.n	8002662 <floorf+0x66>
 800261c:	eddf 7a19 	vldr	s15, [pc, #100]	; 8002684 <floorf+0x88>
 8002620:	ee70 7a27 	vadd.f32	s15, s0, s15
 8002624:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 8002628:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800262c:	dd19      	ble.n	8002662 <floorf+0x66>
 800262e:	2a00      	cmp	r2, #0
 8002630:	db18      	blt.n	8002664 <floorf+0x68>
 8002632:	ea20 0301 	bic.w	r3, r0, r1
 8002636:	ee00 3a10 	vmov	s0, r3
 800263a:	4770      	bx	lr
 800263c:	f1b1 4fff 	cmp.w	r1, #2139095040	; 0x7f800000
 8002640:	d30f      	bcc.n	8002662 <floorf+0x66>
 8002642:	ee30 0a00 	vadd.f32	s0, s0, s0
 8002646:	4770      	bx	lr
 8002648:	eddf 7a0e 	vldr	s15, [pc, #56]	; 8002684 <floorf+0x88>
 800264c:	ee70 7a27 	vadd.f32	s15, s0, s15
 8002650:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 8002654:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8002658:	dd03      	ble.n	8002662 <floorf+0x66>
 800265a:	2a00      	cmp	r2, #0
 800265c:	db08      	blt.n	8002670 <floorf+0x74>
 800265e:	ed9f 0a0a 	vldr	s0, [pc, #40]	; 8002688 <floorf+0x8c>
 8002662:	4770      	bx	lr
 8002664:	f44f 0200 	mov.w	r2, #8388608	; 0x800000
 8002668:	fa42 f303 	asr.w	r3, r2, r3
 800266c:	4418      	add	r0, r3
 800266e:	e7e0      	b.n	8002632 <floorf+0x36>
 8002670:	2900      	cmp	r1, #0
 8002672:	eeff 7a00 	vmov.f32	s15, #240	; 0xbf800000 -1.0
 8002676:	bf18      	it	ne
 8002678:	eeb0 0a67 	vmovne.f32	s0, s15
 800267c:	4770      	bx	lr
 800267e:	bf00      	nop
 8002680:	007fffff 	.word	0x007fffff
 8002684:	7149f2ca 	.word	0x7149f2ca
 8002688:	00000000 	.word	0x00000000

0800268c <scalbnf>:
 800268c:	b508      	push	{r3, lr}
 800268e:	ee10 3a10 	vmov	r3, s0
 8002692:	f033 4200 	bics.w	r2, r3, #2147483648	; 0x80000000
 8002696:	ed2d 8b02 	vpush	{d8}
 800269a:	d011      	beq.n	80026c0 <scalbnf+0x34>
 800269c:	f1b2 4fff 	cmp.w	r2, #2139095040	; 0x7f800000
 80026a0:	d211      	bcs.n	80026c6 <scalbnf+0x3a>
 80026a2:	f5b2 0f00 	cmp.w	r2, #8388608	; 0x800000
 80026a6:	d313      	bcc.n	80026d0 <scalbnf+0x44>
 80026a8:	0dd2      	lsrs	r2, r2, #23
 80026aa:	4402      	add	r2, r0
 80026ac:	2afe      	cmp	r2, #254	; 0xfe
 80026ae:	dc2e      	bgt.n	800270e <scalbnf+0x82>
 80026b0:	2a00      	cmp	r2, #0
 80026b2:	dd1a      	ble.n	80026ea <scalbnf+0x5e>
 80026b4:	f023 43ff 	bic.w	r3, r3, #2139095040	; 0x7f800000
 80026b8:	ea43 53c2 	orr.w	r3, r3, r2, lsl #23
 80026bc:	ee00 3a10 	vmov	s0, r3
 80026c0:	ecbd 8b02 	vpop	{d8}
 80026c4:	bd08      	pop	{r3, pc}
 80026c6:	ecbd 8b02 	vpop	{d8}
 80026ca:	ee30 0a00 	vadd.f32	s0, s0, s0
 80026ce:	bd08      	pop	{r3, pc}
 80026d0:	4b1d      	ldr	r3, [pc, #116]	; (8002748 <scalbnf+0xbc>)
 80026d2:	eddf 7a1e 	vldr	s15, [pc, #120]	; 800274c <scalbnf+0xc0>
 80026d6:	4298      	cmp	r0, r3
 80026d8:	ee20 0a27 	vmul.f32	s0, s0, s15
 80026dc:	db22      	blt.n	8002724 <scalbnf+0x98>
 80026de:	ee10 3a10 	vmov	r3, s0
 80026e2:	f3c3 52c7 	ubfx	r2, r3, #23, #8
 80026e6:	3a19      	subs	r2, #25
 80026e8:	e7df      	b.n	80026aa <scalbnf+0x1e>
 80026ea:	f112 0f16 	cmn.w	r2, #22
 80026ee:	da1e      	bge.n	800272e <scalbnf+0xa2>
 80026f0:	f24c 3350 	movw	r3, #50000	; 0xc350
 80026f4:	4298      	cmp	r0, r3
 80026f6:	dc0a      	bgt.n	800270e <scalbnf+0x82>
 80026f8:	ed9f 8a15 	vldr	s16, [pc, #84]	; 8002750 <scalbnf+0xc4>
 80026fc:	eef0 0a40 	vmov.f32	s1, s0
 8002700:	eeb0 0a48 	vmov.f32	s0, s16
 8002704:	f000 f82a 	bl	800275c <copysignf>
 8002708:	ee20 0a08 	vmul.f32	s0, s0, s16
 800270c:	e7d8      	b.n	80026c0 <scalbnf+0x34>
 800270e:	ed9f 8a11 	vldr	s16, [pc, #68]	; 8002754 <scalbnf+0xc8>
 8002712:	eef0 0a40 	vmov.f32	s1, s0
 8002716:	eeb0 0a48 	vmov.f32	s0, s16
 800271a:	f000 f81f 	bl	800275c <copysignf>
 800271e:	ee20 0a08 	vmul.f32	s0, s0, s16
 8002722:	e7cd      	b.n	80026c0 <scalbnf+0x34>
 8002724:	eddf 0a0a 	vldr	s1, [pc, #40]	; 8002750 <scalbnf+0xc4>
 8002728:	ee20 0a20 	vmul.f32	s0, s0, s1
 800272c:	e7c8      	b.n	80026c0 <scalbnf+0x34>
 800272e:	3219      	adds	r2, #25
 8002730:	f023 43ff 	bic.w	r3, r3, #2139095040	; 0x7f800000
 8002734:	ea43 53c2 	orr.w	r3, r3, r2, lsl #23
 8002738:	eddf 7a07 	vldr	s15, [pc, #28]	; 8002758 <scalbnf+0xcc>
 800273c:	ee00 3a10 	vmov	s0, r3
 8002740:	ee20 0a27 	vmul.f32	s0, s0, s15
 8002744:	e7bc      	b.n	80026c0 <scalbnf+0x34>
 8002746:	bf00      	nop
 8002748:	ffff3cb0 	.word	0xffff3cb0
 800274c:	4c000000 	.word	0x4c000000
 8002750:	0da24260 	.word	0x0da24260
 8002754:	7149f2ca 	.word	0x7149f2ca
 8002758:	33000000 	.word	0x33000000

0800275c <copysignf>:
 800275c:	ee10 3a10 	vmov	r3, s0
 8002760:	ee10 2a90 	vmov	r2, s1
 8002764:	f023 4300 	bic.w	r3, r3, #2147483648	; 0x80000000
 8002768:	f002 4200 	and.w	r2, r2, #2147483648	; 0x80000000
 800276c:	4313      	orrs	r3, r2
 800276e:	ee00 3a10 	vmov	s0, r3
 8002772:	4770      	bx	lr

08002774 <Reset_Handler>:
 8002774:	f8df d034 	ldr.w	sp, [pc, #52]	; 80027ac <LoopFillZerobss+0x14>
 8002778:	2100      	movs	r1, #0
 800277a:	e003      	b.n	8002784 <LoopCopyDataInit>

0800277c <CopyDataInit>:
 800277c:	4b0c      	ldr	r3, [pc, #48]	; (80027b0 <LoopFillZerobss+0x18>)
 800277e:	585b      	ldr	r3, [r3, r1]
 8002780:	5043      	str	r3, [r0, r1]
 8002782:	3104      	adds	r1, #4

08002784 <LoopCopyDataInit>:
 8002784:	480b      	ldr	r0, [pc, #44]	; (80027b4 <LoopFillZerobss+0x1c>)
 8002786:	4b0c      	ldr	r3, [pc, #48]	; (80027b8 <LoopFillZerobss+0x20>)
 8002788:	1842      	adds	r2, r0, r1
 800278a:	429a      	cmp	r2, r3
 800278c:	d3f6      	bcc.n	800277c <CopyDataInit>
 800278e:	4a0b      	ldr	r2, [pc, #44]	; (80027bc <LoopFillZerobss+0x24>)
 8002790:	e002      	b.n	8002798 <LoopFillZerobss>

08002792 <FillZerobss>:
 8002792:	2300      	movs	r3, #0
 8002794:	f842 3b04 	str.w	r3, [r2], #4

08002798 <LoopFillZerobss>:
 8002798:	4b09      	ldr	r3, [pc, #36]	; (80027c0 <LoopFillZerobss+0x28>)
 800279a:	429a      	cmp	r2, r3
 800279c:	d3f9      	bcc.n	8002792 <FillZerobss>
 800279e:	f006 fe7b 	bl	8009498 <SystemInit>
 80027a2:	f7fd ff6f 	bl	8000684 <__libc_init_array>
 80027a6:	f000 f9f5 	bl	8002b94 <main>
 80027aa:	4770      	bx	lr
 80027ac:	20020000 	.word	0x20020000
 80027b0:	08009d84 	.word	0x08009d84
 80027b4:	20000000 	.word	0x20000000
 80027b8:	200008b4 	.word	0x200008b4
 80027bc:	200008b8 	.word	0x200008b8
 80027c0:	20006154 	.word	0x20006154

080027c4 <ADC_IRQHandler>:
 80027c4:	e7fe      	b.n	80027c4 <ADC_IRQHandler>
	...

080027c8 <NVIC_SetPriorityGrouping>:
 80027c8:	b480      	push	{r7}
 80027ca:	b085      	sub	sp, #20
 80027cc:	af00      	add	r7, sp, #0
 80027ce:	6078      	str	r0, [r7, #4]
 80027d0:	687b      	ldr	r3, [r7, #4]
 80027d2:	f003 0307 	and.w	r3, r3, #7
 80027d6:	60fb      	str	r3, [r7, #12]
 80027d8:	4b0c      	ldr	r3, [pc, #48]	; (800280c <NVIC_SetPriorityGrouping+0x44>)
 80027da:	68db      	ldr	r3, [r3, #12]
 80027dc:	60bb      	str	r3, [r7, #8]
 80027de:	68ba      	ldr	r2, [r7, #8]
 80027e0:	f64f 03ff 	movw	r3, #63743	; 0xf8ff
 80027e4:	4013      	ands	r3, r2
 80027e6:	60bb      	str	r3, [r7, #8]
 80027e8:	68fb      	ldr	r3, [r7, #12]
 80027ea:	021a      	lsls	r2, r3, #8
 80027ec:	68bb      	ldr	r3, [r7, #8]
 80027ee:	4313      	orrs	r3, r2
 80027f0:	f043 63bf 	orr.w	r3, r3, #100139008	; 0x5f80000
 80027f4:	f443 3300 	orr.w	r3, r3, #131072	; 0x20000
 80027f8:	60bb      	str	r3, [r7, #8]
 80027fa:	4a04      	ldr	r2, [pc, #16]	; (800280c <NVIC_SetPriorityGrouping+0x44>)
 80027fc:	68bb      	ldr	r3, [r7, #8]
 80027fe:	60d3      	str	r3, [r2, #12]
 8002800:	bf00      	nop
 8002802:	3714      	adds	r7, #20
 8002804:	46bd      	mov	sp, r7
 8002806:	f85d 7b04 	ldr.w	r7, [sp], #4
 800280a:	4770      	bx	lr
 800280c:	e000ed00 	.word	0xe000ed00

08002810 <NVIC_SetPriority>:
 8002810:	b480      	push	{r7}
 8002812:	b083      	sub	sp, #12
 8002814:	af00      	add	r7, sp, #0
 8002816:	4603      	mov	r3, r0
 8002818:	6039      	str	r1, [r7, #0]
 800281a:	71fb      	strb	r3, [r7, #7]
 800281c:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8002820:	2b00      	cmp	r3, #0
 8002822:	da0b      	bge.n	800283c <NVIC_SetPriority+0x2c>
 8002824:	490d      	ldr	r1, [pc, #52]	; (800285c <NVIC_SetPriority+0x4c>)
 8002826:	79fb      	ldrb	r3, [r7, #7]
 8002828:	f003 030f 	and.w	r3, r3, #15
 800282c:	3b04      	subs	r3, #4
 800282e:	683a      	ldr	r2, [r7, #0]
 8002830:	b2d2      	uxtb	r2, r2
 8002832:	0112      	lsls	r2, r2, #4
 8002834:	b2d2      	uxtb	r2, r2
 8002836:	440b      	add	r3, r1
 8002838:	761a      	strb	r2, [r3, #24]
 800283a:	e009      	b.n	8002850 <NVIC_SetPriority+0x40>
 800283c:	4908      	ldr	r1, [pc, #32]	; (8002860 <NVIC_SetPriority+0x50>)
 800283e:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8002842:	683a      	ldr	r2, [r7, #0]
 8002844:	b2d2      	uxtb	r2, r2
 8002846:	0112      	lsls	r2, r2, #4
 8002848:	b2d2      	uxtb	r2, r2
 800284a:	440b      	add	r3, r1
 800284c:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8002850:	bf00      	nop
 8002852:	370c      	adds	r7, #12
 8002854:	46bd      	mov	sp, r7
 8002856:	f85d 7b04 	ldr.w	r7, [sp], #4
 800285a:	4770      	bx	lr
 800285c:	e000ed00 	.word	0xe000ed00
 8002860:	e000e100 	.word	0xe000e100

08002864 <SysTick_Config>:
 8002864:	b580      	push	{r7, lr}
 8002866:	b082      	sub	sp, #8
 8002868:	af00      	add	r7, sp, #0
 800286a:	6078      	str	r0, [r7, #4]
 800286c:	687b      	ldr	r3, [r7, #4]
 800286e:	3b01      	subs	r3, #1
 8002870:	f1b3 7f80 	cmp.w	r3, #16777216	; 0x1000000
 8002874:	d301      	bcc.n	800287a <SysTick_Config+0x16>
 8002876:	2301      	movs	r3, #1
 8002878:	e00f      	b.n	800289a <SysTick_Config+0x36>
 800287a:	4a0a      	ldr	r2, [pc, #40]	; (80028a4 <SysTick_Config+0x40>)
 800287c:	687b      	ldr	r3, [r7, #4]
 800287e:	3b01      	subs	r3, #1
 8002880:	6053      	str	r3, [r2, #4]
 8002882:	210f      	movs	r1, #15
 8002884:	f04f 30ff 	mov.w	r0, #4294967295
 8002888:	f7ff ffc2 	bl	8002810 <NVIC_SetPriority>
 800288c:	4b05      	ldr	r3, [pc, #20]	; (80028a4 <SysTick_Config+0x40>)
 800288e:	2200      	movs	r2, #0
 8002890:	609a      	str	r2, [r3, #8]
 8002892:	4b04      	ldr	r3, [pc, #16]	; (80028a4 <SysTick_Config+0x40>)
 8002894:	2207      	movs	r2, #7
 8002896:	601a      	str	r2, [r3, #0]
 8002898:	2300      	movs	r3, #0
 800289a:	4618      	mov	r0, r3
 800289c:	3708      	adds	r7, #8
 800289e:	46bd      	mov	sp, r7
 80028a0:	bd80      	pop	{r7, pc}
 80028a2:	bf00      	nop
 80028a4:	e000e010 	.word	0xe000e010

080028a8 <LL_RCC_HSE_Enable>:
 80028a8:	b480      	push	{r7}
 80028aa:	af00      	add	r7, sp, #0
 80028ac:	4a05      	ldr	r2, [pc, #20]	; (80028c4 <LL_RCC_HSE_Enable+0x1c>)
 80028ae:	4b05      	ldr	r3, [pc, #20]	; (80028c4 <LL_RCC_HSE_Enable+0x1c>)
 80028b0:	681b      	ldr	r3, [r3, #0]
 80028b2:	f443 3380 	orr.w	r3, r3, #65536	; 0x10000
 80028b6:	6013      	str	r3, [r2, #0]
 80028b8:	bf00      	nop
 80028ba:	46bd      	mov	sp, r7
 80028bc:	f85d 7b04 	ldr.w	r7, [sp], #4
 80028c0:	4770      	bx	lr
 80028c2:	bf00      	nop
 80028c4:	40023800 	.word	0x40023800

080028c8 <LL_RCC_HSE_IsReady>:
 80028c8:	b480      	push	{r7}
 80028ca:	af00      	add	r7, sp, #0
 80028cc:	4b07      	ldr	r3, [pc, #28]	; (80028ec <LL_RCC_HSE_IsReady+0x24>)
 80028ce:	681b      	ldr	r3, [r3, #0]
 80028d0:	f403 3300 	and.w	r3, r3, #131072	; 0x20000
 80028d4:	f5b3 3f00 	cmp.w	r3, #131072	; 0x20000
 80028d8:	bf0c      	ite	eq
 80028da:	2301      	moveq	r3, #1
 80028dc:	2300      	movne	r3, #0
 80028de:	b2db      	uxtb	r3, r3
 80028e0:	4618      	mov	r0, r3
 80028e2:	46bd      	mov	sp, r7
 80028e4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80028e8:	4770      	bx	lr
 80028ea:	bf00      	nop
 80028ec:	40023800 	.word	0x40023800

080028f0 <LL_RCC_SetSysClkSource>:
 80028f0:	b480      	push	{r7}
 80028f2:	b083      	sub	sp, #12
 80028f4:	af00      	add	r7, sp, #0
 80028f6:	6078      	str	r0, [r7, #4]
 80028f8:	4906      	ldr	r1, [pc, #24]	; (8002914 <LL_RCC_SetSysClkSource+0x24>)
 80028fa:	4b06      	ldr	r3, [pc, #24]	; (8002914 <LL_RCC_SetSysClkSource+0x24>)
 80028fc:	689b      	ldr	r3, [r3, #8]
 80028fe:	f023 0203 	bic.w	r2, r3, #3
 8002902:	687b      	ldr	r3, [r7, #4]
 8002904:	4313      	orrs	r3, r2
 8002906:	608b      	str	r3, [r1, #8]
 8002908:	bf00      	nop
 800290a:	370c      	adds	r7, #12
 800290c:	46bd      	mov	sp, r7
 800290e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002912:	4770      	bx	lr
 8002914:	40023800 	.word	0x40023800

08002918 <LL_RCC_GetSysClkSource>:
 8002918:	b480      	push	{r7}
 800291a:	af00      	add	r7, sp, #0
 800291c:	4b04      	ldr	r3, [pc, #16]	; (8002930 <LL_RCC_GetSysClkSource+0x18>)
 800291e:	689b      	ldr	r3, [r3, #8]
 8002920:	f003 030c 	and.w	r3, r3, #12
 8002924:	4618      	mov	r0, r3
 8002926:	46bd      	mov	sp, r7
 8002928:	f85d 7b04 	ldr.w	r7, [sp], #4
 800292c:	4770      	bx	lr
 800292e:	bf00      	nop
 8002930:	40023800 	.word	0x40023800

08002934 <LL_RCC_SetAHBPrescaler>:
 8002934:	b480      	push	{r7}
 8002936:	b083      	sub	sp, #12
 8002938:	af00      	add	r7, sp, #0
 800293a:	6078      	str	r0, [r7, #4]
 800293c:	4906      	ldr	r1, [pc, #24]	; (8002958 <LL_RCC_SetAHBPrescaler+0x24>)
 800293e:	4b06      	ldr	r3, [pc, #24]	; (8002958 <LL_RCC_SetAHBPrescaler+0x24>)
 8002940:	689b      	ldr	r3, [r3, #8]
 8002942:	f023 02f0 	bic.w	r2, r3, #240	; 0xf0
 8002946:	687b      	ldr	r3, [r7, #4]
 8002948:	4313      	orrs	r3, r2
 800294a:	608b      	str	r3, [r1, #8]
 800294c:	bf00      	nop
 800294e:	370c      	adds	r7, #12
 8002950:	46bd      	mov	sp, r7
 8002952:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002956:	4770      	bx	lr
 8002958:	40023800 	.word	0x40023800

0800295c <LL_RCC_SetAPB1Prescaler>:
 800295c:	b480      	push	{r7}
 800295e:	b083      	sub	sp, #12
 8002960:	af00      	add	r7, sp, #0
 8002962:	6078      	str	r0, [r7, #4]
 8002964:	4906      	ldr	r1, [pc, #24]	; (8002980 <LL_RCC_SetAPB1Prescaler+0x24>)
 8002966:	4b06      	ldr	r3, [pc, #24]	; (8002980 <LL_RCC_SetAPB1Prescaler+0x24>)
 8002968:	689b      	ldr	r3, [r3, #8]
 800296a:	f423 52e0 	bic.w	r2, r3, #7168	; 0x1c00
 800296e:	687b      	ldr	r3, [r7, #4]
 8002970:	4313      	orrs	r3, r2
 8002972:	608b      	str	r3, [r1, #8]
 8002974:	bf00      	nop
 8002976:	370c      	adds	r7, #12
 8002978:	46bd      	mov	sp, r7
 800297a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800297e:	4770      	bx	lr
 8002980:	40023800 	.word	0x40023800

08002984 <LL_RCC_SetAPB2Prescaler>:
 8002984:	b480      	push	{r7}
 8002986:	b083      	sub	sp, #12
 8002988:	af00      	add	r7, sp, #0
 800298a:	6078      	str	r0, [r7, #4]
 800298c:	4906      	ldr	r1, [pc, #24]	; (80029a8 <LL_RCC_SetAPB2Prescaler+0x24>)
 800298e:	4b06      	ldr	r3, [pc, #24]	; (80029a8 <LL_RCC_SetAPB2Prescaler+0x24>)
 8002990:	689b      	ldr	r3, [r3, #8]
 8002992:	f423 4260 	bic.w	r2, r3, #57344	; 0xe000
 8002996:	687b      	ldr	r3, [r7, #4]
 8002998:	4313      	orrs	r3, r2
 800299a:	608b      	str	r3, [r1, #8]
 800299c:	bf00      	nop
 800299e:	370c      	adds	r7, #12
 80029a0:	46bd      	mov	sp, r7
 80029a2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80029a6:	4770      	bx	lr
 80029a8:	40023800 	.word	0x40023800

080029ac <LL_RCC_PLL_Enable>:
 80029ac:	b480      	push	{r7}
 80029ae:	af00      	add	r7, sp, #0
 80029b0:	4a05      	ldr	r2, [pc, #20]	; (80029c8 <LL_RCC_PLL_Enable+0x1c>)
 80029b2:	4b05      	ldr	r3, [pc, #20]	; (80029c8 <LL_RCC_PLL_Enable+0x1c>)
 80029b4:	681b      	ldr	r3, [r3, #0]
 80029b6:	f043 7380 	orr.w	r3, r3, #16777216	; 0x1000000
 80029ba:	6013      	str	r3, [r2, #0]
 80029bc:	bf00      	nop
 80029be:	46bd      	mov	sp, r7
 80029c0:	f85d 7b04 	ldr.w	r7, [sp], #4
 80029c4:	4770      	bx	lr
 80029c6:	bf00      	nop
 80029c8:	40023800 	.word	0x40023800

080029cc <LL_RCC_PLL_IsReady>:
 80029cc:	b480      	push	{r7}
 80029ce:	af00      	add	r7, sp, #0
 80029d0:	4b07      	ldr	r3, [pc, #28]	; (80029f0 <LL_RCC_PLL_IsReady+0x24>)
 80029d2:	681b      	ldr	r3, [r3, #0]
 80029d4:	f003 7300 	and.w	r3, r3, #33554432	; 0x2000000
 80029d8:	f1b3 7f00 	cmp.w	r3, #33554432	; 0x2000000
 80029dc:	bf0c      	ite	eq
 80029de:	2301      	moveq	r3, #1
 80029e0:	2300      	movne	r3, #0
 80029e2:	b2db      	uxtb	r3, r3
 80029e4:	4618      	mov	r0, r3
 80029e6:	46bd      	mov	sp, r7
 80029e8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80029ec:	4770      	bx	lr
 80029ee:	bf00      	nop
 80029f0:	40023800 	.word	0x40023800

080029f4 <LL_RCC_PLL_ConfigDomain_SYS>:
 80029f4:	b480      	push	{r7}
 80029f6:	b085      	sub	sp, #20
 80029f8:	af00      	add	r7, sp, #0
 80029fa:	60f8      	str	r0, [r7, #12]
 80029fc:	60b9      	str	r1, [r7, #8]
 80029fe:	607a      	str	r2, [r7, #4]
 8002a00:	603b      	str	r3, [r7, #0]
 8002a02:	480d      	ldr	r0, [pc, #52]	; (8002a38 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8002a04:	4b0c      	ldr	r3, [pc, #48]	; (8002a38 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8002a06:	685a      	ldr	r2, [r3, #4]
 8002a08:	4b0c      	ldr	r3, [pc, #48]	; (8002a3c <LL_RCC_PLL_ConfigDomain_SYS+0x48>)
 8002a0a:	4013      	ands	r3, r2
 8002a0c:	68f9      	ldr	r1, [r7, #12]
 8002a0e:	68ba      	ldr	r2, [r7, #8]
 8002a10:	4311      	orrs	r1, r2
 8002a12:	687a      	ldr	r2, [r7, #4]
 8002a14:	0192      	lsls	r2, r2, #6
 8002a16:	430a      	orrs	r2, r1
 8002a18:	4313      	orrs	r3, r2
 8002a1a:	6043      	str	r3, [r0, #4]
 8002a1c:	4906      	ldr	r1, [pc, #24]	; (8002a38 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8002a1e:	4b06      	ldr	r3, [pc, #24]	; (8002a38 <LL_RCC_PLL_ConfigDomain_SYS+0x44>)
 8002a20:	685b      	ldr	r3, [r3, #4]
 8002a22:	f423 3240 	bic.w	r2, r3, #196608	; 0x30000
 8002a26:	683b      	ldr	r3, [r7, #0]
 8002a28:	4313      	orrs	r3, r2
 8002a2a:	604b      	str	r3, [r1, #4]
 8002a2c:	bf00      	nop
 8002a2e:	3714      	adds	r7, #20
 8002a30:	46bd      	mov	sp, r7
 8002a32:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002a36:	4770      	bx	lr
 8002a38:	40023800 	.word	0x40023800
 8002a3c:	ffbf8000 	.word	0xffbf8000

08002a40 <LL_FLASH_SetLatency>:
 8002a40:	b480      	push	{r7}
 8002a42:	b083      	sub	sp, #12
 8002a44:	af00      	add	r7, sp, #0
 8002a46:	6078      	str	r0, [r7, #4]
 8002a48:	4906      	ldr	r1, [pc, #24]	; (8002a64 <LL_FLASH_SetLatency+0x24>)
 8002a4a:	4b06      	ldr	r3, [pc, #24]	; (8002a64 <LL_FLASH_SetLatency+0x24>)
 8002a4c:	681b      	ldr	r3, [r3, #0]
 8002a4e:	f023 020f 	bic.w	r2, r3, #15
 8002a52:	687b      	ldr	r3, [r7, #4]
 8002a54:	4313      	orrs	r3, r2
 8002a56:	600b      	str	r3, [r1, #0]
 8002a58:	bf00      	nop
 8002a5a:	370c      	adds	r7, #12
 8002a5c:	46bd      	mov	sp, r7
 8002a5e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002a62:	4770      	bx	lr
 8002a64:	40023c00 	.word	0x40023c00

08002a68 <LL_AHB1_GRP1_EnableClock>:
 8002a68:	b480      	push	{r7}
 8002a6a:	b085      	sub	sp, #20
 8002a6c:	af00      	add	r7, sp, #0
 8002a6e:	6078      	str	r0, [r7, #4]
 8002a70:	4908      	ldr	r1, [pc, #32]	; (8002a94 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8002a72:	4b08      	ldr	r3, [pc, #32]	; (8002a94 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8002a74:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8002a76:	687b      	ldr	r3, [r7, #4]
 8002a78:	4313      	orrs	r3, r2
 8002a7a:	630b      	str	r3, [r1, #48]	; 0x30
 8002a7c:	4b05      	ldr	r3, [pc, #20]	; (8002a94 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8002a7e:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8002a80:	687b      	ldr	r3, [r7, #4]
 8002a82:	4013      	ands	r3, r2
 8002a84:	60fb      	str	r3, [r7, #12]
 8002a86:	68fb      	ldr	r3, [r7, #12]
 8002a88:	bf00      	nop
 8002a8a:	3714      	adds	r7, #20
 8002a8c:	46bd      	mov	sp, r7
 8002a8e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002a92:	4770      	bx	lr
 8002a94:	40023800 	.word	0x40023800

08002a98 <LL_GPIO_SetPinMode>:
 8002a98:	b480      	push	{r7}
 8002a9a:	b089      	sub	sp, #36	; 0x24
 8002a9c:	af00      	add	r7, sp, #0
 8002a9e:	60f8      	str	r0, [r7, #12]
 8002aa0:	60b9      	str	r1, [r7, #8]
 8002aa2:	607a      	str	r2, [r7, #4]
 8002aa4:	68fb      	ldr	r3, [r7, #12]
 8002aa6:	681a      	ldr	r2, [r3, #0]
 8002aa8:	68bb      	ldr	r3, [r7, #8]
 8002aaa:	617b      	str	r3, [r7, #20]
 8002aac:	697b      	ldr	r3, [r7, #20]
 8002aae:	fa93 f3a3 	rbit	r3, r3
 8002ab2:	613b      	str	r3, [r7, #16]
 8002ab4:	693b      	ldr	r3, [r7, #16]
 8002ab6:	fab3 f383 	clz	r3, r3
 8002aba:	005b      	lsls	r3, r3, #1
 8002abc:	2103      	movs	r1, #3
 8002abe:	fa01 f303 	lsl.w	r3, r1, r3
 8002ac2:	43db      	mvns	r3, r3
 8002ac4:	401a      	ands	r2, r3
 8002ac6:	68bb      	ldr	r3, [r7, #8]
 8002ac8:	61fb      	str	r3, [r7, #28]
 8002aca:	69fb      	ldr	r3, [r7, #28]
 8002acc:	fa93 f3a3 	rbit	r3, r3
 8002ad0:	61bb      	str	r3, [r7, #24]
 8002ad2:	69bb      	ldr	r3, [r7, #24]
 8002ad4:	fab3 f383 	clz	r3, r3
 8002ad8:	005b      	lsls	r3, r3, #1
 8002ada:	6879      	ldr	r1, [r7, #4]
 8002adc:	fa01 f303 	lsl.w	r3, r1, r3
 8002ae0:	431a      	orrs	r2, r3
 8002ae2:	68fb      	ldr	r3, [r7, #12]
 8002ae4:	601a      	str	r2, [r3, #0]
 8002ae6:	bf00      	nop
 8002ae8:	3724      	adds	r7, #36	; 0x24
 8002aea:	46bd      	mov	sp, r7
 8002aec:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002af0:	4770      	bx	lr
	...

08002af4 <rcc_config>:
 8002af4:	b580      	push	{r7, lr}
 8002af6:	af00      	add	r7, sp, #0
 8002af8:	f7ff fed6 	bl	80028a8 <LL_RCC_HSE_Enable>
 8002afc:	bf00      	nop
 8002afe:	f7ff fee3 	bl	80028c8 <LL_RCC_HSE_IsReady>
 8002b02:	4603      	mov	r3, r0
 8002b04:	2b01      	cmp	r3, #1
 8002b06:	d1fa      	bne.n	8002afe <rcc_config+0xa>
 8002b08:	2005      	movs	r0, #5
 8002b0a:	f7ff ff99 	bl	8002a40 <LL_FLASH_SetLatency>
 8002b0e:	2300      	movs	r3, #0
 8002b10:	f44f 72a8 	mov.w	r2, #336	; 0x150
 8002b14:	2108      	movs	r1, #8
 8002b16:	f44f 0080 	mov.w	r0, #4194304	; 0x400000
 8002b1a:	f7ff ff6b 	bl	80029f4 <LL_RCC_PLL_ConfigDomain_SYS>
 8002b1e:	f7ff ff45 	bl	80029ac <LL_RCC_PLL_Enable>
 8002b22:	bf00      	nop
 8002b24:	f7ff ff52 	bl	80029cc <LL_RCC_PLL_IsReady>
 8002b28:	4603      	mov	r3, r0
 8002b2a:	2b01      	cmp	r3, #1
 8002b2c:	d1fa      	bne.n	8002b24 <rcc_config+0x30>
 8002b2e:	2000      	movs	r0, #0
 8002b30:	f7ff ff00 	bl	8002934 <LL_RCC_SetAHBPrescaler>
 8002b34:	2002      	movs	r0, #2
 8002b36:	f7ff fedb 	bl	80028f0 <LL_RCC_SetSysClkSource>
 8002b3a:	bf00      	nop
 8002b3c:	f7ff feec 	bl	8002918 <LL_RCC_GetSysClkSource>
 8002b40:	4603      	mov	r3, r0
 8002b42:	2b08      	cmp	r3, #8
 8002b44:	d1fa      	bne.n	8002b3c <rcc_config+0x48>
 8002b46:	f44f 50a0 	mov.w	r0, #5120	; 0x1400
 8002b4a:	f7ff ff07 	bl	800295c <LL_RCC_SetAPB1Prescaler>
 8002b4e:	f44f 4000 	mov.w	r0, #32768	; 0x8000
 8002b52:	f7ff ff17 	bl	8002984 <LL_RCC_SetAPB2Prescaler>
 8002b56:	4804      	ldr	r0, [pc, #16]	; (8002b68 <rcc_config+0x74>)
 8002b58:	f7ff fe84 	bl	8002864 <SysTick_Config>
 8002b5c:	4b03      	ldr	r3, [pc, #12]	; (8002b6c <rcc_config+0x78>)
 8002b5e:	4a04      	ldr	r2, [pc, #16]	; (8002b70 <rcc_config+0x7c>)
 8002b60:	601a      	str	r2, [r3, #0]
 8002b62:	bf00      	nop
 8002b64:	bd80      	pop	{r7, pc}
 8002b66:	bf00      	nop
 8002b68:	00029040 	.word	0x00029040
 8002b6c:	200008b0 	.word	0x200008b0
 8002b70:	0a037a00 	.word	0x0a037a00

08002b74 <gpio_config>:
 8002b74:	b580      	push	{r7, lr}
 8002b76:	af00      	add	r7, sp, #0
 8002b78:	2008      	movs	r0, #8
 8002b7a:	f7ff ff75 	bl	8002a68 <LL_AHB1_GRP1_EnableClock>
 8002b7e:	2201      	movs	r2, #1
 8002b80:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 8002b84:	4802      	ldr	r0, [pc, #8]	; (8002b90 <gpio_config+0x1c>)
 8002b86:	f7ff ff87 	bl	8002a98 <LL_GPIO_SetPinMode>
 8002b8a:	bf00      	nop
 8002b8c:	bd80      	pop	{r7, pc}
 8002b8e:	bf00      	nop
 8002b90:	40020c00 	.word	0x40020c00

08002b94 <main>:
 8002b94:	b580      	push	{r7, lr}
 8002b96:	b084      	sub	sp, #16
 8002b98:	af04      	add	r7, sp, #16
 8002b9a:	f7ff ffab 	bl	8002af4 <rcc_config>
 8002b9e:	f7ff ffe9 	bl	8002b74 <gpio_config>
 8002ba2:	2000      	movs	r0, #0
 8002ba4:	f7ff fe10 	bl	80027c8 <NVIC_SetPriorityGrouping>
 8002ba8:	4b1c      	ldr	r3, [pc, #112]	; (8002c1c <main+0x88>)
 8002baa:	9302      	str	r3, [sp, #8]
 8002bac:	4b1c      	ldr	r3, [pc, #112]	; (8002c20 <main+0x8c>)
 8002bae:	9301      	str	r3, [sp, #4]
 8002bb0:	2303      	movs	r3, #3
 8002bb2:	9300      	str	r3, [sp, #0]
 8002bb4:	2300      	movs	r3, #0
 8002bb6:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8002bba:	491a      	ldr	r1, [pc, #104]	; (8002c24 <main+0x90>)
 8002bbc:	481a      	ldr	r0, [pc, #104]	; (8002c28 <main+0x94>)
 8002bbe:	f004 fdc9 	bl	8007754 <xTaskCreateStatic>
 8002bc2:	4b1a      	ldr	r3, [pc, #104]	; (8002c2c <main+0x98>)
 8002bc4:	9302      	str	r3, [sp, #8]
 8002bc6:	4b1a      	ldr	r3, [pc, #104]	; (8002c30 <main+0x9c>)
 8002bc8:	9301      	str	r3, [sp, #4]
 8002bca:	2301      	movs	r3, #1
 8002bcc:	9300      	str	r3, [sp, #0]
 8002bce:	2300      	movs	r3, #0
 8002bd0:	f44f 7280 	mov.w	r2, #256	; 0x100
 8002bd4:	4917      	ldr	r1, [pc, #92]	; (8002c34 <main+0xa0>)
 8002bd6:	4818      	ldr	r0, [pc, #96]	; (8002c38 <main+0xa4>)
 8002bd8:	f004 fdbc 	bl	8007754 <xTaskCreateStatic>
 8002bdc:	4b17      	ldr	r3, [pc, #92]	; (8002c3c <main+0xa8>)
 8002bde:	9302      	str	r3, [sp, #8]
 8002be0:	4b17      	ldr	r3, [pc, #92]	; (8002c40 <main+0xac>)
 8002be2:	9301      	str	r3, [sp, #4]
 8002be4:	2301      	movs	r3, #1
 8002be6:	9300      	str	r3, [sp, #0]
 8002be8:	2300      	movs	r3, #0
 8002bea:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8002bee:	4915      	ldr	r1, [pc, #84]	; (8002c44 <main+0xb0>)
 8002bf0:	4815      	ldr	r0, [pc, #84]	; (8002c48 <main+0xb4>)
 8002bf2:	f004 fdaf 	bl	8007754 <xTaskCreateStatic>
 8002bf6:	4b15      	ldr	r3, [pc, #84]	; (8002c4c <main+0xb8>)
 8002bf8:	9302      	str	r3, [sp, #8]
 8002bfa:	4b15      	ldr	r3, [pc, #84]	; (8002c50 <main+0xbc>)
 8002bfc:	9301      	str	r3, [sp, #4]
 8002bfe:	2301      	movs	r3, #1
 8002c00:	9300      	str	r3, [sp, #0]
 8002c02:	2300      	movs	r3, #0
 8002c04:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8002c08:	4912      	ldr	r1, [pc, #72]	; (8002c54 <main+0xc0>)
 8002c0a:	4813      	ldr	r0, [pc, #76]	; (8002c58 <main+0xc4>)
 8002c0c:	f004 fda2 	bl	8007754 <xTaskCreateStatic>
 8002c10:	f004 ff7a 	bl	8007b08 <vTaskStartScheduler>
 8002c14:	2300      	movs	r3, #0
 8002c16:	4618      	mov	r0, r3
 8002c18:	46bd      	mov	sp, r7
 8002c1a:	bd80      	pop	{r7, pc}
 8002c1c:	20001478 	.word	0x20001478
 8002c20:	2000317c 	.word	0x2000317c
 8002c24:	08009860 	.word	0x08009860
 8002c28:	08004a81 	.word	0x08004a81
 8002c2c:	20005608 	.word	0x20005608
 8002c30:	200028f8 	.word	0x200028f8
 8002c34:	0800986c 	.word	0x0800986c
 8002c38:	08003afd 	.word	0x08003afd
 8002c3c:	20002cf8 	.word	0x20002cf8
 8002c40:	2000417c 	.word	0x2000417c
 8002c44:	08009878 	.word	0x08009878
 8002c48:	08006915 	.word	0x08006915
 8002c4c:	20005180 	.word	0x20005180
 8002c50:	200018f8 	.word	0x200018f8
 8002c54:	08009880 	.word	0x08009880
 8002c58:	08005921 	.word	0x08005921

08002c5c <NVIC_EnableIRQ>:
 8002c5c:	b480      	push	{r7}
 8002c5e:	b083      	sub	sp, #12
 8002c60:	af00      	add	r7, sp, #0
 8002c62:	4603      	mov	r3, r0
 8002c64:	71fb      	strb	r3, [r7, #7]
 8002c66:	4909      	ldr	r1, [pc, #36]	; (8002c8c <NVIC_EnableIRQ+0x30>)
 8002c68:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8002c6c:	095b      	lsrs	r3, r3, #5
 8002c6e:	79fa      	ldrb	r2, [r7, #7]
 8002c70:	f002 021f 	and.w	r2, r2, #31
 8002c74:	2001      	movs	r0, #1
 8002c76:	fa00 f202 	lsl.w	r2, r0, r2
 8002c7a:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 8002c7e:	bf00      	nop
 8002c80:	370c      	adds	r7, #12
 8002c82:	46bd      	mov	sp, r7
 8002c84:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002c88:	4770      	bx	lr
 8002c8a:	bf00      	nop
 8002c8c:	e000e100 	.word	0xe000e100

08002c90 <NVIC_SetPriority>:
 8002c90:	b480      	push	{r7}
 8002c92:	b083      	sub	sp, #12
 8002c94:	af00      	add	r7, sp, #0
 8002c96:	4603      	mov	r3, r0
 8002c98:	6039      	str	r1, [r7, #0]
 8002c9a:	71fb      	strb	r3, [r7, #7]
 8002c9c:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8002ca0:	2b00      	cmp	r3, #0
 8002ca2:	da0b      	bge.n	8002cbc <NVIC_SetPriority+0x2c>
 8002ca4:	490d      	ldr	r1, [pc, #52]	; (8002cdc <NVIC_SetPriority+0x4c>)
 8002ca6:	79fb      	ldrb	r3, [r7, #7]
 8002ca8:	f003 030f 	and.w	r3, r3, #15
 8002cac:	3b04      	subs	r3, #4
 8002cae:	683a      	ldr	r2, [r7, #0]
 8002cb0:	b2d2      	uxtb	r2, r2
 8002cb2:	0112      	lsls	r2, r2, #4
 8002cb4:	b2d2      	uxtb	r2, r2
 8002cb6:	440b      	add	r3, r1
 8002cb8:	761a      	strb	r2, [r3, #24]
 8002cba:	e009      	b.n	8002cd0 <NVIC_SetPriority+0x40>
 8002cbc:	4908      	ldr	r1, [pc, #32]	; (8002ce0 <NVIC_SetPriority+0x50>)
 8002cbe:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8002cc2:	683a      	ldr	r2, [r7, #0]
 8002cc4:	b2d2      	uxtb	r2, r2
 8002cc6:	0112      	lsls	r2, r2, #4
 8002cc8:	b2d2      	uxtb	r2, r2
 8002cca:	440b      	add	r3, r1
 8002ccc:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8002cd0:	bf00      	nop
 8002cd2:	370c      	adds	r7, #12
 8002cd4:	46bd      	mov	sp, r7
 8002cd6:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002cda:	4770      	bx	lr
 8002cdc:	e000ed00 	.word	0xe000ed00
 8002ce0:	e000e100 	.word	0xe000e100

08002ce4 <LL_GPIO_SetPinMode>:
 8002ce4:	b480      	push	{r7}
 8002ce6:	b089      	sub	sp, #36	; 0x24
 8002ce8:	af00      	add	r7, sp, #0
 8002cea:	60f8      	str	r0, [r7, #12]
 8002cec:	60b9      	str	r1, [r7, #8]
 8002cee:	607a      	str	r2, [r7, #4]
 8002cf0:	68fb      	ldr	r3, [r7, #12]
 8002cf2:	681a      	ldr	r2, [r3, #0]
 8002cf4:	68bb      	ldr	r3, [r7, #8]
 8002cf6:	617b      	str	r3, [r7, #20]
 8002cf8:	697b      	ldr	r3, [r7, #20]
 8002cfa:	fa93 f3a3 	rbit	r3, r3
 8002cfe:	613b      	str	r3, [r7, #16]
 8002d00:	693b      	ldr	r3, [r7, #16]
 8002d02:	fab3 f383 	clz	r3, r3
 8002d06:	005b      	lsls	r3, r3, #1
 8002d08:	2103      	movs	r1, #3
 8002d0a:	fa01 f303 	lsl.w	r3, r1, r3
 8002d0e:	43db      	mvns	r3, r3
 8002d10:	401a      	ands	r2, r3
 8002d12:	68bb      	ldr	r3, [r7, #8]
 8002d14:	61fb      	str	r3, [r7, #28]
 8002d16:	69fb      	ldr	r3, [r7, #28]
 8002d18:	fa93 f3a3 	rbit	r3, r3
 8002d1c:	61bb      	str	r3, [r7, #24]
 8002d1e:	69bb      	ldr	r3, [r7, #24]
 8002d20:	fab3 f383 	clz	r3, r3
 8002d24:	005b      	lsls	r3, r3, #1
 8002d26:	6879      	ldr	r1, [r7, #4]
 8002d28:	fa01 f303 	lsl.w	r3, r1, r3
 8002d2c:	431a      	orrs	r2, r3
 8002d2e:	68fb      	ldr	r3, [r7, #12]
 8002d30:	601a      	str	r2, [r3, #0]
 8002d32:	bf00      	nop
 8002d34:	3724      	adds	r7, #36	; 0x24
 8002d36:	46bd      	mov	sp, r7
 8002d38:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002d3c:	4770      	bx	lr

08002d3e <LL_GPIO_SetPinOutputType>:
 8002d3e:	b480      	push	{r7}
 8002d40:	b085      	sub	sp, #20
 8002d42:	af00      	add	r7, sp, #0
 8002d44:	60f8      	str	r0, [r7, #12]
 8002d46:	60b9      	str	r1, [r7, #8]
 8002d48:	607a      	str	r2, [r7, #4]
 8002d4a:	68fb      	ldr	r3, [r7, #12]
 8002d4c:	685a      	ldr	r2, [r3, #4]
 8002d4e:	68bb      	ldr	r3, [r7, #8]
 8002d50:	43db      	mvns	r3, r3
 8002d52:	401a      	ands	r2, r3
 8002d54:	68bb      	ldr	r3, [r7, #8]
 8002d56:	6879      	ldr	r1, [r7, #4]
 8002d58:	fb01 f303 	mul.w	r3, r1, r3
 8002d5c:	431a      	orrs	r2, r3
 8002d5e:	68fb      	ldr	r3, [r7, #12]
 8002d60:	605a      	str	r2, [r3, #4]
 8002d62:	bf00      	nop
 8002d64:	3714      	adds	r7, #20
 8002d66:	46bd      	mov	sp, r7
 8002d68:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002d6c:	4770      	bx	lr

08002d6e <LL_GPIO_SetPinPull>:
 8002d6e:	b480      	push	{r7}
 8002d70:	b089      	sub	sp, #36	; 0x24
 8002d72:	af00      	add	r7, sp, #0
 8002d74:	60f8      	str	r0, [r7, #12]
 8002d76:	60b9      	str	r1, [r7, #8]
 8002d78:	607a      	str	r2, [r7, #4]
 8002d7a:	68fb      	ldr	r3, [r7, #12]
 8002d7c:	68da      	ldr	r2, [r3, #12]
 8002d7e:	68bb      	ldr	r3, [r7, #8]
 8002d80:	617b      	str	r3, [r7, #20]
 8002d82:	697b      	ldr	r3, [r7, #20]
 8002d84:	fa93 f3a3 	rbit	r3, r3
 8002d88:	613b      	str	r3, [r7, #16]
 8002d8a:	693b      	ldr	r3, [r7, #16]
 8002d8c:	fab3 f383 	clz	r3, r3
 8002d90:	005b      	lsls	r3, r3, #1
 8002d92:	2103      	movs	r1, #3
 8002d94:	fa01 f303 	lsl.w	r3, r1, r3
 8002d98:	43db      	mvns	r3, r3
 8002d9a:	401a      	ands	r2, r3
 8002d9c:	68bb      	ldr	r3, [r7, #8]
 8002d9e:	61fb      	str	r3, [r7, #28]
 8002da0:	69fb      	ldr	r3, [r7, #28]
 8002da2:	fa93 f3a3 	rbit	r3, r3
 8002da6:	61bb      	str	r3, [r7, #24]
 8002da8:	69bb      	ldr	r3, [r7, #24]
 8002daa:	fab3 f383 	clz	r3, r3
 8002dae:	005b      	lsls	r3, r3, #1
 8002db0:	6879      	ldr	r1, [r7, #4]
 8002db2:	fa01 f303 	lsl.w	r3, r1, r3
 8002db6:	431a      	orrs	r2, r3
 8002db8:	68fb      	ldr	r3, [r7, #12]
 8002dba:	60da      	str	r2, [r3, #12]
 8002dbc:	bf00      	nop
 8002dbe:	3724      	adds	r7, #36	; 0x24
 8002dc0:	46bd      	mov	sp, r7
 8002dc2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002dc6:	4770      	bx	lr

08002dc8 <LL_GPIO_SetAFPin_0_7>:
 8002dc8:	b480      	push	{r7}
 8002dca:	b089      	sub	sp, #36	; 0x24
 8002dcc:	af00      	add	r7, sp, #0
 8002dce:	60f8      	str	r0, [r7, #12]
 8002dd0:	60b9      	str	r1, [r7, #8]
 8002dd2:	607a      	str	r2, [r7, #4]
 8002dd4:	68fb      	ldr	r3, [r7, #12]
 8002dd6:	6a1a      	ldr	r2, [r3, #32]
 8002dd8:	68bb      	ldr	r3, [r7, #8]
 8002dda:	617b      	str	r3, [r7, #20]
 8002ddc:	697b      	ldr	r3, [r7, #20]
 8002dde:	fa93 f3a3 	rbit	r3, r3
 8002de2:	613b      	str	r3, [r7, #16]
 8002de4:	693b      	ldr	r3, [r7, #16]
 8002de6:	fab3 f383 	clz	r3, r3
 8002dea:	009b      	lsls	r3, r3, #2
 8002dec:	210f      	movs	r1, #15
 8002dee:	fa01 f303 	lsl.w	r3, r1, r3
 8002df2:	43db      	mvns	r3, r3
 8002df4:	401a      	ands	r2, r3
 8002df6:	68bb      	ldr	r3, [r7, #8]
 8002df8:	61fb      	str	r3, [r7, #28]
 8002dfa:	69fb      	ldr	r3, [r7, #28]
 8002dfc:	fa93 f3a3 	rbit	r3, r3
 8002e00:	61bb      	str	r3, [r7, #24]
 8002e02:	69bb      	ldr	r3, [r7, #24]
 8002e04:	fab3 f383 	clz	r3, r3
 8002e08:	009b      	lsls	r3, r3, #2
 8002e0a:	6879      	ldr	r1, [r7, #4]
 8002e0c:	fa01 f303 	lsl.w	r3, r1, r3
 8002e10:	431a      	orrs	r2, r3
 8002e12:	68fb      	ldr	r3, [r7, #12]
 8002e14:	621a      	str	r2, [r3, #32]
 8002e16:	bf00      	nop
 8002e18:	3724      	adds	r7, #36	; 0x24
 8002e1a:	46bd      	mov	sp, r7
 8002e1c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002e20:	4770      	bx	lr

08002e22 <LL_GPIO_SetAFPin_8_15>:
 8002e22:	b480      	push	{r7}
 8002e24:	b089      	sub	sp, #36	; 0x24
 8002e26:	af00      	add	r7, sp, #0
 8002e28:	60f8      	str	r0, [r7, #12]
 8002e2a:	60b9      	str	r1, [r7, #8]
 8002e2c:	607a      	str	r2, [r7, #4]
 8002e2e:	68fb      	ldr	r3, [r7, #12]
 8002e30:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 8002e32:	68bb      	ldr	r3, [r7, #8]
 8002e34:	0a1b      	lsrs	r3, r3, #8
 8002e36:	617b      	str	r3, [r7, #20]
 8002e38:	697b      	ldr	r3, [r7, #20]
 8002e3a:	fa93 f3a3 	rbit	r3, r3
 8002e3e:	613b      	str	r3, [r7, #16]
 8002e40:	693b      	ldr	r3, [r7, #16]
 8002e42:	fab3 f383 	clz	r3, r3
 8002e46:	009b      	lsls	r3, r3, #2
 8002e48:	210f      	movs	r1, #15
 8002e4a:	fa01 f303 	lsl.w	r3, r1, r3
 8002e4e:	43db      	mvns	r3, r3
 8002e50:	401a      	ands	r2, r3
 8002e52:	68bb      	ldr	r3, [r7, #8]
 8002e54:	0a1b      	lsrs	r3, r3, #8
 8002e56:	61fb      	str	r3, [r7, #28]
 8002e58:	69fb      	ldr	r3, [r7, #28]
 8002e5a:	fa93 f3a3 	rbit	r3, r3
 8002e5e:	61bb      	str	r3, [r7, #24]
 8002e60:	69bb      	ldr	r3, [r7, #24]
 8002e62:	fab3 f383 	clz	r3, r3
 8002e66:	009b      	lsls	r3, r3, #2
 8002e68:	6879      	ldr	r1, [r7, #4]
 8002e6a:	fa01 f303 	lsl.w	r3, r1, r3
 8002e6e:	431a      	orrs	r2, r3
 8002e70:	68fb      	ldr	r3, [r7, #12]
 8002e72:	625a      	str	r2, [r3, #36]	; 0x24
 8002e74:	bf00      	nop
 8002e76:	3724      	adds	r7, #36	; 0x24
 8002e78:	46bd      	mov	sp, r7
 8002e7a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002e7e:	4770      	bx	lr

08002e80 <LL_GPIO_SetOutputPin>:
 8002e80:	b480      	push	{r7}
 8002e82:	b083      	sub	sp, #12
 8002e84:	af00      	add	r7, sp, #0
 8002e86:	6078      	str	r0, [r7, #4]
 8002e88:	6039      	str	r1, [r7, #0]
 8002e8a:	687b      	ldr	r3, [r7, #4]
 8002e8c:	683a      	ldr	r2, [r7, #0]
 8002e8e:	619a      	str	r2, [r3, #24]
 8002e90:	bf00      	nop
 8002e92:	370c      	adds	r7, #12
 8002e94:	46bd      	mov	sp, r7
 8002e96:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002e9a:	4770      	bx	lr

08002e9c <LL_GPIO_ResetOutputPin>:
 8002e9c:	b480      	push	{r7}
 8002e9e:	b083      	sub	sp, #12
 8002ea0:	af00      	add	r7, sp, #0
 8002ea2:	6078      	str	r0, [r7, #4]
 8002ea4:	6039      	str	r1, [r7, #0]
 8002ea6:	683b      	ldr	r3, [r7, #0]
 8002ea8:	041a      	lsls	r2, r3, #16
 8002eaa:	687b      	ldr	r3, [r7, #4]
 8002eac:	619a      	str	r2, [r3, #24]
 8002eae:	bf00      	nop
 8002eb0:	370c      	adds	r7, #12
 8002eb2:	46bd      	mov	sp, r7
 8002eb4:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002eb8:	4770      	bx	lr
	...

08002ebc <LL_AHB1_GRP1_EnableClock>:
 8002ebc:	b480      	push	{r7}
 8002ebe:	b085      	sub	sp, #20
 8002ec0:	af00      	add	r7, sp, #0
 8002ec2:	6078      	str	r0, [r7, #4]
 8002ec4:	4908      	ldr	r1, [pc, #32]	; (8002ee8 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8002ec6:	4b08      	ldr	r3, [pc, #32]	; (8002ee8 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8002ec8:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8002eca:	687b      	ldr	r3, [r7, #4]
 8002ecc:	4313      	orrs	r3, r2
 8002ece:	630b      	str	r3, [r1, #48]	; 0x30
 8002ed0:	4b05      	ldr	r3, [pc, #20]	; (8002ee8 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8002ed2:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8002ed4:	687b      	ldr	r3, [r7, #4]
 8002ed6:	4013      	ands	r3, r2
 8002ed8:	60fb      	str	r3, [r7, #12]
 8002eda:	68fb      	ldr	r3, [r7, #12]
 8002edc:	bf00      	nop
 8002ede:	3714      	adds	r7, #20
 8002ee0:	46bd      	mov	sp, r7
 8002ee2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002ee6:	4770      	bx	lr
 8002ee8:	40023800 	.word	0x40023800

08002eec <LL_APB1_GRP1_EnableClock>:
 8002eec:	b480      	push	{r7}
 8002eee:	b085      	sub	sp, #20
 8002ef0:	af00      	add	r7, sp, #0
 8002ef2:	6078      	str	r0, [r7, #4]
 8002ef4:	4908      	ldr	r1, [pc, #32]	; (8002f18 <LL_APB1_GRP1_EnableClock+0x2c>)
 8002ef6:	4b08      	ldr	r3, [pc, #32]	; (8002f18 <LL_APB1_GRP1_EnableClock+0x2c>)
 8002ef8:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8002efa:	687b      	ldr	r3, [r7, #4]
 8002efc:	4313      	orrs	r3, r2
 8002efe:	640b      	str	r3, [r1, #64]	; 0x40
 8002f00:	4b05      	ldr	r3, [pc, #20]	; (8002f18 <LL_APB1_GRP1_EnableClock+0x2c>)
 8002f02:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8002f04:	687b      	ldr	r3, [r7, #4]
 8002f06:	4013      	ands	r3, r2
 8002f08:	60fb      	str	r3, [r7, #12]
 8002f0a:	68fb      	ldr	r3, [r7, #12]
 8002f0c:	bf00      	nop
 8002f0e:	3714      	adds	r7, #20
 8002f10:	46bd      	mov	sp, r7
 8002f12:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002f16:	4770      	bx	lr
 8002f18:	40023800 	.word	0x40023800

08002f1c <LL_TIM_EnableCounter>:
 8002f1c:	b480      	push	{r7}
 8002f1e:	b083      	sub	sp, #12
 8002f20:	af00      	add	r7, sp, #0
 8002f22:	6078      	str	r0, [r7, #4]
 8002f24:	687b      	ldr	r3, [r7, #4]
 8002f26:	681b      	ldr	r3, [r3, #0]
 8002f28:	f043 0201 	orr.w	r2, r3, #1
 8002f2c:	687b      	ldr	r3, [r7, #4]
 8002f2e:	601a      	str	r2, [r3, #0]
 8002f30:	bf00      	nop
 8002f32:	370c      	adds	r7, #12
 8002f34:	46bd      	mov	sp, r7
 8002f36:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002f3a:	4770      	bx	lr

08002f3c <LL_TIM_EnableUpdateEvent>:
 8002f3c:	b480      	push	{r7}
 8002f3e:	b083      	sub	sp, #12
 8002f40:	af00      	add	r7, sp, #0
 8002f42:	6078      	str	r0, [r7, #4]
 8002f44:	687b      	ldr	r3, [r7, #4]
 8002f46:	681b      	ldr	r3, [r3, #0]
 8002f48:	f023 0202 	bic.w	r2, r3, #2
 8002f4c:	687b      	ldr	r3, [r7, #4]
 8002f4e:	601a      	str	r2, [r3, #0]
 8002f50:	bf00      	nop
 8002f52:	370c      	adds	r7, #12
 8002f54:	46bd      	mov	sp, r7
 8002f56:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002f5a:	4770      	bx	lr

08002f5c <LL_TIM_SetUpdateSource>:
 8002f5c:	b480      	push	{r7}
 8002f5e:	b083      	sub	sp, #12
 8002f60:	af00      	add	r7, sp, #0
 8002f62:	6078      	str	r0, [r7, #4]
 8002f64:	6039      	str	r1, [r7, #0]
 8002f66:	687b      	ldr	r3, [r7, #4]
 8002f68:	681b      	ldr	r3, [r3, #0]
 8002f6a:	f023 0204 	bic.w	r2, r3, #4
 8002f6e:	683b      	ldr	r3, [r7, #0]
 8002f70:	431a      	orrs	r2, r3
 8002f72:	687b      	ldr	r3, [r7, #4]
 8002f74:	601a      	str	r2, [r3, #0]
 8002f76:	bf00      	nop
 8002f78:	370c      	adds	r7, #12
 8002f7a:	46bd      	mov	sp, r7
 8002f7c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002f80:	4770      	bx	lr

08002f82 <LL_TIM_SetCounterMode>:
 8002f82:	b480      	push	{r7}
 8002f84:	b083      	sub	sp, #12
 8002f86:	af00      	add	r7, sp, #0
 8002f88:	6078      	str	r0, [r7, #4]
 8002f8a:	6039      	str	r1, [r7, #0]
 8002f8c:	687b      	ldr	r3, [r7, #4]
 8002f8e:	681b      	ldr	r3, [r3, #0]
 8002f90:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8002f94:	683b      	ldr	r3, [r7, #0]
 8002f96:	431a      	orrs	r2, r3
 8002f98:	687b      	ldr	r3, [r7, #4]
 8002f9a:	601a      	str	r2, [r3, #0]
 8002f9c:	bf00      	nop
 8002f9e:	370c      	adds	r7, #12
 8002fa0:	46bd      	mov	sp, r7
 8002fa2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002fa6:	4770      	bx	lr

08002fa8 <LL_TIM_EnableARRPreload>:
 8002fa8:	b480      	push	{r7}
 8002faa:	b083      	sub	sp, #12
 8002fac:	af00      	add	r7, sp, #0
 8002fae:	6078      	str	r0, [r7, #4]
 8002fb0:	687b      	ldr	r3, [r7, #4]
 8002fb2:	681b      	ldr	r3, [r3, #0]
 8002fb4:	f043 0280 	orr.w	r2, r3, #128	; 0x80
 8002fb8:	687b      	ldr	r3, [r7, #4]
 8002fba:	601a      	str	r2, [r3, #0]
 8002fbc:	bf00      	nop
 8002fbe:	370c      	adds	r7, #12
 8002fc0:	46bd      	mov	sp, r7
 8002fc2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002fc6:	4770      	bx	lr

08002fc8 <LL_TIM_SetClockDivision>:
 8002fc8:	b480      	push	{r7}
 8002fca:	b083      	sub	sp, #12
 8002fcc:	af00      	add	r7, sp, #0
 8002fce:	6078      	str	r0, [r7, #4]
 8002fd0:	6039      	str	r1, [r7, #0]
 8002fd2:	687b      	ldr	r3, [r7, #4]
 8002fd4:	681b      	ldr	r3, [r3, #0]
 8002fd6:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 8002fda:	683b      	ldr	r3, [r7, #0]
 8002fdc:	431a      	orrs	r2, r3
 8002fde:	687b      	ldr	r3, [r7, #4]
 8002fe0:	601a      	str	r2, [r3, #0]
 8002fe2:	bf00      	nop
 8002fe4:	370c      	adds	r7, #12
 8002fe6:	46bd      	mov	sp, r7
 8002fe8:	f85d 7b04 	ldr.w	r7, [sp], #4
 8002fec:	4770      	bx	lr

08002fee <LL_TIM_SetPrescaler>:
 8002fee:	b480      	push	{r7}
 8002ff0:	b083      	sub	sp, #12
 8002ff2:	af00      	add	r7, sp, #0
 8002ff4:	6078      	str	r0, [r7, #4]
 8002ff6:	6039      	str	r1, [r7, #0]
 8002ff8:	687b      	ldr	r3, [r7, #4]
 8002ffa:	683a      	ldr	r2, [r7, #0]
 8002ffc:	629a      	str	r2, [r3, #40]	; 0x28
 8002ffe:	bf00      	nop
 8003000:	370c      	adds	r7, #12
 8003002:	46bd      	mov	sp, r7
 8003004:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003008:	4770      	bx	lr

0800300a <LL_TIM_SetAutoReload>:
 800300a:	b480      	push	{r7}
 800300c:	b083      	sub	sp, #12
 800300e:	af00      	add	r7, sp, #0
 8003010:	6078      	str	r0, [r7, #4]
 8003012:	6039      	str	r1, [r7, #0]
 8003014:	687b      	ldr	r3, [r7, #4]
 8003016:	683a      	ldr	r2, [r7, #0]
 8003018:	62da      	str	r2, [r3, #44]	; 0x2c
 800301a:	bf00      	nop
 800301c:	370c      	adds	r7, #12
 800301e:	46bd      	mov	sp, r7
 8003020:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003024:	4770      	bx	lr

08003026 <LL_TIM_CC_EnableChannel>:
 8003026:	b480      	push	{r7}
 8003028:	b083      	sub	sp, #12
 800302a:	af00      	add	r7, sp, #0
 800302c:	6078      	str	r0, [r7, #4]
 800302e:	6039      	str	r1, [r7, #0]
 8003030:	687b      	ldr	r3, [r7, #4]
 8003032:	6a1a      	ldr	r2, [r3, #32]
 8003034:	683b      	ldr	r3, [r7, #0]
 8003036:	431a      	orrs	r2, r3
 8003038:	687b      	ldr	r3, [r7, #4]
 800303a:	621a      	str	r2, [r3, #32]
 800303c:	bf00      	nop
 800303e:	370c      	adds	r7, #12
 8003040:	46bd      	mov	sp, r7
 8003042:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003046:	4770      	bx	lr

08003048 <LL_TIM_OC_SetMode>:
 8003048:	b4b0      	push	{r4, r5, r7}
 800304a:	b085      	sub	sp, #20
 800304c:	af00      	add	r7, sp, #0
 800304e:	60f8      	str	r0, [r7, #12]
 8003050:	60b9      	str	r1, [r7, #8]
 8003052:	607a      	str	r2, [r7, #4]
 8003054:	68bb      	ldr	r3, [r7, #8]
 8003056:	2b01      	cmp	r3, #1
 8003058:	d01c      	beq.n	8003094 <LL_TIM_OC_SetMode+0x4c>
 800305a:	68bb      	ldr	r3, [r7, #8]
 800305c:	2b04      	cmp	r3, #4
 800305e:	d017      	beq.n	8003090 <LL_TIM_OC_SetMode+0x48>
 8003060:	68bb      	ldr	r3, [r7, #8]
 8003062:	2b10      	cmp	r3, #16
 8003064:	d012      	beq.n	800308c <LL_TIM_OC_SetMode+0x44>
 8003066:	68bb      	ldr	r3, [r7, #8]
 8003068:	2b40      	cmp	r3, #64	; 0x40
 800306a:	d00d      	beq.n	8003088 <LL_TIM_OC_SetMode+0x40>
 800306c:	68bb      	ldr	r3, [r7, #8]
 800306e:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8003072:	d007      	beq.n	8003084 <LL_TIM_OC_SetMode+0x3c>
 8003074:	68bb      	ldr	r3, [r7, #8]
 8003076:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 800307a:	d101      	bne.n	8003080 <LL_TIM_OC_SetMode+0x38>
 800307c:	2305      	movs	r3, #5
 800307e:	e00a      	b.n	8003096 <LL_TIM_OC_SetMode+0x4e>
 8003080:	2306      	movs	r3, #6
 8003082:	e008      	b.n	8003096 <LL_TIM_OC_SetMode+0x4e>
 8003084:	2304      	movs	r3, #4
 8003086:	e006      	b.n	8003096 <LL_TIM_OC_SetMode+0x4e>
 8003088:	2303      	movs	r3, #3
 800308a:	e004      	b.n	8003096 <LL_TIM_OC_SetMode+0x4e>
 800308c:	2302      	movs	r3, #2
 800308e:	e002      	b.n	8003096 <LL_TIM_OC_SetMode+0x4e>
 8003090:	2301      	movs	r3, #1
 8003092:	e000      	b.n	8003096 <LL_TIM_OC_SetMode+0x4e>
 8003094:	2300      	movs	r3, #0
 8003096:	461d      	mov	r5, r3
 8003098:	68fb      	ldr	r3, [r7, #12]
 800309a:	3318      	adds	r3, #24
 800309c:	461a      	mov	r2, r3
 800309e:	4629      	mov	r1, r5
 80030a0:	4b0c      	ldr	r3, [pc, #48]	; (80030d4 <LL_TIM_OC_SetMode+0x8c>)
 80030a2:	5c5b      	ldrb	r3, [r3, r1]
 80030a4:	4413      	add	r3, r2
 80030a6:	461c      	mov	r4, r3
 80030a8:	6822      	ldr	r2, [r4, #0]
 80030aa:	4629      	mov	r1, r5
 80030ac:	4b0a      	ldr	r3, [pc, #40]	; (80030d8 <LL_TIM_OC_SetMode+0x90>)
 80030ae:	5c5b      	ldrb	r3, [r3, r1]
 80030b0:	4619      	mov	r1, r3
 80030b2:	2373      	movs	r3, #115	; 0x73
 80030b4:	408b      	lsls	r3, r1
 80030b6:	43db      	mvns	r3, r3
 80030b8:	401a      	ands	r2, r3
 80030ba:	4629      	mov	r1, r5
 80030bc:	4b06      	ldr	r3, [pc, #24]	; (80030d8 <LL_TIM_OC_SetMode+0x90>)
 80030be:	5c5b      	ldrb	r3, [r3, r1]
 80030c0:	4619      	mov	r1, r3
 80030c2:	687b      	ldr	r3, [r7, #4]
 80030c4:	408b      	lsls	r3, r1
 80030c6:	4313      	orrs	r3, r2
 80030c8:	6023      	str	r3, [r4, #0]
 80030ca:	bf00      	nop
 80030cc:	3714      	adds	r7, #20
 80030ce:	46bd      	mov	sp, r7
 80030d0:	bcb0      	pop	{r4, r5, r7}
 80030d2:	4770      	bx	lr
 80030d4:	08009cf0 	.word	0x08009cf0
 80030d8:	08009cf8 	.word	0x08009cf8

080030dc <LL_TIM_OC_EnableFast>:
 80030dc:	b4b0      	push	{r4, r5, r7}
 80030de:	b083      	sub	sp, #12
 80030e0:	af00      	add	r7, sp, #0
 80030e2:	6078      	str	r0, [r7, #4]
 80030e4:	6039      	str	r1, [r7, #0]
 80030e6:	683b      	ldr	r3, [r7, #0]
 80030e8:	2b01      	cmp	r3, #1
 80030ea:	d01c      	beq.n	8003126 <LL_TIM_OC_EnableFast+0x4a>
 80030ec:	683b      	ldr	r3, [r7, #0]
 80030ee:	2b04      	cmp	r3, #4
 80030f0:	d017      	beq.n	8003122 <LL_TIM_OC_EnableFast+0x46>
 80030f2:	683b      	ldr	r3, [r7, #0]
 80030f4:	2b10      	cmp	r3, #16
 80030f6:	d012      	beq.n	800311e <LL_TIM_OC_EnableFast+0x42>
 80030f8:	683b      	ldr	r3, [r7, #0]
 80030fa:	2b40      	cmp	r3, #64	; 0x40
 80030fc:	d00d      	beq.n	800311a <LL_TIM_OC_EnableFast+0x3e>
 80030fe:	683b      	ldr	r3, [r7, #0]
 8003100:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8003104:	d007      	beq.n	8003116 <LL_TIM_OC_EnableFast+0x3a>
 8003106:	683b      	ldr	r3, [r7, #0]
 8003108:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 800310c:	d101      	bne.n	8003112 <LL_TIM_OC_EnableFast+0x36>
 800310e:	2305      	movs	r3, #5
 8003110:	e00a      	b.n	8003128 <LL_TIM_OC_EnableFast+0x4c>
 8003112:	2306      	movs	r3, #6
 8003114:	e008      	b.n	8003128 <LL_TIM_OC_EnableFast+0x4c>
 8003116:	2304      	movs	r3, #4
 8003118:	e006      	b.n	8003128 <LL_TIM_OC_EnableFast+0x4c>
 800311a:	2303      	movs	r3, #3
 800311c:	e004      	b.n	8003128 <LL_TIM_OC_EnableFast+0x4c>
 800311e:	2302      	movs	r3, #2
 8003120:	e002      	b.n	8003128 <LL_TIM_OC_EnableFast+0x4c>
 8003122:	2301      	movs	r3, #1
 8003124:	e000      	b.n	8003128 <LL_TIM_OC_EnableFast+0x4c>
 8003126:	2300      	movs	r3, #0
 8003128:	461d      	mov	r5, r3
 800312a:	687b      	ldr	r3, [r7, #4]
 800312c:	3318      	adds	r3, #24
 800312e:	461a      	mov	r2, r3
 8003130:	4629      	mov	r1, r5
 8003132:	4b09      	ldr	r3, [pc, #36]	; (8003158 <LL_TIM_OC_EnableFast+0x7c>)
 8003134:	5c5b      	ldrb	r3, [r3, r1]
 8003136:	4413      	add	r3, r2
 8003138:	461c      	mov	r4, r3
 800313a:	6822      	ldr	r2, [r4, #0]
 800313c:	4629      	mov	r1, r5
 800313e:	4b07      	ldr	r3, [pc, #28]	; (800315c <LL_TIM_OC_EnableFast+0x80>)
 8003140:	5c5b      	ldrb	r3, [r3, r1]
 8003142:	4619      	mov	r1, r3
 8003144:	2304      	movs	r3, #4
 8003146:	408b      	lsls	r3, r1
 8003148:	4313      	orrs	r3, r2
 800314a:	6023      	str	r3, [r4, #0]
 800314c:	bf00      	nop
 800314e:	370c      	adds	r7, #12
 8003150:	46bd      	mov	sp, r7
 8003152:	bcb0      	pop	{r4, r5, r7}
 8003154:	4770      	bx	lr
 8003156:	bf00      	nop
 8003158:	08009cf0 	.word	0x08009cf0
 800315c:	08009cf8 	.word	0x08009cf8

08003160 <LL_TIM_OC_EnablePreload>:
 8003160:	b4b0      	push	{r4, r5, r7}
 8003162:	b083      	sub	sp, #12
 8003164:	af00      	add	r7, sp, #0
 8003166:	6078      	str	r0, [r7, #4]
 8003168:	6039      	str	r1, [r7, #0]
 800316a:	683b      	ldr	r3, [r7, #0]
 800316c:	2b01      	cmp	r3, #1
 800316e:	d01c      	beq.n	80031aa <LL_TIM_OC_EnablePreload+0x4a>
 8003170:	683b      	ldr	r3, [r7, #0]
 8003172:	2b04      	cmp	r3, #4
 8003174:	d017      	beq.n	80031a6 <LL_TIM_OC_EnablePreload+0x46>
 8003176:	683b      	ldr	r3, [r7, #0]
 8003178:	2b10      	cmp	r3, #16
 800317a:	d012      	beq.n	80031a2 <LL_TIM_OC_EnablePreload+0x42>
 800317c:	683b      	ldr	r3, [r7, #0]
 800317e:	2b40      	cmp	r3, #64	; 0x40
 8003180:	d00d      	beq.n	800319e <LL_TIM_OC_EnablePreload+0x3e>
 8003182:	683b      	ldr	r3, [r7, #0]
 8003184:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8003188:	d007      	beq.n	800319a <LL_TIM_OC_EnablePreload+0x3a>
 800318a:	683b      	ldr	r3, [r7, #0]
 800318c:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8003190:	d101      	bne.n	8003196 <LL_TIM_OC_EnablePreload+0x36>
 8003192:	2305      	movs	r3, #5
 8003194:	e00a      	b.n	80031ac <LL_TIM_OC_EnablePreload+0x4c>
 8003196:	2306      	movs	r3, #6
 8003198:	e008      	b.n	80031ac <LL_TIM_OC_EnablePreload+0x4c>
 800319a:	2304      	movs	r3, #4
 800319c:	e006      	b.n	80031ac <LL_TIM_OC_EnablePreload+0x4c>
 800319e:	2303      	movs	r3, #3
 80031a0:	e004      	b.n	80031ac <LL_TIM_OC_EnablePreload+0x4c>
 80031a2:	2302      	movs	r3, #2
 80031a4:	e002      	b.n	80031ac <LL_TIM_OC_EnablePreload+0x4c>
 80031a6:	2301      	movs	r3, #1
 80031a8:	e000      	b.n	80031ac <LL_TIM_OC_EnablePreload+0x4c>
 80031aa:	2300      	movs	r3, #0
 80031ac:	461d      	mov	r5, r3
 80031ae:	687b      	ldr	r3, [r7, #4]
 80031b0:	3318      	adds	r3, #24
 80031b2:	461a      	mov	r2, r3
 80031b4:	4629      	mov	r1, r5
 80031b6:	4b09      	ldr	r3, [pc, #36]	; (80031dc <LL_TIM_OC_EnablePreload+0x7c>)
 80031b8:	5c5b      	ldrb	r3, [r3, r1]
 80031ba:	4413      	add	r3, r2
 80031bc:	461c      	mov	r4, r3
 80031be:	6822      	ldr	r2, [r4, #0]
 80031c0:	4629      	mov	r1, r5
 80031c2:	4b07      	ldr	r3, [pc, #28]	; (80031e0 <LL_TIM_OC_EnablePreload+0x80>)
 80031c4:	5c5b      	ldrb	r3, [r3, r1]
 80031c6:	4619      	mov	r1, r3
 80031c8:	2308      	movs	r3, #8
 80031ca:	408b      	lsls	r3, r1
 80031cc:	4313      	orrs	r3, r2
 80031ce:	6023      	str	r3, [r4, #0]
 80031d0:	bf00      	nop
 80031d2:	370c      	adds	r7, #12
 80031d4:	46bd      	mov	sp, r7
 80031d6:	bcb0      	pop	{r4, r5, r7}
 80031d8:	4770      	bx	lr
 80031da:	bf00      	nop
 80031dc:	08009cf0 	.word	0x08009cf0
 80031e0:	08009cf8 	.word	0x08009cf8

080031e4 <LL_TIM_OC_SetCompareCH1>:
 80031e4:	b480      	push	{r7}
 80031e6:	b083      	sub	sp, #12
 80031e8:	af00      	add	r7, sp, #0
 80031ea:	6078      	str	r0, [r7, #4]
 80031ec:	6039      	str	r1, [r7, #0]
 80031ee:	687b      	ldr	r3, [r7, #4]
 80031f0:	683a      	ldr	r2, [r7, #0]
 80031f2:	635a      	str	r2, [r3, #52]	; 0x34
 80031f4:	bf00      	nop
 80031f6:	370c      	adds	r7, #12
 80031f8:	46bd      	mov	sp, r7
 80031fa:	f85d 7b04 	ldr.w	r7, [sp], #4
 80031fe:	4770      	bx	lr

08003200 <LL_TIM_OC_SetCompareCH2>:
 8003200:	b480      	push	{r7}
 8003202:	b083      	sub	sp, #12
 8003204:	af00      	add	r7, sp, #0
 8003206:	6078      	str	r0, [r7, #4]
 8003208:	6039      	str	r1, [r7, #0]
 800320a:	687b      	ldr	r3, [r7, #4]
 800320c:	683a      	ldr	r2, [r7, #0]
 800320e:	639a      	str	r2, [r3, #56]	; 0x38
 8003210:	bf00      	nop
 8003212:	370c      	adds	r7, #12
 8003214:	46bd      	mov	sp, r7
 8003216:	f85d 7b04 	ldr.w	r7, [sp], #4
 800321a:	4770      	bx	lr

0800321c <LL_TIM_OC_SetCompareCH3>:
 800321c:	b480      	push	{r7}
 800321e:	b083      	sub	sp, #12
 8003220:	af00      	add	r7, sp, #0
 8003222:	6078      	str	r0, [r7, #4]
 8003224:	6039      	str	r1, [r7, #0]
 8003226:	687b      	ldr	r3, [r7, #4]
 8003228:	683a      	ldr	r2, [r7, #0]
 800322a:	63da      	str	r2, [r3, #60]	; 0x3c
 800322c:	bf00      	nop
 800322e:	370c      	adds	r7, #12
 8003230:	46bd      	mov	sp, r7
 8003232:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003236:	4770      	bx	lr

08003238 <LL_TIM_ClearFlag_UPDATE>:
 8003238:	b480      	push	{r7}
 800323a:	b083      	sub	sp, #12
 800323c:	af00      	add	r7, sp, #0
 800323e:	6078      	str	r0, [r7, #4]
 8003240:	687b      	ldr	r3, [r7, #4]
 8003242:	f06f 0201 	mvn.w	r2, #1
 8003246:	611a      	str	r2, [r3, #16]
 8003248:	bf00      	nop
 800324a:	370c      	adds	r7, #12
 800324c:	46bd      	mov	sp, r7
 800324e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003252:	4770      	bx	lr

08003254 <LL_TIM_IsActiveFlag_UPDATE>:
 8003254:	b480      	push	{r7}
 8003256:	b083      	sub	sp, #12
 8003258:	af00      	add	r7, sp, #0
 800325a:	6078      	str	r0, [r7, #4]
 800325c:	687b      	ldr	r3, [r7, #4]
 800325e:	691b      	ldr	r3, [r3, #16]
 8003260:	f003 0301 	and.w	r3, r3, #1
 8003264:	2b01      	cmp	r3, #1
 8003266:	bf0c      	ite	eq
 8003268:	2301      	moveq	r3, #1
 800326a:	2300      	movne	r3, #0
 800326c:	b2db      	uxtb	r3, r3
 800326e:	4618      	mov	r0, r3
 8003270:	370c      	adds	r7, #12
 8003272:	46bd      	mov	sp, r7
 8003274:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003278:	4770      	bx	lr

0800327a <LL_TIM_EnableIT_UPDATE>:
 800327a:	b480      	push	{r7}
 800327c:	b083      	sub	sp, #12
 800327e:	af00      	add	r7, sp, #0
 8003280:	6078      	str	r0, [r7, #4]
 8003282:	687b      	ldr	r3, [r7, #4]
 8003284:	68db      	ldr	r3, [r3, #12]
 8003286:	f043 0201 	orr.w	r2, r3, #1
 800328a:	687b      	ldr	r3, [r7, #4]
 800328c:	60da      	str	r2, [r3, #12]
 800328e:	bf00      	nop
 8003290:	370c      	adds	r7, #12
 8003292:	46bd      	mov	sp, r7
 8003294:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003298:	4770      	bx	lr

0800329a <LL_TIM_GenerateEvent_UPDATE>:
 800329a:	b480      	push	{r7}
 800329c:	b083      	sub	sp, #12
 800329e:	af00      	add	r7, sp, #0
 80032a0:	6078      	str	r0, [r7, #4]
 80032a2:	687b      	ldr	r3, [r7, #4]
 80032a4:	695b      	ldr	r3, [r3, #20]
 80032a6:	f043 0201 	orr.w	r2, r3, #1
 80032aa:	687b      	ldr	r3, [r7, #4]
 80032ac:	615a      	str	r2, [r3, #20]
 80032ae:	bf00      	nop
 80032b0:	370c      	adds	r7, #12
 80032b2:	46bd      	mov	sp, r7
 80032b4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80032b8:	4770      	bx	lr
	...

080032bc <new_target>:
 80032bc:	b480      	push	{r7}
 80032be:	b085      	sub	sp, #20
 80032c0:	af00      	add	r7, sp, #0
 80032c2:	6078      	str	r0, [r7, #4]
 80032c4:	2300      	movs	r3, #0
 80032c6:	60fb      	str	r3, [r7, #12]
 80032c8:	e00e      	b.n	80032e8 <new_target+0x2c>
 80032ca:	4b0c      	ldr	r3, [pc, #48]	; (80032fc <new_target+0x40>)
 80032cc:	6819      	ldr	r1, [r3, #0]
 80032ce:	68fb      	ldr	r3, [r7, #12]
 80032d0:	009b      	lsls	r3, r3, #2
 80032d2:	687a      	ldr	r2, [r7, #4]
 80032d4:	4413      	add	r3, r2
 80032d6:	681a      	ldr	r2, [r3, #0]
 80032d8:	68fb      	ldr	r3, [r7, #12]
 80032da:	330e      	adds	r3, #14
 80032dc:	009b      	lsls	r3, r3, #2
 80032de:	440b      	add	r3, r1
 80032e0:	601a      	str	r2, [r3, #0]
 80032e2:	68fb      	ldr	r3, [r7, #12]
 80032e4:	3301      	adds	r3, #1
 80032e6:	60fb      	str	r3, [r7, #12]
 80032e8:	68fb      	ldr	r3, [r7, #12]
 80032ea:	2b02      	cmp	r3, #2
 80032ec:	dded      	ble.n	80032ca <new_target+0xe>
 80032ee:	bf00      	nop
 80032f0:	3714      	adds	r7, #20
 80032f2:	46bd      	mov	sp, r7
 80032f4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80032f8:	4770      	bx	lr
 80032fa:	bf00      	nop
 80032fc:	20000910 	.word	0x20000910

08003300 <mk_speed>:
 8003300:	b580      	push	{r7, lr}
 8003302:	b082      	sub	sp, #8
 8003304:	af00      	add	r7, sp, #0
 8003306:	6078      	str	r0, [r7, #4]
 8003308:	687b      	ldr	r3, [r7, #4]
 800330a:	68db      	ldr	r3, [r3, #12]
 800330c:	4a21      	ldr	r2, [pc, #132]	; (8003394 <mk_speed+0x94>)
 800330e:	6013      	str	r3, [r2, #0]
 8003310:	687b      	ldr	r3, [r7, #4]
 8003312:	691b      	ldr	r3, [r3, #16]
 8003314:	4a1f      	ldr	r2, [pc, #124]	; (8003394 <mk_speed+0x94>)
 8003316:	6053      	str	r3, [r2, #4]
 8003318:	687b      	ldr	r3, [r7, #4]
 800331a:	695b      	ldr	r3, [r3, #20]
 800331c:	4a1d      	ldr	r2, [pc, #116]	; (8003394 <mk_speed+0x94>)
 800331e:	6093      	str	r3, [r2, #8]
 8003320:	4b1d      	ldr	r3, [pc, #116]	; (8003398 <mk_speed+0x98>)
 8003322:	2203      	movs	r2, #3
 8003324:	2103      	movs	r1, #3
 8003326:	481d      	ldr	r0, [pc, #116]	; (800339c <mk_speed+0x9c>)
 8003328:	f006 f8ec 	bl	8009504 <arm_mat_init_f32>
 800332c:	4b1c      	ldr	r3, [pc, #112]	; (80033a0 <mk_speed+0xa0>)
 800332e:	2203      	movs	r2, #3
 8003330:	2103      	movs	r1, #3
 8003332:	481c      	ldr	r0, [pc, #112]	; (80033a4 <mk_speed+0xa4>)
 8003334:	f006 f8e6 	bl	8009504 <arm_mat_init_f32>
 8003338:	4b1b      	ldr	r3, [pc, #108]	; (80033a8 <mk_speed+0xa8>)
 800333a:	2201      	movs	r2, #1
 800333c:	2103      	movs	r1, #3
 800333e:	481b      	ldr	r0, [pc, #108]	; (80033ac <mk_speed+0xac>)
 8003340:	f006 f8e0 	bl	8009504 <arm_mat_init_f32>
 8003344:	4b1a      	ldr	r3, [pc, #104]	; (80033b0 <mk_speed+0xb0>)
 8003346:	2201      	movs	r2, #1
 8003348:	2103      	movs	r1, #3
 800334a:	481a      	ldr	r0, [pc, #104]	; (80033b4 <mk_speed+0xb4>)
 800334c:	f006 f8da 	bl	8009504 <arm_mat_init_f32>
 8003350:	4b19      	ldr	r3, [pc, #100]	; (80033b8 <mk_speed+0xb8>)
 8003352:	2201      	movs	r2, #1
 8003354:	2103      	movs	r1, #3
 8003356:	4819      	ldr	r0, [pc, #100]	; (80033bc <mk_speed+0xbc>)
 8003358:	f006 f8d4 	bl	8009504 <arm_mat_init_f32>
 800335c:	4b0d      	ldr	r3, [pc, #52]	; (8003394 <mk_speed+0x94>)
 800335e:	2201      	movs	r2, #1
 8003360:	2103      	movs	r1, #3
 8003362:	4817      	ldr	r0, [pc, #92]	; (80033c0 <mk_speed+0xc0>)
 8003364:	f006 f8ce 	bl	8009504 <arm_mat_init_f32>
 8003368:	4a10      	ldr	r2, [pc, #64]	; (80033ac <mk_speed+0xac>)
 800336a:	4915      	ldr	r1, [pc, #84]	; (80033c0 <mk_speed+0xc0>)
 800336c:	480b      	ldr	r0, [pc, #44]	; (800339c <mk_speed+0x9c>)
 800336e:	f006 f978 	bl	8009662 <arm_mat_mult_f32>
 8003372:	4a10      	ldr	r2, [pc, #64]	; (80033b4 <mk_speed+0xb4>)
 8003374:	4912      	ldr	r1, [pc, #72]	; (80033c0 <mk_speed+0xc0>)
 8003376:	480b      	ldr	r0, [pc, #44]	; (80033a4 <mk_speed+0xa4>)
 8003378:	f006 f973 	bl	8009662 <arm_mat_mult_f32>
 800337c:	4a0f      	ldr	r2, [pc, #60]	; (80033bc <mk_speed+0xbc>)
 800337e:	490d      	ldr	r1, [pc, #52]	; (80033b4 <mk_speed+0xb4>)
 8003380:	480a      	ldr	r0, [pc, #40]	; (80033ac <mk_speed+0xac>)
 8003382:	f006 f8d7 	bl	8009534 <arm_mat_add_f32>
 8003386:	480c      	ldr	r0, [pc, #48]	; (80033b8 <mk_speed+0xb8>)
 8003388:	f7ff ff98 	bl	80032bc <new_target>
 800338c:	bf00      	nop
 800338e:	3708      	adds	r7, #8
 8003390:	46bd      	mov	sp, r7
 8003392:	bd80      	pop	{r7, pc}
 8003394:	20000914 	.word	0x20000914
 8003398:	20000840 	.word	0x20000840
 800339c:	20000920 	.word	0x20000920
 80033a0:	20000864 	.word	0x20000864
 80033a4:	20000928 	.word	0x20000928
 80033a8:	20000938 	.word	0x20000938
 80033ac:	20000930 	.word	0x20000930
 80033b0:	2000094c 	.word	0x2000094c
 80033b4:	20000944 	.word	0x20000944
 80033b8:	20000960 	.word	0x20000960
 80033bc:	20000958 	.word	0x20000958
 80033c0:	2000096c 	.word	0x2000096c

080033c4 <pid_hw_config>:
 80033c4:	b580      	push	{r7, lr}
 80033c6:	af00      	add	r7, sp, #0
 80033c8:	2020      	movs	r0, #32
 80033ca:	f7ff fd8f 	bl	8002eec <LL_APB1_GRP1_EnableClock>
 80033ce:	f24a 410f 	movw	r1, #41999	; 0xa40f
 80033d2:	480d      	ldr	r0, [pc, #52]	; (8003408 <pid_hw_config+0x44>)
 80033d4:	f7ff fe19 	bl	800300a <LL_TIM_SetAutoReload>
 80033d8:	2127      	movs	r1, #39	; 0x27
 80033da:	480b      	ldr	r0, [pc, #44]	; (8003408 <pid_hw_config+0x44>)
 80033dc:	f7ff fe07 	bl	8002fee <LL_TIM_SetPrescaler>
 80033e0:	2100      	movs	r1, #0
 80033e2:	4809      	ldr	r0, [pc, #36]	; (8003408 <pid_hw_config+0x44>)
 80033e4:	f7ff fdcd 	bl	8002f82 <LL_TIM_SetCounterMode>
 80033e8:	4807      	ldr	r0, [pc, #28]	; (8003408 <pid_hw_config+0x44>)
 80033ea:	f7ff ff46 	bl	800327a <LL_TIM_EnableIT_UPDATE>
 80033ee:	2107      	movs	r1, #7
 80033f0:	2037      	movs	r0, #55	; 0x37
 80033f2:	f7ff fc4d 	bl	8002c90 <NVIC_SetPriority>
 80033f6:	2037      	movs	r0, #55	; 0x37
 80033f8:	f7ff fc30 	bl	8002c5c <NVIC_EnableIRQ>
 80033fc:	4802      	ldr	r0, [pc, #8]	; (8003408 <pid_hw_config+0x44>)
 80033fe:	f7ff fd8d 	bl	8002f1c <LL_TIM_EnableCounter>
 8003402:	bf00      	nop
 8003404:	bd80      	pop	{r7, pc}
 8003406:	bf00      	nop
 8003408:	40001400 	.word	0x40001400

0800340c <pwm_set>:
 800340c:	b580      	push	{r7, lr}
 800340e:	b082      	sub	sp, #8
 8003410:	af00      	add	r7, sp, #0
 8003412:	6078      	str	r0, [r7, #4]
 8003414:	687b      	ldr	r3, [r7, #4]
 8003416:	edd3 7a00 	vldr	s15, [r3]
 800341a:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 800341e:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8003422:	db12      	blt.n	800344a <pwm_set+0x3e>
 8003424:	2101      	movs	r1, #1
 8003426:	4847      	ldr	r0, [pc, #284]	; (8003544 <pwm_set+0x138>)
 8003428:	f7ff fd2a 	bl	8002e80 <LL_GPIO_SetOutputPin>
 800342c:	2102      	movs	r1, #2
 800342e:	4845      	ldr	r0, [pc, #276]	; (8003544 <pwm_set+0x138>)
 8003430:	f7ff fd34 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003434:	687b      	ldr	r3, [r7, #4]
 8003436:	edd3 7a00 	vldr	s15, [r3]
 800343a:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 800343e:	ee17 1a90 	vmov	r1, s15
 8003442:	4841      	ldr	r0, [pc, #260]	; (8003548 <pwm_set+0x13c>)
 8003444:	f7ff fece 	bl	80031e4 <LL_TIM_OC_SetCompareCH1>
 8003448:	e013      	b.n	8003472 <pwm_set+0x66>
 800344a:	2101      	movs	r1, #1
 800344c:	483d      	ldr	r0, [pc, #244]	; (8003544 <pwm_set+0x138>)
 800344e:	f7ff fd25 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003452:	2102      	movs	r1, #2
 8003454:	483b      	ldr	r0, [pc, #236]	; (8003544 <pwm_set+0x138>)
 8003456:	f7ff fd13 	bl	8002e80 <LL_GPIO_SetOutputPin>
 800345a:	687b      	ldr	r3, [r7, #4]
 800345c:	edd3 7a00 	vldr	s15, [r3]
 8003460:	eef1 7a67 	vneg.f32	s15, s15
 8003464:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 8003468:	ee17 1a90 	vmov	r1, s15
 800346c:	4836      	ldr	r0, [pc, #216]	; (8003548 <pwm_set+0x13c>)
 800346e:	f7ff feb9 	bl	80031e4 <LL_TIM_OC_SetCompareCH1>
 8003472:	687b      	ldr	r3, [r7, #4]
 8003474:	3304      	adds	r3, #4
 8003476:	edd3 7a00 	vldr	s15, [r3]
 800347a:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 800347e:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8003482:	db13      	blt.n	80034ac <pwm_set+0xa0>
 8003484:	2104      	movs	r1, #4
 8003486:	482f      	ldr	r0, [pc, #188]	; (8003544 <pwm_set+0x138>)
 8003488:	f7ff fcfa 	bl	8002e80 <LL_GPIO_SetOutputPin>
 800348c:	2108      	movs	r1, #8
 800348e:	482d      	ldr	r0, [pc, #180]	; (8003544 <pwm_set+0x138>)
 8003490:	f7ff fd04 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003494:	687b      	ldr	r3, [r7, #4]
 8003496:	3304      	adds	r3, #4
 8003498:	edd3 7a00 	vldr	s15, [r3]
 800349c:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 80034a0:	ee17 1a90 	vmov	r1, s15
 80034a4:	4828      	ldr	r0, [pc, #160]	; (8003548 <pwm_set+0x13c>)
 80034a6:	f7ff feab 	bl	8003200 <LL_TIM_OC_SetCompareCH2>
 80034aa:	e014      	b.n	80034d6 <pwm_set+0xca>
 80034ac:	2104      	movs	r1, #4
 80034ae:	4825      	ldr	r0, [pc, #148]	; (8003544 <pwm_set+0x138>)
 80034b0:	f7ff fcf4 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 80034b4:	2108      	movs	r1, #8
 80034b6:	4823      	ldr	r0, [pc, #140]	; (8003544 <pwm_set+0x138>)
 80034b8:	f7ff fce2 	bl	8002e80 <LL_GPIO_SetOutputPin>
 80034bc:	687b      	ldr	r3, [r7, #4]
 80034be:	3304      	adds	r3, #4
 80034c0:	edd3 7a00 	vldr	s15, [r3]
 80034c4:	eef1 7a67 	vneg.f32	s15, s15
 80034c8:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 80034cc:	ee17 1a90 	vmov	r1, s15
 80034d0:	481d      	ldr	r0, [pc, #116]	; (8003548 <pwm_set+0x13c>)
 80034d2:	f7ff fe95 	bl	8003200 <LL_TIM_OC_SetCompareCH2>
 80034d6:	687b      	ldr	r3, [r7, #4]
 80034d8:	3308      	adds	r3, #8
 80034da:	edd3 7a00 	vldr	s15, [r3]
 80034de:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 80034e2:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80034e6:	db13      	blt.n	8003510 <pwm_set+0x104>
 80034e8:	2110      	movs	r1, #16
 80034ea:	4816      	ldr	r0, [pc, #88]	; (8003544 <pwm_set+0x138>)
 80034ec:	f7ff fcc8 	bl	8002e80 <LL_GPIO_SetOutputPin>
 80034f0:	2120      	movs	r1, #32
 80034f2:	4814      	ldr	r0, [pc, #80]	; (8003544 <pwm_set+0x138>)
 80034f4:	f7ff fcd2 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 80034f8:	687b      	ldr	r3, [r7, #4]
 80034fa:	3308      	adds	r3, #8
 80034fc:	edd3 7a00 	vldr	s15, [r3]
 8003500:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 8003504:	ee17 1a90 	vmov	r1, s15
 8003508:	480f      	ldr	r0, [pc, #60]	; (8003548 <pwm_set+0x13c>)
 800350a:	f7ff fe87 	bl	800321c <LL_TIM_OC_SetCompareCH3>
 800350e:	e015      	b.n	800353c <pwm_set+0x130>
 8003510:	2110      	movs	r1, #16
 8003512:	480c      	ldr	r0, [pc, #48]	; (8003544 <pwm_set+0x138>)
 8003514:	f7ff fcc2 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003518:	2120      	movs	r1, #32
 800351a:	480a      	ldr	r0, [pc, #40]	; (8003544 <pwm_set+0x138>)
 800351c:	f7ff fcb0 	bl	8002e80 <LL_GPIO_SetOutputPin>
 8003520:	687b      	ldr	r3, [r7, #4]
 8003522:	3308      	adds	r3, #8
 8003524:	edd3 7a00 	vldr	s15, [r3]
 8003528:	eef1 7a67 	vneg.f32	s15, s15
 800352c:	eefc 7ae7 	vcvt.u32.f32	s15, s15
 8003530:	ee17 1a90 	vmov	r1, s15
 8003534:	4804      	ldr	r0, [pc, #16]	; (8003548 <pwm_set+0x13c>)
 8003536:	f7ff fe71 	bl	800321c <LL_TIM_OC_SetCompareCH3>
 800353a:	bf00      	nop
 800353c:	3708      	adds	r7, #8
 800353e:	46bd      	mov	sp, r7
 8003540:	bd80      	pop	{r7, pc}
 8003542:	bf00      	nop
 8003544:	40020800 	.word	0x40020800
 8003548:	40000400 	.word	0x40000400

0800354c <wheel_stop>:
 800354c:	b580      	push	{r7, lr}
 800354e:	b086      	sub	sp, #24
 8003550:	af00      	add	r7, sp, #0
 8003552:	4603      	mov	r3, r0
 8003554:	71fb      	strb	r3, [r7, #7]
 8003556:	79fb      	ldrb	r3, [r7, #7]
 8003558:	009b      	lsls	r3, r3, #2
 800355a:	f107 0218 	add.w	r2, r7, #24
 800355e:	4413      	add	r3, r2
 8003560:	3b0c      	subs	r3, #12
 8003562:	f04f 0200 	mov.w	r2, #0
 8003566:	601a      	str	r2, [r3, #0]
 8003568:	f107 030c 	add.w	r3, r7, #12
 800356c:	4618      	mov	r0, r3
 800356e:	f7ff ff4d 	bl	800340c <pwm_set>
 8003572:	bf00      	nop
 8003574:	3718      	adds	r7, #24
 8003576:	46bd      	mov	sp, r7
 8003578:	bd80      	pop	{r7, pc}
	...

0800357c <pid_calc>:
 800357c:	b580      	push	{r7, lr}
 800357e:	b08a      	sub	sp, #40	; 0x28
 8003580:	af00      	add	r7, sp, #0
 8003582:	6078      	str	r0, [r7, #4]
 8003584:	6039      	str	r1, [r7, #0]
 8003586:	2300      	movs	r3, #0
 8003588:	627b      	str	r3, [r7, #36]	; 0x24
 800358a:	e14c      	b.n	8003826 <pid_calc+0x2aa>
 800358c:	6a78      	ldr	r0, [r7, #36]	; 0x24
 800358e:	f002 ffed 	bl	800656c <pid_current_speed>
 8003592:	eef0 7a40 	vmov.f32	s15, s0
 8003596:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003598:	009b      	lsls	r3, r3, #2
 800359a:	f107 0228 	add.w	r2, r7, #40	; 0x28
 800359e:	4413      	add	r3, r2
 80035a0:	3b1c      	subs	r3, #28
 80035a2:	edc3 7a00 	vstr	s15, [r3]
 80035a6:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80035a8:	009b      	lsls	r3, r3, #2
 80035aa:	687a      	ldr	r2, [r7, #4]
 80035ac:	4413      	add	r3, r2
 80035ae:	edd3 7a00 	vldr	s15, [r3]
 80035b2:	eef5 7a40 	vcmp.f32	s15, #0.0
 80035b6:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80035ba:	d11f      	bne.n	80035fc <pid_calc+0x80>
 80035bc:	4ba1      	ldr	r3, [pc, #644]	; (8003844 <pid_calc+0x2c8>)
 80035be:	681a      	ldr	r2, [r3, #0]
 80035c0:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80035c2:	3308      	adds	r3, #8
 80035c4:	009b      	lsls	r3, r3, #2
 80035c6:	4413      	add	r3, r2
 80035c8:	f04f 0200 	mov.w	r2, #0
 80035cc:	601a      	str	r2, [r3, #0]
 80035ce:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80035d0:	b2db      	uxtb	r3, r3
 80035d2:	4618      	mov	r0, r3
 80035d4:	f7ff ffba 	bl	800354c <wheel_stop>
 80035d8:	683a      	ldr	r2, [r7, #0]
 80035da:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80035dc:	3308      	adds	r3, #8
 80035de:	009b      	lsls	r3, r3, #2
 80035e0:	4413      	add	r3, r2
 80035e2:	f04f 0200 	mov.w	r2, #0
 80035e6:	601a      	str	r2, [r3, #0]
 80035e8:	683a      	ldr	r2, [r7, #0]
 80035ea:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80035ec:	330a      	adds	r3, #10
 80035ee:	009b      	lsls	r3, r3, #2
 80035f0:	4413      	add	r3, r2
 80035f2:	3304      	adds	r3, #4
 80035f4:	f04f 0200 	mov.w	r2, #0
 80035f8:	601a      	str	r2, [r3, #0]
 80035fa:	e111      	b.n	8003820 <pid_calc+0x2a4>
 80035fc:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80035fe:	009b      	lsls	r3, r3, #2
 8003600:	687a      	ldr	r2, [r7, #4]
 8003602:	4413      	add	r3, r2
 8003604:	ed93 7a00 	vldr	s14, [r3]
 8003608:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800360a:	009b      	lsls	r3, r3, #2
 800360c:	f107 0228 	add.w	r2, r7, #40	; 0x28
 8003610:	4413      	add	r3, r2
 8003612:	3b1c      	subs	r3, #28
 8003614:	edd3 7a00 	vldr	s15, [r3]
 8003618:	ee77 7a67 	vsub.f32	s15, s14, s15
 800361c:	edc7 7a08 	vstr	s15, [r7, #32]
 8003620:	683a      	ldr	r2, [r7, #0]
 8003622:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003624:	3308      	adds	r3, #8
 8003626:	009b      	lsls	r3, r3, #2
 8003628:	4413      	add	r3, r2
 800362a:	edd3 7a00 	vldr	s15, [r3]
 800362e:	ed97 7a08 	vldr	s14, [r7, #32]
 8003632:	ee77 7a67 	vsub.f32	s15, s14, s15
 8003636:	edc7 7a07 	vstr	s15, [r7, #28]
 800363a:	683a      	ldr	r2, [r7, #0]
 800363c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800363e:	3308      	adds	r3, #8
 8003640:	009b      	lsls	r3, r3, #2
 8003642:	4413      	add	r3, r2
 8003644:	6a3a      	ldr	r2, [r7, #32]
 8003646:	601a      	str	r2, [r3, #0]
 8003648:	683a      	ldr	r2, [r7, #0]
 800364a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800364c:	330a      	adds	r3, #10
 800364e:	009b      	lsls	r3, r3, #2
 8003650:	4413      	add	r3, r2
 8003652:	3304      	adds	r3, #4
 8003654:	ed93 7a00 	vldr	s14, [r3]
 8003658:	edd7 7a08 	vldr	s15, [r7, #32]
 800365c:	ee77 7a27 	vadd.f32	s15, s14, s15
 8003660:	683a      	ldr	r2, [r7, #0]
 8003662:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003664:	330a      	adds	r3, #10
 8003666:	009b      	lsls	r3, r3, #2
 8003668:	4413      	add	r3, r2
 800366a:	3304      	adds	r3, #4
 800366c:	edc3 7a00 	vstr	s15, [r3]
 8003670:	683a      	ldr	r2, [r7, #0]
 8003672:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003674:	330a      	adds	r3, #10
 8003676:	009b      	lsls	r3, r3, #2
 8003678:	4413      	add	r3, r2
 800367a:	3304      	adds	r3, #4
 800367c:	ed93 7a00 	vldr	s14, [r3]
 8003680:	683b      	ldr	r3, [r7, #0]
 8003682:	edd3 7a05 	vldr	s15, [r3, #20]
 8003686:	eeb4 7ae7 	vcmpe.f32	s14, s15
 800368a:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800368e:	dd08      	ble.n	80036a2 <pid_calc+0x126>
 8003690:	683b      	ldr	r3, [r7, #0]
 8003692:	695a      	ldr	r2, [r3, #20]
 8003694:	6839      	ldr	r1, [r7, #0]
 8003696:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003698:	330a      	adds	r3, #10
 800369a:	009b      	lsls	r3, r3, #2
 800369c:	440b      	add	r3, r1
 800369e:	3304      	adds	r3, #4
 80036a0:	601a      	str	r2, [r3, #0]
 80036a2:	683a      	ldr	r2, [r7, #0]
 80036a4:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80036a6:	330a      	adds	r3, #10
 80036a8:	009b      	lsls	r3, r3, #2
 80036aa:	4413      	add	r3, r2
 80036ac:	3304      	adds	r3, #4
 80036ae:	ed93 7a00 	vldr	s14, [r3]
 80036b2:	683b      	ldr	r3, [r7, #0]
 80036b4:	edd3 7a05 	vldr	s15, [r3, #20]
 80036b8:	eef1 7a67 	vneg.f32	s15, s15
 80036bc:	eeb4 7ae7 	vcmpe.f32	s14, s15
 80036c0:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80036c4:	d50c      	bpl.n	80036e0 <pid_calc+0x164>
 80036c6:	683b      	ldr	r3, [r7, #0]
 80036c8:	edd3 7a05 	vldr	s15, [r3, #20]
 80036cc:	eef1 7a67 	vneg.f32	s15, s15
 80036d0:	683a      	ldr	r2, [r7, #0]
 80036d2:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80036d4:	330a      	adds	r3, #10
 80036d6:	009b      	lsls	r3, r3, #2
 80036d8:	4413      	add	r3, r2
 80036da:	3304      	adds	r3, #4
 80036dc:	edc3 7a00 	vstr	s15, [r3]
 80036e0:	683b      	ldr	r3, [r7, #0]
 80036e2:	ed93 7a03 	vldr	s14, [r3, #12]
 80036e6:	683a      	ldr	r2, [r7, #0]
 80036e8:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80036ea:	330a      	adds	r3, #10
 80036ec:	009b      	lsls	r3, r3, #2
 80036ee:	4413      	add	r3, r2
 80036f0:	3304      	adds	r3, #4
 80036f2:	edd3 7a00 	vldr	s15, [r3]
 80036f6:	ee67 7a27 	vmul.f32	s15, s14, s15
 80036fa:	edc7 7a06 	vstr	s15, [r7, #24]
 80036fe:	4b51      	ldr	r3, [pc, #324]	; (8003844 <pid_calc+0x2c8>)
 8003700:	681a      	ldr	r2, [r3, #0]
 8003702:	683b      	ldr	r3, [r7, #0]
 8003704:	ed93 7a02 	vldr	s14, [r3, #8]
 8003708:	edd7 7a08 	vldr	s15, [r7, #32]
 800370c:	ee27 7a27 	vmul.f32	s14, s14, s15
 8003710:	edd7 7a06 	vldr	s15, [r7, #24]
 8003714:	ee37 7a27 	vadd.f32	s14, s14, s15
 8003718:	683b      	ldr	r3, [r7, #0]
 800371a:	edd3 6a04 	vldr	s13, [r3, #16]
 800371e:	edd7 7a07 	vldr	s15, [r7, #28]
 8003722:	ee66 7aa7 	vmul.f32	s15, s13, s15
 8003726:	ee77 7a27 	vadd.f32	s15, s14, s15
 800372a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800372c:	3308      	adds	r3, #8
 800372e:	009b      	lsls	r3, r3, #2
 8003730:	4413      	add	r3, r2
 8003732:	edc3 7a00 	vstr	s15, [r3]
 8003736:	4b43      	ldr	r3, [pc, #268]	; (8003844 <pid_calc+0x2c8>)
 8003738:	681a      	ldr	r2, [r3, #0]
 800373a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800373c:	3308      	adds	r3, #8
 800373e:	009b      	lsls	r3, r3, #2
 8003740:	4413      	add	r3, r2
 8003742:	ed93 7a00 	vldr	s14, [r3]
 8003746:	683b      	ldr	r3, [r7, #0]
 8003748:	edd3 7a06 	vldr	s15, [r3, #24]
 800374c:	eeb4 7ae7 	vcmpe.f32	s14, s15
 8003750:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8003754:	dd09      	ble.n	800376a <pid_calc+0x1ee>
 8003756:	4b3b      	ldr	r3, [pc, #236]	; (8003844 <pid_calc+0x2c8>)
 8003758:	6819      	ldr	r1, [r3, #0]
 800375a:	683b      	ldr	r3, [r7, #0]
 800375c:	699a      	ldr	r2, [r3, #24]
 800375e:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003760:	3308      	adds	r3, #8
 8003762:	009b      	lsls	r3, r3, #2
 8003764:	440b      	add	r3, r1
 8003766:	601a      	str	r2, [r3, #0]
 8003768:	e01e      	b.n	80037a8 <pid_calc+0x22c>
 800376a:	4b36      	ldr	r3, [pc, #216]	; (8003844 <pid_calc+0x2c8>)
 800376c:	681a      	ldr	r2, [r3, #0]
 800376e:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003770:	3308      	adds	r3, #8
 8003772:	009b      	lsls	r3, r3, #2
 8003774:	4413      	add	r3, r2
 8003776:	ed93 7a00 	vldr	s14, [r3]
 800377a:	683b      	ldr	r3, [r7, #0]
 800377c:	edd3 7a06 	vldr	s15, [r3, #24]
 8003780:	eef1 7a67 	vneg.f32	s15, s15
 8003784:	eeb4 7ae7 	vcmpe.f32	s14, s15
 8003788:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800378c:	d50c      	bpl.n	80037a8 <pid_calc+0x22c>
 800378e:	4b2d      	ldr	r3, [pc, #180]	; (8003844 <pid_calc+0x2c8>)
 8003790:	681a      	ldr	r2, [r3, #0]
 8003792:	683b      	ldr	r3, [r7, #0]
 8003794:	edd3 7a06 	vldr	s15, [r3, #24]
 8003798:	eef1 7a67 	vneg.f32	s15, s15
 800379c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 800379e:	3308      	adds	r3, #8
 80037a0:	009b      	lsls	r3, r3, #2
 80037a2:	4413      	add	r3, r2
 80037a4:	edc3 7a00 	vstr	s15, [r3]
 80037a8:	4b26      	ldr	r3, [pc, #152]	; (8003844 <pid_calc+0x2c8>)
 80037aa:	681a      	ldr	r2, [r3, #0]
 80037ac:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80037ae:	3308      	adds	r3, #8
 80037b0:	009b      	lsls	r3, r3, #2
 80037b2:	4413      	add	r3, r2
 80037b4:	ed93 7a00 	vldr	s14, [r3]
 80037b8:	683b      	ldr	r3, [r7, #0]
 80037ba:	edd3 7a07 	vldr	s15, [r3, #28]
 80037be:	eeb4 7ae7 	vcmpe.f32	s14, s15
 80037c2:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80037c6:	d52b      	bpl.n	8003820 <pid_calc+0x2a4>
 80037c8:	4b1e      	ldr	r3, [pc, #120]	; (8003844 <pid_calc+0x2c8>)
 80037ca:	681a      	ldr	r2, [r3, #0]
 80037cc:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80037ce:	3308      	adds	r3, #8
 80037d0:	009b      	lsls	r3, r3, #2
 80037d2:	4413      	add	r3, r2
 80037d4:	ed93 7a00 	vldr	s14, [r3]
 80037d8:	683b      	ldr	r3, [r7, #0]
 80037da:	edd3 7a07 	vldr	s15, [r3, #28]
 80037de:	eef1 7a67 	vneg.f32	s15, s15
 80037e2:	eeb4 7ae7 	vcmpe.f32	s14, s15
 80037e6:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80037ea:	dd19      	ble.n	8003820 <pid_calc+0x2a4>
 80037ec:	4b15      	ldr	r3, [pc, #84]	; (8003844 <pid_calc+0x2c8>)
 80037ee:	681a      	ldr	r2, [r3, #0]
 80037f0:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80037f2:	3308      	adds	r3, #8
 80037f4:	009b      	lsls	r3, r3, #2
 80037f6:	4413      	add	r3, r2
 80037f8:	f04f 0200 	mov.w	r2, #0
 80037fc:	601a      	str	r2, [r3, #0]
 80037fe:	683a      	ldr	r2, [r7, #0]
 8003800:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003802:	3308      	adds	r3, #8
 8003804:	009b      	lsls	r3, r3, #2
 8003806:	4413      	add	r3, r2
 8003808:	f04f 0200 	mov.w	r2, #0
 800380c:	601a      	str	r2, [r3, #0]
 800380e:	683a      	ldr	r2, [r7, #0]
 8003810:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003812:	330a      	adds	r3, #10
 8003814:	009b      	lsls	r3, r3, #2
 8003816:	4413      	add	r3, r2
 8003818:	3304      	adds	r3, #4
 800381a:	f04f 0200 	mov.w	r2, #0
 800381e:	601a      	str	r2, [r3, #0]
 8003820:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003822:	3301      	adds	r3, #1
 8003824:	627b      	str	r3, [r7, #36]	; 0x24
 8003826:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8003828:	2b02      	cmp	r3, #2
 800382a:	f77f aeaf 	ble.w	800358c <pid_calc+0x10>
 800382e:	4b05      	ldr	r3, [pc, #20]	; (8003844 <pid_calc+0x2c8>)
 8003830:	681b      	ldr	r3, [r3, #0]
 8003832:	3320      	adds	r3, #32
 8003834:	4618      	mov	r0, r3
 8003836:	f7ff fde9 	bl	800340c <pwm_set>
 800383a:	bf00      	nop
 800383c:	3728      	adds	r7, #40	; 0x28
 800383e:	46bd      	mov	sp, r7
 8003840:	bd80      	pop	{r7, pc}
 8003842:	bf00      	nop
 8003844:	2000090c 	.word	0x2000090c

08003848 <mk_hw_config>:
 8003848:	b580      	push	{r7, lr}
 800384a:	af00      	add	r7, sp, #0
 800384c:	2001      	movs	r0, #1
 800384e:	f7ff fb35 	bl	8002ebc <LL_AHB1_GRP1_EnableClock>
 8003852:	2002      	movs	r0, #2
 8003854:	f7ff fb32 	bl	8002ebc <LL_AHB1_GRP1_EnableClock>
 8003858:	2010      	movs	r0, #16
 800385a:	f7ff fb2f 	bl	8002ebc <LL_AHB1_GRP1_EnableClock>
 800385e:	2004      	movs	r0, #4
 8003860:	f7ff fb2c 	bl	8002ebc <LL_AHB1_GRP1_EnableClock>
 8003864:	2008      	movs	r0, #8
 8003866:	f7ff fb29 	bl	8002ebc <LL_AHB1_GRP1_EnableClock>
 800386a:	2002      	movs	r0, #2
 800386c:	f7ff fb3e 	bl	8002eec <LL_APB1_GRP1_EnableClock>
 8003870:	2201      	movs	r2, #1
 8003872:	2101      	movs	r1, #1
 8003874:	489f      	ldr	r0, [pc, #636]	; (8003af4 <mk_hw_config+0x2ac>)
 8003876:	f7ff fa35 	bl	8002ce4 <LL_GPIO_SetPinMode>
 800387a:	2200      	movs	r2, #0
 800387c:	2101      	movs	r1, #1
 800387e:	489d      	ldr	r0, [pc, #628]	; (8003af4 <mk_hw_config+0x2ac>)
 8003880:	f7ff fa5d 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 8003884:	2200      	movs	r2, #0
 8003886:	2101      	movs	r1, #1
 8003888:	489a      	ldr	r0, [pc, #616]	; (8003af4 <mk_hw_config+0x2ac>)
 800388a:	f7ff fa70 	bl	8002d6e <LL_GPIO_SetPinPull>
 800388e:	2201      	movs	r2, #1
 8003890:	2102      	movs	r1, #2
 8003892:	4898      	ldr	r0, [pc, #608]	; (8003af4 <mk_hw_config+0x2ac>)
 8003894:	f7ff fa26 	bl	8002ce4 <LL_GPIO_SetPinMode>
 8003898:	2200      	movs	r2, #0
 800389a:	2102      	movs	r1, #2
 800389c:	4895      	ldr	r0, [pc, #596]	; (8003af4 <mk_hw_config+0x2ac>)
 800389e:	f7ff fa4e 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 80038a2:	2200      	movs	r2, #0
 80038a4:	2102      	movs	r1, #2
 80038a6:	4893      	ldr	r0, [pc, #588]	; (8003af4 <mk_hw_config+0x2ac>)
 80038a8:	f7ff fa61 	bl	8002d6e <LL_GPIO_SetPinPull>
 80038ac:	2201      	movs	r2, #1
 80038ae:	2104      	movs	r1, #4
 80038b0:	4890      	ldr	r0, [pc, #576]	; (8003af4 <mk_hw_config+0x2ac>)
 80038b2:	f7ff fa17 	bl	8002ce4 <LL_GPIO_SetPinMode>
 80038b6:	2200      	movs	r2, #0
 80038b8:	2104      	movs	r1, #4
 80038ba:	488e      	ldr	r0, [pc, #568]	; (8003af4 <mk_hw_config+0x2ac>)
 80038bc:	f7ff fa3f 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 80038c0:	2200      	movs	r2, #0
 80038c2:	2104      	movs	r1, #4
 80038c4:	488b      	ldr	r0, [pc, #556]	; (8003af4 <mk_hw_config+0x2ac>)
 80038c6:	f7ff fa52 	bl	8002d6e <LL_GPIO_SetPinPull>
 80038ca:	2201      	movs	r2, #1
 80038cc:	2108      	movs	r1, #8
 80038ce:	4889      	ldr	r0, [pc, #548]	; (8003af4 <mk_hw_config+0x2ac>)
 80038d0:	f7ff fa08 	bl	8002ce4 <LL_GPIO_SetPinMode>
 80038d4:	2200      	movs	r2, #0
 80038d6:	2108      	movs	r1, #8
 80038d8:	4886      	ldr	r0, [pc, #536]	; (8003af4 <mk_hw_config+0x2ac>)
 80038da:	f7ff fa30 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 80038de:	2200      	movs	r2, #0
 80038e0:	2108      	movs	r1, #8
 80038e2:	4884      	ldr	r0, [pc, #528]	; (8003af4 <mk_hw_config+0x2ac>)
 80038e4:	f7ff fa43 	bl	8002d6e <LL_GPIO_SetPinPull>
 80038e8:	2201      	movs	r2, #1
 80038ea:	2110      	movs	r1, #16
 80038ec:	4881      	ldr	r0, [pc, #516]	; (8003af4 <mk_hw_config+0x2ac>)
 80038ee:	f7ff f9f9 	bl	8002ce4 <LL_GPIO_SetPinMode>
 80038f2:	2200      	movs	r2, #0
 80038f4:	2110      	movs	r1, #16
 80038f6:	487f      	ldr	r0, [pc, #508]	; (8003af4 <mk_hw_config+0x2ac>)
 80038f8:	f7ff fa21 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 80038fc:	2200      	movs	r2, #0
 80038fe:	2110      	movs	r1, #16
 8003900:	487c      	ldr	r0, [pc, #496]	; (8003af4 <mk_hw_config+0x2ac>)
 8003902:	f7ff fa34 	bl	8002d6e <LL_GPIO_SetPinPull>
 8003906:	2201      	movs	r2, #1
 8003908:	2120      	movs	r1, #32
 800390a:	487a      	ldr	r0, [pc, #488]	; (8003af4 <mk_hw_config+0x2ac>)
 800390c:	f7ff f9ea 	bl	8002ce4 <LL_GPIO_SetPinMode>
 8003910:	2200      	movs	r2, #0
 8003912:	2120      	movs	r1, #32
 8003914:	4877      	ldr	r0, [pc, #476]	; (8003af4 <mk_hw_config+0x2ac>)
 8003916:	f7ff fa12 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 800391a:	2200      	movs	r2, #0
 800391c:	2120      	movs	r1, #32
 800391e:	4875      	ldr	r0, [pc, #468]	; (8003af4 <mk_hw_config+0x2ac>)
 8003920:	f7ff fa25 	bl	8002d6e <LL_GPIO_SetPinPull>
 8003924:	2202      	movs	r2, #2
 8003926:	2140      	movs	r1, #64	; 0x40
 8003928:	4872      	ldr	r0, [pc, #456]	; (8003af4 <mk_hw_config+0x2ac>)
 800392a:	f7ff f9db 	bl	8002ce4 <LL_GPIO_SetPinMode>
 800392e:	2202      	movs	r2, #2
 8003930:	2140      	movs	r1, #64	; 0x40
 8003932:	4870      	ldr	r0, [pc, #448]	; (8003af4 <mk_hw_config+0x2ac>)
 8003934:	f7ff fa48 	bl	8002dc8 <LL_GPIO_SetAFPin_0_7>
 8003938:	2200      	movs	r2, #0
 800393a:	2140      	movs	r1, #64	; 0x40
 800393c:	486d      	ldr	r0, [pc, #436]	; (8003af4 <mk_hw_config+0x2ac>)
 800393e:	f7ff f9fe 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 8003942:	2202      	movs	r2, #2
 8003944:	2180      	movs	r1, #128	; 0x80
 8003946:	486b      	ldr	r0, [pc, #428]	; (8003af4 <mk_hw_config+0x2ac>)
 8003948:	f7ff f9cc 	bl	8002ce4 <LL_GPIO_SetPinMode>
 800394c:	2202      	movs	r2, #2
 800394e:	2180      	movs	r1, #128	; 0x80
 8003950:	4868      	ldr	r0, [pc, #416]	; (8003af4 <mk_hw_config+0x2ac>)
 8003952:	f7ff fa39 	bl	8002dc8 <LL_GPIO_SetAFPin_0_7>
 8003956:	2200      	movs	r2, #0
 8003958:	2180      	movs	r1, #128	; 0x80
 800395a:	4866      	ldr	r0, [pc, #408]	; (8003af4 <mk_hw_config+0x2ac>)
 800395c:	f7ff f9ef 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 8003960:	2202      	movs	r2, #2
 8003962:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003966:	4863      	ldr	r0, [pc, #396]	; (8003af4 <mk_hw_config+0x2ac>)
 8003968:	f7ff f9bc 	bl	8002ce4 <LL_GPIO_SetPinMode>
 800396c:	2202      	movs	r2, #2
 800396e:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003972:	4860      	ldr	r0, [pc, #384]	; (8003af4 <mk_hw_config+0x2ac>)
 8003974:	f7ff fa55 	bl	8002e22 <LL_GPIO_SetAFPin_8_15>
 8003978:	2200      	movs	r2, #0
 800397a:	f44f 7180 	mov.w	r1, #256	; 0x100
 800397e:	485d      	ldr	r0, [pc, #372]	; (8003af4 <mk_hw_config+0x2ac>)
 8003980:	f7ff f9dd 	bl	8002d3e <LL_GPIO_SetPinOutputType>
 8003984:	485c      	ldr	r0, [pc, #368]	; (8003af8 <mk_hw_config+0x2b0>)
 8003986:	f7ff fad9 	bl	8002f3c <LL_TIM_EnableUpdateEvent>
 800398a:	485b      	ldr	r0, [pc, #364]	; (8003af8 <mk_hw_config+0x2b0>)
 800398c:	f7ff fad6 	bl	8002f3c <LL_TIM_EnableUpdateEvent>
 8003990:	4859      	ldr	r0, [pc, #356]	; (8003af8 <mk_hw_config+0x2b0>)
 8003992:	f7ff fad3 	bl	8002f3c <LL_TIM_EnableUpdateEvent>
 8003996:	f44f 7100 	mov.w	r1, #512	; 0x200
 800399a:	4857      	ldr	r0, [pc, #348]	; (8003af8 <mk_hw_config+0x2b0>)
 800399c:	f7ff fb14 	bl	8002fc8 <LL_TIM_SetClockDivision>
 80039a0:	f44f 7100 	mov.w	r1, #512	; 0x200
 80039a4:	4854      	ldr	r0, [pc, #336]	; (8003af8 <mk_hw_config+0x2b0>)
 80039a6:	f7ff fb0f 	bl	8002fc8 <LL_TIM_SetClockDivision>
 80039aa:	f44f 7100 	mov.w	r1, #512	; 0x200
 80039ae:	4852      	ldr	r0, [pc, #328]	; (8003af8 <mk_hw_config+0x2b0>)
 80039b0:	f7ff fb0a 	bl	8002fc8 <LL_TIM_SetClockDivision>
 80039b4:	2100      	movs	r1, #0
 80039b6:	4850      	ldr	r0, [pc, #320]	; (8003af8 <mk_hw_config+0x2b0>)
 80039b8:	f7ff fae3 	bl	8002f82 <LL_TIM_SetCounterMode>
 80039bc:	2100      	movs	r1, #0
 80039be:	484e      	ldr	r0, [pc, #312]	; (8003af8 <mk_hw_config+0x2b0>)
 80039c0:	f7ff fadf 	bl	8002f82 <LL_TIM_SetCounterMode>
 80039c4:	2100      	movs	r1, #0
 80039c6:	484c      	ldr	r0, [pc, #304]	; (8003af8 <mk_hw_config+0x2b0>)
 80039c8:	f7ff fadb 	bl	8002f82 <LL_TIM_SetCounterMode>
 80039cc:	f241 019a 	movw	r1, #4250	; 0x109a
 80039d0:	4849      	ldr	r0, [pc, #292]	; (8003af8 <mk_hw_config+0x2b0>)
 80039d2:	f7ff fb1a 	bl	800300a <LL_TIM_SetAutoReload>
 80039d6:	f241 019a 	movw	r1, #4250	; 0x109a
 80039da:	4847      	ldr	r0, [pc, #284]	; (8003af8 <mk_hw_config+0x2b0>)
 80039dc:	f7ff fb15 	bl	800300a <LL_TIM_SetAutoReload>
 80039e0:	f241 019a 	movw	r1, #4250	; 0x109a
 80039e4:	4844      	ldr	r0, [pc, #272]	; (8003af8 <mk_hw_config+0x2b0>)
 80039e6:	f7ff fb10 	bl	800300a <LL_TIM_SetAutoReload>
 80039ea:	2100      	movs	r1, #0
 80039ec:	4842      	ldr	r0, [pc, #264]	; (8003af8 <mk_hw_config+0x2b0>)
 80039ee:	f7ff fab5 	bl	8002f5c <LL_TIM_SetUpdateSource>
 80039f2:	2100      	movs	r1, #0
 80039f4:	4840      	ldr	r0, [pc, #256]	; (8003af8 <mk_hw_config+0x2b0>)
 80039f6:	f7ff fab1 	bl	8002f5c <LL_TIM_SetUpdateSource>
 80039fa:	2100      	movs	r1, #0
 80039fc:	483e      	ldr	r0, [pc, #248]	; (8003af8 <mk_hw_config+0x2b0>)
 80039fe:	f7ff faad 	bl	8002f5c <LL_TIM_SetUpdateSource>
 8003a02:	2101      	movs	r1, #1
 8003a04:	483c      	ldr	r0, [pc, #240]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a06:	f7ff fb0e 	bl	8003026 <LL_TIM_CC_EnableChannel>
 8003a0a:	2110      	movs	r1, #16
 8003a0c:	483a      	ldr	r0, [pc, #232]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a0e:	f7ff fb0a 	bl	8003026 <LL_TIM_CC_EnableChannel>
 8003a12:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003a16:	4838      	ldr	r0, [pc, #224]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a18:	f7ff fb05 	bl	8003026 <LL_TIM_CC_EnableChannel>
 8003a1c:	2260      	movs	r2, #96	; 0x60
 8003a1e:	2101      	movs	r1, #1
 8003a20:	4835      	ldr	r0, [pc, #212]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a22:	f7ff fb11 	bl	8003048 <LL_TIM_OC_SetMode>
 8003a26:	2260      	movs	r2, #96	; 0x60
 8003a28:	2110      	movs	r1, #16
 8003a2a:	4833      	ldr	r0, [pc, #204]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a2c:	f7ff fb0c 	bl	8003048 <LL_TIM_OC_SetMode>
 8003a30:	2260      	movs	r2, #96	; 0x60
 8003a32:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003a36:	4830      	ldr	r0, [pc, #192]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a38:	f7ff fb06 	bl	8003048 <LL_TIM_OC_SetMode>
 8003a3c:	2101      	movs	r1, #1
 8003a3e:	482e      	ldr	r0, [pc, #184]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a40:	f7ff fb4c 	bl	80030dc <LL_TIM_OC_EnableFast>
 8003a44:	2110      	movs	r1, #16
 8003a46:	482c      	ldr	r0, [pc, #176]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a48:	f7ff fb48 	bl	80030dc <LL_TIM_OC_EnableFast>
 8003a4c:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003a50:	4829      	ldr	r0, [pc, #164]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a52:	f7ff fb43 	bl	80030dc <LL_TIM_OC_EnableFast>
 8003a56:	2101      	movs	r1, #1
 8003a58:	4827      	ldr	r0, [pc, #156]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a5a:	f7ff fb81 	bl	8003160 <LL_TIM_OC_EnablePreload>
 8003a5e:	2110      	movs	r1, #16
 8003a60:	4825      	ldr	r0, [pc, #148]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a62:	f7ff fb7d 	bl	8003160 <LL_TIM_OC_EnablePreload>
 8003a66:	f44f 7180 	mov.w	r1, #256	; 0x100
 8003a6a:	4823      	ldr	r0, [pc, #140]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a6c:	f7ff fb78 	bl	8003160 <LL_TIM_OC_EnablePreload>
 8003a70:	4821      	ldr	r0, [pc, #132]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a72:	f7ff fa99 	bl	8002fa8 <LL_TIM_EnableARRPreload>
 8003a76:	4820      	ldr	r0, [pc, #128]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a78:	f7ff fa96 	bl	8002fa8 <LL_TIM_EnableARRPreload>
 8003a7c:	481e      	ldr	r0, [pc, #120]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a7e:	f7ff fa93 	bl	8002fa8 <LL_TIM_EnableARRPreload>
 8003a82:	2100      	movs	r1, #0
 8003a84:	481c      	ldr	r0, [pc, #112]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a86:	f7ff fbad 	bl	80031e4 <LL_TIM_OC_SetCompareCH1>
 8003a8a:	2100      	movs	r1, #0
 8003a8c:	481a      	ldr	r0, [pc, #104]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a8e:	f7ff fbb7 	bl	8003200 <LL_TIM_OC_SetCompareCH2>
 8003a92:	2100      	movs	r1, #0
 8003a94:	4818      	ldr	r0, [pc, #96]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a96:	f7ff fbc1 	bl	800321c <LL_TIM_OC_SetCompareCH3>
 8003a9a:	4817      	ldr	r0, [pc, #92]	; (8003af8 <mk_hw_config+0x2b0>)
 8003a9c:	f7ff fbfd 	bl	800329a <LL_TIM_GenerateEvent_UPDATE>
 8003aa0:	4815      	ldr	r0, [pc, #84]	; (8003af8 <mk_hw_config+0x2b0>)
 8003aa2:	f7ff fbfa 	bl	800329a <LL_TIM_GenerateEvent_UPDATE>
 8003aa6:	4814      	ldr	r0, [pc, #80]	; (8003af8 <mk_hw_config+0x2b0>)
 8003aa8:	f7ff fbf7 	bl	800329a <LL_TIM_GenerateEvent_UPDATE>
 8003aac:	4812      	ldr	r0, [pc, #72]	; (8003af8 <mk_hw_config+0x2b0>)
 8003aae:	f7ff fa35 	bl	8002f1c <LL_TIM_EnableCounter>
 8003ab2:	4811      	ldr	r0, [pc, #68]	; (8003af8 <mk_hw_config+0x2b0>)
 8003ab4:	f7ff fa32 	bl	8002f1c <LL_TIM_EnableCounter>
 8003ab8:	480f      	ldr	r0, [pc, #60]	; (8003af8 <mk_hw_config+0x2b0>)
 8003aba:	f7ff fa2f 	bl	8002f1c <LL_TIM_EnableCounter>
 8003abe:	2101      	movs	r1, #1
 8003ac0:	480c      	ldr	r0, [pc, #48]	; (8003af4 <mk_hw_config+0x2ac>)
 8003ac2:	f7ff f9eb 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003ac6:	2104      	movs	r1, #4
 8003ac8:	480a      	ldr	r0, [pc, #40]	; (8003af4 <mk_hw_config+0x2ac>)
 8003aca:	f7ff f9e7 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003ace:	2110      	movs	r1, #16
 8003ad0:	4808      	ldr	r0, [pc, #32]	; (8003af4 <mk_hw_config+0x2ac>)
 8003ad2:	f7ff f9e3 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003ad6:	2102      	movs	r1, #2
 8003ad8:	4806      	ldr	r0, [pc, #24]	; (8003af4 <mk_hw_config+0x2ac>)
 8003ada:	f7ff f9df 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003ade:	2108      	movs	r1, #8
 8003ae0:	4804      	ldr	r0, [pc, #16]	; (8003af4 <mk_hw_config+0x2ac>)
 8003ae2:	f7ff f9db 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003ae6:	2120      	movs	r1, #32
 8003ae8:	4802      	ldr	r0, [pc, #8]	; (8003af4 <mk_hw_config+0x2ac>)
 8003aea:	f7ff f9d7 	bl	8002e9c <LL_GPIO_ResetOutputPin>
 8003aee:	bf00      	nop
 8003af0:	bd80      	pop	{r7, pc}
 8003af2:	bf00      	nop
 8003af4:	40020800 	.word	0x40020800
 8003af8:	40000400 	.word	0x40000400

08003afc <motor_pwm>:
 8003afc:	b580      	push	{r7, lr}
 8003afe:	b09e      	sub	sp, #120	; 0x78
 8003b00:	af00      	add	r7, sp, #0
 8003b02:	6078      	str	r0, [r7, #4]
 8003b04:	f107 034c 	add.w	r3, r7, #76	; 0x4c
 8003b08:	222c      	movs	r2, #44	; 0x2c
 8003b0a:	2100      	movs	r1, #0
 8003b0c:	4618      	mov	r0, r3
 8003b0e:	f7fd f935 	bl	8000d7c <memset>
 8003b12:	4b1e      	ldr	r3, [pc, #120]	; (8003b8c <motor_pwm+0x90>)
 8003b14:	66fb      	str	r3, [r7, #108]	; 0x6c
 8003b16:	4b1d      	ldr	r3, [pc, #116]	; (8003b8c <motor_pwm+0x90>)
 8003b18:	673b      	str	r3, [r7, #112]	; 0x70
 8003b1a:	4b1c      	ldr	r3, [pc, #112]	; (8003b8c <motor_pwm+0x90>)
 8003b1c:	677b      	str	r3, [r7, #116]	; 0x74
 8003b1e:	f107 0308 	add.w	r3, r7, #8
 8003b22:	2244      	movs	r2, #68	; 0x44
 8003b24:	2100      	movs	r1, #0
 8003b26:	4618      	mov	r0, r3
 8003b28:	f7fd f928 	bl	8000d7c <memset>
 8003b2c:	4b18      	ldr	r3, [pc, #96]	; (8003b90 <motor_pwm+0x94>)
 8003b2e:	613b      	str	r3, [r7, #16]
 8003b30:	4b18      	ldr	r3, [pc, #96]	; (8003b94 <motor_pwm+0x98>)
 8003b32:	617b      	str	r3, [r7, #20]
 8003b34:	4b18      	ldr	r3, [pc, #96]	; (8003b98 <motor_pwm+0x9c>)
 8003b36:	61bb      	str	r3, [r7, #24]
 8003b38:	4b18      	ldr	r3, [pc, #96]	; (8003b9c <motor_pwm+0xa0>)
 8003b3a:	61fb      	str	r3, [r7, #28]
 8003b3c:	4b18      	ldr	r3, [pc, #96]	; (8003ba0 <motor_pwm+0xa4>)
 8003b3e:	623b      	str	r3, [r7, #32]
 8003b40:	4b18      	ldr	r3, [pc, #96]	; (8003ba4 <motor_pwm+0xa8>)
 8003b42:	627b      	str	r3, [r7, #36]	; 0x24
 8003b44:	f7ff fe80 	bl	8003848 <mk_hw_config>
 8003b48:	4a17      	ldr	r2, [pc, #92]	; (8003ba8 <motor_pwm+0xac>)
 8003b4a:	f107 034c 	add.w	r3, r7, #76	; 0x4c
 8003b4e:	6013      	str	r3, [r2, #0]
 8003b50:	f004 fc54 	bl	80083fc <xTaskGetCurrentTaskHandle>
 8003b54:	4602      	mov	r2, r0
 8003b56:	4b15      	ldr	r3, [pc, #84]	; (8003bac <motor_pwm+0xb0>)
 8003b58:	601a      	str	r2, [r3, #0]
 8003b5a:	f7ff fc33 	bl	80033c4 <pid_hw_config>
 8003b5e:	4a14      	ldr	r2, [pc, #80]	; (8003bb0 <motor_pwm+0xb4>)
 8003b60:	f107 0308 	add.w	r3, r7, #8
 8003b64:	6013      	str	r3, [r2, #0]
 8003b66:	f001 fcb3 	bl	80054d0 <manip_usart_config>
 8003b6a:	f04f 32ff 	mov.w	r2, #4294967295
 8003b6e:	2101      	movs	r1, #1
 8003b70:	2000      	movs	r0, #0
 8003b72:	f004 fce9 	bl	8008548 <ulTaskGenericNotifyTake>
 8003b76:	4b0e      	ldr	r3, [pc, #56]	; (8003bb0 <motor_pwm+0xb4>)
 8003b78:	681b      	ldr	r3, [r3, #0]
 8003b7a:	3338      	adds	r3, #56	; 0x38
 8003b7c:	f107 0208 	add.w	r2, r7, #8
 8003b80:	4611      	mov	r1, r2
 8003b82:	4618      	mov	r0, r3
 8003b84:	f7ff fcfa 	bl	800357c <pid_calc>
 8003b88:	e7ef      	b.n	8003b6a <motor_pwm+0x6e>
 8003b8a:	bf00      	nop
 8003b8c:	3dcccccd 	.word	0x3dcccccd
 8003b90:	428c0000 	.word	0x428c0000
 8003b94:	41a00000 	.word	0x41a00000
 8003b98:	3c23d70a 	.word	0x3c23d70a
 8003b9c:	5368d4a5 	.word	0x5368d4a5
 8003ba0:	45834000 	.word	0x45834000
 8003ba4:	c5834000 	.word	0xc5834000
 8003ba8:	2000090c 	.word	0x2000090c
 8003bac:	20003178 	.word	0x20003178
 8003bb0:	20000910 	.word	0x20000910

08003bb4 <cmd_set_pwm>:
 8003bb4:	b590      	push	{r4, r7, lr}
 8003bb6:	b085      	sub	sp, #20
 8003bb8:	af00      	add	r7, sp, #0
 8003bba:	6078      	str	r0, [r7, #4]
 8003bbc:	687b      	ldr	r3, [r7, #4]
 8003bbe:	60fb      	str	r3, [r7, #12]
 8003bc0:	4b32      	ldr	r3, [pc, #200]	; (8003c8c <cmd_set_pwm+0xd8>)
 8003bc2:	681b      	ldr	r3, [r3, #0]
 8003bc4:	2b00      	cmp	r3, #0
 8003bc6:	d056      	beq.n	8003c76 <cmd_set_pwm+0xc2>
 8003bc8:	68fb      	ldr	r3, [r7, #12]
 8003bca:	781b      	ldrb	r3, [r3, #0]
 8003bcc:	2b03      	cmp	r3, #3
 8003bce:	d853      	bhi.n	8003c78 <cmd_set_pwm+0xc4>
 8003bd0:	68fb      	ldr	r3, [r7, #12]
 8003bd2:	781b      	ldrb	r3, [r3, #0]
 8003bd4:	2b00      	cmp	r3, #0
 8003bd6:	d04f      	beq.n	8003c78 <cmd_set_pwm+0xc4>
 8003bd8:	68fb      	ldr	r3, [r7, #12]
 8003bda:	785a      	ldrb	r2, [r3, #1]
 8003bdc:	7899      	ldrb	r1, [r3, #2]
 8003bde:	0209      	lsls	r1, r1, #8
 8003be0:	430a      	orrs	r2, r1
 8003be2:	78d9      	ldrb	r1, [r3, #3]
 8003be4:	0409      	lsls	r1, r1, #16
 8003be6:	430a      	orrs	r2, r1
 8003be8:	791b      	ldrb	r3, [r3, #4]
 8003bea:	061b      	lsls	r3, r3, #24
 8003bec:	4313      	orrs	r3, r2
 8003bee:	ee07 3a90 	vmov	s15, r3
 8003bf2:	eef0 7ae7 	vabs.f32	s15, s15
 8003bf6:	ed9f 7a26 	vldr	s14, [pc, #152]	; 8003c90 <cmd_set_pwm+0xdc>
 8003bfa:	eef4 7ac7 	vcmpe.f32	s15, s14
 8003bfe:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8003c02:	d439      	bmi.n	8003c78 <cmd_set_pwm+0xc4>
 8003c04:	68fb      	ldr	r3, [r7, #12]
 8003c06:	785a      	ldrb	r2, [r3, #1]
 8003c08:	7899      	ldrb	r1, [r3, #2]
 8003c0a:	0209      	lsls	r1, r1, #8
 8003c0c:	430a      	orrs	r2, r1
 8003c0e:	78d9      	ldrb	r1, [r3, #3]
 8003c10:	0409      	lsls	r1, r1, #16
 8003c12:	430a      	orrs	r2, r1
 8003c14:	791b      	ldrb	r3, [r3, #4]
 8003c16:	061b      	lsls	r3, r3, #24
 8003c18:	4313      	orrs	r3, r2
 8003c1a:	ee07 3a90 	vmov	s15, r3
 8003c1e:	eef0 7ae7 	vabs.f32	s15, s15
 8003c22:	eeb7 7a00 	vmov.f32	s14, #112	; 0x3f800000  1.0
 8003c26:	eef4 7ac7 	vcmpe.f32	s15, s14
 8003c2a:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8003c2e:	dc23      	bgt.n	8003c78 <cmd_set_pwm+0xc4>
 8003c30:	4b16      	ldr	r3, [pc, #88]	; (8003c8c <cmd_set_pwm+0xd8>)
 8003c32:	681b      	ldr	r3, [r3, #0]
 8003c34:	3320      	adds	r3, #32
 8003c36:	4618      	mov	r0, r3
 8003c38:	f7ff fbe8 	bl	800340c <pwm_set>
 8003c3c:	4b13      	ldr	r3, [pc, #76]	; (8003c8c <cmd_set_pwm+0xd8>)
 8003c3e:	681a      	ldr	r2, [r3, #0]
 8003c40:	68fb      	ldr	r3, [r7, #12]
 8003c42:	781b      	ldrb	r3, [r3, #0]
 8003c44:	1e59      	subs	r1, r3, #1
 8003c46:	68fb      	ldr	r3, [r7, #12]
 8003c48:	7858      	ldrb	r0, [r3, #1]
 8003c4a:	789c      	ldrb	r4, [r3, #2]
 8003c4c:	0224      	lsls	r4, r4, #8
 8003c4e:	4320      	orrs	r0, r4
 8003c50:	78dc      	ldrb	r4, [r3, #3]
 8003c52:	0424      	lsls	r4, r4, #16
 8003c54:	4320      	orrs	r0, r4
 8003c56:	791b      	ldrb	r3, [r3, #4]
 8003c58:	061b      	lsls	r3, r3, #24
 8003c5a:	4303      	orrs	r3, r0
 8003c5c:	4618      	mov	r0, r3
 8003c5e:	f101 0308 	add.w	r3, r1, #8
 8003c62:	009b      	lsls	r3, r3, #2
 8003c64:	4413      	add	r3, r2
 8003c66:	6018      	str	r0, [r3, #0]
 8003c68:	2203      	movs	r2, #3
 8003c6a:	490a      	ldr	r1, [pc, #40]	; (8003c94 <cmd_set_pwm+0xe0>)
 8003c6c:	6878      	ldr	r0, [r7, #4]
 8003c6e:	f7fc ffeb 	bl	8000c48 <memcpy>
 8003c72:	2303      	movs	r3, #3
 8003c74:	e006      	b.n	8003c84 <cmd_set_pwm+0xd0>
 8003c76:	bf00      	nop
 8003c78:	2203      	movs	r2, #3
 8003c7a:	4907      	ldr	r1, [pc, #28]	; (8003c98 <cmd_set_pwm+0xe4>)
 8003c7c:	6878      	ldr	r0, [r7, #4]
 8003c7e:	f7fc ffe3 	bl	8000c48 <memcpy>
 8003c82:	2303      	movs	r3, #3
 8003c84:	4618      	mov	r0, r3
 8003c86:	3714      	adds	r7, #20
 8003c88:	46bd      	mov	sp, r7
 8003c8a:	bd90      	pop	{r4, r7, pc}
 8003c8c:	2000090c 	.word	0x2000090c
 8003c90:	3dcccccd 	.word	0x3dcccccd
 8003c94:	08009888 	.word	0x08009888
 8003c98:	0800988c 	.word	0x0800988c

08003c9c <cmd_set_pid_coef>:
 8003c9c:	b580      	push	{r7, lr}
 8003c9e:	b084      	sub	sp, #16
 8003ca0:	af00      	add	r7, sp, #0
 8003ca2:	6078      	str	r0, [r7, #4]
 8003ca4:	687b      	ldr	r3, [r7, #4]
 8003ca6:	60fb      	str	r3, [r7, #12]
 8003ca8:	4b0e      	ldr	r3, [pc, #56]	; (8003ce4 <cmd_set_pid_coef+0x48>)
 8003caa:	681b      	ldr	r3, [r3, #0]
 8003cac:	2b00      	cmp	r3, #0
 8003cae:	d00e      	beq.n	8003cce <cmd_set_pid_coef+0x32>
 8003cb0:	4b0c      	ldr	r3, [pc, #48]	; (8003ce4 <cmd_set_pid_coef+0x48>)
 8003cb2:	681b      	ldr	r3, [r3, #0]
 8003cb4:	3308      	adds	r3, #8
 8003cb6:	68f9      	ldr	r1, [r7, #12]
 8003cb8:	220c      	movs	r2, #12
 8003cba:	4618      	mov	r0, r3
 8003cbc:	f7fc ffc4 	bl	8000c48 <memcpy>
 8003cc0:	2203      	movs	r2, #3
 8003cc2:	4909      	ldr	r1, [pc, #36]	; (8003ce8 <cmd_set_pid_coef+0x4c>)
 8003cc4:	6878      	ldr	r0, [r7, #4]
 8003cc6:	f7fc ffbf 	bl	8000c48 <memcpy>
 8003cca:	2303      	movs	r3, #3
 8003ccc:	e006      	b.n	8003cdc <cmd_set_pid_coef+0x40>
 8003cce:	bf00      	nop
 8003cd0:	2203      	movs	r2, #3
 8003cd2:	4906      	ldr	r1, [pc, #24]	; (8003cec <cmd_set_pid_coef+0x50>)
 8003cd4:	6878      	ldr	r0, [r7, #4]
 8003cd6:	f7fc ffb7 	bl	8000c48 <memcpy>
 8003cda:	2303      	movs	r3, #3
 8003cdc:	4618      	mov	r0, r3
 8003cde:	3710      	adds	r7, #16
 8003ce0:	46bd      	mov	sp, r7
 8003ce2:	bd80      	pop	{r7, pc}
 8003ce4:	20000910 	.word	0x20000910
 8003ce8:	08009888 	.word	0x08009888
 8003cec:	0800988c 	.word	0x0800988c

08003cf0 <cmd_get_pid_output>:
 8003cf0:	b580      	push	{r7, lr}
 8003cf2:	b082      	sub	sp, #8
 8003cf4:	af00      	add	r7, sp, #0
 8003cf6:	6078      	str	r0, [r7, #4]
 8003cf8:	4b0c      	ldr	r3, [pc, #48]	; (8003d2c <cmd_get_pid_output+0x3c>)
 8003cfa:	681b      	ldr	r3, [r3, #0]
 8003cfc:	2b00      	cmp	r3, #0
 8003cfe:	d009      	beq.n	8003d14 <cmd_get_pid_output+0x24>
 8003d00:	4b0a      	ldr	r3, [pc, #40]	; (8003d2c <cmd_get_pid_output+0x3c>)
 8003d02:	681b      	ldr	r3, [r3, #0]
 8003d04:	3320      	adds	r3, #32
 8003d06:	220c      	movs	r2, #12
 8003d08:	4619      	mov	r1, r3
 8003d0a:	6878      	ldr	r0, [r7, #4]
 8003d0c:	f7fc ff9c 	bl	8000c48 <memcpy>
 8003d10:	230c      	movs	r3, #12
 8003d12:	e006      	b.n	8003d22 <cmd_get_pid_output+0x32>
 8003d14:	bf00      	nop
 8003d16:	2206      	movs	r2, #6
 8003d18:	4905      	ldr	r1, [pc, #20]	; (8003d30 <cmd_get_pid_output+0x40>)
 8003d1a:	6878      	ldr	r0, [r7, #4]
 8003d1c:	f7fc ff94 	bl	8000c48 <memcpy>
 8003d20:	2306      	movs	r3, #6
 8003d22:	4618      	mov	r0, r3
 8003d24:	3708      	adds	r7, #8
 8003d26:	46bd      	mov	sp, r7
 8003d28:	bd80      	pop	{r7, pc}
 8003d2a:	bf00      	nop
 8003d2c:	2000090c 	.word	0x2000090c
 8003d30:	08009890 	.word	0x08009890

08003d34 <cmd_get_pid_setpoint>:
 8003d34:	b580      	push	{r7, lr}
 8003d36:	b082      	sub	sp, #8
 8003d38:	af00      	add	r7, sp, #0
 8003d3a:	6078      	str	r0, [r7, #4]
 8003d3c:	4b0c      	ldr	r3, [pc, #48]	; (8003d70 <cmd_get_pid_setpoint+0x3c>)
 8003d3e:	681b      	ldr	r3, [r3, #0]
 8003d40:	2b00      	cmp	r3, #0
 8003d42:	d009      	beq.n	8003d58 <cmd_get_pid_setpoint+0x24>
 8003d44:	4b0a      	ldr	r3, [pc, #40]	; (8003d70 <cmd_get_pid_setpoint+0x3c>)
 8003d46:	681b      	ldr	r3, [r3, #0]
 8003d48:	3338      	adds	r3, #56	; 0x38
 8003d4a:	220c      	movs	r2, #12
 8003d4c:	4619      	mov	r1, r3
 8003d4e:	6878      	ldr	r0, [r7, #4]
 8003d50:	f7fc ff7a 	bl	8000c48 <memcpy>
 8003d54:	230c      	movs	r3, #12
 8003d56:	e006      	b.n	8003d66 <cmd_get_pid_setpoint+0x32>
 8003d58:	bf00      	nop
 8003d5a:	2206      	movs	r2, #6
 8003d5c:	4905      	ldr	r1, [pc, #20]	; (8003d74 <cmd_get_pid_setpoint+0x40>)
 8003d5e:	6878      	ldr	r0, [r7, #4]
 8003d60:	f7fc ff72 	bl	8000c48 <memcpy>
 8003d64:	2306      	movs	r3, #6
 8003d66:	4618      	mov	r0, r3
 8003d68:	3708      	adds	r7, #8
 8003d6a:	46bd      	mov	sp, r7
 8003d6c:	bd80      	pop	{r7, pc}
 8003d6e:	bf00      	nop
 8003d70:	20000910 	.word	0x20000910
 8003d74:	08009890 	.word	0x08009890

08003d78 <cmd_set_speed>:
 8003d78:	b580      	push	{r7, lr}
 8003d7a:	b084      	sub	sp, #16
 8003d7c:	af00      	add	r7, sp, #0
 8003d7e:	6078      	str	r0, [r7, #4]
 8003d80:	687b      	ldr	r3, [r7, #4]
 8003d82:	60fb      	str	r3, [r7, #12]
 8003d84:	4b11      	ldr	r3, [pc, #68]	; (8003dcc <cmd_set_speed+0x54>)
 8003d86:	681b      	ldr	r3, [r3, #0]
 8003d88:	2b00      	cmp	r3, #0
 8003d8a:	d013      	beq.n	8003db4 <cmd_set_speed+0x3c>
 8003d8c:	4b0f      	ldr	r3, [pc, #60]	; (8003dcc <cmd_set_speed+0x54>)
 8003d8e:	681b      	ldr	r3, [r3, #0]
 8003d90:	330c      	adds	r3, #12
 8003d92:	68f9      	ldr	r1, [r7, #12]
 8003d94:	220c      	movs	r2, #12
 8003d96:	4618      	mov	r0, r3
 8003d98:	f7fc ff56 	bl	8000c48 <memcpy>
 8003d9c:	4b0b      	ldr	r3, [pc, #44]	; (8003dcc <cmd_set_speed+0x54>)
 8003d9e:	681b      	ldr	r3, [r3, #0]
 8003da0:	4618      	mov	r0, r3
 8003da2:	f7ff faad 	bl	8003300 <mk_speed>
 8003da6:	2203      	movs	r2, #3
 8003da8:	4909      	ldr	r1, [pc, #36]	; (8003dd0 <cmd_set_speed+0x58>)
 8003daa:	6878      	ldr	r0, [r7, #4]
 8003dac:	f7fc ff4c 	bl	8000c48 <memcpy>
 8003db0:	2303      	movs	r3, #3
 8003db2:	e006      	b.n	8003dc2 <cmd_set_speed+0x4a>
 8003db4:	bf00      	nop
 8003db6:	2203      	movs	r2, #3
 8003db8:	4906      	ldr	r1, [pc, #24]	; (8003dd4 <cmd_set_speed+0x5c>)
 8003dba:	6878      	ldr	r0, [r7, #4]
 8003dbc:	f7fc ff44 	bl	8000c48 <memcpy>
 8003dc0:	2303      	movs	r3, #3
 8003dc2:	4618      	mov	r0, r3
 8003dc4:	3710      	adds	r7, #16
 8003dc6:	46bd      	mov	sp, r7
 8003dc8:	bd80      	pop	{r7, pc}
 8003dca:	bf00      	nop
 8003dcc:	2000090c 	.word	0x2000090c
 8003dd0:	08009888 	.word	0x08009888
 8003dd4:	0800988c 	.word	0x0800988c

08003dd8 <cmd_set_wheel_speed>:
 8003dd8:	b580      	push	{r7, lr}
 8003dda:	b086      	sub	sp, #24
 8003ddc:	af00      	add	r7, sp, #0
 8003dde:	6078      	str	r0, [r7, #4]
 8003de0:	687b      	ldr	r3, [r7, #4]
 8003de2:	617b      	str	r3, [r7, #20]
 8003de4:	697b      	ldr	r3, [r7, #20]
 8003de6:	781a      	ldrb	r2, [r3, #0]
 8003de8:	7859      	ldrb	r1, [r3, #1]
 8003dea:	0209      	lsls	r1, r1, #8
 8003dec:	430a      	orrs	r2, r1
 8003dee:	7899      	ldrb	r1, [r3, #2]
 8003df0:	0409      	lsls	r1, r1, #16
 8003df2:	430a      	orrs	r2, r1
 8003df4:	78db      	ldrb	r3, [r3, #3]
 8003df6:	061b      	lsls	r3, r3, #24
 8003df8:	4313      	orrs	r3, r2
 8003dfa:	60bb      	str	r3, [r7, #8]
 8003dfc:	697b      	ldr	r3, [r7, #20]
 8003dfe:	791a      	ldrb	r2, [r3, #4]
 8003e00:	7959      	ldrb	r1, [r3, #5]
 8003e02:	0209      	lsls	r1, r1, #8
 8003e04:	430a      	orrs	r2, r1
 8003e06:	7999      	ldrb	r1, [r3, #6]
 8003e08:	0409      	lsls	r1, r1, #16
 8003e0a:	430a      	orrs	r2, r1
 8003e0c:	79db      	ldrb	r3, [r3, #7]
 8003e0e:	061b      	lsls	r3, r3, #24
 8003e10:	4313      	orrs	r3, r2
 8003e12:	60fb      	str	r3, [r7, #12]
 8003e14:	697b      	ldr	r3, [r7, #20]
 8003e16:	7a1a      	ldrb	r2, [r3, #8]
 8003e18:	7a59      	ldrb	r1, [r3, #9]
 8003e1a:	0209      	lsls	r1, r1, #8
 8003e1c:	430a      	orrs	r2, r1
 8003e1e:	7a99      	ldrb	r1, [r3, #10]
 8003e20:	0409      	lsls	r1, r1, #16
 8003e22:	430a      	orrs	r2, r1
 8003e24:	7adb      	ldrb	r3, [r3, #11]
 8003e26:	061b      	lsls	r3, r3, #24
 8003e28:	4313      	orrs	r3, r2
 8003e2a:	613b      	str	r3, [r7, #16]
 8003e2c:	f107 0308 	add.w	r3, r7, #8
 8003e30:	4618      	mov	r0, r3
 8003e32:	f7ff fa43 	bl	80032bc <new_target>
 8003e36:	2203      	movs	r2, #3
 8003e38:	4904      	ldr	r1, [pc, #16]	; (8003e4c <cmd_set_wheel_speed+0x74>)
 8003e3a:	6878      	ldr	r0, [r7, #4]
 8003e3c:	f7fc ff04 	bl	8000c48 <memcpy>
 8003e40:	2303      	movs	r3, #3
 8003e42:	4618      	mov	r0, r3
 8003e44:	3718      	adds	r7, #24
 8003e46:	46bd      	mov	sp, r7
 8003e48:	bd80      	pop	{r7, pc}
 8003e4a:	bf00      	nop
 8003e4c:	08009888 	.word	0x08009888

08003e50 <TIM7_IRQHandler>:
 8003e50:	b580      	push	{r7, lr}
 8003e52:	b082      	sub	sp, #8
 8003e54:	af00      	add	r7, sp, #0
 8003e56:	2300      	movs	r3, #0
 8003e58:	607b      	str	r3, [r7, #4]
 8003e5a:	480f      	ldr	r0, [pc, #60]	; (8003e98 <TIM7_IRQHandler+0x48>)
 8003e5c:	f7ff f9fa 	bl	8003254 <LL_TIM_IsActiveFlag_UPDATE>
 8003e60:	4603      	mov	r3, r0
 8003e62:	2b00      	cmp	r3, #0
 8003e64:	d009      	beq.n	8003e7a <TIM7_IRQHandler+0x2a>
 8003e66:	480c      	ldr	r0, [pc, #48]	; (8003e98 <TIM7_IRQHandler+0x48>)
 8003e68:	f7ff f9e6 	bl	8003238 <LL_TIM_ClearFlag_UPDATE>
 8003e6c:	4b0b      	ldr	r3, [pc, #44]	; (8003e9c <TIM7_IRQHandler+0x4c>)
 8003e6e:	681b      	ldr	r3, [r3, #0]
 8003e70:	1d3a      	adds	r2, r7, #4
 8003e72:	2100      	movs	r1, #0
 8003e74:	4618      	mov	r0, r3
 8003e76:	f004 fbd3 	bl	8008620 <vTaskGenericNotifyGiveFromISR>
 8003e7a:	687b      	ldr	r3, [r7, #4]
 8003e7c:	2b00      	cmp	r3, #0
 8003e7e:	d007      	beq.n	8003e90 <TIM7_IRQHandler+0x40>
 8003e80:	4b07      	ldr	r3, [pc, #28]	; (8003ea0 <TIM7_IRQHandler+0x50>)
 8003e82:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8003e86:	601a      	str	r2, [r3, #0]
 8003e88:	f3bf 8f4f 	dsb	sy
 8003e8c:	f3bf 8f6f 	isb	sy
 8003e90:	bf00      	nop
 8003e92:	3708      	adds	r7, #8
 8003e94:	46bd      	mov	sp, r7
 8003e96:	bd80      	pop	{r7, pc}
 8003e98:	40001400 	.word	0x40001400
 8003e9c:	20003178 	.word	0x20003178
 8003ea0:	e000ed04 	.word	0xe000ed04

08003ea4 <_free_r>:
 8003ea4:	b580      	push	{r7, lr}
 8003ea6:	b082      	sub	sp, #8
 8003ea8:	af00      	add	r7, sp, #0
 8003eaa:	6078      	str	r0, [r7, #4]
 8003eac:	6039      	str	r1, [r7, #0]
 8003eae:	6838      	ldr	r0, [r7, #0]
 8003eb0:	f005 fa38 	bl	8009324 <vPortFree>
 8003eb4:	bf00      	nop
 8003eb6:	3708      	adds	r7, #8
 8003eb8:	46bd      	mov	sp, r7
 8003eba:	bd80      	pop	{r7, pc}

08003ebc <vApplicationIdleHook>:
 8003ebc:	b480      	push	{r7}
 8003ebe:	af00      	add	r7, sp, #0
 8003ec0:	bf00      	nop
 8003ec2:	46bd      	mov	sp, r7
 8003ec4:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003ec8:	4770      	bx	lr

08003eca <vApplicationTickHook>:
 8003eca:	b480      	push	{r7}
 8003ecc:	af00      	add	r7, sp, #0
 8003ece:	bf00      	nop
 8003ed0:	46bd      	mov	sp, r7
 8003ed2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003ed6:	4770      	bx	lr

08003ed8 <vApplicationStackOverflowHook>:
 8003ed8:	b480      	push	{r7}
 8003eda:	b085      	sub	sp, #20
 8003edc:	af00      	add	r7, sp, #0
 8003ede:	6078      	str	r0, [r7, #4]
 8003ee0:	6039      	str	r1, [r7, #0]
 8003ee2:	f04f 0350 	mov.w	r3, #80	; 0x50
 8003ee6:	f383 8811 	msr	BASEPRI, r3
 8003eea:	f3bf 8f6f 	isb	sy
 8003eee:	f3bf 8f4f 	dsb	sy
 8003ef2:	60fb      	str	r3, [r7, #12]
 8003ef4:	e7fe      	b.n	8003ef4 <vApplicationStackOverflowHook+0x1c>
	...

08003ef8 <vApplicationGetIdleTaskMemory>:
 8003ef8:	b480      	push	{r7}
 8003efa:	b085      	sub	sp, #20
 8003efc:	af00      	add	r7, sp, #0
 8003efe:	60f8      	str	r0, [r7, #12]
 8003f00:	60b9      	str	r1, [r7, #8]
 8003f02:	607a      	str	r2, [r7, #4]
 8003f04:	68fb      	ldr	r3, [r7, #12]
 8003f06:	4a07      	ldr	r2, [pc, #28]	; (8003f24 <vApplicationGetIdleTaskMemory+0x2c>)
 8003f08:	601a      	str	r2, [r3, #0]
 8003f0a:	68bb      	ldr	r3, [r7, #8]
 8003f0c:	4a06      	ldr	r2, [pc, #24]	; (8003f28 <vApplicationGetIdleTaskMemory+0x30>)
 8003f0e:	601a      	str	r2, [r3, #0]
 8003f10:	687b      	ldr	r3, [r7, #4]
 8003f12:	2282      	movs	r2, #130	; 0x82
 8003f14:	601a      	str	r2, [r3, #0]
 8003f16:	bf00      	nop
 8003f18:	3714      	adds	r7, #20
 8003f1a:	46bd      	mov	sp, r7
 8003f1c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003f20:	4770      	bx	lr
 8003f22:	bf00      	nop
 8003f24:	20005a88 	.word	0x20005a88
 8003f28:	20005f08 	.word	0x20005f08

08003f2c <vApplicationGetTimerTaskMemory>:
 8003f2c:	b480      	push	{r7}
 8003f2e:	b085      	sub	sp, #20
 8003f30:	af00      	add	r7, sp, #0
 8003f32:	60f8      	str	r0, [r7, #12]
 8003f34:	60b9      	str	r1, [r7, #8]
 8003f36:	607a      	str	r2, [r7, #4]
 8003f38:	68fb      	ldr	r3, [r7, #12]
 8003f3a:	4a07      	ldr	r2, [pc, #28]	; (8003f58 <vApplicationGetTimerTaskMemory+0x2c>)
 8003f3c:	601a      	str	r2, [r3, #0]
 8003f3e:	68bb      	ldr	r3, [r7, #8]
 8003f40:	4a06      	ldr	r2, [pc, #24]	; (8003f5c <vApplicationGetTimerTaskMemory+0x30>)
 8003f42:	601a      	str	r2, [r3, #0]
 8003f44:	687b      	ldr	r3, [r7, #4]
 8003f46:	f44f 7282 	mov.w	r2, #260	; 0x104
 8003f4a:	601a      	str	r2, [r3, #0]
 8003f4c:	bf00      	nop
 8003f4e:	3714      	adds	r7, #20
 8003f50:	46bd      	mov	sp, r7
 8003f52:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003f56:	4770      	bx	lr
 8003f58:	20000978 	.word	0x20000978
 8003f5c:	20000df8 	.word	0x20000df8

08003f60 <NVIC_EnableIRQ>:
 8003f60:	b480      	push	{r7}
 8003f62:	b083      	sub	sp, #12
 8003f64:	af00      	add	r7, sp, #0
 8003f66:	4603      	mov	r3, r0
 8003f68:	71fb      	strb	r3, [r7, #7]
 8003f6a:	4909      	ldr	r1, [pc, #36]	; (8003f90 <NVIC_EnableIRQ+0x30>)
 8003f6c:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8003f70:	095b      	lsrs	r3, r3, #5
 8003f72:	79fa      	ldrb	r2, [r7, #7]
 8003f74:	f002 021f 	and.w	r2, r2, #31
 8003f78:	2001      	movs	r0, #1
 8003f7a:	fa00 f202 	lsl.w	r2, r0, r2
 8003f7e:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 8003f82:	bf00      	nop
 8003f84:	370c      	adds	r7, #12
 8003f86:	46bd      	mov	sp, r7
 8003f88:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003f8c:	4770      	bx	lr
 8003f8e:	bf00      	nop
 8003f90:	e000e100 	.word	0xe000e100

08003f94 <NVIC_SetPriority>:
 8003f94:	b480      	push	{r7}
 8003f96:	b083      	sub	sp, #12
 8003f98:	af00      	add	r7, sp, #0
 8003f9a:	4603      	mov	r3, r0
 8003f9c:	6039      	str	r1, [r7, #0]
 8003f9e:	71fb      	strb	r3, [r7, #7]
 8003fa0:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8003fa4:	2b00      	cmp	r3, #0
 8003fa6:	da0b      	bge.n	8003fc0 <NVIC_SetPriority+0x2c>
 8003fa8:	490d      	ldr	r1, [pc, #52]	; (8003fe0 <NVIC_SetPriority+0x4c>)
 8003faa:	79fb      	ldrb	r3, [r7, #7]
 8003fac:	f003 030f 	and.w	r3, r3, #15
 8003fb0:	3b04      	subs	r3, #4
 8003fb2:	683a      	ldr	r2, [r7, #0]
 8003fb4:	b2d2      	uxtb	r2, r2
 8003fb6:	0112      	lsls	r2, r2, #4
 8003fb8:	b2d2      	uxtb	r2, r2
 8003fba:	440b      	add	r3, r1
 8003fbc:	761a      	strb	r2, [r3, #24]
 8003fbe:	e009      	b.n	8003fd4 <NVIC_SetPriority+0x40>
 8003fc0:	4908      	ldr	r1, [pc, #32]	; (8003fe4 <NVIC_SetPriority+0x50>)
 8003fc2:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8003fc6:	683a      	ldr	r2, [r7, #0]
 8003fc8:	b2d2      	uxtb	r2, r2
 8003fca:	0112      	lsls	r2, r2, #4
 8003fcc:	b2d2      	uxtb	r2, r2
 8003fce:	440b      	add	r3, r1
 8003fd0:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8003fd4:	bf00      	nop
 8003fd6:	370c      	adds	r7, #12
 8003fd8:	46bd      	mov	sp, r7
 8003fda:	f85d 7b04 	ldr.w	r7, [sp], #4
 8003fde:	4770      	bx	lr
 8003fe0:	e000ed00 	.word	0xe000ed00
 8003fe4:	e000e100 	.word	0xe000e100

08003fe8 <LL_USART_Enable>:
 8003fe8:	b480      	push	{r7}
 8003fea:	b083      	sub	sp, #12
 8003fec:	af00      	add	r7, sp, #0
 8003fee:	6078      	str	r0, [r7, #4]
 8003ff0:	687b      	ldr	r3, [r7, #4]
 8003ff2:	68db      	ldr	r3, [r3, #12]
 8003ff4:	f443 5200 	orr.w	r2, r3, #8192	; 0x2000
 8003ff8:	687b      	ldr	r3, [r7, #4]
 8003ffa:	60da      	str	r2, [r3, #12]
 8003ffc:	bf00      	nop
 8003ffe:	370c      	adds	r7, #12
 8004000:	46bd      	mov	sp, r7
 8004002:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004006:	4770      	bx	lr

08004008 <LL_USART_EnableDirectionRx>:
 8004008:	b480      	push	{r7}
 800400a:	b083      	sub	sp, #12
 800400c:	af00      	add	r7, sp, #0
 800400e:	6078      	str	r0, [r7, #4]
 8004010:	687b      	ldr	r3, [r7, #4]
 8004012:	68db      	ldr	r3, [r3, #12]
 8004014:	f043 0204 	orr.w	r2, r3, #4
 8004018:	687b      	ldr	r3, [r7, #4]
 800401a:	60da      	str	r2, [r3, #12]
 800401c:	bf00      	nop
 800401e:	370c      	adds	r7, #12
 8004020:	46bd      	mov	sp, r7
 8004022:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004026:	4770      	bx	lr

08004028 <LL_USART_EnableDirectionTx>:
 8004028:	b480      	push	{r7}
 800402a:	b083      	sub	sp, #12
 800402c:	af00      	add	r7, sp, #0
 800402e:	6078      	str	r0, [r7, #4]
 8004030:	687b      	ldr	r3, [r7, #4]
 8004032:	68db      	ldr	r3, [r3, #12]
 8004034:	f043 0208 	orr.w	r2, r3, #8
 8004038:	687b      	ldr	r3, [r7, #4]
 800403a:	60da      	str	r2, [r3, #12]
 800403c:	bf00      	nop
 800403e:	370c      	adds	r7, #12
 8004040:	46bd      	mov	sp, r7
 8004042:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004046:	4770      	bx	lr

08004048 <LL_USART_SetTransferDirection>:
 8004048:	b480      	push	{r7}
 800404a:	b083      	sub	sp, #12
 800404c:	af00      	add	r7, sp, #0
 800404e:	6078      	str	r0, [r7, #4]
 8004050:	6039      	str	r1, [r7, #0]
 8004052:	687b      	ldr	r3, [r7, #4]
 8004054:	68db      	ldr	r3, [r3, #12]
 8004056:	f023 020c 	bic.w	r2, r3, #12
 800405a:	683b      	ldr	r3, [r7, #0]
 800405c:	431a      	orrs	r2, r3
 800405e:	687b      	ldr	r3, [r7, #4]
 8004060:	60da      	str	r2, [r3, #12]
 8004062:	bf00      	nop
 8004064:	370c      	adds	r7, #12
 8004066:	46bd      	mov	sp, r7
 8004068:	f85d 7b04 	ldr.w	r7, [sp], #4
 800406c:	4770      	bx	lr

0800406e <LL_USART_SetParity>:
 800406e:	b480      	push	{r7}
 8004070:	b083      	sub	sp, #12
 8004072:	af00      	add	r7, sp, #0
 8004074:	6078      	str	r0, [r7, #4]
 8004076:	6039      	str	r1, [r7, #0]
 8004078:	687b      	ldr	r3, [r7, #4]
 800407a:	68db      	ldr	r3, [r3, #12]
 800407c:	f423 62c0 	bic.w	r2, r3, #1536	; 0x600
 8004080:	683b      	ldr	r3, [r7, #0]
 8004082:	431a      	orrs	r2, r3
 8004084:	687b      	ldr	r3, [r7, #4]
 8004086:	60da      	str	r2, [r3, #12]
 8004088:	bf00      	nop
 800408a:	370c      	adds	r7, #12
 800408c:	46bd      	mov	sp, r7
 800408e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004092:	4770      	bx	lr

08004094 <LL_USART_SetDataWidth>:
 8004094:	b480      	push	{r7}
 8004096:	b083      	sub	sp, #12
 8004098:	af00      	add	r7, sp, #0
 800409a:	6078      	str	r0, [r7, #4]
 800409c:	6039      	str	r1, [r7, #0]
 800409e:	687b      	ldr	r3, [r7, #4]
 80040a0:	68db      	ldr	r3, [r3, #12]
 80040a2:	f423 5280 	bic.w	r2, r3, #4096	; 0x1000
 80040a6:	683b      	ldr	r3, [r7, #0]
 80040a8:	431a      	orrs	r2, r3
 80040aa:	687b      	ldr	r3, [r7, #4]
 80040ac:	60da      	str	r2, [r3, #12]
 80040ae:	bf00      	nop
 80040b0:	370c      	adds	r7, #12
 80040b2:	46bd      	mov	sp, r7
 80040b4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80040b8:	4770      	bx	lr

080040ba <LL_USART_SetStopBitsLength>:
 80040ba:	b480      	push	{r7}
 80040bc:	b083      	sub	sp, #12
 80040be:	af00      	add	r7, sp, #0
 80040c0:	6078      	str	r0, [r7, #4]
 80040c2:	6039      	str	r1, [r7, #0]
 80040c4:	687b      	ldr	r3, [r7, #4]
 80040c6:	691b      	ldr	r3, [r3, #16]
 80040c8:	f423 5240 	bic.w	r2, r3, #12288	; 0x3000
 80040cc:	683b      	ldr	r3, [r7, #0]
 80040ce:	431a      	orrs	r2, r3
 80040d0:	687b      	ldr	r3, [r7, #4]
 80040d2:	611a      	str	r2, [r3, #16]
 80040d4:	bf00      	nop
 80040d6:	370c      	adds	r7, #12
 80040d8:	46bd      	mov	sp, r7
 80040da:	f85d 7b04 	ldr.w	r7, [sp], #4
 80040de:	4770      	bx	lr

080040e0 <LL_USART_SetHWFlowCtrl>:
 80040e0:	b480      	push	{r7}
 80040e2:	b083      	sub	sp, #12
 80040e4:	af00      	add	r7, sp, #0
 80040e6:	6078      	str	r0, [r7, #4]
 80040e8:	6039      	str	r1, [r7, #0]
 80040ea:	687b      	ldr	r3, [r7, #4]
 80040ec:	695b      	ldr	r3, [r3, #20]
 80040ee:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 80040f2:	683b      	ldr	r3, [r7, #0]
 80040f4:	431a      	orrs	r2, r3
 80040f6:	687b      	ldr	r3, [r7, #4]
 80040f8:	615a      	str	r2, [r3, #20]
 80040fa:	bf00      	nop
 80040fc:	370c      	adds	r7, #12
 80040fe:	46bd      	mov	sp, r7
 8004100:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004104:	4770      	bx	lr
	...

08004108 <LL_USART_SetBaudRate>:
 8004108:	b480      	push	{r7}
 800410a:	b085      	sub	sp, #20
 800410c:	af00      	add	r7, sp, #0
 800410e:	60f8      	str	r0, [r7, #12]
 8004110:	60b9      	str	r1, [r7, #8]
 8004112:	607a      	str	r2, [r7, #4]
 8004114:	603b      	str	r3, [r7, #0]
 8004116:	687b      	ldr	r3, [r7, #4]
 8004118:	f5b3 4f00 	cmp.w	r3, #32768	; 0x8000
 800411c:	d152      	bne.n	80041c4 <LL_USART_SetBaudRate+0xbc>
 800411e:	68ba      	ldr	r2, [r7, #8]
 8004120:	4613      	mov	r3, r2
 8004122:	009b      	lsls	r3, r3, #2
 8004124:	4413      	add	r3, r2
 8004126:	009a      	lsls	r2, r3, #2
 8004128:	441a      	add	r2, r3
 800412a:	683b      	ldr	r3, [r7, #0]
 800412c:	005b      	lsls	r3, r3, #1
 800412e:	fbb2 f3f3 	udiv	r3, r2, r3
 8004132:	4a4f      	ldr	r2, [pc, #316]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 8004134:	fba2 2303 	umull	r2, r3, r2, r3
 8004138:	095b      	lsrs	r3, r3, #5
 800413a:	b29b      	uxth	r3, r3
 800413c:	011b      	lsls	r3, r3, #4
 800413e:	b299      	uxth	r1, r3
 8004140:	68ba      	ldr	r2, [r7, #8]
 8004142:	4613      	mov	r3, r2
 8004144:	009b      	lsls	r3, r3, #2
 8004146:	4413      	add	r3, r2
 8004148:	009a      	lsls	r2, r3, #2
 800414a:	441a      	add	r2, r3
 800414c:	683b      	ldr	r3, [r7, #0]
 800414e:	005b      	lsls	r3, r3, #1
 8004150:	fbb2 f2f3 	udiv	r2, r2, r3
 8004154:	4b46      	ldr	r3, [pc, #280]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 8004156:	fba3 0302 	umull	r0, r3, r3, r2
 800415a:	095b      	lsrs	r3, r3, #5
 800415c:	2064      	movs	r0, #100	; 0x64
 800415e:	fb00 f303 	mul.w	r3, r0, r3
 8004162:	1ad3      	subs	r3, r2, r3
 8004164:	00db      	lsls	r3, r3, #3
 8004166:	3332      	adds	r3, #50	; 0x32
 8004168:	4a41      	ldr	r2, [pc, #260]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 800416a:	fba2 2303 	umull	r2, r3, r2, r3
 800416e:	095b      	lsrs	r3, r3, #5
 8004170:	b29b      	uxth	r3, r3
 8004172:	005b      	lsls	r3, r3, #1
 8004174:	b29b      	uxth	r3, r3
 8004176:	f403 73f8 	and.w	r3, r3, #496	; 0x1f0
 800417a:	b29b      	uxth	r3, r3
 800417c:	440b      	add	r3, r1
 800417e:	b299      	uxth	r1, r3
 8004180:	68ba      	ldr	r2, [r7, #8]
 8004182:	4613      	mov	r3, r2
 8004184:	009b      	lsls	r3, r3, #2
 8004186:	4413      	add	r3, r2
 8004188:	009a      	lsls	r2, r3, #2
 800418a:	441a      	add	r2, r3
 800418c:	683b      	ldr	r3, [r7, #0]
 800418e:	005b      	lsls	r3, r3, #1
 8004190:	fbb2 f2f3 	udiv	r2, r2, r3
 8004194:	4b36      	ldr	r3, [pc, #216]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 8004196:	fba3 0302 	umull	r0, r3, r3, r2
 800419a:	095b      	lsrs	r3, r3, #5
 800419c:	2064      	movs	r0, #100	; 0x64
 800419e:	fb00 f303 	mul.w	r3, r0, r3
 80041a2:	1ad3      	subs	r3, r2, r3
 80041a4:	00db      	lsls	r3, r3, #3
 80041a6:	3332      	adds	r3, #50	; 0x32
 80041a8:	4a31      	ldr	r2, [pc, #196]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 80041aa:	fba2 2303 	umull	r2, r3, r2, r3
 80041ae:	095b      	lsrs	r3, r3, #5
 80041b0:	b29b      	uxth	r3, r3
 80041b2:	f003 0307 	and.w	r3, r3, #7
 80041b6:	b29b      	uxth	r3, r3
 80041b8:	440b      	add	r3, r1
 80041ba:	b29b      	uxth	r3, r3
 80041bc:	461a      	mov	r2, r3
 80041be:	68fb      	ldr	r3, [r7, #12]
 80041c0:	609a      	str	r2, [r3, #8]
 80041c2:	e04f      	b.n	8004264 <LL_USART_SetBaudRate+0x15c>
 80041c4:	68ba      	ldr	r2, [r7, #8]
 80041c6:	4613      	mov	r3, r2
 80041c8:	009b      	lsls	r3, r3, #2
 80041ca:	4413      	add	r3, r2
 80041cc:	009a      	lsls	r2, r3, #2
 80041ce:	441a      	add	r2, r3
 80041d0:	683b      	ldr	r3, [r7, #0]
 80041d2:	009b      	lsls	r3, r3, #2
 80041d4:	fbb2 f3f3 	udiv	r3, r2, r3
 80041d8:	4a25      	ldr	r2, [pc, #148]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 80041da:	fba2 2303 	umull	r2, r3, r2, r3
 80041de:	095b      	lsrs	r3, r3, #5
 80041e0:	b29b      	uxth	r3, r3
 80041e2:	011b      	lsls	r3, r3, #4
 80041e4:	b299      	uxth	r1, r3
 80041e6:	68ba      	ldr	r2, [r7, #8]
 80041e8:	4613      	mov	r3, r2
 80041ea:	009b      	lsls	r3, r3, #2
 80041ec:	4413      	add	r3, r2
 80041ee:	009a      	lsls	r2, r3, #2
 80041f0:	441a      	add	r2, r3
 80041f2:	683b      	ldr	r3, [r7, #0]
 80041f4:	009b      	lsls	r3, r3, #2
 80041f6:	fbb2 f2f3 	udiv	r2, r2, r3
 80041fa:	4b1d      	ldr	r3, [pc, #116]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 80041fc:	fba3 0302 	umull	r0, r3, r3, r2
 8004200:	095b      	lsrs	r3, r3, #5
 8004202:	2064      	movs	r0, #100	; 0x64
 8004204:	fb00 f303 	mul.w	r3, r0, r3
 8004208:	1ad3      	subs	r3, r2, r3
 800420a:	011b      	lsls	r3, r3, #4
 800420c:	3332      	adds	r3, #50	; 0x32
 800420e:	4a18      	ldr	r2, [pc, #96]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 8004210:	fba2 2303 	umull	r2, r3, r2, r3
 8004214:	095b      	lsrs	r3, r3, #5
 8004216:	b29b      	uxth	r3, r3
 8004218:	f003 03f0 	and.w	r3, r3, #240	; 0xf0
 800421c:	b29b      	uxth	r3, r3
 800421e:	440b      	add	r3, r1
 8004220:	b299      	uxth	r1, r3
 8004222:	68ba      	ldr	r2, [r7, #8]
 8004224:	4613      	mov	r3, r2
 8004226:	009b      	lsls	r3, r3, #2
 8004228:	4413      	add	r3, r2
 800422a:	009a      	lsls	r2, r3, #2
 800422c:	441a      	add	r2, r3
 800422e:	683b      	ldr	r3, [r7, #0]
 8004230:	009b      	lsls	r3, r3, #2
 8004232:	fbb2 f2f3 	udiv	r2, r2, r3
 8004236:	4b0e      	ldr	r3, [pc, #56]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 8004238:	fba3 0302 	umull	r0, r3, r3, r2
 800423c:	095b      	lsrs	r3, r3, #5
 800423e:	2064      	movs	r0, #100	; 0x64
 8004240:	fb00 f303 	mul.w	r3, r0, r3
 8004244:	1ad3      	subs	r3, r2, r3
 8004246:	011b      	lsls	r3, r3, #4
 8004248:	3332      	adds	r3, #50	; 0x32
 800424a:	4a09      	ldr	r2, [pc, #36]	; (8004270 <LL_USART_SetBaudRate+0x168>)
 800424c:	fba2 2303 	umull	r2, r3, r2, r3
 8004250:	095b      	lsrs	r3, r3, #5
 8004252:	b29b      	uxth	r3, r3
 8004254:	f003 030f 	and.w	r3, r3, #15
 8004258:	b29b      	uxth	r3, r3
 800425a:	440b      	add	r3, r1
 800425c:	b29b      	uxth	r3, r3
 800425e:	461a      	mov	r2, r3
 8004260:	68fb      	ldr	r3, [r7, #12]
 8004262:	609a      	str	r2, [r3, #8]
 8004264:	bf00      	nop
 8004266:	3714      	adds	r7, #20
 8004268:	46bd      	mov	sp, r7
 800426a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800426e:	4770      	bx	lr
 8004270:	51eb851f 	.word	0x51eb851f

08004274 <LL_USART_IsActiveFlag_TC>:
 8004274:	b480      	push	{r7}
 8004276:	b083      	sub	sp, #12
 8004278:	af00      	add	r7, sp, #0
 800427a:	6078      	str	r0, [r7, #4]
 800427c:	687b      	ldr	r3, [r7, #4]
 800427e:	681b      	ldr	r3, [r3, #0]
 8004280:	f003 0340 	and.w	r3, r3, #64	; 0x40
 8004284:	2b40      	cmp	r3, #64	; 0x40
 8004286:	bf0c      	ite	eq
 8004288:	2301      	moveq	r3, #1
 800428a:	2300      	movne	r3, #0
 800428c:	b2db      	uxtb	r3, r3
 800428e:	4618      	mov	r0, r3
 8004290:	370c      	adds	r7, #12
 8004292:	46bd      	mov	sp, r7
 8004294:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004298:	4770      	bx	lr

0800429a <LL_USART_IsActiveFlag_TXE>:
 800429a:	b480      	push	{r7}
 800429c:	b083      	sub	sp, #12
 800429e:	af00      	add	r7, sp, #0
 80042a0:	6078      	str	r0, [r7, #4]
 80042a2:	687b      	ldr	r3, [r7, #4]
 80042a4:	681b      	ldr	r3, [r3, #0]
 80042a6:	f003 0380 	and.w	r3, r3, #128	; 0x80
 80042aa:	2b80      	cmp	r3, #128	; 0x80
 80042ac:	bf0c      	ite	eq
 80042ae:	2301      	moveq	r3, #1
 80042b0:	2300      	movne	r3, #0
 80042b2:	b2db      	uxtb	r3, r3
 80042b4:	4618      	mov	r0, r3
 80042b6:	370c      	adds	r7, #12
 80042b8:	46bd      	mov	sp, r7
 80042ba:	f85d 7b04 	ldr.w	r7, [sp], #4
 80042be:	4770      	bx	lr

080042c0 <LL_USART_ClearFlag_IDLE>:
 80042c0:	b480      	push	{r7}
 80042c2:	b085      	sub	sp, #20
 80042c4:	af00      	add	r7, sp, #0
 80042c6:	6078      	str	r0, [r7, #4]
 80042c8:	687b      	ldr	r3, [r7, #4]
 80042ca:	681b      	ldr	r3, [r3, #0]
 80042cc:	60fb      	str	r3, [r7, #12]
 80042ce:	68fb      	ldr	r3, [r7, #12]
 80042d0:	687b      	ldr	r3, [r7, #4]
 80042d2:	685b      	ldr	r3, [r3, #4]
 80042d4:	60fb      	str	r3, [r7, #12]
 80042d6:	68fb      	ldr	r3, [r7, #12]
 80042d8:	bf00      	nop
 80042da:	3714      	adds	r7, #20
 80042dc:	46bd      	mov	sp, r7
 80042de:	f85d 7b04 	ldr.w	r7, [sp], #4
 80042e2:	4770      	bx	lr

080042e4 <LL_USART_ClearFlag_TC>:
 80042e4:	b480      	push	{r7}
 80042e6:	b083      	sub	sp, #12
 80042e8:	af00      	add	r7, sp, #0
 80042ea:	6078      	str	r0, [r7, #4]
 80042ec:	687b      	ldr	r3, [r7, #4]
 80042ee:	f06f 0240 	mvn.w	r2, #64	; 0x40
 80042f2:	601a      	str	r2, [r3, #0]
 80042f4:	bf00      	nop
 80042f6:	370c      	adds	r7, #12
 80042f8:	46bd      	mov	sp, r7
 80042fa:	f85d 7b04 	ldr.w	r7, [sp], #4
 80042fe:	4770      	bx	lr

08004300 <LL_USART_EnableIT_IDLE>:
 8004300:	b480      	push	{r7}
 8004302:	b083      	sub	sp, #12
 8004304:	af00      	add	r7, sp, #0
 8004306:	6078      	str	r0, [r7, #4]
 8004308:	687b      	ldr	r3, [r7, #4]
 800430a:	68db      	ldr	r3, [r3, #12]
 800430c:	f043 0210 	orr.w	r2, r3, #16
 8004310:	687b      	ldr	r3, [r7, #4]
 8004312:	60da      	str	r2, [r3, #12]
 8004314:	bf00      	nop
 8004316:	370c      	adds	r7, #12
 8004318:	46bd      	mov	sp, r7
 800431a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800431e:	4770      	bx	lr

08004320 <LL_USART_EnableDMAReq_RX>:
 8004320:	b480      	push	{r7}
 8004322:	b083      	sub	sp, #12
 8004324:	af00      	add	r7, sp, #0
 8004326:	6078      	str	r0, [r7, #4]
 8004328:	687b      	ldr	r3, [r7, #4]
 800432a:	695b      	ldr	r3, [r3, #20]
 800432c:	f043 0240 	orr.w	r2, r3, #64	; 0x40
 8004330:	687b      	ldr	r3, [r7, #4]
 8004332:	615a      	str	r2, [r3, #20]
 8004334:	bf00      	nop
 8004336:	370c      	adds	r7, #12
 8004338:	46bd      	mov	sp, r7
 800433a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800433e:	4770      	bx	lr

08004340 <LL_USART_TransmitData8>:
 8004340:	b480      	push	{r7}
 8004342:	b083      	sub	sp, #12
 8004344:	af00      	add	r7, sp, #0
 8004346:	6078      	str	r0, [r7, #4]
 8004348:	460b      	mov	r3, r1
 800434a:	70fb      	strb	r3, [r7, #3]
 800434c:	78fa      	ldrb	r2, [r7, #3]
 800434e:	687b      	ldr	r3, [r7, #4]
 8004350:	605a      	str	r2, [r3, #4]
 8004352:	bf00      	nop
 8004354:	370c      	adds	r7, #12
 8004356:	46bd      	mov	sp, r7
 8004358:	f85d 7b04 	ldr.w	r7, [sp], #4
 800435c:	4770      	bx	lr
	...

08004360 <LL_AHB1_GRP1_EnableClock>:
 8004360:	b480      	push	{r7}
 8004362:	b085      	sub	sp, #20
 8004364:	af00      	add	r7, sp, #0
 8004366:	6078      	str	r0, [r7, #4]
 8004368:	4908      	ldr	r1, [pc, #32]	; (800438c <LL_AHB1_GRP1_EnableClock+0x2c>)
 800436a:	4b08      	ldr	r3, [pc, #32]	; (800438c <LL_AHB1_GRP1_EnableClock+0x2c>)
 800436c:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 800436e:	687b      	ldr	r3, [r7, #4]
 8004370:	4313      	orrs	r3, r2
 8004372:	630b      	str	r3, [r1, #48]	; 0x30
 8004374:	4b05      	ldr	r3, [pc, #20]	; (800438c <LL_AHB1_GRP1_EnableClock+0x2c>)
 8004376:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8004378:	687b      	ldr	r3, [r7, #4]
 800437a:	4013      	ands	r3, r2
 800437c:	60fb      	str	r3, [r7, #12]
 800437e:	68fb      	ldr	r3, [r7, #12]
 8004380:	bf00      	nop
 8004382:	3714      	adds	r7, #20
 8004384:	46bd      	mov	sp, r7
 8004386:	f85d 7b04 	ldr.w	r7, [sp], #4
 800438a:	4770      	bx	lr
 800438c:	40023800 	.word	0x40023800

08004390 <LL_APB2_GRP1_EnableClock>:
 8004390:	b480      	push	{r7}
 8004392:	b085      	sub	sp, #20
 8004394:	af00      	add	r7, sp, #0
 8004396:	6078      	str	r0, [r7, #4]
 8004398:	4908      	ldr	r1, [pc, #32]	; (80043bc <LL_APB2_GRP1_EnableClock+0x2c>)
 800439a:	4b08      	ldr	r3, [pc, #32]	; (80043bc <LL_APB2_GRP1_EnableClock+0x2c>)
 800439c:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 800439e:	687b      	ldr	r3, [r7, #4]
 80043a0:	4313      	orrs	r3, r2
 80043a2:	644b      	str	r3, [r1, #68]	; 0x44
 80043a4:	4b05      	ldr	r3, [pc, #20]	; (80043bc <LL_APB2_GRP1_EnableClock+0x2c>)
 80043a6:	6c5a      	ldr	r2, [r3, #68]	; 0x44
 80043a8:	687b      	ldr	r3, [r7, #4]
 80043aa:	4013      	ands	r3, r2
 80043ac:	60fb      	str	r3, [r7, #12]
 80043ae:	68fb      	ldr	r3, [r7, #12]
 80043b0:	bf00      	nop
 80043b2:	3714      	adds	r7, #20
 80043b4:	46bd      	mov	sp, r7
 80043b6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80043ba:	4770      	bx	lr
 80043bc:	40023800 	.word	0x40023800

080043c0 <LL_DMA_EnableStream>:
 80043c0:	b480      	push	{r7}
 80043c2:	b083      	sub	sp, #12
 80043c4:	af00      	add	r7, sp, #0
 80043c6:	6078      	str	r0, [r7, #4]
 80043c8:	6039      	str	r1, [r7, #0]
 80043ca:	4a0c      	ldr	r2, [pc, #48]	; (80043fc <LL_DMA_EnableStream+0x3c>)
 80043cc:	683b      	ldr	r3, [r7, #0]
 80043ce:	4413      	add	r3, r2
 80043d0:	781b      	ldrb	r3, [r3, #0]
 80043d2:	461a      	mov	r2, r3
 80043d4:	687b      	ldr	r3, [r7, #4]
 80043d6:	4413      	add	r3, r2
 80043d8:	4619      	mov	r1, r3
 80043da:	4a08      	ldr	r2, [pc, #32]	; (80043fc <LL_DMA_EnableStream+0x3c>)
 80043dc:	683b      	ldr	r3, [r7, #0]
 80043de:	4413      	add	r3, r2
 80043e0:	781b      	ldrb	r3, [r3, #0]
 80043e2:	461a      	mov	r2, r3
 80043e4:	687b      	ldr	r3, [r7, #4]
 80043e6:	4413      	add	r3, r2
 80043e8:	681b      	ldr	r3, [r3, #0]
 80043ea:	f043 0301 	orr.w	r3, r3, #1
 80043ee:	600b      	str	r3, [r1, #0]
 80043f0:	bf00      	nop
 80043f2:	370c      	adds	r7, #12
 80043f4:	46bd      	mov	sp, r7
 80043f6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80043fa:	4770      	bx	lr
 80043fc:	08009d00 	.word	0x08009d00

08004400 <LL_DMA_DisableStream>:
 8004400:	b480      	push	{r7}
 8004402:	b083      	sub	sp, #12
 8004404:	af00      	add	r7, sp, #0
 8004406:	6078      	str	r0, [r7, #4]
 8004408:	6039      	str	r1, [r7, #0]
 800440a:	4a0c      	ldr	r2, [pc, #48]	; (800443c <LL_DMA_DisableStream+0x3c>)
 800440c:	683b      	ldr	r3, [r7, #0]
 800440e:	4413      	add	r3, r2
 8004410:	781b      	ldrb	r3, [r3, #0]
 8004412:	461a      	mov	r2, r3
 8004414:	687b      	ldr	r3, [r7, #4]
 8004416:	4413      	add	r3, r2
 8004418:	4619      	mov	r1, r3
 800441a:	4a08      	ldr	r2, [pc, #32]	; (800443c <LL_DMA_DisableStream+0x3c>)
 800441c:	683b      	ldr	r3, [r7, #0]
 800441e:	4413      	add	r3, r2
 8004420:	781b      	ldrb	r3, [r3, #0]
 8004422:	461a      	mov	r2, r3
 8004424:	687b      	ldr	r3, [r7, #4]
 8004426:	4413      	add	r3, r2
 8004428:	681b      	ldr	r3, [r3, #0]
 800442a:	f023 0301 	bic.w	r3, r3, #1
 800442e:	600b      	str	r3, [r1, #0]
 8004430:	bf00      	nop
 8004432:	370c      	adds	r7, #12
 8004434:	46bd      	mov	sp, r7
 8004436:	f85d 7b04 	ldr.w	r7, [sp], #4
 800443a:	4770      	bx	lr
 800443c:	08009d00 	.word	0x08009d00

08004440 <LL_DMA_SetMemoryIncMode>:
 8004440:	b480      	push	{r7}
 8004442:	b085      	sub	sp, #20
 8004444:	af00      	add	r7, sp, #0
 8004446:	60f8      	str	r0, [r7, #12]
 8004448:	60b9      	str	r1, [r7, #8]
 800444a:	607a      	str	r2, [r7, #4]
 800444c:	4a0d      	ldr	r2, [pc, #52]	; (8004484 <LL_DMA_SetMemoryIncMode+0x44>)
 800444e:	68bb      	ldr	r3, [r7, #8]
 8004450:	4413      	add	r3, r2
 8004452:	781b      	ldrb	r3, [r3, #0]
 8004454:	461a      	mov	r2, r3
 8004456:	68fb      	ldr	r3, [r7, #12]
 8004458:	4413      	add	r3, r2
 800445a:	4619      	mov	r1, r3
 800445c:	4a09      	ldr	r2, [pc, #36]	; (8004484 <LL_DMA_SetMemoryIncMode+0x44>)
 800445e:	68bb      	ldr	r3, [r7, #8]
 8004460:	4413      	add	r3, r2
 8004462:	781b      	ldrb	r3, [r3, #0]
 8004464:	461a      	mov	r2, r3
 8004466:	68fb      	ldr	r3, [r7, #12]
 8004468:	4413      	add	r3, r2
 800446a:	681b      	ldr	r3, [r3, #0]
 800446c:	f423 6280 	bic.w	r2, r3, #1024	; 0x400
 8004470:	687b      	ldr	r3, [r7, #4]
 8004472:	4313      	orrs	r3, r2
 8004474:	600b      	str	r3, [r1, #0]
 8004476:	bf00      	nop
 8004478:	3714      	adds	r7, #20
 800447a:	46bd      	mov	sp, r7
 800447c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004480:	4770      	bx	lr
 8004482:	bf00      	nop
 8004484:	08009d00 	.word	0x08009d00

08004488 <LL_DMA_SetDataLength>:
 8004488:	b480      	push	{r7}
 800448a:	b085      	sub	sp, #20
 800448c:	af00      	add	r7, sp, #0
 800448e:	60f8      	str	r0, [r7, #12]
 8004490:	60b9      	str	r1, [r7, #8]
 8004492:	607a      	str	r2, [r7, #4]
 8004494:	4a0d      	ldr	r2, [pc, #52]	; (80044cc <LL_DMA_SetDataLength+0x44>)
 8004496:	68bb      	ldr	r3, [r7, #8]
 8004498:	4413      	add	r3, r2
 800449a:	781b      	ldrb	r3, [r3, #0]
 800449c:	461a      	mov	r2, r3
 800449e:	68fb      	ldr	r3, [r7, #12]
 80044a0:	4413      	add	r3, r2
 80044a2:	4619      	mov	r1, r3
 80044a4:	4a09      	ldr	r2, [pc, #36]	; (80044cc <LL_DMA_SetDataLength+0x44>)
 80044a6:	68bb      	ldr	r3, [r7, #8]
 80044a8:	4413      	add	r3, r2
 80044aa:	781b      	ldrb	r3, [r3, #0]
 80044ac:	461a      	mov	r2, r3
 80044ae:	68fb      	ldr	r3, [r7, #12]
 80044b0:	4413      	add	r3, r2
 80044b2:	685b      	ldr	r3, [r3, #4]
 80044b4:	0c1b      	lsrs	r3, r3, #16
 80044b6:	041b      	lsls	r3, r3, #16
 80044b8:	687a      	ldr	r2, [r7, #4]
 80044ba:	4313      	orrs	r3, r2
 80044bc:	604b      	str	r3, [r1, #4]
 80044be:	bf00      	nop
 80044c0:	3714      	adds	r7, #20
 80044c2:	46bd      	mov	sp, r7
 80044c4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80044c8:	4770      	bx	lr
 80044ca:	bf00      	nop
 80044cc:	08009d00 	.word	0x08009d00

080044d0 <LL_DMA_SetChannelSelection>:
 80044d0:	b480      	push	{r7}
 80044d2:	b085      	sub	sp, #20
 80044d4:	af00      	add	r7, sp, #0
 80044d6:	60f8      	str	r0, [r7, #12]
 80044d8:	60b9      	str	r1, [r7, #8]
 80044da:	607a      	str	r2, [r7, #4]
 80044dc:	4a0d      	ldr	r2, [pc, #52]	; (8004514 <LL_DMA_SetChannelSelection+0x44>)
 80044de:	68bb      	ldr	r3, [r7, #8]
 80044e0:	4413      	add	r3, r2
 80044e2:	781b      	ldrb	r3, [r3, #0]
 80044e4:	461a      	mov	r2, r3
 80044e6:	68fb      	ldr	r3, [r7, #12]
 80044e8:	4413      	add	r3, r2
 80044ea:	4619      	mov	r1, r3
 80044ec:	4a09      	ldr	r2, [pc, #36]	; (8004514 <LL_DMA_SetChannelSelection+0x44>)
 80044ee:	68bb      	ldr	r3, [r7, #8]
 80044f0:	4413      	add	r3, r2
 80044f2:	781b      	ldrb	r3, [r3, #0]
 80044f4:	461a      	mov	r2, r3
 80044f6:	68fb      	ldr	r3, [r7, #12]
 80044f8:	4413      	add	r3, r2
 80044fa:	681b      	ldr	r3, [r3, #0]
 80044fc:	f023 6260 	bic.w	r2, r3, #234881024	; 0xe000000
 8004500:	687b      	ldr	r3, [r7, #4]
 8004502:	4313      	orrs	r3, r2
 8004504:	600b      	str	r3, [r1, #0]
 8004506:	bf00      	nop
 8004508:	3714      	adds	r7, #20
 800450a:	46bd      	mov	sp, r7
 800450c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004510:	4770      	bx	lr
 8004512:	bf00      	nop
 8004514:	08009d00 	.word	0x08009d00

08004518 <LL_DMA_ConfigAddresses>:
 8004518:	b480      	push	{r7}
 800451a:	b085      	sub	sp, #20
 800451c:	af00      	add	r7, sp, #0
 800451e:	60f8      	str	r0, [r7, #12]
 8004520:	60b9      	str	r1, [r7, #8]
 8004522:	607a      	str	r2, [r7, #4]
 8004524:	603b      	str	r3, [r7, #0]
 8004526:	69bb      	ldr	r3, [r7, #24]
 8004528:	2b40      	cmp	r3, #64	; 0x40
 800452a:	d114      	bne.n	8004556 <LL_DMA_ConfigAddresses+0x3e>
 800452c:	4a17      	ldr	r2, [pc, #92]	; (800458c <LL_DMA_ConfigAddresses+0x74>)
 800452e:	68bb      	ldr	r3, [r7, #8]
 8004530:	4413      	add	r3, r2
 8004532:	781b      	ldrb	r3, [r3, #0]
 8004534:	461a      	mov	r2, r3
 8004536:	68fb      	ldr	r3, [r7, #12]
 8004538:	4413      	add	r3, r2
 800453a:	461a      	mov	r2, r3
 800453c:	687b      	ldr	r3, [r7, #4]
 800453e:	60d3      	str	r3, [r2, #12]
 8004540:	4a12      	ldr	r2, [pc, #72]	; (800458c <LL_DMA_ConfigAddresses+0x74>)
 8004542:	68bb      	ldr	r3, [r7, #8]
 8004544:	4413      	add	r3, r2
 8004546:	781b      	ldrb	r3, [r3, #0]
 8004548:	461a      	mov	r2, r3
 800454a:	68fb      	ldr	r3, [r7, #12]
 800454c:	4413      	add	r3, r2
 800454e:	461a      	mov	r2, r3
 8004550:	683b      	ldr	r3, [r7, #0]
 8004552:	6093      	str	r3, [r2, #8]
 8004554:	e013      	b.n	800457e <LL_DMA_ConfigAddresses+0x66>
 8004556:	4a0d      	ldr	r2, [pc, #52]	; (800458c <LL_DMA_ConfigAddresses+0x74>)
 8004558:	68bb      	ldr	r3, [r7, #8]
 800455a:	4413      	add	r3, r2
 800455c:	781b      	ldrb	r3, [r3, #0]
 800455e:	461a      	mov	r2, r3
 8004560:	68fb      	ldr	r3, [r7, #12]
 8004562:	4413      	add	r3, r2
 8004564:	461a      	mov	r2, r3
 8004566:	687b      	ldr	r3, [r7, #4]
 8004568:	6093      	str	r3, [r2, #8]
 800456a:	4a08      	ldr	r2, [pc, #32]	; (800458c <LL_DMA_ConfigAddresses+0x74>)
 800456c:	68bb      	ldr	r3, [r7, #8]
 800456e:	4413      	add	r3, r2
 8004570:	781b      	ldrb	r3, [r3, #0]
 8004572:	461a      	mov	r2, r3
 8004574:	68fb      	ldr	r3, [r7, #12]
 8004576:	4413      	add	r3, r2
 8004578:	461a      	mov	r2, r3
 800457a:	683b      	ldr	r3, [r7, #0]
 800457c:	60d3      	str	r3, [r2, #12]
 800457e:	bf00      	nop
 8004580:	3714      	adds	r7, #20
 8004582:	46bd      	mov	sp, r7
 8004584:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004588:	4770      	bx	lr
 800458a:	bf00      	nop
 800458c:	08009d00 	.word	0x08009d00

08004590 <LL_DMA_IsActiveFlag_TC2>:
 8004590:	b480      	push	{r7}
 8004592:	b083      	sub	sp, #12
 8004594:	af00      	add	r7, sp, #0
 8004596:	6078      	str	r0, [r7, #4]
 8004598:	687b      	ldr	r3, [r7, #4]
 800459a:	681b      	ldr	r3, [r3, #0]
 800459c:	f403 1300 	and.w	r3, r3, #2097152	; 0x200000
 80045a0:	f5b3 1f00 	cmp.w	r3, #2097152	; 0x200000
 80045a4:	bf0c      	ite	eq
 80045a6:	2301      	moveq	r3, #1
 80045a8:	2300      	movne	r3, #0
 80045aa:	b2db      	uxtb	r3, r3
 80045ac:	4618      	mov	r0, r3
 80045ae:	370c      	adds	r7, #12
 80045b0:	46bd      	mov	sp, r7
 80045b2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80045b6:	4770      	bx	lr

080045b8 <LL_DMA_ClearFlag_HT2>:
 80045b8:	b480      	push	{r7}
 80045ba:	b083      	sub	sp, #12
 80045bc:	af00      	add	r7, sp, #0
 80045be:	6078      	str	r0, [r7, #4]
 80045c0:	687b      	ldr	r3, [r7, #4]
 80045c2:	f44f 1280 	mov.w	r2, #1048576	; 0x100000
 80045c6:	609a      	str	r2, [r3, #8]
 80045c8:	bf00      	nop
 80045ca:	370c      	adds	r7, #12
 80045cc:	46bd      	mov	sp, r7
 80045ce:	f85d 7b04 	ldr.w	r7, [sp], #4
 80045d2:	4770      	bx	lr

080045d4 <LL_DMA_ClearFlag_TC2>:
 80045d4:	b480      	push	{r7}
 80045d6:	b083      	sub	sp, #12
 80045d8:	af00      	add	r7, sp, #0
 80045da:	6078      	str	r0, [r7, #4]
 80045dc:	687b      	ldr	r3, [r7, #4]
 80045de:	f44f 1200 	mov.w	r2, #2097152	; 0x200000
 80045e2:	609a      	str	r2, [r3, #8]
 80045e4:	bf00      	nop
 80045e6:	370c      	adds	r7, #12
 80045e8:	46bd      	mov	sp, r7
 80045ea:	f85d 7b04 	ldr.w	r7, [sp], #4
 80045ee:	4770      	bx	lr

080045f0 <LL_DMA_EnableIT_TC>:
 80045f0:	b480      	push	{r7}
 80045f2:	b083      	sub	sp, #12
 80045f4:	af00      	add	r7, sp, #0
 80045f6:	6078      	str	r0, [r7, #4]
 80045f8:	6039      	str	r1, [r7, #0]
 80045fa:	4a0c      	ldr	r2, [pc, #48]	; (800462c <LL_DMA_EnableIT_TC+0x3c>)
 80045fc:	683b      	ldr	r3, [r7, #0]
 80045fe:	4413      	add	r3, r2
 8004600:	781b      	ldrb	r3, [r3, #0]
 8004602:	461a      	mov	r2, r3
 8004604:	687b      	ldr	r3, [r7, #4]
 8004606:	4413      	add	r3, r2
 8004608:	4619      	mov	r1, r3
 800460a:	4a08      	ldr	r2, [pc, #32]	; (800462c <LL_DMA_EnableIT_TC+0x3c>)
 800460c:	683b      	ldr	r3, [r7, #0]
 800460e:	4413      	add	r3, r2
 8004610:	781b      	ldrb	r3, [r3, #0]
 8004612:	461a      	mov	r2, r3
 8004614:	687b      	ldr	r3, [r7, #4]
 8004616:	4413      	add	r3, r2
 8004618:	681b      	ldr	r3, [r3, #0]
 800461a:	f043 0310 	orr.w	r3, r3, #16
 800461e:	600b      	str	r3, [r1, #0]
 8004620:	bf00      	nop
 8004622:	370c      	adds	r7, #12
 8004624:	46bd      	mov	sp, r7
 8004626:	f85d 7b04 	ldr.w	r7, [sp], #4
 800462a:	4770      	bx	lr
 800462c:	08009d00 	.word	0x08009d00

08004630 <LL_GPIO_SetPinMode>:
 8004630:	b480      	push	{r7}
 8004632:	b089      	sub	sp, #36	; 0x24
 8004634:	af00      	add	r7, sp, #0
 8004636:	60f8      	str	r0, [r7, #12]
 8004638:	60b9      	str	r1, [r7, #8]
 800463a:	607a      	str	r2, [r7, #4]
 800463c:	68fb      	ldr	r3, [r7, #12]
 800463e:	681a      	ldr	r2, [r3, #0]
 8004640:	68bb      	ldr	r3, [r7, #8]
 8004642:	617b      	str	r3, [r7, #20]
 8004644:	697b      	ldr	r3, [r7, #20]
 8004646:	fa93 f3a3 	rbit	r3, r3
 800464a:	613b      	str	r3, [r7, #16]
 800464c:	693b      	ldr	r3, [r7, #16]
 800464e:	fab3 f383 	clz	r3, r3
 8004652:	005b      	lsls	r3, r3, #1
 8004654:	2103      	movs	r1, #3
 8004656:	fa01 f303 	lsl.w	r3, r1, r3
 800465a:	43db      	mvns	r3, r3
 800465c:	401a      	ands	r2, r3
 800465e:	68bb      	ldr	r3, [r7, #8]
 8004660:	61fb      	str	r3, [r7, #28]
 8004662:	69fb      	ldr	r3, [r7, #28]
 8004664:	fa93 f3a3 	rbit	r3, r3
 8004668:	61bb      	str	r3, [r7, #24]
 800466a:	69bb      	ldr	r3, [r7, #24]
 800466c:	fab3 f383 	clz	r3, r3
 8004670:	005b      	lsls	r3, r3, #1
 8004672:	6879      	ldr	r1, [r7, #4]
 8004674:	fa01 f303 	lsl.w	r3, r1, r3
 8004678:	431a      	orrs	r2, r3
 800467a:	68fb      	ldr	r3, [r7, #12]
 800467c:	601a      	str	r2, [r3, #0]
 800467e:	bf00      	nop
 8004680:	3724      	adds	r7, #36	; 0x24
 8004682:	46bd      	mov	sp, r7
 8004684:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004688:	4770      	bx	lr

0800468a <LL_GPIO_SetPinOutputType>:
 800468a:	b480      	push	{r7}
 800468c:	b085      	sub	sp, #20
 800468e:	af00      	add	r7, sp, #0
 8004690:	60f8      	str	r0, [r7, #12]
 8004692:	60b9      	str	r1, [r7, #8]
 8004694:	607a      	str	r2, [r7, #4]
 8004696:	68fb      	ldr	r3, [r7, #12]
 8004698:	685a      	ldr	r2, [r3, #4]
 800469a:	68bb      	ldr	r3, [r7, #8]
 800469c:	43db      	mvns	r3, r3
 800469e:	401a      	ands	r2, r3
 80046a0:	68bb      	ldr	r3, [r7, #8]
 80046a2:	6879      	ldr	r1, [r7, #4]
 80046a4:	fb01 f303 	mul.w	r3, r1, r3
 80046a8:	431a      	orrs	r2, r3
 80046aa:	68fb      	ldr	r3, [r7, #12]
 80046ac:	605a      	str	r2, [r3, #4]
 80046ae:	bf00      	nop
 80046b0:	3714      	adds	r7, #20
 80046b2:	46bd      	mov	sp, r7
 80046b4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80046b8:	4770      	bx	lr

080046ba <LL_GPIO_SetPinSpeed>:
 80046ba:	b480      	push	{r7}
 80046bc:	b089      	sub	sp, #36	; 0x24
 80046be:	af00      	add	r7, sp, #0
 80046c0:	60f8      	str	r0, [r7, #12]
 80046c2:	60b9      	str	r1, [r7, #8]
 80046c4:	607a      	str	r2, [r7, #4]
 80046c6:	68fb      	ldr	r3, [r7, #12]
 80046c8:	689a      	ldr	r2, [r3, #8]
 80046ca:	68bb      	ldr	r3, [r7, #8]
 80046cc:	617b      	str	r3, [r7, #20]
 80046ce:	697b      	ldr	r3, [r7, #20]
 80046d0:	fa93 f3a3 	rbit	r3, r3
 80046d4:	613b      	str	r3, [r7, #16]
 80046d6:	693b      	ldr	r3, [r7, #16]
 80046d8:	fab3 f383 	clz	r3, r3
 80046dc:	005b      	lsls	r3, r3, #1
 80046de:	2103      	movs	r1, #3
 80046e0:	fa01 f303 	lsl.w	r3, r1, r3
 80046e4:	43db      	mvns	r3, r3
 80046e6:	401a      	ands	r2, r3
 80046e8:	68bb      	ldr	r3, [r7, #8]
 80046ea:	61fb      	str	r3, [r7, #28]
 80046ec:	69fb      	ldr	r3, [r7, #28]
 80046ee:	fa93 f3a3 	rbit	r3, r3
 80046f2:	61bb      	str	r3, [r7, #24]
 80046f4:	69bb      	ldr	r3, [r7, #24]
 80046f6:	fab3 f383 	clz	r3, r3
 80046fa:	005b      	lsls	r3, r3, #1
 80046fc:	6879      	ldr	r1, [r7, #4]
 80046fe:	fa01 f303 	lsl.w	r3, r1, r3
 8004702:	431a      	orrs	r2, r3
 8004704:	68fb      	ldr	r3, [r7, #12]
 8004706:	609a      	str	r2, [r3, #8]
 8004708:	bf00      	nop
 800470a:	3724      	adds	r7, #36	; 0x24
 800470c:	46bd      	mov	sp, r7
 800470e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004712:	4770      	bx	lr

08004714 <LL_GPIO_SetPinPull>:
 8004714:	b480      	push	{r7}
 8004716:	b089      	sub	sp, #36	; 0x24
 8004718:	af00      	add	r7, sp, #0
 800471a:	60f8      	str	r0, [r7, #12]
 800471c:	60b9      	str	r1, [r7, #8]
 800471e:	607a      	str	r2, [r7, #4]
 8004720:	68fb      	ldr	r3, [r7, #12]
 8004722:	68da      	ldr	r2, [r3, #12]
 8004724:	68bb      	ldr	r3, [r7, #8]
 8004726:	617b      	str	r3, [r7, #20]
 8004728:	697b      	ldr	r3, [r7, #20]
 800472a:	fa93 f3a3 	rbit	r3, r3
 800472e:	613b      	str	r3, [r7, #16]
 8004730:	693b      	ldr	r3, [r7, #16]
 8004732:	fab3 f383 	clz	r3, r3
 8004736:	005b      	lsls	r3, r3, #1
 8004738:	2103      	movs	r1, #3
 800473a:	fa01 f303 	lsl.w	r3, r1, r3
 800473e:	43db      	mvns	r3, r3
 8004740:	401a      	ands	r2, r3
 8004742:	68bb      	ldr	r3, [r7, #8]
 8004744:	61fb      	str	r3, [r7, #28]
 8004746:	69fb      	ldr	r3, [r7, #28]
 8004748:	fa93 f3a3 	rbit	r3, r3
 800474c:	61bb      	str	r3, [r7, #24]
 800474e:	69bb      	ldr	r3, [r7, #24]
 8004750:	fab3 f383 	clz	r3, r3
 8004754:	005b      	lsls	r3, r3, #1
 8004756:	6879      	ldr	r1, [r7, #4]
 8004758:	fa01 f303 	lsl.w	r3, r1, r3
 800475c:	431a      	orrs	r2, r3
 800475e:	68fb      	ldr	r3, [r7, #12]
 8004760:	60da      	str	r2, [r3, #12]
 8004762:	bf00      	nop
 8004764:	3724      	adds	r7, #36	; 0x24
 8004766:	46bd      	mov	sp, r7
 8004768:	f85d 7b04 	ldr.w	r7, [sp], #4
 800476c:	4770      	bx	lr

0800476e <LL_GPIO_SetAFPin_0_7>:
 800476e:	b480      	push	{r7}
 8004770:	b089      	sub	sp, #36	; 0x24
 8004772:	af00      	add	r7, sp, #0
 8004774:	60f8      	str	r0, [r7, #12]
 8004776:	60b9      	str	r1, [r7, #8]
 8004778:	607a      	str	r2, [r7, #4]
 800477a:	68fb      	ldr	r3, [r7, #12]
 800477c:	6a1a      	ldr	r2, [r3, #32]
 800477e:	68bb      	ldr	r3, [r7, #8]
 8004780:	617b      	str	r3, [r7, #20]
 8004782:	697b      	ldr	r3, [r7, #20]
 8004784:	fa93 f3a3 	rbit	r3, r3
 8004788:	613b      	str	r3, [r7, #16]
 800478a:	693b      	ldr	r3, [r7, #16]
 800478c:	fab3 f383 	clz	r3, r3
 8004790:	009b      	lsls	r3, r3, #2
 8004792:	210f      	movs	r1, #15
 8004794:	fa01 f303 	lsl.w	r3, r1, r3
 8004798:	43db      	mvns	r3, r3
 800479a:	401a      	ands	r2, r3
 800479c:	68bb      	ldr	r3, [r7, #8]
 800479e:	61fb      	str	r3, [r7, #28]
 80047a0:	69fb      	ldr	r3, [r7, #28]
 80047a2:	fa93 f3a3 	rbit	r3, r3
 80047a6:	61bb      	str	r3, [r7, #24]
 80047a8:	69bb      	ldr	r3, [r7, #24]
 80047aa:	fab3 f383 	clz	r3, r3
 80047ae:	009b      	lsls	r3, r3, #2
 80047b0:	6879      	ldr	r1, [r7, #4]
 80047b2:	fa01 f303 	lsl.w	r3, r1, r3
 80047b6:	431a      	orrs	r2, r3
 80047b8:	68fb      	ldr	r3, [r7, #12]
 80047ba:	621a      	str	r2, [r3, #32]
 80047bc:	bf00      	nop
 80047be:	3724      	adds	r7, #36	; 0x24
 80047c0:	46bd      	mov	sp, r7
 80047c2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80047c6:	4770      	bx	lr

080047c8 <terminal_hw_config>:
 80047c8:	b580      	push	{r7, lr}
 80047ca:	b082      	sub	sp, #8
 80047cc:	af02      	add	r7, sp, #8
 80047ce:	2002      	movs	r0, #2
 80047d0:	f7ff fdc6 	bl	8004360 <LL_AHB1_GRP1_EnableClock>
 80047d4:	2207      	movs	r2, #7
 80047d6:	2140      	movs	r1, #64	; 0x40
 80047d8:	484c      	ldr	r0, [pc, #304]	; (800490c <terminal_hw_config+0x144>)
 80047da:	f7ff ffc8 	bl	800476e <LL_GPIO_SetAFPin_0_7>
 80047de:	2202      	movs	r2, #2
 80047e0:	2140      	movs	r1, #64	; 0x40
 80047e2:	484a      	ldr	r0, [pc, #296]	; (800490c <terminal_hw_config+0x144>)
 80047e4:	f7ff ff24 	bl	8004630 <LL_GPIO_SetPinMode>
 80047e8:	2200      	movs	r2, #0
 80047ea:	2140      	movs	r1, #64	; 0x40
 80047ec:	4847      	ldr	r0, [pc, #284]	; (800490c <terminal_hw_config+0x144>)
 80047ee:	f7ff ff4c 	bl	800468a <LL_GPIO_SetPinOutputType>
 80047f2:	2200      	movs	r2, #0
 80047f4:	2140      	movs	r1, #64	; 0x40
 80047f6:	4845      	ldr	r0, [pc, #276]	; (800490c <terminal_hw_config+0x144>)
 80047f8:	f7ff ff8c 	bl	8004714 <LL_GPIO_SetPinPull>
 80047fc:	2202      	movs	r2, #2
 80047fe:	2140      	movs	r1, #64	; 0x40
 8004800:	4842      	ldr	r0, [pc, #264]	; (800490c <terminal_hw_config+0x144>)
 8004802:	f7ff ff5a 	bl	80046ba <LL_GPIO_SetPinSpeed>
 8004806:	2207      	movs	r2, #7
 8004808:	2180      	movs	r1, #128	; 0x80
 800480a:	4840      	ldr	r0, [pc, #256]	; (800490c <terminal_hw_config+0x144>)
 800480c:	f7ff ffaf 	bl	800476e <LL_GPIO_SetAFPin_0_7>
 8004810:	2202      	movs	r2, #2
 8004812:	2180      	movs	r1, #128	; 0x80
 8004814:	483d      	ldr	r0, [pc, #244]	; (800490c <terminal_hw_config+0x144>)
 8004816:	f7ff ff0b 	bl	8004630 <LL_GPIO_SetPinMode>
 800481a:	2200      	movs	r2, #0
 800481c:	2180      	movs	r1, #128	; 0x80
 800481e:	483b      	ldr	r0, [pc, #236]	; (800490c <terminal_hw_config+0x144>)
 8004820:	f7ff ff33 	bl	800468a <LL_GPIO_SetPinOutputType>
 8004824:	2200      	movs	r2, #0
 8004826:	2180      	movs	r1, #128	; 0x80
 8004828:	4838      	ldr	r0, [pc, #224]	; (800490c <terminal_hw_config+0x144>)
 800482a:	f7ff ff73 	bl	8004714 <LL_GPIO_SetPinPull>
 800482e:	2202      	movs	r2, #2
 8004830:	2180      	movs	r1, #128	; 0x80
 8004832:	4836      	ldr	r0, [pc, #216]	; (800490c <terminal_hw_config+0x144>)
 8004834:	f7ff ff41 	bl	80046ba <LL_GPIO_SetPinSpeed>
 8004838:	f44f 0080 	mov.w	r0, #4194304	; 0x400000
 800483c:	f7ff fd90 	bl	8004360 <LL_AHB1_GRP1_EnableClock>
 8004840:	2010      	movs	r0, #16
 8004842:	f7ff fda5 	bl	8004390 <LL_APB2_GRP1_EnableClock>
 8004846:	210c      	movs	r1, #12
 8004848:	4831      	ldr	r0, [pc, #196]	; (8004910 <terminal_hw_config+0x148>)
 800484a:	f7ff fbfd 	bl	8004048 <LL_USART_SetTransferDirection>
 800484e:	2100      	movs	r1, #0
 8004850:	482f      	ldr	r0, [pc, #188]	; (8004910 <terminal_hw_config+0x148>)
 8004852:	f7ff fc0c 	bl	800406e <LL_USART_SetParity>
 8004856:	2100      	movs	r1, #0
 8004858:	482d      	ldr	r0, [pc, #180]	; (8004910 <terminal_hw_config+0x148>)
 800485a:	f7ff fc1b 	bl	8004094 <LL_USART_SetDataWidth>
 800485e:	2100      	movs	r1, #0
 8004860:	482b      	ldr	r0, [pc, #172]	; (8004910 <terminal_hw_config+0x148>)
 8004862:	f7ff fc2a 	bl	80040ba <LL_USART_SetStopBitsLength>
 8004866:	2100      	movs	r1, #0
 8004868:	4829      	ldr	r0, [pc, #164]	; (8004910 <terminal_hw_config+0x148>)
 800486a:	f7ff fc39 	bl	80040e0 <LL_USART_SetHWFlowCtrl>
 800486e:	4b29      	ldr	r3, [pc, #164]	; (8004914 <terminal_hw_config+0x14c>)
 8004870:	681b      	ldr	r3, [r3, #0]
 8004872:	0859      	lsrs	r1, r3, #1
 8004874:	f44f 33e1 	mov.w	r3, #115200	; 0x1c200
 8004878:	2200      	movs	r2, #0
 800487a:	4825      	ldr	r0, [pc, #148]	; (8004910 <terminal_hw_config+0x148>)
 800487c:	f7ff fc44 	bl	8004108 <LL_USART_SetBaudRate>
 8004880:	4823      	ldr	r0, [pc, #140]	; (8004910 <terminal_hw_config+0x148>)
 8004882:	f7ff fbc1 	bl	8004008 <LL_USART_EnableDirectionRx>
 8004886:	4822      	ldr	r0, [pc, #136]	; (8004910 <terminal_hw_config+0x148>)
 8004888:	f7ff fbce 	bl	8004028 <LL_USART_EnableDirectionTx>
 800488c:	4820      	ldr	r0, [pc, #128]	; (8004910 <terminal_hw_config+0x148>)
 800488e:	f7ff fd47 	bl	8004320 <LL_USART_EnableDMAReq_RX>
 8004892:	481f      	ldr	r0, [pc, #124]	; (8004910 <terminal_hw_config+0x148>)
 8004894:	f7ff fd34 	bl	8004300 <LL_USART_EnableIT_IDLE>
 8004898:	481d      	ldr	r0, [pc, #116]	; (8004910 <terminal_hw_config+0x148>)
 800489a:	f7ff fba5 	bl	8003fe8 <LL_USART_Enable>
 800489e:	2101      	movs	r1, #1
 80048a0:	2025      	movs	r0, #37	; 0x25
 80048a2:	f7ff fb77 	bl	8003f94 <NVIC_SetPriority>
 80048a6:	2025      	movs	r0, #37	; 0x25
 80048a8:	f7ff fb5a 	bl	8003f60 <NVIC_EnableIRQ>
 80048ac:	f04f 6200 	mov.w	r2, #134217728	; 0x8000000
 80048b0:	2102      	movs	r1, #2
 80048b2:	4819      	ldr	r0, [pc, #100]	; (8004918 <terminal_hw_config+0x150>)
 80048b4:	f7ff fe0c 	bl	80044d0 <LL_DMA_SetChannelSelection>
 80048b8:	4b18      	ldr	r3, [pc, #96]	; (800491c <terminal_hw_config+0x154>)
 80048ba:	681b      	ldr	r3, [r3, #0]
 80048bc:	68db      	ldr	r3, [r3, #12]
 80048be:	461a      	mov	r2, r3
 80048c0:	2300      	movs	r3, #0
 80048c2:	9300      	str	r3, [sp, #0]
 80048c4:	4613      	mov	r3, r2
 80048c6:	4a16      	ldr	r2, [pc, #88]	; (8004920 <terminal_hw_config+0x158>)
 80048c8:	2102      	movs	r1, #2
 80048ca:	4813      	ldr	r0, [pc, #76]	; (8004918 <terminal_hw_config+0x150>)
 80048cc:	f7ff fe24 	bl	8004518 <LL_DMA_ConfigAddresses>
 80048d0:	f44f 7280 	mov.w	r2, #256	; 0x100
 80048d4:	2102      	movs	r1, #2
 80048d6:	4810      	ldr	r0, [pc, #64]	; (8004918 <terminal_hw_config+0x150>)
 80048d8:	f7ff fdd6 	bl	8004488 <LL_DMA_SetDataLength>
 80048dc:	f44f 6280 	mov.w	r2, #1024	; 0x400
 80048e0:	2102      	movs	r1, #2
 80048e2:	480d      	ldr	r0, [pc, #52]	; (8004918 <terminal_hw_config+0x150>)
 80048e4:	f7ff fdac 	bl	8004440 <LL_DMA_SetMemoryIncMode>
 80048e8:	2102      	movs	r1, #2
 80048ea:	480b      	ldr	r0, [pc, #44]	; (8004918 <terminal_hw_config+0x150>)
 80048ec:	f7ff fd68 	bl	80043c0 <LL_DMA_EnableStream>
 80048f0:	2102      	movs	r1, #2
 80048f2:	4809      	ldr	r0, [pc, #36]	; (8004918 <terminal_hw_config+0x150>)
 80048f4:	f7ff fe7c 	bl	80045f0 <LL_DMA_EnableIT_TC>
 80048f8:	2106      	movs	r1, #6
 80048fa:	203a      	movs	r0, #58	; 0x3a
 80048fc:	f7ff fb4a 	bl	8003f94 <NVIC_SetPriority>
 8004900:	203a      	movs	r0, #58	; 0x3a
 8004902:	f7ff fb2d 	bl	8003f60 <NVIC_EnableIRQ>
 8004906:	bf00      	nop
 8004908:	46bd      	mov	sp, r7
 800490a:	bd80      	pop	{r7, pc}
 800490c:	40020400 	.word	0x40020400
 8004910:	40011000 	.word	0x40011000
 8004914:	200008b0 	.word	0x200008b0
 8004918:	40026400 	.word	0x40026400
 800491c:	20001208 	.word	0x20001208
 8004920:	40011004 	.word	0x40011004

08004924 <merge>:
 8004924:	b480      	push	{r7}
 8004926:	b083      	sub	sp, #12
 8004928:	af00      	add	r7, sp, #0
 800492a:	4603      	mov	r3, r0
 800492c:	603a      	str	r2, [r7, #0]
 800492e:	71fb      	strb	r3, [r7, #7]
 8004930:	460b      	mov	r3, r1
 8004932:	71bb      	strb	r3, [r7, #6]
 8004934:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8004938:	f003 030f 	and.w	r3, r3, #15
 800493c:	b25a      	sxtb	r2, r3
 800493e:	f997 3006 	ldrsb.w	r3, [r7, #6]
 8004942:	f023 030f 	bic.w	r3, r3, #15
 8004946:	b25b      	sxtb	r3, r3
 8004948:	4313      	orrs	r3, r2
 800494a:	b25b      	sxtb	r3, r3
 800494c:	b2da      	uxtb	r2, r3
 800494e:	683b      	ldr	r3, [r7, #0]
 8004950:	701a      	strb	r2, [r3, #0]
 8004952:	bf00      	nop
 8004954:	370c      	adds	r7, #12
 8004956:	46bd      	mov	sp, r7
 8004958:	f85d 7b04 	ldr.w	r7, [sp], #4
 800495c:	4770      	bx	lr

0800495e <decode>:
 800495e:	b580      	push	{r7, lr}
 8004960:	b086      	sub	sp, #24
 8004962:	af00      	add	r7, sp, #0
 8004964:	60f8      	str	r0, [r7, #12]
 8004966:	60b9      	str	r1, [r7, #8]
 8004968:	607a      	str	r2, [r7, #4]
 800496a:	2300      	movs	r3, #0
 800496c:	617b      	str	r3, [r7, #20]
 800496e:	e015      	b.n	800499c <decode+0x3e>
 8004970:	68fb      	ldr	r3, [r7, #12]
 8004972:	7818      	ldrb	r0, [r3, #0]
 8004974:	68fb      	ldr	r3, [r7, #12]
 8004976:	3301      	adds	r3, #1
 8004978:	781b      	ldrb	r3, [r3, #0]
 800497a:	f107 0213 	add.w	r2, r7, #19
 800497e:	4619      	mov	r1, r3
 8004980:	f7ff ffd0 	bl	8004924 <merge>
 8004984:	7cfa      	ldrb	r2, [r7, #19]
 8004986:	68bb      	ldr	r3, [r7, #8]
 8004988:	701a      	strb	r2, [r3, #0]
 800498a:	68bb      	ldr	r3, [r7, #8]
 800498c:	3301      	adds	r3, #1
 800498e:	60bb      	str	r3, [r7, #8]
 8004990:	68fb      	ldr	r3, [r7, #12]
 8004992:	3302      	adds	r3, #2
 8004994:	60fb      	str	r3, [r7, #12]
 8004996:	697b      	ldr	r3, [r7, #20]
 8004998:	3301      	adds	r3, #1
 800499a:	617b      	str	r3, [r7, #20]
 800499c:	697a      	ldr	r2, [r7, #20]
 800499e:	687b      	ldr	r3, [r7, #4]
 80049a0:	429a      	cmp	r2, r3
 80049a2:	d3e5      	bcc.n	8004970 <decode+0x12>
 80049a4:	bf00      	nop
 80049a6:	3718      	adds	r7, #24
 80049a8:	46bd      	mov	sp, r7
 80049aa:	bd80      	pop	{r7, pc}

080049ac <term_response>:
 80049ac:	b580      	push	{r7, lr}
 80049ae:	b084      	sub	sp, #16
 80049b0:	af00      	add	r7, sp, #0
 80049b2:	6078      	str	r0, [r7, #4]
 80049b4:	6039      	str	r1, [r7, #0]
 80049b6:	2300      	movs	r3, #0
 80049b8:	60fb      	str	r3, [r7, #12]
 80049ba:	687b      	ldr	r3, [r7, #4]
 80049bc:	681b      	ldr	r3, [r3, #0]
 80049be:	4618      	mov	r0, r3
 80049c0:	f7ff fc90 	bl	80042e4 <LL_USART_ClearFlag_TC>
 80049c4:	e01b      	b.n	80049fe <term_response+0x52>
 80049c6:	4b1b      	ldr	r3, [pc, #108]	; (8004a34 <term_response+0x88>)
 80049c8:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80049cc:	601a      	str	r2, [r3, #0]
 80049ce:	f3bf 8f4f 	dsb	sy
 80049d2:	f3bf 8f6f 	isb	sy
 80049d6:	687b      	ldr	r3, [r7, #4]
 80049d8:	681b      	ldr	r3, [r3, #0]
 80049da:	4618      	mov	r0, r3
 80049dc:	f7ff fc5d 	bl	800429a <LL_USART_IsActiveFlag_TXE>
 80049e0:	4603      	mov	r3, r0
 80049e2:	2b00      	cmp	r3, #0
 80049e4:	d0ef      	beq.n	80049c6 <term_response+0x1a>
 80049e6:	687b      	ldr	r3, [r7, #4]
 80049e8:	6818      	ldr	r0, [r3, #0]
 80049ea:	687b      	ldr	r3, [r7, #4]
 80049ec:	695a      	ldr	r2, [r3, #20]
 80049ee:	68fb      	ldr	r3, [r7, #12]
 80049f0:	1c59      	adds	r1, r3, #1
 80049f2:	60f9      	str	r1, [r7, #12]
 80049f4:	4413      	add	r3, r2
 80049f6:	781b      	ldrb	r3, [r3, #0]
 80049f8:	4619      	mov	r1, r3
 80049fa:	f7ff fca1 	bl	8004340 <LL_USART_TransmitData8>
 80049fe:	683b      	ldr	r3, [r7, #0]
 8004a00:	1e5a      	subs	r2, r3, #1
 8004a02:	603a      	str	r2, [r7, #0]
 8004a04:	2b00      	cmp	r3, #0
 8004a06:	d1e6      	bne.n	80049d6 <term_response+0x2a>
 8004a08:	e007      	b.n	8004a1a <term_response+0x6e>
 8004a0a:	4b0a      	ldr	r3, [pc, #40]	; (8004a34 <term_response+0x88>)
 8004a0c:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8004a10:	601a      	str	r2, [r3, #0]
 8004a12:	f3bf 8f4f 	dsb	sy
 8004a16:	f3bf 8f6f 	isb	sy
 8004a1a:	687b      	ldr	r3, [r7, #4]
 8004a1c:	681b      	ldr	r3, [r3, #0]
 8004a1e:	4618      	mov	r0, r3
 8004a20:	f7ff fc28 	bl	8004274 <LL_USART_IsActiveFlag_TC>
 8004a24:	4603      	mov	r3, r0
 8004a26:	2b00      	cmp	r3, #0
 8004a28:	d0ef      	beq.n	8004a0a <term_response+0x5e>
 8004a2a:	bf00      	nop
 8004a2c:	3710      	adds	r7, #16
 8004a2e:	46bd      	mov	sp, r7
 8004a30:	bd80      	pop	{r7, pc}
 8004a32:	bf00      	nop
 8004a34:	e000ed04 	.word	0xe000ed04

08004a38 <term_request>:
 8004a38:	b580      	push	{r7, lr}
 8004a3a:	b082      	sub	sp, #8
 8004a3c:	af00      	add	r7, sp, #0
 8004a3e:	6078      	str	r0, [r7, #4]
 8004a40:	f04f 32ff 	mov.w	r2, #4294967295
 8004a44:	2101      	movs	r1, #1
 8004a46:	2000      	movs	r0, #0
 8004a48:	f003 fd7e 	bl	8008548 <ulTaskGenericNotifyTake>
 8004a4c:	4603      	mov	r3, r0
 8004a4e:	2b00      	cmp	r3, #0
 8004a50:	d010      	beq.n	8004a74 <term_request+0x3c>
 8004a52:	687b      	ldr	r3, [r7, #4]
 8004a54:	68d8      	ldr	r0, [r3, #12]
 8004a56:	687b      	ldr	r3, [r7, #4]
 8004a58:	691b      	ldr	r3, [r3, #16]
 8004a5a:	2280      	movs	r2, #128	; 0x80
 8004a5c:	4619      	mov	r1, r3
 8004a5e:	f7ff ff7e 	bl	800495e <decode>
 8004a62:	687b      	ldr	r3, [r7, #4]
 8004a64:	691b      	ldr	r3, [r3, #16]
 8004a66:	1c5a      	adds	r2, r3, #1
 8004a68:	687b      	ldr	r3, [r7, #4]
 8004a6a:	615a      	str	r2, [r3, #20]
 8004a6c:	687b      	ldr	r3, [r7, #4]
 8004a6e:	691b      	ldr	r3, [r3, #16]
 8004a70:	781b      	ldrb	r3, [r3, #0]
 8004a72:	e000      	b.n	8004a76 <term_request+0x3e>
 8004a74:	2300      	movs	r3, #0
 8004a76:	4618      	mov	r0, r3
 8004a78:	3708      	adds	r7, #8
 8004a7a:	46bd      	mov	sp, r7
 8004a7c:	bd80      	pop	{r7, pc}
	...

08004a80 <terminal_manager>:
 8004a80:	b580      	push	{r7, lr}
 8004a82:	b08c      	sub	sp, #48	; 0x30
 8004a84:	af00      	add	r7, sp, #0
 8004a86:	6078      	str	r0, [r7, #4]
 8004a88:	2300      	movs	r3, #0
 8004a8a:	62fb      	str	r3, [r7, #44]	; 0x2c
 8004a8c:	2300      	movs	r3, #0
 8004a8e:	62bb      	str	r3, [r7, #40]	; 0x28
 8004a90:	4b28      	ldr	r3, [pc, #160]	; (8004b34 <terminal_manager+0xb4>)
 8004a92:	60bb      	str	r3, [r7, #8]
 8004a94:	f44f 7080 	mov.w	r0, #256	; 0x100
 8004a98:	f7fb fe1c 	bl	80006d4 <malloc>
 8004a9c:	4603      	mov	r3, r0
 8004a9e:	617b      	str	r3, [r7, #20]
 8004aa0:	2080      	movs	r0, #128	; 0x80
 8004aa2:	f7fb fe17 	bl	80006d4 <malloc>
 8004aa6:	4603      	mov	r3, r0
 8004aa8:	61bb      	str	r3, [r7, #24]
 8004aaa:	2080      	movs	r0, #128	; 0x80
 8004aac:	f7fb fe12 	bl	80006d4 <malloc>
 8004ab0:	4603      	mov	r3, r0
 8004ab2:	61fb      	str	r3, [r7, #28]
 8004ab4:	69fb      	ldr	r3, [r7, #28]
 8004ab6:	623b      	str	r3, [r7, #32]
 8004ab8:	f003 fca0 	bl	80083fc <xTaskGetCurrentTaskHandle>
 8004abc:	4603      	mov	r3, r0
 8004abe:	627b      	str	r3, [r7, #36]	; 0x24
 8004ac0:	4a1d      	ldr	r2, [pc, #116]	; (8004b38 <terminal_manager+0xb8>)
 8004ac2:	f107 0308 	add.w	r3, r7, #8
 8004ac6:	6013      	str	r3, [r2, #0]
 8004ac8:	f7ff fe7e 	bl	80047c8 <terminal_hw_config>
 8004acc:	f107 0308 	add.w	r3, r7, #8
 8004ad0:	4618      	mov	r0, r3
 8004ad2:	f7ff ffb1 	bl	8004a38 <term_request>
 8004ad6:	62f8      	str	r0, [r7, #44]	; 0x2c
 8004ad8:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8004ada:	2b00      	cmp	r3, #0
 8004adc:	dd08      	ble.n	8004af0 <terminal_manager+0x70>
 8004ade:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8004ae0:	2b12      	cmp	r3, #18
 8004ae2:	dc05      	bgt.n	8004af0 <terminal_manager+0x70>
 8004ae4:	4a15      	ldr	r2, [pc, #84]	; (8004b3c <terminal_manager+0xbc>)
 8004ae6:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8004ae8:	f852 3023 	ldr.w	r3, [r2, r3, lsl #2]
 8004aec:	2b00      	cmp	r3, #0
 8004aee:	d111      	bne.n	8004b14 <terminal_manager+0x94>
 8004af0:	69fb      	ldr	r3, [r7, #28]
 8004af2:	2261      	movs	r2, #97	; 0x61
 8004af4:	701a      	strb	r2, [r3, #0]
 8004af6:	69fb      	ldr	r3, [r7, #28]
 8004af8:	3301      	adds	r3, #1
 8004afa:	2262      	movs	r2, #98	; 0x62
 8004afc:	701a      	strb	r2, [r3, #0]
 8004afe:	69fb      	ldr	r3, [r7, #28]
 8004b00:	3302      	adds	r3, #2
 8004b02:	2263      	movs	r2, #99	; 0x63
 8004b04:	701a      	strb	r2, [r3, #0]
 8004b06:	f107 0308 	add.w	r3, r7, #8
 8004b0a:	2103      	movs	r1, #3
 8004b0c:	4618      	mov	r0, r3
 8004b0e:	f7ff ff4d 	bl	80049ac <term_response>
 8004b12:	e00d      	b.n	8004b30 <terminal_manager+0xb0>
 8004b14:	4a09      	ldr	r2, [pc, #36]	; (8004b3c <terminal_manager+0xbc>)
 8004b16:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8004b18:	f852 3023 	ldr.w	r3, [r2, r3, lsl #2]
 8004b1c:	69fa      	ldr	r2, [r7, #28]
 8004b1e:	4610      	mov	r0, r2
 8004b20:	4798      	blx	r3
 8004b22:	62b8      	str	r0, [r7, #40]	; 0x28
 8004b24:	f107 0308 	add.w	r3, r7, #8
 8004b28:	6ab9      	ldr	r1, [r7, #40]	; 0x28
 8004b2a:	4618      	mov	r0, r3
 8004b2c:	f7ff ff3e 	bl	80049ac <term_response>
 8004b30:	e7cc      	b.n	8004acc <terminal_manager+0x4c>
 8004b32:	bf00      	nop
 8004b34:	40011000 	.word	0x40011000
 8004b38:	20001208 	.word	0x20001208
 8004b3c:	08009d08 	.word	0x08009d08

08004b40 <USART1_IRQHandler>:
 8004b40:	b580      	push	{r7, lr}
 8004b42:	b082      	sub	sp, #8
 8004b44:	af00      	add	r7, sp, #0
 8004b46:	2300      	movs	r3, #0
 8004b48:	607b      	str	r3, [r7, #4]
 8004b4a:	480b      	ldr	r0, [pc, #44]	; (8004b78 <USART1_IRQHandler+0x38>)
 8004b4c:	f7ff fbb8 	bl	80042c0 <LL_USART_ClearFlag_IDLE>
 8004b50:	2102      	movs	r1, #2
 8004b52:	480a      	ldr	r0, [pc, #40]	; (8004b7c <USART1_IRQHandler+0x3c>)
 8004b54:	f7ff fc54 	bl	8004400 <LL_DMA_DisableStream>
 8004b58:	687b      	ldr	r3, [r7, #4]
 8004b5a:	2b00      	cmp	r3, #0
 8004b5c:	d007      	beq.n	8004b6e <USART1_IRQHandler+0x2e>
 8004b5e:	4b08      	ldr	r3, [pc, #32]	; (8004b80 <USART1_IRQHandler+0x40>)
 8004b60:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8004b64:	601a      	str	r2, [r3, #0]
 8004b66:	f3bf 8f4f 	dsb	sy
 8004b6a:	f3bf 8f6f 	isb	sy
 8004b6e:	bf00      	nop
 8004b70:	3708      	adds	r7, #8
 8004b72:	46bd      	mov	sp, r7
 8004b74:	bd80      	pop	{r7, pc}
 8004b76:	bf00      	nop
 8004b78:	40011000 	.word	0x40011000
 8004b7c:	40026400 	.word	0x40026400
 8004b80:	e000ed04 	.word	0xe000ed04

08004b84 <DMA2_Stream2_IRQHandler>:
 8004b84:	b580      	push	{r7, lr}
 8004b86:	b082      	sub	sp, #8
 8004b88:	af00      	add	r7, sp, #0
 8004b8a:	2300      	movs	r3, #0
 8004b8c:	607b      	str	r3, [r7, #4]
 8004b8e:	4813      	ldr	r0, [pc, #76]	; (8004bdc <DMA2_Stream2_IRQHandler+0x58>)
 8004b90:	f7ff fcfe 	bl	8004590 <LL_DMA_IsActiveFlag_TC2>
 8004b94:	4603      	mov	r3, r0
 8004b96:	2b00      	cmp	r3, #0
 8004b98:	d011      	beq.n	8004bbe <DMA2_Stream2_IRQHandler+0x3a>
 8004b9a:	4810      	ldr	r0, [pc, #64]	; (8004bdc <DMA2_Stream2_IRQHandler+0x58>)
 8004b9c:	f7ff fd1a 	bl	80045d4 <LL_DMA_ClearFlag_TC2>
 8004ba0:	480e      	ldr	r0, [pc, #56]	; (8004bdc <DMA2_Stream2_IRQHandler+0x58>)
 8004ba2:	f7ff fd09 	bl	80045b8 <LL_DMA_ClearFlag_HT2>
 8004ba6:	2102      	movs	r1, #2
 8004ba8:	480c      	ldr	r0, [pc, #48]	; (8004bdc <DMA2_Stream2_IRQHandler+0x58>)
 8004baa:	f7ff fc09 	bl	80043c0 <LL_DMA_EnableStream>
 8004bae:	4b0c      	ldr	r3, [pc, #48]	; (8004be0 <DMA2_Stream2_IRQHandler+0x5c>)
 8004bb0:	681b      	ldr	r3, [r3, #0]
 8004bb2:	69db      	ldr	r3, [r3, #28]
 8004bb4:	1d3a      	adds	r2, r7, #4
 8004bb6:	2100      	movs	r1, #0
 8004bb8:	4618      	mov	r0, r3
 8004bba:	f003 fd31 	bl	8008620 <vTaskGenericNotifyGiveFromISR>
 8004bbe:	687b      	ldr	r3, [r7, #4]
 8004bc0:	2b00      	cmp	r3, #0
 8004bc2:	d007      	beq.n	8004bd4 <DMA2_Stream2_IRQHandler+0x50>
 8004bc4:	4b07      	ldr	r3, [pc, #28]	; (8004be4 <DMA2_Stream2_IRQHandler+0x60>)
 8004bc6:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8004bca:	601a      	str	r2, [r3, #0]
 8004bcc:	f3bf 8f4f 	dsb	sy
 8004bd0:	f3bf 8f6f 	isb	sy
 8004bd4:	bf00      	nop
 8004bd6:	3708      	adds	r7, #8
 8004bd8:	46bd      	mov	sp, r7
 8004bda:	bd80      	pop	{r7, pc}
 8004bdc:	40026400 	.word	0x40026400
 8004be0:	20001208 	.word	0x20001208
 8004be4:	e000ed04 	.word	0xe000ed04

08004be8 <cmd_echo_handler>:
 8004be8:	b480      	push	{r7}
 8004bea:	b083      	sub	sp, #12
 8004bec:	af00      	add	r7, sp, #0
 8004bee:	6078      	str	r0, [r7, #4]
 8004bf0:	2304      	movs	r3, #4
 8004bf2:	4618      	mov	r0, r3
 8004bf4:	370c      	adds	r7, #12
 8004bf6:	46bd      	mov	sp, r7
 8004bf8:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004bfc:	4770      	bx	lr
	...

08004c00 <NVIC_EnableIRQ>:
 8004c00:	b480      	push	{r7}
 8004c02:	b083      	sub	sp, #12
 8004c04:	af00      	add	r7, sp, #0
 8004c06:	4603      	mov	r3, r0
 8004c08:	71fb      	strb	r3, [r7, #7]
 8004c0a:	4909      	ldr	r1, [pc, #36]	; (8004c30 <NVIC_EnableIRQ+0x30>)
 8004c0c:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8004c10:	095b      	lsrs	r3, r3, #5
 8004c12:	79fa      	ldrb	r2, [r7, #7]
 8004c14:	f002 021f 	and.w	r2, r2, #31
 8004c18:	2001      	movs	r0, #1
 8004c1a:	fa00 f202 	lsl.w	r2, r0, r2
 8004c1e:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 8004c22:	bf00      	nop
 8004c24:	370c      	adds	r7, #12
 8004c26:	46bd      	mov	sp, r7
 8004c28:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004c2c:	4770      	bx	lr
 8004c2e:	bf00      	nop
 8004c30:	e000e100 	.word	0xe000e100

08004c34 <NVIC_SetPriority>:
 8004c34:	b480      	push	{r7}
 8004c36:	b083      	sub	sp, #12
 8004c38:	af00      	add	r7, sp, #0
 8004c3a:	4603      	mov	r3, r0
 8004c3c:	6039      	str	r1, [r7, #0]
 8004c3e:	71fb      	strb	r3, [r7, #7]
 8004c40:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8004c44:	2b00      	cmp	r3, #0
 8004c46:	da0b      	bge.n	8004c60 <NVIC_SetPriority+0x2c>
 8004c48:	490d      	ldr	r1, [pc, #52]	; (8004c80 <NVIC_SetPriority+0x4c>)
 8004c4a:	79fb      	ldrb	r3, [r7, #7]
 8004c4c:	f003 030f 	and.w	r3, r3, #15
 8004c50:	3b04      	subs	r3, #4
 8004c52:	683a      	ldr	r2, [r7, #0]
 8004c54:	b2d2      	uxtb	r2, r2
 8004c56:	0112      	lsls	r2, r2, #4
 8004c58:	b2d2      	uxtb	r2, r2
 8004c5a:	440b      	add	r3, r1
 8004c5c:	761a      	strb	r2, [r3, #24]
 8004c5e:	e009      	b.n	8004c74 <NVIC_SetPriority+0x40>
 8004c60:	4908      	ldr	r1, [pc, #32]	; (8004c84 <NVIC_SetPriority+0x50>)
 8004c62:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8004c66:	683a      	ldr	r2, [r7, #0]
 8004c68:	b2d2      	uxtb	r2, r2
 8004c6a:	0112      	lsls	r2, r2, #4
 8004c6c:	b2d2      	uxtb	r2, r2
 8004c6e:	440b      	add	r3, r1
 8004c70:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8004c74:	bf00      	nop
 8004c76:	370c      	adds	r7, #12
 8004c78:	46bd      	mov	sp, r7
 8004c7a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004c7e:	4770      	bx	lr
 8004c80:	e000ed00 	.word	0xe000ed00
 8004c84:	e000e100 	.word	0xe000e100

08004c88 <LL_USART_Enable>:
 8004c88:	b480      	push	{r7}
 8004c8a:	b083      	sub	sp, #12
 8004c8c:	af00      	add	r7, sp, #0
 8004c8e:	6078      	str	r0, [r7, #4]
 8004c90:	687b      	ldr	r3, [r7, #4]
 8004c92:	68db      	ldr	r3, [r3, #12]
 8004c94:	f443 5200 	orr.w	r2, r3, #8192	; 0x2000
 8004c98:	687b      	ldr	r3, [r7, #4]
 8004c9a:	60da      	str	r2, [r3, #12]
 8004c9c:	bf00      	nop
 8004c9e:	370c      	adds	r7, #12
 8004ca0:	46bd      	mov	sp, r7
 8004ca2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004ca6:	4770      	bx	lr

08004ca8 <LL_USART_EnableDirectionRx>:
 8004ca8:	b480      	push	{r7}
 8004caa:	b083      	sub	sp, #12
 8004cac:	af00      	add	r7, sp, #0
 8004cae:	6078      	str	r0, [r7, #4]
 8004cb0:	687b      	ldr	r3, [r7, #4]
 8004cb2:	68db      	ldr	r3, [r3, #12]
 8004cb4:	f043 0204 	orr.w	r2, r3, #4
 8004cb8:	687b      	ldr	r3, [r7, #4]
 8004cba:	60da      	str	r2, [r3, #12]
 8004cbc:	bf00      	nop
 8004cbe:	370c      	adds	r7, #12
 8004cc0:	46bd      	mov	sp, r7
 8004cc2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004cc6:	4770      	bx	lr

08004cc8 <LL_USART_EnableDirectionTx>:
 8004cc8:	b480      	push	{r7}
 8004cca:	b083      	sub	sp, #12
 8004ccc:	af00      	add	r7, sp, #0
 8004cce:	6078      	str	r0, [r7, #4]
 8004cd0:	687b      	ldr	r3, [r7, #4]
 8004cd2:	68db      	ldr	r3, [r3, #12]
 8004cd4:	f043 0208 	orr.w	r2, r3, #8
 8004cd8:	687b      	ldr	r3, [r7, #4]
 8004cda:	60da      	str	r2, [r3, #12]
 8004cdc:	bf00      	nop
 8004cde:	370c      	adds	r7, #12
 8004ce0:	46bd      	mov	sp, r7
 8004ce2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004ce6:	4770      	bx	lr

08004ce8 <LL_USART_SetTransferDirection>:
 8004ce8:	b480      	push	{r7}
 8004cea:	b083      	sub	sp, #12
 8004cec:	af00      	add	r7, sp, #0
 8004cee:	6078      	str	r0, [r7, #4]
 8004cf0:	6039      	str	r1, [r7, #0]
 8004cf2:	687b      	ldr	r3, [r7, #4]
 8004cf4:	68db      	ldr	r3, [r3, #12]
 8004cf6:	f023 020c 	bic.w	r2, r3, #12
 8004cfa:	683b      	ldr	r3, [r7, #0]
 8004cfc:	431a      	orrs	r2, r3
 8004cfe:	687b      	ldr	r3, [r7, #4]
 8004d00:	60da      	str	r2, [r3, #12]
 8004d02:	bf00      	nop
 8004d04:	370c      	adds	r7, #12
 8004d06:	46bd      	mov	sp, r7
 8004d08:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d0c:	4770      	bx	lr

08004d0e <LL_USART_SetParity>:
 8004d0e:	b480      	push	{r7}
 8004d10:	b083      	sub	sp, #12
 8004d12:	af00      	add	r7, sp, #0
 8004d14:	6078      	str	r0, [r7, #4]
 8004d16:	6039      	str	r1, [r7, #0]
 8004d18:	687b      	ldr	r3, [r7, #4]
 8004d1a:	68db      	ldr	r3, [r3, #12]
 8004d1c:	f423 62c0 	bic.w	r2, r3, #1536	; 0x600
 8004d20:	683b      	ldr	r3, [r7, #0]
 8004d22:	431a      	orrs	r2, r3
 8004d24:	687b      	ldr	r3, [r7, #4]
 8004d26:	60da      	str	r2, [r3, #12]
 8004d28:	bf00      	nop
 8004d2a:	370c      	adds	r7, #12
 8004d2c:	46bd      	mov	sp, r7
 8004d2e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d32:	4770      	bx	lr

08004d34 <LL_USART_SetDataWidth>:
 8004d34:	b480      	push	{r7}
 8004d36:	b083      	sub	sp, #12
 8004d38:	af00      	add	r7, sp, #0
 8004d3a:	6078      	str	r0, [r7, #4]
 8004d3c:	6039      	str	r1, [r7, #0]
 8004d3e:	687b      	ldr	r3, [r7, #4]
 8004d40:	68db      	ldr	r3, [r3, #12]
 8004d42:	f423 5280 	bic.w	r2, r3, #4096	; 0x1000
 8004d46:	683b      	ldr	r3, [r7, #0]
 8004d48:	431a      	orrs	r2, r3
 8004d4a:	687b      	ldr	r3, [r7, #4]
 8004d4c:	60da      	str	r2, [r3, #12]
 8004d4e:	bf00      	nop
 8004d50:	370c      	adds	r7, #12
 8004d52:	46bd      	mov	sp, r7
 8004d54:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d58:	4770      	bx	lr

08004d5a <LL_USART_SetStopBitsLength>:
 8004d5a:	b480      	push	{r7}
 8004d5c:	b083      	sub	sp, #12
 8004d5e:	af00      	add	r7, sp, #0
 8004d60:	6078      	str	r0, [r7, #4]
 8004d62:	6039      	str	r1, [r7, #0]
 8004d64:	687b      	ldr	r3, [r7, #4]
 8004d66:	691b      	ldr	r3, [r3, #16]
 8004d68:	f423 5240 	bic.w	r2, r3, #12288	; 0x3000
 8004d6c:	683b      	ldr	r3, [r7, #0]
 8004d6e:	431a      	orrs	r2, r3
 8004d70:	687b      	ldr	r3, [r7, #4]
 8004d72:	611a      	str	r2, [r3, #16]
 8004d74:	bf00      	nop
 8004d76:	370c      	adds	r7, #12
 8004d78:	46bd      	mov	sp, r7
 8004d7a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004d7e:	4770      	bx	lr

08004d80 <LL_USART_SetHWFlowCtrl>:
 8004d80:	b480      	push	{r7}
 8004d82:	b083      	sub	sp, #12
 8004d84:	af00      	add	r7, sp, #0
 8004d86:	6078      	str	r0, [r7, #4]
 8004d88:	6039      	str	r1, [r7, #0]
 8004d8a:	687b      	ldr	r3, [r7, #4]
 8004d8c:	695b      	ldr	r3, [r3, #20]
 8004d8e:	f423 7240 	bic.w	r2, r3, #768	; 0x300
 8004d92:	683b      	ldr	r3, [r7, #0]
 8004d94:	431a      	orrs	r2, r3
 8004d96:	687b      	ldr	r3, [r7, #4]
 8004d98:	615a      	str	r2, [r3, #20]
 8004d9a:	bf00      	nop
 8004d9c:	370c      	adds	r7, #12
 8004d9e:	46bd      	mov	sp, r7
 8004da0:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004da4:	4770      	bx	lr
	...

08004da8 <LL_USART_SetBaudRate>:
 8004da8:	b480      	push	{r7}
 8004daa:	b085      	sub	sp, #20
 8004dac:	af00      	add	r7, sp, #0
 8004dae:	60f8      	str	r0, [r7, #12]
 8004db0:	60b9      	str	r1, [r7, #8]
 8004db2:	607a      	str	r2, [r7, #4]
 8004db4:	603b      	str	r3, [r7, #0]
 8004db6:	687b      	ldr	r3, [r7, #4]
 8004db8:	f5b3 4f00 	cmp.w	r3, #32768	; 0x8000
 8004dbc:	d152      	bne.n	8004e64 <LL_USART_SetBaudRate+0xbc>
 8004dbe:	68ba      	ldr	r2, [r7, #8]
 8004dc0:	4613      	mov	r3, r2
 8004dc2:	009b      	lsls	r3, r3, #2
 8004dc4:	4413      	add	r3, r2
 8004dc6:	009a      	lsls	r2, r3, #2
 8004dc8:	441a      	add	r2, r3
 8004dca:	683b      	ldr	r3, [r7, #0]
 8004dcc:	005b      	lsls	r3, r3, #1
 8004dce:	fbb2 f3f3 	udiv	r3, r2, r3
 8004dd2:	4a4f      	ldr	r2, [pc, #316]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004dd4:	fba2 2303 	umull	r2, r3, r2, r3
 8004dd8:	095b      	lsrs	r3, r3, #5
 8004dda:	b29b      	uxth	r3, r3
 8004ddc:	011b      	lsls	r3, r3, #4
 8004dde:	b299      	uxth	r1, r3
 8004de0:	68ba      	ldr	r2, [r7, #8]
 8004de2:	4613      	mov	r3, r2
 8004de4:	009b      	lsls	r3, r3, #2
 8004de6:	4413      	add	r3, r2
 8004de8:	009a      	lsls	r2, r3, #2
 8004dea:	441a      	add	r2, r3
 8004dec:	683b      	ldr	r3, [r7, #0]
 8004dee:	005b      	lsls	r3, r3, #1
 8004df0:	fbb2 f2f3 	udiv	r2, r2, r3
 8004df4:	4b46      	ldr	r3, [pc, #280]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004df6:	fba3 0302 	umull	r0, r3, r3, r2
 8004dfa:	095b      	lsrs	r3, r3, #5
 8004dfc:	2064      	movs	r0, #100	; 0x64
 8004dfe:	fb00 f303 	mul.w	r3, r0, r3
 8004e02:	1ad3      	subs	r3, r2, r3
 8004e04:	00db      	lsls	r3, r3, #3
 8004e06:	3332      	adds	r3, #50	; 0x32
 8004e08:	4a41      	ldr	r2, [pc, #260]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004e0a:	fba2 2303 	umull	r2, r3, r2, r3
 8004e0e:	095b      	lsrs	r3, r3, #5
 8004e10:	b29b      	uxth	r3, r3
 8004e12:	005b      	lsls	r3, r3, #1
 8004e14:	b29b      	uxth	r3, r3
 8004e16:	f403 73f8 	and.w	r3, r3, #496	; 0x1f0
 8004e1a:	b29b      	uxth	r3, r3
 8004e1c:	440b      	add	r3, r1
 8004e1e:	b299      	uxth	r1, r3
 8004e20:	68ba      	ldr	r2, [r7, #8]
 8004e22:	4613      	mov	r3, r2
 8004e24:	009b      	lsls	r3, r3, #2
 8004e26:	4413      	add	r3, r2
 8004e28:	009a      	lsls	r2, r3, #2
 8004e2a:	441a      	add	r2, r3
 8004e2c:	683b      	ldr	r3, [r7, #0]
 8004e2e:	005b      	lsls	r3, r3, #1
 8004e30:	fbb2 f2f3 	udiv	r2, r2, r3
 8004e34:	4b36      	ldr	r3, [pc, #216]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004e36:	fba3 0302 	umull	r0, r3, r3, r2
 8004e3a:	095b      	lsrs	r3, r3, #5
 8004e3c:	2064      	movs	r0, #100	; 0x64
 8004e3e:	fb00 f303 	mul.w	r3, r0, r3
 8004e42:	1ad3      	subs	r3, r2, r3
 8004e44:	00db      	lsls	r3, r3, #3
 8004e46:	3332      	adds	r3, #50	; 0x32
 8004e48:	4a31      	ldr	r2, [pc, #196]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004e4a:	fba2 2303 	umull	r2, r3, r2, r3
 8004e4e:	095b      	lsrs	r3, r3, #5
 8004e50:	b29b      	uxth	r3, r3
 8004e52:	f003 0307 	and.w	r3, r3, #7
 8004e56:	b29b      	uxth	r3, r3
 8004e58:	440b      	add	r3, r1
 8004e5a:	b29b      	uxth	r3, r3
 8004e5c:	461a      	mov	r2, r3
 8004e5e:	68fb      	ldr	r3, [r7, #12]
 8004e60:	609a      	str	r2, [r3, #8]
 8004e62:	e04f      	b.n	8004f04 <LL_USART_SetBaudRate+0x15c>
 8004e64:	68ba      	ldr	r2, [r7, #8]
 8004e66:	4613      	mov	r3, r2
 8004e68:	009b      	lsls	r3, r3, #2
 8004e6a:	4413      	add	r3, r2
 8004e6c:	009a      	lsls	r2, r3, #2
 8004e6e:	441a      	add	r2, r3
 8004e70:	683b      	ldr	r3, [r7, #0]
 8004e72:	009b      	lsls	r3, r3, #2
 8004e74:	fbb2 f3f3 	udiv	r3, r2, r3
 8004e78:	4a25      	ldr	r2, [pc, #148]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004e7a:	fba2 2303 	umull	r2, r3, r2, r3
 8004e7e:	095b      	lsrs	r3, r3, #5
 8004e80:	b29b      	uxth	r3, r3
 8004e82:	011b      	lsls	r3, r3, #4
 8004e84:	b299      	uxth	r1, r3
 8004e86:	68ba      	ldr	r2, [r7, #8]
 8004e88:	4613      	mov	r3, r2
 8004e8a:	009b      	lsls	r3, r3, #2
 8004e8c:	4413      	add	r3, r2
 8004e8e:	009a      	lsls	r2, r3, #2
 8004e90:	441a      	add	r2, r3
 8004e92:	683b      	ldr	r3, [r7, #0]
 8004e94:	009b      	lsls	r3, r3, #2
 8004e96:	fbb2 f2f3 	udiv	r2, r2, r3
 8004e9a:	4b1d      	ldr	r3, [pc, #116]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004e9c:	fba3 0302 	umull	r0, r3, r3, r2
 8004ea0:	095b      	lsrs	r3, r3, #5
 8004ea2:	2064      	movs	r0, #100	; 0x64
 8004ea4:	fb00 f303 	mul.w	r3, r0, r3
 8004ea8:	1ad3      	subs	r3, r2, r3
 8004eaa:	011b      	lsls	r3, r3, #4
 8004eac:	3332      	adds	r3, #50	; 0x32
 8004eae:	4a18      	ldr	r2, [pc, #96]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004eb0:	fba2 2303 	umull	r2, r3, r2, r3
 8004eb4:	095b      	lsrs	r3, r3, #5
 8004eb6:	b29b      	uxth	r3, r3
 8004eb8:	f003 03f0 	and.w	r3, r3, #240	; 0xf0
 8004ebc:	b29b      	uxth	r3, r3
 8004ebe:	440b      	add	r3, r1
 8004ec0:	b299      	uxth	r1, r3
 8004ec2:	68ba      	ldr	r2, [r7, #8]
 8004ec4:	4613      	mov	r3, r2
 8004ec6:	009b      	lsls	r3, r3, #2
 8004ec8:	4413      	add	r3, r2
 8004eca:	009a      	lsls	r2, r3, #2
 8004ecc:	441a      	add	r2, r3
 8004ece:	683b      	ldr	r3, [r7, #0]
 8004ed0:	009b      	lsls	r3, r3, #2
 8004ed2:	fbb2 f2f3 	udiv	r2, r2, r3
 8004ed6:	4b0e      	ldr	r3, [pc, #56]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004ed8:	fba3 0302 	umull	r0, r3, r3, r2
 8004edc:	095b      	lsrs	r3, r3, #5
 8004ede:	2064      	movs	r0, #100	; 0x64
 8004ee0:	fb00 f303 	mul.w	r3, r0, r3
 8004ee4:	1ad3      	subs	r3, r2, r3
 8004ee6:	011b      	lsls	r3, r3, #4
 8004ee8:	3332      	adds	r3, #50	; 0x32
 8004eea:	4a09      	ldr	r2, [pc, #36]	; (8004f10 <LL_USART_SetBaudRate+0x168>)
 8004eec:	fba2 2303 	umull	r2, r3, r2, r3
 8004ef0:	095b      	lsrs	r3, r3, #5
 8004ef2:	b29b      	uxth	r3, r3
 8004ef4:	f003 030f 	and.w	r3, r3, #15
 8004ef8:	b29b      	uxth	r3, r3
 8004efa:	440b      	add	r3, r1
 8004efc:	b29b      	uxth	r3, r3
 8004efe:	461a      	mov	r2, r3
 8004f00:	68fb      	ldr	r3, [r7, #12]
 8004f02:	609a      	str	r2, [r3, #8]
 8004f04:	bf00      	nop
 8004f06:	3714      	adds	r7, #20
 8004f08:	46bd      	mov	sp, r7
 8004f0a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004f0e:	4770      	bx	lr
 8004f10:	51eb851f 	.word	0x51eb851f

08004f14 <LL_USART_IsActiveFlag_TC>:
 8004f14:	b480      	push	{r7}
 8004f16:	b083      	sub	sp, #12
 8004f18:	af00      	add	r7, sp, #0
 8004f1a:	6078      	str	r0, [r7, #4]
 8004f1c:	687b      	ldr	r3, [r7, #4]
 8004f1e:	681b      	ldr	r3, [r3, #0]
 8004f20:	f003 0340 	and.w	r3, r3, #64	; 0x40
 8004f24:	2b40      	cmp	r3, #64	; 0x40
 8004f26:	bf0c      	ite	eq
 8004f28:	2301      	moveq	r3, #1
 8004f2a:	2300      	movne	r3, #0
 8004f2c:	b2db      	uxtb	r3, r3
 8004f2e:	4618      	mov	r0, r3
 8004f30:	370c      	adds	r7, #12
 8004f32:	46bd      	mov	sp, r7
 8004f34:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004f38:	4770      	bx	lr

08004f3a <LL_USART_IsActiveFlag_TXE>:
 8004f3a:	b480      	push	{r7}
 8004f3c:	b083      	sub	sp, #12
 8004f3e:	af00      	add	r7, sp, #0
 8004f40:	6078      	str	r0, [r7, #4]
 8004f42:	687b      	ldr	r3, [r7, #4]
 8004f44:	681b      	ldr	r3, [r3, #0]
 8004f46:	f003 0380 	and.w	r3, r3, #128	; 0x80
 8004f4a:	2b80      	cmp	r3, #128	; 0x80
 8004f4c:	bf0c      	ite	eq
 8004f4e:	2301      	moveq	r3, #1
 8004f50:	2300      	movne	r3, #0
 8004f52:	b2db      	uxtb	r3, r3
 8004f54:	4618      	mov	r0, r3
 8004f56:	370c      	adds	r7, #12
 8004f58:	46bd      	mov	sp, r7
 8004f5a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004f5e:	4770      	bx	lr

08004f60 <LL_USART_ClearFlag_IDLE>:
 8004f60:	b480      	push	{r7}
 8004f62:	b085      	sub	sp, #20
 8004f64:	af00      	add	r7, sp, #0
 8004f66:	6078      	str	r0, [r7, #4]
 8004f68:	687b      	ldr	r3, [r7, #4]
 8004f6a:	681b      	ldr	r3, [r3, #0]
 8004f6c:	60fb      	str	r3, [r7, #12]
 8004f6e:	68fb      	ldr	r3, [r7, #12]
 8004f70:	687b      	ldr	r3, [r7, #4]
 8004f72:	685b      	ldr	r3, [r3, #4]
 8004f74:	60fb      	str	r3, [r7, #12]
 8004f76:	68fb      	ldr	r3, [r7, #12]
 8004f78:	bf00      	nop
 8004f7a:	3714      	adds	r7, #20
 8004f7c:	46bd      	mov	sp, r7
 8004f7e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004f82:	4770      	bx	lr

08004f84 <LL_USART_ClearFlag_TC>:
 8004f84:	b480      	push	{r7}
 8004f86:	b083      	sub	sp, #12
 8004f88:	af00      	add	r7, sp, #0
 8004f8a:	6078      	str	r0, [r7, #4]
 8004f8c:	687b      	ldr	r3, [r7, #4]
 8004f8e:	f06f 0240 	mvn.w	r2, #64	; 0x40
 8004f92:	601a      	str	r2, [r3, #0]
 8004f94:	bf00      	nop
 8004f96:	370c      	adds	r7, #12
 8004f98:	46bd      	mov	sp, r7
 8004f9a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004f9e:	4770      	bx	lr

08004fa0 <LL_USART_EnableIT_IDLE>:
 8004fa0:	b480      	push	{r7}
 8004fa2:	b083      	sub	sp, #12
 8004fa4:	af00      	add	r7, sp, #0
 8004fa6:	6078      	str	r0, [r7, #4]
 8004fa8:	687b      	ldr	r3, [r7, #4]
 8004faa:	68db      	ldr	r3, [r3, #12]
 8004fac:	f043 0210 	orr.w	r2, r3, #16
 8004fb0:	687b      	ldr	r3, [r7, #4]
 8004fb2:	60da      	str	r2, [r3, #12]
 8004fb4:	bf00      	nop
 8004fb6:	370c      	adds	r7, #12
 8004fb8:	46bd      	mov	sp, r7
 8004fba:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004fbe:	4770      	bx	lr

08004fc0 <LL_USART_EnableDMAReq_RX>:
 8004fc0:	b480      	push	{r7}
 8004fc2:	b083      	sub	sp, #12
 8004fc4:	af00      	add	r7, sp, #0
 8004fc6:	6078      	str	r0, [r7, #4]
 8004fc8:	687b      	ldr	r3, [r7, #4]
 8004fca:	695b      	ldr	r3, [r3, #20]
 8004fcc:	f043 0240 	orr.w	r2, r3, #64	; 0x40
 8004fd0:	687b      	ldr	r3, [r7, #4]
 8004fd2:	615a      	str	r2, [r3, #20]
 8004fd4:	bf00      	nop
 8004fd6:	370c      	adds	r7, #12
 8004fd8:	46bd      	mov	sp, r7
 8004fda:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004fde:	4770      	bx	lr

08004fe0 <LL_USART_TransmitData8>:
 8004fe0:	b480      	push	{r7}
 8004fe2:	b083      	sub	sp, #12
 8004fe4:	af00      	add	r7, sp, #0
 8004fe6:	6078      	str	r0, [r7, #4]
 8004fe8:	460b      	mov	r3, r1
 8004fea:	70fb      	strb	r3, [r7, #3]
 8004fec:	78fa      	ldrb	r2, [r7, #3]
 8004fee:	687b      	ldr	r3, [r7, #4]
 8004ff0:	605a      	str	r2, [r3, #4]
 8004ff2:	bf00      	nop
 8004ff4:	370c      	adds	r7, #12
 8004ff6:	46bd      	mov	sp, r7
 8004ff8:	f85d 7b04 	ldr.w	r7, [sp], #4
 8004ffc:	4770      	bx	lr

08004ffe <LL_GPIO_SetPinMode>:
 8004ffe:	b480      	push	{r7}
 8005000:	b089      	sub	sp, #36	; 0x24
 8005002:	af00      	add	r7, sp, #0
 8005004:	60f8      	str	r0, [r7, #12]
 8005006:	60b9      	str	r1, [r7, #8]
 8005008:	607a      	str	r2, [r7, #4]
 800500a:	68fb      	ldr	r3, [r7, #12]
 800500c:	681a      	ldr	r2, [r3, #0]
 800500e:	68bb      	ldr	r3, [r7, #8]
 8005010:	617b      	str	r3, [r7, #20]
 8005012:	697b      	ldr	r3, [r7, #20]
 8005014:	fa93 f3a3 	rbit	r3, r3
 8005018:	613b      	str	r3, [r7, #16]
 800501a:	693b      	ldr	r3, [r7, #16]
 800501c:	fab3 f383 	clz	r3, r3
 8005020:	005b      	lsls	r3, r3, #1
 8005022:	2103      	movs	r1, #3
 8005024:	fa01 f303 	lsl.w	r3, r1, r3
 8005028:	43db      	mvns	r3, r3
 800502a:	401a      	ands	r2, r3
 800502c:	68bb      	ldr	r3, [r7, #8]
 800502e:	61fb      	str	r3, [r7, #28]
 8005030:	69fb      	ldr	r3, [r7, #28]
 8005032:	fa93 f3a3 	rbit	r3, r3
 8005036:	61bb      	str	r3, [r7, #24]
 8005038:	69bb      	ldr	r3, [r7, #24]
 800503a:	fab3 f383 	clz	r3, r3
 800503e:	005b      	lsls	r3, r3, #1
 8005040:	6879      	ldr	r1, [r7, #4]
 8005042:	fa01 f303 	lsl.w	r3, r1, r3
 8005046:	431a      	orrs	r2, r3
 8005048:	68fb      	ldr	r3, [r7, #12]
 800504a:	601a      	str	r2, [r3, #0]
 800504c:	bf00      	nop
 800504e:	3724      	adds	r7, #36	; 0x24
 8005050:	46bd      	mov	sp, r7
 8005052:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005056:	4770      	bx	lr

08005058 <LL_GPIO_SetPinOutputType>:
 8005058:	b480      	push	{r7}
 800505a:	b085      	sub	sp, #20
 800505c:	af00      	add	r7, sp, #0
 800505e:	60f8      	str	r0, [r7, #12]
 8005060:	60b9      	str	r1, [r7, #8]
 8005062:	607a      	str	r2, [r7, #4]
 8005064:	68fb      	ldr	r3, [r7, #12]
 8005066:	685a      	ldr	r2, [r3, #4]
 8005068:	68bb      	ldr	r3, [r7, #8]
 800506a:	43db      	mvns	r3, r3
 800506c:	401a      	ands	r2, r3
 800506e:	68bb      	ldr	r3, [r7, #8]
 8005070:	6879      	ldr	r1, [r7, #4]
 8005072:	fb01 f303 	mul.w	r3, r1, r3
 8005076:	431a      	orrs	r2, r3
 8005078:	68fb      	ldr	r3, [r7, #12]
 800507a:	605a      	str	r2, [r3, #4]
 800507c:	bf00      	nop
 800507e:	3714      	adds	r7, #20
 8005080:	46bd      	mov	sp, r7
 8005082:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005086:	4770      	bx	lr

08005088 <LL_GPIO_SetPinSpeed>:
 8005088:	b480      	push	{r7}
 800508a:	b089      	sub	sp, #36	; 0x24
 800508c:	af00      	add	r7, sp, #0
 800508e:	60f8      	str	r0, [r7, #12]
 8005090:	60b9      	str	r1, [r7, #8]
 8005092:	607a      	str	r2, [r7, #4]
 8005094:	68fb      	ldr	r3, [r7, #12]
 8005096:	689a      	ldr	r2, [r3, #8]
 8005098:	68bb      	ldr	r3, [r7, #8]
 800509a:	617b      	str	r3, [r7, #20]
 800509c:	697b      	ldr	r3, [r7, #20]
 800509e:	fa93 f3a3 	rbit	r3, r3
 80050a2:	613b      	str	r3, [r7, #16]
 80050a4:	693b      	ldr	r3, [r7, #16]
 80050a6:	fab3 f383 	clz	r3, r3
 80050aa:	005b      	lsls	r3, r3, #1
 80050ac:	2103      	movs	r1, #3
 80050ae:	fa01 f303 	lsl.w	r3, r1, r3
 80050b2:	43db      	mvns	r3, r3
 80050b4:	401a      	ands	r2, r3
 80050b6:	68bb      	ldr	r3, [r7, #8]
 80050b8:	61fb      	str	r3, [r7, #28]
 80050ba:	69fb      	ldr	r3, [r7, #28]
 80050bc:	fa93 f3a3 	rbit	r3, r3
 80050c0:	61bb      	str	r3, [r7, #24]
 80050c2:	69bb      	ldr	r3, [r7, #24]
 80050c4:	fab3 f383 	clz	r3, r3
 80050c8:	005b      	lsls	r3, r3, #1
 80050ca:	6879      	ldr	r1, [r7, #4]
 80050cc:	fa01 f303 	lsl.w	r3, r1, r3
 80050d0:	431a      	orrs	r2, r3
 80050d2:	68fb      	ldr	r3, [r7, #12]
 80050d4:	609a      	str	r2, [r3, #8]
 80050d6:	bf00      	nop
 80050d8:	3724      	adds	r7, #36	; 0x24
 80050da:	46bd      	mov	sp, r7
 80050dc:	f85d 7b04 	ldr.w	r7, [sp], #4
 80050e0:	4770      	bx	lr

080050e2 <LL_GPIO_SetPinPull>:
 80050e2:	b480      	push	{r7}
 80050e4:	b089      	sub	sp, #36	; 0x24
 80050e6:	af00      	add	r7, sp, #0
 80050e8:	60f8      	str	r0, [r7, #12]
 80050ea:	60b9      	str	r1, [r7, #8]
 80050ec:	607a      	str	r2, [r7, #4]
 80050ee:	68fb      	ldr	r3, [r7, #12]
 80050f0:	68da      	ldr	r2, [r3, #12]
 80050f2:	68bb      	ldr	r3, [r7, #8]
 80050f4:	617b      	str	r3, [r7, #20]
 80050f6:	697b      	ldr	r3, [r7, #20]
 80050f8:	fa93 f3a3 	rbit	r3, r3
 80050fc:	613b      	str	r3, [r7, #16]
 80050fe:	693b      	ldr	r3, [r7, #16]
 8005100:	fab3 f383 	clz	r3, r3
 8005104:	005b      	lsls	r3, r3, #1
 8005106:	2103      	movs	r1, #3
 8005108:	fa01 f303 	lsl.w	r3, r1, r3
 800510c:	43db      	mvns	r3, r3
 800510e:	401a      	ands	r2, r3
 8005110:	68bb      	ldr	r3, [r7, #8]
 8005112:	61fb      	str	r3, [r7, #28]
 8005114:	69fb      	ldr	r3, [r7, #28]
 8005116:	fa93 f3a3 	rbit	r3, r3
 800511a:	61bb      	str	r3, [r7, #24]
 800511c:	69bb      	ldr	r3, [r7, #24]
 800511e:	fab3 f383 	clz	r3, r3
 8005122:	005b      	lsls	r3, r3, #1
 8005124:	6879      	ldr	r1, [r7, #4]
 8005126:	fa01 f303 	lsl.w	r3, r1, r3
 800512a:	431a      	orrs	r2, r3
 800512c:	68fb      	ldr	r3, [r7, #12]
 800512e:	60da      	str	r2, [r3, #12]
 8005130:	bf00      	nop
 8005132:	3724      	adds	r7, #36	; 0x24
 8005134:	46bd      	mov	sp, r7
 8005136:	f85d 7b04 	ldr.w	r7, [sp], #4
 800513a:	4770      	bx	lr

0800513c <LL_GPIO_SetAFPin_8_15>:
 800513c:	b480      	push	{r7}
 800513e:	b089      	sub	sp, #36	; 0x24
 8005140:	af00      	add	r7, sp, #0
 8005142:	60f8      	str	r0, [r7, #12]
 8005144:	60b9      	str	r1, [r7, #8]
 8005146:	607a      	str	r2, [r7, #4]
 8005148:	68fb      	ldr	r3, [r7, #12]
 800514a:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 800514c:	68bb      	ldr	r3, [r7, #8]
 800514e:	0a1b      	lsrs	r3, r3, #8
 8005150:	617b      	str	r3, [r7, #20]
 8005152:	697b      	ldr	r3, [r7, #20]
 8005154:	fa93 f3a3 	rbit	r3, r3
 8005158:	613b      	str	r3, [r7, #16]
 800515a:	693b      	ldr	r3, [r7, #16]
 800515c:	fab3 f383 	clz	r3, r3
 8005160:	009b      	lsls	r3, r3, #2
 8005162:	210f      	movs	r1, #15
 8005164:	fa01 f303 	lsl.w	r3, r1, r3
 8005168:	43db      	mvns	r3, r3
 800516a:	401a      	ands	r2, r3
 800516c:	68bb      	ldr	r3, [r7, #8]
 800516e:	0a1b      	lsrs	r3, r3, #8
 8005170:	61fb      	str	r3, [r7, #28]
 8005172:	69fb      	ldr	r3, [r7, #28]
 8005174:	fa93 f3a3 	rbit	r3, r3
 8005178:	61bb      	str	r3, [r7, #24]
 800517a:	69bb      	ldr	r3, [r7, #24]
 800517c:	fab3 f383 	clz	r3, r3
 8005180:	009b      	lsls	r3, r3, #2
 8005182:	6879      	ldr	r1, [r7, #4]
 8005184:	fa01 f303 	lsl.w	r3, r1, r3
 8005188:	431a      	orrs	r2, r3
 800518a:	68fb      	ldr	r3, [r7, #12]
 800518c:	625a      	str	r2, [r3, #36]	; 0x24
 800518e:	bf00      	nop
 8005190:	3724      	adds	r7, #36	; 0x24
 8005192:	46bd      	mov	sp, r7
 8005194:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005198:	4770      	bx	lr

0800519a <LL_GPIO_IsInputPinSet>:
 800519a:	b480      	push	{r7}
 800519c:	b083      	sub	sp, #12
 800519e:	af00      	add	r7, sp, #0
 80051a0:	6078      	str	r0, [r7, #4]
 80051a2:	6039      	str	r1, [r7, #0]
 80051a4:	687b      	ldr	r3, [r7, #4]
 80051a6:	691a      	ldr	r2, [r3, #16]
 80051a8:	683b      	ldr	r3, [r7, #0]
 80051aa:	401a      	ands	r2, r3
 80051ac:	683b      	ldr	r3, [r7, #0]
 80051ae:	429a      	cmp	r2, r3
 80051b0:	bf0c      	ite	eq
 80051b2:	2301      	moveq	r3, #1
 80051b4:	2300      	movne	r3, #0
 80051b6:	b2db      	uxtb	r3, r3
 80051b8:	4618      	mov	r0, r3
 80051ba:	370c      	adds	r7, #12
 80051bc:	46bd      	mov	sp, r7
 80051be:	f85d 7b04 	ldr.w	r7, [sp], #4
 80051c2:	4770      	bx	lr

080051c4 <LL_GPIO_SetOutputPin>:
 80051c4:	b480      	push	{r7}
 80051c6:	b083      	sub	sp, #12
 80051c8:	af00      	add	r7, sp, #0
 80051ca:	6078      	str	r0, [r7, #4]
 80051cc:	6039      	str	r1, [r7, #0]
 80051ce:	687b      	ldr	r3, [r7, #4]
 80051d0:	683a      	ldr	r2, [r7, #0]
 80051d2:	619a      	str	r2, [r3, #24]
 80051d4:	bf00      	nop
 80051d6:	370c      	adds	r7, #12
 80051d8:	46bd      	mov	sp, r7
 80051da:	f85d 7b04 	ldr.w	r7, [sp], #4
 80051de:	4770      	bx	lr

080051e0 <LL_GPIO_ResetOutputPin>:
 80051e0:	b480      	push	{r7}
 80051e2:	b083      	sub	sp, #12
 80051e4:	af00      	add	r7, sp, #0
 80051e6:	6078      	str	r0, [r7, #4]
 80051e8:	6039      	str	r1, [r7, #0]
 80051ea:	683b      	ldr	r3, [r7, #0]
 80051ec:	041a      	lsls	r2, r3, #16
 80051ee:	687b      	ldr	r3, [r7, #4]
 80051f0:	619a      	str	r2, [r3, #24]
 80051f2:	bf00      	nop
 80051f4:	370c      	adds	r7, #12
 80051f6:	46bd      	mov	sp, r7
 80051f8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80051fc:	4770      	bx	lr
	...

08005200 <LL_AHB1_GRP1_EnableClock>:
 8005200:	b480      	push	{r7}
 8005202:	b085      	sub	sp, #20
 8005204:	af00      	add	r7, sp, #0
 8005206:	6078      	str	r0, [r7, #4]
 8005208:	4908      	ldr	r1, [pc, #32]	; (800522c <LL_AHB1_GRP1_EnableClock+0x2c>)
 800520a:	4b08      	ldr	r3, [pc, #32]	; (800522c <LL_AHB1_GRP1_EnableClock+0x2c>)
 800520c:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 800520e:	687b      	ldr	r3, [r7, #4]
 8005210:	4313      	orrs	r3, r2
 8005212:	630b      	str	r3, [r1, #48]	; 0x30
 8005214:	4b05      	ldr	r3, [pc, #20]	; (800522c <LL_AHB1_GRP1_EnableClock+0x2c>)
 8005216:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8005218:	687b      	ldr	r3, [r7, #4]
 800521a:	4013      	ands	r3, r2
 800521c:	60fb      	str	r3, [r7, #12]
 800521e:	68fb      	ldr	r3, [r7, #12]
 8005220:	bf00      	nop
 8005222:	3714      	adds	r7, #20
 8005224:	46bd      	mov	sp, r7
 8005226:	f85d 7b04 	ldr.w	r7, [sp], #4
 800522a:	4770      	bx	lr
 800522c:	40023800 	.word	0x40023800

08005230 <LL_APB1_GRP1_EnableClock>:
 8005230:	b480      	push	{r7}
 8005232:	b085      	sub	sp, #20
 8005234:	af00      	add	r7, sp, #0
 8005236:	6078      	str	r0, [r7, #4]
 8005238:	4908      	ldr	r1, [pc, #32]	; (800525c <LL_APB1_GRP1_EnableClock+0x2c>)
 800523a:	4b08      	ldr	r3, [pc, #32]	; (800525c <LL_APB1_GRP1_EnableClock+0x2c>)
 800523c:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 800523e:	687b      	ldr	r3, [r7, #4]
 8005240:	4313      	orrs	r3, r2
 8005242:	640b      	str	r3, [r1, #64]	; 0x40
 8005244:	4b05      	ldr	r3, [pc, #20]	; (800525c <LL_APB1_GRP1_EnableClock+0x2c>)
 8005246:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8005248:	687b      	ldr	r3, [r7, #4]
 800524a:	4013      	ands	r3, r2
 800524c:	60fb      	str	r3, [r7, #12]
 800524e:	68fb      	ldr	r3, [r7, #12]
 8005250:	bf00      	nop
 8005252:	3714      	adds	r7, #20
 8005254:	46bd      	mov	sp, r7
 8005256:	f85d 7b04 	ldr.w	r7, [sp], #4
 800525a:	4770      	bx	lr
 800525c:	40023800 	.word	0x40023800

08005260 <LL_DMA_EnableStream>:
 8005260:	b480      	push	{r7}
 8005262:	b083      	sub	sp, #12
 8005264:	af00      	add	r7, sp, #0
 8005266:	6078      	str	r0, [r7, #4]
 8005268:	6039      	str	r1, [r7, #0]
 800526a:	4a0c      	ldr	r2, [pc, #48]	; (800529c <LL_DMA_EnableStream+0x3c>)
 800526c:	683b      	ldr	r3, [r7, #0]
 800526e:	4413      	add	r3, r2
 8005270:	781b      	ldrb	r3, [r3, #0]
 8005272:	461a      	mov	r2, r3
 8005274:	687b      	ldr	r3, [r7, #4]
 8005276:	4413      	add	r3, r2
 8005278:	4619      	mov	r1, r3
 800527a:	4a08      	ldr	r2, [pc, #32]	; (800529c <LL_DMA_EnableStream+0x3c>)
 800527c:	683b      	ldr	r3, [r7, #0]
 800527e:	4413      	add	r3, r2
 8005280:	781b      	ldrb	r3, [r3, #0]
 8005282:	461a      	mov	r2, r3
 8005284:	687b      	ldr	r3, [r7, #4]
 8005286:	4413      	add	r3, r2
 8005288:	681b      	ldr	r3, [r3, #0]
 800528a:	f043 0301 	orr.w	r3, r3, #1
 800528e:	600b      	str	r3, [r1, #0]
 8005290:	bf00      	nop
 8005292:	370c      	adds	r7, #12
 8005294:	46bd      	mov	sp, r7
 8005296:	f85d 7b04 	ldr.w	r7, [sp], #4
 800529a:	4770      	bx	lr
 800529c:	08009d54 	.word	0x08009d54

080052a0 <LL_DMA_DisableStream>:
 80052a0:	b480      	push	{r7}
 80052a2:	b083      	sub	sp, #12
 80052a4:	af00      	add	r7, sp, #0
 80052a6:	6078      	str	r0, [r7, #4]
 80052a8:	6039      	str	r1, [r7, #0]
 80052aa:	4a0c      	ldr	r2, [pc, #48]	; (80052dc <LL_DMA_DisableStream+0x3c>)
 80052ac:	683b      	ldr	r3, [r7, #0]
 80052ae:	4413      	add	r3, r2
 80052b0:	781b      	ldrb	r3, [r3, #0]
 80052b2:	461a      	mov	r2, r3
 80052b4:	687b      	ldr	r3, [r7, #4]
 80052b6:	4413      	add	r3, r2
 80052b8:	4619      	mov	r1, r3
 80052ba:	4a08      	ldr	r2, [pc, #32]	; (80052dc <LL_DMA_DisableStream+0x3c>)
 80052bc:	683b      	ldr	r3, [r7, #0]
 80052be:	4413      	add	r3, r2
 80052c0:	781b      	ldrb	r3, [r3, #0]
 80052c2:	461a      	mov	r2, r3
 80052c4:	687b      	ldr	r3, [r7, #4]
 80052c6:	4413      	add	r3, r2
 80052c8:	681b      	ldr	r3, [r3, #0]
 80052ca:	f023 0301 	bic.w	r3, r3, #1
 80052ce:	600b      	str	r3, [r1, #0]
 80052d0:	bf00      	nop
 80052d2:	370c      	adds	r7, #12
 80052d4:	46bd      	mov	sp, r7
 80052d6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80052da:	4770      	bx	lr
 80052dc:	08009d54 	.word	0x08009d54

080052e0 <LL_DMA_SetMemoryIncMode>:
 80052e0:	b480      	push	{r7}
 80052e2:	b085      	sub	sp, #20
 80052e4:	af00      	add	r7, sp, #0
 80052e6:	60f8      	str	r0, [r7, #12]
 80052e8:	60b9      	str	r1, [r7, #8]
 80052ea:	607a      	str	r2, [r7, #4]
 80052ec:	4a0d      	ldr	r2, [pc, #52]	; (8005324 <LL_DMA_SetMemoryIncMode+0x44>)
 80052ee:	68bb      	ldr	r3, [r7, #8]
 80052f0:	4413      	add	r3, r2
 80052f2:	781b      	ldrb	r3, [r3, #0]
 80052f4:	461a      	mov	r2, r3
 80052f6:	68fb      	ldr	r3, [r7, #12]
 80052f8:	4413      	add	r3, r2
 80052fa:	4619      	mov	r1, r3
 80052fc:	4a09      	ldr	r2, [pc, #36]	; (8005324 <LL_DMA_SetMemoryIncMode+0x44>)
 80052fe:	68bb      	ldr	r3, [r7, #8]
 8005300:	4413      	add	r3, r2
 8005302:	781b      	ldrb	r3, [r3, #0]
 8005304:	461a      	mov	r2, r3
 8005306:	68fb      	ldr	r3, [r7, #12]
 8005308:	4413      	add	r3, r2
 800530a:	681b      	ldr	r3, [r3, #0]
 800530c:	f423 6280 	bic.w	r2, r3, #1024	; 0x400
 8005310:	687b      	ldr	r3, [r7, #4]
 8005312:	4313      	orrs	r3, r2
 8005314:	600b      	str	r3, [r1, #0]
 8005316:	bf00      	nop
 8005318:	3714      	adds	r7, #20
 800531a:	46bd      	mov	sp, r7
 800531c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005320:	4770      	bx	lr
 8005322:	bf00      	nop
 8005324:	08009d54 	.word	0x08009d54

08005328 <LL_DMA_SetDataLength>:
 8005328:	b480      	push	{r7}
 800532a:	b085      	sub	sp, #20
 800532c:	af00      	add	r7, sp, #0
 800532e:	60f8      	str	r0, [r7, #12]
 8005330:	60b9      	str	r1, [r7, #8]
 8005332:	607a      	str	r2, [r7, #4]
 8005334:	4a0d      	ldr	r2, [pc, #52]	; (800536c <LL_DMA_SetDataLength+0x44>)
 8005336:	68bb      	ldr	r3, [r7, #8]
 8005338:	4413      	add	r3, r2
 800533a:	781b      	ldrb	r3, [r3, #0]
 800533c:	461a      	mov	r2, r3
 800533e:	68fb      	ldr	r3, [r7, #12]
 8005340:	4413      	add	r3, r2
 8005342:	4619      	mov	r1, r3
 8005344:	4a09      	ldr	r2, [pc, #36]	; (800536c <LL_DMA_SetDataLength+0x44>)
 8005346:	68bb      	ldr	r3, [r7, #8]
 8005348:	4413      	add	r3, r2
 800534a:	781b      	ldrb	r3, [r3, #0]
 800534c:	461a      	mov	r2, r3
 800534e:	68fb      	ldr	r3, [r7, #12]
 8005350:	4413      	add	r3, r2
 8005352:	685b      	ldr	r3, [r3, #4]
 8005354:	0c1b      	lsrs	r3, r3, #16
 8005356:	041b      	lsls	r3, r3, #16
 8005358:	687a      	ldr	r2, [r7, #4]
 800535a:	4313      	orrs	r3, r2
 800535c:	604b      	str	r3, [r1, #4]
 800535e:	bf00      	nop
 8005360:	3714      	adds	r7, #20
 8005362:	46bd      	mov	sp, r7
 8005364:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005368:	4770      	bx	lr
 800536a:	bf00      	nop
 800536c:	08009d54 	.word	0x08009d54

08005370 <LL_DMA_SetChannelSelection>:
 8005370:	b480      	push	{r7}
 8005372:	b085      	sub	sp, #20
 8005374:	af00      	add	r7, sp, #0
 8005376:	60f8      	str	r0, [r7, #12]
 8005378:	60b9      	str	r1, [r7, #8]
 800537a:	607a      	str	r2, [r7, #4]
 800537c:	4a0d      	ldr	r2, [pc, #52]	; (80053b4 <LL_DMA_SetChannelSelection+0x44>)
 800537e:	68bb      	ldr	r3, [r7, #8]
 8005380:	4413      	add	r3, r2
 8005382:	781b      	ldrb	r3, [r3, #0]
 8005384:	461a      	mov	r2, r3
 8005386:	68fb      	ldr	r3, [r7, #12]
 8005388:	4413      	add	r3, r2
 800538a:	4619      	mov	r1, r3
 800538c:	4a09      	ldr	r2, [pc, #36]	; (80053b4 <LL_DMA_SetChannelSelection+0x44>)
 800538e:	68bb      	ldr	r3, [r7, #8]
 8005390:	4413      	add	r3, r2
 8005392:	781b      	ldrb	r3, [r3, #0]
 8005394:	461a      	mov	r2, r3
 8005396:	68fb      	ldr	r3, [r7, #12]
 8005398:	4413      	add	r3, r2
 800539a:	681b      	ldr	r3, [r3, #0]
 800539c:	f023 6260 	bic.w	r2, r3, #234881024	; 0xe000000
 80053a0:	687b      	ldr	r3, [r7, #4]
 80053a2:	4313      	orrs	r3, r2
 80053a4:	600b      	str	r3, [r1, #0]
 80053a6:	bf00      	nop
 80053a8:	3714      	adds	r7, #20
 80053aa:	46bd      	mov	sp, r7
 80053ac:	f85d 7b04 	ldr.w	r7, [sp], #4
 80053b0:	4770      	bx	lr
 80053b2:	bf00      	nop
 80053b4:	08009d54 	.word	0x08009d54

080053b8 <LL_DMA_ConfigAddresses>:
 80053b8:	b480      	push	{r7}
 80053ba:	b085      	sub	sp, #20
 80053bc:	af00      	add	r7, sp, #0
 80053be:	60f8      	str	r0, [r7, #12]
 80053c0:	60b9      	str	r1, [r7, #8]
 80053c2:	607a      	str	r2, [r7, #4]
 80053c4:	603b      	str	r3, [r7, #0]
 80053c6:	69bb      	ldr	r3, [r7, #24]
 80053c8:	2b40      	cmp	r3, #64	; 0x40
 80053ca:	d114      	bne.n	80053f6 <LL_DMA_ConfigAddresses+0x3e>
 80053cc:	4a17      	ldr	r2, [pc, #92]	; (800542c <LL_DMA_ConfigAddresses+0x74>)
 80053ce:	68bb      	ldr	r3, [r7, #8]
 80053d0:	4413      	add	r3, r2
 80053d2:	781b      	ldrb	r3, [r3, #0]
 80053d4:	461a      	mov	r2, r3
 80053d6:	68fb      	ldr	r3, [r7, #12]
 80053d8:	4413      	add	r3, r2
 80053da:	461a      	mov	r2, r3
 80053dc:	687b      	ldr	r3, [r7, #4]
 80053de:	60d3      	str	r3, [r2, #12]
 80053e0:	4a12      	ldr	r2, [pc, #72]	; (800542c <LL_DMA_ConfigAddresses+0x74>)
 80053e2:	68bb      	ldr	r3, [r7, #8]
 80053e4:	4413      	add	r3, r2
 80053e6:	781b      	ldrb	r3, [r3, #0]
 80053e8:	461a      	mov	r2, r3
 80053ea:	68fb      	ldr	r3, [r7, #12]
 80053ec:	4413      	add	r3, r2
 80053ee:	461a      	mov	r2, r3
 80053f0:	683b      	ldr	r3, [r7, #0]
 80053f2:	6093      	str	r3, [r2, #8]
 80053f4:	e013      	b.n	800541e <LL_DMA_ConfigAddresses+0x66>
 80053f6:	4a0d      	ldr	r2, [pc, #52]	; (800542c <LL_DMA_ConfigAddresses+0x74>)
 80053f8:	68bb      	ldr	r3, [r7, #8]
 80053fa:	4413      	add	r3, r2
 80053fc:	781b      	ldrb	r3, [r3, #0]
 80053fe:	461a      	mov	r2, r3
 8005400:	68fb      	ldr	r3, [r7, #12]
 8005402:	4413      	add	r3, r2
 8005404:	461a      	mov	r2, r3
 8005406:	687b      	ldr	r3, [r7, #4]
 8005408:	6093      	str	r3, [r2, #8]
 800540a:	4a08      	ldr	r2, [pc, #32]	; (800542c <LL_DMA_ConfigAddresses+0x74>)
 800540c:	68bb      	ldr	r3, [r7, #8]
 800540e:	4413      	add	r3, r2
 8005410:	781b      	ldrb	r3, [r3, #0]
 8005412:	461a      	mov	r2, r3
 8005414:	68fb      	ldr	r3, [r7, #12]
 8005416:	4413      	add	r3, r2
 8005418:	461a      	mov	r2, r3
 800541a:	683b      	ldr	r3, [r7, #0]
 800541c:	60d3      	str	r3, [r2, #12]
 800541e:	bf00      	nop
 8005420:	3714      	adds	r7, #20
 8005422:	46bd      	mov	sp, r7
 8005424:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005428:	4770      	bx	lr
 800542a:	bf00      	nop
 800542c:	08009d54 	.word	0x08009d54

08005430 <LL_DMA_IsActiveFlag_TC1>:
 8005430:	b480      	push	{r7}
 8005432:	b083      	sub	sp, #12
 8005434:	af00      	add	r7, sp, #0
 8005436:	6078      	str	r0, [r7, #4]
 8005438:	687b      	ldr	r3, [r7, #4]
 800543a:	681b      	ldr	r3, [r3, #0]
 800543c:	f403 6300 	and.w	r3, r3, #2048	; 0x800
 8005440:	f5b3 6f00 	cmp.w	r3, #2048	; 0x800
 8005444:	bf0c      	ite	eq
 8005446:	2301      	moveq	r3, #1
 8005448:	2300      	movne	r3, #0
 800544a:	b2db      	uxtb	r3, r3
 800544c:	4618      	mov	r0, r3
 800544e:	370c      	adds	r7, #12
 8005450:	46bd      	mov	sp, r7
 8005452:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005456:	4770      	bx	lr

08005458 <LL_DMA_ClearFlag_HT1>:
 8005458:	b480      	push	{r7}
 800545a:	b083      	sub	sp, #12
 800545c:	af00      	add	r7, sp, #0
 800545e:	6078      	str	r0, [r7, #4]
 8005460:	687b      	ldr	r3, [r7, #4]
 8005462:	f44f 6280 	mov.w	r2, #1024	; 0x400
 8005466:	609a      	str	r2, [r3, #8]
 8005468:	bf00      	nop
 800546a:	370c      	adds	r7, #12
 800546c:	46bd      	mov	sp, r7
 800546e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005472:	4770      	bx	lr

08005474 <LL_DMA_ClearFlag_TC1>:
 8005474:	b480      	push	{r7}
 8005476:	b083      	sub	sp, #12
 8005478:	af00      	add	r7, sp, #0
 800547a:	6078      	str	r0, [r7, #4]
 800547c:	687b      	ldr	r3, [r7, #4]
 800547e:	f44f 6200 	mov.w	r2, #2048	; 0x800
 8005482:	609a      	str	r2, [r3, #8]
 8005484:	bf00      	nop
 8005486:	370c      	adds	r7, #12
 8005488:	46bd      	mov	sp, r7
 800548a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800548e:	4770      	bx	lr

08005490 <LL_DMA_EnableIT_TC>:
 8005490:	b480      	push	{r7}
 8005492:	b083      	sub	sp, #12
 8005494:	af00      	add	r7, sp, #0
 8005496:	6078      	str	r0, [r7, #4]
 8005498:	6039      	str	r1, [r7, #0]
 800549a:	4a0c      	ldr	r2, [pc, #48]	; (80054cc <LL_DMA_EnableIT_TC+0x3c>)
 800549c:	683b      	ldr	r3, [r7, #0]
 800549e:	4413      	add	r3, r2
 80054a0:	781b      	ldrb	r3, [r3, #0]
 80054a2:	461a      	mov	r2, r3
 80054a4:	687b      	ldr	r3, [r7, #4]
 80054a6:	4413      	add	r3, r2
 80054a8:	4619      	mov	r1, r3
 80054aa:	4a08      	ldr	r2, [pc, #32]	; (80054cc <LL_DMA_EnableIT_TC+0x3c>)
 80054ac:	683b      	ldr	r3, [r7, #0]
 80054ae:	4413      	add	r3, r2
 80054b0:	781b      	ldrb	r3, [r3, #0]
 80054b2:	461a      	mov	r2, r3
 80054b4:	687b      	ldr	r3, [r7, #4]
 80054b6:	4413      	add	r3, r2
 80054b8:	681b      	ldr	r3, [r3, #0]
 80054ba:	f043 0310 	orr.w	r3, r3, #16
 80054be:	600b      	str	r3, [r1, #0]
 80054c0:	bf00      	nop
 80054c2:	370c      	adds	r7, #12
 80054c4:	46bd      	mov	sp, r7
 80054c6:	f85d 7b04 	ldr.w	r7, [sp], #4
 80054ca:	4770      	bx	lr
 80054cc:	08009d54 	.word	0x08009d54

080054d0 <manip_usart_config>:
 80054d0:	b580      	push	{r7, lr}
 80054d2:	b082      	sub	sp, #8
 80054d4:	af02      	add	r7, sp, #8
 80054d6:	f44f 1000 	mov.w	r0, #2097152	; 0x200000
 80054da:	f7ff fe91 	bl	8005200 <LL_AHB1_GRP1_EnableClock>
 80054de:	f44f 2080 	mov.w	r0, #262144	; 0x40000
 80054e2:	f7ff fea5 	bl	8005230 <LL_APB1_GRP1_EnableClock>
 80054e6:	2004      	movs	r0, #4
 80054e8:	f7ff fe8a 	bl	8005200 <LL_AHB1_GRP1_EnableClock>
 80054ec:	2202      	movs	r2, #2
 80054ee:	f44f 6180 	mov.w	r1, #1024	; 0x400
 80054f2:	484e      	ldr	r0, [pc, #312]	; (800562c <manip_usart_config+0x15c>)
 80054f4:	f7ff fd83 	bl	8004ffe <LL_GPIO_SetPinMode>
 80054f8:	2207      	movs	r2, #7
 80054fa:	f44f 6180 	mov.w	r1, #1024	; 0x400
 80054fe:	484b      	ldr	r0, [pc, #300]	; (800562c <manip_usart_config+0x15c>)
 8005500:	f7ff fe1c 	bl	800513c <LL_GPIO_SetAFPin_8_15>
 8005504:	2200      	movs	r2, #0
 8005506:	f44f 6180 	mov.w	r1, #1024	; 0x400
 800550a:	4848      	ldr	r0, [pc, #288]	; (800562c <manip_usart_config+0x15c>)
 800550c:	f7ff fda4 	bl	8005058 <LL_GPIO_SetPinOutputType>
 8005510:	2201      	movs	r2, #1
 8005512:	f44f 6180 	mov.w	r1, #1024	; 0x400
 8005516:	4845      	ldr	r0, [pc, #276]	; (800562c <manip_usart_config+0x15c>)
 8005518:	f7ff fde3 	bl	80050e2 <LL_GPIO_SetPinPull>
 800551c:	2202      	movs	r2, #2
 800551e:	f44f 6180 	mov.w	r1, #1024	; 0x400
 8005522:	4842      	ldr	r0, [pc, #264]	; (800562c <manip_usart_config+0x15c>)
 8005524:	f7ff fdb0 	bl	8005088 <LL_GPIO_SetPinSpeed>
 8005528:	2202      	movs	r2, #2
 800552a:	f44f 6100 	mov.w	r1, #2048	; 0x800
 800552e:	483f      	ldr	r0, [pc, #252]	; (800562c <manip_usart_config+0x15c>)
 8005530:	f7ff fd65 	bl	8004ffe <LL_GPIO_SetPinMode>
 8005534:	2207      	movs	r2, #7
 8005536:	f44f 6100 	mov.w	r1, #2048	; 0x800
 800553a:	483c      	ldr	r0, [pc, #240]	; (800562c <manip_usart_config+0x15c>)
 800553c:	f7ff fdfe 	bl	800513c <LL_GPIO_SetAFPin_8_15>
 8005540:	2200      	movs	r2, #0
 8005542:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8005546:	4839      	ldr	r0, [pc, #228]	; (800562c <manip_usart_config+0x15c>)
 8005548:	f7ff fd86 	bl	8005058 <LL_GPIO_SetPinOutputType>
 800554c:	2201      	movs	r2, #1
 800554e:	f44f 6100 	mov.w	r1, #2048	; 0x800
 8005552:	4836      	ldr	r0, [pc, #216]	; (800562c <manip_usart_config+0x15c>)
 8005554:	f7ff fdc5 	bl	80050e2 <LL_GPIO_SetPinPull>
 8005558:	2202      	movs	r2, #2
 800555a:	f44f 6100 	mov.w	r1, #2048	; 0x800
 800555e:	4833      	ldr	r0, [pc, #204]	; (800562c <manip_usart_config+0x15c>)
 8005560:	f7ff fd92 	bl	8005088 <LL_GPIO_SetPinSpeed>
 8005564:	210c      	movs	r1, #12
 8005566:	4832      	ldr	r0, [pc, #200]	; (8005630 <manip_usart_config+0x160>)
 8005568:	f7ff fbbe 	bl	8004ce8 <LL_USART_SetTransferDirection>
 800556c:	2100      	movs	r1, #0
 800556e:	4830      	ldr	r0, [pc, #192]	; (8005630 <manip_usart_config+0x160>)
 8005570:	f7ff fbcd 	bl	8004d0e <LL_USART_SetParity>
 8005574:	2100      	movs	r1, #0
 8005576:	482e      	ldr	r0, [pc, #184]	; (8005630 <manip_usart_config+0x160>)
 8005578:	f7ff fbdc 	bl	8004d34 <LL_USART_SetDataWidth>
 800557c:	2100      	movs	r1, #0
 800557e:	482c      	ldr	r0, [pc, #176]	; (8005630 <manip_usart_config+0x160>)
 8005580:	f7ff fbeb 	bl	8004d5a <LL_USART_SetStopBitsLength>
 8005584:	2100      	movs	r1, #0
 8005586:	482a      	ldr	r0, [pc, #168]	; (8005630 <manip_usart_config+0x160>)
 8005588:	f7ff fbfa 	bl	8004d80 <LL_USART_SetHWFlowCtrl>
 800558c:	4b29      	ldr	r3, [pc, #164]	; (8005634 <manip_usart_config+0x164>)
 800558e:	681b      	ldr	r3, [r3, #0]
 8005590:	0899      	lsrs	r1, r3, #2
 8005592:	4b29      	ldr	r3, [pc, #164]	; (8005638 <manip_usart_config+0x168>)
 8005594:	2200      	movs	r2, #0
 8005596:	4826      	ldr	r0, [pc, #152]	; (8005630 <manip_usart_config+0x160>)
 8005598:	f7ff fc06 	bl	8004da8 <LL_USART_SetBaudRate>
 800559c:	4824      	ldr	r0, [pc, #144]	; (8005630 <manip_usart_config+0x160>)
 800559e:	f7ff fb83 	bl	8004ca8 <LL_USART_EnableDirectionRx>
 80055a2:	4823      	ldr	r0, [pc, #140]	; (8005630 <manip_usart_config+0x160>)
 80055a4:	f7ff fb90 	bl	8004cc8 <LL_USART_EnableDirectionTx>
 80055a8:	4821      	ldr	r0, [pc, #132]	; (8005630 <manip_usart_config+0x160>)
 80055aa:	f7ff fd09 	bl	8004fc0 <LL_USART_EnableDMAReq_RX>
 80055ae:	4820      	ldr	r0, [pc, #128]	; (8005630 <manip_usart_config+0x160>)
 80055b0:	f7ff fcf6 	bl	8004fa0 <LL_USART_EnableIT_IDLE>
 80055b4:	481e      	ldr	r0, [pc, #120]	; (8005630 <manip_usart_config+0x160>)
 80055b6:	f7ff fb67 	bl	8004c88 <LL_USART_Enable>
 80055ba:	2101      	movs	r1, #1
 80055bc:	2027      	movs	r0, #39	; 0x27
 80055be:	f7ff fb39 	bl	8004c34 <NVIC_SetPriority>
 80055c2:	2027      	movs	r0, #39	; 0x27
 80055c4:	f7ff fb1c 	bl	8004c00 <NVIC_EnableIRQ>
 80055c8:	f04f 6200 	mov.w	r2, #134217728	; 0x8000000
 80055cc:	2101      	movs	r1, #1
 80055ce:	481b      	ldr	r0, [pc, #108]	; (800563c <manip_usart_config+0x16c>)
 80055d0:	f7ff fece 	bl	8005370 <LL_DMA_SetChannelSelection>
 80055d4:	200a      	movs	r0, #10
 80055d6:	f7fb f87d 	bl	80006d4 <malloc>
 80055da:	4603      	mov	r3, r0
 80055dc:	461a      	mov	r2, r3
 80055de:	2300      	movs	r3, #0
 80055e0:	9300      	str	r3, [sp, #0]
 80055e2:	4613      	mov	r3, r2
 80055e4:	4a16      	ldr	r2, [pc, #88]	; (8005640 <manip_usart_config+0x170>)
 80055e6:	2101      	movs	r1, #1
 80055e8:	4814      	ldr	r0, [pc, #80]	; (800563c <manip_usart_config+0x16c>)
 80055ea:	f7ff fee5 	bl	80053b8 <LL_DMA_ConfigAddresses>
 80055ee:	f44f 7280 	mov.w	r2, #256	; 0x100
 80055f2:	2101      	movs	r1, #1
 80055f4:	4811      	ldr	r0, [pc, #68]	; (800563c <manip_usart_config+0x16c>)
 80055f6:	f7ff fe97 	bl	8005328 <LL_DMA_SetDataLength>
 80055fa:	f44f 6280 	mov.w	r2, #1024	; 0x400
 80055fe:	2101      	movs	r1, #1
 8005600:	480e      	ldr	r0, [pc, #56]	; (800563c <manip_usart_config+0x16c>)
 8005602:	f7ff fe6d 	bl	80052e0 <LL_DMA_SetMemoryIncMode>
 8005606:	2101      	movs	r1, #1
 8005608:	480c      	ldr	r0, [pc, #48]	; (800563c <manip_usart_config+0x16c>)
 800560a:	f7ff fe29 	bl	8005260 <LL_DMA_EnableStream>
 800560e:	2101      	movs	r1, #1
 8005610:	480a      	ldr	r0, [pc, #40]	; (800563c <manip_usart_config+0x16c>)
 8005612:	f7ff ff3d 	bl	8005490 <LL_DMA_EnableIT_TC>
 8005616:	2106      	movs	r1, #6
 8005618:	200c      	movs	r0, #12
 800561a:	f7ff fb0b 	bl	8004c34 <NVIC_SetPriority>
 800561e:	200c      	movs	r0, #12
 8005620:	f7ff faee 	bl	8004c00 <NVIC_EnableIRQ>
 8005624:	bf00      	nop
 8005626:	46bd      	mov	sp, r7
 8005628:	bd80      	pop	{r7, pc}
 800562a:	bf00      	nop
 800562c:	40020800 	.word	0x40020800
 8005630:	40004800 	.word	0x40004800
 8005634:	200008b0 	.word	0x200008b0
 8005638:	000f4240 	.word	0x000f4240
 800563c:	40026000 	.word	0x40026000
 8005640:	40004804 	.word	0x40004804

08005644 <manip_hw_config>:
 8005644:	b580      	push	{r7, lr}
 8005646:	af00      	add	r7, sp, #0
 8005648:	2001      	movs	r0, #1
 800564a:	f7ff fdd9 	bl	8005200 <LL_AHB1_GRP1_EnableClock>
 800564e:	2002      	movs	r0, #2
 8005650:	f7ff fdd6 	bl	8005200 <LL_AHB1_GRP1_EnableClock>
 8005654:	2201      	movs	r2, #1
 8005656:	2104      	movs	r1, #4
 8005658:	485f      	ldr	r0, [pc, #380]	; (80057d8 <manip_hw_config+0x194>)
 800565a:	f7ff fcd0 	bl	8004ffe <LL_GPIO_SetPinMode>
 800565e:	2200      	movs	r2, #0
 8005660:	2104      	movs	r1, #4
 8005662:	485d      	ldr	r0, [pc, #372]	; (80057d8 <manip_hw_config+0x194>)
 8005664:	f7ff fcf8 	bl	8005058 <LL_GPIO_SetPinOutputType>
 8005668:	2200      	movs	r2, #0
 800566a:	2104      	movs	r1, #4
 800566c:	485a      	ldr	r0, [pc, #360]	; (80057d8 <manip_hw_config+0x194>)
 800566e:	f7ff fd38 	bl	80050e2 <LL_GPIO_SetPinPull>
 8005672:	2201      	movs	r2, #1
 8005674:	2108      	movs	r1, #8
 8005676:	4858      	ldr	r0, [pc, #352]	; (80057d8 <manip_hw_config+0x194>)
 8005678:	f7ff fcc1 	bl	8004ffe <LL_GPIO_SetPinMode>
 800567c:	2200      	movs	r2, #0
 800567e:	2108      	movs	r1, #8
 8005680:	4855      	ldr	r0, [pc, #340]	; (80057d8 <manip_hw_config+0x194>)
 8005682:	f7ff fce9 	bl	8005058 <LL_GPIO_SetPinOutputType>
 8005686:	2200      	movs	r2, #0
 8005688:	2108      	movs	r1, #8
 800568a:	4853      	ldr	r0, [pc, #332]	; (80057d8 <manip_hw_config+0x194>)
 800568c:	f7ff fd29 	bl	80050e2 <LL_GPIO_SetPinPull>
 8005690:	2201      	movs	r2, #1
 8005692:	2102      	movs	r1, #2
 8005694:	4851      	ldr	r0, [pc, #324]	; (80057dc <manip_hw_config+0x198>)
 8005696:	f7ff fcb2 	bl	8004ffe <LL_GPIO_SetPinMode>
 800569a:	2200      	movs	r2, #0
 800569c:	2102      	movs	r1, #2
 800569e:	484f      	ldr	r0, [pc, #316]	; (80057dc <manip_hw_config+0x198>)
 80056a0:	f7ff fcda 	bl	8005058 <LL_GPIO_SetPinOutputType>
 80056a4:	2200      	movs	r2, #0
 80056a6:	2102      	movs	r1, #2
 80056a8:	484c      	ldr	r0, [pc, #304]	; (80057dc <manip_hw_config+0x198>)
 80056aa:	f7ff fd1a 	bl	80050e2 <LL_GPIO_SetPinPull>
 80056ae:	2201      	movs	r2, #1
 80056b0:	2140      	movs	r1, #64	; 0x40
 80056b2:	4849      	ldr	r0, [pc, #292]	; (80057d8 <manip_hw_config+0x194>)
 80056b4:	f7ff fca3 	bl	8004ffe <LL_GPIO_SetPinMode>
 80056b8:	2200      	movs	r2, #0
 80056ba:	2140      	movs	r1, #64	; 0x40
 80056bc:	4846      	ldr	r0, [pc, #280]	; (80057d8 <manip_hw_config+0x194>)
 80056be:	f7ff fccb 	bl	8005058 <LL_GPIO_SetPinOutputType>
 80056c2:	2200      	movs	r2, #0
 80056c4:	2140      	movs	r1, #64	; 0x40
 80056c6:	4844      	ldr	r0, [pc, #272]	; (80057d8 <manip_hw_config+0x194>)
 80056c8:	f7ff fd0b 	bl	80050e2 <LL_GPIO_SetPinPull>
 80056cc:	2200      	movs	r2, #0
 80056ce:	2110      	movs	r1, #16
 80056d0:	4841      	ldr	r0, [pc, #260]	; (80057d8 <manip_hw_config+0x194>)
 80056d2:	f7ff fc94 	bl	8004ffe <LL_GPIO_SetPinMode>
 80056d6:	2201      	movs	r2, #1
 80056d8:	2110      	movs	r1, #16
 80056da:	483f      	ldr	r0, [pc, #252]	; (80057d8 <manip_hw_config+0x194>)
 80056dc:	f7ff fd01 	bl	80050e2 <LL_GPIO_SetPinPull>
 80056e0:	2200      	movs	r2, #0
 80056e2:	2120      	movs	r1, #32
 80056e4:	483c      	ldr	r0, [pc, #240]	; (80057d8 <manip_hw_config+0x194>)
 80056e6:	f7ff fc8a 	bl	8004ffe <LL_GPIO_SetPinMode>
 80056ea:	2201      	movs	r2, #1
 80056ec:	2120      	movs	r1, #32
 80056ee:	483a      	ldr	r0, [pc, #232]	; (80057d8 <manip_hw_config+0x194>)
 80056f0:	f7ff fcf7 	bl	80050e2 <LL_GPIO_SetPinPull>
 80056f4:	2104      	movs	r1, #4
 80056f6:	4838      	ldr	r0, [pc, #224]	; (80057d8 <manip_hw_config+0x194>)
 80056f8:	f7ff fd72 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 80056fc:	2108      	movs	r1, #8
 80056fe:	4836      	ldr	r0, [pc, #216]	; (80057d8 <manip_hw_config+0x194>)
 8005700:	f7ff fd6e 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 8005704:	2102      	movs	r1, #2
 8005706:	4835      	ldr	r0, [pc, #212]	; (80057dc <manip_hw_config+0x198>)
 8005708:	f7ff fd6a 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 800570c:	2140      	movs	r1, #64	; 0x40
 800570e:	4832      	ldr	r0, [pc, #200]	; (80057d8 <manip_hw_config+0x194>)
 8005710:	f7ff fd66 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 8005714:	2140      	movs	r1, #64	; 0x40
 8005716:	4830      	ldr	r0, [pc, #192]	; (80057d8 <manip_hw_config+0x194>)
 8005718:	f7ff fd54 	bl	80051c4 <LL_GPIO_SetOutputPin>
 800571c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005720:	f002 f9be 	bl	8007aa0 <vTaskDelay>
 8005724:	2140      	movs	r1, #64	; 0x40
 8005726:	482c      	ldr	r0, [pc, #176]	; (80057d8 <manip_hw_config+0x194>)
 8005728:	f7ff fd5a 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 800572c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005730:	f002 f9b6 	bl	8007aa0 <vTaskDelay>
 8005734:	2140      	movs	r1, #64	; 0x40
 8005736:	4828      	ldr	r0, [pc, #160]	; (80057d8 <manip_hw_config+0x194>)
 8005738:	f7ff fd44 	bl	80051c4 <LL_GPIO_SetOutputPin>
 800573c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005740:	f002 f9ae 	bl	8007aa0 <vTaskDelay>
 8005744:	2140      	movs	r1, #64	; 0x40
 8005746:	4824      	ldr	r0, [pc, #144]	; (80057d8 <manip_hw_config+0x194>)
 8005748:	f7ff fd4a 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 800574c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005750:	f002 f9a6 	bl	8007aa0 <vTaskDelay>
 8005754:	2140      	movs	r1, #64	; 0x40
 8005756:	4820      	ldr	r0, [pc, #128]	; (80057d8 <manip_hw_config+0x194>)
 8005758:	f7ff fd34 	bl	80051c4 <LL_GPIO_SetOutputPin>
 800575c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005760:	f002 f99e 	bl	8007aa0 <vTaskDelay>
 8005764:	2140      	movs	r1, #64	; 0x40
 8005766:	481c      	ldr	r0, [pc, #112]	; (80057d8 <manip_hw_config+0x194>)
 8005768:	f7ff fd3a 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 800576c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005770:	f002 f996 	bl	8007aa0 <vTaskDelay>
 8005774:	2140      	movs	r1, #64	; 0x40
 8005776:	4818      	ldr	r0, [pc, #96]	; (80057d8 <manip_hw_config+0x194>)
 8005778:	f7ff fd24 	bl	80051c4 <LL_GPIO_SetOutputPin>
 800577c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005780:	f002 f98e 	bl	8007aa0 <vTaskDelay>
 8005784:	2140      	movs	r1, #64	; 0x40
 8005786:	4814      	ldr	r0, [pc, #80]	; (80057d8 <manip_hw_config+0x194>)
 8005788:	f7ff fd2a 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 800578c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 8005790:	f002 f986 	bl	8007aa0 <vTaskDelay>
 8005794:	2140      	movs	r1, #64	; 0x40
 8005796:	4810      	ldr	r0, [pc, #64]	; (80057d8 <manip_hw_config+0x194>)
 8005798:	f7ff fd14 	bl	80051c4 <LL_GPIO_SetOutputPin>
 800579c:	f640 30b8 	movw	r0, #3000	; 0xbb8
 80057a0:	f002 f97e 	bl	8007aa0 <vTaskDelay>
 80057a4:	2140      	movs	r1, #64	; 0x40
 80057a6:	480c      	ldr	r0, [pc, #48]	; (80057d8 <manip_hw_config+0x194>)
 80057a8:	f7ff fd1a 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 80057ac:	f640 30b8 	movw	r0, #3000	; 0xbb8
 80057b0:	f002 f976 	bl	8007aa0 <vTaskDelay>
 80057b4:	2140      	movs	r1, #64	; 0x40
 80057b6:	4808      	ldr	r0, [pc, #32]	; (80057d8 <manip_hw_config+0x194>)
 80057b8:	f7ff fd04 	bl	80051c4 <LL_GPIO_SetOutputPin>
 80057bc:	f640 30b8 	movw	r0, #3000	; 0xbb8
 80057c0:	f002 f96e 	bl	8007aa0 <vTaskDelay>
 80057c4:	2140      	movs	r1, #64	; 0x40
 80057c6:	4804      	ldr	r0, [pc, #16]	; (80057d8 <manip_hw_config+0x194>)
 80057c8:	f7ff fd0a 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 80057cc:	f640 30b8 	movw	r0, #3000	; 0xbb8
 80057d0:	f002 f966 	bl	8007aa0 <vTaskDelay>
 80057d4:	bf00      	nop
 80057d6:	bd80      	pop	{r7, pc}
 80057d8:	40020000 	.word	0x40020000
 80057dc:	40020400 	.word	0x40020400

080057e0 <manip_usart_response>:
 80057e0:	b580      	push	{r7, lr}
 80057e2:	b082      	sub	sp, #8
 80057e4:	af00      	add	r7, sp, #0
 80057e6:	6078      	str	r0, [r7, #4]
 80057e8:	4815      	ldr	r0, [pc, #84]	; (8005840 <manip_usart_response+0x60>)
 80057ea:	f7ff fbcb 	bl	8004f84 <LL_USART_ClearFlag_TC>
 80057ee:	e007      	b.n	8005800 <manip_usart_response+0x20>
 80057f0:	4b14      	ldr	r3, [pc, #80]	; (8005844 <manip_usart_response+0x64>)
 80057f2:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80057f6:	601a      	str	r2, [r3, #0]
 80057f8:	f3bf 8f4f 	dsb	sy
 80057fc:	f3bf 8f6f 	isb	sy
 8005800:	480f      	ldr	r0, [pc, #60]	; (8005840 <manip_usart_response+0x60>)
 8005802:	f7ff fb9a 	bl	8004f3a <LL_USART_IsActiveFlag_TXE>
 8005806:	4603      	mov	r3, r0
 8005808:	2b00      	cmp	r3, #0
 800580a:	d0f1      	beq.n	80057f0 <manip_usart_response+0x10>
 800580c:	687b      	ldr	r3, [r7, #4]
 800580e:	b2db      	uxtb	r3, r3
 8005810:	4619      	mov	r1, r3
 8005812:	480b      	ldr	r0, [pc, #44]	; (8005840 <manip_usart_response+0x60>)
 8005814:	f7ff fbe4 	bl	8004fe0 <LL_USART_TransmitData8>
 8005818:	e007      	b.n	800582a <manip_usart_response+0x4a>
 800581a:	4b0a      	ldr	r3, [pc, #40]	; (8005844 <manip_usart_response+0x64>)
 800581c:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8005820:	601a      	str	r2, [r3, #0]
 8005822:	f3bf 8f4f 	dsb	sy
 8005826:	f3bf 8f6f 	isb	sy
 800582a:	4805      	ldr	r0, [pc, #20]	; (8005840 <manip_usart_response+0x60>)
 800582c:	f7ff fb72 	bl	8004f14 <LL_USART_IsActiveFlag_TC>
 8005830:	4603      	mov	r3, r0
 8005832:	2b00      	cmp	r3, #0
 8005834:	d0f1      	beq.n	800581a <manip_usart_response+0x3a>
 8005836:	bf00      	nop
 8005838:	3708      	adds	r7, #8
 800583a:	46bd      	mov	sp, r7
 800583c:	bd80      	pop	{r7, pc}
 800583e:	bf00      	nop
 8005840:	40004800 	.word	0x40004800
 8005844:	e000ed04 	.word	0xe000ed04

08005848 <WritePos>:
 8005848:	b580      	push	{r7, lr}
 800584a:	b08a      	sub	sp, #40	; 0x28
 800584c:	af00      	add	r7, sp, #0
 800584e:	60f8      	str	r0, [r7, #12]
 8005850:	60b9      	str	r1, [r7, #8]
 8005852:	607a      	str	r2, [r7, #4]
 8005854:	2307      	movs	r3, #7
 8005856:	627b      	str	r3, [r7, #36]	; 0x24
 8005858:	68bb      	ldr	r3, [r7, #8]
 800585a:	4a30      	ldr	r2, [pc, #192]	; (800591c <WritePos+0xd4>)
 800585c:	fb82 1203 	smull	r1, r2, r2, r3
 8005860:	441a      	add	r2, r3
 8005862:	11d2      	asrs	r2, r2, #7
 8005864:	17db      	asrs	r3, r3, #31
 8005866:	1ad3      	subs	r3, r2, r3
 8005868:	623b      	str	r3, [r7, #32]
 800586a:	68ba      	ldr	r2, [r7, #8]
 800586c:	4b2b      	ldr	r3, [pc, #172]	; (800591c <WritePos+0xd4>)
 800586e:	fb83 1302 	smull	r1, r3, r3, r2
 8005872:	4413      	add	r3, r2
 8005874:	11d9      	asrs	r1, r3, #7
 8005876:	17d3      	asrs	r3, r2, #31
 8005878:	1ac9      	subs	r1, r1, r3
 800587a:	460b      	mov	r3, r1
 800587c:	021b      	lsls	r3, r3, #8
 800587e:	1a5b      	subs	r3, r3, r1
 8005880:	1ad3      	subs	r3, r2, r3
 8005882:	61fb      	str	r3, [r7, #28]
 8005884:	687b      	ldr	r3, [r7, #4]
 8005886:	4a25      	ldr	r2, [pc, #148]	; (800591c <WritePos+0xd4>)
 8005888:	fb82 1203 	smull	r1, r2, r2, r3
 800588c:	441a      	add	r2, r3
 800588e:	11d2      	asrs	r2, r2, #7
 8005890:	17db      	asrs	r3, r3, #31
 8005892:	1ad3      	subs	r3, r2, r3
 8005894:	61bb      	str	r3, [r7, #24]
 8005896:	687a      	ldr	r2, [r7, #4]
 8005898:	4b20      	ldr	r3, [pc, #128]	; (800591c <WritePos+0xd4>)
 800589a:	fb83 1302 	smull	r1, r3, r3, r2
 800589e:	4413      	add	r3, r2
 80058a0:	11d9      	asrs	r1, r3, #7
 80058a2:	17d3      	asrs	r3, r2, #31
 80058a4:	1ac9      	subs	r1, r1, r3
 80058a6:	460b      	mov	r3, r1
 80058a8:	021b      	lsls	r3, r3, #8
 80058aa:	1a5b      	subs	r3, r3, r1
 80058ac:	1ad3      	subs	r3, r2, r3
 80058ae:	617b      	str	r3, [r7, #20]
 80058b0:	20ff      	movs	r0, #255	; 0xff
 80058b2:	f7ff ff95 	bl	80057e0 <manip_usart_response>
 80058b6:	20ff      	movs	r0, #255	; 0xff
 80058b8:	f7ff ff92 	bl	80057e0 <manip_usart_response>
 80058bc:	68f8      	ldr	r0, [r7, #12]
 80058be:	f7ff ff8f 	bl	80057e0 <manip_usart_response>
 80058c2:	6a78      	ldr	r0, [r7, #36]	; 0x24
 80058c4:	f7ff ff8c 	bl	80057e0 <manip_usart_response>
 80058c8:	2003      	movs	r0, #3
 80058ca:	f7ff ff89 	bl	80057e0 <manip_usart_response>
 80058ce:	201e      	movs	r0, #30
 80058d0:	f7ff ff86 	bl	80057e0 <manip_usart_response>
 80058d4:	69f8      	ldr	r0, [r7, #28]
 80058d6:	f7ff ff83 	bl	80057e0 <manip_usart_response>
 80058da:	6a38      	ldr	r0, [r7, #32]
 80058dc:	f7ff ff80 	bl	80057e0 <manip_usart_response>
 80058e0:	6978      	ldr	r0, [r7, #20]
 80058e2:	f7ff ff7d 	bl	80057e0 <manip_usart_response>
 80058e6:	69b8      	ldr	r0, [r7, #24]
 80058e8:	f7ff ff7a 	bl	80057e0 <manip_usart_response>
 80058ec:	68fa      	ldr	r2, [r7, #12]
 80058ee:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80058f0:	4413      	add	r3, r2
 80058f2:	f103 0221 	add.w	r2, r3, #33	; 0x21
 80058f6:	69fb      	ldr	r3, [r7, #28]
 80058f8:	441a      	add	r2, r3
 80058fa:	6a3b      	ldr	r3, [r7, #32]
 80058fc:	441a      	add	r2, r3
 80058fe:	697b      	ldr	r3, [r7, #20]
 8005900:	441a      	add	r2, r3
 8005902:	69bb      	ldr	r3, [r7, #24]
 8005904:	4413      	add	r3, r2
 8005906:	43db      	mvns	r3, r3
 8005908:	b2db      	uxtb	r3, r3
 800590a:	613b      	str	r3, [r7, #16]
 800590c:	6938      	ldr	r0, [r7, #16]
 800590e:	f7ff ff67 	bl	80057e0 <manip_usart_response>
 8005912:	bf00      	nop
 8005914:	3728      	adds	r7, #40	; 0x28
 8005916:	46bd      	mov	sp, r7
 8005918:	bd80      	pop	{r7, pc}
 800591a:	bf00      	nop
 800591c:	80808081 	.word	0x80808081

08005920 <servo>:
 8005920:	b580      	push	{r7, lr}
 8005922:	b082      	sub	sp, #8
 8005924:	af00      	add	r7, sp, #0
 8005926:	6078      	str	r0, [r7, #4]
 8005928:	f7ff fe8c 	bl	8005644 <manip_hw_config>
 800592c:	f04f 32ff 	mov.w	r2, #4294967295
 8005930:	2101      	movs	r1, #1
 8005932:	2000      	movs	r0, #0
 8005934:	f002 fe08 	bl	8008548 <ulTaskGenericNotifyTake>
 8005938:	e7f8      	b.n	800592c <servo+0xc>
	...

0800593c <cmd_set_servo_pose>:
 800593c:	b590      	push	{r4, r7, lr}
 800593e:	b085      	sub	sp, #20
 8005940:	af00      	add	r7, sp, #0
 8005942:	6078      	str	r0, [r7, #4]
 8005944:	687b      	ldr	r3, [r7, #4]
 8005946:	60fb      	str	r3, [r7, #12]
 8005948:	68fb      	ldr	r3, [r7, #12]
 800594a:	781a      	ldrb	r2, [r3, #0]
 800594c:	7859      	ldrb	r1, [r3, #1]
 800594e:	0209      	lsls	r1, r1, #8
 8005950:	430a      	orrs	r2, r1
 8005952:	7899      	ldrb	r1, [r3, #2]
 8005954:	0409      	lsls	r1, r1, #16
 8005956:	430a      	orrs	r2, r1
 8005958:	78db      	ldrb	r3, [r3, #3]
 800595a:	061b      	lsls	r3, r3, #24
 800595c:	4313      	orrs	r3, r2
 800595e:	2b01      	cmp	r3, #1
 8005960:	d14e      	bne.n	8005a00 <cmd_set_servo_pose+0xc4>
 8005962:	68fb      	ldr	r3, [r7, #12]
 8005964:	781a      	ldrb	r2, [r3, #0]
 8005966:	7859      	ldrb	r1, [r3, #1]
 8005968:	0209      	lsls	r1, r1, #8
 800596a:	430a      	orrs	r2, r1
 800596c:	7899      	ldrb	r1, [r3, #2]
 800596e:	0409      	lsls	r1, r1, #16
 8005970:	430a      	orrs	r2, r1
 8005972:	78db      	ldrb	r3, [r3, #3]
 8005974:	061b      	lsls	r3, r3, #24
 8005976:	4313      	orrs	r3, r2
 8005978:	4618      	mov	r0, r3
 800597a:	68fb      	ldr	r3, [r7, #12]
 800597c:	791a      	ldrb	r2, [r3, #4]
 800597e:	7959      	ldrb	r1, [r3, #5]
 8005980:	0209      	lsls	r1, r1, #8
 8005982:	430a      	orrs	r2, r1
 8005984:	7999      	ldrb	r1, [r3, #6]
 8005986:	0409      	lsls	r1, r1, #16
 8005988:	430a      	orrs	r2, r1
 800598a:	79db      	ldrb	r3, [r3, #7]
 800598c:	061b      	lsls	r3, r3, #24
 800598e:	4313      	orrs	r3, r2
 8005990:	461c      	mov	r4, r3
 8005992:	68fb      	ldr	r3, [r7, #12]
 8005994:	7a1a      	ldrb	r2, [r3, #8]
 8005996:	7a59      	ldrb	r1, [r3, #9]
 8005998:	0209      	lsls	r1, r1, #8
 800599a:	430a      	orrs	r2, r1
 800599c:	7a99      	ldrb	r1, [r3, #10]
 800599e:	0409      	lsls	r1, r1, #16
 80059a0:	430a      	orrs	r2, r1
 80059a2:	7adb      	ldrb	r3, [r3, #11]
 80059a4:	061b      	lsls	r3, r3, #24
 80059a6:	4313      	orrs	r3, r2
 80059a8:	461a      	mov	r2, r3
 80059aa:	4621      	mov	r1, r4
 80059ac:	f7ff ff4c 	bl	8005848 <WritePos>
 80059b0:	68fb      	ldr	r3, [r7, #12]
 80059b2:	781a      	ldrb	r2, [r3, #0]
 80059b4:	7859      	ldrb	r1, [r3, #1]
 80059b6:	0209      	lsls	r1, r1, #8
 80059b8:	430a      	orrs	r2, r1
 80059ba:	7899      	ldrb	r1, [r3, #2]
 80059bc:	0409      	lsls	r1, r1, #16
 80059be:	430a      	orrs	r2, r1
 80059c0:	78db      	ldrb	r3, [r3, #3]
 80059c2:	061b      	lsls	r3, r3, #24
 80059c4:	4313      	orrs	r3, r2
 80059c6:	1c58      	adds	r0, r3, #1
 80059c8:	68fb      	ldr	r3, [r7, #12]
 80059ca:	791a      	ldrb	r2, [r3, #4]
 80059cc:	7959      	ldrb	r1, [r3, #5]
 80059ce:	0209      	lsls	r1, r1, #8
 80059d0:	430a      	orrs	r2, r1
 80059d2:	7999      	ldrb	r1, [r3, #6]
 80059d4:	0409      	lsls	r1, r1, #16
 80059d6:	430a      	orrs	r2, r1
 80059d8:	79db      	ldrb	r3, [r3, #7]
 80059da:	061b      	lsls	r3, r3, #24
 80059dc:	4313      	orrs	r3, r2
 80059de:	f5c3 6480 	rsb	r4, r3, #1024	; 0x400
 80059e2:	68fb      	ldr	r3, [r7, #12]
 80059e4:	7a1a      	ldrb	r2, [r3, #8]
 80059e6:	7a59      	ldrb	r1, [r3, #9]
 80059e8:	0209      	lsls	r1, r1, #8
 80059ea:	430a      	orrs	r2, r1
 80059ec:	7a99      	ldrb	r1, [r3, #10]
 80059ee:	0409      	lsls	r1, r1, #16
 80059f0:	430a      	orrs	r2, r1
 80059f2:	7adb      	ldrb	r3, [r3, #11]
 80059f4:	061b      	lsls	r3, r3, #24
 80059f6:	4313      	orrs	r3, r2
 80059f8:	461a      	mov	r2, r3
 80059fa:	4621      	mov	r1, r4
 80059fc:	f7ff ff24 	bl	8005848 <WritePos>
 8005a00:	68fb      	ldr	r3, [r7, #12]
 8005a02:	781a      	ldrb	r2, [r3, #0]
 8005a04:	7859      	ldrb	r1, [r3, #1]
 8005a06:	0209      	lsls	r1, r1, #8
 8005a08:	430a      	orrs	r2, r1
 8005a0a:	7899      	ldrb	r1, [r3, #2]
 8005a0c:	0409      	lsls	r1, r1, #16
 8005a0e:	430a      	orrs	r2, r1
 8005a10:	78db      	ldrb	r3, [r3, #3]
 8005a12:	061b      	lsls	r3, r3, #24
 8005a14:	4313      	orrs	r3, r2
 8005a16:	4618      	mov	r0, r3
 8005a18:	68fb      	ldr	r3, [r7, #12]
 8005a1a:	791a      	ldrb	r2, [r3, #4]
 8005a1c:	7959      	ldrb	r1, [r3, #5]
 8005a1e:	0209      	lsls	r1, r1, #8
 8005a20:	430a      	orrs	r2, r1
 8005a22:	7999      	ldrb	r1, [r3, #6]
 8005a24:	0409      	lsls	r1, r1, #16
 8005a26:	430a      	orrs	r2, r1
 8005a28:	79db      	ldrb	r3, [r3, #7]
 8005a2a:	061b      	lsls	r3, r3, #24
 8005a2c:	4313      	orrs	r3, r2
 8005a2e:	461c      	mov	r4, r3
 8005a30:	68fb      	ldr	r3, [r7, #12]
 8005a32:	7a1a      	ldrb	r2, [r3, #8]
 8005a34:	7a59      	ldrb	r1, [r3, #9]
 8005a36:	0209      	lsls	r1, r1, #8
 8005a38:	430a      	orrs	r2, r1
 8005a3a:	7a99      	ldrb	r1, [r3, #10]
 8005a3c:	0409      	lsls	r1, r1, #16
 8005a3e:	430a      	orrs	r2, r1
 8005a40:	7adb      	ldrb	r3, [r3, #11]
 8005a42:	061b      	lsls	r3, r3, #24
 8005a44:	4313      	orrs	r3, r2
 8005a46:	461a      	mov	r2, r3
 8005a48:	4621      	mov	r1, r4
 8005a4a:	f7ff fefd 	bl	8005848 <WritePos>
 8005a4e:	2203      	movs	r2, #3
 8005a50:	4904      	ldr	r1, [pc, #16]	; (8005a64 <cmd_set_servo_pose+0x128>)
 8005a52:	6878      	ldr	r0, [r7, #4]
 8005a54:	f7fb f8f8 	bl	8000c48 <memcpy>
 8005a58:	2303      	movs	r3, #3
 8005a5a:	4618      	mov	r0, r3
 8005a5c:	3714      	adds	r7, #20
 8005a5e:	46bd      	mov	sp, r7
 8005a60:	bd90      	pop	{r4, r7, pc}
 8005a62:	bf00      	nop
 8005a64:	08009898 	.word	0x08009898

08005a68 <cmd_set_servo_angle>:
 8005a68:	b580      	push	{r7, lr}
 8005a6a:	b084      	sub	sp, #16
 8005a6c:	af00      	add	r7, sp, #0
 8005a6e:	6078      	str	r0, [r7, #4]
 8005a70:	687b      	ldr	r3, [r7, #4]
 8005a72:	60fb      	str	r3, [r7, #12]
 8005a74:	68fb      	ldr	r3, [r7, #12]
 8005a76:	791a      	ldrb	r2, [r3, #4]
 8005a78:	7959      	ldrb	r1, [r3, #5]
 8005a7a:	0209      	lsls	r1, r1, #8
 8005a7c:	430a      	orrs	r2, r1
 8005a7e:	7999      	ldrb	r1, [r3, #6]
 8005a80:	0409      	lsls	r1, r1, #16
 8005a82:	430a      	orrs	r2, r1
 8005a84:	79db      	ldrb	r3, [r3, #7]
 8005a86:	061b      	lsls	r3, r3, #24
 8005a88:	4313      	orrs	r3, r2
 8005a8a:	f5b3 7f96 	cmp.w	r3, #300	; 0x12c
 8005a8e:	dc44      	bgt.n	8005b1a <cmd_set_servo_angle+0xb2>
 8005a90:	68fb      	ldr	r3, [r7, #12]
 8005a92:	791a      	ldrb	r2, [r3, #4]
 8005a94:	7959      	ldrb	r1, [r3, #5]
 8005a96:	0209      	lsls	r1, r1, #8
 8005a98:	430a      	orrs	r2, r1
 8005a9a:	7999      	ldrb	r1, [r3, #6]
 8005a9c:	0409      	lsls	r1, r1, #16
 8005a9e:	430a      	orrs	r2, r1
 8005aa0:	79db      	ldrb	r3, [r3, #7]
 8005aa2:	061b      	lsls	r3, r3, #24
 8005aa4:	4313      	orrs	r3, r2
 8005aa6:	2b00      	cmp	r3, #0
 8005aa8:	db37      	blt.n	8005b1a <cmd_set_servo_angle+0xb2>
 8005aaa:	68fb      	ldr	r3, [r7, #12]
 8005aac:	791a      	ldrb	r2, [r3, #4]
 8005aae:	7959      	ldrb	r1, [r3, #5]
 8005ab0:	0209      	lsls	r1, r1, #8
 8005ab2:	430a      	orrs	r2, r1
 8005ab4:	7999      	ldrb	r1, [r3, #6]
 8005ab6:	0409      	lsls	r1, r1, #16
 8005ab8:	430a      	orrs	r2, r1
 8005aba:	79db      	ldrb	r3, [r3, #7]
 8005abc:	061b      	lsls	r3, r3, #24
 8005abe:	4313      	orrs	r3, r2
 8005ac0:	461a      	mov	r2, r3
 8005ac2:	4613      	mov	r3, r2
 8005ac4:	029b      	lsls	r3, r3, #10
 8005ac6:	1a9b      	subs	r3, r3, r2
 8005ac8:	4a19      	ldr	r2, [pc, #100]	; (8005b30 <cmd_set_servo_angle+0xc8>)
 8005aca:	fb82 1203 	smull	r1, r2, r2, r3
 8005ace:	1152      	asrs	r2, r2, #5
 8005ad0:	17db      	asrs	r3, r3, #31
 8005ad2:	1ad3      	subs	r3, r2, r3
 8005ad4:	60bb      	str	r3, [r7, #8]
 8005ad6:	68fb      	ldr	r3, [r7, #12]
 8005ad8:	781a      	ldrb	r2, [r3, #0]
 8005ada:	7859      	ldrb	r1, [r3, #1]
 8005adc:	0209      	lsls	r1, r1, #8
 8005ade:	430a      	orrs	r2, r1
 8005ae0:	7899      	ldrb	r1, [r3, #2]
 8005ae2:	0409      	lsls	r1, r1, #16
 8005ae4:	430a      	orrs	r2, r1
 8005ae6:	78db      	ldrb	r3, [r3, #3]
 8005ae8:	061b      	lsls	r3, r3, #24
 8005aea:	4313      	orrs	r3, r2
 8005aec:	4618      	mov	r0, r3
 8005aee:	68fb      	ldr	r3, [r7, #12]
 8005af0:	7a1a      	ldrb	r2, [r3, #8]
 8005af2:	7a59      	ldrb	r1, [r3, #9]
 8005af4:	0209      	lsls	r1, r1, #8
 8005af6:	430a      	orrs	r2, r1
 8005af8:	7a99      	ldrb	r1, [r3, #10]
 8005afa:	0409      	lsls	r1, r1, #16
 8005afc:	430a      	orrs	r2, r1
 8005afe:	7adb      	ldrb	r3, [r3, #11]
 8005b00:	061b      	lsls	r3, r3, #24
 8005b02:	4313      	orrs	r3, r2
 8005b04:	461a      	mov	r2, r3
 8005b06:	68b9      	ldr	r1, [r7, #8]
 8005b08:	f7ff fe9e 	bl	8005848 <WritePos>
 8005b0c:	2203      	movs	r2, #3
 8005b0e:	4909      	ldr	r1, [pc, #36]	; (8005b34 <cmd_set_servo_angle+0xcc>)
 8005b10:	6878      	ldr	r0, [r7, #4]
 8005b12:	f7fb f899 	bl	8000c48 <memcpy>
 8005b16:	2303      	movs	r3, #3
 8005b18:	e005      	b.n	8005b26 <cmd_set_servo_angle+0xbe>
 8005b1a:	2203      	movs	r2, #3
 8005b1c:	4906      	ldr	r1, [pc, #24]	; (8005b38 <cmd_set_servo_angle+0xd0>)
 8005b1e:	6878      	ldr	r0, [r7, #4]
 8005b20:	f7fb f892 	bl	8000c48 <memcpy>
 8005b24:	2303      	movs	r3, #3
 8005b26:	4618      	mov	r0, r3
 8005b28:	3710      	adds	r7, #16
 8005b2a:	46bd      	mov	sp, r7
 8005b2c:	bd80      	pop	{r7, pc}
 8005b2e:	bf00      	nop
 8005b30:	1b4e81b5 	.word	0x1b4e81b5
 8005b34:	0800989c 	.word	0x0800989c
 8005b38:	080098a0 	.word	0x080098a0

08005b3c <cmd_set_comp_state>:
 8005b3c:	b580      	push	{r7, lr}
 8005b3e:	b084      	sub	sp, #16
 8005b40:	af00      	add	r7, sp, #0
 8005b42:	6078      	str	r0, [r7, #4]
 8005b44:	687b      	ldr	r3, [r7, #4]
 8005b46:	60fb      	str	r3, [r7, #12]
 8005b48:	68fb      	ldr	r3, [r7, #12]
 8005b4a:	781a      	ldrb	r2, [r3, #0]
 8005b4c:	7859      	ldrb	r1, [r3, #1]
 8005b4e:	0209      	lsls	r1, r1, #8
 8005b50:	430a      	orrs	r2, r1
 8005b52:	7899      	ldrb	r1, [r3, #2]
 8005b54:	0409      	lsls	r1, r1, #16
 8005b56:	430a      	orrs	r2, r1
 8005b58:	78db      	ldrb	r3, [r3, #3]
 8005b5a:	061b      	lsls	r3, r3, #24
 8005b5c:	4313      	orrs	r3, r2
 8005b5e:	2b01      	cmp	r3, #1
 8005b60:	d10c      	bne.n	8005b7c <cmd_set_comp_state+0x40>
 8005b62:	2104      	movs	r1, #4
 8005b64:	481b      	ldr	r0, [pc, #108]	; (8005bd4 <cmd_set_comp_state+0x98>)
 8005b66:	f7ff fb2d 	bl	80051c4 <LL_GPIO_SetOutputPin>
 8005b6a:	2108      	movs	r1, #8
 8005b6c:	4819      	ldr	r0, [pc, #100]	; (8005bd4 <cmd_set_comp_state+0x98>)
 8005b6e:	f7ff fb37 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 8005b72:	2102      	movs	r1, #2
 8005b74:	4818      	ldr	r0, [pc, #96]	; (8005bd8 <cmd_set_comp_state+0x9c>)
 8005b76:	f7ff fb25 	bl	80051c4 <LL_GPIO_SetOutputPin>
 8005b7a:	e018      	b.n	8005bae <cmd_set_comp_state+0x72>
 8005b7c:	68fb      	ldr	r3, [r7, #12]
 8005b7e:	781a      	ldrb	r2, [r3, #0]
 8005b80:	7859      	ldrb	r1, [r3, #1]
 8005b82:	0209      	lsls	r1, r1, #8
 8005b84:	430a      	orrs	r2, r1
 8005b86:	7899      	ldrb	r1, [r3, #2]
 8005b88:	0409      	lsls	r1, r1, #16
 8005b8a:	430a      	orrs	r2, r1
 8005b8c:	78db      	ldrb	r3, [r3, #3]
 8005b8e:	061b      	lsls	r3, r3, #24
 8005b90:	4313      	orrs	r3, r2
 8005b92:	2b00      	cmp	r3, #0
 8005b94:	d112      	bne.n	8005bbc <cmd_set_comp_state+0x80>
 8005b96:	2104      	movs	r1, #4
 8005b98:	480e      	ldr	r0, [pc, #56]	; (8005bd4 <cmd_set_comp_state+0x98>)
 8005b9a:	f7ff fb21 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 8005b9e:	2108      	movs	r1, #8
 8005ba0:	480c      	ldr	r0, [pc, #48]	; (8005bd4 <cmd_set_comp_state+0x98>)
 8005ba2:	f7ff fb1d 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 8005ba6:	2102      	movs	r1, #2
 8005ba8:	480b      	ldr	r0, [pc, #44]	; (8005bd8 <cmd_set_comp_state+0x9c>)
 8005baa:	f7ff fb19 	bl	80051e0 <LL_GPIO_ResetOutputPin>
 8005bae:	2203      	movs	r2, #3
 8005bb0:	490a      	ldr	r1, [pc, #40]	; (8005bdc <cmd_set_comp_state+0xa0>)
 8005bb2:	6878      	ldr	r0, [r7, #4]
 8005bb4:	f7fb f848 	bl	8000c48 <memcpy>
 8005bb8:	2303      	movs	r3, #3
 8005bba:	e006      	b.n	8005bca <cmd_set_comp_state+0x8e>
 8005bbc:	bf00      	nop
 8005bbe:	2203      	movs	r2, #3
 8005bc0:	4907      	ldr	r1, [pc, #28]	; (8005be0 <cmd_set_comp_state+0xa4>)
 8005bc2:	6878      	ldr	r0, [r7, #4]
 8005bc4:	f7fb f840 	bl	8000c48 <memcpy>
 8005bc8:	2303      	movs	r3, #3
 8005bca:	4618      	mov	r0, r3
 8005bcc:	3710      	adds	r7, #16
 8005bce:	46bd      	mov	sp, r7
 8005bd0:	bd80      	pop	{r7, pc}
 8005bd2:	bf00      	nop
 8005bd4:	40020000 	.word	0x40020000
 8005bd8:	40020400 	.word	0x40020400
 8005bdc:	0800989c 	.word	0x0800989c
 8005be0:	080098a0 	.word	0x080098a0

08005be4 <cmd_get_key_state>:
 8005be4:	b580      	push	{r7, lr}
 8005be6:	b084      	sub	sp, #16
 8005be8:	af00      	add	r7, sp, #0
 8005bea:	6078      	str	r0, [r7, #4]
 8005bec:	2110      	movs	r1, #16
 8005bee:	480b      	ldr	r0, [pc, #44]	; (8005c1c <cmd_get_key_state+0x38>)
 8005bf0:	f7ff fad3 	bl	800519a <LL_GPIO_IsInputPinSet>
 8005bf4:	4603      	mov	r3, r0
 8005bf6:	2b00      	cmp	r3, #0
 8005bf8:	d006      	beq.n	8005c08 <cmd_get_key_state+0x24>
 8005bfa:	2300      	movs	r3, #0
 8005bfc:	60fb      	str	r3, [r7, #12]
 8005bfe:	7b3a      	ldrb	r2, [r7, #12]
 8005c00:	687b      	ldr	r3, [r7, #4]
 8005c02:	701a      	strb	r2, [r3, #0]
 8005c04:	2301      	movs	r3, #1
 8005c06:	e005      	b.n	8005c14 <cmd_get_key_state+0x30>
 8005c08:	2301      	movs	r3, #1
 8005c0a:	60fb      	str	r3, [r7, #12]
 8005c0c:	7b3a      	ldrb	r2, [r7, #12]
 8005c0e:	687b      	ldr	r3, [r7, #4]
 8005c10:	701a      	strb	r2, [r3, #0]
 8005c12:	2301      	movs	r3, #1
 8005c14:	4618      	mov	r0, r3
 8005c16:	3710      	adds	r7, #16
 8005c18:	46bd      	mov	sp, r7
 8005c1a:	bd80      	pop	{r7, pc}
 8005c1c:	40020000 	.word	0x40020000

08005c20 <cmd_get_side>:
 8005c20:	b580      	push	{r7, lr}
 8005c22:	b084      	sub	sp, #16
 8005c24:	af00      	add	r7, sp, #0
 8005c26:	6078      	str	r0, [r7, #4]
 8005c28:	2120      	movs	r1, #32
 8005c2a:	480b      	ldr	r0, [pc, #44]	; (8005c58 <cmd_get_side+0x38>)
 8005c2c:	f7ff fab5 	bl	800519a <LL_GPIO_IsInputPinSet>
 8005c30:	4603      	mov	r3, r0
 8005c32:	2b00      	cmp	r3, #0
 8005c34:	d006      	beq.n	8005c44 <cmd_get_side+0x24>
 8005c36:	2300      	movs	r3, #0
 8005c38:	60fb      	str	r3, [r7, #12]
 8005c3a:	7b3a      	ldrb	r2, [r7, #12]
 8005c3c:	687b      	ldr	r3, [r7, #4]
 8005c3e:	701a      	strb	r2, [r3, #0]
 8005c40:	2301      	movs	r3, #1
 8005c42:	e005      	b.n	8005c50 <cmd_get_side+0x30>
 8005c44:	2301      	movs	r3, #1
 8005c46:	60fb      	str	r3, [r7, #12]
 8005c48:	7b3a      	ldrb	r2, [r7, #12]
 8005c4a:	687b      	ldr	r3, [r7, #4]
 8005c4c:	701a      	strb	r2, [r3, #0]
 8005c4e:	2301      	movs	r3, #1
 8005c50:	4618      	mov	r0, r3
 8005c52:	3710      	adds	r7, #16
 8005c54:	46bd      	mov	sp, r7
 8005c56:	bd80      	pop	{r7, pc}
 8005c58:	40020000 	.word	0x40020000

08005c5c <USART3_IRQHandler>:
 8005c5c:	b580      	push	{r7, lr}
 8005c5e:	b082      	sub	sp, #8
 8005c60:	af00      	add	r7, sp, #0
 8005c62:	2300      	movs	r3, #0
 8005c64:	607b      	str	r3, [r7, #4]
 8005c66:	480b      	ldr	r0, [pc, #44]	; (8005c94 <USART3_IRQHandler+0x38>)
 8005c68:	f7ff f97a 	bl	8004f60 <LL_USART_ClearFlag_IDLE>
 8005c6c:	2101      	movs	r1, #1
 8005c6e:	480a      	ldr	r0, [pc, #40]	; (8005c98 <USART3_IRQHandler+0x3c>)
 8005c70:	f7ff fb16 	bl	80052a0 <LL_DMA_DisableStream>
 8005c74:	687b      	ldr	r3, [r7, #4]
 8005c76:	2b00      	cmp	r3, #0
 8005c78:	d007      	beq.n	8005c8a <USART3_IRQHandler+0x2e>
 8005c7a:	4b08      	ldr	r3, [pc, #32]	; (8005c9c <USART3_IRQHandler+0x40>)
 8005c7c:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8005c80:	601a      	str	r2, [r3, #0]
 8005c82:	f3bf 8f4f 	dsb	sy
 8005c86:	f3bf 8f6f 	isb	sy
 8005c8a:	bf00      	nop
 8005c8c:	3708      	adds	r7, #8
 8005c8e:	46bd      	mov	sp, r7
 8005c90:	bd80      	pop	{r7, pc}
 8005c92:	bf00      	nop
 8005c94:	40004800 	.word	0x40004800
 8005c98:	40026000 	.word	0x40026000
 8005c9c:	e000ed04 	.word	0xe000ed04

08005ca0 <DMA1_Stream1_IRQHandler>:
 8005ca0:	b580      	push	{r7, lr}
 8005ca2:	b082      	sub	sp, #8
 8005ca4:	af00      	add	r7, sp, #0
 8005ca6:	2300      	movs	r3, #0
 8005ca8:	607b      	str	r3, [r7, #4]
 8005caa:	480f      	ldr	r0, [pc, #60]	; (8005ce8 <DMA1_Stream1_IRQHandler+0x48>)
 8005cac:	f7ff fbc0 	bl	8005430 <LL_DMA_IsActiveFlag_TC1>
 8005cb0:	4603      	mov	r3, r0
 8005cb2:	2b00      	cmp	r3, #0
 8005cb4:	d009      	beq.n	8005cca <DMA1_Stream1_IRQHandler+0x2a>
 8005cb6:	480c      	ldr	r0, [pc, #48]	; (8005ce8 <DMA1_Stream1_IRQHandler+0x48>)
 8005cb8:	f7ff fbdc 	bl	8005474 <LL_DMA_ClearFlag_TC1>
 8005cbc:	480a      	ldr	r0, [pc, #40]	; (8005ce8 <DMA1_Stream1_IRQHandler+0x48>)
 8005cbe:	f7ff fbcb 	bl	8005458 <LL_DMA_ClearFlag_HT1>
 8005cc2:	2101      	movs	r1, #1
 8005cc4:	4808      	ldr	r0, [pc, #32]	; (8005ce8 <DMA1_Stream1_IRQHandler+0x48>)
 8005cc6:	f7ff facb 	bl	8005260 <LL_DMA_EnableStream>
 8005cca:	687b      	ldr	r3, [r7, #4]
 8005ccc:	2b00      	cmp	r3, #0
 8005cce:	d007      	beq.n	8005ce0 <DMA1_Stream1_IRQHandler+0x40>
 8005cd0:	4b06      	ldr	r3, [pc, #24]	; (8005cec <DMA1_Stream1_IRQHandler+0x4c>)
 8005cd2:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8005cd6:	601a      	str	r2, [r3, #0]
 8005cd8:	f3bf 8f4f 	dsb	sy
 8005cdc:	f3bf 8f6f 	isb	sy
 8005ce0:	bf00      	nop
 8005ce2:	3708      	adds	r7, #8
 8005ce4:	46bd      	mov	sp, r7
 8005ce6:	bd80      	pop	{r7, pc}
 8005ce8:	40026000 	.word	0x40026000
 8005cec:	e000ed04 	.word	0xe000ed04

08005cf0 <NVIC_EnableIRQ>:
 8005cf0:	b480      	push	{r7}
 8005cf2:	b083      	sub	sp, #12
 8005cf4:	af00      	add	r7, sp, #0
 8005cf6:	4603      	mov	r3, r0
 8005cf8:	71fb      	strb	r3, [r7, #7]
 8005cfa:	4909      	ldr	r1, [pc, #36]	; (8005d20 <NVIC_EnableIRQ+0x30>)
 8005cfc:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8005d00:	095b      	lsrs	r3, r3, #5
 8005d02:	79fa      	ldrb	r2, [r7, #7]
 8005d04:	f002 021f 	and.w	r2, r2, #31
 8005d08:	2001      	movs	r0, #1
 8005d0a:	fa00 f202 	lsl.w	r2, r0, r2
 8005d0e:	f841 2023 	str.w	r2, [r1, r3, lsl #2]
 8005d12:	bf00      	nop
 8005d14:	370c      	adds	r7, #12
 8005d16:	46bd      	mov	sp, r7
 8005d18:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005d1c:	4770      	bx	lr
 8005d1e:	bf00      	nop
 8005d20:	e000e100 	.word	0xe000e100

08005d24 <NVIC_SetPriority>:
 8005d24:	b480      	push	{r7}
 8005d26:	b083      	sub	sp, #12
 8005d28:	af00      	add	r7, sp, #0
 8005d2a:	4603      	mov	r3, r0
 8005d2c:	6039      	str	r1, [r7, #0]
 8005d2e:	71fb      	strb	r3, [r7, #7]
 8005d30:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8005d34:	2b00      	cmp	r3, #0
 8005d36:	da0b      	bge.n	8005d50 <NVIC_SetPriority+0x2c>
 8005d38:	490d      	ldr	r1, [pc, #52]	; (8005d70 <NVIC_SetPriority+0x4c>)
 8005d3a:	79fb      	ldrb	r3, [r7, #7]
 8005d3c:	f003 030f 	and.w	r3, r3, #15
 8005d40:	3b04      	subs	r3, #4
 8005d42:	683a      	ldr	r2, [r7, #0]
 8005d44:	b2d2      	uxtb	r2, r2
 8005d46:	0112      	lsls	r2, r2, #4
 8005d48:	b2d2      	uxtb	r2, r2
 8005d4a:	440b      	add	r3, r1
 8005d4c:	761a      	strb	r2, [r3, #24]
 8005d4e:	e009      	b.n	8005d64 <NVIC_SetPriority+0x40>
 8005d50:	4908      	ldr	r1, [pc, #32]	; (8005d74 <NVIC_SetPriority+0x50>)
 8005d52:	f997 3007 	ldrsb.w	r3, [r7, #7]
 8005d56:	683a      	ldr	r2, [r7, #0]
 8005d58:	b2d2      	uxtb	r2, r2
 8005d5a:	0112      	lsls	r2, r2, #4
 8005d5c:	b2d2      	uxtb	r2, r2
 8005d5e:	440b      	add	r3, r1
 8005d60:	f883 2300 	strb.w	r2, [r3, #768]	; 0x300
 8005d64:	bf00      	nop
 8005d66:	370c      	adds	r7, #12
 8005d68:	46bd      	mov	sp, r7
 8005d6a:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005d6e:	4770      	bx	lr
 8005d70:	e000ed00 	.word	0xe000ed00
 8005d74:	e000e100 	.word	0xe000e100

08005d78 <LL_AHB1_GRP1_EnableClock>:
 8005d78:	b480      	push	{r7}
 8005d7a:	b085      	sub	sp, #20
 8005d7c:	af00      	add	r7, sp, #0
 8005d7e:	6078      	str	r0, [r7, #4]
 8005d80:	4908      	ldr	r1, [pc, #32]	; (8005da4 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8005d82:	4b08      	ldr	r3, [pc, #32]	; (8005da4 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8005d84:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8005d86:	687b      	ldr	r3, [r7, #4]
 8005d88:	4313      	orrs	r3, r2
 8005d8a:	630b      	str	r3, [r1, #48]	; 0x30
 8005d8c:	4b05      	ldr	r3, [pc, #20]	; (8005da4 <LL_AHB1_GRP1_EnableClock+0x2c>)
 8005d8e:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 8005d90:	687b      	ldr	r3, [r7, #4]
 8005d92:	4013      	ands	r3, r2
 8005d94:	60fb      	str	r3, [r7, #12]
 8005d96:	68fb      	ldr	r3, [r7, #12]
 8005d98:	bf00      	nop
 8005d9a:	3714      	adds	r7, #20
 8005d9c:	46bd      	mov	sp, r7
 8005d9e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005da2:	4770      	bx	lr
 8005da4:	40023800 	.word	0x40023800

08005da8 <LL_APB1_GRP1_EnableClock>:
 8005da8:	b480      	push	{r7}
 8005daa:	b085      	sub	sp, #20
 8005dac:	af00      	add	r7, sp, #0
 8005dae:	6078      	str	r0, [r7, #4]
 8005db0:	4908      	ldr	r1, [pc, #32]	; (8005dd4 <LL_APB1_GRP1_EnableClock+0x2c>)
 8005db2:	4b08      	ldr	r3, [pc, #32]	; (8005dd4 <LL_APB1_GRP1_EnableClock+0x2c>)
 8005db4:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8005db6:	687b      	ldr	r3, [r7, #4]
 8005db8:	4313      	orrs	r3, r2
 8005dba:	640b      	str	r3, [r1, #64]	; 0x40
 8005dbc:	4b05      	ldr	r3, [pc, #20]	; (8005dd4 <LL_APB1_GRP1_EnableClock+0x2c>)
 8005dbe:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 8005dc0:	687b      	ldr	r3, [r7, #4]
 8005dc2:	4013      	ands	r3, r2
 8005dc4:	60fb      	str	r3, [r7, #12]
 8005dc6:	68fb      	ldr	r3, [r7, #12]
 8005dc8:	bf00      	nop
 8005dca:	3714      	adds	r7, #20
 8005dcc:	46bd      	mov	sp, r7
 8005dce:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005dd2:	4770      	bx	lr
 8005dd4:	40023800 	.word	0x40023800

08005dd8 <LL_TIM_EnableCounter>:
 8005dd8:	b480      	push	{r7}
 8005dda:	b083      	sub	sp, #12
 8005ddc:	af00      	add	r7, sp, #0
 8005dde:	6078      	str	r0, [r7, #4]
 8005de0:	687b      	ldr	r3, [r7, #4]
 8005de2:	681b      	ldr	r3, [r3, #0]
 8005de4:	f043 0201 	orr.w	r2, r3, #1
 8005de8:	687b      	ldr	r3, [r7, #4]
 8005dea:	601a      	str	r2, [r3, #0]
 8005dec:	bf00      	nop
 8005dee:	370c      	adds	r7, #12
 8005df0:	46bd      	mov	sp, r7
 8005df2:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005df6:	4770      	bx	lr

08005df8 <LL_TIM_SetCounterMode>:
 8005df8:	b480      	push	{r7}
 8005dfa:	b083      	sub	sp, #12
 8005dfc:	af00      	add	r7, sp, #0
 8005dfe:	6078      	str	r0, [r7, #4]
 8005e00:	6039      	str	r1, [r7, #0]
 8005e02:	687b      	ldr	r3, [r7, #4]
 8005e04:	681b      	ldr	r3, [r3, #0]
 8005e06:	f023 0270 	bic.w	r2, r3, #112	; 0x70
 8005e0a:	683b      	ldr	r3, [r7, #0]
 8005e0c:	431a      	orrs	r2, r3
 8005e0e:	687b      	ldr	r3, [r7, #4]
 8005e10:	601a      	str	r2, [r3, #0]
 8005e12:	bf00      	nop
 8005e14:	370c      	adds	r7, #12
 8005e16:	46bd      	mov	sp, r7
 8005e18:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005e1c:	4770      	bx	lr

08005e1e <LL_TIM_SetPrescaler>:
 8005e1e:	b480      	push	{r7}
 8005e20:	b083      	sub	sp, #12
 8005e22:	af00      	add	r7, sp, #0
 8005e24:	6078      	str	r0, [r7, #4]
 8005e26:	6039      	str	r1, [r7, #0]
 8005e28:	687b      	ldr	r3, [r7, #4]
 8005e2a:	683a      	ldr	r2, [r7, #0]
 8005e2c:	629a      	str	r2, [r3, #40]	; 0x28
 8005e2e:	bf00      	nop
 8005e30:	370c      	adds	r7, #12
 8005e32:	46bd      	mov	sp, r7
 8005e34:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005e38:	4770      	bx	lr

08005e3a <LL_TIM_SetAutoReload>:
 8005e3a:	b480      	push	{r7}
 8005e3c:	b083      	sub	sp, #12
 8005e3e:	af00      	add	r7, sp, #0
 8005e40:	6078      	str	r0, [r7, #4]
 8005e42:	6039      	str	r1, [r7, #0]
 8005e44:	687b      	ldr	r3, [r7, #4]
 8005e46:	683a      	ldr	r2, [r7, #0]
 8005e48:	62da      	str	r2, [r3, #44]	; 0x2c
 8005e4a:	bf00      	nop
 8005e4c:	370c      	adds	r7, #12
 8005e4e:	46bd      	mov	sp, r7
 8005e50:	f85d 7b04 	ldr.w	r7, [sp], #4
 8005e54:	4770      	bx	lr
	...

08005e58 <LL_TIM_IC_SetActiveInput>:
 8005e58:	b4b0      	push	{r4, r5, r7}
 8005e5a:	b085      	sub	sp, #20
 8005e5c:	af00      	add	r7, sp, #0
 8005e5e:	60f8      	str	r0, [r7, #12]
 8005e60:	60b9      	str	r1, [r7, #8]
 8005e62:	607a      	str	r2, [r7, #4]
 8005e64:	68bb      	ldr	r3, [r7, #8]
 8005e66:	2b01      	cmp	r3, #1
 8005e68:	d01c      	beq.n	8005ea4 <LL_TIM_IC_SetActiveInput+0x4c>
 8005e6a:	68bb      	ldr	r3, [r7, #8]
 8005e6c:	2b04      	cmp	r3, #4
 8005e6e:	d017      	beq.n	8005ea0 <LL_TIM_IC_SetActiveInput+0x48>
 8005e70:	68bb      	ldr	r3, [r7, #8]
 8005e72:	2b10      	cmp	r3, #16
 8005e74:	d012      	beq.n	8005e9c <LL_TIM_IC_SetActiveInput+0x44>
 8005e76:	68bb      	ldr	r3, [r7, #8]
 8005e78:	2b40      	cmp	r3, #64	; 0x40
 8005e7a:	d00d      	beq.n	8005e98 <LL_TIM_IC_SetActiveInput+0x40>
 8005e7c:	68bb      	ldr	r3, [r7, #8]
 8005e7e:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8005e82:	d007      	beq.n	8005e94 <LL_TIM_IC_SetActiveInput+0x3c>
 8005e84:	68bb      	ldr	r3, [r7, #8]
 8005e86:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8005e8a:	d101      	bne.n	8005e90 <LL_TIM_IC_SetActiveInput+0x38>
 8005e8c:	2305      	movs	r3, #5
 8005e8e:	e00a      	b.n	8005ea6 <LL_TIM_IC_SetActiveInput+0x4e>
 8005e90:	2306      	movs	r3, #6
 8005e92:	e008      	b.n	8005ea6 <LL_TIM_IC_SetActiveInput+0x4e>
 8005e94:	2304      	movs	r3, #4
 8005e96:	e006      	b.n	8005ea6 <LL_TIM_IC_SetActiveInput+0x4e>
 8005e98:	2303      	movs	r3, #3
 8005e9a:	e004      	b.n	8005ea6 <LL_TIM_IC_SetActiveInput+0x4e>
 8005e9c:	2302      	movs	r3, #2
 8005e9e:	e002      	b.n	8005ea6 <LL_TIM_IC_SetActiveInput+0x4e>
 8005ea0:	2301      	movs	r3, #1
 8005ea2:	e000      	b.n	8005ea6 <LL_TIM_IC_SetActiveInput+0x4e>
 8005ea4:	2300      	movs	r3, #0
 8005ea6:	461d      	mov	r5, r3
 8005ea8:	68fb      	ldr	r3, [r7, #12]
 8005eaa:	3318      	adds	r3, #24
 8005eac:	461a      	mov	r2, r3
 8005eae:	4629      	mov	r1, r5
 8005eb0:	4b0c      	ldr	r3, [pc, #48]	; (8005ee4 <LL_TIM_IC_SetActiveInput+0x8c>)
 8005eb2:	5c5b      	ldrb	r3, [r3, r1]
 8005eb4:	4413      	add	r3, r2
 8005eb6:	461c      	mov	r4, r3
 8005eb8:	6822      	ldr	r2, [r4, #0]
 8005eba:	4629      	mov	r1, r5
 8005ebc:	4b0a      	ldr	r3, [pc, #40]	; (8005ee8 <LL_TIM_IC_SetActiveInput+0x90>)
 8005ebe:	5c5b      	ldrb	r3, [r3, r1]
 8005ec0:	4619      	mov	r1, r3
 8005ec2:	2303      	movs	r3, #3
 8005ec4:	408b      	lsls	r3, r1
 8005ec6:	43db      	mvns	r3, r3
 8005ec8:	401a      	ands	r2, r3
 8005eca:	687b      	ldr	r3, [r7, #4]
 8005ecc:	0c1b      	lsrs	r3, r3, #16
 8005ece:	4628      	mov	r0, r5
 8005ed0:	4905      	ldr	r1, [pc, #20]	; (8005ee8 <LL_TIM_IC_SetActiveInput+0x90>)
 8005ed2:	5c09      	ldrb	r1, [r1, r0]
 8005ed4:	408b      	lsls	r3, r1
 8005ed6:	4313      	orrs	r3, r2
 8005ed8:	6023      	str	r3, [r4, #0]
 8005eda:	bf00      	nop
 8005edc:	3714      	adds	r7, #20
 8005ede:	46bd      	mov	sp, r7
 8005ee0:	bcb0      	pop	{r4, r5, r7}
 8005ee2:	4770      	bx	lr
 8005ee4:	08009d5c 	.word	0x08009d5c
 8005ee8:	08009d64 	.word	0x08009d64

08005eec <LL_TIM_IC_SetPrescaler>:
 8005eec:	b4b0      	push	{r4, r5, r7}
 8005eee:	b085      	sub	sp, #20
 8005ef0:	af00      	add	r7, sp, #0
 8005ef2:	60f8      	str	r0, [r7, #12]
 8005ef4:	60b9      	str	r1, [r7, #8]
 8005ef6:	607a      	str	r2, [r7, #4]
 8005ef8:	68bb      	ldr	r3, [r7, #8]
 8005efa:	2b01      	cmp	r3, #1
 8005efc:	d01c      	beq.n	8005f38 <LL_TIM_IC_SetPrescaler+0x4c>
 8005efe:	68bb      	ldr	r3, [r7, #8]
 8005f00:	2b04      	cmp	r3, #4
 8005f02:	d017      	beq.n	8005f34 <LL_TIM_IC_SetPrescaler+0x48>
 8005f04:	68bb      	ldr	r3, [r7, #8]
 8005f06:	2b10      	cmp	r3, #16
 8005f08:	d012      	beq.n	8005f30 <LL_TIM_IC_SetPrescaler+0x44>
 8005f0a:	68bb      	ldr	r3, [r7, #8]
 8005f0c:	2b40      	cmp	r3, #64	; 0x40
 8005f0e:	d00d      	beq.n	8005f2c <LL_TIM_IC_SetPrescaler+0x40>
 8005f10:	68bb      	ldr	r3, [r7, #8]
 8005f12:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8005f16:	d007      	beq.n	8005f28 <LL_TIM_IC_SetPrescaler+0x3c>
 8005f18:	68bb      	ldr	r3, [r7, #8]
 8005f1a:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8005f1e:	d101      	bne.n	8005f24 <LL_TIM_IC_SetPrescaler+0x38>
 8005f20:	2305      	movs	r3, #5
 8005f22:	e00a      	b.n	8005f3a <LL_TIM_IC_SetPrescaler+0x4e>
 8005f24:	2306      	movs	r3, #6
 8005f26:	e008      	b.n	8005f3a <LL_TIM_IC_SetPrescaler+0x4e>
 8005f28:	2304      	movs	r3, #4
 8005f2a:	e006      	b.n	8005f3a <LL_TIM_IC_SetPrescaler+0x4e>
 8005f2c:	2303      	movs	r3, #3
 8005f2e:	e004      	b.n	8005f3a <LL_TIM_IC_SetPrescaler+0x4e>
 8005f30:	2302      	movs	r3, #2
 8005f32:	e002      	b.n	8005f3a <LL_TIM_IC_SetPrescaler+0x4e>
 8005f34:	2301      	movs	r3, #1
 8005f36:	e000      	b.n	8005f3a <LL_TIM_IC_SetPrescaler+0x4e>
 8005f38:	2300      	movs	r3, #0
 8005f3a:	461d      	mov	r5, r3
 8005f3c:	68fb      	ldr	r3, [r7, #12]
 8005f3e:	3318      	adds	r3, #24
 8005f40:	461a      	mov	r2, r3
 8005f42:	4629      	mov	r1, r5
 8005f44:	4b0c      	ldr	r3, [pc, #48]	; (8005f78 <LL_TIM_IC_SetPrescaler+0x8c>)
 8005f46:	5c5b      	ldrb	r3, [r3, r1]
 8005f48:	4413      	add	r3, r2
 8005f4a:	461c      	mov	r4, r3
 8005f4c:	6822      	ldr	r2, [r4, #0]
 8005f4e:	4629      	mov	r1, r5
 8005f50:	4b0a      	ldr	r3, [pc, #40]	; (8005f7c <LL_TIM_IC_SetPrescaler+0x90>)
 8005f52:	5c5b      	ldrb	r3, [r3, r1]
 8005f54:	4619      	mov	r1, r3
 8005f56:	230c      	movs	r3, #12
 8005f58:	408b      	lsls	r3, r1
 8005f5a:	43db      	mvns	r3, r3
 8005f5c:	401a      	ands	r2, r3
 8005f5e:	687b      	ldr	r3, [r7, #4]
 8005f60:	0c1b      	lsrs	r3, r3, #16
 8005f62:	4628      	mov	r0, r5
 8005f64:	4905      	ldr	r1, [pc, #20]	; (8005f7c <LL_TIM_IC_SetPrescaler+0x90>)
 8005f66:	5c09      	ldrb	r1, [r1, r0]
 8005f68:	408b      	lsls	r3, r1
 8005f6a:	4313      	orrs	r3, r2
 8005f6c:	6023      	str	r3, [r4, #0]
 8005f6e:	bf00      	nop
 8005f70:	3714      	adds	r7, #20
 8005f72:	46bd      	mov	sp, r7
 8005f74:	bcb0      	pop	{r4, r5, r7}
 8005f76:	4770      	bx	lr
 8005f78:	08009d5c 	.word	0x08009d5c
 8005f7c:	08009d64 	.word	0x08009d64

08005f80 <LL_TIM_IC_SetFilter>:
 8005f80:	b4b0      	push	{r4, r5, r7}
 8005f82:	b085      	sub	sp, #20
 8005f84:	af00      	add	r7, sp, #0
 8005f86:	60f8      	str	r0, [r7, #12]
 8005f88:	60b9      	str	r1, [r7, #8]
 8005f8a:	607a      	str	r2, [r7, #4]
 8005f8c:	68bb      	ldr	r3, [r7, #8]
 8005f8e:	2b01      	cmp	r3, #1
 8005f90:	d01c      	beq.n	8005fcc <LL_TIM_IC_SetFilter+0x4c>
 8005f92:	68bb      	ldr	r3, [r7, #8]
 8005f94:	2b04      	cmp	r3, #4
 8005f96:	d017      	beq.n	8005fc8 <LL_TIM_IC_SetFilter+0x48>
 8005f98:	68bb      	ldr	r3, [r7, #8]
 8005f9a:	2b10      	cmp	r3, #16
 8005f9c:	d012      	beq.n	8005fc4 <LL_TIM_IC_SetFilter+0x44>
 8005f9e:	68bb      	ldr	r3, [r7, #8]
 8005fa0:	2b40      	cmp	r3, #64	; 0x40
 8005fa2:	d00d      	beq.n	8005fc0 <LL_TIM_IC_SetFilter+0x40>
 8005fa4:	68bb      	ldr	r3, [r7, #8]
 8005fa6:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 8005faa:	d007      	beq.n	8005fbc <LL_TIM_IC_SetFilter+0x3c>
 8005fac:	68bb      	ldr	r3, [r7, #8]
 8005fae:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8005fb2:	d101      	bne.n	8005fb8 <LL_TIM_IC_SetFilter+0x38>
 8005fb4:	2305      	movs	r3, #5
 8005fb6:	e00a      	b.n	8005fce <LL_TIM_IC_SetFilter+0x4e>
 8005fb8:	2306      	movs	r3, #6
 8005fba:	e008      	b.n	8005fce <LL_TIM_IC_SetFilter+0x4e>
 8005fbc:	2304      	movs	r3, #4
 8005fbe:	e006      	b.n	8005fce <LL_TIM_IC_SetFilter+0x4e>
 8005fc0:	2303      	movs	r3, #3
 8005fc2:	e004      	b.n	8005fce <LL_TIM_IC_SetFilter+0x4e>
 8005fc4:	2302      	movs	r3, #2
 8005fc6:	e002      	b.n	8005fce <LL_TIM_IC_SetFilter+0x4e>
 8005fc8:	2301      	movs	r3, #1
 8005fca:	e000      	b.n	8005fce <LL_TIM_IC_SetFilter+0x4e>
 8005fcc:	2300      	movs	r3, #0
 8005fce:	461d      	mov	r5, r3
 8005fd0:	68fb      	ldr	r3, [r7, #12]
 8005fd2:	3318      	adds	r3, #24
 8005fd4:	461a      	mov	r2, r3
 8005fd6:	4629      	mov	r1, r5
 8005fd8:	4b0c      	ldr	r3, [pc, #48]	; (800600c <LL_TIM_IC_SetFilter+0x8c>)
 8005fda:	5c5b      	ldrb	r3, [r3, r1]
 8005fdc:	4413      	add	r3, r2
 8005fde:	461c      	mov	r4, r3
 8005fe0:	6822      	ldr	r2, [r4, #0]
 8005fe2:	4629      	mov	r1, r5
 8005fe4:	4b0a      	ldr	r3, [pc, #40]	; (8006010 <LL_TIM_IC_SetFilter+0x90>)
 8005fe6:	5c5b      	ldrb	r3, [r3, r1]
 8005fe8:	4619      	mov	r1, r3
 8005fea:	23f0      	movs	r3, #240	; 0xf0
 8005fec:	408b      	lsls	r3, r1
 8005fee:	43db      	mvns	r3, r3
 8005ff0:	401a      	ands	r2, r3
 8005ff2:	687b      	ldr	r3, [r7, #4]
 8005ff4:	0c1b      	lsrs	r3, r3, #16
 8005ff6:	4628      	mov	r0, r5
 8005ff8:	4905      	ldr	r1, [pc, #20]	; (8006010 <LL_TIM_IC_SetFilter+0x90>)
 8005ffa:	5c09      	ldrb	r1, [r1, r0]
 8005ffc:	408b      	lsls	r3, r1
 8005ffe:	4313      	orrs	r3, r2
 8006000:	6023      	str	r3, [r4, #0]
 8006002:	bf00      	nop
 8006004:	3714      	adds	r7, #20
 8006006:	46bd      	mov	sp, r7
 8006008:	bcb0      	pop	{r4, r5, r7}
 800600a:	4770      	bx	lr
 800600c:	08009d5c 	.word	0x08009d5c
 8006010:	08009d64 	.word	0x08009d64

08006014 <LL_TIM_IC_SetPolarity>:
 8006014:	b490      	push	{r4, r7}
 8006016:	b084      	sub	sp, #16
 8006018:	af00      	add	r7, sp, #0
 800601a:	60f8      	str	r0, [r7, #12]
 800601c:	60b9      	str	r1, [r7, #8]
 800601e:	607a      	str	r2, [r7, #4]
 8006020:	68bb      	ldr	r3, [r7, #8]
 8006022:	2b01      	cmp	r3, #1
 8006024:	d01c      	beq.n	8006060 <LL_TIM_IC_SetPolarity+0x4c>
 8006026:	68bb      	ldr	r3, [r7, #8]
 8006028:	2b04      	cmp	r3, #4
 800602a:	d017      	beq.n	800605c <LL_TIM_IC_SetPolarity+0x48>
 800602c:	68bb      	ldr	r3, [r7, #8]
 800602e:	2b10      	cmp	r3, #16
 8006030:	d012      	beq.n	8006058 <LL_TIM_IC_SetPolarity+0x44>
 8006032:	68bb      	ldr	r3, [r7, #8]
 8006034:	2b40      	cmp	r3, #64	; 0x40
 8006036:	d00d      	beq.n	8006054 <LL_TIM_IC_SetPolarity+0x40>
 8006038:	68bb      	ldr	r3, [r7, #8]
 800603a:	f5b3 7f80 	cmp.w	r3, #256	; 0x100
 800603e:	d007      	beq.n	8006050 <LL_TIM_IC_SetPolarity+0x3c>
 8006040:	68bb      	ldr	r3, [r7, #8]
 8006042:	f5b3 6f80 	cmp.w	r3, #1024	; 0x400
 8006046:	d101      	bne.n	800604c <LL_TIM_IC_SetPolarity+0x38>
 8006048:	2305      	movs	r3, #5
 800604a:	e00a      	b.n	8006062 <LL_TIM_IC_SetPolarity+0x4e>
 800604c:	2306      	movs	r3, #6
 800604e:	e008      	b.n	8006062 <LL_TIM_IC_SetPolarity+0x4e>
 8006050:	2304      	movs	r3, #4
 8006052:	e006      	b.n	8006062 <LL_TIM_IC_SetPolarity+0x4e>
 8006054:	2303      	movs	r3, #3
 8006056:	e004      	b.n	8006062 <LL_TIM_IC_SetPolarity+0x4e>
 8006058:	2302      	movs	r3, #2
 800605a:	e002      	b.n	8006062 <LL_TIM_IC_SetPolarity+0x4e>
 800605c:	2301      	movs	r3, #1
 800605e:	e000      	b.n	8006062 <LL_TIM_IC_SetPolarity+0x4e>
 8006060:	2300      	movs	r3, #0
 8006062:	461c      	mov	r4, r3
 8006064:	68fb      	ldr	r3, [r7, #12]
 8006066:	6a1a      	ldr	r2, [r3, #32]
 8006068:	4621      	mov	r1, r4
 800606a:	4b0a      	ldr	r3, [pc, #40]	; (8006094 <LL_TIM_IC_SetPolarity+0x80>)
 800606c:	5c5b      	ldrb	r3, [r3, r1]
 800606e:	4619      	mov	r1, r3
 8006070:	230a      	movs	r3, #10
 8006072:	408b      	lsls	r3, r1
 8006074:	43db      	mvns	r3, r3
 8006076:	401a      	ands	r2, r3
 8006078:	4621      	mov	r1, r4
 800607a:	4b06      	ldr	r3, [pc, #24]	; (8006094 <LL_TIM_IC_SetPolarity+0x80>)
 800607c:	5c5b      	ldrb	r3, [r3, r1]
 800607e:	4619      	mov	r1, r3
 8006080:	687b      	ldr	r3, [r7, #4]
 8006082:	408b      	lsls	r3, r1
 8006084:	431a      	orrs	r2, r3
 8006086:	68fb      	ldr	r3, [r7, #12]
 8006088:	621a      	str	r2, [r3, #32]
 800608a:	bf00      	nop
 800608c:	3710      	adds	r7, #16
 800608e:	46bd      	mov	sp, r7
 8006090:	bc90      	pop	{r4, r7}
 8006092:	4770      	bx	lr
 8006094:	08009d6c 	.word	0x08009d6c

08006098 <LL_TIM_SetEncoderMode>:
 8006098:	b480      	push	{r7}
 800609a:	b083      	sub	sp, #12
 800609c:	af00      	add	r7, sp, #0
 800609e:	6078      	str	r0, [r7, #4]
 80060a0:	6039      	str	r1, [r7, #0]
 80060a2:	687b      	ldr	r3, [r7, #4]
 80060a4:	689b      	ldr	r3, [r3, #8]
 80060a6:	f023 0207 	bic.w	r2, r3, #7
 80060aa:	683b      	ldr	r3, [r7, #0]
 80060ac:	431a      	orrs	r2, r3
 80060ae:	687b      	ldr	r3, [r7, #4]
 80060b0:	609a      	str	r2, [r3, #8]
 80060b2:	bf00      	nop
 80060b4:	370c      	adds	r7, #12
 80060b6:	46bd      	mov	sp, r7
 80060b8:	f85d 7b04 	ldr.w	r7, [sp], #4
 80060bc:	4770      	bx	lr

080060be <LL_TIM_ClearFlag_UPDATE>:
 80060be:	b480      	push	{r7}
 80060c0:	b083      	sub	sp, #12
 80060c2:	af00      	add	r7, sp, #0
 80060c4:	6078      	str	r0, [r7, #4]
 80060c6:	687b      	ldr	r3, [r7, #4]
 80060c8:	f06f 0201 	mvn.w	r2, #1
 80060cc:	611a      	str	r2, [r3, #16]
 80060ce:	bf00      	nop
 80060d0:	370c      	adds	r7, #12
 80060d2:	46bd      	mov	sp, r7
 80060d4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80060d8:	4770      	bx	lr

080060da <LL_TIM_IsActiveFlag_UPDATE>:
 80060da:	b480      	push	{r7}
 80060dc:	b083      	sub	sp, #12
 80060de:	af00      	add	r7, sp, #0
 80060e0:	6078      	str	r0, [r7, #4]
 80060e2:	687b      	ldr	r3, [r7, #4]
 80060e4:	691b      	ldr	r3, [r3, #16]
 80060e6:	f003 0301 	and.w	r3, r3, #1
 80060ea:	2b01      	cmp	r3, #1
 80060ec:	bf0c      	ite	eq
 80060ee:	2301      	moveq	r3, #1
 80060f0:	2300      	movne	r3, #0
 80060f2:	b2db      	uxtb	r3, r3
 80060f4:	4618      	mov	r0, r3
 80060f6:	370c      	adds	r7, #12
 80060f8:	46bd      	mov	sp, r7
 80060fa:	f85d 7b04 	ldr.w	r7, [sp], #4
 80060fe:	4770      	bx	lr

08006100 <LL_TIM_EnableIT_UPDATE>:
 8006100:	b480      	push	{r7}
 8006102:	b083      	sub	sp, #12
 8006104:	af00      	add	r7, sp, #0
 8006106:	6078      	str	r0, [r7, #4]
 8006108:	687b      	ldr	r3, [r7, #4]
 800610a:	68db      	ldr	r3, [r3, #12]
 800610c:	f043 0201 	orr.w	r2, r3, #1
 8006110:	687b      	ldr	r3, [r7, #4]
 8006112:	60da      	str	r2, [r3, #12]
 8006114:	bf00      	nop
 8006116:	370c      	adds	r7, #12
 8006118:	46bd      	mov	sp, r7
 800611a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800611e:	4770      	bx	lr

08006120 <LL_GPIO_SetPinMode>:
 8006120:	b480      	push	{r7}
 8006122:	b089      	sub	sp, #36	; 0x24
 8006124:	af00      	add	r7, sp, #0
 8006126:	60f8      	str	r0, [r7, #12]
 8006128:	60b9      	str	r1, [r7, #8]
 800612a:	607a      	str	r2, [r7, #4]
 800612c:	68fb      	ldr	r3, [r7, #12]
 800612e:	681a      	ldr	r2, [r3, #0]
 8006130:	68bb      	ldr	r3, [r7, #8]
 8006132:	617b      	str	r3, [r7, #20]
 8006134:	697b      	ldr	r3, [r7, #20]
 8006136:	fa93 f3a3 	rbit	r3, r3
 800613a:	613b      	str	r3, [r7, #16]
 800613c:	693b      	ldr	r3, [r7, #16]
 800613e:	fab3 f383 	clz	r3, r3
 8006142:	005b      	lsls	r3, r3, #1
 8006144:	2103      	movs	r1, #3
 8006146:	fa01 f303 	lsl.w	r3, r1, r3
 800614a:	43db      	mvns	r3, r3
 800614c:	401a      	ands	r2, r3
 800614e:	68bb      	ldr	r3, [r7, #8]
 8006150:	61fb      	str	r3, [r7, #28]
 8006152:	69fb      	ldr	r3, [r7, #28]
 8006154:	fa93 f3a3 	rbit	r3, r3
 8006158:	61bb      	str	r3, [r7, #24]
 800615a:	69bb      	ldr	r3, [r7, #24]
 800615c:	fab3 f383 	clz	r3, r3
 8006160:	005b      	lsls	r3, r3, #1
 8006162:	6879      	ldr	r1, [r7, #4]
 8006164:	fa01 f303 	lsl.w	r3, r1, r3
 8006168:	431a      	orrs	r2, r3
 800616a:	68fb      	ldr	r3, [r7, #12]
 800616c:	601a      	str	r2, [r3, #0]
 800616e:	bf00      	nop
 8006170:	3724      	adds	r7, #36	; 0x24
 8006172:	46bd      	mov	sp, r7
 8006174:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006178:	4770      	bx	lr

0800617a <LL_GPIO_SetPinOutputType>:
 800617a:	b480      	push	{r7}
 800617c:	b085      	sub	sp, #20
 800617e:	af00      	add	r7, sp, #0
 8006180:	60f8      	str	r0, [r7, #12]
 8006182:	60b9      	str	r1, [r7, #8]
 8006184:	607a      	str	r2, [r7, #4]
 8006186:	68fb      	ldr	r3, [r7, #12]
 8006188:	685a      	ldr	r2, [r3, #4]
 800618a:	68bb      	ldr	r3, [r7, #8]
 800618c:	43db      	mvns	r3, r3
 800618e:	401a      	ands	r2, r3
 8006190:	68bb      	ldr	r3, [r7, #8]
 8006192:	6879      	ldr	r1, [r7, #4]
 8006194:	fb01 f303 	mul.w	r3, r1, r3
 8006198:	431a      	orrs	r2, r3
 800619a:	68fb      	ldr	r3, [r7, #12]
 800619c:	605a      	str	r2, [r3, #4]
 800619e:	bf00      	nop
 80061a0:	3714      	adds	r7, #20
 80061a2:	46bd      	mov	sp, r7
 80061a4:	f85d 7b04 	ldr.w	r7, [sp], #4
 80061a8:	4770      	bx	lr

080061aa <LL_GPIO_SetAFPin_0_7>:
 80061aa:	b480      	push	{r7}
 80061ac:	b089      	sub	sp, #36	; 0x24
 80061ae:	af00      	add	r7, sp, #0
 80061b0:	60f8      	str	r0, [r7, #12]
 80061b2:	60b9      	str	r1, [r7, #8]
 80061b4:	607a      	str	r2, [r7, #4]
 80061b6:	68fb      	ldr	r3, [r7, #12]
 80061b8:	6a1a      	ldr	r2, [r3, #32]
 80061ba:	68bb      	ldr	r3, [r7, #8]
 80061bc:	617b      	str	r3, [r7, #20]
 80061be:	697b      	ldr	r3, [r7, #20]
 80061c0:	fa93 f3a3 	rbit	r3, r3
 80061c4:	613b      	str	r3, [r7, #16]
 80061c6:	693b      	ldr	r3, [r7, #16]
 80061c8:	fab3 f383 	clz	r3, r3
 80061cc:	009b      	lsls	r3, r3, #2
 80061ce:	210f      	movs	r1, #15
 80061d0:	fa01 f303 	lsl.w	r3, r1, r3
 80061d4:	43db      	mvns	r3, r3
 80061d6:	401a      	ands	r2, r3
 80061d8:	68bb      	ldr	r3, [r7, #8]
 80061da:	61fb      	str	r3, [r7, #28]
 80061dc:	69fb      	ldr	r3, [r7, #28]
 80061de:	fa93 f3a3 	rbit	r3, r3
 80061e2:	61bb      	str	r3, [r7, #24]
 80061e4:	69bb      	ldr	r3, [r7, #24]
 80061e6:	fab3 f383 	clz	r3, r3
 80061ea:	009b      	lsls	r3, r3, #2
 80061ec:	6879      	ldr	r1, [r7, #4]
 80061ee:	fa01 f303 	lsl.w	r3, r1, r3
 80061f2:	431a      	orrs	r2, r3
 80061f4:	68fb      	ldr	r3, [r7, #12]
 80061f6:	621a      	str	r2, [r3, #32]
 80061f8:	bf00      	nop
 80061fa:	3724      	adds	r7, #36	; 0x24
 80061fc:	46bd      	mov	sp, r7
 80061fe:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006202:	4770      	bx	lr

08006204 <LL_GPIO_SetAFPin_8_15>:
 8006204:	b480      	push	{r7}
 8006206:	b089      	sub	sp, #36	; 0x24
 8006208:	af00      	add	r7, sp, #0
 800620a:	60f8      	str	r0, [r7, #12]
 800620c:	60b9      	str	r1, [r7, #8]
 800620e:	607a      	str	r2, [r7, #4]
 8006210:	68fb      	ldr	r3, [r7, #12]
 8006212:	6a5a      	ldr	r2, [r3, #36]	; 0x24
 8006214:	68bb      	ldr	r3, [r7, #8]
 8006216:	0a1b      	lsrs	r3, r3, #8
 8006218:	617b      	str	r3, [r7, #20]
 800621a:	697b      	ldr	r3, [r7, #20]
 800621c:	fa93 f3a3 	rbit	r3, r3
 8006220:	613b      	str	r3, [r7, #16]
 8006222:	693b      	ldr	r3, [r7, #16]
 8006224:	fab3 f383 	clz	r3, r3
 8006228:	009b      	lsls	r3, r3, #2
 800622a:	210f      	movs	r1, #15
 800622c:	fa01 f303 	lsl.w	r3, r1, r3
 8006230:	43db      	mvns	r3, r3
 8006232:	401a      	ands	r2, r3
 8006234:	68bb      	ldr	r3, [r7, #8]
 8006236:	0a1b      	lsrs	r3, r3, #8
 8006238:	61fb      	str	r3, [r7, #28]
 800623a:	69fb      	ldr	r3, [r7, #28]
 800623c:	fa93 f3a3 	rbit	r3, r3
 8006240:	61bb      	str	r3, [r7, #24]
 8006242:	69bb      	ldr	r3, [r7, #24]
 8006244:	fab3 f383 	clz	r3, r3
 8006248:	009b      	lsls	r3, r3, #2
 800624a:	6879      	ldr	r1, [r7, #4]
 800624c:	fa01 f303 	lsl.w	r3, r1, r3
 8006250:	431a      	orrs	r2, r3
 8006252:	68fb      	ldr	r3, [r7, #12]
 8006254:	625a      	str	r2, [r3, #36]	; 0x24
 8006256:	bf00      	nop
 8006258:	3724      	adds	r7, #36	; 0x24
 800625a:	46bd      	mov	sp, r7
 800625c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006260:	4770      	bx	lr
	...

08006264 <normalize_angle>:
 8006264:	b480      	push	{r7}
 8006266:	b083      	sub	sp, #12
 8006268:	af00      	add	r7, sp, #0
 800626a:	ed87 0a01 	vstr	s0, [r7, #4]
 800626e:	edd7 7a01 	vldr	s15, [r7, #4]
 8006272:	ed9f 7a13 	vldr	s14, [pc, #76]	; 80062c0 <normalize_angle+0x5c>
 8006276:	eef4 7ac7 	vcmpe.f32	s15, s14
 800627a:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800627e:	dd06      	ble.n	800628e <normalize_angle+0x2a>
 8006280:	edd7 7a01 	vldr	s15, [r7, #4]
 8006284:	ed9f 7a0e 	vldr	s14, [pc, #56]	; 80062c0 <normalize_angle+0x5c>
 8006288:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800628c:	e011      	b.n	80062b2 <normalize_angle+0x4e>
 800628e:	edd7 7a01 	vldr	s15, [r7, #4]
 8006292:	ed9f 7a0c 	vldr	s14, [pc, #48]	; 80062c4 <normalize_angle+0x60>
 8006296:	eef4 7ac7 	vcmpe.f32	s15, s14
 800629a:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800629e:	d506      	bpl.n	80062ae <normalize_angle+0x4a>
 80062a0:	edd7 7a01 	vldr	s15, [r7, #4]
 80062a4:	ed9f 7a06 	vldr	s14, [pc, #24]	; 80062c0 <normalize_angle+0x5c>
 80062a8:	ee77 7a87 	vadd.f32	s15, s15, s14
 80062ac:	e001      	b.n	80062b2 <normalize_angle+0x4e>
 80062ae:	edd7 7a01 	vldr	s15, [r7, #4]
 80062b2:	eeb0 0a67 	vmov.f32	s0, s15
 80062b6:	370c      	adds	r7, #12
 80062b8:	46bd      	mov	sp, r7
 80062ba:	f85d 7b04 	ldr.w	r7, [sp], #4
 80062be:	4770      	bx	lr
 80062c0:	40c90fdb 	.word	0x40c90fdb
 80062c4:	c0c90fdb 	.word	0xc0c90fdb

080062c8 <odom_hw_config>:
 80062c8:	b580      	push	{r7, lr}
 80062ca:	af00      	add	r7, sp, #0
 80062cc:	2001      	movs	r0, #1
 80062ce:	f7ff fd53 	bl	8005d78 <LL_AHB1_GRP1_EnableClock>
 80062d2:	2002      	movs	r0, #2
 80062d4:	f7ff fd50 	bl	8005d78 <LL_AHB1_GRP1_EnableClock>
 80062d8:	2008      	movs	r0, #8
 80062da:	f7ff fd4d 	bl	8005d78 <LL_AHB1_GRP1_EnableClock>
 80062de:	2001      	movs	r0, #1
 80062e0:	f7ff fd62 	bl	8005da8 <LL_APB1_GRP1_EnableClock>
 80062e4:	2004      	movs	r0, #4
 80062e6:	f7ff fd5f 	bl	8005da8 <LL_APB1_GRP1_EnableClock>
 80062ea:	2008      	movs	r0, #8
 80062ec:	f7ff fd5c 	bl	8005da8 <LL_APB1_GRP1_EnableClock>
 80062f0:	2010      	movs	r0, #16
 80062f2:	f7ff fd59 	bl	8005da8 <LL_APB1_GRP1_EnableClock>
 80062f6:	2202      	movs	r2, #2
 80062f8:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 80062fc:	4892      	ldr	r0, [pc, #584]	; (8006548 <odom_hw_config+0x280>)
 80062fe:	f7ff ff0f 	bl	8006120 <LL_GPIO_SetPinMode>
 8006302:	2201      	movs	r2, #1
 8006304:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 8006308:	488f      	ldr	r0, [pc, #572]	; (8006548 <odom_hw_config+0x280>)
 800630a:	f7ff ff7b 	bl	8006204 <LL_GPIO_SetAFPin_8_15>
 800630e:	2200      	movs	r2, #0
 8006310:	f44f 4100 	mov.w	r1, #32768	; 0x8000
 8006314:	488c      	ldr	r0, [pc, #560]	; (8006548 <odom_hw_config+0x280>)
 8006316:	f7ff ff30 	bl	800617a <LL_GPIO_SetPinOutputType>
 800631a:	2202      	movs	r2, #2
 800631c:	2108      	movs	r1, #8
 800631e:	488b      	ldr	r0, [pc, #556]	; (800654c <odom_hw_config+0x284>)
 8006320:	f7ff fefe 	bl	8006120 <LL_GPIO_SetPinMode>
 8006324:	2201      	movs	r2, #1
 8006326:	2108      	movs	r1, #8
 8006328:	4888      	ldr	r0, [pc, #544]	; (800654c <odom_hw_config+0x284>)
 800632a:	f7ff ff3e 	bl	80061aa <LL_GPIO_SetAFPin_0_7>
 800632e:	2200      	movs	r2, #0
 8006330:	2108      	movs	r1, #8
 8006332:	4886      	ldr	r0, [pc, #536]	; (800654c <odom_hw_config+0x284>)
 8006334:	f7ff ff21 	bl	800617a <LL_GPIO_SetPinOutputType>
 8006338:	2103      	movs	r1, #3
 800633a:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800633e:	f7ff feab 	bl	8006098 <LL_TIM_SetEncoderMode>
 8006342:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 8006346:	2101      	movs	r1, #1
 8006348:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800634c:	f7ff fd84 	bl	8005e58 <LL_TIM_IC_SetActiveInput>
 8006350:	2200      	movs	r2, #0
 8006352:	2101      	movs	r1, #1
 8006354:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8006358:	f7ff fdc8 	bl	8005eec <LL_TIM_IC_SetPrescaler>
 800635c:	2200      	movs	r2, #0
 800635e:	2101      	movs	r1, #1
 8006360:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8006364:	f7ff fe0c 	bl	8005f80 <LL_TIM_IC_SetFilter>
 8006368:	2200      	movs	r2, #0
 800636a:	2101      	movs	r1, #1
 800636c:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8006370:	f7ff fe50 	bl	8006014 <LL_TIM_IC_SetPolarity>
 8006374:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 8006378:	2110      	movs	r1, #16
 800637a:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800637e:	f7ff fd6b 	bl	8005e58 <LL_TIM_IC_SetActiveInput>
 8006382:	2200      	movs	r2, #0
 8006384:	2110      	movs	r1, #16
 8006386:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 800638a:	f7ff fdaf 	bl	8005eec <LL_TIM_IC_SetPrescaler>
 800638e:	2200      	movs	r2, #0
 8006390:	2110      	movs	r1, #16
 8006392:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 8006396:	f7ff fdf3 	bl	8005f80 <LL_TIM_IC_SetFilter>
 800639a:	2200      	movs	r2, #0
 800639c:	2110      	movs	r1, #16
 800639e:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 80063a2:	f7ff fe37 	bl	8006014 <LL_TIM_IC_SetPolarity>
 80063a6:	4b6a      	ldr	r3, [pc, #424]	; (8006550 <odom_hw_config+0x288>)
 80063a8:	f247 5230 	movw	r2, #30000	; 0x7530
 80063ac:	801a      	strh	r2, [r3, #0]
 80063ae:	f04f 4080 	mov.w	r0, #1073741824	; 0x40000000
 80063b2:	f7ff fd11 	bl	8005dd8 <LL_TIM_EnableCounter>
 80063b6:	2202      	movs	r2, #2
 80063b8:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 80063bc:	4865      	ldr	r0, [pc, #404]	; (8006554 <odom_hw_config+0x28c>)
 80063be:	f7ff feaf 	bl	8006120 <LL_GPIO_SetPinMode>
 80063c2:	2202      	movs	r2, #2
 80063c4:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 80063c8:	4862      	ldr	r0, [pc, #392]	; (8006554 <odom_hw_config+0x28c>)
 80063ca:	f7ff ff1b 	bl	8006204 <LL_GPIO_SetAFPin_8_15>
 80063ce:	2200      	movs	r2, #0
 80063d0:	f44f 5180 	mov.w	r1, #4096	; 0x1000
 80063d4:	485f      	ldr	r0, [pc, #380]	; (8006554 <odom_hw_config+0x28c>)
 80063d6:	f7ff fed0 	bl	800617a <LL_GPIO_SetPinOutputType>
 80063da:	2202      	movs	r2, #2
 80063dc:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 80063e0:	485c      	ldr	r0, [pc, #368]	; (8006554 <odom_hw_config+0x28c>)
 80063e2:	f7ff fe9d 	bl	8006120 <LL_GPIO_SetPinMode>
 80063e6:	2202      	movs	r2, #2
 80063e8:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 80063ec:	4859      	ldr	r0, [pc, #356]	; (8006554 <odom_hw_config+0x28c>)
 80063ee:	f7ff ff09 	bl	8006204 <LL_GPIO_SetAFPin_8_15>
 80063f2:	2200      	movs	r2, #0
 80063f4:	f44f 5100 	mov.w	r1, #8192	; 0x2000
 80063f8:	4856      	ldr	r0, [pc, #344]	; (8006554 <odom_hw_config+0x28c>)
 80063fa:	f7ff febe 	bl	800617a <LL_GPIO_SetPinOutputType>
 80063fe:	2103      	movs	r1, #3
 8006400:	4855      	ldr	r0, [pc, #340]	; (8006558 <odom_hw_config+0x290>)
 8006402:	f7ff fe49 	bl	8006098 <LL_TIM_SetEncoderMode>
 8006406:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 800640a:	2101      	movs	r1, #1
 800640c:	4852      	ldr	r0, [pc, #328]	; (8006558 <odom_hw_config+0x290>)
 800640e:	f7ff fd23 	bl	8005e58 <LL_TIM_IC_SetActiveInput>
 8006412:	2200      	movs	r2, #0
 8006414:	2101      	movs	r1, #1
 8006416:	4850      	ldr	r0, [pc, #320]	; (8006558 <odom_hw_config+0x290>)
 8006418:	f7ff fd68 	bl	8005eec <LL_TIM_IC_SetPrescaler>
 800641c:	2200      	movs	r2, #0
 800641e:	2101      	movs	r1, #1
 8006420:	484d      	ldr	r0, [pc, #308]	; (8006558 <odom_hw_config+0x290>)
 8006422:	f7ff fdad 	bl	8005f80 <LL_TIM_IC_SetFilter>
 8006426:	2200      	movs	r2, #0
 8006428:	2101      	movs	r1, #1
 800642a:	484b      	ldr	r0, [pc, #300]	; (8006558 <odom_hw_config+0x290>)
 800642c:	f7ff fdf2 	bl	8006014 <LL_TIM_IC_SetPolarity>
 8006430:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 8006434:	2110      	movs	r1, #16
 8006436:	4848      	ldr	r0, [pc, #288]	; (8006558 <odom_hw_config+0x290>)
 8006438:	f7ff fd0e 	bl	8005e58 <LL_TIM_IC_SetActiveInput>
 800643c:	2200      	movs	r2, #0
 800643e:	2110      	movs	r1, #16
 8006440:	4845      	ldr	r0, [pc, #276]	; (8006558 <odom_hw_config+0x290>)
 8006442:	f7ff fd53 	bl	8005eec <LL_TIM_IC_SetPrescaler>
 8006446:	2200      	movs	r2, #0
 8006448:	2110      	movs	r1, #16
 800644a:	4843      	ldr	r0, [pc, #268]	; (8006558 <odom_hw_config+0x290>)
 800644c:	f7ff fd98 	bl	8005f80 <LL_TIM_IC_SetFilter>
 8006450:	2200      	movs	r2, #0
 8006452:	2110      	movs	r1, #16
 8006454:	4840      	ldr	r0, [pc, #256]	; (8006558 <odom_hw_config+0x290>)
 8006456:	f7ff fddd 	bl	8006014 <LL_TIM_IC_SetPolarity>
 800645a:	4b40      	ldr	r3, [pc, #256]	; (800655c <odom_hw_config+0x294>)
 800645c:	f247 5230 	movw	r2, #30000	; 0x7530
 8006460:	801a      	strh	r2, [r3, #0]
 8006462:	483d      	ldr	r0, [pc, #244]	; (8006558 <odom_hw_config+0x290>)
 8006464:	f7ff fcb8 	bl	8005dd8 <LL_TIM_EnableCounter>
 8006468:	2202      	movs	r2, #2
 800646a:	2101      	movs	r1, #1
 800646c:	4836      	ldr	r0, [pc, #216]	; (8006548 <odom_hw_config+0x280>)
 800646e:	f7ff fe57 	bl	8006120 <LL_GPIO_SetPinMode>
 8006472:	2202      	movs	r2, #2
 8006474:	2101      	movs	r1, #1
 8006476:	4834      	ldr	r0, [pc, #208]	; (8006548 <odom_hw_config+0x280>)
 8006478:	f7ff fe97 	bl	80061aa <LL_GPIO_SetAFPin_0_7>
 800647c:	2200      	movs	r2, #0
 800647e:	2101      	movs	r1, #1
 8006480:	4831      	ldr	r0, [pc, #196]	; (8006548 <odom_hw_config+0x280>)
 8006482:	f7ff fe7a 	bl	800617a <LL_GPIO_SetPinOutputType>
 8006486:	2202      	movs	r2, #2
 8006488:	2102      	movs	r1, #2
 800648a:	482f      	ldr	r0, [pc, #188]	; (8006548 <odom_hw_config+0x280>)
 800648c:	f7ff fe48 	bl	8006120 <LL_GPIO_SetPinMode>
 8006490:	2202      	movs	r2, #2
 8006492:	2102      	movs	r1, #2
 8006494:	482c      	ldr	r0, [pc, #176]	; (8006548 <odom_hw_config+0x280>)
 8006496:	f7ff fe88 	bl	80061aa <LL_GPIO_SetAFPin_0_7>
 800649a:	2200      	movs	r2, #0
 800649c:	2102      	movs	r1, #2
 800649e:	482a      	ldr	r0, [pc, #168]	; (8006548 <odom_hw_config+0x280>)
 80064a0:	f7ff fe6b 	bl	800617a <LL_GPIO_SetPinOutputType>
 80064a4:	2103      	movs	r1, #3
 80064a6:	482e      	ldr	r0, [pc, #184]	; (8006560 <odom_hw_config+0x298>)
 80064a8:	f7ff fdf6 	bl	8006098 <LL_TIM_SetEncoderMode>
 80064ac:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 80064b0:	2101      	movs	r1, #1
 80064b2:	482b      	ldr	r0, [pc, #172]	; (8006560 <odom_hw_config+0x298>)
 80064b4:	f7ff fcd0 	bl	8005e58 <LL_TIM_IC_SetActiveInput>
 80064b8:	2200      	movs	r2, #0
 80064ba:	2101      	movs	r1, #1
 80064bc:	4828      	ldr	r0, [pc, #160]	; (8006560 <odom_hw_config+0x298>)
 80064be:	f7ff fd15 	bl	8005eec <LL_TIM_IC_SetPrescaler>
 80064c2:	2200      	movs	r2, #0
 80064c4:	2101      	movs	r1, #1
 80064c6:	4826      	ldr	r0, [pc, #152]	; (8006560 <odom_hw_config+0x298>)
 80064c8:	f7ff fd5a 	bl	8005f80 <LL_TIM_IC_SetFilter>
 80064cc:	2200      	movs	r2, #0
 80064ce:	2101      	movs	r1, #1
 80064d0:	4823      	ldr	r0, [pc, #140]	; (8006560 <odom_hw_config+0x298>)
 80064d2:	f7ff fd9f 	bl	8006014 <LL_TIM_IC_SetPolarity>
 80064d6:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 80064da:	2110      	movs	r1, #16
 80064dc:	4820      	ldr	r0, [pc, #128]	; (8006560 <odom_hw_config+0x298>)
 80064de:	f7ff fcbb 	bl	8005e58 <LL_TIM_IC_SetActiveInput>
 80064e2:	2200      	movs	r2, #0
 80064e4:	2110      	movs	r1, #16
 80064e6:	481e      	ldr	r0, [pc, #120]	; (8006560 <odom_hw_config+0x298>)
 80064e8:	f7ff fd00 	bl	8005eec <LL_TIM_IC_SetPrescaler>
 80064ec:	2200      	movs	r2, #0
 80064ee:	2110      	movs	r1, #16
 80064f0:	481b      	ldr	r0, [pc, #108]	; (8006560 <odom_hw_config+0x298>)
 80064f2:	f7ff fd45 	bl	8005f80 <LL_TIM_IC_SetFilter>
 80064f6:	2200      	movs	r2, #0
 80064f8:	2110      	movs	r1, #16
 80064fa:	4819      	ldr	r0, [pc, #100]	; (8006560 <odom_hw_config+0x298>)
 80064fc:	f7ff fd8a 	bl	8006014 <LL_TIM_IC_SetPolarity>
 8006500:	4b18      	ldr	r3, [pc, #96]	; (8006564 <odom_hw_config+0x29c>)
 8006502:	f247 5230 	movw	r2, #30000	; 0x7530
 8006506:	801a      	strh	r2, [r3, #0]
 8006508:	4815      	ldr	r0, [pc, #84]	; (8006560 <odom_hw_config+0x298>)
 800650a:	f7ff fc65 	bl	8005dd8 <LL_TIM_EnableCounter>
 800650e:	f24a 410f 	movw	r1, #41999	; 0xa40f
 8006512:	4815      	ldr	r0, [pc, #84]	; (8006568 <odom_hw_config+0x2a0>)
 8006514:	f7ff fc91 	bl	8005e3a <LL_TIM_SetAutoReload>
 8006518:	2113      	movs	r1, #19
 800651a:	4813      	ldr	r0, [pc, #76]	; (8006568 <odom_hw_config+0x2a0>)
 800651c:	f7ff fc7f 	bl	8005e1e <LL_TIM_SetPrescaler>
 8006520:	2100      	movs	r1, #0
 8006522:	4811      	ldr	r0, [pc, #68]	; (8006568 <odom_hw_config+0x2a0>)
 8006524:	f7ff fc68 	bl	8005df8 <LL_TIM_SetCounterMode>
 8006528:	480f      	ldr	r0, [pc, #60]	; (8006568 <odom_hw_config+0x2a0>)
 800652a:	f7ff fde9 	bl	8006100 <LL_TIM_EnableIT_UPDATE>
 800652e:	2107      	movs	r1, #7
 8006530:	2036      	movs	r0, #54	; 0x36
 8006532:	f7ff fbf7 	bl	8005d24 <NVIC_SetPriority>
 8006536:	2036      	movs	r0, #54	; 0x36
 8006538:	f7ff fbda 	bl	8005cf0 <NVIC_EnableIRQ>
 800653c:	480a      	ldr	r0, [pc, #40]	; (8006568 <odom_hw_config+0x2a0>)
 800653e:	f7ff fc4b 	bl	8005dd8 <LL_TIM_EnableCounter>
 8006542:	bf00      	nop
 8006544:	bd80      	pop	{r7, pc}
 8006546:	bf00      	nop
 8006548:	40020000 	.word	0x40020000
 800654c:	40020400 	.word	0x40020400
 8006550:	40000024 	.word	0x40000024
 8006554:	40020c00 	.word	0x40020c00
 8006558:	40000800 	.word	0x40000800
 800655c:	40000824 	.word	0x40000824
 8006560:	40000c00 	.word	0x40000c00
 8006564:	40000c24 	.word	0x40000c24
 8006568:	40001000 	.word	0x40001000

0800656c <pid_current_speed>:
 800656c:	b480      	push	{r7}
 800656e:	b083      	sub	sp, #12
 8006570:	af00      	add	r7, sp, #0
 8006572:	6078      	str	r0, [r7, #4]
 8006574:	4b08      	ldr	r3, [pc, #32]	; (8006598 <pid_current_speed+0x2c>)
 8006576:	681a      	ldr	r2, [r3, #0]
 8006578:	687b      	ldr	r3, [r7, #4]
 800657a:	330a      	adds	r3, #10
 800657c:	009b      	lsls	r3, r3, #2
 800657e:	4413      	add	r3, r2
 8006580:	3304      	adds	r3, #4
 8006582:	681b      	ldr	r3, [r3, #0]
 8006584:	ee07 3a90 	vmov	s15, r3
 8006588:	eeb0 0a67 	vmov.f32	s0, s15
 800658c:	370c      	adds	r7, #12
 800658e:	46bd      	mov	sp, r7
 8006590:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006594:	4770      	bx	lr
 8006596:	bf00      	nop
 8006598:	2000120c 	.word	0x2000120c

0800659c <normalize_angle_60>:
 800659c:	b480      	push	{r7}
 800659e:	b083      	sub	sp, #12
 80065a0:	af00      	add	r7, sp, #0
 80065a2:	ed87 0a01 	vstr	s0, [r7, #4]
 80065a6:	e007      	b.n	80065b8 <normalize_angle_60+0x1c>
 80065a8:	edd7 7a01 	vldr	s15, [r7, #4]
 80065ac:	ed9f 7a25 	vldr	s14, [pc, #148]	; 8006644 <normalize_angle_60+0xa8>
 80065b0:	ee77 7ac7 	vsub.f32	s15, s15, s14
 80065b4:	edc7 7a01 	vstr	s15, [r7, #4]
 80065b8:	edd7 7a01 	vldr	s15, [r7, #4]
 80065bc:	ed9f 7a21 	vldr	s14, [pc, #132]	; 8006644 <normalize_angle_60+0xa8>
 80065c0:	eef4 7ac7 	vcmpe.f32	s15, s14
 80065c4:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80065c8:	daee      	bge.n	80065a8 <normalize_angle_60+0xc>
 80065ca:	e007      	b.n	80065dc <normalize_angle_60+0x40>
 80065cc:	edd7 7a01 	vldr	s15, [r7, #4]
 80065d0:	ed9f 7a1c 	vldr	s14, [pc, #112]	; 8006644 <normalize_angle_60+0xa8>
 80065d4:	ee77 7a87 	vadd.f32	s15, s15, s14
 80065d8:	edc7 7a01 	vstr	s15, [r7, #4]
 80065dc:	edd7 7a01 	vldr	s15, [r7, #4]
 80065e0:	eef5 7ac0 	vcmpe.f32	s15, #0.0
 80065e4:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80065e8:	d4f0      	bmi.n	80065cc <normalize_angle_60+0x30>
 80065ea:	edd7 7a01 	vldr	s15, [r7, #4]
 80065ee:	ed9f 7a15 	vldr	s14, [pc, #84]	; 8006644 <normalize_angle_60+0xa8>
 80065f2:	eef4 7ac7 	vcmpe.f32	s15, s14
 80065f6:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 80065fa:	d511      	bpl.n	8006620 <normalize_angle_60+0x84>
 80065fc:	edd7 7a01 	vldr	s15, [r7, #4]
 8006600:	ed9f 7a11 	vldr	s14, [pc, #68]	; 8006648 <normalize_angle_60+0xac>
 8006604:	eef4 7ac7 	vcmpe.f32	s15, s14
 8006608:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800660c:	dd08      	ble.n	8006620 <normalize_angle_60+0x84>
 800660e:	edd7 7a01 	vldr	s15, [r7, #4]
 8006612:	ed9f 7a0d 	vldr	s14, [pc, #52]	; 8006648 <normalize_angle_60+0xac>
 8006616:	ee77 7ac7 	vsub.f32	s15, s15, s14
 800661a:	edc7 7a01 	vstr	s15, [r7, #4]
 800661e:	e007      	b.n	8006630 <normalize_angle_60+0x94>
 8006620:	ed9f 7a09 	vldr	s14, [pc, #36]	; 8006648 <normalize_angle_60+0xac>
 8006624:	edd7 7a01 	vldr	s15, [r7, #4]
 8006628:	ee77 7a67 	vsub.f32	s15, s14, s15
 800662c:	edc7 7a01 	vstr	s15, [r7, #4]
 8006630:	687b      	ldr	r3, [r7, #4]
 8006632:	ee07 3a90 	vmov	s15, r3
 8006636:	eeb0 0a67 	vmov.f32	s0, s15
 800663a:	370c      	adds	r7, #12
 800663c:	46bd      	mov	sp, r7
 800663e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006642:	4770      	bx	lr
 8006644:	3f860a92 	.word	0x3f860a92
 8006648:	3f060a92 	.word	0x3f060a92

0800664c <odom_calc_wheels_speeds>:
 800664c:	b480      	push	{r7}
 800664e:	b085      	sub	sp, #20
 8006650:	af00      	add	r7, sp, #0
 8006652:	6078      	str	r0, [r7, #4]
 8006654:	2300      	movs	r3, #0
 8006656:	60fb      	str	r3, [r7, #12]
 8006658:	2300      	movs	r3, #0
 800665a:	60fb      	str	r3, [r7, #12]
 800665c:	e041      	b.n	80066e2 <odom_calc_wheels_speeds+0x96>
 800665e:	687b      	ldr	r3, [r7, #4]
 8006660:	68fa      	ldr	r2, [r7, #12]
 8006662:	320e      	adds	r2, #14
 8006664:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 8006668:	881b      	ldrh	r3, [r3, #0]
 800666a:	f5a3 43ea 	sub.w	r3, r3, #29952	; 0x7500
 800666e:	3b30      	subs	r3, #48	; 0x30
 8006670:	b29b      	uxth	r3, r3
 8006672:	b219      	sxth	r1, r3
 8006674:	687a      	ldr	r2, [r7, #4]
 8006676:	68fb      	ldr	r3, [r7, #12]
 8006678:	3320      	adds	r3, #32
 800667a:	005b      	lsls	r3, r3, #1
 800667c:	4413      	add	r3, r2
 800667e:	460a      	mov	r2, r1
 8006680:	809a      	strh	r2, [r3, #4]
 8006682:	687b      	ldr	r3, [r7, #4]
 8006684:	68fa      	ldr	r2, [r7, #12]
 8006686:	320e      	adds	r2, #14
 8006688:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 800668c:	f247 5230 	movw	r2, #30000	; 0x7530
 8006690:	801a      	strh	r2, [r3, #0]
 8006692:	687a      	ldr	r2, [r7, #4]
 8006694:	68fb      	ldr	r3, [r7, #12]
 8006696:	3320      	adds	r3, #32
 8006698:	005b      	lsls	r3, r3, #1
 800669a:	4413      	add	r3, r2
 800669c:	f9b3 3004 	ldrsh.w	r3, [r3, #4]
 80066a0:	ee07 3a90 	vmov	s15, r3
 80066a4:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 80066a8:	ee77 7aa7 	vadd.f32	s15, s15, s15
 80066ac:	ed9f 7a15 	vldr	s14, [pc, #84]	; 8006704 <odom_calc_wheels_speeds+0xb8>
 80066b0:	ee67 7a87 	vmul.f32	s15, s15, s14
 80066b4:	ed9f 7a14 	vldr	s14, [pc, #80]	; 8006708 <odom_calc_wheels_speeds+0xbc>
 80066b8:	ee67 7a87 	vmul.f32	s15, s15, s14
 80066bc:	eddf 6a13 	vldr	s13, [pc, #76]	; 800670c <odom_calc_wheels_speeds+0xc0>
 80066c0:	ee87 7aa6 	vdiv.f32	s14, s15, s13
 80066c4:	eddf 6a12 	vldr	s13, [pc, #72]	; 8006710 <odom_calc_wheels_speeds+0xc4>
 80066c8:	eec7 7a26 	vdiv.f32	s15, s14, s13
 80066cc:	687a      	ldr	r2, [r7, #4]
 80066ce:	68fb      	ldr	r3, [r7, #12]
 80066d0:	330a      	adds	r3, #10
 80066d2:	009b      	lsls	r3, r3, #2
 80066d4:	4413      	add	r3, r2
 80066d6:	3304      	adds	r3, #4
 80066d8:	edc3 7a00 	vstr	s15, [r3]
 80066dc:	68fb      	ldr	r3, [r7, #12]
 80066de:	3301      	adds	r3, #1
 80066e0:	60fb      	str	r3, [r7, #12]
 80066e2:	68fb      	ldr	r3, [r7, #12]
 80066e4:	2b02      	cmp	r3, #2
 80066e6:	ddba      	ble.n	800665e <odom_calc_wheels_speeds+0x12>
 80066e8:	687b      	ldr	r3, [r7, #4]
 80066ea:	edd3 7a0d 	vldr	s15, [r3, #52]	; 0x34
 80066ee:	eef1 7a67 	vneg.f32	s15, s15
 80066f2:	687b      	ldr	r3, [r7, #4]
 80066f4:	edc3 7a0d 	vstr	s15, [r3, #52]	; 0x34
 80066f8:	bf00      	nop
 80066fa:	3714      	adds	r7, #20
 80066fc:	46bd      	mov	sp, r7
 80066fe:	f85d 7b04 	ldr.w	r7, [sp], #4
 8006702:	4770      	bx	lr
 8006704:	40490fdb 	.word	0x40490fdb
 8006708:	3cf1a9fc 	.word	0x3cf1a9fc
 800670c:	450c8dc3 	.word	0x450c8dc3
 8006710:	399aaa3b 	.word	0x399aaa3b

08006714 <odom_calc_robot_speed>:
 8006714:	b5b0      	push	{r4, r5, r7, lr}
 8006716:	b084      	sub	sp, #16
 8006718:	af00      	add	r7, sp, #0
 800671a:	6078      	str	r0, [r7, #4]
 800671c:	687b      	ldr	r3, [r7, #4]
 800671e:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8006720:	4a32      	ldr	r2, [pc, #200]	; (80067ec <odom_calc_robot_speed+0xd8>)
 8006722:	6013      	str	r3, [r2, #0]
 8006724:	687b      	ldr	r3, [r7, #4]
 8006726:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 8006728:	4a30      	ldr	r2, [pc, #192]	; (80067ec <odom_calc_robot_speed+0xd8>)
 800672a:	6053      	str	r3, [r2, #4]
 800672c:	687b      	ldr	r3, [r7, #4]
 800672e:	6b5b      	ldr	r3, [r3, #52]	; 0x34
 8006730:	4a2e      	ldr	r2, [pc, #184]	; (80067ec <odom_calc_robot_speed+0xd8>)
 8006732:	6093      	str	r3, [r2, #8]
 8006734:	4b2e      	ldr	r3, [pc, #184]	; (80067f0 <odom_calc_robot_speed+0xdc>)
 8006736:	2203      	movs	r2, #3
 8006738:	2103      	movs	r1, #3
 800673a:	482e      	ldr	r0, [pc, #184]	; (80067f4 <odom_calc_robot_speed+0xe0>)
 800673c:	f002 fee2 	bl	8009504 <arm_mat_init_f32>
 8006740:	4b2a      	ldr	r3, [pc, #168]	; (80067ec <odom_calc_robot_speed+0xd8>)
 8006742:	2201      	movs	r2, #1
 8006744:	2103      	movs	r1, #3
 8006746:	482c      	ldr	r0, [pc, #176]	; (80067f8 <odom_calc_robot_speed+0xe4>)
 8006748:	f002 fedc 	bl	8009504 <arm_mat_init_f32>
 800674c:	687b      	ldr	r3, [r7, #4]
 800674e:	3320      	adds	r3, #32
 8006750:	2201      	movs	r2, #1
 8006752:	2103      	movs	r1, #3
 8006754:	4829      	ldr	r0, [pc, #164]	; (80067fc <odom_calc_robot_speed+0xe8>)
 8006756:	f002 fed5 	bl	8009504 <arm_mat_init_f32>
 800675a:	4a28      	ldr	r2, [pc, #160]	; (80067fc <odom_calc_robot_speed+0xe8>)
 800675c:	4926      	ldr	r1, [pc, #152]	; (80067f8 <odom_calc_robot_speed+0xe4>)
 800675e:	4825      	ldr	r0, [pc, #148]	; (80067f4 <odom_calc_robot_speed+0xe0>)
 8006760:	f002 ff7f 	bl	8009662 <arm_mat_mult_f32>
 8006764:	687b      	ldr	r3, [r7, #4]
 8006766:	edd3 7a09 	vldr	s15, [r3, #36]	; 0x24
 800676a:	eef5 7a40 	vcmp.f32	s15, #0.0
 800676e:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8006772:	d107      	bne.n	8006784 <odom_calc_robot_speed+0x70>
 8006774:	687b      	ldr	r3, [r7, #4]
 8006776:	edd3 7a08 	vldr	s15, [r3, #32]
 800677a:	eef5 7a40 	vcmp.f32	s15, #0.0
 800677e:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8006782:	d02e      	beq.n	80067e2 <odom_calc_robot_speed+0xce>
 8006784:	687b      	ldr	r3, [r7, #4]
 8006786:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8006788:	4618      	mov	r0, r3
 800678a:	f7f9 feb7 	bl	80004fc <__aeabi_f2d>
 800678e:	4604      	mov	r4, r0
 8006790:	460d      	mov	r5, r1
 8006792:	687b      	ldr	r3, [r7, #4]
 8006794:	6a1b      	ldr	r3, [r3, #32]
 8006796:	4618      	mov	r0, r3
 8006798:	f7f9 feb0 	bl	80004fc <__aeabi_f2d>
 800679c:	4602      	mov	r2, r0
 800679e:	460b      	mov	r3, r1
 80067a0:	ec43 2b11 	vmov	d1, r2, r3
 80067a4:	ec45 4b10 	vmov	d0, r4, r5
 80067a8:	f7fa ff2a 	bl	8001600 <atan2>
 80067ac:	ec54 3b10 	vmov	r3, r4, d0
 80067b0:	4618      	mov	r0, r3
 80067b2:	4621      	mov	r1, r4
 80067b4:	f7f9 fef6 	bl	80005a4 <__aeabi_d2f>
 80067b8:	4603      	mov	r3, r0
 80067ba:	60fb      	str	r3, [r7, #12]
 80067bc:	edd7 7a03 	vldr	s15, [r7, #12]
 80067c0:	ed9f 7a0f 	vldr	s14, [pc, #60]	; 8006800 <odom_calc_robot_speed+0xec>
 80067c4:	ee77 7ac7 	vsub.f32	s15, s15, s14
 80067c8:	eeb0 0a67 	vmov.f32	s0, s15
 80067cc:	f7ff fee6 	bl	800659c <normalize_angle_60>
 80067d0:	eeb0 7a40 	vmov.f32	s14, s0
 80067d4:	eddf 7a0b 	vldr	s15, [pc, #44]	; 8006804 <odom_calc_robot_speed+0xf0>
 80067d8:	ee67 7a27 	vmul.f32	s15, s14, s15
 80067dc:	edc7 7a02 	vstr	s15, [r7, #8]
 80067e0:	bf00      	nop
 80067e2:	bf00      	nop
 80067e4:	3710      	adds	r7, #16
 80067e6:	46bd      	mov	sp, r7
 80067e8:	bdb0      	pop	{r4, r5, r7, pc}
 80067ea:	bf00      	nop
 80067ec:	20001210 	.word	0x20001210
 80067f0:	20000888 	.word	0x20000888
 80067f4:	2000121c 	.word	0x2000121c
 80067f8:	20001224 	.word	0x20001224
 80067fc:	2000122c 	.word	0x2000122c
 8006800:	3fc90fdb 	.word	0x3fc90fdb
 8006804:	3c1374bc 	.word	0x3c1374bc

08006808 <odom_calc_glob_params>:
 8006808:	b580      	push	{r7, lr}
 800680a:	b088      	sub	sp, #32
 800680c:	af00      	add	r7, sp, #0
 800680e:	6078      	str	r0, [r7, #4]
 8006810:	687b      	ldr	r3, [r7, #4]
 8006812:	ed93 7a04 	vldr	s14, [r3, #16]
 8006816:	687b      	ldr	r3, [r7, #4]
 8006818:	edd3 7a0a 	vldr	s15, [r3, #40]	; 0x28
 800681c:	eddf 6a39 	vldr	s13, [pc, #228]	; 8006904 <odom_calc_glob_params+0xfc>
 8006820:	ee67 7aa6 	vmul.f32	s15, s15, s13
 8006824:	ee77 7a27 	vadd.f32	s15, s14, s15
 8006828:	edc7 7a07 	vstr	s15, [r7, #28]
 800682c:	ed97 0a07 	vldr	s0, [r7, #28]
 8006830:	f7ff fd18 	bl	8006264 <normalize_angle>
 8006834:	ed87 0a07 	vstr	s0, [r7, #28]
 8006838:	ed97 0a07 	vldr	s0, [r7, #28]
 800683c:	f7fa fe4e 	bl	80014dc <cosf>
 8006840:	eef0 7a40 	vmov.f32	s15, s0
 8006844:	edc7 7a03 	vstr	s15, [r7, #12]
 8006848:	ed97 0a07 	vldr	s0, [r7, #28]
 800684c:	f7fa fe8e 	bl	800156c <sinf>
 8006850:	eef0 7a40 	vmov.f32	s15, s0
 8006854:	eef1 7a67 	vneg.f32	s15, s15
 8006858:	edc7 7a04 	vstr	s15, [r7, #16]
 800685c:	ed97 0a07 	vldr	s0, [r7, #28]
 8006860:	f7fa fe84 	bl	800156c <sinf>
 8006864:	eef0 7a40 	vmov.f32	s15, s0
 8006868:	edc7 7a05 	vstr	s15, [r7, #20]
 800686c:	ed97 0a07 	vldr	s0, [r7, #28]
 8006870:	f7fa fe34 	bl	80014dc <cosf>
 8006874:	eef0 7a40 	vmov.f32	s15, s0
 8006878:	edc7 7a06 	vstr	s15, [r7, #24]
 800687c:	f107 030c 	add.w	r3, r7, #12
 8006880:	2202      	movs	r2, #2
 8006882:	2102      	movs	r1, #2
 8006884:	4820      	ldr	r0, [pc, #128]	; (8006908 <odom_calc_glob_params+0x100>)
 8006886:	f002 fe3d 	bl	8009504 <arm_mat_init_f32>
 800688a:	687b      	ldr	r3, [r7, #4]
 800688c:	3314      	adds	r3, #20
 800688e:	2201      	movs	r2, #1
 8006890:	2102      	movs	r1, #2
 8006892:	481e      	ldr	r0, [pc, #120]	; (800690c <odom_calc_glob_params+0x104>)
 8006894:	f002 fe36 	bl	8009504 <arm_mat_init_f32>
 8006898:	687b      	ldr	r3, [r7, #4]
 800689a:	3320      	adds	r3, #32
 800689c:	2201      	movs	r2, #1
 800689e:	2102      	movs	r1, #2
 80068a0:	481b      	ldr	r0, [pc, #108]	; (8006910 <odom_calc_glob_params+0x108>)
 80068a2:	f002 fe2f 	bl	8009504 <arm_mat_init_f32>
 80068a6:	4a19      	ldr	r2, [pc, #100]	; (800690c <odom_calc_glob_params+0x104>)
 80068a8:	4919      	ldr	r1, [pc, #100]	; (8006910 <odom_calc_glob_params+0x108>)
 80068aa:	4817      	ldr	r0, [pc, #92]	; (8006908 <odom_calc_glob_params+0x100>)
 80068ac:	f002 fed9 	bl	8009662 <arm_mat_mult_f32>
 80068b0:	687b      	ldr	r3, [r7, #4]
 80068b2:	6a9a      	ldr	r2, [r3, #40]	; 0x28
 80068b4:	687b      	ldr	r3, [r7, #4]
 80068b6:	61da      	str	r2, [r3, #28]
 80068b8:	687b      	ldr	r3, [r7, #4]
 80068ba:	ed93 7a02 	vldr	s14, [r3, #8]
 80068be:	687b      	ldr	r3, [r7, #4]
 80068c0:	edd3 7a05 	vldr	s15, [r3, #20]
 80068c4:	eddf 6a0f 	vldr	s13, [pc, #60]	; 8006904 <odom_calc_glob_params+0xfc>
 80068c8:	ee67 7aa6 	vmul.f32	s15, s15, s13
 80068cc:	ee77 7a27 	vadd.f32	s15, s14, s15
 80068d0:	687b      	ldr	r3, [r7, #4]
 80068d2:	edc3 7a02 	vstr	s15, [r3, #8]
 80068d6:	687b      	ldr	r3, [r7, #4]
 80068d8:	ed93 7a03 	vldr	s14, [r3, #12]
 80068dc:	687b      	ldr	r3, [r7, #4]
 80068de:	edd3 7a06 	vldr	s15, [r3, #24]
 80068e2:	eddf 6a08 	vldr	s13, [pc, #32]	; 8006904 <odom_calc_glob_params+0xfc>
 80068e6:	ee67 7aa6 	vmul.f32	s15, s15, s13
 80068ea:	ee77 7a27 	vadd.f32	s15, s14, s15
 80068ee:	687b      	ldr	r3, [r7, #4]
 80068f0:	edc3 7a03 	vstr	s15, [r3, #12]
 80068f4:	687b      	ldr	r3, [r7, #4]
 80068f6:	69fa      	ldr	r2, [r7, #28]
 80068f8:	611a      	str	r2, [r3, #16]
 80068fa:	bf00      	nop
 80068fc:	3720      	adds	r7, #32
 80068fe:	46bd      	mov	sp, r7
 8006900:	bd80      	pop	{r7, pc}
 8006902:	bf00      	nop
 8006904:	3c23d70a 	.word	0x3c23d70a
 8006908:	20001234 	.word	0x20001234
 800690c:	2000123c 	.word	0x2000123c
 8006910:	20001244 	.word	0x20001244

08006914 <odom>:
 8006914:	b580      	push	{r7, lr}
 8006916:	b096      	sub	sp, #88	; 0x58
 8006918:	af00      	add	r7, sp, #0
 800691a:	6078      	str	r0, [r7, #4]
 800691c:	f107 0308 	add.w	r3, r7, #8
 8006920:	2250      	movs	r2, #80	; 0x50
 8006922:	2100      	movs	r1, #0
 8006924:	4618      	mov	r0, r3
 8006926:	f7fa fa29 	bl	8000d7c <memset>
 800692a:	4b13      	ldr	r3, [pc, #76]	; (8006978 <odom+0x64>)
 800692c:	643b      	str	r3, [r7, #64]	; 0x40
 800692e:	4b13      	ldr	r3, [pc, #76]	; (800697c <odom+0x68>)
 8006930:	647b      	str	r3, [r7, #68]	; 0x44
 8006932:	4b13      	ldr	r3, [pc, #76]	; (8006980 <odom+0x6c>)
 8006934:	64bb      	str	r3, [r7, #72]	; 0x48
 8006936:	f001 fd61 	bl	80083fc <xTaskGetCurrentTaskHandle>
 800693a:	4603      	mov	r3, r0
 800693c:	657b      	str	r3, [r7, #84]	; 0x54
 800693e:	4a11      	ldr	r2, [pc, #68]	; (8006984 <odom+0x70>)
 8006940:	f107 0308 	add.w	r3, r7, #8
 8006944:	6013      	str	r3, [r2, #0]
 8006946:	f7ff fcbf 	bl	80062c8 <odom_hw_config>
 800694a:	f04f 32ff 	mov.w	r2, #4294967295
 800694e:	2101      	movs	r1, #1
 8006950:	2000      	movs	r0, #0
 8006952:	f001 fdf9 	bl	8008548 <ulTaskGenericNotifyTake>
 8006956:	f107 0308 	add.w	r3, r7, #8
 800695a:	4618      	mov	r0, r3
 800695c:	f7ff fe76 	bl	800664c <odom_calc_wheels_speeds>
 8006960:	f107 0308 	add.w	r3, r7, #8
 8006964:	4618      	mov	r0, r3
 8006966:	f7ff fed5 	bl	8006714 <odom_calc_robot_speed>
 800696a:	f107 0308 	add.w	r3, r7, #8
 800696e:	4618      	mov	r0, r3
 8006970:	f7ff ff4a 	bl	8006808 <odom_calc_glob_params>
 8006974:	e7e9      	b.n	800694a <odom+0x36>
 8006976:	bf00      	nop
 8006978:	40000024 	.word	0x40000024
 800697c:	40000824 	.word	0x40000824
 8006980:	40000c24 	.word	0x40000c24
 8006984:	2000120c 	.word	0x2000120c

08006988 <cmd_get_wheel_speed>:
 8006988:	b580      	push	{r7, lr}
 800698a:	b082      	sub	sp, #8
 800698c:	af00      	add	r7, sp, #0
 800698e:	6078      	str	r0, [r7, #4]
 8006990:	4b0c      	ldr	r3, [pc, #48]	; (80069c4 <cmd_get_wheel_speed+0x3c>)
 8006992:	681b      	ldr	r3, [r3, #0]
 8006994:	2b00      	cmp	r3, #0
 8006996:	d009      	beq.n	80069ac <cmd_get_wheel_speed+0x24>
 8006998:	4b0a      	ldr	r3, [pc, #40]	; (80069c4 <cmd_get_wheel_speed+0x3c>)
 800699a:	681b      	ldr	r3, [r3, #0]
 800699c:	332c      	adds	r3, #44	; 0x2c
 800699e:	220c      	movs	r2, #12
 80069a0:	4619      	mov	r1, r3
 80069a2:	6878      	ldr	r0, [r7, #4]
 80069a4:	f7fa f950 	bl	8000c48 <memcpy>
 80069a8:	230c      	movs	r3, #12
 80069aa:	e006      	b.n	80069ba <cmd_get_wheel_speed+0x32>
 80069ac:	bf00      	nop
 80069ae:	2203      	movs	r2, #3
 80069b0:	4905      	ldr	r1, [pc, #20]	; (80069c8 <cmd_get_wheel_speed+0x40>)
 80069b2:	6878      	ldr	r0, [r7, #4]
 80069b4:	f7fa f948 	bl	8000c48 <memcpy>
 80069b8:	2303      	movs	r3, #3
 80069ba:	4618      	mov	r0, r3
 80069bc:	3708      	adds	r7, #8
 80069be:	46bd      	mov	sp, r7
 80069c0:	bd80      	pop	{r7, pc}
 80069c2:	bf00      	nop
 80069c4:	2000120c 	.word	0x2000120c
 80069c8:	080098a4 	.word	0x080098a4

080069cc <cmd_get_speed>:
 80069cc:	b580      	push	{r7, lr}
 80069ce:	b082      	sub	sp, #8
 80069d0:	af00      	add	r7, sp, #0
 80069d2:	6078      	str	r0, [r7, #4]
 80069d4:	4b0c      	ldr	r3, [pc, #48]	; (8006a08 <cmd_get_speed+0x3c>)
 80069d6:	681b      	ldr	r3, [r3, #0]
 80069d8:	2b00      	cmp	r3, #0
 80069da:	d009      	beq.n	80069f0 <cmd_get_speed+0x24>
 80069dc:	4b0a      	ldr	r3, [pc, #40]	; (8006a08 <cmd_get_speed+0x3c>)
 80069de:	681b      	ldr	r3, [r3, #0]
 80069e0:	3320      	adds	r3, #32
 80069e2:	220c      	movs	r2, #12
 80069e4:	4619      	mov	r1, r3
 80069e6:	6878      	ldr	r0, [r7, #4]
 80069e8:	f7fa f92e 	bl	8000c48 <memcpy>
 80069ec:	230c      	movs	r3, #12
 80069ee:	e006      	b.n	80069fe <cmd_get_speed+0x32>
 80069f0:	bf00      	nop
 80069f2:	2203      	movs	r2, #3
 80069f4:	4905      	ldr	r1, [pc, #20]	; (8006a0c <cmd_get_speed+0x40>)
 80069f6:	6878      	ldr	r0, [r7, #4]
 80069f8:	f7fa f926 	bl	8000c48 <memcpy>
 80069fc:	2303      	movs	r3, #3
 80069fe:	4618      	mov	r0, r3
 8006a00:	3708      	adds	r7, #8
 8006a02:	46bd      	mov	sp, r7
 8006a04:	bd80      	pop	{r7, pc}
 8006a06:	bf00      	nop
 8006a08:	2000120c 	.word	0x2000120c
 8006a0c:	080098a4 	.word	0x080098a4

08006a10 <cmd_set_coord>:
 8006a10:	b580      	push	{r7, lr}
 8006a12:	b084      	sub	sp, #16
 8006a14:	af00      	add	r7, sp, #0
 8006a16:	6078      	str	r0, [r7, #4]
 8006a18:	687b      	ldr	r3, [r7, #4]
 8006a1a:	60fb      	str	r3, [r7, #12]
 8006a1c:	4b1f      	ldr	r3, [pc, #124]	; (8006a9c <cmd_set_coord+0x8c>)
 8006a1e:	681b      	ldr	r3, [r3, #0]
 8006a20:	2b00      	cmp	r3, #0
 8006a22:	d030      	beq.n	8006a86 <cmd_set_coord+0x76>
 8006a24:	4b1d      	ldr	r3, [pc, #116]	; (8006a9c <cmd_set_coord+0x8c>)
 8006a26:	681a      	ldr	r2, [r3, #0]
 8006a28:	68fb      	ldr	r3, [r7, #12]
 8006a2a:	7819      	ldrb	r1, [r3, #0]
 8006a2c:	7858      	ldrb	r0, [r3, #1]
 8006a2e:	0200      	lsls	r0, r0, #8
 8006a30:	4301      	orrs	r1, r0
 8006a32:	7898      	ldrb	r0, [r3, #2]
 8006a34:	0400      	lsls	r0, r0, #16
 8006a36:	4301      	orrs	r1, r0
 8006a38:	78db      	ldrb	r3, [r3, #3]
 8006a3a:	061b      	lsls	r3, r3, #24
 8006a3c:	430b      	orrs	r3, r1
 8006a3e:	6093      	str	r3, [r2, #8]
 8006a40:	4b16      	ldr	r3, [pc, #88]	; (8006a9c <cmd_set_coord+0x8c>)
 8006a42:	681a      	ldr	r2, [r3, #0]
 8006a44:	68fb      	ldr	r3, [r7, #12]
 8006a46:	7919      	ldrb	r1, [r3, #4]
 8006a48:	7958      	ldrb	r0, [r3, #5]
 8006a4a:	0200      	lsls	r0, r0, #8
 8006a4c:	4301      	orrs	r1, r0
 8006a4e:	7998      	ldrb	r0, [r3, #6]
 8006a50:	0400      	lsls	r0, r0, #16
 8006a52:	4301      	orrs	r1, r0
 8006a54:	79db      	ldrb	r3, [r3, #7]
 8006a56:	061b      	lsls	r3, r3, #24
 8006a58:	430b      	orrs	r3, r1
 8006a5a:	60d3      	str	r3, [r2, #12]
 8006a5c:	4b0f      	ldr	r3, [pc, #60]	; (8006a9c <cmd_set_coord+0x8c>)
 8006a5e:	681a      	ldr	r2, [r3, #0]
 8006a60:	68fb      	ldr	r3, [r7, #12]
 8006a62:	7a19      	ldrb	r1, [r3, #8]
 8006a64:	7a58      	ldrb	r0, [r3, #9]
 8006a66:	0200      	lsls	r0, r0, #8
 8006a68:	4301      	orrs	r1, r0
 8006a6a:	7a98      	ldrb	r0, [r3, #10]
 8006a6c:	0400      	lsls	r0, r0, #16
 8006a6e:	4301      	orrs	r1, r0
 8006a70:	7adb      	ldrb	r3, [r3, #11]
 8006a72:	061b      	lsls	r3, r3, #24
 8006a74:	430b      	orrs	r3, r1
 8006a76:	6113      	str	r3, [r2, #16]
 8006a78:	2203      	movs	r2, #3
 8006a7a:	4909      	ldr	r1, [pc, #36]	; (8006aa0 <cmd_set_coord+0x90>)
 8006a7c:	6878      	ldr	r0, [r7, #4]
 8006a7e:	f7fa f8e3 	bl	8000c48 <memcpy>
 8006a82:	2303      	movs	r3, #3
 8006a84:	e006      	b.n	8006a94 <cmd_set_coord+0x84>
 8006a86:	bf00      	nop
 8006a88:	2203      	movs	r2, #3
 8006a8a:	4906      	ldr	r1, [pc, #24]	; (8006aa4 <cmd_set_coord+0x94>)
 8006a8c:	6878      	ldr	r0, [r7, #4]
 8006a8e:	f7fa f8db 	bl	8000c48 <memcpy>
 8006a92:	2303      	movs	r3, #3
 8006a94:	4618      	mov	r0, r3
 8006a96:	3710      	adds	r7, #16
 8006a98:	46bd      	mov	sp, r7
 8006a9a:	bd80      	pop	{r7, pc}
 8006a9c:	2000120c 	.word	0x2000120c
 8006aa0:	080098a8 	.word	0x080098a8
 8006aa4:	080098a4 	.word	0x080098a4

08006aa8 <cmd_get_coord>:
 8006aa8:	b580      	push	{r7, lr}
 8006aaa:	b082      	sub	sp, #8
 8006aac:	af00      	add	r7, sp, #0
 8006aae:	6078      	str	r0, [r7, #4]
 8006ab0:	4b0c      	ldr	r3, [pc, #48]	; (8006ae4 <cmd_get_coord+0x3c>)
 8006ab2:	681b      	ldr	r3, [r3, #0]
 8006ab4:	2b00      	cmp	r3, #0
 8006ab6:	d009      	beq.n	8006acc <cmd_get_coord+0x24>
 8006ab8:	4b0a      	ldr	r3, [pc, #40]	; (8006ae4 <cmd_get_coord+0x3c>)
 8006aba:	681b      	ldr	r3, [r3, #0]
 8006abc:	3308      	adds	r3, #8
 8006abe:	220c      	movs	r2, #12
 8006ac0:	4619      	mov	r1, r3
 8006ac2:	6878      	ldr	r0, [r7, #4]
 8006ac4:	f7fa f8c0 	bl	8000c48 <memcpy>
 8006ac8:	230c      	movs	r3, #12
 8006aca:	e006      	b.n	8006ada <cmd_get_coord+0x32>
 8006acc:	bf00      	nop
 8006ace:	2206      	movs	r2, #6
 8006ad0:	4905      	ldr	r1, [pc, #20]	; (8006ae8 <cmd_get_coord+0x40>)
 8006ad2:	6878      	ldr	r0, [r7, #4]
 8006ad4:	f7fa f8b8 	bl	8000c48 <memcpy>
 8006ad8:	2306      	movs	r3, #6
 8006ada:	4618      	mov	r0, r3
 8006adc:	3708      	adds	r7, #8
 8006ade:	46bd      	mov	sp, r7
 8006ae0:	bd80      	pop	{r7, pc}
 8006ae2:	bf00      	nop
 8006ae4:	2000120c 	.word	0x2000120c
 8006ae8:	080098ac 	.word	0x080098ac

08006aec <cmd_set_xy>:
 8006aec:	b580      	push	{r7, lr}
 8006aee:	b084      	sub	sp, #16
 8006af0:	af00      	add	r7, sp, #0
 8006af2:	6078      	str	r0, [r7, #4]
 8006af4:	687b      	ldr	r3, [r7, #4]
 8006af6:	60fb      	str	r3, [r7, #12]
 8006af8:	4b18      	ldr	r3, [pc, #96]	; (8006b5c <cmd_set_xy+0x70>)
 8006afa:	681b      	ldr	r3, [r3, #0]
 8006afc:	2b00      	cmp	r3, #0
 8006afe:	d022      	beq.n	8006b46 <cmd_set_xy+0x5a>
 8006b00:	4b16      	ldr	r3, [pc, #88]	; (8006b5c <cmd_set_xy+0x70>)
 8006b02:	681a      	ldr	r2, [r3, #0]
 8006b04:	68fb      	ldr	r3, [r7, #12]
 8006b06:	7819      	ldrb	r1, [r3, #0]
 8006b08:	7858      	ldrb	r0, [r3, #1]
 8006b0a:	0200      	lsls	r0, r0, #8
 8006b0c:	4301      	orrs	r1, r0
 8006b0e:	7898      	ldrb	r0, [r3, #2]
 8006b10:	0400      	lsls	r0, r0, #16
 8006b12:	4301      	orrs	r1, r0
 8006b14:	78db      	ldrb	r3, [r3, #3]
 8006b16:	061b      	lsls	r3, r3, #24
 8006b18:	430b      	orrs	r3, r1
 8006b1a:	6093      	str	r3, [r2, #8]
 8006b1c:	4b0f      	ldr	r3, [pc, #60]	; (8006b5c <cmd_set_xy+0x70>)
 8006b1e:	681a      	ldr	r2, [r3, #0]
 8006b20:	68fb      	ldr	r3, [r7, #12]
 8006b22:	7919      	ldrb	r1, [r3, #4]
 8006b24:	7958      	ldrb	r0, [r3, #5]
 8006b26:	0200      	lsls	r0, r0, #8
 8006b28:	4301      	orrs	r1, r0
 8006b2a:	7998      	ldrb	r0, [r3, #6]
 8006b2c:	0400      	lsls	r0, r0, #16
 8006b2e:	4301      	orrs	r1, r0
 8006b30:	79db      	ldrb	r3, [r3, #7]
 8006b32:	061b      	lsls	r3, r3, #24
 8006b34:	430b      	orrs	r3, r1
 8006b36:	60d3      	str	r3, [r2, #12]
 8006b38:	2203      	movs	r2, #3
 8006b3a:	4909      	ldr	r1, [pc, #36]	; (8006b60 <cmd_set_xy+0x74>)
 8006b3c:	6878      	ldr	r0, [r7, #4]
 8006b3e:	f7fa f883 	bl	8000c48 <memcpy>
 8006b42:	2303      	movs	r3, #3
 8006b44:	e006      	b.n	8006b54 <cmd_set_xy+0x68>
 8006b46:	bf00      	nop
 8006b48:	2203      	movs	r2, #3
 8006b4a:	4906      	ldr	r1, [pc, #24]	; (8006b64 <cmd_set_xy+0x78>)
 8006b4c:	6878      	ldr	r0, [r7, #4]
 8006b4e:	f7fa f87b 	bl	8000c48 <memcpy>
 8006b52:	2303      	movs	r3, #3
 8006b54:	4618      	mov	r0, r3
 8006b56:	3710      	adds	r7, #16
 8006b58:	46bd      	mov	sp, r7
 8006b5a:	bd80      	pop	{r7, pc}
 8006b5c:	2000120c 	.word	0x2000120c
 8006b60:	080098b4 	.word	0x080098b4
 8006b64:	080098b8 	.word	0x080098b8

08006b68 <cmd_set_theta>:
 8006b68:	b580      	push	{r7, lr}
 8006b6a:	b084      	sub	sp, #16
 8006b6c:	af00      	add	r7, sp, #0
 8006b6e:	6078      	str	r0, [r7, #4]
 8006b70:	687b      	ldr	r3, [r7, #4]
 8006b72:	60fb      	str	r3, [r7, #12]
 8006b74:	4b11      	ldr	r3, [pc, #68]	; (8006bbc <cmd_set_theta+0x54>)
 8006b76:	681b      	ldr	r3, [r3, #0]
 8006b78:	2b00      	cmp	r3, #0
 8006b7a:	d014      	beq.n	8006ba6 <cmd_set_theta+0x3e>
 8006b7c:	4b0f      	ldr	r3, [pc, #60]	; (8006bbc <cmd_set_theta+0x54>)
 8006b7e:	681a      	ldr	r2, [r3, #0]
 8006b80:	68fb      	ldr	r3, [r7, #12]
 8006b82:	7819      	ldrb	r1, [r3, #0]
 8006b84:	7858      	ldrb	r0, [r3, #1]
 8006b86:	0200      	lsls	r0, r0, #8
 8006b88:	4301      	orrs	r1, r0
 8006b8a:	7898      	ldrb	r0, [r3, #2]
 8006b8c:	0400      	lsls	r0, r0, #16
 8006b8e:	4301      	orrs	r1, r0
 8006b90:	78db      	ldrb	r3, [r3, #3]
 8006b92:	061b      	lsls	r3, r3, #24
 8006b94:	430b      	orrs	r3, r1
 8006b96:	6113      	str	r3, [r2, #16]
 8006b98:	2203      	movs	r2, #3
 8006b9a:	4909      	ldr	r1, [pc, #36]	; (8006bc0 <cmd_set_theta+0x58>)
 8006b9c:	6878      	ldr	r0, [r7, #4]
 8006b9e:	f7fa f853 	bl	8000c48 <memcpy>
 8006ba2:	2303      	movs	r3, #3
 8006ba4:	e006      	b.n	8006bb4 <cmd_set_theta+0x4c>
 8006ba6:	bf00      	nop
 8006ba8:	2203      	movs	r2, #3
 8006baa:	4906      	ldr	r1, [pc, #24]	; (8006bc4 <cmd_set_theta+0x5c>)
 8006bac:	6878      	ldr	r0, [r7, #4]
 8006bae:	f7fa f84b 	bl	8000c48 <memcpy>
 8006bb2:	2303      	movs	r3, #3
 8006bb4:	4618      	mov	r0, r3
 8006bb6:	3710      	adds	r7, #16
 8006bb8:	46bd      	mov	sp, r7
 8006bba:	bd80      	pop	{r7, pc}
 8006bbc:	2000120c 	.word	0x2000120c
 8006bc0:	080098b4 	.word	0x080098b4
 8006bc4:	080098b8 	.word	0x080098b8

08006bc8 <TIM6_DAC_IRQHandler>:
 8006bc8:	b580      	push	{r7, lr}
 8006bca:	b082      	sub	sp, #8
 8006bcc:	af00      	add	r7, sp, #0
 8006bce:	2300      	movs	r3, #0
 8006bd0:	607b      	str	r3, [r7, #4]
 8006bd2:	4810      	ldr	r0, [pc, #64]	; (8006c14 <TIM6_DAC_IRQHandler+0x4c>)
 8006bd4:	f7ff fa81 	bl	80060da <LL_TIM_IsActiveFlag_UPDATE>
 8006bd8:	4603      	mov	r3, r0
 8006bda:	2b00      	cmp	r3, #0
 8006bdc:	d00a      	beq.n	8006bf4 <TIM6_DAC_IRQHandler+0x2c>
 8006bde:	480d      	ldr	r0, [pc, #52]	; (8006c14 <TIM6_DAC_IRQHandler+0x4c>)
 8006be0:	f7ff fa6d 	bl	80060be <LL_TIM_ClearFlag_UPDATE>
 8006be4:	4b0c      	ldr	r3, [pc, #48]	; (8006c18 <TIM6_DAC_IRQHandler+0x50>)
 8006be6:	681b      	ldr	r3, [r3, #0]
 8006be8:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8006bea:	1d3a      	adds	r2, r7, #4
 8006bec:	2100      	movs	r1, #0
 8006bee:	4618      	mov	r0, r3
 8006bf0:	f001 fd16 	bl	8008620 <vTaskGenericNotifyGiveFromISR>
 8006bf4:	687b      	ldr	r3, [r7, #4]
 8006bf6:	2b00      	cmp	r3, #0
 8006bf8:	d007      	beq.n	8006c0a <TIM6_DAC_IRQHandler+0x42>
 8006bfa:	4b08      	ldr	r3, [pc, #32]	; (8006c1c <TIM6_DAC_IRQHandler+0x54>)
 8006bfc:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006c00:	601a      	str	r2, [r3, #0]
 8006c02:	f3bf 8f4f 	dsb	sy
 8006c06:	f3bf 8f6f 	isb	sy
 8006c0a:	bf00      	nop
 8006c0c:	3708      	adds	r7, #8
 8006c0e:	46bd      	mov	sp, r7
 8006c10:	bd80      	pop	{r7, pc}
 8006c12:	bf00      	nop
 8006c14:	40001000 	.word	0x40001000
 8006c18:	2000120c 	.word	0x2000120c
 8006c1c:	e000ed04 	.word	0xe000ed04

08006c20 <xQueueGenericReset>:
 8006c20:	b580      	push	{r7, lr}
 8006c22:	b084      	sub	sp, #16
 8006c24:	af00      	add	r7, sp, #0
 8006c26:	6078      	str	r0, [r7, #4]
 8006c28:	6039      	str	r1, [r7, #0]
 8006c2a:	687b      	ldr	r3, [r7, #4]
 8006c2c:	60fb      	str	r3, [r7, #12]
 8006c2e:	68fb      	ldr	r3, [r7, #12]
 8006c30:	2b00      	cmp	r3, #0
 8006c32:	d109      	bne.n	8006c48 <xQueueGenericReset+0x28>
 8006c34:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006c38:	f383 8811 	msr	BASEPRI, r3
 8006c3c:	f3bf 8f6f 	isb	sy
 8006c40:	f3bf 8f4f 	dsb	sy
 8006c44:	60bb      	str	r3, [r7, #8]
 8006c46:	e7fe      	b.n	8006c46 <xQueueGenericReset+0x26>
 8006c48:	f002 fa52 	bl	80090f0 <vPortEnterCritical>
 8006c4c:	68fb      	ldr	r3, [r7, #12]
 8006c4e:	681a      	ldr	r2, [r3, #0]
 8006c50:	68fb      	ldr	r3, [r7, #12]
 8006c52:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8006c54:	68f9      	ldr	r1, [r7, #12]
 8006c56:	6c09      	ldr	r1, [r1, #64]	; 0x40
 8006c58:	fb01 f303 	mul.w	r3, r1, r3
 8006c5c:	441a      	add	r2, r3
 8006c5e:	68fb      	ldr	r3, [r7, #12]
 8006c60:	609a      	str	r2, [r3, #8]
 8006c62:	68fb      	ldr	r3, [r7, #12]
 8006c64:	2200      	movs	r2, #0
 8006c66:	639a      	str	r2, [r3, #56]	; 0x38
 8006c68:	68fb      	ldr	r3, [r7, #12]
 8006c6a:	681a      	ldr	r2, [r3, #0]
 8006c6c:	68fb      	ldr	r3, [r7, #12]
 8006c6e:	605a      	str	r2, [r3, #4]
 8006c70:	68fb      	ldr	r3, [r7, #12]
 8006c72:	681a      	ldr	r2, [r3, #0]
 8006c74:	68fb      	ldr	r3, [r7, #12]
 8006c76:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8006c78:	3b01      	subs	r3, #1
 8006c7a:	68f9      	ldr	r1, [r7, #12]
 8006c7c:	6c09      	ldr	r1, [r1, #64]	; 0x40
 8006c7e:	fb01 f303 	mul.w	r3, r1, r3
 8006c82:	441a      	add	r2, r3
 8006c84:	68fb      	ldr	r3, [r7, #12]
 8006c86:	60da      	str	r2, [r3, #12]
 8006c88:	68fb      	ldr	r3, [r7, #12]
 8006c8a:	22ff      	movs	r2, #255	; 0xff
 8006c8c:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8006c90:	68fb      	ldr	r3, [r7, #12]
 8006c92:	22ff      	movs	r2, #255	; 0xff
 8006c94:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 8006c98:	683b      	ldr	r3, [r7, #0]
 8006c9a:	2b00      	cmp	r3, #0
 8006c9c:	d114      	bne.n	8006cc8 <xQueueGenericReset+0xa8>
 8006c9e:	68fb      	ldr	r3, [r7, #12]
 8006ca0:	691b      	ldr	r3, [r3, #16]
 8006ca2:	2b00      	cmp	r3, #0
 8006ca4:	d01a      	beq.n	8006cdc <xQueueGenericReset+0xbc>
 8006ca6:	68fb      	ldr	r3, [r7, #12]
 8006ca8:	3310      	adds	r3, #16
 8006caa:	4618      	mov	r0, r3
 8006cac:	f001 f9e8 	bl	8008080 <xTaskRemoveFromEventList>
 8006cb0:	4603      	mov	r3, r0
 8006cb2:	2b00      	cmp	r3, #0
 8006cb4:	d012      	beq.n	8006cdc <xQueueGenericReset+0xbc>
 8006cb6:	4b0d      	ldr	r3, [pc, #52]	; (8006cec <xQueueGenericReset+0xcc>)
 8006cb8:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006cbc:	601a      	str	r2, [r3, #0]
 8006cbe:	f3bf 8f4f 	dsb	sy
 8006cc2:	f3bf 8f6f 	isb	sy
 8006cc6:	e009      	b.n	8006cdc <xQueueGenericReset+0xbc>
 8006cc8:	68fb      	ldr	r3, [r7, #12]
 8006cca:	3310      	adds	r3, #16
 8006ccc:	4618      	mov	r0, r3
 8006cce:	f000 fc8d 	bl	80075ec <vListInitialise>
 8006cd2:	68fb      	ldr	r3, [r7, #12]
 8006cd4:	3324      	adds	r3, #36	; 0x24
 8006cd6:	4618      	mov	r0, r3
 8006cd8:	f000 fc88 	bl	80075ec <vListInitialise>
 8006cdc:	f002 fa36 	bl	800914c <vPortExitCritical>
 8006ce0:	2301      	movs	r3, #1
 8006ce2:	4618      	mov	r0, r3
 8006ce4:	3710      	adds	r7, #16
 8006ce6:	46bd      	mov	sp, r7
 8006ce8:	bd80      	pop	{r7, pc}
 8006cea:	bf00      	nop
 8006cec:	e000ed04 	.word	0xe000ed04

08006cf0 <xQueueGenericCreateStatic>:
 8006cf0:	b580      	push	{r7, lr}
 8006cf2:	b08e      	sub	sp, #56	; 0x38
 8006cf4:	af02      	add	r7, sp, #8
 8006cf6:	60f8      	str	r0, [r7, #12]
 8006cf8:	60b9      	str	r1, [r7, #8]
 8006cfa:	607a      	str	r2, [r7, #4]
 8006cfc:	603b      	str	r3, [r7, #0]
 8006cfe:	68fb      	ldr	r3, [r7, #12]
 8006d00:	2b00      	cmp	r3, #0
 8006d02:	d109      	bne.n	8006d18 <xQueueGenericCreateStatic+0x28>
 8006d04:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006d08:	f383 8811 	msr	BASEPRI, r3
 8006d0c:	f3bf 8f6f 	isb	sy
 8006d10:	f3bf 8f4f 	dsb	sy
 8006d14:	62bb      	str	r3, [r7, #40]	; 0x28
 8006d16:	e7fe      	b.n	8006d16 <xQueueGenericCreateStatic+0x26>
 8006d18:	683b      	ldr	r3, [r7, #0]
 8006d1a:	2b00      	cmp	r3, #0
 8006d1c:	d109      	bne.n	8006d32 <xQueueGenericCreateStatic+0x42>
 8006d1e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006d22:	f383 8811 	msr	BASEPRI, r3
 8006d26:	f3bf 8f6f 	isb	sy
 8006d2a:	f3bf 8f4f 	dsb	sy
 8006d2e:	627b      	str	r3, [r7, #36]	; 0x24
 8006d30:	e7fe      	b.n	8006d30 <xQueueGenericCreateStatic+0x40>
 8006d32:	687b      	ldr	r3, [r7, #4]
 8006d34:	2b00      	cmp	r3, #0
 8006d36:	d002      	beq.n	8006d3e <xQueueGenericCreateStatic+0x4e>
 8006d38:	68bb      	ldr	r3, [r7, #8]
 8006d3a:	2b00      	cmp	r3, #0
 8006d3c:	d001      	beq.n	8006d42 <xQueueGenericCreateStatic+0x52>
 8006d3e:	2301      	movs	r3, #1
 8006d40:	e000      	b.n	8006d44 <xQueueGenericCreateStatic+0x54>
 8006d42:	2300      	movs	r3, #0
 8006d44:	2b00      	cmp	r3, #0
 8006d46:	d109      	bne.n	8006d5c <xQueueGenericCreateStatic+0x6c>
 8006d48:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006d4c:	f383 8811 	msr	BASEPRI, r3
 8006d50:	f3bf 8f6f 	isb	sy
 8006d54:	f3bf 8f4f 	dsb	sy
 8006d58:	623b      	str	r3, [r7, #32]
 8006d5a:	e7fe      	b.n	8006d5a <xQueueGenericCreateStatic+0x6a>
 8006d5c:	687b      	ldr	r3, [r7, #4]
 8006d5e:	2b00      	cmp	r3, #0
 8006d60:	d102      	bne.n	8006d68 <xQueueGenericCreateStatic+0x78>
 8006d62:	68bb      	ldr	r3, [r7, #8]
 8006d64:	2b00      	cmp	r3, #0
 8006d66:	d101      	bne.n	8006d6c <xQueueGenericCreateStatic+0x7c>
 8006d68:	2301      	movs	r3, #1
 8006d6a:	e000      	b.n	8006d6e <xQueueGenericCreateStatic+0x7e>
 8006d6c:	2300      	movs	r3, #0
 8006d6e:	2b00      	cmp	r3, #0
 8006d70:	d109      	bne.n	8006d86 <xQueueGenericCreateStatic+0x96>
 8006d72:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006d76:	f383 8811 	msr	BASEPRI, r3
 8006d7a:	f3bf 8f6f 	isb	sy
 8006d7e:	f3bf 8f4f 	dsb	sy
 8006d82:	61fb      	str	r3, [r7, #28]
 8006d84:	e7fe      	b.n	8006d84 <xQueueGenericCreateStatic+0x94>
 8006d86:	2350      	movs	r3, #80	; 0x50
 8006d88:	617b      	str	r3, [r7, #20]
 8006d8a:	697b      	ldr	r3, [r7, #20]
 8006d8c:	2b50      	cmp	r3, #80	; 0x50
 8006d8e:	d009      	beq.n	8006da4 <xQueueGenericCreateStatic+0xb4>
 8006d90:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006d94:	f383 8811 	msr	BASEPRI, r3
 8006d98:	f3bf 8f6f 	isb	sy
 8006d9c:	f3bf 8f4f 	dsb	sy
 8006da0:	61bb      	str	r3, [r7, #24]
 8006da2:	e7fe      	b.n	8006da2 <xQueueGenericCreateStatic+0xb2>
 8006da4:	697b      	ldr	r3, [r7, #20]
 8006da6:	683b      	ldr	r3, [r7, #0]
 8006da8:	62fb      	str	r3, [r7, #44]	; 0x2c
 8006daa:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8006dac:	2b00      	cmp	r3, #0
 8006dae:	d00d      	beq.n	8006dcc <xQueueGenericCreateStatic+0xdc>
 8006db0:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8006db2:	2201      	movs	r2, #1
 8006db4:	f883 2046 	strb.w	r2, [r3, #70]	; 0x46
 8006db8:	f897 2038 	ldrb.w	r2, [r7, #56]	; 0x38
 8006dbc:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8006dbe:	9300      	str	r3, [sp, #0]
 8006dc0:	4613      	mov	r3, r2
 8006dc2:	687a      	ldr	r2, [r7, #4]
 8006dc4:	68b9      	ldr	r1, [r7, #8]
 8006dc6:	68f8      	ldr	r0, [r7, #12]
 8006dc8:	f000 f805 	bl	8006dd6 <prvInitialiseNewQueue>
 8006dcc:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8006dce:	4618      	mov	r0, r3
 8006dd0:	3730      	adds	r7, #48	; 0x30
 8006dd2:	46bd      	mov	sp, r7
 8006dd4:	bd80      	pop	{r7, pc}

08006dd6 <prvInitialiseNewQueue>:
 8006dd6:	b580      	push	{r7, lr}
 8006dd8:	b084      	sub	sp, #16
 8006dda:	af00      	add	r7, sp, #0
 8006ddc:	60f8      	str	r0, [r7, #12]
 8006dde:	60b9      	str	r1, [r7, #8]
 8006de0:	607a      	str	r2, [r7, #4]
 8006de2:	70fb      	strb	r3, [r7, #3]
 8006de4:	68bb      	ldr	r3, [r7, #8]
 8006de6:	2b00      	cmp	r3, #0
 8006de8:	d103      	bne.n	8006df2 <prvInitialiseNewQueue+0x1c>
 8006dea:	69bb      	ldr	r3, [r7, #24]
 8006dec:	69ba      	ldr	r2, [r7, #24]
 8006dee:	601a      	str	r2, [r3, #0]
 8006df0:	e002      	b.n	8006df8 <prvInitialiseNewQueue+0x22>
 8006df2:	69bb      	ldr	r3, [r7, #24]
 8006df4:	687a      	ldr	r2, [r7, #4]
 8006df6:	601a      	str	r2, [r3, #0]
 8006df8:	69bb      	ldr	r3, [r7, #24]
 8006dfa:	68fa      	ldr	r2, [r7, #12]
 8006dfc:	63da      	str	r2, [r3, #60]	; 0x3c
 8006dfe:	69bb      	ldr	r3, [r7, #24]
 8006e00:	68ba      	ldr	r2, [r7, #8]
 8006e02:	641a      	str	r2, [r3, #64]	; 0x40
 8006e04:	2101      	movs	r1, #1
 8006e06:	69b8      	ldr	r0, [r7, #24]
 8006e08:	f7ff ff0a 	bl	8006c20 <xQueueGenericReset>
 8006e0c:	69bb      	ldr	r3, [r7, #24]
 8006e0e:	78fa      	ldrb	r2, [r7, #3]
 8006e10:	f883 204c 	strb.w	r2, [r3, #76]	; 0x4c
 8006e14:	bf00      	nop
 8006e16:	3710      	adds	r7, #16
 8006e18:	46bd      	mov	sp, r7
 8006e1a:	bd80      	pop	{r7, pc}

08006e1c <xQueueGenericSend>:
 8006e1c:	b580      	push	{r7, lr}
 8006e1e:	b08e      	sub	sp, #56	; 0x38
 8006e20:	af00      	add	r7, sp, #0
 8006e22:	60f8      	str	r0, [r7, #12]
 8006e24:	60b9      	str	r1, [r7, #8]
 8006e26:	607a      	str	r2, [r7, #4]
 8006e28:	603b      	str	r3, [r7, #0]
 8006e2a:	2300      	movs	r3, #0
 8006e2c:	637b      	str	r3, [r7, #52]	; 0x34
 8006e2e:	68fb      	ldr	r3, [r7, #12]
 8006e30:	633b      	str	r3, [r7, #48]	; 0x30
 8006e32:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006e34:	2b00      	cmp	r3, #0
 8006e36:	d109      	bne.n	8006e4c <xQueueGenericSend+0x30>
 8006e38:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006e3c:	f383 8811 	msr	BASEPRI, r3
 8006e40:	f3bf 8f6f 	isb	sy
 8006e44:	f3bf 8f4f 	dsb	sy
 8006e48:	62bb      	str	r3, [r7, #40]	; 0x28
 8006e4a:	e7fe      	b.n	8006e4a <xQueueGenericSend+0x2e>
 8006e4c:	68bb      	ldr	r3, [r7, #8]
 8006e4e:	2b00      	cmp	r3, #0
 8006e50:	d103      	bne.n	8006e5a <xQueueGenericSend+0x3e>
 8006e52:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006e54:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8006e56:	2b00      	cmp	r3, #0
 8006e58:	d101      	bne.n	8006e5e <xQueueGenericSend+0x42>
 8006e5a:	2301      	movs	r3, #1
 8006e5c:	e000      	b.n	8006e60 <xQueueGenericSend+0x44>
 8006e5e:	2300      	movs	r3, #0
 8006e60:	2b00      	cmp	r3, #0
 8006e62:	d109      	bne.n	8006e78 <xQueueGenericSend+0x5c>
 8006e64:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006e68:	f383 8811 	msr	BASEPRI, r3
 8006e6c:	f3bf 8f6f 	isb	sy
 8006e70:	f3bf 8f4f 	dsb	sy
 8006e74:	627b      	str	r3, [r7, #36]	; 0x24
 8006e76:	e7fe      	b.n	8006e76 <xQueueGenericSend+0x5a>
 8006e78:	683b      	ldr	r3, [r7, #0]
 8006e7a:	2b02      	cmp	r3, #2
 8006e7c:	d103      	bne.n	8006e86 <xQueueGenericSend+0x6a>
 8006e7e:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006e80:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8006e82:	2b01      	cmp	r3, #1
 8006e84:	d101      	bne.n	8006e8a <xQueueGenericSend+0x6e>
 8006e86:	2301      	movs	r3, #1
 8006e88:	e000      	b.n	8006e8c <xQueueGenericSend+0x70>
 8006e8a:	2300      	movs	r3, #0
 8006e8c:	2b00      	cmp	r3, #0
 8006e8e:	d109      	bne.n	8006ea4 <xQueueGenericSend+0x88>
 8006e90:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006e94:	f383 8811 	msr	BASEPRI, r3
 8006e98:	f3bf 8f6f 	isb	sy
 8006e9c:	f3bf 8f4f 	dsb	sy
 8006ea0:	623b      	str	r3, [r7, #32]
 8006ea2:	e7fe      	b.n	8006ea2 <xQueueGenericSend+0x86>
 8006ea4:	f001 faba 	bl	800841c <xTaskGetSchedulerState>
 8006ea8:	4603      	mov	r3, r0
 8006eaa:	2b00      	cmp	r3, #0
 8006eac:	d102      	bne.n	8006eb4 <xQueueGenericSend+0x98>
 8006eae:	687b      	ldr	r3, [r7, #4]
 8006eb0:	2b00      	cmp	r3, #0
 8006eb2:	d101      	bne.n	8006eb8 <xQueueGenericSend+0x9c>
 8006eb4:	2301      	movs	r3, #1
 8006eb6:	e000      	b.n	8006eba <xQueueGenericSend+0x9e>
 8006eb8:	2300      	movs	r3, #0
 8006eba:	2b00      	cmp	r3, #0
 8006ebc:	d109      	bne.n	8006ed2 <xQueueGenericSend+0xb6>
 8006ebe:	f04f 0350 	mov.w	r3, #80	; 0x50
 8006ec2:	f383 8811 	msr	BASEPRI, r3
 8006ec6:	f3bf 8f6f 	isb	sy
 8006eca:	f3bf 8f4f 	dsb	sy
 8006ece:	61fb      	str	r3, [r7, #28]
 8006ed0:	e7fe      	b.n	8006ed0 <xQueueGenericSend+0xb4>
 8006ed2:	f002 f90d 	bl	80090f0 <vPortEnterCritical>
 8006ed6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006ed8:	6b9a      	ldr	r2, [r3, #56]	; 0x38
 8006eda:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006edc:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8006ede:	429a      	cmp	r2, r3
 8006ee0:	d302      	bcc.n	8006ee8 <xQueueGenericSend+0xcc>
 8006ee2:	683b      	ldr	r3, [r7, #0]
 8006ee4:	2b02      	cmp	r3, #2
 8006ee6:	d129      	bne.n	8006f3c <xQueueGenericSend+0x120>
 8006ee8:	683a      	ldr	r2, [r7, #0]
 8006eea:	68b9      	ldr	r1, [r7, #8]
 8006eec:	6b38      	ldr	r0, [r7, #48]	; 0x30
 8006eee:	f000 fa11 	bl	8007314 <prvCopyDataToQueue>
 8006ef2:	62f8      	str	r0, [r7, #44]	; 0x2c
 8006ef4:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006ef6:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 8006ef8:	2b00      	cmp	r3, #0
 8006efa:	d010      	beq.n	8006f1e <xQueueGenericSend+0x102>
 8006efc:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006efe:	3324      	adds	r3, #36	; 0x24
 8006f00:	4618      	mov	r0, r3
 8006f02:	f001 f8bd 	bl	8008080 <xTaskRemoveFromEventList>
 8006f06:	4603      	mov	r3, r0
 8006f08:	2b00      	cmp	r3, #0
 8006f0a:	d013      	beq.n	8006f34 <xQueueGenericSend+0x118>
 8006f0c:	4b3f      	ldr	r3, [pc, #252]	; (800700c <xQueueGenericSend+0x1f0>)
 8006f0e:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006f12:	601a      	str	r2, [r3, #0]
 8006f14:	f3bf 8f4f 	dsb	sy
 8006f18:	f3bf 8f6f 	isb	sy
 8006f1c:	e00a      	b.n	8006f34 <xQueueGenericSend+0x118>
 8006f1e:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8006f20:	2b00      	cmp	r3, #0
 8006f22:	d007      	beq.n	8006f34 <xQueueGenericSend+0x118>
 8006f24:	4b39      	ldr	r3, [pc, #228]	; (800700c <xQueueGenericSend+0x1f0>)
 8006f26:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006f2a:	601a      	str	r2, [r3, #0]
 8006f2c:	f3bf 8f4f 	dsb	sy
 8006f30:	f3bf 8f6f 	isb	sy
 8006f34:	f002 f90a 	bl	800914c <vPortExitCritical>
 8006f38:	2301      	movs	r3, #1
 8006f3a:	e063      	b.n	8007004 <xQueueGenericSend+0x1e8>
 8006f3c:	687b      	ldr	r3, [r7, #4]
 8006f3e:	2b00      	cmp	r3, #0
 8006f40:	d103      	bne.n	8006f4a <xQueueGenericSend+0x12e>
 8006f42:	f002 f903 	bl	800914c <vPortExitCritical>
 8006f46:	2300      	movs	r3, #0
 8006f48:	e05c      	b.n	8007004 <xQueueGenericSend+0x1e8>
 8006f4a:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8006f4c:	2b00      	cmp	r3, #0
 8006f4e:	d106      	bne.n	8006f5e <xQueueGenericSend+0x142>
 8006f50:	f107 0314 	add.w	r3, r7, #20
 8006f54:	4618      	mov	r0, r3
 8006f56:	f001 f8f5 	bl	8008144 <vTaskInternalSetTimeOutState>
 8006f5a:	2301      	movs	r3, #1
 8006f5c:	637b      	str	r3, [r7, #52]	; 0x34
 8006f5e:	f002 f8f5 	bl	800914c <vPortExitCritical>
 8006f62:	f000 fe3f 	bl	8007be4 <vTaskSuspendAll>
 8006f66:	f002 f8c3 	bl	80090f0 <vPortEnterCritical>
 8006f6a:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006f6c:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8006f70:	b25b      	sxtb	r3, r3
 8006f72:	f1b3 3fff 	cmp.w	r3, #4294967295
 8006f76:	d103      	bne.n	8006f80 <xQueueGenericSend+0x164>
 8006f78:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006f7a:	2200      	movs	r2, #0
 8006f7c:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8006f80:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006f82:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 8006f86:	b25b      	sxtb	r3, r3
 8006f88:	f1b3 3fff 	cmp.w	r3, #4294967295
 8006f8c:	d103      	bne.n	8006f96 <xQueueGenericSend+0x17a>
 8006f8e:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006f90:	2200      	movs	r2, #0
 8006f92:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 8006f96:	f002 f8d9 	bl	800914c <vPortExitCritical>
 8006f9a:	1d3a      	adds	r2, r7, #4
 8006f9c:	f107 0314 	add.w	r3, r7, #20
 8006fa0:	4611      	mov	r1, r2
 8006fa2:	4618      	mov	r0, r3
 8006fa4:	f001 f8e4 	bl	8008170 <xTaskCheckForTimeOut>
 8006fa8:	4603      	mov	r3, r0
 8006faa:	2b00      	cmp	r3, #0
 8006fac:	d124      	bne.n	8006ff8 <xQueueGenericSend+0x1dc>
 8006fae:	6b38      	ldr	r0, [r7, #48]	; 0x30
 8006fb0:	f000 faa8 	bl	8007504 <prvIsQueueFull>
 8006fb4:	4603      	mov	r3, r0
 8006fb6:	2b00      	cmp	r3, #0
 8006fb8:	d018      	beq.n	8006fec <xQueueGenericSend+0x1d0>
 8006fba:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8006fbc:	3310      	adds	r3, #16
 8006fbe:	687a      	ldr	r2, [r7, #4]
 8006fc0:	4611      	mov	r1, r2
 8006fc2:	4618      	mov	r0, r3
 8006fc4:	f001 f80e 	bl	8007fe4 <vTaskPlaceOnEventList>
 8006fc8:	6b38      	ldr	r0, [r7, #48]	; 0x30
 8006fca:	f000 fa33 	bl	8007434 <prvUnlockQueue>
 8006fce:	f000 fe17 	bl	8007c00 <xTaskResumeAll>
 8006fd2:	4603      	mov	r3, r0
 8006fd4:	2b00      	cmp	r3, #0
 8006fd6:	f47f af7c 	bne.w	8006ed2 <xQueueGenericSend+0xb6>
 8006fda:	4b0c      	ldr	r3, [pc, #48]	; (800700c <xQueueGenericSend+0x1f0>)
 8006fdc:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8006fe0:	601a      	str	r2, [r3, #0]
 8006fe2:	f3bf 8f4f 	dsb	sy
 8006fe6:	f3bf 8f6f 	isb	sy
 8006fea:	e772      	b.n	8006ed2 <xQueueGenericSend+0xb6>
 8006fec:	6b38      	ldr	r0, [r7, #48]	; 0x30
 8006fee:	f000 fa21 	bl	8007434 <prvUnlockQueue>
 8006ff2:	f000 fe05 	bl	8007c00 <xTaskResumeAll>
 8006ff6:	e76c      	b.n	8006ed2 <xQueueGenericSend+0xb6>
 8006ff8:	6b38      	ldr	r0, [r7, #48]	; 0x30
 8006ffa:	f000 fa1b 	bl	8007434 <prvUnlockQueue>
 8006ffe:	f000 fdff 	bl	8007c00 <xTaskResumeAll>
 8007002:	2300      	movs	r3, #0
 8007004:	4618      	mov	r0, r3
 8007006:	3738      	adds	r7, #56	; 0x38
 8007008:	46bd      	mov	sp, r7
 800700a:	bd80      	pop	{r7, pc}
 800700c:	e000ed04 	.word	0xe000ed04

08007010 <xQueueGenericSendFromISR>:
 8007010:	b580      	push	{r7, lr}
 8007012:	b090      	sub	sp, #64	; 0x40
 8007014:	af00      	add	r7, sp, #0
 8007016:	60f8      	str	r0, [r7, #12]
 8007018:	60b9      	str	r1, [r7, #8]
 800701a:	607a      	str	r2, [r7, #4]
 800701c:	603b      	str	r3, [r7, #0]
 800701e:	68fb      	ldr	r3, [r7, #12]
 8007020:	63bb      	str	r3, [r7, #56]	; 0x38
 8007022:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007024:	2b00      	cmp	r3, #0
 8007026:	d109      	bne.n	800703c <xQueueGenericSendFromISR+0x2c>
 8007028:	f04f 0350 	mov.w	r3, #80	; 0x50
 800702c:	f383 8811 	msr	BASEPRI, r3
 8007030:	f3bf 8f6f 	isb	sy
 8007034:	f3bf 8f4f 	dsb	sy
 8007038:	62bb      	str	r3, [r7, #40]	; 0x28
 800703a:	e7fe      	b.n	800703a <xQueueGenericSendFromISR+0x2a>
 800703c:	68bb      	ldr	r3, [r7, #8]
 800703e:	2b00      	cmp	r3, #0
 8007040:	d103      	bne.n	800704a <xQueueGenericSendFromISR+0x3a>
 8007042:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007044:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007046:	2b00      	cmp	r3, #0
 8007048:	d101      	bne.n	800704e <xQueueGenericSendFromISR+0x3e>
 800704a:	2301      	movs	r3, #1
 800704c:	e000      	b.n	8007050 <xQueueGenericSendFromISR+0x40>
 800704e:	2300      	movs	r3, #0
 8007050:	2b00      	cmp	r3, #0
 8007052:	d109      	bne.n	8007068 <xQueueGenericSendFromISR+0x58>
 8007054:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007058:	f383 8811 	msr	BASEPRI, r3
 800705c:	f3bf 8f6f 	isb	sy
 8007060:	f3bf 8f4f 	dsb	sy
 8007064:	627b      	str	r3, [r7, #36]	; 0x24
 8007066:	e7fe      	b.n	8007066 <xQueueGenericSendFromISR+0x56>
 8007068:	683b      	ldr	r3, [r7, #0]
 800706a:	2b02      	cmp	r3, #2
 800706c:	d103      	bne.n	8007076 <xQueueGenericSendFromISR+0x66>
 800706e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007070:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8007072:	2b01      	cmp	r3, #1
 8007074:	d101      	bne.n	800707a <xQueueGenericSendFromISR+0x6a>
 8007076:	2301      	movs	r3, #1
 8007078:	e000      	b.n	800707c <xQueueGenericSendFromISR+0x6c>
 800707a:	2300      	movs	r3, #0
 800707c:	2b00      	cmp	r3, #0
 800707e:	d109      	bne.n	8007094 <xQueueGenericSendFromISR+0x84>
 8007080:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007084:	f383 8811 	msr	BASEPRI, r3
 8007088:	f3bf 8f6f 	isb	sy
 800708c:	f3bf 8f4f 	dsb	sy
 8007090:	623b      	str	r3, [r7, #32]
 8007092:	e7fe      	b.n	8007092 <xQueueGenericSendFromISR+0x82>
 8007094:	f002 f908 	bl	80092a8 <vPortValidateInterruptPriority>
 8007098:	f3ef 8211 	mrs	r2, BASEPRI
 800709c:	f04f 0350 	mov.w	r3, #80	; 0x50
 80070a0:	f383 8811 	msr	BASEPRI, r3
 80070a4:	f3bf 8f6f 	isb	sy
 80070a8:	f3bf 8f4f 	dsb	sy
 80070ac:	61fa      	str	r2, [r7, #28]
 80070ae:	61bb      	str	r3, [r7, #24]
 80070b0:	69fb      	ldr	r3, [r7, #28]
 80070b2:	637b      	str	r3, [r7, #52]	; 0x34
 80070b4:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80070b6:	6b9a      	ldr	r2, [r3, #56]	; 0x38
 80070b8:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80070ba:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 80070bc:	429a      	cmp	r2, r3
 80070be:	d302      	bcc.n	80070c6 <xQueueGenericSendFromISR+0xb6>
 80070c0:	683b      	ldr	r3, [r7, #0]
 80070c2:	2b02      	cmp	r3, #2
 80070c4:	d13d      	bne.n	8007142 <xQueueGenericSendFromISR+0x132>
 80070c6:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80070c8:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 80070cc:	f887 3033 	strb.w	r3, [r7, #51]	; 0x33
 80070d0:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80070d2:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 80070d4:	62fb      	str	r3, [r7, #44]	; 0x2c
 80070d6:	683a      	ldr	r2, [r7, #0]
 80070d8:	68b9      	ldr	r1, [r7, #8]
 80070da:	6bb8      	ldr	r0, [r7, #56]	; 0x38
 80070dc:	f000 f91a 	bl	8007314 <prvCopyDataToQueue>
 80070e0:	f997 3033 	ldrsb.w	r3, [r7, #51]	; 0x33
 80070e4:	f1b3 3fff 	cmp.w	r3, #4294967295
 80070e8:	d112      	bne.n	8007110 <xQueueGenericSendFromISR+0x100>
 80070ea:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80070ec:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 80070ee:	2b00      	cmp	r3, #0
 80070f0:	d024      	beq.n	800713c <xQueueGenericSendFromISR+0x12c>
 80070f2:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80070f4:	3324      	adds	r3, #36	; 0x24
 80070f6:	4618      	mov	r0, r3
 80070f8:	f000 ffc2 	bl	8008080 <xTaskRemoveFromEventList>
 80070fc:	4603      	mov	r3, r0
 80070fe:	2b00      	cmp	r3, #0
 8007100:	d01c      	beq.n	800713c <xQueueGenericSendFromISR+0x12c>
 8007102:	687b      	ldr	r3, [r7, #4]
 8007104:	2b00      	cmp	r3, #0
 8007106:	d019      	beq.n	800713c <xQueueGenericSendFromISR+0x12c>
 8007108:	687b      	ldr	r3, [r7, #4]
 800710a:	2201      	movs	r2, #1
 800710c:	601a      	str	r2, [r3, #0]
 800710e:	e015      	b.n	800713c <xQueueGenericSendFromISR+0x12c>
 8007110:	f997 3033 	ldrsb.w	r3, [r7, #51]	; 0x33
 8007114:	2b7f      	cmp	r3, #127	; 0x7f
 8007116:	d109      	bne.n	800712c <xQueueGenericSendFromISR+0x11c>
 8007118:	f04f 0350 	mov.w	r3, #80	; 0x50
 800711c:	f383 8811 	msr	BASEPRI, r3
 8007120:	f3bf 8f6f 	isb	sy
 8007124:	f3bf 8f4f 	dsb	sy
 8007128:	617b      	str	r3, [r7, #20]
 800712a:	e7fe      	b.n	800712a <xQueueGenericSendFromISR+0x11a>
 800712c:	f897 3033 	ldrb.w	r3, [r7, #51]	; 0x33
 8007130:	3301      	adds	r3, #1
 8007132:	b2db      	uxtb	r3, r3
 8007134:	b25a      	sxtb	r2, r3
 8007136:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007138:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 800713c:	2301      	movs	r3, #1
 800713e:	63fb      	str	r3, [r7, #60]	; 0x3c
 8007140:	e001      	b.n	8007146 <xQueueGenericSendFromISR+0x136>
 8007142:	2300      	movs	r3, #0
 8007144:	63fb      	str	r3, [r7, #60]	; 0x3c
 8007146:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8007148:	613b      	str	r3, [r7, #16]
 800714a:	693b      	ldr	r3, [r7, #16]
 800714c:	f383 8811 	msr	BASEPRI, r3
 8007150:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8007152:	4618      	mov	r0, r3
 8007154:	3740      	adds	r7, #64	; 0x40
 8007156:	46bd      	mov	sp, r7
 8007158:	bd80      	pop	{r7, pc}
	...

0800715c <xQueueReceive>:
 800715c:	b580      	push	{r7, lr}
 800715e:	b08c      	sub	sp, #48	; 0x30
 8007160:	af00      	add	r7, sp, #0
 8007162:	60f8      	str	r0, [r7, #12]
 8007164:	60b9      	str	r1, [r7, #8]
 8007166:	607a      	str	r2, [r7, #4]
 8007168:	2300      	movs	r3, #0
 800716a:	62fb      	str	r3, [r7, #44]	; 0x2c
 800716c:	68fb      	ldr	r3, [r7, #12]
 800716e:	62bb      	str	r3, [r7, #40]	; 0x28
 8007170:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007172:	2b00      	cmp	r3, #0
 8007174:	d109      	bne.n	800718a <xQueueReceive+0x2e>
 8007176:	f04f 0350 	mov.w	r3, #80	; 0x50
 800717a:	f383 8811 	msr	BASEPRI, r3
 800717e:	f3bf 8f6f 	isb	sy
 8007182:	f3bf 8f4f 	dsb	sy
 8007186:	623b      	str	r3, [r7, #32]
 8007188:	e7fe      	b.n	8007188 <xQueueReceive+0x2c>
 800718a:	68bb      	ldr	r3, [r7, #8]
 800718c:	2b00      	cmp	r3, #0
 800718e:	d103      	bne.n	8007198 <xQueueReceive+0x3c>
 8007190:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007192:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007194:	2b00      	cmp	r3, #0
 8007196:	d101      	bne.n	800719c <xQueueReceive+0x40>
 8007198:	2301      	movs	r3, #1
 800719a:	e000      	b.n	800719e <xQueueReceive+0x42>
 800719c:	2300      	movs	r3, #0
 800719e:	2b00      	cmp	r3, #0
 80071a0:	d109      	bne.n	80071b6 <xQueueReceive+0x5a>
 80071a2:	f04f 0350 	mov.w	r3, #80	; 0x50
 80071a6:	f383 8811 	msr	BASEPRI, r3
 80071aa:	f3bf 8f6f 	isb	sy
 80071ae:	f3bf 8f4f 	dsb	sy
 80071b2:	61fb      	str	r3, [r7, #28]
 80071b4:	e7fe      	b.n	80071b4 <xQueueReceive+0x58>
 80071b6:	f001 f931 	bl	800841c <xTaskGetSchedulerState>
 80071ba:	4603      	mov	r3, r0
 80071bc:	2b00      	cmp	r3, #0
 80071be:	d102      	bne.n	80071c6 <xQueueReceive+0x6a>
 80071c0:	687b      	ldr	r3, [r7, #4]
 80071c2:	2b00      	cmp	r3, #0
 80071c4:	d101      	bne.n	80071ca <xQueueReceive+0x6e>
 80071c6:	2301      	movs	r3, #1
 80071c8:	e000      	b.n	80071cc <xQueueReceive+0x70>
 80071ca:	2300      	movs	r3, #0
 80071cc:	2b00      	cmp	r3, #0
 80071ce:	d109      	bne.n	80071e4 <xQueueReceive+0x88>
 80071d0:	f04f 0350 	mov.w	r3, #80	; 0x50
 80071d4:	f383 8811 	msr	BASEPRI, r3
 80071d8:	f3bf 8f6f 	isb	sy
 80071dc:	f3bf 8f4f 	dsb	sy
 80071e0:	61bb      	str	r3, [r7, #24]
 80071e2:	e7fe      	b.n	80071e2 <xQueueReceive+0x86>
 80071e4:	f001 ff84 	bl	80090f0 <vPortEnterCritical>
 80071e8:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80071ea:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 80071ec:	627b      	str	r3, [r7, #36]	; 0x24
 80071ee:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80071f0:	2b00      	cmp	r3, #0
 80071f2:	d01f      	beq.n	8007234 <xQueueReceive+0xd8>
 80071f4:	68b9      	ldr	r1, [r7, #8]
 80071f6:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80071f8:	f000 f8f6 	bl	80073e8 <prvCopyDataFromQueue>
 80071fc:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80071fe:	1e5a      	subs	r2, r3, #1
 8007200:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007202:	639a      	str	r2, [r3, #56]	; 0x38
 8007204:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007206:	691b      	ldr	r3, [r3, #16]
 8007208:	2b00      	cmp	r3, #0
 800720a:	d00f      	beq.n	800722c <xQueueReceive+0xd0>
 800720c:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800720e:	3310      	adds	r3, #16
 8007210:	4618      	mov	r0, r3
 8007212:	f000 ff35 	bl	8008080 <xTaskRemoveFromEventList>
 8007216:	4603      	mov	r3, r0
 8007218:	2b00      	cmp	r3, #0
 800721a:	d007      	beq.n	800722c <xQueueReceive+0xd0>
 800721c:	4b3c      	ldr	r3, [pc, #240]	; (8007310 <xQueueReceive+0x1b4>)
 800721e:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8007222:	601a      	str	r2, [r3, #0]
 8007224:	f3bf 8f4f 	dsb	sy
 8007228:	f3bf 8f6f 	isb	sy
 800722c:	f001 ff8e 	bl	800914c <vPortExitCritical>
 8007230:	2301      	movs	r3, #1
 8007232:	e069      	b.n	8007308 <xQueueReceive+0x1ac>
 8007234:	687b      	ldr	r3, [r7, #4]
 8007236:	2b00      	cmp	r3, #0
 8007238:	d103      	bne.n	8007242 <xQueueReceive+0xe6>
 800723a:	f001 ff87 	bl	800914c <vPortExitCritical>
 800723e:	2300      	movs	r3, #0
 8007240:	e062      	b.n	8007308 <xQueueReceive+0x1ac>
 8007242:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8007244:	2b00      	cmp	r3, #0
 8007246:	d106      	bne.n	8007256 <xQueueReceive+0xfa>
 8007248:	f107 0310 	add.w	r3, r7, #16
 800724c:	4618      	mov	r0, r3
 800724e:	f000 ff79 	bl	8008144 <vTaskInternalSetTimeOutState>
 8007252:	2301      	movs	r3, #1
 8007254:	62fb      	str	r3, [r7, #44]	; 0x2c
 8007256:	f001 ff79 	bl	800914c <vPortExitCritical>
 800725a:	f000 fcc3 	bl	8007be4 <vTaskSuspendAll>
 800725e:	f001 ff47 	bl	80090f0 <vPortEnterCritical>
 8007262:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007264:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8007268:	b25b      	sxtb	r3, r3
 800726a:	f1b3 3fff 	cmp.w	r3, #4294967295
 800726e:	d103      	bne.n	8007278 <xQueueReceive+0x11c>
 8007270:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007272:	2200      	movs	r2, #0
 8007274:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 8007278:	6abb      	ldr	r3, [r7, #40]	; 0x28
 800727a:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 800727e:	b25b      	sxtb	r3, r3
 8007280:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007284:	d103      	bne.n	800728e <xQueueReceive+0x132>
 8007286:	6abb      	ldr	r3, [r7, #40]	; 0x28
 8007288:	2200      	movs	r2, #0
 800728a:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 800728e:	f001 ff5d 	bl	800914c <vPortExitCritical>
 8007292:	1d3a      	adds	r2, r7, #4
 8007294:	f107 0310 	add.w	r3, r7, #16
 8007298:	4611      	mov	r1, r2
 800729a:	4618      	mov	r0, r3
 800729c:	f000 ff68 	bl	8008170 <xTaskCheckForTimeOut>
 80072a0:	4603      	mov	r3, r0
 80072a2:	2b00      	cmp	r3, #0
 80072a4:	d123      	bne.n	80072ee <xQueueReceive+0x192>
 80072a6:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80072a8:	f000 f916 	bl	80074d8 <prvIsQueueEmpty>
 80072ac:	4603      	mov	r3, r0
 80072ae:	2b00      	cmp	r3, #0
 80072b0:	d017      	beq.n	80072e2 <xQueueReceive+0x186>
 80072b2:	6abb      	ldr	r3, [r7, #40]	; 0x28
 80072b4:	3324      	adds	r3, #36	; 0x24
 80072b6:	687a      	ldr	r2, [r7, #4]
 80072b8:	4611      	mov	r1, r2
 80072ba:	4618      	mov	r0, r3
 80072bc:	f000 fe92 	bl	8007fe4 <vTaskPlaceOnEventList>
 80072c0:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80072c2:	f000 f8b7 	bl	8007434 <prvUnlockQueue>
 80072c6:	f000 fc9b 	bl	8007c00 <xTaskResumeAll>
 80072ca:	4603      	mov	r3, r0
 80072cc:	2b00      	cmp	r3, #0
 80072ce:	d189      	bne.n	80071e4 <xQueueReceive+0x88>
 80072d0:	4b0f      	ldr	r3, [pc, #60]	; (8007310 <xQueueReceive+0x1b4>)
 80072d2:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80072d6:	601a      	str	r2, [r3, #0]
 80072d8:	f3bf 8f4f 	dsb	sy
 80072dc:	f3bf 8f6f 	isb	sy
 80072e0:	e780      	b.n	80071e4 <xQueueReceive+0x88>
 80072e2:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80072e4:	f000 f8a6 	bl	8007434 <prvUnlockQueue>
 80072e8:	f000 fc8a 	bl	8007c00 <xTaskResumeAll>
 80072ec:	e77a      	b.n	80071e4 <xQueueReceive+0x88>
 80072ee:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80072f0:	f000 f8a0 	bl	8007434 <prvUnlockQueue>
 80072f4:	f000 fc84 	bl	8007c00 <xTaskResumeAll>
 80072f8:	6ab8      	ldr	r0, [r7, #40]	; 0x28
 80072fa:	f000 f8ed 	bl	80074d8 <prvIsQueueEmpty>
 80072fe:	4603      	mov	r3, r0
 8007300:	2b00      	cmp	r3, #0
 8007302:	f43f af6f 	beq.w	80071e4 <xQueueReceive+0x88>
 8007306:	2300      	movs	r3, #0
 8007308:	4618      	mov	r0, r3
 800730a:	3730      	adds	r7, #48	; 0x30
 800730c:	46bd      	mov	sp, r7
 800730e:	bd80      	pop	{r7, pc}
 8007310:	e000ed04 	.word	0xe000ed04

08007314 <prvCopyDataToQueue>:
 8007314:	b580      	push	{r7, lr}
 8007316:	b086      	sub	sp, #24
 8007318:	af00      	add	r7, sp, #0
 800731a:	60f8      	str	r0, [r7, #12]
 800731c:	60b9      	str	r1, [r7, #8]
 800731e:	607a      	str	r2, [r7, #4]
 8007320:	2300      	movs	r3, #0
 8007322:	617b      	str	r3, [r7, #20]
 8007324:	68fb      	ldr	r3, [r7, #12]
 8007326:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 8007328:	613b      	str	r3, [r7, #16]
 800732a:	68fb      	ldr	r3, [r7, #12]
 800732c:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 800732e:	2b00      	cmp	r3, #0
 8007330:	d10d      	bne.n	800734e <prvCopyDataToQueue+0x3a>
 8007332:	68fb      	ldr	r3, [r7, #12]
 8007334:	681b      	ldr	r3, [r3, #0]
 8007336:	2b00      	cmp	r3, #0
 8007338:	d14d      	bne.n	80073d6 <prvCopyDataToQueue+0xc2>
 800733a:	68fb      	ldr	r3, [r7, #12]
 800733c:	689b      	ldr	r3, [r3, #8]
 800733e:	4618      	mov	r0, r3
 8007340:	f001 f88a 	bl	8008458 <xTaskPriorityDisinherit>
 8007344:	6178      	str	r0, [r7, #20]
 8007346:	68fb      	ldr	r3, [r7, #12]
 8007348:	2200      	movs	r2, #0
 800734a:	609a      	str	r2, [r3, #8]
 800734c:	e043      	b.n	80073d6 <prvCopyDataToQueue+0xc2>
 800734e:	687b      	ldr	r3, [r7, #4]
 8007350:	2b00      	cmp	r3, #0
 8007352:	d119      	bne.n	8007388 <prvCopyDataToQueue+0x74>
 8007354:	68fb      	ldr	r3, [r7, #12]
 8007356:	6858      	ldr	r0, [r3, #4]
 8007358:	68fb      	ldr	r3, [r7, #12]
 800735a:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 800735c:	461a      	mov	r2, r3
 800735e:	68b9      	ldr	r1, [r7, #8]
 8007360:	f7f9 fc72 	bl	8000c48 <memcpy>
 8007364:	68fb      	ldr	r3, [r7, #12]
 8007366:	685a      	ldr	r2, [r3, #4]
 8007368:	68fb      	ldr	r3, [r7, #12]
 800736a:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 800736c:	441a      	add	r2, r3
 800736e:	68fb      	ldr	r3, [r7, #12]
 8007370:	605a      	str	r2, [r3, #4]
 8007372:	68fb      	ldr	r3, [r7, #12]
 8007374:	685a      	ldr	r2, [r3, #4]
 8007376:	68fb      	ldr	r3, [r7, #12]
 8007378:	689b      	ldr	r3, [r3, #8]
 800737a:	429a      	cmp	r2, r3
 800737c:	d32b      	bcc.n	80073d6 <prvCopyDataToQueue+0xc2>
 800737e:	68fb      	ldr	r3, [r7, #12]
 8007380:	681a      	ldr	r2, [r3, #0]
 8007382:	68fb      	ldr	r3, [r7, #12]
 8007384:	605a      	str	r2, [r3, #4]
 8007386:	e026      	b.n	80073d6 <prvCopyDataToQueue+0xc2>
 8007388:	68fb      	ldr	r3, [r7, #12]
 800738a:	68d8      	ldr	r0, [r3, #12]
 800738c:	68fb      	ldr	r3, [r7, #12]
 800738e:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007390:	461a      	mov	r2, r3
 8007392:	68b9      	ldr	r1, [r7, #8]
 8007394:	f7f9 fc58 	bl	8000c48 <memcpy>
 8007398:	68fb      	ldr	r3, [r7, #12]
 800739a:	68da      	ldr	r2, [r3, #12]
 800739c:	68fb      	ldr	r3, [r7, #12]
 800739e:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 80073a0:	425b      	negs	r3, r3
 80073a2:	441a      	add	r2, r3
 80073a4:	68fb      	ldr	r3, [r7, #12]
 80073a6:	60da      	str	r2, [r3, #12]
 80073a8:	68fb      	ldr	r3, [r7, #12]
 80073aa:	68da      	ldr	r2, [r3, #12]
 80073ac:	68fb      	ldr	r3, [r7, #12]
 80073ae:	681b      	ldr	r3, [r3, #0]
 80073b0:	429a      	cmp	r2, r3
 80073b2:	d207      	bcs.n	80073c4 <prvCopyDataToQueue+0xb0>
 80073b4:	68fb      	ldr	r3, [r7, #12]
 80073b6:	689a      	ldr	r2, [r3, #8]
 80073b8:	68fb      	ldr	r3, [r7, #12]
 80073ba:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 80073bc:	425b      	negs	r3, r3
 80073be:	441a      	add	r2, r3
 80073c0:	68fb      	ldr	r3, [r7, #12]
 80073c2:	60da      	str	r2, [r3, #12]
 80073c4:	687b      	ldr	r3, [r7, #4]
 80073c6:	2b02      	cmp	r3, #2
 80073c8:	d105      	bne.n	80073d6 <prvCopyDataToQueue+0xc2>
 80073ca:	693b      	ldr	r3, [r7, #16]
 80073cc:	2b00      	cmp	r3, #0
 80073ce:	d002      	beq.n	80073d6 <prvCopyDataToQueue+0xc2>
 80073d0:	693b      	ldr	r3, [r7, #16]
 80073d2:	3b01      	subs	r3, #1
 80073d4:	613b      	str	r3, [r7, #16]
 80073d6:	693b      	ldr	r3, [r7, #16]
 80073d8:	1c5a      	adds	r2, r3, #1
 80073da:	68fb      	ldr	r3, [r7, #12]
 80073dc:	639a      	str	r2, [r3, #56]	; 0x38
 80073de:	697b      	ldr	r3, [r7, #20]
 80073e0:	4618      	mov	r0, r3
 80073e2:	3718      	adds	r7, #24
 80073e4:	46bd      	mov	sp, r7
 80073e6:	bd80      	pop	{r7, pc}

080073e8 <prvCopyDataFromQueue>:
 80073e8:	b580      	push	{r7, lr}
 80073ea:	b082      	sub	sp, #8
 80073ec:	af00      	add	r7, sp, #0
 80073ee:	6078      	str	r0, [r7, #4]
 80073f0:	6039      	str	r1, [r7, #0]
 80073f2:	687b      	ldr	r3, [r7, #4]
 80073f4:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 80073f6:	2b00      	cmp	r3, #0
 80073f8:	d018      	beq.n	800742c <prvCopyDataFromQueue+0x44>
 80073fa:	687b      	ldr	r3, [r7, #4]
 80073fc:	68da      	ldr	r2, [r3, #12]
 80073fe:	687b      	ldr	r3, [r7, #4]
 8007400:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007402:	441a      	add	r2, r3
 8007404:	687b      	ldr	r3, [r7, #4]
 8007406:	60da      	str	r2, [r3, #12]
 8007408:	687b      	ldr	r3, [r7, #4]
 800740a:	68da      	ldr	r2, [r3, #12]
 800740c:	687b      	ldr	r3, [r7, #4]
 800740e:	689b      	ldr	r3, [r3, #8]
 8007410:	429a      	cmp	r2, r3
 8007412:	d303      	bcc.n	800741c <prvCopyDataFromQueue+0x34>
 8007414:	687b      	ldr	r3, [r7, #4]
 8007416:	681a      	ldr	r2, [r3, #0]
 8007418:	687b      	ldr	r3, [r7, #4]
 800741a:	60da      	str	r2, [r3, #12]
 800741c:	687b      	ldr	r3, [r7, #4]
 800741e:	68d9      	ldr	r1, [r3, #12]
 8007420:	687b      	ldr	r3, [r7, #4]
 8007422:	6c1b      	ldr	r3, [r3, #64]	; 0x40
 8007424:	461a      	mov	r2, r3
 8007426:	6838      	ldr	r0, [r7, #0]
 8007428:	f7f9 fc0e 	bl	8000c48 <memcpy>
 800742c:	bf00      	nop
 800742e:	3708      	adds	r7, #8
 8007430:	46bd      	mov	sp, r7
 8007432:	bd80      	pop	{r7, pc}

08007434 <prvUnlockQueue>:
 8007434:	b580      	push	{r7, lr}
 8007436:	b084      	sub	sp, #16
 8007438:	af00      	add	r7, sp, #0
 800743a:	6078      	str	r0, [r7, #4]
 800743c:	f001 fe58 	bl	80090f0 <vPortEnterCritical>
 8007440:	687b      	ldr	r3, [r7, #4]
 8007442:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 8007446:	73fb      	strb	r3, [r7, #15]
 8007448:	e011      	b.n	800746e <prvUnlockQueue+0x3a>
 800744a:	687b      	ldr	r3, [r7, #4]
 800744c:	6a5b      	ldr	r3, [r3, #36]	; 0x24
 800744e:	2b00      	cmp	r3, #0
 8007450:	d012      	beq.n	8007478 <prvUnlockQueue+0x44>
 8007452:	687b      	ldr	r3, [r7, #4]
 8007454:	3324      	adds	r3, #36	; 0x24
 8007456:	4618      	mov	r0, r3
 8007458:	f000 fe12 	bl	8008080 <xTaskRemoveFromEventList>
 800745c:	4603      	mov	r3, r0
 800745e:	2b00      	cmp	r3, #0
 8007460:	d001      	beq.n	8007466 <prvUnlockQueue+0x32>
 8007462:	f000 fee9 	bl	8008238 <vTaskMissedYield>
 8007466:	7bfb      	ldrb	r3, [r7, #15]
 8007468:	3b01      	subs	r3, #1
 800746a:	b2db      	uxtb	r3, r3
 800746c:	73fb      	strb	r3, [r7, #15]
 800746e:	f997 300f 	ldrsb.w	r3, [r7, #15]
 8007472:	2b00      	cmp	r3, #0
 8007474:	dce9      	bgt.n	800744a <prvUnlockQueue+0x16>
 8007476:	e000      	b.n	800747a <prvUnlockQueue+0x46>
 8007478:	bf00      	nop
 800747a:	687b      	ldr	r3, [r7, #4]
 800747c:	22ff      	movs	r2, #255	; 0xff
 800747e:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 8007482:	f001 fe63 	bl	800914c <vPortExitCritical>
 8007486:	f001 fe33 	bl	80090f0 <vPortEnterCritical>
 800748a:	687b      	ldr	r3, [r7, #4]
 800748c:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 8007490:	73bb      	strb	r3, [r7, #14]
 8007492:	e011      	b.n	80074b8 <prvUnlockQueue+0x84>
 8007494:	687b      	ldr	r3, [r7, #4]
 8007496:	691b      	ldr	r3, [r3, #16]
 8007498:	2b00      	cmp	r3, #0
 800749a:	d012      	beq.n	80074c2 <prvUnlockQueue+0x8e>
 800749c:	687b      	ldr	r3, [r7, #4]
 800749e:	3310      	adds	r3, #16
 80074a0:	4618      	mov	r0, r3
 80074a2:	f000 fded 	bl	8008080 <xTaskRemoveFromEventList>
 80074a6:	4603      	mov	r3, r0
 80074a8:	2b00      	cmp	r3, #0
 80074aa:	d001      	beq.n	80074b0 <prvUnlockQueue+0x7c>
 80074ac:	f000 fec4 	bl	8008238 <vTaskMissedYield>
 80074b0:	7bbb      	ldrb	r3, [r7, #14]
 80074b2:	3b01      	subs	r3, #1
 80074b4:	b2db      	uxtb	r3, r3
 80074b6:	73bb      	strb	r3, [r7, #14]
 80074b8:	f997 300e 	ldrsb.w	r3, [r7, #14]
 80074bc:	2b00      	cmp	r3, #0
 80074be:	dce9      	bgt.n	8007494 <prvUnlockQueue+0x60>
 80074c0:	e000      	b.n	80074c4 <prvUnlockQueue+0x90>
 80074c2:	bf00      	nop
 80074c4:	687b      	ldr	r3, [r7, #4]
 80074c6:	22ff      	movs	r2, #255	; 0xff
 80074c8:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 80074cc:	f001 fe3e 	bl	800914c <vPortExitCritical>
 80074d0:	bf00      	nop
 80074d2:	3710      	adds	r7, #16
 80074d4:	46bd      	mov	sp, r7
 80074d6:	bd80      	pop	{r7, pc}

080074d8 <prvIsQueueEmpty>:
 80074d8:	b580      	push	{r7, lr}
 80074da:	b084      	sub	sp, #16
 80074dc:	af00      	add	r7, sp, #0
 80074de:	6078      	str	r0, [r7, #4]
 80074e0:	f001 fe06 	bl	80090f0 <vPortEnterCritical>
 80074e4:	687b      	ldr	r3, [r7, #4]
 80074e6:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 80074e8:	2b00      	cmp	r3, #0
 80074ea:	d102      	bne.n	80074f2 <prvIsQueueEmpty+0x1a>
 80074ec:	2301      	movs	r3, #1
 80074ee:	60fb      	str	r3, [r7, #12]
 80074f0:	e001      	b.n	80074f6 <prvIsQueueEmpty+0x1e>
 80074f2:	2300      	movs	r3, #0
 80074f4:	60fb      	str	r3, [r7, #12]
 80074f6:	f001 fe29 	bl	800914c <vPortExitCritical>
 80074fa:	68fb      	ldr	r3, [r7, #12]
 80074fc:	4618      	mov	r0, r3
 80074fe:	3710      	adds	r7, #16
 8007500:	46bd      	mov	sp, r7
 8007502:	bd80      	pop	{r7, pc}

08007504 <prvIsQueueFull>:
 8007504:	b580      	push	{r7, lr}
 8007506:	b084      	sub	sp, #16
 8007508:	af00      	add	r7, sp, #0
 800750a:	6078      	str	r0, [r7, #4]
 800750c:	f001 fdf0 	bl	80090f0 <vPortEnterCritical>
 8007510:	687b      	ldr	r3, [r7, #4]
 8007512:	6b9a      	ldr	r2, [r3, #56]	; 0x38
 8007514:	687b      	ldr	r3, [r7, #4]
 8007516:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 8007518:	429a      	cmp	r2, r3
 800751a:	d102      	bne.n	8007522 <prvIsQueueFull+0x1e>
 800751c:	2301      	movs	r3, #1
 800751e:	60fb      	str	r3, [r7, #12]
 8007520:	e001      	b.n	8007526 <prvIsQueueFull+0x22>
 8007522:	2300      	movs	r3, #0
 8007524:	60fb      	str	r3, [r7, #12]
 8007526:	f001 fe11 	bl	800914c <vPortExitCritical>
 800752a:	68fb      	ldr	r3, [r7, #12]
 800752c:	4618      	mov	r0, r3
 800752e:	3710      	adds	r7, #16
 8007530:	46bd      	mov	sp, r7
 8007532:	bd80      	pop	{r7, pc}

08007534 <vQueueAddToRegistry>:
 8007534:	b480      	push	{r7}
 8007536:	b085      	sub	sp, #20
 8007538:	af00      	add	r7, sp, #0
 800753a:	6078      	str	r0, [r7, #4]
 800753c:	6039      	str	r1, [r7, #0]
 800753e:	2300      	movs	r3, #0
 8007540:	60fb      	str	r3, [r7, #12]
 8007542:	e014      	b.n	800756e <vQueueAddToRegistry+0x3a>
 8007544:	4a0e      	ldr	r2, [pc, #56]	; (8007580 <vQueueAddToRegistry+0x4c>)
 8007546:	68fb      	ldr	r3, [r7, #12]
 8007548:	f852 3033 	ldr.w	r3, [r2, r3, lsl #3]
 800754c:	2b00      	cmp	r3, #0
 800754e:	d10b      	bne.n	8007568 <vQueueAddToRegistry+0x34>
 8007550:	490b      	ldr	r1, [pc, #44]	; (8007580 <vQueueAddToRegistry+0x4c>)
 8007552:	68fb      	ldr	r3, [r7, #12]
 8007554:	683a      	ldr	r2, [r7, #0]
 8007556:	f841 2033 	str.w	r2, [r1, r3, lsl #3]
 800755a:	4a09      	ldr	r2, [pc, #36]	; (8007580 <vQueueAddToRegistry+0x4c>)
 800755c:	68fb      	ldr	r3, [r7, #12]
 800755e:	00db      	lsls	r3, r3, #3
 8007560:	4413      	add	r3, r2
 8007562:	687a      	ldr	r2, [r7, #4]
 8007564:	605a      	str	r2, [r3, #4]
 8007566:	e005      	b.n	8007574 <vQueueAddToRegistry+0x40>
 8007568:	68fb      	ldr	r3, [r7, #12]
 800756a:	3301      	adds	r3, #1
 800756c:	60fb      	str	r3, [r7, #12]
 800756e:	68fb      	ldr	r3, [r7, #12]
 8007570:	2b07      	cmp	r3, #7
 8007572:	d9e7      	bls.n	8007544 <vQueueAddToRegistry+0x10>
 8007574:	bf00      	nop
 8007576:	3714      	adds	r7, #20
 8007578:	46bd      	mov	sp, r7
 800757a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800757e:	4770      	bx	lr
 8007580:	20006110 	.word	0x20006110

08007584 <vQueueWaitForMessageRestricted>:
 8007584:	b580      	push	{r7, lr}
 8007586:	b086      	sub	sp, #24
 8007588:	af00      	add	r7, sp, #0
 800758a:	60f8      	str	r0, [r7, #12]
 800758c:	60b9      	str	r1, [r7, #8]
 800758e:	607a      	str	r2, [r7, #4]
 8007590:	68fb      	ldr	r3, [r7, #12]
 8007592:	617b      	str	r3, [r7, #20]
 8007594:	f001 fdac 	bl	80090f0 <vPortEnterCritical>
 8007598:	697b      	ldr	r3, [r7, #20]
 800759a:	f893 3044 	ldrb.w	r3, [r3, #68]	; 0x44
 800759e:	b25b      	sxtb	r3, r3
 80075a0:	f1b3 3fff 	cmp.w	r3, #4294967295
 80075a4:	d103      	bne.n	80075ae <vQueueWaitForMessageRestricted+0x2a>
 80075a6:	697b      	ldr	r3, [r7, #20]
 80075a8:	2200      	movs	r2, #0
 80075aa:	f883 2044 	strb.w	r2, [r3, #68]	; 0x44
 80075ae:	697b      	ldr	r3, [r7, #20]
 80075b0:	f893 3045 	ldrb.w	r3, [r3, #69]	; 0x45
 80075b4:	b25b      	sxtb	r3, r3
 80075b6:	f1b3 3fff 	cmp.w	r3, #4294967295
 80075ba:	d103      	bne.n	80075c4 <vQueueWaitForMessageRestricted+0x40>
 80075bc:	697b      	ldr	r3, [r7, #20]
 80075be:	2200      	movs	r2, #0
 80075c0:	f883 2045 	strb.w	r2, [r3, #69]	; 0x45
 80075c4:	f001 fdc2 	bl	800914c <vPortExitCritical>
 80075c8:	697b      	ldr	r3, [r7, #20]
 80075ca:	6b9b      	ldr	r3, [r3, #56]	; 0x38
 80075cc:	2b00      	cmp	r3, #0
 80075ce:	d106      	bne.n	80075de <vQueueWaitForMessageRestricted+0x5a>
 80075d0:	697b      	ldr	r3, [r7, #20]
 80075d2:	3324      	adds	r3, #36	; 0x24
 80075d4:	687a      	ldr	r2, [r7, #4]
 80075d6:	68b9      	ldr	r1, [r7, #8]
 80075d8:	4618      	mov	r0, r3
 80075da:	f000 fd27 	bl	800802c <vTaskPlaceOnEventListRestricted>
 80075de:	6978      	ldr	r0, [r7, #20]
 80075e0:	f7ff ff28 	bl	8007434 <prvUnlockQueue>
 80075e4:	bf00      	nop
 80075e6:	3718      	adds	r7, #24
 80075e8:	46bd      	mov	sp, r7
 80075ea:	bd80      	pop	{r7, pc}

080075ec <vListInitialise>:
 80075ec:	b480      	push	{r7}
 80075ee:	b083      	sub	sp, #12
 80075f0:	af00      	add	r7, sp, #0
 80075f2:	6078      	str	r0, [r7, #4]
 80075f4:	687b      	ldr	r3, [r7, #4]
 80075f6:	f103 0208 	add.w	r2, r3, #8
 80075fa:	687b      	ldr	r3, [r7, #4]
 80075fc:	605a      	str	r2, [r3, #4]
 80075fe:	687b      	ldr	r3, [r7, #4]
 8007600:	f04f 32ff 	mov.w	r2, #4294967295
 8007604:	609a      	str	r2, [r3, #8]
 8007606:	687b      	ldr	r3, [r7, #4]
 8007608:	f103 0208 	add.w	r2, r3, #8
 800760c:	687b      	ldr	r3, [r7, #4]
 800760e:	60da      	str	r2, [r3, #12]
 8007610:	687b      	ldr	r3, [r7, #4]
 8007612:	f103 0208 	add.w	r2, r3, #8
 8007616:	687b      	ldr	r3, [r7, #4]
 8007618:	611a      	str	r2, [r3, #16]
 800761a:	687b      	ldr	r3, [r7, #4]
 800761c:	2200      	movs	r2, #0
 800761e:	601a      	str	r2, [r3, #0]
 8007620:	bf00      	nop
 8007622:	370c      	adds	r7, #12
 8007624:	46bd      	mov	sp, r7
 8007626:	f85d 7b04 	ldr.w	r7, [sp], #4
 800762a:	4770      	bx	lr

0800762c <vListInitialiseItem>:
 800762c:	b480      	push	{r7}
 800762e:	b083      	sub	sp, #12
 8007630:	af00      	add	r7, sp, #0
 8007632:	6078      	str	r0, [r7, #4]
 8007634:	687b      	ldr	r3, [r7, #4]
 8007636:	2200      	movs	r2, #0
 8007638:	611a      	str	r2, [r3, #16]
 800763a:	bf00      	nop
 800763c:	370c      	adds	r7, #12
 800763e:	46bd      	mov	sp, r7
 8007640:	f85d 7b04 	ldr.w	r7, [sp], #4
 8007644:	4770      	bx	lr

08007646 <vListInsertEnd>:
 8007646:	b480      	push	{r7}
 8007648:	b085      	sub	sp, #20
 800764a:	af00      	add	r7, sp, #0
 800764c:	6078      	str	r0, [r7, #4]
 800764e:	6039      	str	r1, [r7, #0]
 8007650:	687b      	ldr	r3, [r7, #4]
 8007652:	685b      	ldr	r3, [r3, #4]
 8007654:	60fb      	str	r3, [r7, #12]
 8007656:	683b      	ldr	r3, [r7, #0]
 8007658:	68fa      	ldr	r2, [r7, #12]
 800765a:	605a      	str	r2, [r3, #4]
 800765c:	68fb      	ldr	r3, [r7, #12]
 800765e:	689a      	ldr	r2, [r3, #8]
 8007660:	683b      	ldr	r3, [r7, #0]
 8007662:	609a      	str	r2, [r3, #8]
 8007664:	68fb      	ldr	r3, [r7, #12]
 8007666:	689b      	ldr	r3, [r3, #8]
 8007668:	683a      	ldr	r2, [r7, #0]
 800766a:	605a      	str	r2, [r3, #4]
 800766c:	68fb      	ldr	r3, [r7, #12]
 800766e:	683a      	ldr	r2, [r7, #0]
 8007670:	609a      	str	r2, [r3, #8]
 8007672:	683b      	ldr	r3, [r7, #0]
 8007674:	687a      	ldr	r2, [r7, #4]
 8007676:	611a      	str	r2, [r3, #16]
 8007678:	687b      	ldr	r3, [r7, #4]
 800767a:	681b      	ldr	r3, [r3, #0]
 800767c:	1c5a      	adds	r2, r3, #1
 800767e:	687b      	ldr	r3, [r7, #4]
 8007680:	601a      	str	r2, [r3, #0]
 8007682:	bf00      	nop
 8007684:	3714      	adds	r7, #20
 8007686:	46bd      	mov	sp, r7
 8007688:	f85d 7b04 	ldr.w	r7, [sp], #4
 800768c:	4770      	bx	lr

0800768e <vListInsert>:
 800768e:	b480      	push	{r7}
 8007690:	b085      	sub	sp, #20
 8007692:	af00      	add	r7, sp, #0
 8007694:	6078      	str	r0, [r7, #4]
 8007696:	6039      	str	r1, [r7, #0]
 8007698:	683b      	ldr	r3, [r7, #0]
 800769a:	681b      	ldr	r3, [r3, #0]
 800769c:	60bb      	str	r3, [r7, #8]
 800769e:	68bb      	ldr	r3, [r7, #8]
 80076a0:	f1b3 3fff 	cmp.w	r3, #4294967295
 80076a4:	d103      	bne.n	80076ae <vListInsert+0x20>
 80076a6:	687b      	ldr	r3, [r7, #4]
 80076a8:	691b      	ldr	r3, [r3, #16]
 80076aa:	60fb      	str	r3, [r7, #12]
 80076ac:	e00c      	b.n	80076c8 <vListInsert+0x3a>
 80076ae:	687b      	ldr	r3, [r7, #4]
 80076b0:	3308      	adds	r3, #8
 80076b2:	60fb      	str	r3, [r7, #12]
 80076b4:	e002      	b.n	80076bc <vListInsert+0x2e>
 80076b6:	68fb      	ldr	r3, [r7, #12]
 80076b8:	685b      	ldr	r3, [r3, #4]
 80076ba:	60fb      	str	r3, [r7, #12]
 80076bc:	68fb      	ldr	r3, [r7, #12]
 80076be:	685b      	ldr	r3, [r3, #4]
 80076c0:	681a      	ldr	r2, [r3, #0]
 80076c2:	68bb      	ldr	r3, [r7, #8]
 80076c4:	429a      	cmp	r2, r3
 80076c6:	d9f6      	bls.n	80076b6 <vListInsert+0x28>
 80076c8:	68fb      	ldr	r3, [r7, #12]
 80076ca:	685a      	ldr	r2, [r3, #4]
 80076cc:	683b      	ldr	r3, [r7, #0]
 80076ce:	605a      	str	r2, [r3, #4]
 80076d0:	683b      	ldr	r3, [r7, #0]
 80076d2:	685b      	ldr	r3, [r3, #4]
 80076d4:	683a      	ldr	r2, [r7, #0]
 80076d6:	609a      	str	r2, [r3, #8]
 80076d8:	683b      	ldr	r3, [r7, #0]
 80076da:	68fa      	ldr	r2, [r7, #12]
 80076dc:	609a      	str	r2, [r3, #8]
 80076de:	68fb      	ldr	r3, [r7, #12]
 80076e0:	683a      	ldr	r2, [r7, #0]
 80076e2:	605a      	str	r2, [r3, #4]
 80076e4:	683b      	ldr	r3, [r7, #0]
 80076e6:	687a      	ldr	r2, [r7, #4]
 80076e8:	611a      	str	r2, [r3, #16]
 80076ea:	687b      	ldr	r3, [r7, #4]
 80076ec:	681b      	ldr	r3, [r3, #0]
 80076ee:	1c5a      	adds	r2, r3, #1
 80076f0:	687b      	ldr	r3, [r7, #4]
 80076f2:	601a      	str	r2, [r3, #0]
 80076f4:	bf00      	nop
 80076f6:	3714      	adds	r7, #20
 80076f8:	46bd      	mov	sp, r7
 80076fa:	f85d 7b04 	ldr.w	r7, [sp], #4
 80076fe:	4770      	bx	lr

08007700 <uxListRemove>:
 8007700:	b480      	push	{r7}
 8007702:	b085      	sub	sp, #20
 8007704:	af00      	add	r7, sp, #0
 8007706:	6078      	str	r0, [r7, #4]
 8007708:	687b      	ldr	r3, [r7, #4]
 800770a:	691b      	ldr	r3, [r3, #16]
 800770c:	60fb      	str	r3, [r7, #12]
 800770e:	687b      	ldr	r3, [r7, #4]
 8007710:	685b      	ldr	r3, [r3, #4]
 8007712:	687a      	ldr	r2, [r7, #4]
 8007714:	6892      	ldr	r2, [r2, #8]
 8007716:	609a      	str	r2, [r3, #8]
 8007718:	687b      	ldr	r3, [r7, #4]
 800771a:	689b      	ldr	r3, [r3, #8]
 800771c:	687a      	ldr	r2, [r7, #4]
 800771e:	6852      	ldr	r2, [r2, #4]
 8007720:	605a      	str	r2, [r3, #4]
 8007722:	68fb      	ldr	r3, [r7, #12]
 8007724:	685a      	ldr	r2, [r3, #4]
 8007726:	687b      	ldr	r3, [r7, #4]
 8007728:	429a      	cmp	r2, r3
 800772a:	d103      	bne.n	8007734 <uxListRemove+0x34>
 800772c:	687b      	ldr	r3, [r7, #4]
 800772e:	689a      	ldr	r2, [r3, #8]
 8007730:	68fb      	ldr	r3, [r7, #12]
 8007732:	605a      	str	r2, [r3, #4]
 8007734:	687b      	ldr	r3, [r7, #4]
 8007736:	2200      	movs	r2, #0
 8007738:	611a      	str	r2, [r3, #16]
 800773a:	68fb      	ldr	r3, [r7, #12]
 800773c:	681b      	ldr	r3, [r3, #0]
 800773e:	1e5a      	subs	r2, r3, #1
 8007740:	68fb      	ldr	r3, [r7, #12]
 8007742:	601a      	str	r2, [r3, #0]
 8007744:	68fb      	ldr	r3, [r7, #12]
 8007746:	681b      	ldr	r3, [r3, #0]
 8007748:	4618      	mov	r0, r3
 800774a:	3714      	adds	r7, #20
 800774c:	46bd      	mov	sp, r7
 800774e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8007752:	4770      	bx	lr

08007754 <xTaskCreateStatic>:
 8007754:	b580      	push	{r7, lr}
 8007756:	b08e      	sub	sp, #56	; 0x38
 8007758:	af04      	add	r7, sp, #16
 800775a:	60f8      	str	r0, [r7, #12]
 800775c:	60b9      	str	r1, [r7, #8]
 800775e:	607a      	str	r2, [r7, #4]
 8007760:	603b      	str	r3, [r7, #0]
 8007762:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8007764:	2b00      	cmp	r3, #0
 8007766:	d109      	bne.n	800777c <xTaskCreateStatic+0x28>
 8007768:	f04f 0350 	mov.w	r3, #80	; 0x50
 800776c:	f383 8811 	msr	BASEPRI, r3
 8007770:	f3bf 8f6f 	isb	sy
 8007774:	f3bf 8f4f 	dsb	sy
 8007778:	623b      	str	r3, [r7, #32]
 800777a:	e7fe      	b.n	800777a <xTaskCreateStatic+0x26>
 800777c:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800777e:	2b00      	cmp	r3, #0
 8007780:	d109      	bne.n	8007796 <xTaskCreateStatic+0x42>
 8007782:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007786:	f383 8811 	msr	BASEPRI, r3
 800778a:	f3bf 8f6f 	isb	sy
 800778e:	f3bf 8f4f 	dsb	sy
 8007792:	61fb      	str	r3, [r7, #28]
 8007794:	e7fe      	b.n	8007794 <xTaskCreateStatic+0x40>
 8007796:	f44f 6390 	mov.w	r3, #1152	; 0x480
 800779a:	613b      	str	r3, [r7, #16]
 800779c:	693b      	ldr	r3, [r7, #16]
 800779e:	f5b3 6f90 	cmp.w	r3, #1152	; 0x480
 80077a2:	d009      	beq.n	80077b8 <xTaskCreateStatic+0x64>
 80077a4:	f04f 0350 	mov.w	r3, #80	; 0x50
 80077a8:	f383 8811 	msr	BASEPRI, r3
 80077ac:	f3bf 8f6f 	isb	sy
 80077b0:	f3bf 8f4f 	dsb	sy
 80077b4:	61bb      	str	r3, [r7, #24]
 80077b6:	e7fe      	b.n	80077b6 <xTaskCreateStatic+0x62>
 80077b8:	693b      	ldr	r3, [r7, #16]
 80077ba:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80077bc:	2b00      	cmp	r3, #0
 80077be:	d01e      	beq.n	80077fe <xTaskCreateStatic+0xaa>
 80077c0:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80077c2:	2b00      	cmp	r3, #0
 80077c4:	d01b      	beq.n	80077fe <xTaskCreateStatic+0xaa>
 80077c6:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80077c8:	627b      	str	r3, [r7, #36]	; 0x24
 80077ca:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80077cc:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 80077ce:	631a      	str	r2, [r3, #48]	; 0x30
 80077d0:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80077d2:	2202      	movs	r2, #2
 80077d4:	f883 247d 	strb.w	r2, [r3, #1149]	; 0x47d
 80077d8:	2300      	movs	r3, #0
 80077da:	9303      	str	r3, [sp, #12]
 80077dc:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 80077de:	9302      	str	r3, [sp, #8]
 80077e0:	f107 0314 	add.w	r3, r7, #20
 80077e4:	9301      	str	r3, [sp, #4]
 80077e6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80077e8:	9300      	str	r3, [sp, #0]
 80077ea:	683b      	ldr	r3, [r7, #0]
 80077ec:	687a      	ldr	r2, [r7, #4]
 80077ee:	68b9      	ldr	r1, [r7, #8]
 80077f0:	68f8      	ldr	r0, [r7, #12]
 80077f2:	f000 f80b 	bl	800780c <prvInitialiseNewTask>
 80077f6:	6a78      	ldr	r0, [r7, #36]	; 0x24
 80077f8:	f000 f8e4 	bl	80079c4 <prvAddNewTaskToReadyList>
 80077fc:	e001      	b.n	8007802 <xTaskCreateStatic+0xae>
 80077fe:	2300      	movs	r3, #0
 8007800:	617b      	str	r3, [r7, #20]
 8007802:	697b      	ldr	r3, [r7, #20]
 8007804:	4618      	mov	r0, r3
 8007806:	3728      	adds	r7, #40	; 0x28
 8007808:	46bd      	mov	sp, r7
 800780a:	bd80      	pop	{r7, pc}

0800780c <prvInitialiseNewTask>:
 800780c:	b590      	push	{r4, r7, lr}
 800780e:	b089      	sub	sp, #36	; 0x24
 8007810:	af00      	add	r7, sp, #0
 8007812:	60f8      	str	r0, [r7, #12]
 8007814:	60b9      	str	r1, [r7, #8]
 8007816:	607a      	str	r2, [r7, #4]
 8007818:	603b      	str	r3, [r7, #0]
 800781a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800781c:	6b18      	ldr	r0, [r3, #48]	; 0x30
 800781e:	687b      	ldr	r3, [r7, #4]
 8007820:	009b      	lsls	r3, r3, #2
 8007822:	461a      	mov	r2, r3
 8007824:	21a5      	movs	r1, #165	; 0xa5
 8007826:	f7f9 faa9 	bl	8000d7c <memset>
 800782a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800782c:	6b1a      	ldr	r2, [r3, #48]	; 0x30
 800782e:	687b      	ldr	r3, [r7, #4]
 8007830:	f103 4380 	add.w	r3, r3, #1073741824	; 0x40000000
 8007834:	3b01      	subs	r3, #1
 8007836:	009b      	lsls	r3, r3, #2
 8007838:	4413      	add	r3, r2
 800783a:	61bb      	str	r3, [r7, #24]
 800783c:	69bb      	ldr	r3, [r7, #24]
 800783e:	f023 0307 	bic.w	r3, r3, #7
 8007842:	61bb      	str	r3, [r7, #24]
 8007844:	69bb      	ldr	r3, [r7, #24]
 8007846:	f003 0307 	and.w	r3, r3, #7
 800784a:	2b00      	cmp	r3, #0
 800784c:	d009      	beq.n	8007862 <prvInitialiseNewTask+0x56>
 800784e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007852:	f383 8811 	msr	BASEPRI, r3
 8007856:	f3bf 8f6f 	isb	sy
 800785a:	f3bf 8f4f 	dsb	sy
 800785e:	617b      	str	r3, [r7, #20]
 8007860:	e7fe      	b.n	8007860 <prvInitialiseNewTask+0x54>
 8007862:	68bb      	ldr	r3, [r7, #8]
 8007864:	2b00      	cmp	r3, #0
 8007866:	d01f      	beq.n	80078a8 <prvInitialiseNewTask+0x9c>
 8007868:	2300      	movs	r3, #0
 800786a:	61fb      	str	r3, [r7, #28]
 800786c:	e012      	b.n	8007894 <prvInitialiseNewTask+0x88>
 800786e:	68ba      	ldr	r2, [r7, #8]
 8007870:	69fb      	ldr	r3, [r7, #28]
 8007872:	4413      	add	r3, r2
 8007874:	7819      	ldrb	r1, [r3, #0]
 8007876:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8007878:	69fb      	ldr	r3, [r7, #28]
 800787a:	4413      	add	r3, r2
 800787c:	3334      	adds	r3, #52	; 0x34
 800787e:	460a      	mov	r2, r1
 8007880:	701a      	strb	r2, [r3, #0]
 8007882:	68ba      	ldr	r2, [r7, #8]
 8007884:	69fb      	ldr	r3, [r7, #28]
 8007886:	4413      	add	r3, r2
 8007888:	781b      	ldrb	r3, [r3, #0]
 800788a:	2b00      	cmp	r3, #0
 800788c:	d006      	beq.n	800789c <prvInitialiseNewTask+0x90>
 800788e:	69fb      	ldr	r3, [r7, #28]
 8007890:	3301      	adds	r3, #1
 8007892:	61fb      	str	r3, [r7, #28]
 8007894:	69fb      	ldr	r3, [r7, #28]
 8007896:	2b09      	cmp	r3, #9
 8007898:	d9e9      	bls.n	800786e <prvInitialiseNewTask+0x62>
 800789a:	e000      	b.n	800789e <prvInitialiseNewTask+0x92>
 800789c:	bf00      	nop
 800789e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078a0:	2200      	movs	r2, #0
 80078a2:	f883 203d 	strb.w	r2, [r3, #61]	; 0x3d
 80078a6:	e003      	b.n	80078b0 <prvInitialiseNewTask+0xa4>
 80078a8:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078aa:	2200      	movs	r2, #0
 80078ac:	f883 2034 	strb.w	r2, [r3, #52]	; 0x34
 80078b0:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80078b2:	2b04      	cmp	r3, #4
 80078b4:	d901      	bls.n	80078ba <prvInitialiseNewTask+0xae>
 80078b6:	2304      	movs	r3, #4
 80078b8:	633b      	str	r3, [r7, #48]	; 0x30
 80078ba:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078bc:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 80078be:	62da      	str	r2, [r3, #44]	; 0x2c
 80078c0:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078c2:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 80078c4:	649a      	str	r2, [r3, #72]	; 0x48
 80078c6:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078c8:	2200      	movs	r2, #0
 80078ca:	64da      	str	r2, [r3, #76]	; 0x4c
 80078cc:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078ce:	3304      	adds	r3, #4
 80078d0:	4618      	mov	r0, r3
 80078d2:	f7ff feab 	bl	800762c <vListInitialiseItem>
 80078d6:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078d8:	3318      	adds	r3, #24
 80078da:	4618      	mov	r0, r3
 80078dc:	f7ff fea6 	bl	800762c <vListInitialiseItem>
 80078e0:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078e2:	6bba      	ldr	r2, [r7, #56]	; 0x38
 80078e4:	611a      	str	r2, [r3, #16]
 80078e6:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 80078e8:	f1c3 0205 	rsb	r2, r3, #5
 80078ec:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078ee:	619a      	str	r2, [r3, #24]
 80078f0:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078f2:	6bba      	ldr	r2, [r7, #56]	; 0x38
 80078f4:	625a      	str	r2, [r3, #36]	; 0x24
 80078f6:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80078f8:	f503 638f 	add.w	r3, r3, #1144	; 0x478
 80078fc:	2204      	movs	r2, #4
 80078fe:	2100      	movs	r1, #0
 8007900:	4618      	mov	r0, r3
 8007902:	f7f9 fa3b 	bl	8000d7c <memset>
 8007906:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007908:	f203 437c 	addw	r3, r3, #1148	; 0x47c
 800790c:	2201      	movs	r2, #1
 800790e:	2100      	movs	r1, #0
 8007910:	4618      	mov	r0, r3
 8007912:	f7f9 fa33 	bl	8000d7c <memset>
 8007916:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007918:	3350      	adds	r3, #80	; 0x50
 800791a:	f44f 6285 	mov.w	r2, #1064	; 0x428
 800791e:	2100      	movs	r1, #0
 8007920:	4618      	mov	r0, r3
 8007922:	f7f9 fa2b 	bl	8000d7c <memset>
 8007926:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007928:	f503 724f 	add.w	r2, r3, #828	; 0x33c
 800792c:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800792e:	655a      	str	r2, [r3, #84]	; 0x54
 8007930:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007932:	f503 7269 	add.w	r2, r3, #932	; 0x3a4
 8007936:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007938:	659a      	str	r2, [r3, #88]	; 0x58
 800793a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800793c:	f203 420c 	addw	r2, r3, #1036	; 0x40c
 8007940:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007942:	65da      	str	r2, [r3, #92]	; 0x5c
 8007944:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007946:	4a1e      	ldr	r2, [pc, #120]	; (80079c0 <prvInitialiseNewTask+0x1b4>)
 8007948:	f8c3 2084 	str.w	r2, [r3, #132]	; 0x84
 800794c:	6bba      	ldr	r2, [r7, #56]	; 0x38
 800794e:	f04f 0301 	mov.w	r3, #1
 8007952:	f04f 0400 	mov.w	r4, #0
 8007956:	e9c2 343e 	strd	r3, r4, [r2, #248]	; 0xf8
 800795a:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800795c:	f243 320e 	movw	r2, #13070	; 0x330e
 8007960:	f8a3 2100 	strh.w	r2, [r3, #256]	; 0x100
 8007964:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007966:	f64a 32cd 	movw	r2, #43981	; 0xabcd
 800796a:	f8a3 2102 	strh.w	r2, [r3, #258]	; 0x102
 800796e:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007970:	f241 2234 	movw	r2, #4660	; 0x1234
 8007974:	f8a3 2104 	strh.w	r2, [r3, #260]	; 0x104
 8007978:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800797a:	f24e 626d 	movw	r2, #58989	; 0xe66d
 800797e:	f8a3 2106 	strh.w	r2, [r3, #262]	; 0x106
 8007982:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007984:	f64d 62ec 	movw	r2, #57068	; 0xdeec
 8007988:	f8a3 2108 	strh.w	r2, [r3, #264]	; 0x108
 800798c:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800798e:	2205      	movs	r2, #5
 8007990:	f8a3 210a 	strh.w	r2, [r3, #266]	; 0x10a
 8007994:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8007996:	220b      	movs	r2, #11
 8007998:	f8a3 210c 	strh.w	r2, [r3, #268]	; 0x10c
 800799c:	683a      	ldr	r2, [r7, #0]
 800799e:	68f9      	ldr	r1, [r7, #12]
 80079a0:	69b8      	ldr	r0, [r7, #24]
 80079a2:	f001 fa7d 	bl	8008ea0 <pxPortInitialiseStack>
 80079a6:	4602      	mov	r2, r0
 80079a8:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80079aa:	601a      	str	r2, [r3, #0]
 80079ac:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80079ae:	2b00      	cmp	r3, #0
 80079b0:	d002      	beq.n	80079b8 <prvInitialiseNewTask+0x1ac>
 80079b2:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80079b4:	6bba      	ldr	r2, [r7, #56]	; 0x38
 80079b6:	601a      	str	r2, [r3, #0]
 80079b8:	bf00      	nop
 80079ba:	3724      	adds	r7, #36	; 0x24
 80079bc:	46bd      	mov	sp, r7
 80079be:	bd90      	pop	{r4, r7, pc}
 80079c0:	080098bc 	.word	0x080098bc

080079c4 <prvAddNewTaskToReadyList>:
 80079c4:	b580      	push	{r7, lr}
 80079c6:	b082      	sub	sp, #8
 80079c8:	af00      	add	r7, sp, #0
 80079ca:	6078      	str	r0, [r7, #4]
 80079cc:	f001 fb90 	bl	80090f0 <vPortEnterCritical>
 80079d0:	4b2c      	ldr	r3, [pc, #176]	; (8007a84 <prvAddNewTaskToReadyList+0xc0>)
 80079d2:	681b      	ldr	r3, [r3, #0]
 80079d4:	3301      	adds	r3, #1
 80079d6:	4a2b      	ldr	r2, [pc, #172]	; (8007a84 <prvAddNewTaskToReadyList+0xc0>)
 80079d8:	6013      	str	r3, [r2, #0]
 80079da:	4b2b      	ldr	r3, [pc, #172]	; (8007a88 <prvAddNewTaskToReadyList+0xc4>)
 80079dc:	681b      	ldr	r3, [r3, #0]
 80079de:	2b00      	cmp	r3, #0
 80079e0:	d109      	bne.n	80079f6 <prvAddNewTaskToReadyList+0x32>
 80079e2:	4a29      	ldr	r2, [pc, #164]	; (8007a88 <prvAddNewTaskToReadyList+0xc4>)
 80079e4:	687b      	ldr	r3, [r7, #4]
 80079e6:	6013      	str	r3, [r2, #0]
 80079e8:	4b26      	ldr	r3, [pc, #152]	; (8007a84 <prvAddNewTaskToReadyList+0xc0>)
 80079ea:	681b      	ldr	r3, [r3, #0]
 80079ec:	2b01      	cmp	r3, #1
 80079ee:	d110      	bne.n	8007a12 <prvAddNewTaskToReadyList+0x4e>
 80079f0:	f000 fc48 	bl	8008284 <prvInitialiseTaskLists>
 80079f4:	e00d      	b.n	8007a12 <prvAddNewTaskToReadyList+0x4e>
 80079f6:	4b25      	ldr	r3, [pc, #148]	; (8007a8c <prvAddNewTaskToReadyList+0xc8>)
 80079f8:	681b      	ldr	r3, [r3, #0]
 80079fa:	2b00      	cmp	r3, #0
 80079fc:	d109      	bne.n	8007a12 <prvAddNewTaskToReadyList+0x4e>
 80079fe:	4b22      	ldr	r3, [pc, #136]	; (8007a88 <prvAddNewTaskToReadyList+0xc4>)
 8007a00:	681b      	ldr	r3, [r3, #0]
 8007a02:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007a04:	687b      	ldr	r3, [r7, #4]
 8007a06:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007a08:	429a      	cmp	r2, r3
 8007a0a:	d802      	bhi.n	8007a12 <prvAddNewTaskToReadyList+0x4e>
 8007a0c:	4a1e      	ldr	r2, [pc, #120]	; (8007a88 <prvAddNewTaskToReadyList+0xc4>)
 8007a0e:	687b      	ldr	r3, [r7, #4]
 8007a10:	6013      	str	r3, [r2, #0]
 8007a12:	4b1f      	ldr	r3, [pc, #124]	; (8007a90 <prvAddNewTaskToReadyList+0xcc>)
 8007a14:	681b      	ldr	r3, [r3, #0]
 8007a16:	3301      	adds	r3, #1
 8007a18:	4a1d      	ldr	r2, [pc, #116]	; (8007a90 <prvAddNewTaskToReadyList+0xcc>)
 8007a1a:	6013      	str	r3, [r2, #0]
 8007a1c:	4b1c      	ldr	r3, [pc, #112]	; (8007a90 <prvAddNewTaskToReadyList+0xcc>)
 8007a1e:	681a      	ldr	r2, [r3, #0]
 8007a20:	687b      	ldr	r3, [r7, #4]
 8007a22:	641a      	str	r2, [r3, #64]	; 0x40
 8007a24:	687b      	ldr	r3, [r7, #4]
 8007a26:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007a28:	2201      	movs	r2, #1
 8007a2a:	409a      	lsls	r2, r3
 8007a2c:	4b19      	ldr	r3, [pc, #100]	; (8007a94 <prvAddNewTaskToReadyList+0xd0>)
 8007a2e:	681b      	ldr	r3, [r3, #0]
 8007a30:	4313      	orrs	r3, r2
 8007a32:	4a18      	ldr	r2, [pc, #96]	; (8007a94 <prvAddNewTaskToReadyList+0xd0>)
 8007a34:	6013      	str	r3, [r2, #0]
 8007a36:	687b      	ldr	r3, [r7, #4]
 8007a38:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007a3a:	4613      	mov	r3, r2
 8007a3c:	009b      	lsls	r3, r3, #2
 8007a3e:	4413      	add	r3, r2
 8007a40:	009b      	lsls	r3, r3, #2
 8007a42:	4a15      	ldr	r2, [pc, #84]	; (8007a98 <prvAddNewTaskToReadyList+0xd4>)
 8007a44:	441a      	add	r2, r3
 8007a46:	687b      	ldr	r3, [r7, #4]
 8007a48:	3304      	adds	r3, #4
 8007a4a:	4619      	mov	r1, r3
 8007a4c:	4610      	mov	r0, r2
 8007a4e:	f7ff fdfa 	bl	8007646 <vListInsertEnd>
 8007a52:	f001 fb7b 	bl	800914c <vPortExitCritical>
 8007a56:	4b0d      	ldr	r3, [pc, #52]	; (8007a8c <prvAddNewTaskToReadyList+0xc8>)
 8007a58:	681b      	ldr	r3, [r3, #0]
 8007a5a:	2b00      	cmp	r3, #0
 8007a5c:	d00e      	beq.n	8007a7c <prvAddNewTaskToReadyList+0xb8>
 8007a5e:	4b0a      	ldr	r3, [pc, #40]	; (8007a88 <prvAddNewTaskToReadyList+0xc4>)
 8007a60:	681b      	ldr	r3, [r3, #0]
 8007a62:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007a64:	687b      	ldr	r3, [r7, #4]
 8007a66:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007a68:	429a      	cmp	r2, r3
 8007a6a:	d207      	bcs.n	8007a7c <prvAddNewTaskToReadyList+0xb8>
 8007a6c:	4b0b      	ldr	r3, [pc, #44]	; (8007a9c <prvAddNewTaskToReadyList+0xd8>)
 8007a6e:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8007a72:	601a      	str	r2, [r3, #0]
 8007a74:	f3bf 8f4f 	dsb	sy
 8007a78:	f3bf 8f6f 	isb	sy
 8007a7c:	bf00      	nop
 8007a7e:	3708      	adds	r7, #8
 8007a80:	46bd      	mov	sp, r7
 8007a82:	bd80      	pop	{r7, pc}
 8007a84:	20001324 	.word	0x20001324
 8007a88:	2000124c 	.word	0x2000124c
 8007a8c:	20001330 	.word	0x20001330
 8007a90:	20001340 	.word	0x20001340
 8007a94:	2000132c 	.word	0x2000132c
 8007a98:	20001250 	.word	0x20001250
 8007a9c:	e000ed04 	.word	0xe000ed04

08007aa0 <vTaskDelay>:
 8007aa0:	b580      	push	{r7, lr}
 8007aa2:	b084      	sub	sp, #16
 8007aa4:	af00      	add	r7, sp, #0
 8007aa6:	6078      	str	r0, [r7, #4]
 8007aa8:	2300      	movs	r3, #0
 8007aaa:	60fb      	str	r3, [r7, #12]
 8007aac:	687b      	ldr	r3, [r7, #4]
 8007aae:	2b00      	cmp	r3, #0
 8007ab0:	d016      	beq.n	8007ae0 <vTaskDelay+0x40>
 8007ab2:	4b13      	ldr	r3, [pc, #76]	; (8007b00 <vTaskDelay+0x60>)
 8007ab4:	681b      	ldr	r3, [r3, #0]
 8007ab6:	2b00      	cmp	r3, #0
 8007ab8:	d009      	beq.n	8007ace <vTaskDelay+0x2e>
 8007aba:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007abe:	f383 8811 	msr	BASEPRI, r3
 8007ac2:	f3bf 8f6f 	isb	sy
 8007ac6:	f3bf 8f4f 	dsb	sy
 8007aca:	60bb      	str	r3, [r7, #8]
 8007acc:	e7fe      	b.n	8007acc <vTaskDelay+0x2c>
 8007ace:	f000 f889 	bl	8007be4 <vTaskSuspendAll>
 8007ad2:	2100      	movs	r1, #0
 8007ad4:	6878      	ldr	r0, [r7, #4]
 8007ad6:	f000 fe4f 	bl	8008778 <prvAddCurrentTaskToDelayedList>
 8007ada:	f000 f891 	bl	8007c00 <xTaskResumeAll>
 8007ade:	60f8      	str	r0, [r7, #12]
 8007ae0:	68fb      	ldr	r3, [r7, #12]
 8007ae2:	2b00      	cmp	r3, #0
 8007ae4:	d107      	bne.n	8007af6 <vTaskDelay+0x56>
 8007ae6:	4b07      	ldr	r3, [pc, #28]	; (8007b04 <vTaskDelay+0x64>)
 8007ae8:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8007aec:	601a      	str	r2, [r3, #0]
 8007aee:	f3bf 8f4f 	dsb	sy
 8007af2:	f3bf 8f6f 	isb	sy
 8007af6:	bf00      	nop
 8007af8:	3710      	adds	r7, #16
 8007afa:	46bd      	mov	sp, r7
 8007afc:	bd80      	pop	{r7, pc}
 8007afe:	bf00      	nop
 8007b00:	2000134c 	.word	0x2000134c
 8007b04:	e000ed04 	.word	0xe000ed04

08007b08 <vTaskStartScheduler>:
 8007b08:	b580      	push	{r7, lr}
 8007b0a:	b08a      	sub	sp, #40	; 0x28
 8007b0c:	af04      	add	r7, sp, #16
 8007b0e:	2300      	movs	r3, #0
 8007b10:	60bb      	str	r3, [r7, #8]
 8007b12:	2300      	movs	r3, #0
 8007b14:	607b      	str	r3, [r7, #4]
 8007b16:	463a      	mov	r2, r7
 8007b18:	1d39      	adds	r1, r7, #4
 8007b1a:	f107 0308 	add.w	r3, r7, #8
 8007b1e:	4618      	mov	r0, r3
 8007b20:	f7fc f9ea 	bl	8003ef8 <vApplicationGetIdleTaskMemory>
 8007b24:	6839      	ldr	r1, [r7, #0]
 8007b26:	687b      	ldr	r3, [r7, #4]
 8007b28:	68ba      	ldr	r2, [r7, #8]
 8007b2a:	9202      	str	r2, [sp, #8]
 8007b2c:	9301      	str	r3, [sp, #4]
 8007b2e:	2300      	movs	r3, #0
 8007b30:	9300      	str	r3, [sp, #0]
 8007b32:	2300      	movs	r3, #0
 8007b34:	460a      	mov	r2, r1
 8007b36:	4923      	ldr	r1, [pc, #140]	; (8007bc4 <vTaskStartScheduler+0xbc>)
 8007b38:	4823      	ldr	r0, [pc, #140]	; (8007bc8 <vTaskStartScheduler+0xc0>)
 8007b3a:	f7ff fe0b 	bl	8007754 <xTaskCreateStatic>
 8007b3e:	4602      	mov	r2, r0
 8007b40:	4b22      	ldr	r3, [pc, #136]	; (8007bcc <vTaskStartScheduler+0xc4>)
 8007b42:	601a      	str	r2, [r3, #0]
 8007b44:	4b21      	ldr	r3, [pc, #132]	; (8007bcc <vTaskStartScheduler+0xc4>)
 8007b46:	681b      	ldr	r3, [r3, #0]
 8007b48:	2b00      	cmp	r3, #0
 8007b4a:	d002      	beq.n	8007b52 <vTaskStartScheduler+0x4a>
 8007b4c:	2301      	movs	r3, #1
 8007b4e:	617b      	str	r3, [r7, #20]
 8007b50:	e001      	b.n	8007b56 <vTaskStartScheduler+0x4e>
 8007b52:	2300      	movs	r3, #0
 8007b54:	617b      	str	r3, [r7, #20]
 8007b56:	697b      	ldr	r3, [r7, #20]
 8007b58:	2b01      	cmp	r3, #1
 8007b5a:	d102      	bne.n	8007b62 <vTaskStartScheduler+0x5a>
 8007b5c:	f000 fe72 	bl	8008844 <xTimerCreateTimerTask>
 8007b60:	6178      	str	r0, [r7, #20]
 8007b62:	697b      	ldr	r3, [r7, #20]
 8007b64:	2b01      	cmp	r3, #1
 8007b66:	d11a      	bne.n	8007b9e <vTaskStartScheduler+0x96>
 8007b68:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007b6c:	f383 8811 	msr	BASEPRI, r3
 8007b70:	f3bf 8f6f 	isb	sy
 8007b74:	f3bf 8f4f 	dsb	sy
 8007b78:	613b      	str	r3, [r7, #16]
 8007b7a:	4b15      	ldr	r3, [pc, #84]	; (8007bd0 <vTaskStartScheduler+0xc8>)
 8007b7c:	681b      	ldr	r3, [r3, #0]
 8007b7e:	3350      	adds	r3, #80	; 0x50
 8007b80:	4a14      	ldr	r2, [pc, #80]	; (8007bd4 <vTaskStartScheduler+0xcc>)
 8007b82:	6013      	str	r3, [r2, #0]
 8007b84:	4b14      	ldr	r3, [pc, #80]	; (8007bd8 <vTaskStartScheduler+0xd0>)
 8007b86:	f04f 32ff 	mov.w	r2, #4294967295
 8007b8a:	601a      	str	r2, [r3, #0]
 8007b8c:	4b13      	ldr	r3, [pc, #76]	; (8007bdc <vTaskStartScheduler+0xd4>)
 8007b8e:	2201      	movs	r2, #1
 8007b90:	601a      	str	r2, [r3, #0]
 8007b92:	4b13      	ldr	r3, [pc, #76]	; (8007be0 <vTaskStartScheduler+0xd8>)
 8007b94:	2200      	movs	r2, #0
 8007b96:	601a      	str	r2, [r3, #0]
 8007b98:	f001 fa0c 	bl	8008fb4 <xPortStartScheduler>
 8007b9c:	e00d      	b.n	8007bba <vTaskStartScheduler+0xb2>
 8007b9e:	697b      	ldr	r3, [r7, #20]
 8007ba0:	f1b3 3fff 	cmp.w	r3, #4294967295
 8007ba4:	d109      	bne.n	8007bba <vTaskStartScheduler+0xb2>
 8007ba6:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007baa:	f383 8811 	msr	BASEPRI, r3
 8007bae:	f3bf 8f6f 	isb	sy
 8007bb2:	f3bf 8f4f 	dsb	sy
 8007bb6:	60fb      	str	r3, [r7, #12]
 8007bb8:	e7fe      	b.n	8007bb8 <vTaskStartScheduler+0xb0>
 8007bba:	bf00      	nop
 8007bbc:	3718      	adds	r7, #24
 8007bbe:	46bd      	mov	sp, r7
 8007bc0:	bd80      	pop	{r7, pc}
 8007bc2:	bf00      	nop
 8007bc4:	080098c0 	.word	0x080098c0
 8007bc8:	08008251 	.word	0x08008251
 8007bcc:	20001348 	.word	0x20001348
 8007bd0:	2000124c 	.word	0x2000124c
 8007bd4:	20000000 	.word	0x20000000
 8007bd8:	20001344 	.word	0x20001344
 8007bdc:	20001330 	.word	0x20001330
 8007be0:	20001328 	.word	0x20001328

08007be4 <vTaskSuspendAll>:
 8007be4:	b480      	push	{r7}
 8007be6:	af00      	add	r7, sp, #0
 8007be8:	4b04      	ldr	r3, [pc, #16]	; (8007bfc <vTaskSuspendAll+0x18>)
 8007bea:	681b      	ldr	r3, [r3, #0]
 8007bec:	3301      	adds	r3, #1
 8007bee:	4a03      	ldr	r2, [pc, #12]	; (8007bfc <vTaskSuspendAll+0x18>)
 8007bf0:	6013      	str	r3, [r2, #0]
 8007bf2:	bf00      	nop
 8007bf4:	46bd      	mov	sp, r7
 8007bf6:	f85d 7b04 	ldr.w	r7, [sp], #4
 8007bfa:	4770      	bx	lr
 8007bfc:	2000134c 	.word	0x2000134c

08007c00 <xTaskResumeAll>:
 8007c00:	b580      	push	{r7, lr}
 8007c02:	b084      	sub	sp, #16
 8007c04:	af00      	add	r7, sp, #0
 8007c06:	2300      	movs	r3, #0
 8007c08:	60fb      	str	r3, [r7, #12]
 8007c0a:	2300      	movs	r3, #0
 8007c0c:	60bb      	str	r3, [r7, #8]
 8007c0e:	4b41      	ldr	r3, [pc, #260]	; (8007d14 <xTaskResumeAll+0x114>)
 8007c10:	681b      	ldr	r3, [r3, #0]
 8007c12:	2b00      	cmp	r3, #0
 8007c14:	d109      	bne.n	8007c2a <xTaskResumeAll+0x2a>
 8007c16:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007c1a:	f383 8811 	msr	BASEPRI, r3
 8007c1e:	f3bf 8f6f 	isb	sy
 8007c22:	f3bf 8f4f 	dsb	sy
 8007c26:	603b      	str	r3, [r7, #0]
 8007c28:	e7fe      	b.n	8007c28 <xTaskResumeAll+0x28>
 8007c2a:	f001 fa61 	bl	80090f0 <vPortEnterCritical>
 8007c2e:	4b39      	ldr	r3, [pc, #228]	; (8007d14 <xTaskResumeAll+0x114>)
 8007c30:	681b      	ldr	r3, [r3, #0]
 8007c32:	3b01      	subs	r3, #1
 8007c34:	4a37      	ldr	r2, [pc, #220]	; (8007d14 <xTaskResumeAll+0x114>)
 8007c36:	6013      	str	r3, [r2, #0]
 8007c38:	4b36      	ldr	r3, [pc, #216]	; (8007d14 <xTaskResumeAll+0x114>)
 8007c3a:	681b      	ldr	r3, [r3, #0]
 8007c3c:	2b00      	cmp	r3, #0
 8007c3e:	d161      	bne.n	8007d04 <xTaskResumeAll+0x104>
 8007c40:	4b35      	ldr	r3, [pc, #212]	; (8007d18 <xTaskResumeAll+0x118>)
 8007c42:	681b      	ldr	r3, [r3, #0]
 8007c44:	2b00      	cmp	r3, #0
 8007c46:	d05d      	beq.n	8007d04 <xTaskResumeAll+0x104>
 8007c48:	e02e      	b.n	8007ca8 <xTaskResumeAll+0xa8>
 8007c4a:	4b34      	ldr	r3, [pc, #208]	; (8007d1c <xTaskResumeAll+0x11c>)
 8007c4c:	68db      	ldr	r3, [r3, #12]
 8007c4e:	68db      	ldr	r3, [r3, #12]
 8007c50:	60fb      	str	r3, [r7, #12]
 8007c52:	68fb      	ldr	r3, [r7, #12]
 8007c54:	3318      	adds	r3, #24
 8007c56:	4618      	mov	r0, r3
 8007c58:	f7ff fd52 	bl	8007700 <uxListRemove>
 8007c5c:	68fb      	ldr	r3, [r7, #12]
 8007c5e:	3304      	adds	r3, #4
 8007c60:	4618      	mov	r0, r3
 8007c62:	f7ff fd4d 	bl	8007700 <uxListRemove>
 8007c66:	68fb      	ldr	r3, [r7, #12]
 8007c68:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007c6a:	2201      	movs	r2, #1
 8007c6c:	409a      	lsls	r2, r3
 8007c6e:	4b2c      	ldr	r3, [pc, #176]	; (8007d20 <xTaskResumeAll+0x120>)
 8007c70:	681b      	ldr	r3, [r3, #0]
 8007c72:	4313      	orrs	r3, r2
 8007c74:	4a2a      	ldr	r2, [pc, #168]	; (8007d20 <xTaskResumeAll+0x120>)
 8007c76:	6013      	str	r3, [r2, #0]
 8007c78:	68fb      	ldr	r3, [r7, #12]
 8007c7a:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007c7c:	4613      	mov	r3, r2
 8007c7e:	009b      	lsls	r3, r3, #2
 8007c80:	4413      	add	r3, r2
 8007c82:	009b      	lsls	r3, r3, #2
 8007c84:	4a27      	ldr	r2, [pc, #156]	; (8007d24 <xTaskResumeAll+0x124>)
 8007c86:	441a      	add	r2, r3
 8007c88:	68fb      	ldr	r3, [r7, #12]
 8007c8a:	3304      	adds	r3, #4
 8007c8c:	4619      	mov	r1, r3
 8007c8e:	4610      	mov	r0, r2
 8007c90:	f7ff fcd9 	bl	8007646 <vListInsertEnd>
 8007c94:	68fb      	ldr	r3, [r7, #12]
 8007c96:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007c98:	4b23      	ldr	r3, [pc, #140]	; (8007d28 <xTaskResumeAll+0x128>)
 8007c9a:	681b      	ldr	r3, [r3, #0]
 8007c9c:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007c9e:	429a      	cmp	r2, r3
 8007ca0:	d302      	bcc.n	8007ca8 <xTaskResumeAll+0xa8>
 8007ca2:	4b22      	ldr	r3, [pc, #136]	; (8007d2c <xTaskResumeAll+0x12c>)
 8007ca4:	2201      	movs	r2, #1
 8007ca6:	601a      	str	r2, [r3, #0]
 8007ca8:	4b1c      	ldr	r3, [pc, #112]	; (8007d1c <xTaskResumeAll+0x11c>)
 8007caa:	681b      	ldr	r3, [r3, #0]
 8007cac:	2b00      	cmp	r3, #0
 8007cae:	d1cc      	bne.n	8007c4a <xTaskResumeAll+0x4a>
 8007cb0:	68fb      	ldr	r3, [r7, #12]
 8007cb2:	2b00      	cmp	r3, #0
 8007cb4:	d001      	beq.n	8007cba <xTaskResumeAll+0xba>
 8007cb6:	f000 fb85 	bl	80083c4 <prvResetNextTaskUnblockTime>
 8007cba:	4b1d      	ldr	r3, [pc, #116]	; (8007d30 <xTaskResumeAll+0x130>)
 8007cbc:	681b      	ldr	r3, [r3, #0]
 8007cbe:	607b      	str	r3, [r7, #4]
 8007cc0:	687b      	ldr	r3, [r7, #4]
 8007cc2:	2b00      	cmp	r3, #0
 8007cc4:	d010      	beq.n	8007ce8 <xTaskResumeAll+0xe8>
 8007cc6:	f000 f847 	bl	8007d58 <xTaskIncrementTick>
 8007cca:	4603      	mov	r3, r0
 8007ccc:	2b00      	cmp	r3, #0
 8007cce:	d002      	beq.n	8007cd6 <xTaskResumeAll+0xd6>
 8007cd0:	4b16      	ldr	r3, [pc, #88]	; (8007d2c <xTaskResumeAll+0x12c>)
 8007cd2:	2201      	movs	r2, #1
 8007cd4:	601a      	str	r2, [r3, #0]
 8007cd6:	687b      	ldr	r3, [r7, #4]
 8007cd8:	3b01      	subs	r3, #1
 8007cda:	607b      	str	r3, [r7, #4]
 8007cdc:	687b      	ldr	r3, [r7, #4]
 8007cde:	2b00      	cmp	r3, #0
 8007ce0:	d1f1      	bne.n	8007cc6 <xTaskResumeAll+0xc6>
 8007ce2:	4b13      	ldr	r3, [pc, #76]	; (8007d30 <xTaskResumeAll+0x130>)
 8007ce4:	2200      	movs	r2, #0
 8007ce6:	601a      	str	r2, [r3, #0]
 8007ce8:	4b10      	ldr	r3, [pc, #64]	; (8007d2c <xTaskResumeAll+0x12c>)
 8007cea:	681b      	ldr	r3, [r3, #0]
 8007cec:	2b00      	cmp	r3, #0
 8007cee:	d009      	beq.n	8007d04 <xTaskResumeAll+0x104>
 8007cf0:	2301      	movs	r3, #1
 8007cf2:	60bb      	str	r3, [r7, #8]
 8007cf4:	4b0f      	ldr	r3, [pc, #60]	; (8007d34 <xTaskResumeAll+0x134>)
 8007cf6:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8007cfa:	601a      	str	r2, [r3, #0]
 8007cfc:	f3bf 8f4f 	dsb	sy
 8007d00:	f3bf 8f6f 	isb	sy
 8007d04:	f001 fa22 	bl	800914c <vPortExitCritical>
 8007d08:	68bb      	ldr	r3, [r7, #8]
 8007d0a:	4618      	mov	r0, r3
 8007d0c:	3710      	adds	r7, #16
 8007d0e:	46bd      	mov	sp, r7
 8007d10:	bd80      	pop	{r7, pc}
 8007d12:	bf00      	nop
 8007d14:	2000134c 	.word	0x2000134c
 8007d18:	20001324 	.word	0x20001324
 8007d1c:	200012e4 	.word	0x200012e4
 8007d20:	2000132c 	.word	0x2000132c
 8007d24:	20001250 	.word	0x20001250
 8007d28:	2000124c 	.word	0x2000124c
 8007d2c:	20001338 	.word	0x20001338
 8007d30:	20001334 	.word	0x20001334
 8007d34:	e000ed04 	.word	0xe000ed04

08007d38 <xTaskGetTickCount>:
 8007d38:	b480      	push	{r7}
 8007d3a:	b083      	sub	sp, #12
 8007d3c:	af00      	add	r7, sp, #0
 8007d3e:	4b05      	ldr	r3, [pc, #20]	; (8007d54 <xTaskGetTickCount+0x1c>)
 8007d40:	681b      	ldr	r3, [r3, #0]
 8007d42:	607b      	str	r3, [r7, #4]
 8007d44:	687b      	ldr	r3, [r7, #4]
 8007d46:	4618      	mov	r0, r3
 8007d48:	370c      	adds	r7, #12
 8007d4a:	46bd      	mov	sp, r7
 8007d4c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8007d50:	4770      	bx	lr
 8007d52:	bf00      	nop
 8007d54:	20001328 	.word	0x20001328

08007d58 <xTaskIncrementTick>:
 8007d58:	b580      	push	{r7, lr}
 8007d5a:	b086      	sub	sp, #24
 8007d5c:	af00      	add	r7, sp, #0
 8007d5e:	2300      	movs	r3, #0
 8007d60:	617b      	str	r3, [r7, #20]
 8007d62:	4b52      	ldr	r3, [pc, #328]	; (8007eac <xTaskIncrementTick+0x154>)
 8007d64:	681b      	ldr	r3, [r3, #0]
 8007d66:	2b00      	cmp	r3, #0
 8007d68:	f040 8093 	bne.w	8007e92 <xTaskIncrementTick+0x13a>
 8007d6c:	4b50      	ldr	r3, [pc, #320]	; (8007eb0 <xTaskIncrementTick+0x158>)
 8007d6e:	681b      	ldr	r3, [r3, #0]
 8007d70:	3301      	adds	r3, #1
 8007d72:	613b      	str	r3, [r7, #16]
 8007d74:	4a4e      	ldr	r2, [pc, #312]	; (8007eb0 <xTaskIncrementTick+0x158>)
 8007d76:	693b      	ldr	r3, [r7, #16]
 8007d78:	6013      	str	r3, [r2, #0]
 8007d7a:	693b      	ldr	r3, [r7, #16]
 8007d7c:	2b00      	cmp	r3, #0
 8007d7e:	d11f      	bne.n	8007dc0 <xTaskIncrementTick+0x68>
 8007d80:	4b4c      	ldr	r3, [pc, #304]	; (8007eb4 <xTaskIncrementTick+0x15c>)
 8007d82:	681b      	ldr	r3, [r3, #0]
 8007d84:	681b      	ldr	r3, [r3, #0]
 8007d86:	2b00      	cmp	r3, #0
 8007d88:	d009      	beq.n	8007d9e <xTaskIncrementTick+0x46>
 8007d8a:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007d8e:	f383 8811 	msr	BASEPRI, r3
 8007d92:	f3bf 8f6f 	isb	sy
 8007d96:	f3bf 8f4f 	dsb	sy
 8007d9a:	603b      	str	r3, [r7, #0]
 8007d9c:	e7fe      	b.n	8007d9c <xTaskIncrementTick+0x44>
 8007d9e:	4b45      	ldr	r3, [pc, #276]	; (8007eb4 <xTaskIncrementTick+0x15c>)
 8007da0:	681b      	ldr	r3, [r3, #0]
 8007da2:	60fb      	str	r3, [r7, #12]
 8007da4:	4b44      	ldr	r3, [pc, #272]	; (8007eb8 <xTaskIncrementTick+0x160>)
 8007da6:	681b      	ldr	r3, [r3, #0]
 8007da8:	4a42      	ldr	r2, [pc, #264]	; (8007eb4 <xTaskIncrementTick+0x15c>)
 8007daa:	6013      	str	r3, [r2, #0]
 8007dac:	4a42      	ldr	r2, [pc, #264]	; (8007eb8 <xTaskIncrementTick+0x160>)
 8007dae:	68fb      	ldr	r3, [r7, #12]
 8007db0:	6013      	str	r3, [r2, #0]
 8007db2:	4b42      	ldr	r3, [pc, #264]	; (8007ebc <xTaskIncrementTick+0x164>)
 8007db4:	681b      	ldr	r3, [r3, #0]
 8007db6:	3301      	adds	r3, #1
 8007db8:	4a40      	ldr	r2, [pc, #256]	; (8007ebc <xTaskIncrementTick+0x164>)
 8007dba:	6013      	str	r3, [r2, #0]
 8007dbc:	f000 fb02 	bl	80083c4 <prvResetNextTaskUnblockTime>
 8007dc0:	4b3f      	ldr	r3, [pc, #252]	; (8007ec0 <xTaskIncrementTick+0x168>)
 8007dc2:	681b      	ldr	r3, [r3, #0]
 8007dc4:	693a      	ldr	r2, [r7, #16]
 8007dc6:	429a      	cmp	r2, r3
 8007dc8:	d348      	bcc.n	8007e5c <xTaskIncrementTick+0x104>
 8007dca:	4b3a      	ldr	r3, [pc, #232]	; (8007eb4 <xTaskIncrementTick+0x15c>)
 8007dcc:	681b      	ldr	r3, [r3, #0]
 8007dce:	681b      	ldr	r3, [r3, #0]
 8007dd0:	2b00      	cmp	r3, #0
 8007dd2:	d104      	bne.n	8007dde <xTaskIncrementTick+0x86>
 8007dd4:	4b3a      	ldr	r3, [pc, #232]	; (8007ec0 <xTaskIncrementTick+0x168>)
 8007dd6:	f04f 32ff 	mov.w	r2, #4294967295
 8007dda:	601a      	str	r2, [r3, #0]
 8007ddc:	e03e      	b.n	8007e5c <xTaskIncrementTick+0x104>
 8007dde:	4b35      	ldr	r3, [pc, #212]	; (8007eb4 <xTaskIncrementTick+0x15c>)
 8007de0:	681b      	ldr	r3, [r3, #0]
 8007de2:	68db      	ldr	r3, [r3, #12]
 8007de4:	68db      	ldr	r3, [r3, #12]
 8007de6:	60bb      	str	r3, [r7, #8]
 8007de8:	68bb      	ldr	r3, [r7, #8]
 8007dea:	685b      	ldr	r3, [r3, #4]
 8007dec:	607b      	str	r3, [r7, #4]
 8007dee:	693a      	ldr	r2, [r7, #16]
 8007df0:	687b      	ldr	r3, [r7, #4]
 8007df2:	429a      	cmp	r2, r3
 8007df4:	d203      	bcs.n	8007dfe <xTaskIncrementTick+0xa6>
 8007df6:	4a32      	ldr	r2, [pc, #200]	; (8007ec0 <xTaskIncrementTick+0x168>)
 8007df8:	687b      	ldr	r3, [r7, #4]
 8007dfa:	6013      	str	r3, [r2, #0]
 8007dfc:	e02e      	b.n	8007e5c <xTaskIncrementTick+0x104>
 8007dfe:	68bb      	ldr	r3, [r7, #8]
 8007e00:	3304      	adds	r3, #4
 8007e02:	4618      	mov	r0, r3
 8007e04:	f7ff fc7c 	bl	8007700 <uxListRemove>
 8007e08:	68bb      	ldr	r3, [r7, #8]
 8007e0a:	6a9b      	ldr	r3, [r3, #40]	; 0x28
 8007e0c:	2b00      	cmp	r3, #0
 8007e0e:	d004      	beq.n	8007e1a <xTaskIncrementTick+0xc2>
 8007e10:	68bb      	ldr	r3, [r7, #8]
 8007e12:	3318      	adds	r3, #24
 8007e14:	4618      	mov	r0, r3
 8007e16:	f7ff fc73 	bl	8007700 <uxListRemove>
 8007e1a:	68bb      	ldr	r3, [r7, #8]
 8007e1c:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007e1e:	2201      	movs	r2, #1
 8007e20:	409a      	lsls	r2, r3
 8007e22:	4b28      	ldr	r3, [pc, #160]	; (8007ec4 <xTaskIncrementTick+0x16c>)
 8007e24:	681b      	ldr	r3, [r3, #0]
 8007e26:	4313      	orrs	r3, r2
 8007e28:	4a26      	ldr	r2, [pc, #152]	; (8007ec4 <xTaskIncrementTick+0x16c>)
 8007e2a:	6013      	str	r3, [r2, #0]
 8007e2c:	68bb      	ldr	r3, [r7, #8]
 8007e2e:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007e30:	4613      	mov	r3, r2
 8007e32:	009b      	lsls	r3, r3, #2
 8007e34:	4413      	add	r3, r2
 8007e36:	009b      	lsls	r3, r3, #2
 8007e38:	4a23      	ldr	r2, [pc, #140]	; (8007ec8 <xTaskIncrementTick+0x170>)
 8007e3a:	441a      	add	r2, r3
 8007e3c:	68bb      	ldr	r3, [r7, #8]
 8007e3e:	3304      	adds	r3, #4
 8007e40:	4619      	mov	r1, r3
 8007e42:	4610      	mov	r0, r2
 8007e44:	f7ff fbff 	bl	8007646 <vListInsertEnd>
 8007e48:	68bb      	ldr	r3, [r7, #8]
 8007e4a:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007e4c:	4b1f      	ldr	r3, [pc, #124]	; (8007ecc <xTaskIncrementTick+0x174>)
 8007e4e:	681b      	ldr	r3, [r3, #0]
 8007e50:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8007e52:	429a      	cmp	r2, r3
 8007e54:	d3b9      	bcc.n	8007dca <xTaskIncrementTick+0x72>
 8007e56:	2301      	movs	r3, #1
 8007e58:	617b      	str	r3, [r7, #20]
 8007e5a:	e7b6      	b.n	8007dca <xTaskIncrementTick+0x72>
 8007e5c:	4b1b      	ldr	r3, [pc, #108]	; (8007ecc <xTaskIncrementTick+0x174>)
 8007e5e:	681b      	ldr	r3, [r3, #0]
 8007e60:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8007e62:	4919      	ldr	r1, [pc, #100]	; (8007ec8 <xTaskIncrementTick+0x170>)
 8007e64:	4613      	mov	r3, r2
 8007e66:	009b      	lsls	r3, r3, #2
 8007e68:	4413      	add	r3, r2
 8007e6a:	009b      	lsls	r3, r3, #2
 8007e6c:	440b      	add	r3, r1
 8007e6e:	681b      	ldr	r3, [r3, #0]
 8007e70:	2b01      	cmp	r3, #1
 8007e72:	d901      	bls.n	8007e78 <xTaskIncrementTick+0x120>
 8007e74:	2301      	movs	r3, #1
 8007e76:	617b      	str	r3, [r7, #20]
 8007e78:	4b15      	ldr	r3, [pc, #84]	; (8007ed0 <xTaskIncrementTick+0x178>)
 8007e7a:	681b      	ldr	r3, [r3, #0]
 8007e7c:	2b00      	cmp	r3, #0
 8007e7e:	d101      	bne.n	8007e84 <xTaskIncrementTick+0x12c>
 8007e80:	f7fc f823 	bl	8003eca <vApplicationTickHook>
 8007e84:	4b13      	ldr	r3, [pc, #76]	; (8007ed4 <xTaskIncrementTick+0x17c>)
 8007e86:	681b      	ldr	r3, [r3, #0]
 8007e88:	2b00      	cmp	r3, #0
 8007e8a:	d009      	beq.n	8007ea0 <xTaskIncrementTick+0x148>
 8007e8c:	2301      	movs	r3, #1
 8007e8e:	617b      	str	r3, [r7, #20]
 8007e90:	e006      	b.n	8007ea0 <xTaskIncrementTick+0x148>
 8007e92:	4b0f      	ldr	r3, [pc, #60]	; (8007ed0 <xTaskIncrementTick+0x178>)
 8007e94:	681b      	ldr	r3, [r3, #0]
 8007e96:	3301      	adds	r3, #1
 8007e98:	4a0d      	ldr	r2, [pc, #52]	; (8007ed0 <xTaskIncrementTick+0x178>)
 8007e9a:	6013      	str	r3, [r2, #0]
 8007e9c:	f7fc f815 	bl	8003eca <vApplicationTickHook>
 8007ea0:	697b      	ldr	r3, [r7, #20]
 8007ea2:	4618      	mov	r0, r3
 8007ea4:	3718      	adds	r7, #24
 8007ea6:	46bd      	mov	sp, r7
 8007ea8:	bd80      	pop	{r7, pc}
 8007eaa:	bf00      	nop
 8007eac:	2000134c 	.word	0x2000134c
 8007eb0:	20001328 	.word	0x20001328
 8007eb4:	200012dc 	.word	0x200012dc
 8007eb8:	200012e0 	.word	0x200012e0
 8007ebc:	2000133c 	.word	0x2000133c
 8007ec0:	20001344 	.word	0x20001344
 8007ec4:	2000132c 	.word	0x2000132c
 8007ec8:	20001250 	.word	0x20001250
 8007ecc:	2000124c 	.word	0x2000124c
 8007ed0:	20001334 	.word	0x20001334
 8007ed4:	20001338 	.word	0x20001338

08007ed8 <vTaskSwitchContext>:
 8007ed8:	b580      	push	{r7, lr}
 8007eda:	b088      	sub	sp, #32
 8007edc:	af00      	add	r7, sp, #0
 8007ede:	4b3b      	ldr	r3, [pc, #236]	; (8007fcc <vTaskSwitchContext+0xf4>)
 8007ee0:	681b      	ldr	r3, [r3, #0]
 8007ee2:	2b00      	cmp	r3, #0
 8007ee4:	d003      	beq.n	8007eee <vTaskSwitchContext+0x16>
 8007ee6:	4b3a      	ldr	r3, [pc, #232]	; (8007fd0 <vTaskSwitchContext+0xf8>)
 8007ee8:	2201      	movs	r2, #1
 8007eea:	601a      	str	r2, [r3, #0]
 8007eec:	e06a      	b.n	8007fc4 <vTaskSwitchContext+0xec>
 8007eee:	4b38      	ldr	r3, [pc, #224]	; (8007fd0 <vTaskSwitchContext+0xf8>)
 8007ef0:	2200      	movs	r2, #0
 8007ef2:	601a      	str	r2, [r3, #0]
 8007ef4:	4b37      	ldr	r3, [pc, #220]	; (8007fd4 <vTaskSwitchContext+0xfc>)
 8007ef6:	681b      	ldr	r3, [r3, #0]
 8007ef8:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 8007efa:	61fb      	str	r3, [r7, #28]
 8007efc:	f04f 33a5 	mov.w	r3, #2779096485	; 0xa5a5a5a5
 8007f00:	61bb      	str	r3, [r7, #24]
 8007f02:	69fb      	ldr	r3, [r7, #28]
 8007f04:	681a      	ldr	r2, [r3, #0]
 8007f06:	69bb      	ldr	r3, [r7, #24]
 8007f08:	429a      	cmp	r2, r3
 8007f0a:	d111      	bne.n	8007f30 <vTaskSwitchContext+0x58>
 8007f0c:	69fb      	ldr	r3, [r7, #28]
 8007f0e:	3304      	adds	r3, #4
 8007f10:	681a      	ldr	r2, [r3, #0]
 8007f12:	69bb      	ldr	r3, [r7, #24]
 8007f14:	429a      	cmp	r2, r3
 8007f16:	d10b      	bne.n	8007f30 <vTaskSwitchContext+0x58>
 8007f18:	69fb      	ldr	r3, [r7, #28]
 8007f1a:	3308      	adds	r3, #8
 8007f1c:	681a      	ldr	r2, [r3, #0]
 8007f1e:	69bb      	ldr	r3, [r7, #24]
 8007f20:	429a      	cmp	r2, r3
 8007f22:	d105      	bne.n	8007f30 <vTaskSwitchContext+0x58>
 8007f24:	69fb      	ldr	r3, [r7, #28]
 8007f26:	330c      	adds	r3, #12
 8007f28:	681a      	ldr	r2, [r3, #0]
 8007f2a:	69bb      	ldr	r3, [r7, #24]
 8007f2c:	429a      	cmp	r2, r3
 8007f2e:	d008      	beq.n	8007f42 <vTaskSwitchContext+0x6a>
 8007f30:	4b28      	ldr	r3, [pc, #160]	; (8007fd4 <vTaskSwitchContext+0xfc>)
 8007f32:	681a      	ldr	r2, [r3, #0]
 8007f34:	4b27      	ldr	r3, [pc, #156]	; (8007fd4 <vTaskSwitchContext+0xfc>)
 8007f36:	681b      	ldr	r3, [r3, #0]
 8007f38:	3334      	adds	r3, #52	; 0x34
 8007f3a:	4619      	mov	r1, r3
 8007f3c:	4610      	mov	r0, r2
 8007f3e:	f7fb ffcb 	bl	8003ed8 <vApplicationStackOverflowHook>
 8007f42:	4b25      	ldr	r3, [pc, #148]	; (8007fd8 <vTaskSwitchContext+0x100>)
 8007f44:	681b      	ldr	r3, [r3, #0]
 8007f46:	60fb      	str	r3, [r7, #12]
 8007f48:	68fb      	ldr	r3, [r7, #12]
 8007f4a:	fab3 f383 	clz	r3, r3
 8007f4e:	72fb      	strb	r3, [r7, #11]
 8007f50:	7afb      	ldrb	r3, [r7, #11]
 8007f52:	f1c3 031f 	rsb	r3, r3, #31
 8007f56:	617b      	str	r3, [r7, #20]
 8007f58:	4920      	ldr	r1, [pc, #128]	; (8007fdc <vTaskSwitchContext+0x104>)
 8007f5a:	697a      	ldr	r2, [r7, #20]
 8007f5c:	4613      	mov	r3, r2
 8007f5e:	009b      	lsls	r3, r3, #2
 8007f60:	4413      	add	r3, r2
 8007f62:	009b      	lsls	r3, r3, #2
 8007f64:	440b      	add	r3, r1
 8007f66:	681b      	ldr	r3, [r3, #0]
 8007f68:	2b00      	cmp	r3, #0
 8007f6a:	d109      	bne.n	8007f80 <vTaskSwitchContext+0xa8>
 8007f6c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007f70:	f383 8811 	msr	BASEPRI, r3
 8007f74:	f3bf 8f6f 	isb	sy
 8007f78:	f3bf 8f4f 	dsb	sy
 8007f7c:	607b      	str	r3, [r7, #4]
 8007f7e:	e7fe      	b.n	8007f7e <vTaskSwitchContext+0xa6>
 8007f80:	697a      	ldr	r2, [r7, #20]
 8007f82:	4613      	mov	r3, r2
 8007f84:	009b      	lsls	r3, r3, #2
 8007f86:	4413      	add	r3, r2
 8007f88:	009b      	lsls	r3, r3, #2
 8007f8a:	4a14      	ldr	r2, [pc, #80]	; (8007fdc <vTaskSwitchContext+0x104>)
 8007f8c:	4413      	add	r3, r2
 8007f8e:	613b      	str	r3, [r7, #16]
 8007f90:	693b      	ldr	r3, [r7, #16]
 8007f92:	685b      	ldr	r3, [r3, #4]
 8007f94:	685a      	ldr	r2, [r3, #4]
 8007f96:	693b      	ldr	r3, [r7, #16]
 8007f98:	605a      	str	r2, [r3, #4]
 8007f9a:	693b      	ldr	r3, [r7, #16]
 8007f9c:	685a      	ldr	r2, [r3, #4]
 8007f9e:	693b      	ldr	r3, [r7, #16]
 8007fa0:	3308      	adds	r3, #8
 8007fa2:	429a      	cmp	r2, r3
 8007fa4:	d104      	bne.n	8007fb0 <vTaskSwitchContext+0xd8>
 8007fa6:	693b      	ldr	r3, [r7, #16]
 8007fa8:	685b      	ldr	r3, [r3, #4]
 8007faa:	685a      	ldr	r2, [r3, #4]
 8007fac:	693b      	ldr	r3, [r7, #16]
 8007fae:	605a      	str	r2, [r3, #4]
 8007fb0:	693b      	ldr	r3, [r7, #16]
 8007fb2:	685b      	ldr	r3, [r3, #4]
 8007fb4:	68db      	ldr	r3, [r3, #12]
 8007fb6:	4a07      	ldr	r2, [pc, #28]	; (8007fd4 <vTaskSwitchContext+0xfc>)
 8007fb8:	6013      	str	r3, [r2, #0]
 8007fba:	4b06      	ldr	r3, [pc, #24]	; (8007fd4 <vTaskSwitchContext+0xfc>)
 8007fbc:	681b      	ldr	r3, [r3, #0]
 8007fbe:	3350      	adds	r3, #80	; 0x50
 8007fc0:	4a07      	ldr	r2, [pc, #28]	; (8007fe0 <vTaskSwitchContext+0x108>)
 8007fc2:	6013      	str	r3, [r2, #0]
 8007fc4:	bf00      	nop
 8007fc6:	3720      	adds	r7, #32
 8007fc8:	46bd      	mov	sp, r7
 8007fca:	bd80      	pop	{r7, pc}
 8007fcc:	2000134c 	.word	0x2000134c
 8007fd0:	20001338 	.word	0x20001338
 8007fd4:	2000124c 	.word	0x2000124c
 8007fd8:	2000132c 	.word	0x2000132c
 8007fdc:	20001250 	.word	0x20001250
 8007fe0:	20000000 	.word	0x20000000

08007fe4 <vTaskPlaceOnEventList>:
 8007fe4:	b580      	push	{r7, lr}
 8007fe6:	b084      	sub	sp, #16
 8007fe8:	af00      	add	r7, sp, #0
 8007fea:	6078      	str	r0, [r7, #4]
 8007fec:	6039      	str	r1, [r7, #0]
 8007fee:	687b      	ldr	r3, [r7, #4]
 8007ff0:	2b00      	cmp	r3, #0
 8007ff2:	d109      	bne.n	8008008 <vTaskPlaceOnEventList+0x24>
 8007ff4:	f04f 0350 	mov.w	r3, #80	; 0x50
 8007ff8:	f383 8811 	msr	BASEPRI, r3
 8007ffc:	f3bf 8f6f 	isb	sy
 8008000:	f3bf 8f4f 	dsb	sy
 8008004:	60fb      	str	r3, [r7, #12]
 8008006:	e7fe      	b.n	8008006 <vTaskPlaceOnEventList+0x22>
 8008008:	4b07      	ldr	r3, [pc, #28]	; (8008028 <vTaskPlaceOnEventList+0x44>)
 800800a:	681b      	ldr	r3, [r3, #0]
 800800c:	3318      	adds	r3, #24
 800800e:	4619      	mov	r1, r3
 8008010:	6878      	ldr	r0, [r7, #4]
 8008012:	f7ff fb3c 	bl	800768e <vListInsert>
 8008016:	2101      	movs	r1, #1
 8008018:	6838      	ldr	r0, [r7, #0]
 800801a:	f000 fbad 	bl	8008778 <prvAddCurrentTaskToDelayedList>
 800801e:	bf00      	nop
 8008020:	3710      	adds	r7, #16
 8008022:	46bd      	mov	sp, r7
 8008024:	bd80      	pop	{r7, pc}
 8008026:	bf00      	nop
 8008028:	2000124c 	.word	0x2000124c

0800802c <vTaskPlaceOnEventListRestricted>:
 800802c:	b580      	push	{r7, lr}
 800802e:	b086      	sub	sp, #24
 8008030:	af00      	add	r7, sp, #0
 8008032:	60f8      	str	r0, [r7, #12]
 8008034:	60b9      	str	r1, [r7, #8]
 8008036:	607a      	str	r2, [r7, #4]
 8008038:	68fb      	ldr	r3, [r7, #12]
 800803a:	2b00      	cmp	r3, #0
 800803c:	d109      	bne.n	8008052 <vTaskPlaceOnEventListRestricted+0x26>
 800803e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008042:	f383 8811 	msr	BASEPRI, r3
 8008046:	f3bf 8f6f 	isb	sy
 800804a:	f3bf 8f4f 	dsb	sy
 800804e:	617b      	str	r3, [r7, #20]
 8008050:	e7fe      	b.n	8008050 <vTaskPlaceOnEventListRestricted+0x24>
 8008052:	4b0a      	ldr	r3, [pc, #40]	; (800807c <vTaskPlaceOnEventListRestricted+0x50>)
 8008054:	681b      	ldr	r3, [r3, #0]
 8008056:	3318      	adds	r3, #24
 8008058:	4619      	mov	r1, r3
 800805a:	68f8      	ldr	r0, [r7, #12]
 800805c:	f7ff faf3 	bl	8007646 <vListInsertEnd>
 8008060:	687b      	ldr	r3, [r7, #4]
 8008062:	2b00      	cmp	r3, #0
 8008064:	d002      	beq.n	800806c <vTaskPlaceOnEventListRestricted+0x40>
 8008066:	f04f 33ff 	mov.w	r3, #4294967295
 800806a:	60bb      	str	r3, [r7, #8]
 800806c:	6879      	ldr	r1, [r7, #4]
 800806e:	68b8      	ldr	r0, [r7, #8]
 8008070:	f000 fb82 	bl	8008778 <prvAddCurrentTaskToDelayedList>
 8008074:	bf00      	nop
 8008076:	3718      	adds	r7, #24
 8008078:	46bd      	mov	sp, r7
 800807a:	bd80      	pop	{r7, pc}
 800807c:	2000124c 	.word	0x2000124c

08008080 <xTaskRemoveFromEventList>:
 8008080:	b580      	push	{r7, lr}
 8008082:	b086      	sub	sp, #24
 8008084:	af00      	add	r7, sp, #0
 8008086:	6078      	str	r0, [r7, #4]
 8008088:	687b      	ldr	r3, [r7, #4]
 800808a:	68db      	ldr	r3, [r3, #12]
 800808c:	68db      	ldr	r3, [r3, #12]
 800808e:	613b      	str	r3, [r7, #16]
 8008090:	693b      	ldr	r3, [r7, #16]
 8008092:	2b00      	cmp	r3, #0
 8008094:	d109      	bne.n	80080aa <xTaskRemoveFromEventList+0x2a>
 8008096:	f04f 0350 	mov.w	r3, #80	; 0x50
 800809a:	f383 8811 	msr	BASEPRI, r3
 800809e:	f3bf 8f6f 	isb	sy
 80080a2:	f3bf 8f4f 	dsb	sy
 80080a6:	60fb      	str	r3, [r7, #12]
 80080a8:	e7fe      	b.n	80080a8 <xTaskRemoveFromEventList+0x28>
 80080aa:	693b      	ldr	r3, [r7, #16]
 80080ac:	3318      	adds	r3, #24
 80080ae:	4618      	mov	r0, r3
 80080b0:	f7ff fb26 	bl	8007700 <uxListRemove>
 80080b4:	4b1d      	ldr	r3, [pc, #116]	; (800812c <xTaskRemoveFromEventList+0xac>)
 80080b6:	681b      	ldr	r3, [r3, #0]
 80080b8:	2b00      	cmp	r3, #0
 80080ba:	d11c      	bne.n	80080f6 <xTaskRemoveFromEventList+0x76>
 80080bc:	693b      	ldr	r3, [r7, #16]
 80080be:	3304      	adds	r3, #4
 80080c0:	4618      	mov	r0, r3
 80080c2:	f7ff fb1d 	bl	8007700 <uxListRemove>
 80080c6:	693b      	ldr	r3, [r7, #16]
 80080c8:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80080ca:	2201      	movs	r2, #1
 80080cc:	409a      	lsls	r2, r3
 80080ce:	4b18      	ldr	r3, [pc, #96]	; (8008130 <xTaskRemoveFromEventList+0xb0>)
 80080d0:	681b      	ldr	r3, [r3, #0]
 80080d2:	4313      	orrs	r3, r2
 80080d4:	4a16      	ldr	r2, [pc, #88]	; (8008130 <xTaskRemoveFromEventList+0xb0>)
 80080d6:	6013      	str	r3, [r2, #0]
 80080d8:	693b      	ldr	r3, [r7, #16]
 80080da:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 80080dc:	4613      	mov	r3, r2
 80080de:	009b      	lsls	r3, r3, #2
 80080e0:	4413      	add	r3, r2
 80080e2:	009b      	lsls	r3, r3, #2
 80080e4:	4a13      	ldr	r2, [pc, #76]	; (8008134 <xTaskRemoveFromEventList+0xb4>)
 80080e6:	441a      	add	r2, r3
 80080e8:	693b      	ldr	r3, [r7, #16]
 80080ea:	3304      	adds	r3, #4
 80080ec:	4619      	mov	r1, r3
 80080ee:	4610      	mov	r0, r2
 80080f0:	f7ff faa9 	bl	8007646 <vListInsertEnd>
 80080f4:	e005      	b.n	8008102 <xTaskRemoveFromEventList+0x82>
 80080f6:	693b      	ldr	r3, [r7, #16]
 80080f8:	3318      	adds	r3, #24
 80080fa:	4619      	mov	r1, r3
 80080fc:	480e      	ldr	r0, [pc, #56]	; (8008138 <xTaskRemoveFromEventList+0xb8>)
 80080fe:	f7ff faa2 	bl	8007646 <vListInsertEnd>
 8008102:	693b      	ldr	r3, [r7, #16]
 8008104:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008106:	4b0d      	ldr	r3, [pc, #52]	; (800813c <xTaskRemoveFromEventList+0xbc>)
 8008108:	681b      	ldr	r3, [r3, #0]
 800810a:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 800810c:	429a      	cmp	r2, r3
 800810e:	d905      	bls.n	800811c <xTaskRemoveFromEventList+0x9c>
 8008110:	2301      	movs	r3, #1
 8008112:	617b      	str	r3, [r7, #20]
 8008114:	4b0a      	ldr	r3, [pc, #40]	; (8008140 <xTaskRemoveFromEventList+0xc0>)
 8008116:	2201      	movs	r2, #1
 8008118:	601a      	str	r2, [r3, #0]
 800811a:	e001      	b.n	8008120 <xTaskRemoveFromEventList+0xa0>
 800811c:	2300      	movs	r3, #0
 800811e:	617b      	str	r3, [r7, #20]
 8008120:	697b      	ldr	r3, [r7, #20]
 8008122:	4618      	mov	r0, r3
 8008124:	3718      	adds	r7, #24
 8008126:	46bd      	mov	sp, r7
 8008128:	bd80      	pop	{r7, pc}
 800812a:	bf00      	nop
 800812c:	2000134c 	.word	0x2000134c
 8008130:	2000132c 	.word	0x2000132c
 8008134:	20001250 	.word	0x20001250
 8008138:	200012e4 	.word	0x200012e4
 800813c:	2000124c 	.word	0x2000124c
 8008140:	20001338 	.word	0x20001338

08008144 <vTaskInternalSetTimeOutState>:
 8008144:	b480      	push	{r7}
 8008146:	b083      	sub	sp, #12
 8008148:	af00      	add	r7, sp, #0
 800814a:	6078      	str	r0, [r7, #4]
 800814c:	4b06      	ldr	r3, [pc, #24]	; (8008168 <vTaskInternalSetTimeOutState+0x24>)
 800814e:	681a      	ldr	r2, [r3, #0]
 8008150:	687b      	ldr	r3, [r7, #4]
 8008152:	601a      	str	r2, [r3, #0]
 8008154:	4b05      	ldr	r3, [pc, #20]	; (800816c <vTaskInternalSetTimeOutState+0x28>)
 8008156:	681a      	ldr	r2, [r3, #0]
 8008158:	687b      	ldr	r3, [r7, #4]
 800815a:	605a      	str	r2, [r3, #4]
 800815c:	bf00      	nop
 800815e:	370c      	adds	r7, #12
 8008160:	46bd      	mov	sp, r7
 8008162:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008166:	4770      	bx	lr
 8008168:	2000133c 	.word	0x2000133c
 800816c:	20001328 	.word	0x20001328

08008170 <xTaskCheckForTimeOut>:
 8008170:	b580      	push	{r7, lr}
 8008172:	b088      	sub	sp, #32
 8008174:	af00      	add	r7, sp, #0
 8008176:	6078      	str	r0, [r7, #4]
 8008178:	6039      	str	r1, [r7, #0]
 800817a:	687b      	ldr	r3, [r7, #4]
 800817c:	2b00      	cmp	r3, #0
 800817e:	d109      	bne.n	8008194 <xTaskCheckForTimeOut+0x24>
 8008180:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008184:	f383 8811 	msr	BASEPRI, r3
 8008188:	f3bf 8f6f 	isb	sy
 800818c:	f3bf 8f4f 	dsb	sy
 8008190:	613b      	str	r3, [r7, #16]
 8008192:	e7fe      	b.n	8008192 <xTaskCheckForTimeOut+0x22>
 8008194:	683b      	ldr	r3, [r7, #0]
 8008196:	2b00      	cmp	r3, #0
 8008198:	d109      	bne.n	80081ae <xTaskCheckForTimeOut+0x3e>
 800819a:	f04f 0350 	mov.w	r3, #80	; 0x50
 800819e:	f383 8811 	msr	BASEPRI, r3
 80081a2:	f3bf 8f6f 	isb	sy
 80081a6:	f3bf 8f4f 	dsb	sy
 80081aa:	60fb      	str	r3, [r7, #12]
 80081ac:	e7fe      	b.n	80081ac <xTaskCheckForTimeOut+0x3c>
 80081ae:	f000 ff9f 	bl	80090f0 <vPortEnterCritical>
 80081b2:	4b1f      	ldr	r3, [pc, #124]	; (8008230 <xTaskCheckForTimeOut+0xc0>)
 80081b4:	681b      	ldr	r3, [r3, #0]
 80081b6:	61bb      	str	r3, [r7, #24]
 80081b8:	687b      	ldr	r3, [r7, #4]
 80081ba:	685b      	ldr	r3, [r3, #4]
 80081bc:	69ba      	ldr	r2, [r7, #24]
 80081be:	1ad3      	subs	r3, r2, r3
 80081c0:	617b      	str	r3, [r7, #20]
 80081c2:	683b      	ldr	r3, [r7, #0]
 80081c4:	681b      	ldr	r3, [r3, #0]
 80081c6:	f1b3 3fff 	cmp.w	r3, #4294967295
 80081ca:	d102      	bne.n	80081d2 <xTaskCheckForTimeOut+0x62>
 80081cc:	2300      	movs	r3, #0
 80081ce:	61fb      	str	r3, [r7, #28]
 80081d0:	e026      	b.n	8008220 <xTaskCheckForTimeOut+0xb0>
 80081d2:	687b      	ldr	r3, [r7, #4]
 80081d4:	681a      	ldr	r2, [r3, #0]
 80081d6:	4b17      	ldr	r3, [pc, #92]	; (8008234 <xTaskCheckForTimeOut+0xc4>)
 80081d8:	681b      	ldr	r3, [r3, #0]
 80081da:	429a      	cmp	r2, r3
 80081dc:	d00a      	beq.n	80081f4 <xTaskCheckForTimeOut+0x84>
 80081de:	687b      	ldr	r3, [r7, #4]
 80081e0:	685a      	ldr	r2, [r3, #4]
 80081e2:	69bb      	ldr	r3, [r7, #24]
 80081e4:	429a      	cmp	r2, r3
 80081e6:	d805      	bhi.n	80081f4 <xTaskCheckForTimeOut+0x84>
 80081e8:	2301      	movs	r3, #1
 80081ea:	61fb      	str	r3, [r7, #28]
 80081ec:	683b      	ldr	r3, [r7, #0]
 80081ee:	2200      	movs	r2, #0
 80081f0:	601a      	str	r2, [r3, #0]
 80081f2:	e015      	b.n	8008220 <xTaskCheckForTimeOut+0xb0>
 80081f4:	683b      	ldr	r3, [r7, #0]
 80081f6:	681a      	ldr	r2, [r3, #0]
 80081f8:	697b      	ldr	r3, [r7, #20]
 80081fa:	429a      	cmp	r2, r3
 80081fc:	d90b      	bls.n	8008216 <xTaskCheckForTimeOut+0xa6>
 80081fe:	683b      	ldr	r3, [r7, #0]
 8008200:	681a      	ldr	r2, [r3, #0]
 8008202:	697b      	ldr	r3, [r7, #20]
 8008204:	1ad2      	subs	r2, r2, r3
 8008206:	683b      	ldr	r3, [r7, #0]
 8008208:	601a      	str	r2, [r3, #0]
 800820a:	6878      	ldr	r0, [r7, #4]
 800820c:	f7ff ff9a 	bl	8008144 <vTaskInternalSetTimeOutState>
 8008210:	2300      	movs	r3, #0
 8008212:	61fb      	str	r3, [r7, #28]
 8008214:	e004      	b.n	8008220 <xTaskCheckForTimeOut+0xb0>
 8008216:	683b      	ldr	r3, [r7, #0]
 8008218:	2200      	movs	r2, #0
 800821a:	601a      	str	r2, [r3, #0]
 800821c:	2301      	movs	r3, #1
 800821e:	61fb      	str	r3, [r7, #28]
 8008220:	f000 ff94 	bl	800914c <vPortExitCritical>
 8008224:	69fb      	ldr	r3, [r7, #28]
 8008226:	4618      	mov	r0, r3
 8008228:	3720      	adds	r7, #32
 800822a:	46bd      	mov	sp, r7
 800822c:	bd80      	pop	{r7, pc}
 800822e:	bf00      	nop
 8008230:	20001328 	.word	0x20001328
 8008234:	2000133c 	.word	0x2000133c

08008238 <vTaskMissedYield>:
 8008238:	b480      	push	{r7}
 800823a:	af00      	add	r7, sp, #0
 800823c:	4b03      	ldr	r3, [pc, #12]	; (800824c <vTaskMissedYield+0x14>)
 800823e:	2201      	movs	r2, #1
 8008240:	601a      	str	r2, [r3, #0]
 8008242:	bf00      	nop
 8008244:	46bd      	mov	sp, r7
 8008246:	f85d 7b04 	ldr.w	r7, [sp], #4
 800824a:	4770      	bx	lr
 800824c:	20001338 	.word	0x20001338

08008250 <prvIdleTask>:
 8008250:	b580      	push	{r7, lr}
 8008252:	b082      	sub	sp, #8
 8008254:	af00      	add	r7, sp, #0
 8008256:	6078      	str	r0, [r7, #4]
 8008258:	f000 f854 	bl	8008304 <prvCheckTasksWaitingTermination>
 800825c:	4b07      	ldr	r3, [pc, #28]	; (800827c <prvIdleTask+0x2c>)
 800825e:	681b      	ldr	r3, [r3, #0]
 8008260:	2b01      	cmp	r3, #1
 8008262:	d907      	bls.n	8008274 <prvIdleTask+0x24>
 8008264:	4b06      	ldr	r3, [pc, #24]	; (8008280 <prvIdleTask+0x30>)
 8008266:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800826a:	601a      	str	r2, [r3, #0]
 800826c:	f3bf 8f4f 	dsb	sy
 8008270:	f3bf 8f6f 	isb	sy
 8008274:	f7fb fe22 	bl	8003ebc <vApplicationIdleHook>
 8008278:	e7ee      	b.n	8008258 <prvIdleTask+0x8>
 800827a:	bf00      	nop
 800827c:	20001250 	.word	0x20001250
 8008280:	e000ed04 	.word	0xe000ed04

08008284 <prvInitialiseTaskLists>:
 8008284:	b580      	push	{r7, lr}
 8008286:	b082      	sub	sp, #8
 8008288:	af00      	add	r7, sp, #0
 800828a:	2300      	movs	r3, #0
 800828c:	607b      	str	r3, [r7, #4]
 800828e:	e00c      	b.n	80082aa <prvInitialiseTaskLists+0x26>
 8008290:	687a      	ldr	r2, [r7, #4]
 8008292:	4613      	mov	r3, r2
 8008294:	009b      	lsls	r3, r3, #2
 8008296:	4413      	add	r3, r2
 8008298:	009b      	lsls	r3, r3, #2
 800829a:	4a12      	ldr	r2, [pc, #72]	; (80082e4 <prvInitialiseTaskLists+0x60>)
 800829c:	4413      	add	r3, r2
 800829e:	4618      	mov	r0, r3
 80082a0:	f7ff f9a4 	bl	80075ec <vListInitialise>
 80082a4:	687b      	ldr	r3, [r7, #4]
 80082a6:	3301      	adds	r3, #1
 80082a8:	607b      	str	r3, [r7, #4]
 80082aa:	687b      	ldr	r3, [r7, #4]
 80082ac:	2b04      	cmp	r3, #4
 80082ae:	d9ef      	bls.n	8008290 <prvInitialiseTaskLists+0xc>
 80082b0:	480d      	ldr	r0, [pc, #52]	; (80082e8 <prvInitialiseTaskLists+0x64>)
 80082b2:	f7ff f99b 	bl	80075ec <vListInitialise>
 80082b6:	480d      	ldr	r0, [pc, #52]	; (80082ec <prvInitialiseTaskLists+0x68>)
 80082b8:	f7ff f998 	bl	80075ec <vListInitialise>
 80082bc:	480c      	ldr	r0, [pc, #48]	; (80082f0 <prvInitialiseTaskLists+0x6c>)
 80082be:	f7ff f995 	bl	80075ec <vListInitialise>
 80082c2:	480c      	ldr	r0, [pc, #48]	; (80082f4 <prvInitialiseTaskLists+0x70>)
 80082c4:	f7ff f992 	bl	80075ec <vListInitialise>
 80082c8:	480b      	ldr	r0, [pc, #44]	; (80082f8 <prvInitialiseTaskLists+0x74>)
 80082ca:	f7ff f98f 	bl	80075ec <vListInitialise>
 80082ce:	4b0b      	ldr	r3, [pc, #44]	; (80082fc <prvInitialiseTaskLists+0x78>)
 80082d0:	4a05      	ldr	r2, [pc, #20]	; (80082e8 <prvInitialiseTaskLists+0x64>)
 80082d2:	601a      	str	r2, [r3, #0]
 80082d4:	4b0a      	ldr	r3, [pc, #40]	; (8008300 <prvInitialiseTaskLists+0x7c>)
 80082d6:	4a05      	ldr	r2, [pc, #20]	; (80082ec <prvInitialiseTaskLists+0x68>)
 80082d8:	601a      	str	r2, [r3, #0]
 80082da:	bf00      	nop
 80082dc:	3708      	adds	r7, #8
 80082de:	46bd      	mov	sp, r7
 80082e0:	bd80      	pop	{r7, pc}
 80082e2:	bf00      	nop
 80082e4:	20001250 	.word	0x20001250
 80082e8:	200012b4 	.word	0x200012b4
 80082ec:	200012c8 	.word	0x200012c8
 80082f0:	200012e4 	.word	0x200012e4
 80082f4:	200012f8 	.word	0x200012f8
 80082f8:	20001310 	.word	0x20001310
 80082fc:	200012dc 	.word	0x200012dc
 8008300:	200012e0 	.word	0x200012e0

08008304 <prvCheckTasksWaitingTermination>:
 8008304:	b580      	push	{r7, lr}
 8008306:	b082      	sub	sp, #8
 8008308:	af00      	add	r7, sp, #0
 800830a:	e019      	b.n	8008340 <prvCheckTasksWaitingTermination+0x3c>
 800830c:	f000 fef0 	bl	80090f0 <vPortEnterCritical>
 8008310:	4b0f      	ldr	r3, [pc, #60]	; (8008350 <prvCheckTasksWaitingTermination+0x4c>)
 8008312:	68db      	ldr	r3, [r3, #12]
 8008314:	68db      	ldr	r3, [r3, #12]
 8008316:	607b      	str	r3, [r7, #4]
 8008318:	687b      	ldr	r3, [r7, #4]
 800831a:	3304      	adds	r3, #4
 800831c:	4618      	mov	r0, r3
 800831e:	f7ff f9ef 	bl	8007700 <uxListRemove>
 8008322:	4b0c      	ldr	r3, [pc, #48]	; (8008354 <prvCheckTasksWaitingTermination+0x50>)
 8008324:	681b      	ldr	r3, [r3, #0]
 8008326:	3b01      	subs	r3, #1
 8008328:	4a0a      	ldr	r2, [pc, #40]	; (8008354 <prvCheckTasksWaitingTermination+0x50>)
 800832a:	6013      	str	r3, [r2, #0]
 800832c:	4b0a      	ldr	r3, [pc, #40]	; (8008358 <prvCheckTasksWaitingTermination+0x54>)
 800832e:	681b      	ldr	r3, [r3, #0]
 8008330:	3b01      	subs	r3, #1
 8008332:	4a09      	ldr	r2, [pc, #36]	; (8008358 <prvCheckTasksWaitingTermination+0x54>)
 8008334:	6013      	str	r3, [r2, #0]
 8008336:	f000 ff09 	bl	800914c <vPortExitCritical>
 800833a:	6878      	ldr	r0, [r7, #4]
 800833c:	f000 f80e 	bl	800835c <prvDeleteTCB>
 8008340:	4b05      	ldr	r3, [pc, #20]	; (8008358 <prvCheckTasksWaitingTermination+0x54>)
 8008342:	681b      	ldr	r3, [r3, #0]
 8008344:	2b00      	cmp	r3, #0
 8008346:	d1e1      	bne.n	800830c <prvCheckTasksWaitingTermination+0x8>
 8008348:	bf00      	nop
 800834a:	3708      	adds	r7, #8
 800834c:	46bd      	mov	sp, r7
 800834e:	bd80      	pop	{r7, pc}
 8008350:	200012f8 	.word	0x200012f8
 8008354:	20001324 	.word	0x20001324
 8008358:	2000130c 	.word	0x2000130c

0800835c <prvDeleteTCB>:
 800835c:	b580      	push	{r7, lr}
 800835e:	b084      	sub	sp, #16
 8008360:	af00      	add	r7, sp, #0
 8008362:	6078      	str	r0, [r7, #4]
 8008364:	687b      	ldr	r3, [r7, #4]
 8008366:	3350      	adds	r3, #80	; 0x50
 8008368:	4618      	mov	r0, r3
 800836a:	f7f8 fd67 	bl	8000e3c <_reclaim_reent>
 800836e:	687b      	ldr	r3, [r7, #4]
 8008370:	f893 347d 	ldrb.w	r3, [r3, #1149]	; 0x47d
 8008374:	2b00      	cmp	r3, #0
 8008376:	d108      	bne.n	800838a <prvDeleteTCB+0x2e>
 8008378:	687b      	ldr	r3, [r7, #4]
 800837a:	6b1b      	ldr	r3, [r3, #48]	; 0x30
 800837c:	4618      	mov	r0, r3
 800837e:	f000 ffd1 	bl	8009324 <vPortFree>
 8008382:	6878      	ldr	r0, [r7, #4]
 8008384:	f000 ffce 	bl	8009324 <vPortFree>
 8008388:	e017      	b.n	80083ba <prvDeleteTCB+0x5e>
 800838a:	687b      	ldr	r3, [r7, #4]
 800838c:	f893 347d 	ldrb.w	r3, [r3, #1149]	; 0x47d
 8008390:	2b01      	cmp	r3, #1
 8008392:	d103      	bne.n	800839c <prvDeleteTCB+0x40>
 8008394:	6878      	ldr	r0, [r7, #4]
 8008396:	f000 ffc5 	bl	8009324 <vPortFree>
 800839a:	e00e      	b.n	80083ba <prvDeleteTCB+0x5e>
 800839c:	687b      	ldr	r3, [r7, #4]
 800839e:	f893 347d 	ldrb.w	r3, [r3, #1149]	; 0x47d
 80083a2:	2b02      	cmp	r3, #2
 80083a4:	d009      	beq.n	80083ba <prvDeleteTCB+0x5e>
 80083a6:	f04f 0350 	mov.w	r3, #80	; 0x50
 80083aa:	f383 8811 	msr	BASEPRI, r3
 80083ae:	f3bf 8f6f 	isb	sy
 80083b2:	f3bf 8f4f 	dsb	sy
 80083b6:	60fb      	str	r3, [r7, #12]
 80083b8:	e7fe      	b.n	80083b8 <prvDeleteTCB+0x5c>
 80083ba:	bf00      	nop
 80083bc:	3710      	adds	r7, #16
 80083be:	46bd      	mov	sp, r7
 80083c0:	bd80      	pop	{r7, pc}
	...

080083c4 <prvResetNextTaskUnblockTime>:
 80083c4:	b480      	push	{r7}
 80083c6:	af00      	add	r7, sp, #0
 80083c8:	4b0a      	ldr	r3, [pc, #40]	; (80083f4 <prvResetNextTaskUnblockTime+0x30>)
 80083ca:	681b      	ldr	r3, [r3, #0]
 80083cc:	681b      	ldr	r3, [r3, #0]
 80083ce:	2b00      	cmp	r3, #0
 80083d0:	d104      	bne.n	80083dc <prvResetNextTaskUnblockTime+0x18>
 80083d2:	4b09      	ldr	r3, [pc, #36]	; (80083f8 <prvResetNextTaskUnblockTime+0x34>)
 80083d4:	f04f 32ff 	mov.w	r2, #4294967295
 80083d8:	601a      	str	r2, [r3, #0]
 80083da:	e005      	b.n	80083e8 <prvResetNextTaskUnblockTime+0x24>
 80083dc:	4b05      	ldr	r3, [pc, #20]	; (80083f4 <prvResetNextTaskUnblockTime+0x30>)
 80083de:	681b      	ldr	r3, [r3, #0]
 80083e0:	68db      	ldr	r3, [r3, #12]
 80083e2:	681b      	ldr	r3, [r3, #0]
 80083e4:	4a04      	ldr	r2, [pc, #16]	; (80083f8 <prvResetNextTaskUnblockTime+0x34>)
 80083e6:	6013      	str	r3, [r2, #0]
 80083e8:	bf00      	nop
 80083ea:	46bd      	mov	sp, r7
 80083ec:	f85d 7b04 	ldr.w	r7, [sp], #4
 80083f0:	4770      	bx	lr
 80083f2:	bf00      	nop
 80083f4:	200012dc 	.word	0x200012dc
 80083f8:	20001344 	.word	0x20001344

080083fc <xTaskGetCurrentTaskHandle>:
 80083fc:	b480      	push	{r7}
 80083fe:	b083      	sub	sp, #12
 8008400:	af00      	add	r7, sp, #0
 8008402:	4b05      	ldr	r3, [pc, #20]	; (8008418 <xTaskGetCurrentTaskHandle+0x1c>)
 8008404:	681b      	ldr	r3, [r3, #0]
 8008406:	607b      	str	r3, [r7, #4]
 8008408:	687b      	ldr	r3, [r7, #4]
 800840a:	4618      	mov	r0, r3
 800840c:	370c      	adds	r7, #12
 800840e:	46bd      	mov	sp, r7
 8008410:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008414:	4770      	bx	lr
 8008416:	bf00      	nop
 8008418:	2000124c 	.word	0x2000124c

0800841c <xTaskGetSchedulerState>:
 800841c:	b480      	push	{r7}
 800841e:	b083      	sub	sp, #12
 8008420:	af00      	add	r7, sp, #0
 8008422:	4b0b      	ldr	r3, [pc, #44]	; (8008450 <xTaskGetSchedulerState+0x34>)
 8008424:	681b      	ldr	r3, [r3, #0]
 8008426:	2b00      	cmp	r3, #0
 8008428:	d102      	bne.n	8008430 <xTaskGetSchedulerState+0x14>
 800842a:	2301      	movs	r3, #1
 800842c:	607b      	str	r3, [r7, #4]
 800842e:	e008      	b.n	8008442 <xTaskGetSchedulerState+0x26>
 8008430:	4b08      	ldr	r3, [pc, #32]	; (8008454 <xTaskGetSchedulerState+0x38>)
 8008432:	681b      	ldr	r3, [r3, #0]
 8008434:	2b00      	cmp	r3, #0
 8008436:	d102      	bne.n	800843e <xTaskGetSchedulerState+0x22>
 8008438:	2302      	movs	r3, #2
 800843a:	607b      	str	r3, [r7, #4]
 800843c:	e001      	b.n	8008442 <xTaskGetSchedulerState+0x26>
 800843e:	2300      	movs	r3, #0
 8008440:	607b      	str	r3, [r7, #4]
 8008442:	687b      	ldr	r3, [r7, #4]
 8008444:	4618      	mov	r0, r3
 8008446:	370c      	adds	r7, #12
 8008448:	46bd      	mov	sp, r7
 800844a:	f85d 7b04 	ldr.w	r7, [sp], #4
 800844e:	4770      	bx	lr
 8008450:	20001330 	.word	0x20001330
 8008454:	2000134c 	.word	0x2000134c

08008458 <xTaskPriorityDisinherit>:
 8008458:	b580      	push	{r7, lr}
 800845a:	b086      	sub	sp, #24
 800845c:	af00      	add	r7, sp, #0
 800845e:	6078      	str	r0, [r7, #4]
 8008460:	687b      	ldr	r3, [r7, #4]
 8008462:	613b      	str	r3, [r7, #16]
 8008464:	2300      	movs	r3, #0
 8008466:	617b      	str	r3, [r7, #20]
 8008468:	687b      	ldr	r3, [r7, #4]
 800846a:	2b00      	cmp	r3, #0
 800846c:	d061      	beq.n	8008532 <xTaskPriorityDisinherit+0xda>
 800846e:	4b33      	ldr	r3, [pc, #204]	; (800853c <xTaskPriorityDisinherit+0xe4>)
 8008470:	681b      	ldr	r3, [r3, #0]
 8008472:	693a      	ldr	r2, [r7, #16]
 8008474:	429a      	cmp	r2, r3
 8008476:	d009      	beq.n	800848c <xTaskPriorityDisinherit+0x34>
 8008478:	f04f 0350 	mov.w	r3, #80	; 0x50
 800847c:	f383 8811 	msr	BASEPRI, r3
 8008480:	f3bf 8f6f 	isb	sy
 8008484:	f3bf 8f4f 	dsb	sy
 8008488:	60fb      	str	r3, [r7, #12]
 800848a:	e7fe      	b.n	800848a <xTaskPriorityDisinherit+0x32>
 800848c:	693b      	ldr	r3, [r7, #16]
 800848e:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 8008490:	2b00      	cmp	r3, #0
 8008492:	d109      	bne.n	80084a8 <xTaskPriorityDisinherit+0x50>
 8008494:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008498:	f383 8811 	msr	BASEPRI, r3
 800849c:	f3bf 8f6f 	isb	sy
 80084a0:	f3bf 8f4f 	dsb	sy
 80084a4:	60bb      	str	r3, [r7, #8]
 80084a6:	e7fe      	b.n	80084a6 <xTaskPriorityDisinherit+0x4e>
 80084a8:	693b      	ldr	r3, [r7, #16]
 80084aa:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 80084ac:	1e5a      	subs	r2, r3, #1
 80084ae:	693b      	ldr	r3, [r7, #16]
 80084b0:	64da      	str	r2, [r3, #76]	; 0x4c
 80084b2:	693b      	ldr	r3, [r7, #16]
 80084b4:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 80084b6:	693b      	ldr	r3, [r7, #16]
 80084b8:	6c9b      	ldr	r3, [r3, #72]	; 0x48
 80084ba:	429a      	cmp	r2, r3
 80084bc:	d039      	beq.n	8008532 <xTaskPriorityDisinherit+0xda>
 80084be:	693b      	ldr	r3, [r7, #16]
 80084c0:	6cdb      	ldr	r3, [r3, #76]	; 0x4c
 80084c2:	2b00      	cmp	r3, #0
 80084c4:	d135      	bne.n	8008532 <xTaskPriorityDisinherit+0xda>
 80084c6:	693b      	ldr	r3, [r7, #16]
 80084c8:	3304      	adds	r3, #4
 80084ca:	4618      	mov	r0, r3
 80084cc:	f7ff f918 	bl	8007700 <uxListRemove>
 80084d0:	4603      	mov	r3, r0
 80084d2:	2b00      	cmp	r3, #0
 80084d4:	d10a      	bne.n	80084ec <xTaskPriorityDisinherit+0x94>
 80084d6:	693b      	ldr	r3, [r7, #16]
 80084d8:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80084da:	2201      	movs	r2, #1
 80084dc:	fa02 f303 	lsl.w	r3, r2, r3
 80084e0:	43da      	mvns	r2, r3
 80084e2:	4b17      	ldr	r3, [pc, #92]	; (8008540 <xTaskPriorityDisinherit+0xe8>)
 80084e4:	681b      	ldr	r3, [r3, #0]
 80084e6:	4013      	ands	r3, r2
 80084e8:	4a15      	ldr	r2, [pc, #84]	; (8008540 <xTaskPriorityDisinherit+0xe8>)
 80084ea:	6013      	str	r3, [r2, #0]
 80084ec:	693b      	ldr	r3, [r7, #16]
 80084ee:	6c9a      	ldr	r2, [r3, #72]	; 0x48
 80084f0:	693b      	ldr	r3, [r7, #16]
 80084f2:	62da      	str	r2, [r3, #44]	; 0x2c
 80084f4:	693b      	ldr	r3, [r7, #16]
 80084f6:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80084f8:	f1c3 0205 	rsb	r2, r3, #5
 80084fc:	693b      	ldr	r3, [r7, #16]
 80084fe:	619a      	str	r2, [r3, #24]
 8008500:	693b      	ldr	r3, [r7, #16]
 8008502:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008504:	2201      	movs	r2, #1
 8008506:	409a      	lsls	r2, r3
 8008508:	4b0d      	ldr	r3, [pc, #52]	; (8008540 <xTaskPriorityDisinherit+0xe8>)
 800850a:	681b      	ldr	r3, [r3, #0]
 800850c:	4313      	orrs	r3, r2
 800850e:	4a0c      	ldr	r2, [pc, #48]	; (8008540 <xTaskPriorityDisinherit+0xe8>)
 8008510:	6013      	str	r3, [r2, #0]
 8008512:	693b      	ldr	r3, [r7, #16]
 8008514:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008516:	4613      	mov	r3, r2
 8008518:	009b      	lsls	r3, r3, #2
 800851a:	4413      	add	r3, r2
 800851c:	009b      	lsls	r3, r3, #2
 800851e:	4a09      	ldr	r2, [pc, #36]	; (8008544 <xTaskPriorityDisinherit+0xec>)
 8008520:	441a      	add	r2, r3
 8008522:	693b      	ldr	r3, [r7, #16]
 8008524:	3304      	adds	r3, #4
 8008526:	4619      	mov	r1, r3
 8008528:	4610      	mov	r0, r2
 800852a:	f7ff f88c 	bl	8007646 <vListInsertEnd>
 800852e:	2301      	movs	r3, #1
 8008530:	617b      	str	r3, [r7, #20]
 8008532:	697b      	ldr	r3, [r7, #20]
 8008534:	4618      	mov	r0, r3
 8008536:	3718      	adds	r7, #24
 8008538:	46bd      	mov	sp, r7
 800853a:	bd80      	pop	{r7, pc}
 800853c:	2000124c 	.word	0x2000124c
 8008540:	2000132c 	.word	0x2000132c
 8008544:	20001250 	.word	0x20001250

08008548 <ulTaskGenericNotifyTake>:
 8008548:	b580      	push	{r7, lr}
 800854a:	b086      	sub	sp, #24
 800854c:	af00      	add	r7, sp, #0
 800854e:	60f8      	str	r0, [r7, #12]
 8008550:	60b9      	str	r1, [r7, #8]
 8008552:	607a      	str	r2, [r7, #4]
 8008554:	68fb      	ldr	r3, [r7, #12]
 8008556:	2b00      	cmp	r3, #0
 8008558:	d009      	beq.n	800856e <ulTaskGenericNotifyTake+0x26>
 800855a:	f04f 0350 	mov.w	r3, #80	; 0x50
 800855e:	f383 8811 	msr	BASEPRI, r3
 8008562:	f3bf 8f6f 	isb	sy
 8008566:	f3bf 8f4f 	dsb	sy
 800856a:	613b      	str	r3, [r7, #16]
 800856c:	e7fe      	b.n	800856c <ulTaskGenericNotifyTake+0x24>
 800856e:	f000 fdbf 	bl	80090f0 <vPortEnterCritical>
 8008572:	4b29      	ldr	r3, [pc, #164]	; (8008618 <ulTaskGenericNotifyTake+0xd0>)
 8008574:	681b      	ldr	r3, [r3, #0]
 8008576:	68fa      	ldr	r2, [r7, #12]
 8008578:	f502 728f 	add.w	r2, r2, #286	; 0x11e
 800857c:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 8008580:	2b00      	cmp	r3, #0
 8008582:	d116      	bne.n	80085b2 <ulTaskGenericNotifyTake+0x6a>
 8008584:	4b24      	ldr	r3, [pc, #144]	; (8008618 <ulTaskGenericNotifyTake+0xd0>)
 8008586:	681a      	ldr	r2, [r3, #0]
 8008588:	68fb      	ldr	r3, [r7, #12]
 800858a:	4413      	add	r3, r2
 800858c:	f203 437c 	addw	r3, r3, #1148	; 0x47c
 8008590:	2201      	movs	r2, #1
 8008592:	701a      	strb	r2, [r3, #0]
 8008594:	687b      	ldr	r3, [r7, #4]
 8008596:	2b00      	cmp	r3, #0
 8008598:	d00b      	beq.n	80085b2 <ulTaskGenericNotifyTake+0x6a>
 800859a:	2101      	movs	r1, #1
 800859c:	6878      	ldr	r0, [r7, #4]
 800859e:	f000 f8eb 	bl	8008778 <prvAddCurrentTaskToDelayedList>
 80085a2:	4b1e      	ldr	r3, [pc, #120]	; (800861c <ulTaskGenericNotifyTake+0xd4>)
 80085a4:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 80085a8:	601a      	str	r2, [r3, #0]
 80085aa:	f3bf 8f4f 	dsb	sy
 80085ae:	f3bf 8f6f 	isb	sy
 80085b2:	f000 fdcb 	bl	800914c <vPortExitCritical>
 80085b6:	f000 fd9b 	bl	80090f0 <vPortEnterCritical>
 80085ba:	4b17      	ldr	r3, [pc, #92]	; (8008618 <ulTaskGenericNotifyTake+0xd0>)
 80085bc:	681b      	ldr	r3, [r3, #0]
 80085be:	68fa      	ldr	r2, [r7, #12]
 80085c0:	f502 728f 	add.w	r2, r2, #286	; 0x11e
 80085c4:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 80085c8:	617b      	str	r3, [r7, #20]
 80085ca:	697b      	ldr	r3, [r7, #20]
 80085cc:	2b00      	cmp	r3, #0
 80085ce:	d014      	beq.n	80085fa <ulTaskGenericNotifyTake+0xb2>
 80085d0:	68bb      	ldr	r3, [r7, #8]
 80085d2:	2b00      	cmp	r3, #0
 80085d4:	d008      	beq.n	80085e8 <ulTaskGenericNotifyTake+0xa0>
 80085d6:	4b10      	ldr	r3, [pc, #64]	; (8008618 <ulTaskGenericNotifyTake+0xd0>)
 80085d8:	681b      	ldr	r3, [r3, #0]
 80085da:	68fa      	ldr	r2, [r7, #12]
 80085dc:	f502 728f 	add.w	r2, r2, #286	; 0x11e
 80085e0:	2100      	movs	r1, #0
 80085e2:	f843 1022 	str.w	r1, [r3, r2, lsl #2]
 80085e6:	e008      	b.n	80085fa <ulTaskGenericNotifyTake+0xb2>
 80085e8:	4b0b      	ldr	r3, [pc, #44]	; (8008618 <ulTaskGenericNotifyTake+0xd0>)
 80085ea:	681b      	ldr	r3, [r3, #0]
 80085ec:	697a      	ldr	r2, [r7, #20]
 80085ee:	1e51      	subs	r1, r2, #1
 80085f0:	68fa      	ldr	r2, [r7, #12]
 80085f2:	f502 728f 	add.w	r2, r2, #286	; 0x11e
 80085f6:	f843 1022 	str.w	r1, [r3, r2, lsl #2]
 80085fa:	4b07      	ldr	r3, [pc, #28]	; (8008618 <ulTaskGenericNotifyTake+0xd0>)
 80085fc:	681a      	ldr	r2, [r3, #0]
 80085fe:	68fb      	ldr	r3, [r7, #12]
 8008600:	4413      	add	r3, r2
 8008602:	f203 437c 	addw	r3, r3, #1148	; 0x47c
 8008606:	2200      	movs	r2, #0
 8008608:	701a      	strb	r2, [r3, #0]
 800860a:	f000 fd9f 	bl	800914c <vPortExitCritical>
 800860e:	697b      	ldr	r3, [r7, #20]
 8008610:	4618      	mov	r0, r3
 8008612:	3718      	adds	r7, #24
 8008614:	46bd      	mov	sp, r7
 8008616:	bd80      	pop	{r7, pc}
 8008618:	2000124c 	.word	0x2000124c
 800861c:	e000ed04 	.word	0xe000ed04

08008620 <vTaskGenericNotifyGiveFromISR>:
 8008620:	b580      	push	{r7, lr}
 8008622:	b08e      	sub	sp, #56	; 0x38
 8008624:	af00      	add	r7, sp, #0
 8008626:	60f8      	str	r0, [r7, #12]
 8008628:	60b9      	str	r1, [r7, #8]
 800862a:	607a      	str	r2, [r7, #4]
 800862c:	68fb      	ldr	r3, [r7, #12]
 800862e:	2b00      	cmp	r3, #0
 8008630:	d109      	bne.n	8008646 <vTaskGenericNotifyGiveFromISR+0x26>
 8008632:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008636:	f383 8811 	msr	BASEPRI, r3
 800863a:	f3bf 8f6f 	isb	sy
 800863e:	f3bf 8f4f 	dsb	sy
 8008642:	62bb      	str	r3, [r7, #40]	; 0x28
 8008644:	e7fe      	b.n	8008644 <vTaskGenericNotifyGiveFromISR+0x24>
 8008646:	68bb      	ldr	r3, [r7, #8]
 8008648:	2b00      	cmp	r3, #0
 800864a:	d009      	beq.n	8008660 <vTaskGenericNotifyGiveFromISR+0x40>
 800864c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008650:	f383 8811 	msr	BASEPRI, r3
 8008654:	f3bf 8f6f 	isb	sy
 8008658:	f3bf 8f4f 	dsb	sy
 800865c:	627b      	str	r3, [r7, #36]	; 0x24
 800865e:	e7fe      	b.n	800865e <vTaskGenericNotifyGiveFromISR+0x3e>
 8008660:	f000 fe22 	bl	80092a8 <vPortValidateInterruptPriority>
 8008664:	68fb      	ldr	r3, [r7, #12]
 8008666:	637b      	str	r3, [r7, #52]	; 0x34
 8008668:	f3ef 8211 	mrs	r2, BASEPRI
 800866c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008670:	f383 8811 	msr	BASEPRI, r3
 8008674:	f3bf 8f6f 	isb	sy
 8008678:	f3bf 8f4f 	dsb	sy
 800867c:	623a      	str	r2, [r7, #32]
 800867e:	61fb      	str	r3, [r7, #28]
 8008680:	6a3b      	ldr	r3, [r7, #32]
 8008682:	633b      	str	r3, [r7, #48]	; 0x30
 8008684:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 8008686:	68bb      	ldr	r3, [r7, #8]
 8008688:	4413      	add	r3, r2
 800868a:	f203 437c 	addw	r3, r3, #1148	; 0x47c
 800868e:	781b      	ldrb	r3, [r3, #0]
 8008690:	f887 302f 	strb.w	r3, [r7, #47]	; 0x2f
 8008694:	6b7a      	ldr	r2, [r7, #52]	; 0x34
 8008696:	68bb      	ldr	r3, [r7, #8]
 8008698:	4413      	add	r3, r2
 800869a:	f203 437c 	addw	r3, r3, #1148	; 0x47c
 800869e:	2202      	movs	r2, #2
 80086a0:	701a      	strb	r2, [r3, #0]
 80086a2:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80086a4:	68ba      	ldr	r2, [r7, #8]
 80086a6:	f502 728f 	add.w	r2, r2, #286	; 0x11e
 80086aa:	f853 3022 	ldr.w	r3, [r3, r2, lsl #2]
 80086ae:	1c59      	adds	r1, r3, #1
 80086b0:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80086b2:	68ba      	ldr	r2, [r7, #8]
 80086b4:	f502 728f 	add.w	r2, r2, #286	; 0x11e
 80086b8:	f843 1022 	str.w	r1, [r3, r2, lsl #2]
 80086bc:	f897 302f 	ldrb.w	r3, [r7, #47]	; 0x2f
 80086c0:	2b01      	cmp	r3, #1
 80086c2:	d144      	bne.n	800874e <vTaskGenericNotifyGiveFromISR+0x12e>
 80086c4:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80086c6:	6a9b      	ldr	r3, [r3, #40]	; 0x28
 80086c8:	2b00      	cmp	r3, #0
 80086ca:	d009      	beq.n	80086e0 <vTaskGenericNotifyGiveFromISR+0xc0>
 80086cc:	f04f 0350 	mov.w	r3, #80	; 0x50
 80086d0:	f383 8811 	msr	BASEPRI, r3
 80086d4:	f3bf 8f6f 	isb	sy
 80086d8:	f3bf 8f4f 	dsb	sy
 80086dc:	61bb      	str	r3, [r7, #24]
 80086de:	e7fe      	b.n	80086de <vTaskGenericNotifyGiveFromISR+0xbe>
 80086e0:	4b1f      	ldr	r3, [pc, #124]	; (8008760 <vTaskGenericNotifyGiveFromISR+0x140>)
 80086e2:	681b      	ldr	r3, [r3, #0]
 80086e4:	2b00      	cmp	r3, #0
 80086e6:	d11c      	bne.n	8008722 <vTaskGenericNotifyGiveFromISR+0x102>
 80086e8:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80086ea:	3304      	adds	r3, #4
 80086ec:	4618      	mov	r0, r3
 80086ee:	f7ff f807 	bl	8007700 <uxListRemove>
 80086f2:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80086f4:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80086f6:	2201      	movs	r2, #1
 80086f8:	409a      	lsls	r2, r3
 80086fa:	4b1a      	ldr	r3, [pc, #104]	; (8008764 <vTaskGenericNotifyGiveFromISR+0x144>)
 80086fc:	681b      	ldr	r3, [r3, #0]
 80086fe:	4313      	orrs	r3, r2
 8008700:	4a18      	ldr	r2, [pc, #96]	; (8008764 <vTaskGenericNotifyGiveFromISR+0x144>)
 8008702:	6013      	str	r3, [r2, #0]
 8008704:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008706:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008708:	4613      	mov	r3, r2
 800870a:	009b      	lsls	r3, r3, #2
 800870c:	4413      	add	r3, r2
 800870e:	009b      	lsls	r3, r3, #2
 8008710:	4a15      	ldr	r2, [pc, #84]	; (8008768 <vTaskGenericNotifyGiveFromISR+0x148>)
 8008712:	441a      	add	r2, r3
 8008714:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008716:	3304      	adds	r3, #4
 8008718:	4619      	mov	r1, r3
 800871a:	4610      	mov	r0, r2
 800871c:	f7fe ff93 	bl	8007646 <vListInsertEnd>
 8008720:	e005      	b.n	800872e <vTaskGenericNotifyGiveFromISR+0x10e>
 8008722:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008724:	3318      	adds	r3, #24
 8008726:	4619      	mov	r1, r3
 8008728:	4810      	ldr	r0, [pc, #64]	; (800876c <vTaskGenericNotifyGiveFromISR+0x14c>)
 800872a:	f7fe ff8c 	bl	8007646 <vListInsertEnd>
 800872e:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8008730:	6ada      	ldr	r2, [r3, #44]	; 0x2c
 8008732:	4b0f      	ldr	r3, [pc, #60]	; (8008770 <vTaskGenericNotifyGiveFromISR+0x150>)
 8008734:	681b      	ldr	r3, [r3, #0]
 8008736:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 8008738:	429a      	cmp	r2, r3
 800873a:	d908      	bls.n	800874e <vTaskGenericNotifyGiveFromISR+0x12e>
 800873c:	687b      	ldr	r3, [r7, #4]
 800873e:	2b00      	cmp	r3, #0
 8008740:	d002      	beq.n	8008748 <vTaskGenericNotifyGiveFromISR+0x128>
 8008742:	687b      	ldr	r3, [r7, #4]
 8008744:	2201      	movs	r2, #1
 8008746:	601a      	str	r2, [r3, #0]
 8008748:	4b0a      	ldr	r3, [pc, #40]	; (8008774 <vTaskGenericNotifyGiveFromISR+0x154>)
 800874a:	2201      	movs	r2, #1
 800874c:	601a      	str	r2, [r3, #0]
 800874e:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8008750:	617b      	str	r3, [r7, #20]
 8008752:	697b      	ldr	r3, [r7, #20]
 8008754:	f383 8811 	msr	BASEPRI, r3
 8008758:	bf00      	nop
 800875a:	3738      	adds	r7, #56	; 0x38
 800875c:	46bd      	mov	sp, r7
 800875e:	bd80      	pop	{r7, pc}
 8008760:	2000134c 	.word	0x2000134c
 8008764:	2000132c 	.word	0x2000132c
 8008768:	20001250 	.word	0x20001250
 800876c:	200012e4 	.word	0x200012e4
 8008770:	2000124c 	.word	0x2000124c
 8008774:	20001338 	.word	0x20001338

08008778 <prvAddCurrentTaskToDelayedList>:
 8008778:	b580      	push	{r7, lr}
 800877a:	b084      	sub	sp, #16
 800877c:	af00      	add	r7, sp, #0
 800877e:	6078      	str	r0, [r7, #4]
 8008780:	6039      	str	r1, [r7, #0]
 8008782:	4b29      	ldr	r3, [pc, #164]	; (8008828 <prvAddCurrentTaskToDelayedList+0xb0>)
 8008784:	681b      	ldr	r3, [r3, #0]
 8008786:	60fb      	str	r3, [r7, #12]
 8008788:	4b28      	ldr	r3, [pc, #160]	; (800882c <prvAddCurrentTaskToDelayedList+0xb4>)
 800878a:	681b      	ldr	r3, [r3, #0]
 800878c:	3304      	adds	r3, #4
 800878e:	4618      	mov	r0, r3
 8008790:	f7fe ffb6 	bl	8007700 <uxListRemove>
 8008794:	4603      	mov	r3, r0
 8008796:	2b00      	cmp	r3, #0
 8008798:	d10b      	bne.n	80087b2 <prvAddCurrentTaskToDelayedList+0x3a>
 800879a:	4b24      	ldr	r3, [pc, #144]	; (800882c <prvAddCurrentTaskToDelayedList+0xb4>)
 800879c:	681b      	ldr	r3, [r3, #0]
 800879e:	6adb      	ldr	r3, [r3, #44]	; 0x2c
 80087a0:	2201      	movs	r2, #1
 80087a2:	fa02 f303 	lsl.w	r3, r2, r3
 80087a6:	43da      	mvns	r2, r3
 80087a8:	4b21      	ldr	r3, [pc, #132]	; (8008830 <prvAddCurrentTaskToDelayedList+0xb8>)
 80087aa:	681b      	ldr	r3, [r3, #0]
 80087ac:	4013      	ands	r3, r2
 80087ae:	4a20      	ldr	r2, [pc, #128]	; (8008830 <prvAddCurrentTaskToDelayedList+0xb8>)
 80087b0:	6013      	str	r3, [r2, #0]
 80087b2:	687b      	ldr	r3, [r7, #4]
 80087b4:	f1b3 3fff 	cmp.w	r3, #4294967295
 80087b8:	d10a      	bne.n	80087d0 <prvAddCurrentTaskToDelayedList+0x58>
 80087ba:	683b      	ldr	r3, [r7, #0]
 80087bc:	2b00      	cmp	r3, #0
 80087be:	d007      	beq.n	80087d0 <prvAddCurrentTaskToDelayedList+0x58>
 80087c0:	4b1a      	ldr	r3, [pc, #104]	; (800882c <prvAddCurrentTaskToDelayedList+0xb4>)
 80087c2:	681b      	ldr	r3, [r3, #0]
 80087c4:	3304      	adds	r3, #4
 80087c6:	4619      	mov	r1, r3
 80087c8:	481a      	ldr	r0, [pc, #104]	; (8008834 <prvAddCurrentTaskToDelayedList+0xbc>)
 80087ca:	f7fe ff3c 	bl	8007646 <vListInsertEnd>
 80087ce:	e026      	b.n	800881e <prvAddCurrentTaskToDelayedList+0xa6>
 80087d0:	68fa      	ldr	r2, [r7, #12]
 80087d2:	687b      	ldr	r3, [r7, #4]
 80087d4:	4413      	add	r3, r2
 80087d6:	60bb      	str	r3, [r7, #8]
 80087d8:	4b14      	ldr	r3, [pc, #80]	; (800882c <prvAddCurrentTaskToDelayedList+0xb4>)
 80087da:	681b      	ldr	r3, [r3, #0]
 80087dc:	68ba      	ldr	r2, [r7, #8]
 80087de:	605a      	str	r2, [r3, #4]
 80087e0:	68ba      	ldr	r2, [r7, #8]
 80087e2:	68fb      	ldr	r3, [r7, #12]
 80087e4:	429a      	cmp	r2, r3
 80087e6:	d209      	bcs.n	80087fc <prvAddCurrentTaskToDelayedList+0x84>
 80087e8:	4b13      	ldr	r3, [pc, #76]	; (8008838 <prvAddCurrentTaskToDelayedList+0xc0>)
 80087ea:	681a      	ldr	r2, [r3, #0]
 80087ec:	4b0f      	ldr	r3, [pc, #60]	; (800882c <prvAddCurrentTaskToDelayedList+0xb4>)
 80087ee:	681b      	ldr	r3, [r3, #0]
 80087f0:	3304      	adds	r3, #4
 80087f2:	4619      	mov	r1, r3
 80087f4:	4610      	mov	r0, r2
 80087f6:	f7fe ff4a 	bl	800768e <vListInsert>
 80087fa:	e010      	b.n	800881e <prvAddCurrentTaskToDelayedList+0xa6>
 80087fc:	4b0f      	ldr	r3, [pc, #60]	; (800883c <prvAddCurrentTaskToDelayedList+0xc4>)
 80087fe:	681a      	ldr	r2, [r3, #0]
 8008800:	4b0a      	ldr	r3, [pc, #40]	; (800882c <prvAddCurrentTaskToDelayedList+0xb4>)
 8008802:	681b      	ldr	r3, [r3, #0]
 8008804:	3304      	adds	r3, #4
 8008806:	4619      	mov	r1, r3
 8008808:	4610      	mov	r0, r2
 800880a:	f7fe ff40 	bl	800768e <vListInsert>
 800880e:	4b0c      	ldr	r3, [pc, #48]	; (8008840 <prvAddCurrentTaskToDelayedList+0xc8>)
 8008810:	681b      	ldr	r3, [r3, #0]
 8008812:	68ba      	ldr	r2, [r7, #8]
 8008814:	429a      	cmp	r2, r3
 8008816:	d202      	bcs.n	800881e <prvAddCurrentTaskToDelayedList+0xa6>
 8008818:	4a09      	ldr	r2, [pc, #36]	; (8008840 <prvAddCurrentTaskToDelayedList+0xc8>)
 800881a:	68bb      	ldr	r3, [r7, #8]
 800881c:	6013      	str	r3, [r2, #0]
 800881e:	bf00      	nop
 8008820:	3710      	adds	r7, #16
 8008822:	46bd      	mov	sp, r7
 8008824:	bd80      	pop	{r7, pc}
 8008826:	bf00      	nop
 8008828:	20001328 	.word	0x20001328
 800882c:	2000124c 	.word	0x2000124c
 8008830:	2000132c 	.word	0x2000132c
 8008834:	20001310 	.word	0x20001310
 8008838:	200012e0 	.word	0x200012e0
 800883c:	200012dc 	.word	0x200012dc
 8008840:	20001344 	.word	0x20001344

08008844 <xTimerCreateTimerTask>:
 8008844:	b580      	push	{r7, lr}
 8008846:	b08a      	sub	sp, #40	; 0x28
 8008848:	af04      	add	r7, sp, #16
 800884a:	2300      	movs	r3, #0
 800884c:	617b      	str	r3, [r7, #20]
 800884e:	f000 fae7 	bl	8008e20 <prvCheckForValidListAndQueue>
 8008852:	4b1c      	ldr	r3, [pc, #112]	; (80088c4 <xTimerCreateTimerTask+0x80>)
 8008854:	681b      	ldr	r3, [r3, #0]
 8008856:	2b00      	cmp	r3, #0
 8008858:	d021      	beq.n	800889e <xTimerCreateTimerTask+0x5a>
 800885a:	2300      	movs	r3, #0
 800885c:	60fb      	str	r3, [r7, #12]
 800885e:	2300      	movs	r3, #0
 8008860:	60bb      	str	r3, [r7, #8]
 8008862:	1d3a      	adds	r2, r7, #4
 8008864:	f107 0108 	add.w	r1, r7, #8
 8008868:	f107 030c 	add.w	r3, r7, #12
 800886c:	4618      	mov	r0, r3
 800886e:	f7fb fb5d 	bl	8003f2c <vApplicationGetTimerTaskMemory>
 8008872:	6879      	ldr	r1, [r7, #4]
 8008874:	68bb      	ldr	r3, [r7, #8]
 8008876:	68fa      	ldr	r2, [r7, #12]
 8008878:	9202      	str	r2, [sp, #8]
 800887a:	9301      	str	r3, [sp, #4]
 800887c:	2302      	movs	r3, #2
 800887e:	9300      	str	r3, [sp, #0]
 8008880:	2300      	movs	r3, #0
 8008882:	460a      	mov	r2, r1
 8008884:	4910      	ldr	r1, [pc, #64]	; (80088c8 <xTimerCreateTimerTask+0x84>)
 8008886:	4811      	ldr	r0, [pc, #68]	; (80088cc <xTimerCreateTimerTask+0x88>)
 8008888:	f7fe ff64 	bl	8007754 <xTaskCreateStatic>
 800888c:	4602      	mov	r2, r0
 800888e:	4b10      	ldr	r3, [pc, #64]	; (80088d0 <xTimerCreateTimerTask+0x8c>)
 8008890:	601a      	str	r2, [r3, #0]
 8008892:	4b0f      	ldr	r3, [pc, #60]	; (80088d0 <xTimerCreateTimerTask+0x8c>)
 8008894:	681b      	ldr	r3, [r3, #0]
 8008896:	2b00      	cmp	r3, #0
 8008898:	d001      	beq.n	800889e <xTimerCreateTimerTask+0x5a>
 800889a:	2301      	movs	r3, #1
 800889c:	617b      	str	r3, [r7, #20]
 800889e:	697b      	ldr	r3, [r7, #20]
 80088a0:	2b00      	cmp	r3, #0
 80088a2:	d109      	bne.n	80088b8 <xTimerCreateTimerTask+0x74>
 80088a4:	f04f 0350 	mov.w	r3, #80	; 0x50
 80088a8:	f383 8811 	msr	BASEPRI, r3
 80088ac:	f3bf 8f6f 	isb	sy
 80088b0:	f3bf 8f4f 	dsb	sy
 80088b4:	613b      	str	r3, [r7, #16]
 80088b6:	e7fe      	b.n	80088b6 <xTimerCreateTimerTask+0x72>
 80088b8:	697b      	ldr	r3, [r7, #20]
 80088ba:	4618      	mov	r0, r3
 80088bc:	3718      	adds	r7, #24
 80088be:	46bd      	mov	sp, r7
 80088c0:	bd80      	pop	{r7, pc}
 80088c2:	bf00      	nop
 80088c4:	20001380 	.word	0x20001380
 80088c8:	080098c8 	.word	0x080098c8
 80088cc:	08008a05 	.word	0x08008a05
 80088d0:	20001384 	.word	0x20001384

080088d4 <xTimerGenericCommand>:
 80088d4:	b580      	push	{r7, lr}
 80088d6:	b08a      	sub	sp, #40	; 0x28
 80088d8:	af00      	add	r7, sp, #0
 80088da:	60f8      	str	r0, [r7, #12]
 80088dc:	60b9      	str	r1, [r7, #8]
 80088de:	607a      	str	r2, [r7, #4]
 80088e0:	603b      	str	r3, [r7, #0]
 80088e2:	2300      	movs	r3, #0
 80088e4:	627b      	str	r3, [r7, #36]	; 0x24
 80088e6:	68fb      	ldr	r3, [r7, #12]
 80088e8:	2b00      	cmp	r3, #0
 80088ea:	d109      	bne.n	8008900 <xTimerGenericCommand+0x2c>
 80088ec:	f04f 0350 	mov.w	r3, #80	; 0x50
 80088f0:	f383 8811 	msr	BASEPRI, r3
 80088f4:	f3bf 8f6f 	isb	sy
 80088f8:	f3bf 8f4f 	dsb	sy
 80088fc:	623b      	str	r3, [r7, #32]
 80088fe:	e7fe      	b.n	80088fe <xTimerGenericCommand+0x2a>
 8008900:	4b19      	ldr	r3, [pc, #100]	; (8008968 <xTimerGenericCommand+0x94>)
 8008902:	681b      	ldr	r3, [r3, #0]
 8008904:	2b00      	cmp	r3, #0
 8008906:	d02a      	beq.n	800895e <xTimerGenericCommand+0x8a>
 8008908:	68bb      	ldr	r3, [r7, #8]
 800890a:	617b      	str	r3, [r7, #20]
 800890c:	687b      	ldr	r3, [r7, #4]
 800890e:	61bb      	str	r3, [r7, #24]
 8008910:	68fb      	ldr	r3, [r7, #12]
 8008912:	61fb      	str	r3, [r7, #28]
 8008914:	68bb      	ldr	r3, [r7, #8]
 8008916:	2b05      	cmp	r3, #5
 8008918:	dc18      	bgt.n	800894c <xTimerGenericCommand+0x78>
 800891a:	f7ff fd7f 	bl	800841c <xTaskGetSchedulerState>
 800891e:	4603      	mov	r3, r0
 8008920:	2b02      	cmp	r3, #2
 8008922:	d109      	bne.n	8008938 <xTimerGenericCommand+0x64>
 8008924:	4b10      	ldr	r3, [pc, #64]	; (8008968 <xTimerGenericCommand+0x94>)
 8008926:	6818      	ldr	r0, [r3, #0]
 8008928:	f107 0114 	add.w	r1, r7, #20
 800892c:	2300      	movs	r3, #0
 800892e:	6b3a      	ldr	r2, [r7, #48]	; 0x30
 8008930:	f7fe fa74 	bl	8006e1c <xQueueGenericSend>
 8008934:	6278      	str	r0, [r7, #36]	; 0x24
 8008936:	e012      	b.n	800895e <xTimerGenericCommand+0x8a>
 8008938:	4b0b      	ldr	r3, [pc, #44]	; (8008968 <xTimerGenericCommand+0x94>)
 800893a:	6818      	ldr	r0, [r3, #0]
 800893c:	f107 0114 	add.w	r1, r7, #20
 8008940:	2300      	movs	r3, #0
 8008942:	2200      	movs	r2, #0
 8008944:	f7fe fa6a 	bl	8006e1c <xQueueGenericSend>
 8008948:	6278      	str	r0, [r7, #36]	; 0x24
 800894a:	e008      	b.n	800895e <xTimerGenericCommand+0x8a>
 800894c:	4b06      	ldr	r3, [pc, #24]	; (8008968 <xTimerGenericCommand+0x94>)
 800894e:	6818      	ldr	r0, [r3, #0]
 8008950:	f107 0114 	add.w	r1, r7, #20
 8008954:	2300      	movs	r3, #0
 8008956:	683a      	ldr	r2, [r7, #0]
 8008958:	f7fe fb5a 	bl	8007010 <xQueueGenericSendFromISR>
 800895c:	6278      	str	r0, [r7, #36]	; 0x24
 800895e:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008960:	4618      	mov	r0, r3
 8008962:	3728      	adds	r7, #40	; 0x28
 8008964:	46bd      	mov	sp, r7
 8008966:	bd80      	pop	{r7, pc}
 8008968:	20001380 	.word	0x20001380

0800896c <prvProcessExpiredTimer>:
 800896c:	b580      	push	{r7, lr}
 800896e:	b088      	sub	sp, #32
 8008970:	af02      	add	r7, sp, #8
 8008972:	6078      	str	r0, [r7, #4]
 8008974:	6039      	str	r1, [r7, #0]
 8008976:	4b22      	ldr	r3, [pc, #136]	; (8008a00 <prvProcessExpiredTimer+0x94>)
 8008978:	681b      	ldr	r3, [r3, #0]
 800897a:	68db      	ldr	r3, [r3, #12]
 800897c:	68db      	ldr	r3, [r3, #12]
 800897e:	617b      	str	r3, [r7, #20]
 8008980:	697b      	ldr	r3, [r7, #20]
 8008982:	3304      	adds	r3, #4
 8008984:	4618      	mov	r0, r3
 8008986:	f7fe febb 	bl	8007700 <uxListRemove>
 800898a:	697b      	ldr	r3, [r7, #20]
 800898c:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008990:	f003 0304 	and.w	r3, r3, #4
 8008994:	2b00      	cmp	r3, #0
 8008996:	d021      	beq.n	80089dc <prvProcessExpiredTimer+0x70>
 8008998:	697b      	ldr	r3, [r7, #20]
 800899a:	699a      	ldr	r2, [r3, #24]
 800899c:	687b      	ldr	r3, [r7, #4]
 800899e:	18d1      	adds	r1, r2, r3
 80089a0:	687b      	ldr	r3, [r7, #4]
 80089a2:	683a      	ldr	r2, [r7, #0]
 80089a4:	6978      	ldr	r0, [r7, #20]
 80089a6:	f000 f8d1 	bl	8008b4c <prvInsertTimerInActiveList>
 80089aa:	4603      	mov	r3, r0
 80089ac:	2b00      	cmp	r3, #0
 80089ae:	d01e      	beq.n	80089ee <prvProcessExpiredTimer+0x82>
 80089b0:	2300      	movs	r3, #0
 80089b2:	9300      	str	r3, [sp, #0]
 80089b4:	2300      	movs	r3, #0
 80089b6:	687a      	ldr	r2, [r7, #4]
 80089b8:	2100      	movs	r1, #0
 80089ba:	6978      	ldr	r0, [r7, #20]
 80089bc:	f7ff ff8a 	bl	80088d4 <xTimerGenericCommand>
 80089c0:	6138      	str	r0, [r7, #16]
 80089c2:	693b      	ldr	r3, [r7, #16]
 80089c4:	2b00      	cmp	r3, #0
 80089c6:	d112      	bne.n	80089ee <prvProcessExpiredTimer+0x82>
 80089c8:	f04f 0350 	mov.w	r3, #80	; 0x50
 80089cc:	f383 8811 	msr	BASEPRI, r3
 80089d0:	f3bf 8f6f 	isb	sy
 80089d4:	f3bf 8f4f 	dsb	sy
 80089d8:	60fb      	str	r3, [r7, #12]
 80089da:	e7fe      	b.n	80089da <prvProcessExpiredTimer+0x6e>
 80089dc:	697b      	ldr	r3, [r7, #20]
 80089de:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 80089e2:	f023 0301 	bic.w	r3, r3, #1
 80089e6:	b2da      	uxtb	r2, r3
 80089e8:	697b      	ldr	r3, [r7, #20]
 80089ea:	f883 2028 	strb.w	r2, [r3, #40]	; 0x28
 80089ee:	697b      	ldr	r3, [r7, #20]
 80089f0:	6a1b      	ldr	r3, [r3, #32]
 80089f2:	6978      	ldr	r0, [r7, #20]
 80089f4:	4798      	blx	r3
 80089f6:	bf00      	nop
 80089f8:	3718      	adds	r7, #24
 80089fa:	46bd      	mov	sp, r7
 80089fc:	bd80      	pop	{r7, pc}
 80089fe:	bf00      	nop
 8008a00:	20001378 	.word	0x20001378

08008a04 <prvTimerTask>:
 8008a04:	b580      	push	{r7, lr}
 8008a06:	b084      	sub	sp, #16
 8008a08:	af00      	add	r7, sp, #0
 8008a0a:	6078      	str	r0, [r7, #4]
 8008a0c:	f107 0308 	add.w	r3, r7, #8
 8008a10:	4618      	mov	r0, r3
 8008a12:	f000 f857 	bl	8008ac4 <prvGetNextExpireTime>
 8008a16:	60f8      	str	r0, [r7, #12]
 8008a18:	68bb      	ldr	r3, [r7, #8]
 8008a1a:	4619      	mov	r1, r3
 8008a1c:	68f8      	ldr	r0, [r7, #12]
 8008a1e:	f000 f803 	bl	8008a28 <prvProcessTimerOrBlockTask>
 8008a22:	f000 f8d5 	bl	8008bd0 <prvProcessReceivedCommands>
 8008a26:	e7f1      	b.n	8008a0c <prvTimerTask+0x8>

08008a28 <prvProcessTimerOrBlockTask>:
 8008a28:	b580      	push	{r7, lr}
 8008a2a:	b084      	sub	sp, #16
 8008a2c:	af00      	add	r7, sp, #0
 8008a2e:	6078      	str	r0, [r7, #4]
 8008a30:	6039      	str	r1, [r7, #0]
 8008a32:	f7ff f8d7 	bl	8007be4 <vTaskSuspendAll>
 8008a36:	f107 0308 	add.w	r3, r7, #8
 8008a3a:	4618      	mov	r0, r3
 8008a3c:	f000 f866 	bl	8008b0c <prvSampleTimeNow>
 8008a40:	60f8      	str	r0, [r7, #12]
 8008a42:	68bb      	ldr	r3, [r7, #8]
 8008a44:	2b00      	cmp	r3, #0
 8008a46:	d130      	bne.n	8008aaa <prvProcessTimerOrBlockTask+0x82>
 8008a48:	683b      	ldr	r3, [r7, #0]
 8008a4a:	2b00      	cmp	r3, #0
 8008a4c:	d10a      	bne.n	8008a64 <prvProcessTimerOrBlockTask+0x3c>
 8008a4e:	687a      	ldr	r2, [r7, #4]
 8008a50:	68fb      	ldr	r3, [r7, #12]
 8008a52:	429a      	cmp	r2, r3
 8008a54:	d806      	bhi.n	8008a64 <prvProcessTimerOrBlockTask+0x3c>
 8008a56:	f7ff f8d3 	bl	8007c00 <xTaskResumeAll>
 8008a5a:	68f9      	ldr	r1, [r7, #12]
 8008a5c:	6878      	ldr	r0, [r7, #4]
 8008a5e:	f7ff ff85 	bl	800896c <prvProcessExpiredTimer>
 8008a62:	e024      	b.n	8008aae <prvProcessTimerOrBlockTask+0x86>
 8008a64:	683b      	ldr	r3, [r7, #0]
 8008a66:	2b00      	cmp	r3, #0
 8008a68:	d008      	beq.n	8008a7c <prvProcessTimerOrBlockTask+0x54>
 8008a6a:	4b13      	ldr	r3, [pc, #76]	; (8008ab8 <prvProcessTimerOrBlockTask+0x90>)
 8008a6c:	681b      	ldr	r3, [r3, #0]
 8008a6e:	681b      	ldr	r3, [r3, #0]
 8008a70:	2b00      	cmp	r3, #0
 8008a72:	d101      	bne.n	8008a78 <prvProcessTimerOrBlockTask+0x50>
 8008a74:	2301      	movs	r3, #1
 8008a76:	e000      	b.n	8008a7a <prvProcessTimerOrBlockTask+0x52>
 8008a78:	2300      	movs	r3, #0
 8008a7a:	603b      	str	r3, [r7, #0]
 8008a7c:	4b0f      	ldr	r3, [pc, #60]	; (8008abc <prvProcessTimerOrBlockTask+0x94>)
 8008a7e:	6818      	ldr	r0, [r3, #0]
 8008a80:	687a      	ldr	r2, [r7, #4]
 8008a82:	68fb      	ldr	r3, [r7, #12]
 8008a84:	1ad3      	subs	r3, r2, r3
 8008a86:	683a      	ldr	r2, [r7, #0]
 8008a88:	4619      	mov	r1, r3
 8008a8a:	f7fe fd7b 	bl	8007584 <vQueueWaitForMessageRestricted>
 8008a8e:	f7ff f8b7 	bl	8007c00 <xTaskResumeAll>
 8008a92:	4603      	mov	r3, r0
 8008a94:	2b00      	cmp	r3, #0
 8008a96:	d10a      	bne.n	8008aae <prvProcessTimerOrBlockTask+0x86>
 8008a98:	4b09      	ldr	r3, [pc, #36]	; (8008ac0 <prvProcessTimerOrBlockTask+0x98>)
 8008a9a:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8008a9e:	601a      	str	r2, [r3, #0]
 8008aa0:	f3bf 8f4f 	dsb	sy
 8008aa4:	f3bf 8f6f 	isb	sy
 8008aa8:	e001      	b.n	8008aae <prvProcessTimerOrBlockTask+0x86>
 8008aaa:	f7ff f8a9 	bl	8007c00 <xTaskResumeAll>
 8008aae:	bf00      	nop
 8008ab0:	3710      	adds	r7, #16
 8008ab2:	46bd      	mov	sp, r7
 8008ab4:	bd80      	pop	{r7, pc}
 8008ab6:	bf00      	nop
 8008ab8:	2000137c 	.word	0x2000137c
 8008abc:	20001380 	.word	0x20001380
 8008ac0:	e000ed04 	.word	0xe000ed04

08008ac4 <prvGetNextExpireTime>:
 8008ac4:	b480      	push	{r7}
 8008ac6:	b085      	sub	sp, #20
 8008ac8:	af00      	add	r7, sp, #0
 8008aca:	6078      	str	r0, [r7, #4]
 8008acc:	4b0e      	ldr	r3, [pc, #56]	; (8008b08 <prvGetNextExpireTime+0x44>)
 8008ace:	681b      	ldr	r3, [r3, #0]
 8008ad0:	681b      	ldr	r3, [r3, #0]
 8008ad2:	2b00      	cmp	r3, #0
 8008ad4:	d101      	bne.n	8008ada <prvGetNextExpireTime+0x16>
 8008ad6:	2201      	movs	r2, #1
 8008ad8:	e000      	b.n	8008adc <prvGetNextExpireTime+0x18>
 8008ada:	2200      	movs	r2, #0
 8008adc:	687b      	ldr	r3, [r7, #4]
 8008ade:	601a      	str	r2, [r3, #0]
 8008ae0:	687b      	ldr	r3, [r7, #4]
 8008ae2:	681b      	ldr	r3, [r3, #0]
 8008ae4:	2b00      	cmp	r3, #0
 8008ae6:	d105      	bne.n	8008af4 <prvGetNextExpireTime+0x30>
 8008ae8:	4b07      	ldr	r3, [pc, #28]	; (8008b08 <prvGetNextExpireTime+0x44>)
 8008aea:	681b      	ldr	r3, [r3, #0]
 8008aec:	68db      	ldr	r3, [r3, #12]
 8008aee:	681b      	ldr	r3, [r3, #0]
 8008af0:	60fb      	str	r3, [r7, #12]
 8008af2:	e001      	b.n	8008af8 <prvGetNextExpireTime+0x34>
 8008af4:	2300      	movs	r3, #0
 8008af6:	60fb      	str	r3, [r7, #12]
 8008af8:	68fb      	ldr	r3, [r7, #12]
 8008afa:	4618      	mov	r0, r3
 8008afc:	3714      	adds	r7, #20
 8008afe:	46bd      	mov	sp, r7
 8008b00:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008b04:	4770      	bx	lr
 8008b06:	bf00      	nop
 8008b08:	20001378 	.word	0x20001378

08008b0c <prvSampleTimeNow>:
 8008b0c:	b580      	push	{r7, lr}
 8008b0e:	b084      	sub	sp, #16
 8008b10:	af00      	add	r7, sp, #0
 8008b12:	6078      	str	r0, [r7, #4]
 8008b14:	f7ff f910 	bl	8007d38 <xTaskGetTickCount>
 8008b18:	60f8      	str	r0, [r7, #12]
 8008b1a:	4b0b      	ldr	r3, [pc, #44]	; (8008b48 <prvSampleTimeNow+0x3c>)
 8008b1c:	681b      	ldr	r3, [r3, #0]
 8008b1e:	68fa      	ldr	r2, [r7, #12]
 8008b20:	429a      	cmp	r2, r3
 8008b22:	d205      	bcs.n	8008b30 <prvSampleTimeNow+0x24>
 8008b24:	f000 f918 	bl	8008d58 <prvSwitchTimerLists>
 8008b28:	687b      	ldr	r3, [r7, #4]
 8008b2a:	2201      	movs	r2, #1
 8008b2c:	601a      	str	r2, [r3, #0]
 8008b2e:	e002      	b.n	8008b36 <prvSampleTimeNow+0x2a>
 8008b30:	687b      	ldr	r3, [r7, #4]
 8008b32:	2200      	movs	r2, #0
 8008b34:	601a      	str	r2, [r3, #0]
 8008b36:	4a04      	ldr	r2, [pc, #16]	; (8008b48 <prvSampleTimeNow+0x3c>)
 8008b38:	68fb      	ldr	r3, [r7, #12]
 8008b3a:	6013      	str	r3, [r2, #0]
 8008b3c:	68fb      	ldr	r3, [r7, #12]
 8008b3e:	4618      	mov	r0, r3
 8008b40:	3710      	adds	r7, #16
 8008b42:	46bd      	mov	sp, r7
 8008b44:	bd80      	pop	{r7, pc}
 8008b46:	bf00      	nop
 8008b48:	20001388 	.word	0x20001388

08008b4c <prvInsertTimerInActiveList>:
 8008b4c:	b580      	push	{r7, lr}
 8008b4e:	b086      	sub	sp, #24
 8008b50:	af00      	add	r7, sp, #0
 8008b52:	60f8      	str	r0, [r7, #12]
 8008b54:	60b9      	str	r1, [r7, #8]
 8008b56:	607a      	str	r2, [r7, #4]
 8008b58:	603b      	str	r3, [r7, #0]
 8008b5a:	2300      	movs	r3, #0
 8008b5c:	617b      	str	r3, [r7, #20]
 8008b5e:	68fb      	ldr	r3, [r7, #12]
 8008b60:	68ba      	ldr	r2, [r7, #8]
 8008b62:	605a      	str	r2, [r3, #4]
 8008b64:	68fb      	ldr	r3, [r7, #12]
 8008b66:	68fa      	ldr	r2, [r7, #12]
 8008b68:	611a      	str	r2, [r3, #16]
 8008b6a:	68ba      	ldr	r2, [r7, #8]
 8008b6c:	687b      	ldr	r3, [r7, #4]
 8008b6e:	429a      	cmp	r2, r3
 8008b70:	d812      	bhi.n	8008b98 <prvInsertTimerInActiveList+0x4c>
 8008b72:	687a      	ldr	r2, [r7, #4]
 8008b74:	683b      	ldr	r3, [r7, #0]
 8008b76:	1ad2      	subs	r2, r2, r3
 8008b78:	68fb      	ldr	r3, [r7, #12]
 8008b7a:	699b      	ldr	r3, [r3, #24]
 8008b7c:	429a      	cmp	r2, r3
 8008b7e:	d302      	bcc.n	8008b86 <prvInsertTimerInActiveList+0x3a>
 8008b80:	2301      	movs	r3, #1
 8008b82:	617b      	str	r3, [r7, #20]
 8008b84:	e01b      	b.n	8008bbe <prvInsertTimerInActiveList+0x72>
 8008b86:	4b10      	ldr	r3, [pc, #64]	; (8008bc8 <prvInsertTimerInActiveList+0x7c>)
 8008b88:	681a      	ldr	r2, [r3, #0]
 8008b8a:	68fb      	ldr	r3, [r7, #12]
 8008b8c:	3304      	adds	r3, #4
 8008b8e:	4619      	mov	r1, r3
 8008b90:	4610      	mov	r0, r2
 8008b92:	f7fe fd7c 	bl	800768e <vListInsert>
 8008b96:	e012      	b.n	8008bbe <prvInsertTimerInActiveList+0x72>
 8008b98:	687a      	ldr	r2, [r7, #4]
 8008b9a:	683b      	ldr	r3, [r7, #0]
 8008b9c:	429a      	cmp	r2, r3
 8008b9e:	d206      	bcs.n	8008bae <prvInsertTimerInActiveList+0x62>
 8008ba0:	68ba      	ldr	r2, [r7, #8]
 8008ba2:	683b      	ldr	r3, [r7, #0]
 8008ba4:	429a      	cmp	r2, r3
 8008ba6:	d302      	bcc.n	8008bae <prvInsertTimerInActiveList+0x62>
 8008ba8:	2301      	movs	r3, #1
 8008baa:	617b      	str	r3, [r7, #20]
 8008bac:	e007      	b.n	8008bbe <prvInsertTimerInActiveList+0x72>
 8008bae:	4b07      	ldr	r3, [pc, #28]	; (8008bcc <prvInsertTimerInActiveList+0x80>)
 8008bb0:	681a      	ldr	r2, [r3, #0]
 8008bb2:	68fb      	ldr	r3, [r7, #12]
 8008bb4:	3304      	adds	r3, #4
 8008bb6:	4619      	mov	r1, r3
 8008bb8:	4610      	mov	r0, r2
 8008bba:	f7fe fd68 	bl	800768e <vListInsert>
 8008bbe:	697b      	ldr	r3, [r7, #20]
 8008bc0:	4618      	mov	r0, r3
 8008bc2:	3718      	adds	r7, #24
 8008bc4:	46bd      	mov	sp, r7
 8008bc6:	bd80      	pop	{r7, pc}
 8008bc8:	2000137c 	.word	0x2000137c
 8008bcc:	20001378 	.word	0x20001378

08008bd0 <prvProcessReceivedCommands>:
 8008bd0:	b580      	push	{r7, lr}
 8008bd2:	b08c      	sub	sp, #48	; 0x30
 8008bd4:	af02      	add	r7, sp, #8
 8008bd6:	e0ac      	b.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008bd8:	68bb      	ldr	r3, [r7, #8]
 8008bda:	2b00      	cmp	r3, #0
 8008bdc:	f2c0 80a9 	blt.w	8008d32 <prvProcessReceivedCommands+0x162>
 8008be0:	693b      	ldr	r3, [r7, #16]
 8008be2:	627b      	str	r3, [r7, #36]	; 0x24
 8008be4:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008be6:	695b      	ldr	r3, [r3, #20]
 8008be8:	2b00      	cmp	r3, #0
 8008bea:	d004      	beq.n	8008bf6 <prvProcessReceivedCommands+0x26>
 8008bec:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008bee:	3304      	adds	r3, #4
 8008bf0:	4618      	mov	r0, r3
 8008bf2:	f7fe fd85 	bl	8007700 <uxListRemove>
 8008bf6:	1d3b      	adds	r3, r7, #4
 8008bf8:	4618      	mov	r0, r3
 8008bfa:	f7ff ff87 	bl	8008b0c <prvSampleTimeNow>
 8008bfe:	6238      	str	r0, [r7, #32]
 8008c00:	68bb      	ldr	r3, [r7, #8]
 8008c02:	2b09      	cmp	r3, #9
 8008c04:	f200 8094 	bhi.w	8008d30 <prvProcessReceivedCommands+0x160>
 8008c08:	a201      	add	r2, pc, #4	; (adr r2, 8008c10 <prvProcessReceivedCommands+0x40>)
 8008c0a:	f852 f023 	ldr.w	pc, [r2, r3, lsl #2]
 8008c0e:	bf00      	nop
 8008c10:	08008c39 	.word	0x08008c39
 8008c14:	08008c39 	.word	0x08008c39
 8008c18:	08008c39 	.word	0x08008c39
 8008c1c:	08008cab 	.word	0x08008cab
 8008c20:	08008cbf 	.word	0x08008cbf
 8008c24:	08008d07 	.word	0x08008d07
 8008c28:	08008c39 	.word	0x08008c39
 8008c2c:	08008c39 	.word	0x08008c39
 8008c30:	08008cab 	.word	0x08008cab
 8008c34:	08008cbf 	.word	0x08008cbf
 8008c38:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008c3a:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008c3e:	f043 0301 	orr.w	r3, r3, #1
 8008c42:	b2da      	uxtb	r2, r3
 8008c44:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008c46:	f883 2028 	strb.w	r2, [r3, #40]	; 0x28
 8008c4a:	68fa      	ldr	r2, [r7, #12]
 8008c4c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008c4e:	699b      	ldr	r3, [r3, #24]
 8008c50:	18d1      	adds	r1, r2, r3
 8008c52:	68fb      	ldr	r3, [r7, #12]
 8008c54:	6a3a      	ldr	r2, [r7, #32]
 8008c56:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8008c58:	f7ff ff78 	bl	8008b4c <prvInsertTimerInActiveList>
 8008c5c:	4603      	mov	r3, r0
 8008c5e:	2b00      	cmp	r3, #0
 8008c60:	d067      	beq.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008c62:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008c64:	6a1b      	ldr	r3, [r3, #32]
 8008c66:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8008c68:	4798      	blx	r3
 8008c6a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008c6c:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008c70:	f003 0304 	and.w	r3, r3, #4
 8008c74:	2b00      	cmp	r3, #0
 8008c76:	d05c      	beq.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008c78:	68fa      	ldr	r2, [r7, #12]
 8008c7a:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008c7c:	699b      	ldr	r3, [r3, #24]
 8008c7e:	441a      	add	r2, r3
 8008c80:	2300      	movs	r3, #0
 8008c82:	9300      	str	r3, [sp, #0]
 8008c84:	2300      	movs	r3, #0
 8008c86:	2100      	movs	r1, #0
 8008c88:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8008c8a:	f7ff fe23 	bl	80088d4 <xTimerGenericCommand>
 8008c8e:	61f8      	str	r0, [r7, #28]
 8008c90:	69fb      	ldr	r3, [r7, #28]
 8008c92:	2b00      	cmp	r3, #0
 8008c94:	d14d      	bne.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008c96:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008c9a:	f383 8811 	msr	BASEPRI, r3
 8008c9e:	f3bf 8f6f 	isb	sy
 8008ca2:	f3bf 8f4f 	dsb	sy
 8008ca6:	61bb      	str	r3, [r7, #24]
 8008ca8:	e7fe      	b.n	8008ca8 <prvProcessReceivedCommands+0xd8>
 8008caa:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008cac:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008cb0:	f023 0301 	bic.w	r3, r3, #1
 8008cb4:	b2da      	uxtb	r2, r3
 8008cb6:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008cb8:	f883 2028 	strb.w	r2, [r3, #40]	; 0x28
 8008cbc:	e039      	b.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008cbe:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008cc0:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008cc4:	f043 0301 	orr.w	r3, r3, #1
 8008cc8:	b2da      	uxtb	r2, r3
 8008cca:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008ccc:	f883 2028 	strb.w	r2, [r3, #40]	; 0x28
 8008cd0:	68fa      	ldr	r2, [r7, #12]
 8008cd2:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008cd4:	619a      	str	r2, [r3, #24]
 8008cd6:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008cd8:	699b      	ldr	r3, [r3, #24]
 8008cda:	2b00      	cmp	r3, #0
 8008cdc:	d109      	bne.n	8008cf2 <prvProcessReceivedCommands+0x122>
 8008cde:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008ce2:	f383 8811 	msr	BASEPRI, r3
 8008ce6:	f3bf 8f6f 	isb	sy
 8008cea:	f3bf 8f4f 	dsb	sy
 8008cee:	617b      	str	r3, [r7, #20]
 8008cf0:	e7fe      	b.n	8008cf0 <prvProcessReceivedCommands+0x120>
 8008cf2:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008cf4:	699a      	ldr	r2, [r3, #24]
 8008cf6:	6a3b      	ldr	r3, [r7, #32]
 8008cf8:	18d1      	adds	r1, r2, r3
 8008cfa:	6a3b      	ldr	r3, [r7, #32]
 8008cfc:	6a3a      	ldr	r2, [r7, #32]
 8008cfe:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8008d00:	f7ff ff24 	bl	8008b4c <prvInsertTimerInActiveList>
 8008d04:	e015      	b.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008d06:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008d08:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008d0c:	f003 0302 	and.w	r3, r3, #2
 8008d10:	2b00      	cmp	r3, #0
 8008d12:	d103      	bne.n	8008d1c <prvProcessReceivedCommands+0x14c>
 8008d14:	6a78      	ldr	r0, [r7, #36]	; 0x24
 8008d16:	f000 fb05 	bl	8009324 <vPortFree>
 8008d1a:	e00a      	b.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008d1c:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008d1e:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008d22:	f023 0301 	bic.w	r3, r3, #1
 8008d26:	b2da      	uxtb	r2, r3
 8008d28:	6a7b      	ldr	r3, [r7, #36]	; 0x24
 8008d2a:	f883 2028 	strb.w	r2, [r3, #40]	; 0x28
 8008d2e:	e000      	b.n	8008d32 <prvProcessReceivedCommands+0x162>
 8008d30:	bf00      	nop
 8008d32:	4b08      	ldr	r3, [pc, #32]	; (8008d54 <prvProcessReceivedCommands+0x184>)
 8008d34:	681b      	ldr	r3, [r3, #0]
 8008d36:	f107 0108 	add.w	r1, r7, #8
 8008d3a:	2200      	movs	r2, #0
 8008d3c:	4618      	mov	r0, r3
 8008d3e:	f7fe fa0d 	bl	800715c <xQueueReceive>
 8008d42:	4603      	mov	r3, r0
 8008d44:	2b00      	cmp	r3, #0
 8008d46:	f47f af47 	bne.w	8008bd8 <prvProcessReceivedCommands+0x8>
 8008d4a:	bf00      	nop
 8008d4c:	3728      	adds	r7, #40	; 0x28
 8008d4e:	46bd      	mov	sp, r7
 8008d50:	bd80      	pop	{r7, pc}
 8008d52:	bf00      	nop
 8008d54:	20001380 	.word	0x20001380

08008d58 <prvSwitchTimerLists>:
 8008d58:	b580      	push	{r7, lr}
 8008d5a:	b088      	sub	sp, #32
 8008d5c:	af02      	add	r7, sp, #8
 8008d5e:	e047      	b.n	8008df0 <prvSwitchTimerLists+0x98>
 8008d60:	4b2d      	ldr	r3, [pc, #180]	; (8008e18 <prvSwitchTimerLists+0xc0>)
 8008d62:	681b      	ldr	r3, [r3, #0]
 8008d64:	68db      	ldr	r3, [r3, #12]
 8008d66:	681b      	ldr	r3, [r3, #0]
 8008d68:	617b      	str	r3, [r7, #20]
 8008d6a:	4b2b      	ldr	r3, [pc, #172]	; (8008e18 <prvSwitchTimerLists+0xc0>)
 8008d6c:	681b      	ldr	r3, [r3, #0]
 8008d6e:	68db      	ldr	r3, [r3, #12]
 8008d70:	68db      	ldr	r3, [r3, #12]
 8008d72:	613b      	str	r3, [r7, #16]
 8008d74:	693b      	ldr	r3, [r7, #16]
 8008d76:	3304      	adds	r3, #4
 8008d78:	4618      	mov	r0, r3
 8008d7a:	f7fe fcc1 	bl	8007700 <uxListRemove>
 8008d7e:	693b      	ldr	r3, [r7, #16]
 8008d80:	6a1b      	ldr	r3, [r3, #32]
 8008d82:	6938      	ldr	r0, [r7, #16]
 8008d84:	4798      	blx	r3
 8008d86:	693b      	ldr	r3, [r7, #16]
 8008d88:	f893 3028 	ldrb.w	r3, [r3, #40]	; 0x28
 8008d8c:	f003 0304 	and.w	r3, r3, #4
 8008d90:	2b00      	cmp	r3, #0
 8008d92:	d02d      	beq.n	8008df0 <prvSwitchTimerLists+0x98>
 8008d94:	693b      	ldr	r3, [r7, #16]
 8008d96:	699a      	ldr	r2, [r3, #24]
 8008d98:	697b      	ldr	r3, [r7, #20]
 8008d9a:	4413      	add	r3, r2
 8008d9c:	60fb      	str	r3, [r7, #12]
 8008d9e:	68fa      	ldr	r2, [r7, #12]
 8008da0:	697b      	ldr	r3, [r7, #20]
 8008da2:	429a      	cmp	r2, r3
 8008da4:	d90e      	bls.n	8008dc4 <prvSwitchTimerLists+0x6c>
 8008da6:	693b      	ldr	r3, [r7, #16]
 8008da8:	68fa      	ldr	r2, [r7, #12]
 8008daa:	605a      	str	r2, [r3, #4]
 8008dac:	693b      	ldr	r3, [r7, #16]
 8008dae:	693a      	ldr	r2, [r7, #16]
 8008db0:	611a      	str	r2, [r3, #16]
 8008db2:	4b19      	ldr	r3, [pc, #100]	; (8008e18 <prvSwitchTimerLists+0xc0>)
 8008db4:	681a      	ldr	r2, [r3, #0]
 8008db6:	693b      	ldr	r3, [r7, #16]
 8008db8:	3304      	adds	r3, #4
 8008dba:	4619      	mov	r1, r3
 8008dbc:	4610      	mov	r0, r2
 8008dbe:	f7fe fc66 	bl	800768e <vListInsert>
 8008dc2:	e015      	b.n	8008df0 <prvSwitchTimerLists+0x98>
 8008dc4:	2300      	movs	r3, #0
 8008dc6:	9300      	str	r3, [sp, #0]
 8008dc8:	2300      	movs	r3, #0
 8008dca:	697a      	ldr	r2, [r7, #20]
 8008dcc:	2100      	movs	r1, #0
 8008dce:	6938      	ldr	r0, [r7, #16]
 8008dd0:	f7ff fd80 	bl	80088d4 <xTimerGenericCommand>
 8008dd4:	60b8      	str	r0, [r7, #8]
 8008dd6:	68bb      	ldr	r3, [r7, #8]
 8008dd8:	2b00      	cmp	r3, #0
 8008dda:	d109      	bne.n	8008df0 <prvSwitchTimerLists+0x98>
 8008ddc:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008de0:	f383 8811 	msr	BASEPRI, r3
 8008de4:	f3bf 8f6f 	isb	sy
 8008de8:	f3bf 8f4f 	dsb	sy
 8008dec:	603b      	str	r3, [r7, #0]
 8008dee:	e7fe      	b.n	8008dee <prvSwitchTimerLists+0x96>
 8008df0:	4b09      	ldr	r3, [pc, #36]	; (8008e18 <prvSwitchTimerLists+0xc0>)
 8008df2:	681b      	ldr	r3, [r3, #0]
 8008df4:	681b      	ldr	r3, [r3, #0]
 8008df6:	2b00      	cmp	r3, #0
 8008df8:	d1b2      	bne.n	8008d60 <prvSwitchTimerLists+0x8>
 8008dfa:	4b07      	ldr	r3, [pc, #28]	; (8008e18 <prvSwitchTimerLists+0xc0>)
 8008dfc:	681b      	ldr	r3, [r3, #0]
 8008dfe:	607b      	str	r3, [r7, #4]
 8008e00:	4b06      	ldr	r3, [pc, #24]	; (8008e1c <prvSwitchTimerLists+0xc4>)
 8008e02:	681b      	ldr	r3, [r3, #0]
 8008e04:	4a04      	ldr	r2, [pc, #16]	; (8008e18 <prvSwitchTimerLists+0xc0>)
 8008e06:	6013      	str	r3, [r2, #0]
 8008e08:	4a04      	ldr	r2, [pc, #16]	; (8008e1c <prvSwitchTimerLists+0xc4>)
 8008e0a:	687b      	ldr	r3, [r7, #4]
 8008e0c:	6013      	str	r3, [r2, #0]
 8008e0e:	bf00      	nop
 8008e10:	3718      	adds	r7, #24
 8008e12:	46bd      	mov	sp, r7
 8008e14:	bd80      	pop	{r7, pc}
 8008e16:	bf00      	nop
 8008e18:	20001378 	.word	0x20001378
 8008e1c:	2000137c 	.word	0x2000137c

08008e20 <prvCheckForValidListAndQueue>:
 8008e20:	b580      	push	{r7, lr}
 8008e22:	b082      	sub	sp, #8
 8008e24:	af02      	add	r7, sp, #8
 8008e26:	f000 f963 	bl	80090f0 <vPortEnterCritical>
 8008e2a:	4b15      	ldr	r3, [pc, #84]	; (8008e80 <prvCheckForValidListAndQueue+0x60>)
 8008e2c:	681b      	ldr	r3, [r3, #0]
 8008e2e:	2b00      	cmp	r3, #0
 8008e30:	d120      	bne.n	8008e74 <prvCheckForValidListAndQueue+0x54>
 8008e32:	4814      	ldr	r0, [pc, #80]	; (8008e84 <prvCheckForValidListAndQueue+0x64>)
 8008e34:	f7fe fbda 	bl	80075ec <vListInitialise>
 8008e38:	4813      	ldr	r0, [pc, #76]	; (8008e88 <prvCheckForValidListAndQueue+0x68>)
 8008e3a:	f7fe fbd7 	bl	80075ec <vListInitialise>
 8008e3e:	4b13      	ldr	r3, [pc, #76]	; (8008e8c <prvCheckForValidListAndQueue+0x6c>)
 8008e40:	4a10      	ldr	r2, [pc, #64]	; (8008e84 <prvCheckForValidListAndQueue+0x64>)
 8008e42:	601a      	str	r2, [r3, #0]
 8008e44:	4b12      	ldr	r3, [pc, #72]	; (8008e90 <prvCheckForValidListAndQueue+0x70>)
 8008e46:	4a10      	ldr	r2, [pc, #64]	; (8008e88 <prvCheckForValidListAndQueue+0x68>)
 8008e48:	601a      	str	r2, [r3, #0]
 8008e4a:	2300      	movs	r3, #0
 8008e4c:	9300      	str	r3, [sp, #0]
 8008e4e:	4b11      	ldr	r3, [pc, #68]	; (8008e94 <prvCheckForValidListAndQueue+0x74>)
 8008e50:	4a11      	ldr	r2, [pc, #68]	; (8008e98 <prvCheckForValidListAndQueue+0x78>)
 8008e52:	210c      	movs	r1, #12
 8008e54:	200a      	movs	r0, #10
 8008e56:	f7fd ff4b 	bl	8006cf0 <xQueueGenericCreateStatic>
 8008e5a:	4602      	mov	r2, r0
 8008e5c:	4b08      	ldr	r3, [pc, #32]	; (8008e80 <prvCheckForValidListAndQueue+0x60>)
 8008e5e:	601a      	str	r2, [r3, #0]
 8008e60:	4b07      	ldr	r3, [pc, #28]	; (8008e80 <prvCheckForValidListAndQueue+0x60>)
 8008e62:	681b      	ldr	r3, [r3, #0]
 8008e64:	2b00      	cmp	r3, #0
 8008e66:	d005      	beq.n	8008e74 <prvCheckForValidListAndQueue+0x54>
 8008e68:	4b05      	ldr	r3, [pc, #20]	; (8008e80 <prvCheckForValidListAndQueue+0x60>)
 8008e6a:	681b      	ldr	r3, [r3, #0]
 8008e6c:	490b      	ldr	r1, [pc, #44]	; (8008e9c <prvCheckForValidListAndQueue+0x7c>)
 8008e6e:	4618      	mov	r0, r3
 8008e70:	f7fe fb60 	bl	8007534 <vQueueAddToRegistry>
 8008e74:	f000 f96a 	bl	800914c <vPortExitCritical>
 8008e78:	bf00      	nop
 8008e7a:	46bd      	mov	sp, r7
 8008e7c:	bd80      	pop	{r7, pc}
 8008e7e:	bf00      	nop
 8008e80:	20001380 	.word	0x20001380
 8008e84:	20001350 	.word	0x20001350
 8008e88:	20001364 	.word	0x20001364
 8008e8c:	20001378 	.word	0x20001378
 8008e90:	2000137c 	.word	0x2000137c
 8008e94:	20001404 	.word	0x20001404
 8008e98:	2000138c 	.word	0x2000138c
 8008e9c:	080098d0 	.word	0x080098d0

08008ea0 <pxPortInitialiseStack>:
 8008ea0:	b480      	push	{r7}
 8008ea2:	b085      	sub	sp, #20
 8008ea4:	af00      	add	r7, sp, #0
 8008ea6:	60f8      	str	r0, [r7, #12]
 8008ea8:	60b9      	str	r1, [r7, #8]
 8008eaa:	607a      	str	r2, [r7, #4]
 8008eac:	68fb      	ldr	r3, [r7, #12]
 8008eae:	3b04      	subs	r3, #4
 8008eb0:	60fb      	str	r3, [r7, #12]
 8008eb2:	68fb      	ldr	r3, [r7, #12]
 8008eb4:	f04f 7280 	mov.w	r2, #16777216	; 0x1000000
 8008eb8:	601a      	str	r2, [r3, #0]
 8008eba:	68fb      	ldr	r3, [r7, #12]
 8008ebc:	3b04      	subs	r3, #4
 8008ebe:	60fb      	str	r3, [r7, #12]
 8008ec0:	68bb      	ldr	r3, [r7, #8]
 8008ec2:	f023 0201 	bic.w	r2, r3, #1
 8008ec6:	68fb      	ldr	r3, [r7, #12]
 8008ec8:	601a      	str	r2, [r3, #0]
 8008eca:	68fb      	ldr	r3, [r7, #12]
 8008ecc:	3b04      	subs	r3, #4
 8008ece:	60fb      	str	r3, [r7, #12]
 8008ed0:	4a0c      	ldr	r2, [pc, #48]	; (8008f04 <pxPortInitialiseStack+0x64>)
 8008ed2:	68fb      	ldr	r3, [r7, #12]
 8008ed4:	601a      	str	r2, [r3, #0]
 8008ed6:	68fb      	ldr	r3, [r7, #12]
 8008ed8:	3b14      	subs	r3, #20
 8008eda:	60fb      	str	r3, [r7, #12]
 8008edc:	687a      	ldr	r2, [r7, #4]
 8008ede:	68fb      	ldr	r3, [r7, #12]
 8008ee0:	601a      	str	r2, [r3, #0]
 8008ee2:	68fb      	ldr	r3, [r7, #12]
 8008ee4:	3b04      	subs	r3, #4
 8008ee6:	60fb      	str	r3, [r7, #12]
 8008ee8:	68fb      	ldr	r3, [r7, #12]
 8008eea:	f06f 0202 	mvn.w	r2, #2
 8008eee:	601a      	str	r2, [r3, #0]
 8008ef0:	68fb      	ldr	r3, [r7, #12]
 8008ef2:	3b20      	subs	r3, #32
 8008ef4:	60fb      	str	r3, [r7, #12]
 8008ef6:	68fb      	ldr	r3, [r7, #12]
 8008ef8:	4618      	mov	r0, r3
 8008efa:	3714      	adds	r7, #20
 8008efc:	46bd      	mov	sp, r7
 8008efe:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008f02:	4770      	bx	lr
 8008f04:	08008f09 	.word	0x08008f09

08008f08 <prvTaskExitError>:
 8008f08:	b480      	push	{r7}
 8008f0a:	b085      	sub	sp, #20
 8008f0c:	af00      	add	r7, sp, #0
 8008f0e:	2300      	movs	r3, #0
 8008f10:	607b      	str	r3, [r7, #4]
 8008f12:	4b11      	ldr	r3, [pc, #68]	; (8008f58 <prvTaskExitError+0x50>)
 8008f14:	681b      	ldr	r3, [r3, #0]
 8008f16:	f1b3 3fff 	cmp.w	r3, #4294967295
 8008f1a:	d009      	beq.n	8008f30 <prvTaskExitError+0x28>
 8008f1c:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008f20:	f383 8811 	msr	BASEPRI, r3
 8008f24:	f3bf 8f6f 	isb	sy
 8008f28:	f3bf 8f4f 	dsb	sy
 8008f2c:	60fb      	str	r3, [r7, #12]
 8008f2e:	e7fe      	b.n	8008f2e <prvTaskExitError+0x26>
 8008f30:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008f34:	f383 8811 	msr	BASEPRI, r3
 8008f38:	f3bf 8f6f 	isb	sy
 8008f3c:	f3bf 8f4f 	dsb	sy
 8008f40:	60bb      	str	r3, [r7, #8]
 8008f42:	bf00      	nop
 8008f44:	687b      	ldr	r3, [r7, #4]
 8008f46:	2b00      	cmp	r3, #0
 8008f48:	d0fc      	beq.n	8008f44 <prvTaskExitError+0x3c>
 8008f4a:	bf00      	nop
 8008f4c:	3714      	adds	r7, #20
 8008f4e:	46bd      	mov	sp, r7
 8008f50:	f85d 7b04 	ldr.w	r7, [sp], #4
 8008f54:	4770      	bx	lr
 8008f56:	bf00      	nop
 8008f58:	200008ac 	.word	0x200008ac
 8008f5c:	00000000 	.word	0x00000000

08008f60 <SVC_Handler>:
 8008f60:	4b07      	ldr	r3, [pc, #28]	; (8008f80 <pxCurrentTCBConst2>)
 8008f62:	6819      	ldr	r1, [r3, #0]
 8008f64:	6808      	ldr	r0, [r1, #0]
 8008f66:	e8b0 4ff0 	ldmia.w	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8008f6a:	f380 8809 	msr	PSP, r0
 8008f6e:	f3bf 8f6f 	isb	sy
 8008f72:	f04f 0000 	mov.w	r0, #0
 8008f76:	f380 8811 	msr	BASEPRI, r0
 8008f7a:	4770      	bx	lr
 8008f7c:	f3af 8000 	nop.w

08008f80 <pxCurrentTCBConst2>:
 8008f80:	2000124c 	.word	0x2000124c
 8008f84:	bf00      	nop
 8008f86:	bf00      	nop

08008f88 <prvPortStartFirstTask>:
 8008f88:	4808      	ldr	r0, [pc, #32]	; (8008fac <prvPortStartFirstTask+0x24>)
 8008f8a:	6800      	ldr	r0, [r0, #0]
 8008f8c:	6800      	ldr	r0, [r0, #0]
 8008f8e:	f380 8808 	msr	MSP, r0
 8008f92:	f04f 0000 	mov.w	r0, #0
 8008f96:	f380 8814 	msr	CONTROL, r0
 8008f9a:	b662      	cpsie	i
 8008f9c:	b661      	cpsie	f
 8008f9e:	f3bf 8f4f 	dsb	sy
 8008fa2:	f3bf 8f6f 	isb	sy
 8008fa6:	df00      	svc	0
 8008fa8:	bf00      	nop
 8008faa:	0000      	.short	0x0000
 8008fac:	e000ed08 	.word	0xe000ed08
 8008fb0:	bf00      	nop
 8008fb2:	bf00      	nop

08008fb4 <xPortStartScheduler>:
 8008fb4:	b580      	push	{r7, lr}
 8008fb6:	b086      	sub	sp, #24
 8008fb8:	af00      	add	r7, sp, #0
 8008fba:	4b44      	ldr	r3, [pc, #272]	; (80090cc <xPortStartScheduler+0x118>)
 8008fbc:	681b      	ldr	r3, [r3, #0]
 8008fbe:	4a44      	ldr	r2, [pc, #272]	; (80090d0 <xPortStartScheduler+0x11c>)
 8008fc0:	4293      	cmp	r3, r2
 8008fc2:	d109      	bne.n	8008fd8 <xPortStartScheduler+0x24>
 8008fc4:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008fc8:	f383 8811 	msr	BASEPRI, r3
 8008fcc:	f3bf 8f6f 	isb	sy
 8008fd0:	f3bf 8f4f 	dsb	sy
 8008fd4:	613b      	str	r3, [r7, #16]
 8008fd6:	e7fe      	b.n	8008fd6 <xPortStartScheduler+0x22>
 8008fd8:	4b3c      	ldr	r3, [pc, #240]	; (80090cc <xPortStartScheduler+0x118>)
 8008fda:	681b      	ldr	r3, [r3, #0]
 8008fdc:	4a3d      	ldr	r2, [pc, #244]	; (80090d4 <xPortStartScheduler+0x120>)
 8008fde:	4293      	cmp	r3, r2
 8008fe0:	d109      	bne.n	8008ff6 <xPortStartScheduler+0x42>
 8008fe2:	f04f 0350 	mov.w	r3, #80	; 0x50
 8008fe6:	f383 8811 	msr	BASEPRI, r3
 8008fea:	f3bf 8f6f 	isb	sy
 8008fee:	f3bf 8f4f 	dsb	sy
 8008ff2:	60fb      	str	r3, [r7, #12]
 8008ff4:	e7fe      	b.n	8008ff4 <xPortStartScheduler+0x40>
 8008ff6:	4b38      	ldr	r3, [pc, #224]	; (80090d8 <xPortStartScheduler+0x124>)
 8008ff8:	617b      	str	r3, [r7, #20]
 8008ffa:	697b      	ldr	r3, [r7, #20]
 8008ffc:	781b      	ldrb	r3, [r3, #0]
 8008ffe:	b2db      	uxtb	r3, r3
 8009000:	607b      	str	r3, [r7, #4]
 8009002:	697b      	ldr	r3, [r7, #20]
 8009004:	22ff      	movs	r2, #255	; 0xff
 8009006:	701a      	strb	r2, [r3, #0]
 8009008:	697b      	ldr	r3, [r7, #20]
 800900a:	781b      	ldrb	r3, [r3, #0]
 800900c:	b2db      	uxtb	r3, r3
 800900e:	70fb      	strb	r3, [r7, #3]
 8009010:	78fb      	ldrb	r3, [r7, #3]
 8009012:	b2db      	uxtb	r3, r3
 8009014:	f003 0350 	and.w	r3, r3, #80	; 0x50
 8009018:	b2da      	uxtb	r2, r3
 800901a:	4b30      	ldr	r3, [pc, #192]	; (80090dc <xPortStartScheduler+0x128>)
 800901c:	701a      	strb	r2, [r3, #0]
 800901e:	4b30      	ldr	r3, [pc, #192]	; (80090e0 <xPortStartScheduler+0x12c>)
 8009020:	2207      	movs	r2, #7
 8009022:	601a      	str	r2, [r3, #0]
 8009024:	e009      	b.n	800903a <xPortStartScheduler+0x86>
 8009026:	4b2e      	ldr	r3, [pc, #184]	; (80090e0 <xPortStartScheduler+0x12c>)
 8009028:	681b      	ldr	r3, [r3, #0]
 800902a:	3b01      	subs	r3, #1
 800902c:	4a2c      	ldr	r2, [pc, #176]	; (80090e0 <xPortStartScheduler+0x12c>)
 800902e:	6013      	str	r3, [r2, #0]
 8009030:	78fb      	ldrb	r3, [r7, #3]
 8009032:	b2db      	uxtb	r3, r3
 8009034:	005b      	lsls	r3, r3, #1
 8009036:	b2db      	uxtb	r3, r3
 8009038:	70fb      	strb	r3, [r7, #3]
 800903a:	78fb      	ldrb	r3, [r7, #3]
 800903c:	b2db      	uxtb	r3, r3
 800903e:	f003 0380 	and.w	r3, r3, #128	; 0x80
 8009042:	2b80      	cmp	r3, #128	; 0x80
 8009044:	d0ef      	beq.n	8009026 <xPortStartScheduler+0x72>
 8009046:	4b26      	ldr	r3, [pc, #152]	; (80090e0 <xPortStartScheduler+0x12c>)
 8009048:	681b      	ldr	r3, [r3, #0]
 800904a:	f1c3 0307 	rsb	r3, r3, #7
 800904e:	2b04      	cmp	r3, #4
 8009050:	d009      	beq.n	8009066 <xPortStartScheduler+0xb2>
 8009052:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009056:	f383 8811 	msr	BASEPRI, r3
 800905a:	f3bf 8f6f 	isb	sy
 800905e:	f3bf 8f4f 	dsb	sy
 8009062:	60bb      	str	r3, [r7, #8]
 8009064:	e7fe      	b.n	8009064 <xPortStartScheduler+0xb0>
 8009066:	4b1e      	ldr	r3, [pc, #120]	; (80090e0 <xPortStartScheduler+0x12c>)
 8009068:	681b      	ldr	r3, [r3, #0]
 800906a:	021b      	lsls	r3, r3, #8
 800906c:	4a1c      	ldr	r2, [pc, #112]	; (80090e0 <xPortStartScheduler+0x12c>)
 800906e:	6013      	str	r3, [r2, #0]
 8009070:	4b1b      	ldr	r3, [pc, #108]	; (80090e0 <xPortStartScheduler+0x12c>)
 8009072:	681b      	ldr	r3, [r3, #0]
 8009074:	f403 63e0 	and.w	r3, r3, #1792	; 0x700
 8009078:	4a19      	ldr	r2, [pc, #100]	; (80090e0 <xPortStartScheduler+0x12c>)
 800907a:	6013      	str	r3, [r2, #0]
 800907c:	687b      	ldr	r3, [r7, #4]
 800907e:	b2da      	uxtb	r2, r3
 8009080:	697b      	ldr	r3, [r7, #20]
 8009082:	701a      	strb	r2, [r3, #0]
 8009084:	4a17      	ldr	r2, [pc, #92]	; (80090e4 <xPortStartScheduler+0x130>)
 8009086:	4b17      	ldr	r3, [pc, #92]	; (80090e4 <xPortStartScheduler+0x130>)
 8009088:	681b      	ldr	r3, [r3, #0]
 800908a:	f443 0370 	orr.w	r3, r3, #15728640	; 0xf00000
 800908e:	6013      	str	r3, [r2, #0]
 8009090:	4a14      	ldr	r2, [pc, #80]	; (80090e4 <xPortStartScheduler+0x130>)
 8009092:	4b14      	ldr	r3, [pc, #80]	; (80090e4 <xPortStartScheduler+0x130>)
 8009094:	681b      	ldr	r3, [r3, #0]
 8009096:	f043 4370 	orr.w	r3, r3, #4026531840	; 0xf0000000
 800909a:	6013      	str	r3, [r2, #0]
 800909c:	f000 f8d4 	bl	8009248 <vPortSetupTimerInterrupt>
 80090a0:	4b11      	ldr	r3, [pc, #68]	; (80090e8 <xPortStartScheduler+0x134>)
 80090a2:	2200      	movs	r2, #0
 80090a4:	601a      	str	r2, [r3, #0]
 80090a6:	f000 f8f3 	bl	8009290 <vPortEnableVFP>
 80090aa:	4a10      	ldr	r2, [pc, #64]	; (80090ec <xPortStartScheduler+0x138>)
 80090ac:	4b0f      	ldr	r3, [pc, #60]	; (80090ec <xPortStartScheduler+0x138>)
 80090ae:	681b      	ldr	r3, [r3, #0]
 80090b0:	f043 4340 	orr.w	r3, r3, #3221225472	; 0xc0000000
 80090b4:	6013      	str	r3, [r2, #0]
 80090b6:	f7ff ff67 	bl	8008f88 <prvPortStartFirstTask>
 80090ba:	f7fe ff0d 	bl	8007ed8 <vTaskSwitchContext>
 80090be:	f7ff ff23 	bl	8008f08 <prvTaskExitError>
 80090c2:	2300      	movs	r3, #0
 80090c4:	4618      	mov	r0, r3
 80090c6:	3718      	adds	r7, #24
 80090c8:	46bd      	mov	sp, r7
 80090ca:	bd80      	pop	{r7, pc}
 80090cc:	e000ed00 	.word	0xe000ed00
 80090d0:	410fc271 	.word	0x410fc271
 80090d4:	410fc270 	.word	0x410fc270
 80090d8:	e000e400 	.word	0xe000e400
 80090dc:	20001454 	.word	0x20001454
 80090e0:	20001458 	.word	0x20001458
 80090e4:	e000ed20 	.word	0xe000ed20
 80090e8:	200008ac 	.word	0x200008ac
 80090ec:	e000ef34 	.word	0xe000ef34

080090f0 <vPortEnterCritical>:
 80090f0:	b480      	push	{r7}
 80090f2:	b083      	sub	sp, #12
 80090f4:	af00      	add	r7, sp, #0
 80090f6:	f04f 0350 	mov.w	r3, #80	; 0x50
 80090fa:	f383 8811 	msr	BASEPRI, r3
 80090fe:	f3bf 8f6f 	isb	sy
 8009102:	f3bf 8f4f 	dsb	sy
 8009106:	607b      	str	r3, [r7, #4]
 8009108:	4b0e      	ldr	r3, [pc, #56]	; (8009144 <vPortEnterCritical+0x54>)
 800910a:	681b      	ldr	r3, [r3, #0]
 800910c:	3301      	adds	r3, #1
 800910e:	4a0d      	ldr	r2, [pc, #52]	; (8009144 <vPortEnterCritical+0x54>)
 8009110:	6013      	str	r3, [r2, #0]
 8009112:	4b0c      	ldr	r3, [pc, #48]	; (8009144 <vPortEnterCritical+0x54>)
 8009114:	681b      	ldr	r3, [r3, #0]
 8009116:	2b01      	cmp	r3, #1
 8009118:	d10e      	bne.n	8009138 <vPortEnterCritical+0x48>
 800911a:	4b0b      	ldr	r3, [pc, #44]	; (8009148 <vPortEnterCritical+0x58>)
 800911c:	681b      	ldr	r3, [r3, #0]
 800911e:	b2db      	uxtb	r3, r3
 8009120:	2b00      	cmp	r3, #0
 8009122:	d009      	beq.n	8009138 <vPortEnterCritical+0x48>
 8009124:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009128:	f383 8811 	msr	BASEPRI, r3
 800912c:	f3bf 8f6f 	isb	sy
 8009130:	f3bf 8f4f 	dsb	sy
 8009134:	603b      	str	r3, [r7, #0]
 8009136:	e7fe      	b.n	8009136 <vPortEnterCritical+0x46>
 8009138:	bf00      	nop
 800913a:	370c      	adds	r7, #12
 800913c:	46bd      	mov	sp, r7
 800913e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009142:	4770      	bx	lr
 8009144:	200008ac 	.word	0x200008ac
 8009148:	e000ed04 	.word	0xe000ed04

0800914c <vPortExitCritical>:
 800914c:	b480      	push	{r7}
 800914e:	b083      	sub	sp, #12
 8009150:	af00      	add	r7, sp, #0
 8009152:	4b11      	ldr	r3, [pc, #68]	; (8009198 <vPortExitCritical+0x4c>)
 8009154:	681b      	ldr	r3, [r3, #0]
 8009156:	2b00      	cmp	r3, #0
 8009158:	d109      	bne.n	800916e <vPortExitCritical+0x22>
 800915a:	f04f 0350 	mov.w	r3, #80	; 0x50
 800915e:	f383 8811 	msr	BASEPRI, r3
 8009162:	f3bf 8f6f 	isb	sy
 8009166:	f3bf 8f4f 	dsb	sy
 800916a:	607b      	str	r3, [r7, #4]
 800916c:	e7fe      	b.n	800916c <vPortExitCritical+0x20>
 800916e:	4b0a      	ldr	r3, [pc, #40]	; (8009198 <vPortExitCritical+0x4c>)
 8009170:	681b      	ldr	r3, [r3, #0]
 8009172:	3b01      	subs	r3, #1
 8009174:	4a08      	ldr	r2, [pc, #32]	; (8009198 <vPortExitCritical+0x4c>)
 8009176:	6013      	str	r3, [r2, #0]
 8009178:	4b07      	ldr	r3, [pc, #28]	; (8009198 <vPortExitCritical+0x4c>)
 800917a:	681b      	ldr	r3, [r3, #0]
 800917c:	2b00      	cmp	r3, #0
 800917e:	d104      	bne.n	800918a <vPortExitCritical+0x3e>
 8009180:	2300      	movs	r3, #0
 8009182:	603b      	str	r3, [r7, #0]
 8009184:	683b      	ldr	r3, [r7, #0]
 8009186:	f383 8811 	msr	BASEPRI, r3
 800918a:	bf00      	nop
 800918c:	370c      	adds	r7, #12
 800918e:	46bd      	mov	sp, r7
 8009190:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009194:	4770      	bx	lr
 8009196:	bf00      	nop
 8009198:	200008ac 	.word	0x200008ac
 800919c:	00000000 	.word	0x00000000

080091a0 <PendSV_Handler>:
 80091a0:	f3ef 8009 	mrs	r0, PSP
 80091a4:	f3bf 8f6f 	isb	sy
 80091a8:	4b15      	ldr	r3, [pc, #84]	; (8009200 <pxCurrentTCBConst>)
 80091aa:	681a      	ldr	r2, [r3, #0]
 80091ac:	f01e 0f10 	tst.w	lr, #16
 80091b0:	bf08      	it	eq
 80091b2:	ed20 8a10 	vstmdbeq	r0!, {s16-s31}
 80091b6:	e920 4ff0 	stmdb	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80091ba:	6010      	str	r0, [r2, #0]
 80091bc:	e92d 0009 	stmdb	sp!, {r0, r3}
 80091c0:	f04f 0050 	mov.w	r0, #80	; 0x50
 80091c4:	f380 8811 	msr	BASEPRI, r0
 80091c8:	f3bf 8f4f 	dsb	sy
 80091cc:	f3bf 8f6f 	isb	sy
 80091d0:	f7fe fe82 	bl	8007ed8 <vTaskSwitchContext>
 80091d4:	f04f 0000 	mov.w	r0, #0
 80091d8:	f380 8811 	msr	BASEPRI, r0
 80091dc:	bc09      	pop	{r0, r3}
 80091de:	6819      	ldr	r1, [r3, #0]
 80091e0:	6808      	ldr	r0, [r1, #0]
 80091e2:	e8b0 4ff0 	ldmia.w	r0!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80091e6:	f01e 0f10 	tst.w	lr, #16
 80091ea:	bf08      	it	eq
 80091ec:	ecb0 8a10 	vldmiaeq	r0!, {s16-s31}
 80091f0:	f380 8809 	msr	PSP, r0
 80091f4:	f3bf 8f6f 	isb	sy
 80091f8:	4770      	bx	lr
 80091fa:	bf00      	nop
 80091fc:	f3af 8000 	nop.w

08009200 <pxCurrentTCBConst>:
 8009200:	2000124c 	.word	0x2000124c
 8009204:	bf00      	nop
 8009206:	bf00      	nop

08009208 <SysTick_Handler>:
 8009208:	b580      	push	{r7, lr}
 800920a:	b082      	sub	sp, #8
 800920c:	af00      	add	r7, sp, #0
 800920e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009212:	f383 8811 	msr	BASEPRI, r3
 8009216:	f3bf 8f6f 	isb	sy
 800921a:	f3bf 8f4f 	dsb	sy
 800921e:	607b      	str	r3, [r7, #4]
 8009220:	f7fe fd9a 	bl	8007d58 <xTaskIncrementTick>
 8009224:	4603      	mov	r3, r0
 8009226:	2b00      	cmp	r3, #0
 8009228:	d003      	beq.n	8009232 <SysTick_Handler+0x2a>
 800922a:	4b06      	ldr	r3, [pc, #24]	; (8009244 <SysTick_Handler+0x3c>)
 800922c:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 8009230:	601a      	str	r2, [r3, #0]
 8009232:	2300      	movs	r3, #0
 8009234:	603b      	str	r3, [r7, #0]
 8009236:	683b      	ldr	r3, [r7, #0]
 8009238:	f383 8811 	msr	BASEPRI, r3
 800923c:	bf00      	nop
 800923e:	3708      	adds	r7, #8
 8009240:	46bd      	mov	sp, r7
 8009242:	bd80      	pop	{r7, pc}
 8009244:	e000ed04 	.word	0xe000ed04

08009248 <vPortSetupTimerInterrupt>:
 8009248:	b480      	push	{r7}
 800924a:	af00      	add	r7, sp, #0
 800924c:	4b0b      	ldr	r3, [pc, #44]	; (800927c <vPortSetupTimerInterrupt+0x34>)
 800924e:	2200      	movs	r2, #0
 8009250:	601a      	str	r2, [r3, #0]
 8009252:	4b0b      	ldr	r3, [pc, #44]	; (8009280 <vPortSetupTimerInterrupt+0x38>)
 8009254:	2200      	movs	r2, #0
 8009256:	601a      	str	r2, [r3, #0]
 8009258:	4a0a      	ldr	r2, [pc, #40]	; (8009284 <vPortSetupTimerInterrupt+0x3c>)
 800925a:	4b0b      	ldr	r3, [pc, #44]	; (8009288 <vPortSetupTimerInterrupt+0x40>)
 800925c:	681b      	ldr	r3, [r3, #0]
 800925e:	490b      	ldr	r1, [pc, #44]	; (800928c <vPortSetupTimerInterrupt+0x44>)
 8009260:	fba1 1303 	umull	r1, r3, r1, r3
 8009264:	099b      	lsrs	r3, r3, #6
 8009266:	3b01      	subs	r3, #1
 8009268:	6013      	str	r3, [r2, #0]
 800926a:	4b04      	ldr	r3, [pc, #16]	; (800927c <vPortSetupTimerInterrupt+0x34>)
 800926c:	2207      	movs	r2, #7
 800926e:	601a      	str	r2, [r3, #0]
 8009270:	bf00      	nop
 8009272:	46bd      	mov	sp, r7
 8009274:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009278:	4770      	bx	lr
 800927a:	bf00      	nop
 800927c:	e000e010 	.word	0xe000e010
 8009280:	e000e018 	.word	0xe000e018
 8009284:	e000e014 	.word	0xe000e014
 8009288:	200008b0 	.word	0x200008b0
 800928c:	10624dd3 	.word	0x10624dd3

08009290 <vPortEnableVFP>:
 8009290:	f8df 000c 	ldr.w	r0, [pc, #12]	; 80092a0 <vPortEnableVFP+0x10>
 8009294:	6801      	ldr	r1, [r0, #0]
 8009296:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 800929a:	6001      	str	r1, [r0, #0]
 800929c:	4770      	bx	lr
 800929e:	0000      	.short	0x0000
 80092a0:	e000ed88 	.word	0xe000ed88
 80092a4:	bf00      	nop
 80092a6:	bf00      	nop

080092a8 <vPortValidateInterruptPriority>:
 80092a8:	b480      	push	{r7}
 80092aa:	b085      	sub	sp, #20
 80092ac:	af00      	add	r7, sp, #0
 80092ae:	f3ef 8305 	mrs	r3, IPSR
 80092b2:	60fb      	str	r3, [r7, #12]
 80092b4:	68fb      	ldr	r3, [r7, #12]
 80092b6:	2b0f      	cmp	r3, #15
 80092b8:	d913      	bls.n	80092e2 <vPortValidateInterruptPriority+0x3a>
 80092ba:	4a16      	ldr	r2, [pc, #88]	; (8009314 <vPortValidateInterruptPriority+0x6c>)
 80092bc:	68fb      	ldr	r3, [r7, #12]
 80092be:	4413      	add	r3, r2
 80092c0:	781b      	ldrb	r3, [r3, #0]
 80092c2:	72fb      	strb	r3, [r7, #11]
 80092c4:	4b14      	ldr	r3, [pc, #80]	; (8009318 <vPortValidateInterruptPriority+0x70>)
 80092c6:	781b      	ldrb	r3, [r3, #0]
 80092c8:	7afa      	ldrb	r2, [r7, #11]
 80092ca:	429a      	cmp	r2, r3
 80092cc:	d209      	bcs.n	80092e2 <vPortValidateInterruptPriority+0x3a>
 80092ce:	f04f 0350 	mov.w	r3, #80	; 0x50
 80092d2:	f383 8811 	msr	BASEPRI, r3
 80092d6:	f3bf 8f6f 	isb	sy
 80092da:	f3bf 8f4f 	dsb	sy
 80092de:	607b      	str	r3, [r7, #4]
 80092e0:	e7fe      	b.n	80092e0 <vPortValidateInterruptPriority+0x38>
 80092e2:	4b0e      	ldr	r3, [pc, #56]	; (800931c <vPortValidateInterruptPriority+0x74>)
 80092e4:	681b      	ldr	r3, [r3, #0]
 80092e6:	f403 62e0 	and.w	r2, r3, #1792	; 0x700
 80092ea:	4b0d      	ldr	r3, [pc, #52]	; (8009320 <vPortValidateInterruptPriority+0x78>)
 80092ec:	681b      	ldr	r3, [r3, #0]
 80092ee:	429a      	cmp	r2, r3
 80092f0:	d909      	bls.n	8009306 <vPortValidateInterruptPriority+0x5e>
 80092f2:	f04f 0350 	mov.w	r3, #80	; 0x50
 80092f6:	f383 8811 	msr	BASEPRI, r3
 80092fa:	f3bf 8f6f 	isb	sy
 80092fe:	f3bf 8f4f 	dsb	sy
 8009302:	603b      	str	r3, [r7, #0]
 8009304:	e7fe      	b.n	8009304 <vPortValidateInterruptPriority+0x5c>
 8009306:	bf00      	nop
 8009308:	3714      	adds	r7, #20
 800930a:	46bd      	mov	sp, r7
 800930c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009310:	4770      	bx	lr
 8009312:	bf00      	nop
 8009314:	e000e3f0 	.word	0xe000e3f0
 8009318:	20001454 	.word	0x20001454
 800931c:	e000ed0c 	.word	0xe000ed0c
 8009320:	20001458 	.word	0x20001458

08009324 <vPortFree>:
 8009324:	b580      	push	{r7, lr}
 8009326:	b086      	sub	sp, #24
 8009328:	af00      	add	r7, sp, #0
 800932a:	6078      	str	r0, [r7, #4]
 800932c:	687b      	ldr	r3, [r7, #4]
 800932e:	617b      	str	r3, [r7, #20]
 8009330:	687b      	ldr	r3, [r7, #4]
 8009332:	2b00      	cmp	r3, #0
 8009334:	d04b      	beq.n	80093ce <vPortFree+0xaa>
 8009336:	2308      	movs	r3, #8
 8009338:	425b      	negs	r3, r3
 800933a:	697a      	ldr	r2, [r7, #20]
 800933c:	4413      	add	r3, r2
 800933e:	617b      	str	r3, [r7, #20]
 8009340:	697b      	ldr	r3, [r7, #20]
 8009342:	613b      	str	r3, [r7, #16]
 8009344:	693b      	ldr	r3, [r7, #16]
 8009346:	685a      	ldr	r2, [r3, #4]
 8009348:	4b23      	ldr	r3, [pc, #140]	; (80093d8 <vPortFree+0xb4>)
 800934a:	681b      	ldr	r3, [r3, #0]
 800934c:	4013      	ands	r3, r2
 800934e:	2b00      	cmp	r3, #0
 8009350:	d109      	bne.n	8009366 <vPortFree+0x42>
 8009352:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009356:	f383 8811 	msr	BASEPRI, r3
 800935a:	f3bf 8f6f 	isb	sy
 800935e:	f3bf 8f4f 	dsb	sy
 8009362:	60fb      	str	r3, [r7, #12]
 8009364:	e7fe      	b.n	8009364 <vPortFree+0x40>
 8009366:	693b      	ldr	r3, [r7, #16]
 8009368:	681b      	ldr	r3, [r3, #0]
 800936a:	2b00      	cmp	r3, #0
 800936c:	d009      	beq.n	8009382 <vPortFree+0x5e>
 800936e:	f04f 0350 	mov.w	r3, #80	; 0x50
 8009372:	f383 8811 	msr	BASEPRI, r3
 8009376:	f3bf 8f6f 	isb	sy
 800937a:	f3bf 8f4f 	dsb	sy
 800937e:	60bb      	str	r3, [r7, #8]
 8009380:	e7fe      	b.n	8009380 <vPortFree+0x5c>
 8009382:	693b      	ldr	r3, [r7, #16]
 8009384:	685a      	ldr	r2, [r3, #4]
 8009386:	4b14      	ldr	r3, [pc, #80]	; (80093d8 <vPortFree+0xb4>)
 8009388:	681b      	ldr	r3, [r3, #0]
 800938a:	4013      	ands	r3, r2
 800938c:	2b00      	cmp	r3, #0
 800938e:	d01e      	beq.n	80093ce <vPortFree+0xaa>
 8009390:	693b      	ldr	r3, [r7, #16]
 8009392:	681b      	ldr	r3, [r3, #0]
 8009394:	2b00      	cmp	r3, #0
 8009396:	d11a      	bne.n	80093ce <vPortFree+0xaa>
 8009398:	693b      	ldr	r3, [r7, #16]
 800939a:	685a      	ldr	r2, [r3, #4]
 800939c:	4b0e      	ldr	r3, [pc, #56]	; (80093d8 <vPortFree+0xb4>)
 800939e:	681b      	ldr	r3, [r3, #0]
 80093a0:	43db      	mvns	r3, r3
 80093a2:	401a      	ands	r2, r3
 80093a4:	693b      	ldr	r3, [r7, #16]
 80093a6:	605a      	str	r2, [r3, #4]
 80093a8:	f7fe fc1c 	bl	8007be4 <vTaskSuspendAll>
 80093ac:	693b      	ldr	r3, [r7, #16]
 80093ae:	685a      	ldr	r2, [r3, #4]
 80093b0:	4b0a      	ldr	r3, [pc, #40]	; (80093dc <vPortFree+0xb8>)
 80093b2:	681b      	ldr	r3, [r3, #0]
 80093b4:	4413      	add	r3, r2
 80093b6:	4a09      	ldr	r2, [pc, #36]	; (80093dc <vPortFree+0xb8>)
 80093b8:	6013      	str	r3, [r2, #0]
 80093ba:	6938      	ldr	r0, [r7, #16]
 80093bc:	f000 f812 	bl	80093e4 <prvInsertBlockIntoFreeList>
 80093c0:	4b07      	ldr	r3, [pc, #28]	; (80093e0 <vPortFree+0xbc>)
 80093c2:	681b      	ldr	r3, [r3, #0]
 80093c4:	3301      	adds	r3, #1
 80093c6:	4a06      	ldr	r2, [pc, #24]	; (80093e0 <vPortFree+0xbc>)
 80093c8:	6013      	str	r3, [r2, #0]
 80093ca:	f7fe fc19 	bl	8007c00 <xTaskResumeAll>
 80093ce:	bf00      	nop
 80093d0:	3718      	adds	r7, #24
 80093d2:	46bd      	mov	sp, r7
 80093d4:	bd80      	pop	{r7, pc}
 80093d6:	bf00      	nop
 80093d8:	20001470 	.word	0x20001470
 80093dc:	20001468 	.word	0x20001468
 80093e0:	2000146c 	.word	0x2000146c

080093e4 <prvInsertBlockIntoFreeList>:
 80093e4:	b480      	push	{r7}
 80093e6:	b085      	sub	sp, #20
 80093e8:	af00      	add	r7, sp, #0
 80093ea:	6078      	str	r0, [r7, #4]
 80093ec:	4b28      	ldr	r3, [pc, #160]	; (8009490 <prvInsertBlockIntoFreeList+0xac>)
 80093ee:	60fb      	str	r3, [r7, #12]
 80093f0:	e002      	b.n	80093f8 <prvInsertBlockIntoFreeList+0x14>
 80093f2:	68fb      	ldr	r3, [r7, #12]
 80093f4:	681b      	ldr	r3, [r3, #0]
 80093f6:	60fb      	str	r3, [r7, #12]
 80093f8:	68fb      	ldr	r3, [r7, #12]
 80093fa:	681a      	ldr	r2, [r3, #0]
 80093fc:	687b      	ldr	r3, [r7, #4]
 80093fe:	429a      	cmp	r2, r3
 8009400:	d3f7      	bcc.n	80093f2 <prvInsertBlockIntoFreeList+0xe>
 8009402:	68fb      	ldr	r3, [r7, #12]
 8009404:	60bb      	str	r3, [r7, #8]
 8009406:	68fb      	ldr	r3, [r7, #12]
 8009408:	685b      	ldr	r3, [r3, #4]
 800940a:	68ba      	ldr	r2, [r7, #8]
 800940c:	441a      	add	r2, r3
 800940e:	687b      	ldr	r3, [r7, #4]
 8009410:	429a      	cmp	r2, r3
 8009412:	d108      	bne.n	8009426 <prvInsertBlockIntoFreeList+0x42>
 8009414:	68fb      	ldr	r3, [r7, #12]
 8009416:	685a      	ldr	r2, [r3, #4]
 8009418:	687b      	ldr	r3, [r7, #4]
 800941a:	685b      	ldr	r3, [r3, #4]
 800941c:	441a      	add	r2, r3
 800941e:	68fb      	ldr	r3, [r7, #12]
 8009420:	605a      	str	r2, [r3, #4]
 8009422:	68fb      	ldr	r3, [r7, #12]
 8009424:	607b      	str	r3, [r7, #4]
 8009426:	687b      	ldr	r3, [r7, #4]
 8009428:	60bb      	str	r3, [r7, #8]
 800942a:	687b      	ldr	r3, [r7, #4]
 800942c:	685b      	ldr	r3, [r3, #4]
 800942e:	68ba      	ldr	r2, [r7, #8]
 8009430:	441a      	add	r2, r3
 8009432:	68fb      	ldr	r3, [r7, #12]
 8009434:	681b      	ldr	r3, [r3, #0]
 8009436:	429a      	cmp	r2, r3
 8009438:	d118      	bne.n	800946c <prvInsertBlockIntoFreeList+0x88>
 800943a:	68fb      	ldr	r3, [r7, #12]
 800943c:	681a      	ldr	r2, [r3, #0]
 800943e:	4b15      	ldr	r3, [pc, #84]	; (8009494 <prvInsertBlockIntoFreeList+0xb0>)
 8009440:	681b      	ldr	r3, [r3, #0]
 8009442:	429a      	cmp	r2, r3
 8009444:	d00d      	beq.n	8009462 <prvInsertBlockIntoFreeList+0x7e>
 8009446:	687b      	ldr	r3, [r7, #4]
 8009448:	685a      	ldr	r2, [r3, #4]
 800944a:	68fb      	ldr	r3, [r7, #12]
 800944c:	681b      	ldr	r3, [r3, #0]
 800944e:	685b      	ldr	r3, [r3, #4]
 8009450:	441a      	add	r2, r3
 8009452:	687b      	ldr	r3, [r7, #4]
 8009454:	605a      	str	r2, [r3, #4]
 8009456:	68fb      	ldr	r3, [r7, #12]
 8009458:	681b      	ldr	r3, [r3, #0]
 800945a:	681a      	ldr	r2, [r3, #0]
 800945c:	687b      	ldr	r3, [r7, #4]
 800945e:	601a      	str	r2, [r3, #0]
 8009460:	e008      	b.n	8009474 <prvInsertBlockIntoFreeList+0x90>
 8009462:	4b0c      	ldr	r3, [pc, #48]	; (8009494 <prvInsertBlockIntoFreeList+0xb0>)
 8009464:	681a      	ldr	r2, [r3, #0]
 8009466:	687b      	ldr	r3, [r7, #4]
 8009468:	601a      	str	r2, [r3, #0]
 800946a:	e003      	b.n	8009474 <prvInsertBlockIntoFreeList+0x90>
 800946c:	68fb      	ldr	r3, [r7, #12]
 800946e:	681a      	ldr	r2, [r3, #0]
 8009470:	687b      	ldr	r3, [r7, #4]
 8009472:	601a      	str	r2, [r3, #0]
 8009474:	68fa      	ldr	r2, [r7, #12]
 8009476:	687b      	ldr	r3, [r7, #4]
 8009478:	429a      	cmp	r2, r3
 800947a:	d002      	beq.n	8009482 <prvInsertBlockIntoFreeList+0x9e>
 800947c:	68fb      	ldr	r3, [r7, #12]
 800947e:	687a      	ldr	r2, [r7, #4]
 8009480:	601a      	str	r2, [r3, #0]
 8009482:	bf00      	nop
 8009484:	3714      	adds	r7, #20
 8009486:	46bd      	mov	sp, r7
 8009488:	f85d 7b04 	ldr.w	r7, [sp], #4
 800948c:	4770      	bx	lr
 800948e:	bf00      	nop
 8009490:	2000145c 	.word	0x2000145c
 8009494:	20001464 	.word	0x20001464

08009498 <SystemInit>:
 8009498:	b480      	push	{r7}
 800949a:	af00      	add	r7, sp, #0
 800949c:	4a16      	ldr	r2, [pc, #88]	; (80094f8 <SystemInit+0x60>)
 800949e:	4b16      	ldr	r3, [pc, #88]	; (80094f8 <SystemInit+0x60>)
 80094a0:	f8d3 3088 	ldr.w	r3, [r3, #136]	; 0x88
 80094a4:	f443 0370 	orr.w	r3, r3, #15728640	; 0xf00000
 80094a8:	f8c2 3088 	str.w	r3, [r2, #136]	; 0x88
 80094ac:	4a13      	ldr	r2, [pc, #76]	; (80094fc <SystemInit+0x64>)
 80094ae:	4b13      	ldr	r3, [pc, #76]	; (80094fc <SystemInit+0x64>)
 80094b0:	681b      	ldr	r3, [r3, #0]
 80094b2:	f043 0301 	orr.w	r3, r3, #1
 80094b6:	6013      	str	r3, [r2, #0]
 80094b8:	4b10      	ldr	r3, [pc, #64]	; (80094fc <SystemInit+0x64>)
 80094ba:	2200      	movs	r2, #0
 80094bc:	609a      	str	r2, [r3, #8]
 80094be:	4a0f      	ldr	r2, [pc, #60]	; (80094fc <SystemInit+0x64>)
 80094c0:	4b0e      	ldr	r3, [pc, #56]	; (80094fc <SystemInit+0x64>)
 80094c2:	681b      	ldr	r3, [r3, #0]
 80094c4:	f023 7384 	bic.w	r3, r3, #17301504	; 0x1080000
 80094c8:	f423 3380 	bic.w	r3, r3, #65536	; 0x10000
 80094cc:	6013      	str	r3, [r2, #0]
 80094ce:	4b0b      	ldr	r3, [pc, #44]	; (80094fc <SystemInit+0x64>)
 80094d0:	4a0b      	ldr	r2, [pc, #44]	; (8009500 <SystemInit+0x68>)
 80094d2:	605a      	str	r2, [r3, #4]
 80094d4:	4a09      	ldr	r2, [pc, #36]	; (80094fc <SystemInit+0x64>)
 80094d6:	4b09      	ldr	r3, [pc, #36]	; (80094fc <SystemInit+0x64>)
 80094d8:	681b      	ldr	r3, [r3, #0]
 80094da:	f423 2380 	bic.w	r3, r3, #262144	; 0x40000
 80094de:	6013      	str	r3, [r2, #0]
 80094e0:	4b06      	ldr	r3, [pc, #24]	; (80094fc <SystemInit+0x64>)
 80094e2:	2200      	movs	r2, #0
 80094e4:	60da      	str	r2, [r3, #12]
 80094e6:	4b04      	ldr	r3, [pc, #16]	; (80094f8 <SystemInit+0x60>)
 80094e8:	f04f 6200 	mov.w	r2, #134217728	; 0x8000000
 80094ec:	609a      	str	r2, [r3, #8]
 80094ee:	bf00      	nop
 80094f0:	46bd      	mov	sp, r7
 80094f2:	f85d 7b04 	ldr.w	r7, [sp], #4
 80094f6:	4770      	bx	lr
 80094f8:	e000ed00 	.word	0xe000ed00
 80094fc:	40023800 	.word	0x40023800
 8009500:	24003010 	.word	0x24003010

08009504 <arm_mat_init_f32>:
 8009504:	b480      	push	{r7}
 8009506:	b085      	sub	sp, #20
 8009508:	af00      	add	r7, sp, #0
 800950a:	60f8      	str	r0, [r7, #12]
 800950c:	607b      	str	r3, [r7, #4]
 800950e:	460b      	mov	r3, r1
 8009510:	817b      	strh	r3, [r7, #10]
 8009512:	4613      	mov	r3, r2
 8009514:	813b      	strh	r3, [r7, #8]
 8009516:	68fb      	ldr	r3, [r7, #12]
 8009518:	897a      	ldrh	r2, [r7, #10]
 800951a:	801a      	strh	r2, [r3, #0]
 800951c:	68fb      	ldr	r3, [r7, #12]
 800951e:	893a      	ldrh	r2, [r7, #8]
 8009520:	805a      	strh	r2, [r3, #2]
 8009522:	68fb      	ldr	r3, [r7, #12]
 8009524:	687a      	ldr	r2, [r7, #4]
 8009526:	605a      	str	r2, [r3, #4]
 8009528:	bf00      	nop
 800952a:	3714      	adds	r7, #20
 800952c:	46bd      	mov	sp, r7
 800952e:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009532:	4770      	bx	lr

08009534 <arm_mat_add_f32>:
 8009534:	b480      	push	{r7}
 8009536:	b091      	sub	sp, #68	; 0x44
 8009538:	af00      	add	r7, sp, #0
 800953a:	60f8      	str	r0, [r7, #12]
 800953c:	60b9      	str	r1, [r7, #8]
 800953e:	607a      	str	r2, [r7, #4]
 8009540:	68fb      	ldr	r3, [r7, #12]
 8009542:	685b      	ldr	r3, [r3, #4]
 8009544:	63fb      	str	r3, [r7, #60]	; 0x3c
 8009546:	68bb      	ldr	r3, [r7, #8]
 8009548:	685b      	ldr	r3, [r3, #4]
 800954a:	63bb      	str	r3, [r7, #56]	; 0x38
 800954c:	687b      	ldr	r3, [r7, #4]
 800954e:	685b      	ldr	r3, [r3, #4]
 8009550:	637b      	str	r3, [r7, #52]	; 0x34
 8009552:	68fb      	ldr	r3, [r7, #12]
 8009554:	881b      	ldrh	r3, [r3, #0]
 8009556:	461a      	mov	r2, r3
 8009558:	68fb      	ldr	r3, [r7, #12]
 800955a:	885b      	ldrh	r3, [r3, #2]
 800955c:	fb03 f302 	mul.w	r3, r3, r2
 8009560:	62fb      	str	r3, [r7, #44]	; 0x2c
 8009562:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8009564:	089b      	lsrs	r3, r3, #2
 8009566:	633b      	str	r3, [r7, #48]	; 0x30
 8009568:	e052      	b.n	8009610 <arm_mat_add_f32+0xdc>
 800956a:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 800956c:	681b      	ldr	r3, [r3, #0]
 800956e:	62bb      	str	r3, [r7, #40]	; 0x28
 8009570:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8009572:	681b      	ldr	r3, [r3, #0]
 8009574:	627b      	str	r3, [r7, #36]	; 0x24
 8009576:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8009578:	685b      	ldr	r3, [r3, #4]
 800957a:	623b      	str	r3, [r7, #32]
 800957c:	ed97 7a0a 	vldr	s14, [r7, #40]	; 0x28
 8009580:	edd7 7a09 	vldr	s15, [r7, #36]	; 0x24
 8009584:	ee77 7a27 	vadd.f32	s15, s14, s15
 8009588:	edc7 7a07 	vstr	s15, [r7, #28]
 800958c:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 800958e:	685b      	ldr	r3, [r3, #4]
 8009590:	61bb      	str	r3, [r7, #24]
 8009592:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 8009594:	689b      	ldr	r3, [r3, #8]
 8009596:	62bb      	str	r3, [r7, #40]	; 0x28
 8009598:	ed97 7a08 	vldr	s14, [r7, #32]
 800959c:	edd7 7a06 	vldr	s15, [r7, #24]
 80095a0:	ee77 7a27 	vadd.f32	s15, s14, s15
 80095a4:	edc7 7a05 	vstr	s15, [r7, #20]
 80095a8:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80095aa:	689b      	ldr	r3, [r3, #8]
 80095ac:	627b      	str	r3, [r7, #36]	; 0x24
 80095ae:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80095b0:	69fa      	ldr	r2, [r7, #28]
 80095b2:	601a      	str	r2, [r3, #0]
 80095b4:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80095b6:	3304      	adds	r3, #4
 80095b8:	697a      	ldr	r2, [r7, #20]
 80095ba:	601a      	str	r2, [r3, #0]
 80095bc:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 80095be:	68db      	ldr	r3, [r3, #12]
 80095c0:	623b      	str	r3, [r7, #32]
 80095c2:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 80095c4:	68db      	ldr	r3, [r3, #12]
 80095c6:	61bb      	str	r3, [r7, #24]
 80095c8:	ed97 7a0a 	vldr	s14, [r7, #40]	; 0x28
 80095cc:	edd7 7a09 	vldr	s15, [r7, #36]	; 0x24
 80095d0:	ee77 7a27 	vadd.f32	s15, s14, s15
 80095d4:	edc7 7a07 	vstr	s15, [r7, #28]
 80095d8:	ed97 7a08 	vldr	s14, [r7, #32]
 80095dc:	edd7 7a06 	vldr	s15, [r7, #24]
 80095e0:	ee77 7a27 	vadd.f32	s15, s14, s15
 80095e4:	edc7 7a05 	vstr	s15, [r7, #20]
 80095e8:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80095ea:	3308      	adds	r3, #8
 80095ec:	69fa      	ldr	r2, [r7, #28]
 80095ee:	601a      	str	r2, [r3, #0]
 80095f0:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 80095f2:	330c      	adds	r3, #12
 80095f4:	697a      	ldr	r2, [r7, #20]
 80095f6:	601a      	str	r2, [r3, #0]
 80095f8:	6bfb      	ldr	r3, [r7, #60]	; 0x3c
 80095fa:	3310      	adds	r3, #16
 80095fc:	63fb      	str	r3, [r7, #60]	; 0x3c
 80095fe:	6bbb      	ldr	r3, [r7, #56]	; 0x38
 8009600:	3310      	adds	r3, #16
 8009602:	63bb      	str	r3, [r7, #56]	; 0x38
 8009604:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8009606:	3310      	adds	r3, #16
 8009608:	637b      	str	r3, [r7, #52]	; 0x34
 800960a:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800960c:	3b01      	subs	r3, #1
 800960e:	633b      	str	r3, [r7, #48]	; 0x30
 8009610:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8009612:	2b00      	cmp	r3, #0
 8009614:	d1a9      	bne.n	800956a <arm_mat_add_f32+0x36>
 8009616:	6afb      	ldr	r3, [r7, #44]	; 0x2c
 8009618:	f003 0303 	and.w	r3, r3, #3
 800961c:	633b      	str	r3, [r7, #48]	; 0x30
 800961e:	e013      	b.n	8009648 <arm_mat_add_f32+0x114>
 8009620:	6b7b      	ldr	r3, [r7, #52]	; 0x34
 8009622:	1d1a      	adds	r2, r3, #4
 8009624:	637a      	str	r2, [r7, #52]	; 0x34
 8009626:	6bfa      	ldr	r2, [r7, #60]	; 0x3c
 8009628:	1d11      	adds	r1, r2, #4
 800962a:	63f9      	str	r1, [r7, #60]	; 0x3c
 800962c:	ed92 7a00 	vldr	s14, [r2]
 8009630:	6bba      	ldr	r2, [r7, #56]	; 0x38
 8009632:	1d11      	adds	r1, r2, #4
 8009634:	63b9      	str	r1, [r7, #56]	; 0x38
 8009636:	edd2 7a00 	vldr	s15, [r2]
 800963a:	ee77 7a27 	vadd.f32	s15, s14, s15
 800963e:	edc3 7a00 	vstr	s15, [r3]
 8009642:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 8009644:	3b01      	subs	r3, #1
 8009646:	633b      	str	r3, [r7, #48]	; 0x30
 8009648:	6b3b      	ldr	r3, [r7, #48]	; 0x30
 800964a:	2b00      	cmp	r3, #0
 800964c:	d1e8      	bne.n	8009620 <arm_mat_add_f32+0xec>
 800964e:	2300      	movs	r3, #0
 8009650:	74fb      	strb	r3, [r7, #19]
 8009652:	f997 3013 	ldrsb.w	r3, [r7, #19]
 8009656:	4618      	mov	r0, r3
 8009658:	3744      	adds	r7, #68	; 0x44
 800965a:	46bd      	mov	sp, r7
 800965c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009660:	4770      	bx	lr

08009662 <arm_mat_mult_f32>:
 8009662:	b480      	push	{r7}
 8009664:	b095      	sub	sp, #84	; 0x54
 8009666:	af00      	add	r7, sp, #0
 8009668:	60f8      	str	r0, [r7, #12]
 800966a:	60b9      	str	r1, [r7, #8]
 800966c:	607a      	str	r2, [r7, #4]
 800966e:	68fb      	ldr	r3, [r7, #12]
 8009670:	685b      	ldr	r3, [r3, #4]
 8009672:	64fb      	str	r3, [r7, #76]	; 0x4c
 8009674:	68bb      	ldr	r3, [r7, #8]
 8009676:	685b      	ldr	r3, [r3, #4]
 8009678:	64bb      	str	r3, [r7, #72]	; 0x48
 800967a:	68fb      	ldr	r3, [r7, #12]
 800967c:	685b      	ldr	r3, [r3, #4]
 800967e:	647b      	str	r3, [r7, #68]	; 0x44
 8009680:	687b      	ldr	r3, [r7, #4]
 8009682:	685b      	ldr	r3, [r3, #4]
 8009684:	62fb      	str	r3, [r7, #44]	; 0x2c
 8009686:	68fb      	ldr	r3, [r7, #12]
 8009688:	881b      	ldrh	r3, [r3, #0]
 800968a:	857b      	strh	r3, [r7, #42]	; 0x2a
 800968c:	68bb      	ldr	r3, [r7, #8]
 800968e:	885b      	ldrh	r3, [r3, #2]
 8009690:	853b      	strh	r3, [r7, #40]	; 0x28
 8009692:	68fb      	ldr	r3, [r7, #12]
 8009694:	885b      	ldrh	r3, [r3, #2]
 8009696:	84fb      	strh	r3, [r7, #38]	; 0x26
 8009698:	2300      	movs	r3, #0
 800969a:	873b      	strh	r3, [r7, #56]	; 0x38
 800969c:	8d7b      	ldrh	r3, [r7, #42]	; 0x2a
 800969e:	86bb      	strh	r3, [r7, #52]	; 0x34
 80096a0:	8f3b      	ldrh	r3, [r7, #56]	; 0x38
 80096a2:	009b      	lsls	r3, r3, #2
 80096a4:	6afa      	ldr	r2, [r7, #44]	; 0x2c
 80096a6:	4413      	add	r3, r2
 80096a8:	643b      	str	r3, [r7, #64]	; 0x40
 80096aa:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 80096ac:	877b      	strh	r3, [r7, #58]	; 0x3a
 80096ae:	68bb      	ldr	r3, [r7, #8]
 80096b0:	685b      	ldr	r3, [r3, #4]
 80096b2:	64bb      	str	r3, [r7, #72]	; 0x48
 80096b4:	2300      	movs	r3, #0
 80096b6:	86fb      	strh	r3, [r7, #54]	; 0x36
 80096b8:	f04f 0300 	mov.w	r3, #0
 80096bc:	63fb      	str	r3, [r7, #60]	; 0x3c
 80096be:	6c7b      	ldr	r3, [r7, #68]	; 0x44
 80096c0:	64fb      	str	r3, [r7, #76]	; 0x4c
 80096c2:	8cfb      	ldrh	r3, [r7, #38]	; 0x26
 80096c4:	089b      	lsrs	r3, r3, #2
 80096c6:	867b      	strh	r3, [r7, #50]	; 0x32
 80096c8:	e061      	b.n	800978e <arm_mat_mult_f32+0x12c>
 80096ca:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 80096cc:	681b      	ldr	r3, [r3, #0]
 80096ce:	623b      	str	r3, [r7, #32]
 80096d0:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 80096d2:	009b      	lsls	r3, r3, #2
 80096d4:	6cba      	ldr	r2, [r7, #72]	; 0x48
 80096d6:	4413      	add	r3, r2
 80096d8:	64bb      	str	r3, [r7, #72]	; 0x48
 80096da:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 80096dc:	681b      	ldr	r3, [r3, #0]
 80096de:	61fb      	str	r3, [r7, #28]
 80096e0:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 80096e2:	685b      	ldr	r3, [r3, #4]
 80096e4:	61bb      	str	r3, [r7, #24]
 80096e6:	ed97 7a07 	vldr	s14, [r7, #28]
 80096ea:	edd7 7a08 	vldr	s15, [r7, #32]
 80096ee:	ee67 7a27 	vmul.f32	s15, s14, s15
 80096f2:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 80096f6:	ee77 7a27 	vadd.f32	s15, s14, s15
 80096fa:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 80096fe:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 8009700:	681b      	ldr	r3, [r3, #0]
 8009702:	617b      	str	r3, [r7, #20]
 8009704:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 8009706:	009b      	lsls	r3, r3, #2
 8009708:	6cba      	ldr	r2, [r7, #72]	; 0x48
 800970a:	4413      	add	r3, r2
 800970c:	64bb      	str	r3, [r7, #72]	; 0x48
 800970e:	ed97 7a06 	vldr	s14, [r7, #24]
 8009712:	edd7 7a05 	vldr	s15, [r7, #20]
 8009716:	ee67 7a27 	vmul.f32	s15, s14, s15
 800971a:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 800971e:	ee77 7a27 	vadd.f32	s15, s14, s15
 8009722:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 8009726:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 8009728:	681b      	ldr	r3, [r3, #0]
 800972a:	623b      	str	r3, [r7, #32]
 800972c:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 800972e:	009b      	lsls	r3, r3, #2
 8009730:	6cba      	ldr	r2, [r7, #72]	; 0x48
 8009732:	4413      	add	r3, r2
 8009734:	64bb      	str	r3, [r7, #72]	; 0x48
 8009736:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 8009738:	689b      	ldr	r3, [r3, #8]
 800973a:	61fb      	str	r3, [r7, #28]
 800973c:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 800973e:	68db      	ldr	r3, [r3, #12]
 8009740:	61bb      	str	r3, [r7, #24]
 8009742:	ed97 7a07 	vldr	s14, [r7, #28]
 8009746:	edd7 7a08 	vldr	s15, [r7, #32]
 800974a:	ee67 7a27 	vmul.f32	s15, s14, s15
 800974e:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 8009752:	ee77 7a27 	vadd.f32	s15, s14, s15
 8009756:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 800975a:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 800975c:	681b      	ldr	r3, [r3, #0]
 800975e:	617b      	str	r3, [r7, #20]
 8009760:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 8009762:	009b      	lsls	r3, r3, #2
 8009764:	6cba      	ldr	r2, [r7, #72]	; 0x48
 8009766:	4413      	add	r3, r2
 8009768:	64bb      	str	r3, [r7, #72]	; 0x48
 800976a:	ed97 7a06 	vldr	s14, [r7, #24]
 800976e:	edd7 7a05 	vldr	s15, [r7, #20]
 8009772:	ee67 7a27 	vmul.f32	s15, s14, s15
 8009776:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 800977a:	ee77 7a27 	vadd.f32	s15, s14, s15
 800977e:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 8009782:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 8009784:	3310      	adds	r3, #16
 8009786:	64fb      	str	r3, [r7, #76]	; 0x4c
 8009788:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 800978a:	3b01      	subs	r3, #1
 800978c:	867b      	strh	r3, [r7, #50]	; 0x32
 800978e:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 8009790:	2b00      	cmp	r3, #0
 8009792:	d19a      	bne.n	80096ca <arm_mat_mult_f32+0x68>
 8009794:	8cfb      	ldrh	r3, [r7, #38]	; 0x26
 8009796:	f003 0303 	and.w	r3, r3, #3
 800979a:	867b      	strh	r3, [r7, #50]	; 0x32
 800979c:	e017      	b.n	80097ce <arm_mat_mult_f32+0x16c>
 800979e:	6cfb      	ldr	r3, [r7, #76]	; 0x4c
 80097a0:	1d1a      	adds	r2, r3, #4
 80097a2:	64fa      	str	r2, [r7, #76]	; 0x4c
 80097a4:	ed93 7a00 	vldr	s14, [r3]
 80097a8:	6cbb      	ldr	r3, [r7, #72]	; 0x48
 80097aa:	edd3 7a00 	vldr	s15, [r3]
 80097ae:	ee67 7a27 	vmul.f32	s15, s14, s15
 80097b2:	ed97 7a0f 	vldr	s14, [r7, #60]	; 0x3c
 80097b6:	ee77 7a27 	vadd.f32	s15, s14, s15
 80097ba:	edc7 7a0f 	vstr	s15, [r7, #60]	; 0x3c
 80097be:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 80097c0:	009b      	lsls	r3, r3, #2
 80097c2:	6cba      	ldr	r2, [r7, #72]	; 0x48
 80097c4:	4413      	add	r3, r2
 80097c6:	64bb      	str	r3, [r7, #72]	; 0x48
 80097c8:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 80097ca:	3b01      	subs	r3, #1
 80097cc:	867b      	strh	r3, [r7, #50]	; 0x32
 80097ce:	8e7b      	ldrh	r3, [r7, #50]	; 0x32
 80097d0:	2b00      	cmp	r3, #0
 80097d2:	d1e4      	bne.n	800979e <arm_mat_mult_f32+0x13c>
 80097d4:	6c3b      	ldr	r3, [r7, #64]	; 0x40
 80097d6:	1d1a      	adds	r2, r3, #4
 80097d8:	643a      	str	r2, [r7, #64]	; 0x40
 80097da:	6bfa      	ldr	r2, [r7, #60]	; 0x3c
 80097dc:	601a      	str	r2, [r3, #0]
 80097de:	8efb      	ldrh	r3, [r7, #54]	; 0x36
 80097e0:	3301      	adds	r3, #1
 80097e2:	86fb      	strh	r3, [r7, #54]	; 0x36
 80097e4:	68bb      	ldr	r3, [r7, #8]
 80097e6:	685a      	ldr	r2, [r3, #4]
 80097e8:	8efb      	ldrh	r3, [r7, #54]	; 0x36
 80097ea:	009b      	lsls	r3, r3, #2
 80097ec:	4413      	add	r3, r2
 80097ee:	64bb      	str	r3, [r7, #72]	; 0x48
 80097f0:	8f7b      	ldrh	r3, [r7, #58]	; 0x3a
 80097f2:	3b01      	subs	r3, #1
 80097f4:	877b      	strh	r3, [r7, #58]	; 0x3a
 80097f6:	8f7b      	ldrh	r3, [r7, #58]	; 0x3a
 80097f8:	2b00      	cmp	r3, #0
 80097fa:	f47f af5d 	bne.w	80096b8 <arm_mat_mult_f32+0x56>
 80097fe:	8f3a      	ldrh	r2, [r7, #56]	; 0x38
 8009800:	8d3b      	ldrh	r3, [r7, #40]	; 0x28
 8009802:	4413      	add	r3, r2
 8009804:	873b      	strh	r3, [r7, #56]	; 0x38
 8009806:	8cfb      	ldrh	r3, [r7, #38]	; 0x26
 8009808:	009b      	lsls	r3, r3, #2
 800980a:	6c7a      	ldr	r2, [r7, #68]	; 0x44
 800980c:	4413      	add	r3, r2
 800980e:	647b      	str	r3, [r7, #68]	; 0x44
 8009810:	8ebb      	ldrh	r3, [r7, #52]	; 0x34
 8009812:	3b01      	subs	r3, #1
 8009814:	86bb      	strh	r3, [r7, #52]	; 0x34
 8009816:	8ebb      	ldrh	r3, [r7, #52]	; 0x34
 8009818:	2b00      	cmp	r3, #0
 800981a:	f47f af41 	bne.w	80096a0 <arm_mat_mult_f32+0x3e>
 800981e:	2300      	movs	r3, #0
 8009820:	74fb      	strb	r3, [r7, #19]
 8009822:	f997 3013 	ldrsb.w	r3, [r7, #19]
 8009826:	4618      	mov	r0, r3
 8009828:	3754      	adds	r7, #84	; 0x54
 800982a:	46bd      	mov	sp, r7
 800982c:	f85d 7b04 	ldr.w	r7, [sp], #4
 8009830:	4770      	bx	lr
	...

08009834 <register_fini>:
 8009834:	4b02      	ldr	r3, [pc, #8]	; (8009840 <register_fini+0xc>)
 8009836:	b113      	cbz	r3, 800983e <register_fini+0xa>
 8009838:	4802      	ldr	r0, [pc, #8]	; (8009844 <register_fini+0x10>)
 800983a:	f7f6 bf03 	b.w	8000644 <atexit>
 800983e:	4770      	bx	lr
 8009840:	00000000 	.word	0x00000000
 8009844:	08000651 	.word	0x08000651

08009848 <_init>:
 8009848:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 800984a:	bf00      	nop
 800984c:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800984e:	bc08      	pop	{r3}
 8009850:	469e      	mov	lr, r3
 8009852:	4770      	bx	lr

08009854 <_fini>:
 8009854:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 8009856:	bf00      	nop
 8009858:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800985a:	bc08      	pop	{r3}
 800985c:	469e      	mov	lr, r3
 800985e:	4770      	bx	lr
