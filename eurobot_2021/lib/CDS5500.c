#include <stdlib.h>
#include "CDS5500.h"

#include "stm32f407xx.h"
#include "stm32f4xx_ll_usart.h"

#include "gpio.h"
#include "periph.h"

#include "FreeRTOS.h"
#include "task.h"

void manip_usart_config()
{
        /* Enable clocking on USART and DMA */
        LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);
        LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART3);

        /* Init communication pins */
        LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);

        LL_GPIO_SetPinMode(STM_DRIVER_USART_TX_PORT, STM_DRIVER_USART_TX_PIN,
                           LL_GPIO_MODE_ALTERNATE);
        LL_GPIO_SetAFPin_8_15(STM_DRIVER_USART_TX_PORT, STM_DRIVER_USART_TX_PIN,
                             STM_DRIVER_USART_PIN_AF);
        LL_GPIO_SetPinOutputType(STM_DRIVER_USART_TX_PORT,
                                 STM_DRIVER_USART_TX_PIN,
                                 LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(STM_DRIVER_USART_TX_PORT, STM_DRIVER_USART_TX_PIN,
                           LL_GPIO_PULL_UP);
        LL_GPIO_SetPinSpeed(STM_DRIVER_USART_TX_PORT, STM_DRIVER_USART_TX_PIN,
                            LL_GPIO_SPEED_FREQ_HIGH);

        LL_GPIO_SetPinMode(STM_DRIVER_USART_RX_PORT, STM_DRIVER_USART_RX_PIN,
                           LL_GPIO_MODE_ALTERNATE);
        LL_GPIO_SetAFPin_8_15(STM_DRIVER_USART_RX_PORT, STM_DRIVER_USART_RX_PIN,
                             STM_DRIVER_USART_PIN_AF);
        LL_GPIO_SetPinOutputType(STM_DRIVER_USART_RX_PORT,
                                 STM_DRIVER_USART_RX_PIN,
                                 LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(STM_DRIVER_USART_RX_PORT, STM_DRIVER_USART_RX_PIN,
                           LL_GPIO_PULL_UP);
        LL_GPIO_SetPinSpeed(STM_DRIVER_USART_RX_PORT, STM_DRIVER_USART_RX_PIN,
                            LL_GPIO_SPEED_FREQ_HIGH);

        /* UART configuration for communication */
        LL_USART_SetTransferDirection(STM_DRIVER_USART,
                                      LL_USART_DIRECTION_TX_RX);
        LL_USART_SetParity(STM_DRIVER_USART, LL_USART_PARITY_NONE);
        LL_USART_SetDataWidth(STM_DRIVER_USART, LL_USART_DATAWIDTH_8B);
        LL_USART_SetStopBitsLength(STM_DRIVER_USART, LL_USART_STOPBITS_1);
        LL_USART_SetHWFlowCtrl(STM_DRIVER_USART, LL_USART_HWCONTROL_NONE);
        LL_USART_SetBaudRate(STM_DRIVER_USART,
                             SystemCoreClock/STM_DRIVER_USART_PERIPH_PRESCALER,
                             LL_USART_OVERSAMPLING_16,
                             STM_DRIVER_USART_BAUDRATE);
        LL_USART_EnableDirectionRx(STM_DRIVER_USART);
        LL_USART_EnableDirectionTx(STM_DRIVER_USART);
        LL_USART_EnableDMAReq_RX(STM_DRIVER_USART);
        LL_USART_EnableIT_IDLE(STM_DRIVER_USART);
        LL_USART_Enable(STM_DRIVER_USART);

        NVIC_SetPriority(STM_DRIVER_USART_IRQN, STM_DRIVER_USART_IRQN_PRIORITY);
        NVIC_EnableIRQ(STM_DRIVER_USART_IRQN);

        /* DMA configuration for communication */
        LL_DMA_SetChannelSelection(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM,
                                   STM_DRIVER_DMA_CHANNEL);
        LL_DMA_ConfigAddresses(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM,
                               STM_DRIVER_DMA_SRC_ADDR,
                               (uint32_t)malloc(TERM_STM_DR_BUF_SIZE),
                               STM_DRIVER_DMA_DIRECTION);
        LL_DMA_SetDataLength(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM,
                             STM_DRIVER_DMA_BUFFER_SIZE);
        LL_DMA_SetMemoryIncMode(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM,
                                STM_DRIVER_DMA_MEM_INC_MODE);

        LL_DMA_EnableStream(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM);
        LL_DMA_EnableIT_TC(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM);

        /* Enable global communication DMA stream interrupts */
        NVIC_SetPriority(STM_DRIVER_DMA_STREAM_IRQN,
                         STM_DRIVER_DMA_STREAM_IRQN_PRIORITY);
        NVIC_EnableIRQ(STM_DRIVER_DMA_STREAM_IRQN);
        return;
}

void manip_hw_config()
{
        LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
        LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
        
        LL_GPIO_SetPinMode(COMP_1_PORT, COMP_1_PIN, LL_GPIO_MODE_OUTPUT);
        LL_GPIO_SetPinOutputType(COMP_1_PORT, COMP_1_PIN, LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(COMP_1_PORT, COMP_1_PIN, LL_GPIO_PULL_NO);
        
        LL_GPIO_SetPinMode(COMP_2_PORT, COMP_2_PIN, LL_GPIO_MODE_OUTPUT);
        LL_GPIO_SetPinOutputType(COMP_2_PORT, COMP_2_PIN, LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(COMP_2_PORT, COMP_2_PIN, LL_GPIO_PULL_NO);
        
        LL_GPIO_SetPinMode(COMP_PWM_PORT, COMP_PWM_PIN, LL_GPIO_MODE_OUTPUT);
        LL_GPIO_SetPinOutputType(COMP_PWM_PORT, COMP_PWM_PIN, LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(COMP_PWM_PORT, COMP_PWM_PIN, LL_GPIO_PULL_NO);
        
        LL_GPIO_SetPinMode(VALVE_PORT, VALVE_PIN, LL_GPIO_MODE_OUTPUT);
        LL_GPIO_SetPinOutputType(VALVE_PORT, VALVE_PIN, LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(VALVE_PORT, VALVE_PIN, LL_GPIO_PULL_NO);
        
        LL_GPIO_SetPinMode(KEY_SWITCH_PORT, KEY_SWITCH_PIN, LL_GPIO_MODE_INPUT);
        LL_GPIO_SetPinPull(KEY_SWITCH_PORT, KEY_SWITCH_PIN, LL_GPIO_PULL_UP);
        
        LL_GPIO_SetPinMode(CHOOSE_SIDE_PORT, CHOOSE_SIDE_PIN, LL_GPIO_MODE_INPUT);
        LL_GPIO_SetPinPull(CHOOSE_SIDE_PORT, CHOOSE_SIDE_PIN, LL_GPIO_PULL_UP);
        /* 
        LL_GPIO_SetPinMode(COMP_PWM_PORT, COMP_PWM_PIN, LL_GPIO_MODE_ALTERNATE);
        LL_GPIO_SetAFPin_0_7(COMP_PWM_PORT, COMP_PWM_PIN, COMP_PWM_PIN_AF); 
        LL_GPIO_SetPinOutputType(COMP_PWM_PORT, COMP_PWM_PIN, LL_GPIO_OUTPUT_PUSHPULL);

        LL_TIM_EnableUpdateEvent(COMP_TIM);
        LL_TIM_SetClockDivision(COMP_TIM, LL_TIM_CLOCKDIVISION_DIV4); 
        LL_TIM_SetCounterMode(COMP_TIM, LL_TIM_COUNTERMODE_UP);
        LL_TIM_SetAutoReload(COMP_TIM, COMP_PWM_TIM_ARR);
        LL_TIM_SetUpdateSource(COMP_TIM, LL_TIM_UPDATESOURCE_REGULAR);
        LL_TIM_CC_EnableChannel(COMP_TIM, COMP_TIM_CHANNEL);
        LL_TIM_OC_SetMode(COMP_TIM, COMP_TIM_CHANNEL, LL_TIM_OCMODE_PWM1);
        LL_TIM_OC_EnableFast(COMP_TIM, COMP_TIM_CHANNEL);
        LL_TIM_OC_EnablePreload(COMP_TIM, COMP_TIM_CHANNEL);
        LL_TIM_EnableARRPreload(COMP_TIM);
        LL_TIM_OC_SetCompareCH4(COMP_TIM, COMP_PWM_TIM_CCR_INIT);
        LL_TIM_GenerateEvent_UPDATE(COMP_TIM);
        LL_TIM_EnableCounter(COMP_TIM);
        */
        LL_GPIO_ResetOutputPin(COMP_1_PORT, COMP_1_PIN);
        LL_GPIO_ResetOutputPin(COMP_2_PORT, COMP_2_PIN);
        LL_GPIO_ResetOutputPin(COMP_PWM_PORT, COMP_PWM_PIN);
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        
        
        LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        
        vTaskDelay(3000);
        
        LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        
        vTaskDelay(3000);
        LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        
        vTaskDelay(3000);
        return;
}

static void manip_usart_response(int data)
{
        LL_USART_ClearFlag_TC(STM_DRIVER_USART);
        while (!LL_USART_IsActiveFlag_TXE(STM_DRIVER_USART))
            taskYIELD();
        LL_USART_TransmitData8(STM_DRIVER_USART, data);
        while (!LL_USART_IsActiveFlag_TC(STM_DRIVER_USART))
            taskYIELD();
        return;
}

void WritePos(int ID, int position, int velocity)
{ 
	int messageLength = 7;
	int pos2 = position / 255;
	int pos = position % 255;	
	
	int vel2 = velocity / 255;
	int vel = velocity % 255;
    	
	manip_usart_response(startByte);              // send some data
    manip_usart_response(startByte);
    manip_usart_response(ID);
    manip_usart_response(messageLength);
	manip_usart_response(INST_WRITE);
	manip_usart_response(P_GOAL_POSITION_L);
	manip_usart_response(pos);
	manip_usart_response(pos2);
	manip_usart_response(vel);
	manip_usart_response(vel2);
    int last_data = (~(ID + (messageLength) + INST_WRITE 
                     + P_GOAL_POSITION_L + pos + pos2 + vel + vel2)) & 0xFF;
	manip_usart_response(last_data);
    return;
}

void RegWritePos(int ID, int position, int velocity)
{
	int messageLength = 5;
	int pos2 = position / 255;
	int pos = position % 255;	
	
	int vel2 = velocity / 255;
	int vel = velocity % 255;

	manip_usart_response(startByte);              // send some data
	manip_usart_response(startByte);
	manip_usart_response(ID);
	manip_usart_response(messageLength);
	manip_usart_response(INST_REG_WRITE);
	manip_usart_response(P_GOAL_POSITION_L);
	manip_usart_response(pos);
	manip_usart_response(pos2);
    int last_data = (~(ID + (messageLength) + INST_WRITE
                     + P_GOAL_POSITION_L + pos + pos2 + vel + vel2)) & 0xFF;
	manip_usart_response(last_data);
}

void RegWriteAction()
{
	int messageLength = 2;
	int ID = 0xFE; 

	manip_usart_response(startByte);              // send some data
	manip_usart_response(startByte);
	manip_usart_response(ID);
	manip_usart_response(messageLength);
	manip_usart_response(INST_ACTION);
	int last_data = (~(ID + (messageLength) + INST_ACTION)) & 0xFF;
    manip_usart_response(last_data);
}

void SetID(int ID, int newID)
{
	int messageLength = 4;
	manip_usart_response(startByte);              // send some data
	manip_usart_response(startByte);
	manip_usart_response(ID);
	manip_usart_response(messageLength);
	manip_usart_response(INST_WRITE);
	manip_usart_response(P_ID);
	manip_usart_response(newID);
    int last_data = (~(ID + (messageLength) + INST_WRITE+ P_ID + newID)) & 0xFF;
	manip_usart_response(last_data);
}

void SetAngleLimit(int ID, int lowerLimit, int upperLimit)
{
	int messageLength = 8;
	int lowerLimitB = lowerLimit % 255;
	int lowerLimitS = lowerLimit / 255;
	int upperLimitB = upperLimit % 255;
	int upperLimitS = upperLimit / 255;
	manip_usart_response(startByte);              // send some data
	manip_usart_response(startByte);
	manip_usart_response(ID);
	manip_usart_response(messageLength);
	manip_usart_response(INST_WRITE);
	manip_usart_response(0x08);
	manip_usart_response(upperLimitB);
	manip_usart_response(0x09);
	manip_usart_response(upperLimitS);
	manip_usart_response(0x06);
	manip_usart_response(lowerLimitB);
	manip_usart_response(0x07);
	manip_usart_response(lowerLimitS);
    int last_data = (~(ID + (messageLength) + INST_WRITE
                     + 0x08 + 0x09 + 0x06 + 0x07 + upperLimitB
                     + upperLimitS + lowerLimitB + lowerLimitS)) & 0xFF;
	manip_usart_response(last_data);
}

void tempMethod()
{
	manip_usart_response(startByte);              // send some data
	manip_usart_response(startByte);
	manip_usart_response(0x01);
	manip_usart_response(0x05);
	manip_usart_response(0x03);
	manip_usart_response(0x08);
	manip_usart_response(0xFF);
	manip_usart_response(0x01);
	manip_usart_response(0xF4);
	manip_usart_response(0xef);
}

void Reset(int ID)
{
	manip_usart_response(startByte);              // send some data
	manip_usart_response(startByte);
	manip_usart_response(ID);
	manip_usart_response(0x02);
	manip_usart_response(0x06);
    int last_data = (~(ID + 0x02 +0x06)) & 0xFF;
	manip_usart_response(last_data);
}

void SetTempLimit(int ID, int temperature)
{
	manip_usart_response(startByte);              // send some data
	manip_usart_response(startByte);
	manip_usart_response(ID);
	manip_usart_response(0x04);
	manip_usart_response(0x03);
	manip_usart_response(0x0B);
	manip_usart_response(temperature);
    int last_data = (~(ID + 0x04 +0x03 + 0x0B + temperature)) & 0xFF;
	manip_usart_response(last_data);
}

void servo(void *args)
{
    (void) args;
    
    manip_hw_config();

    while(1)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    }
    return;
}

int cmd_set_servo_pose(void *args)
{
    cmd_set_servo_pose_t *cmd_args = (cmd_set_servo_pose_t *)args;
    if (cmd_args->id == 1)
    {
        WritePos(cmd_args->id, cmd_args->pos, cmd_args->vel);
        WritePos(cmd_args->id + 1, 1024 - cmd_args->pos, cmd_args->vel);
    }
    WritePos(cmd_args->id, cmd_args->pos, cmd_args->vel);
    memcpy(args, "ok", 3);
    return 3;
}

int cmd_set_servo_angle(void *args)
{
    cmd_set_servo_angle_t *cmd_args = (cmd_set_servo_angle_t *)args;
    if (cmd_args->angle > 300 || cmd_args->angle < 0)
    {
        goto error_set_servo_angle;
    }
    int pose = cmd_args->angle * 1023 / 300;
    WritePos(cmd_args->id, pose, cmd_args->vel);
    memcpy(args, "OK", 3);
    return 3;
error_set_servo_angle:
    memcpy(args, "ER", 3);
    return 3;
}

int cmd_set_comp_state(void *args)
{
    cmd_set_comp_state_t *cmd_args = (cmd_set_comp_state_t *)args;
    if (cmd_args->state == 1)
    {
        LL_GPIO_SetOutputPin(COMP_1_PORT, COMP_1_PIN);
        LL_GPIO_ResetOutputPin(COMP_2_PORT, COMP_2_PIN);
        LL_GPIO_SetOutputPin(COMP_PWM_PORT, COMP_PWM_PIN);
        //LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        /*
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_SetOutputPin(VALVE_PORT, VALVE_PIN);
        vTaskDelay(3000);
        
        LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
        */
    }
    else if (cmd_args->state == 0)
    {
        LL_GPIO_ResetOutputPin(COMP_1_PORT, COMP_1_PIN);
        LL_GPIO_ResetOutputPin(COMP_2_PORT, COMP_2_PIN);
        LL_GPIO_ResetOutputPin(COMP_PWM_PORT, COMP_PWM_PIN);
        //LL_GPIO_ResetOutputPin(VALVE_PORT, VALVE_PIN);
    }
    else
    {
        goto error_set_comp_state;
    }
    memcpy(args, "OK", 3);
    return 3;
error_set_comp_state:
    memcpy(args, "ER", 3);
    return 3;
}

int cmd_get_key_state(void *args)
{
    int key_state[1];
    if (LL_GPIO_IsInputPinSet(KEY_SWITCH_PORT, KEY_SWITCH_PIN))
    {
        key_state[0] = 0;
        memcpy(args, key_state, 1);
        return 1;
    }
    else
    {
        key_state[0] = 1;
        memcpy(args, key_state, 1);
        return 1;
    }
}

int cmd_get_side(void *args)
{
    int side[1];
    if (LL_GPIO_IsInputPinSet(CHOOSE_SIDE_PORT, CHOOSE_SIDE_PIN))
    {
        side[0] = 0;
        memcpy(args, side, 1);
        return 1;
    }
    else
    {
        side[0] = 1;
        memcpy(args, side, 1);
        return 1;
    }
}

void USART3_IRQHandler(void)
{
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        LL_USART_ClearFlag_IDLE(STM_DRIVER_USART);
        LL_DMA_DisableStream(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void DMA1_Stream1_IRQHandler(void)
{
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (LL_DMA_IsActiveFlag_TC1(STM_DRIVER_DMA)) {
                LL_DMA_ClearFlag_TC1(STM_DRIVER_DMA);
                LL_DMA_ClearFlag_HT1(STM_DRIVER_DMA);
                LL_DMA_EnableStream(STM_DRIVER_DMA, STM_DRIVER_DMA_STREAM);
        }
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
